
#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "RatingBar.h"
#import <MapKit/MapKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "XMLReader.h"
#import "RideDashboard.h"
#import "CBAutoScrollLabel.h"
@interface RouteDetailViewController : UIViewController<UIScrollViewDelegate,GMSMapViewDelegate,MKMapViewDelegate>{
    
    UIScrollView *scrole_views;
    UIPageControl *pages_;
    UIView *scroles_back_view;
    UIView *desc_view;
    UILabel *place_name,*place_desc;
    UIActivityIndicatorView  *activeIndicatore;
    UIView *indicaterview;
    UIImage *img_name;
    NSString *url_str_for_images,*check_str;
    NSString *encoded_path;
}
@property (strong, nonatomic) IBOutlet UIButton *comments_Right_Btn;
@property (weak, nonatomic) IBOutlet MKMapView *map;
@property (strong, nonatomic) IBOutlet UIButton *coments_Left_BTN;

@property (weak, nonatomic) IBOutlet UIView *view_map;

@property (weak, nonatomic) IBOutlet UILabel *route_desc_lbl;
@property (weak, nonatomic) IBOutlet UIScrollView *main_scrl_view;
@property (weak, nonatomic) IBOutlet RatingBar *rating_view;

@property (weak, nonatomic) IBOutlet UILabel *route_title_lble;
- (IBAction)left_click:(id)sender;

- (IBAction)right_click:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *comments_view;
@property (strong, nonatomic) IBOutlet UIScrollView *comments_scrlview;
@property (weak, nonatomic) IBOutlet UITableView *table_view;
@property BOOL pageControlBeingUsed;

@property (weak, nonatomic) IBOutlet UIView *bottom_View;
@property (weak, nonatomic) IBOutlet UIView *content_view;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *content_View_Height_Constrint;
@property (nonatomic, strong) NSString *route_id;
@property (weak, nonatomic) IBOutlet UIImageView *comment_image;
@property (weak, nonatomic) IBOutlet UILabel *comment_label;
@property (weak, nonatomic) IBOutlet UIView *comment_line_view;
@property (weak, nonatomic) IBOutlet UILabel *places_label;
@property (weak, nonatomic) IBOutlet UIImageView *placesDetails_image;
@property (weak, nonatomic) IBOutlet UIButton *routeDetail_rideGoingBtn;
- (IBAction)onClick_routeDetail_rideGoingBtn:(id)sender;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *routeDetail_scrollLabel;

@end
