//
//  EditGroupViewController.h
//  openroadrides
//
//  Created by apple on 13/12/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RideInviteFriends.h"
#import "Constants.h"
#import "APIHelper.h"
#import "AppHelperClass.h"
#import "AppDelegate.h"
#import "DeleteMemberCell.h"
#import "CBAutoScrollLabel.h"
@interface EditGroupViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>
{
    UIBarButtonItem *item0;
    UIImage *cameraImage;
    NSData *profileImgData;
    NSString *base64String;
    UIActivityIndicatorView *activeIndicatore;
    UIView *indicaterview;
    NSString *existing_Image,*group_iD;
    NSMutableArray *existing_Group_Members_Array;
    NSInteger delete_Member_index;
}
@property (weak, nonatomic) IBOutlet UIImageView *logo_img;
@property (weak, nonatomic) IBOutlet UIButton *editGroup_ProfileImageBtn;
- (IBAction)onClick_editGroup_profileImageBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *editGroup_groupNameTF;
@property (weak, nonatomic) IBOutlet UITextField *editGroup_groupDescriptionTF;
@property (weak, nonatomic) IBOutlet UIButton *editGroup_inviteMembersBtn;
- (IBAction)onClick_editGroup_inviteMembersBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *editGroup_selectedMembersLabel;
@property(strong,nonatomic)NSDictionary *Group_Details;
@property (weak, nonatomic) IBOutlet UIView *deleteGroupView;
@property (weak, nonatomic) IBOutlet UITableView *deleteMember_tableView;
- (IBAction)onClick_deleteMember:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *members_View_height;
@property (weak, nonatomic) IBOutlet UIButton *editGroup_rideGoingBtn;
- (IBAction)onClick_editGroup_rideGoingBtn:(id)sender;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *editGroup_scrollLabel;

@end
