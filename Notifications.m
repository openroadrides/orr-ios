//
//  Notifications.m
//  openroadrides
//
//  Created by apple on 05/06/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "Notifications.h"
#import "NotificationCell.h"
#import "Constants.h"
#import "APIHelper.h"
#import "AppHelperClass.h"
#import "NSDate+NVTimeAgo.h"

@interface Notifications (){
 
    NSString *readstatus,*notification_type,*notification_ID;
     NSString *timestatus;
}

@end

@implementation Notifications

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.title = @"NOTIFICATIONS";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    self.noData_notifications_label.hidden=YES;
    self.navigationController.navigationBar.hidden=NO;
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBarTintColor:APP_YELLOW_COLOR];
    
    self.notifications_scrollLabel.hidden=YES;
    self.notifications_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.notifications_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.notifications_scrollLabel.textColor = [UIColor blackColor];
    self.notifications_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.notifications_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.notifications_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.notifications_scrollLabel.scrollSpeed = 60; // pixels per second
    self.notifications_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.notifications_scrollLabel.fadeLength = 0.f;
    self.notifications_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.notifications_scrollLabel observeApplicationNotifications];
    
    self.notifications_rideGoingBtn.hidden=YES;
    if (appDelegate.ridedashboard_home) {
        self.notifications_scrollLabel.hidden=NO;
        self.notifications_rideGoingBtn.hidden=NO;
        self.bottomConstraint_notificationsTV.constant=30;
    }
    
#pragma mark - Bar buttons on nav bar.....
    
    
  
    UIBarButtonItem *cancle_btn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:back_Image] style:UIBarButtonItemStylePlain target:self action:@selector(cancle_clicked)];
//    [cancle_btn setTitleTextAttributes:@{
//                                         NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:26.0],
//                                         NSForegroundColorAttributeName: [UIColor blackColor]
//                                         } forState:UIControlStateNormal];
    
    self.navigationItem.leftBarButtonItem = cancle_btn;
    

    
    
    user_token_id = [NSString stringWithFormat:@"%@",[Defaults valueForKey:User_ID]];
//    _main_content_vw.hidden = YES;
   
    self.notification_table_view.dataSource=self;
    self.notification_table_view.delegate=self;
   self.notification_table_view.separatorStyle=UITableViewCellSeparatorStyleNone;
    
    self.notification_table_view.backgroundColor=[UIColor colorWithRed:41/255.0 green:41/255.0 blue:41/255.0 alpha:1.0];

    
    indicaterview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    activeIndicatore.backgroundColor=[UIColor clearColor];
    activeIndicatore = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.frame = CGRectMake(0.0, 0.0, 80, 80);
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    [indicaterview addSubview:activeIndicatore];
    [self.view addSubview:indicaterview];
    [indicaterview bringSubviewToFront:self.view];
    [activeIndicatore startAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    indicaterview.hidden=YES;

    
    refreshControl = [[UIRefreshControl alloc]init];
    refreshControl.tintColor=APP_YELLOW_COLOR;
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
    [refreshControl addTarget:self action:@selector(getNotifications) forControlEvents:UIControlEventValueChanged];
    [self.notification_table_view addSubview:refreshControl];
    
    
    
    pending_rides_array=[self get_pending_Rides_From_DB];
     [self getNotifications];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden=NO;
    
}

#pragma mark - Tableview methods......
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    notification_type=[[_notifications_array objectAtIndex:indexPath.row] valueForKey:@"notification_type"];
//    NSString*about_notification=[[_notifications_array objectAtIndex:indexPath.row] valueForKey:@"notification_about"];
    if ([notification_type isEqualToString:@"inline"]) {
        return 70;
    }
    else if ([notification_type isEqualToString:@"invitation"])
    {
//        if ([about_notification isEqualToString:@"friend"])
//        {
//            return 120;
//        }
//        else if ([about_notification isEqualToString:@"ride"])
//        {
//            return 120;
//        }
//        else if ([about_notification isEqualToString:@"group"])
//        {
//            return 120;
//        }
        return 140;
    }
    else if ([notification_type isEqualToString:@"start"]) {
        return 70;
    }
    else{
    
    return 70;
    }
  
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _notifications_array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    static NSString *simpleTableIdentifier = @"NotificationCell";
//    NotificationCell *cell = (NotificationCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    NotificationCell *cell=[tableView dequeueReusableCellWithIdentifier:@"NotificationCell"];
    
    if (cell == nil)
    {
        cell = [[NotificationCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }

    
//    self.notification_table_view.separatorStyle=UITableViewCellSeparatorStyleNone;
     [self.notification_table_view setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [self.notification_table_view setSeparatorColor:[UIColor blackColor]];
//    if (cell == nil)
//    {
//        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NotificationCell" owner:self options:nil];
//        cell = [nib objectAtIndex:0];
//    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.inline_view.hidden=YES;
    cell.invitation_notification_view.hidden=YES;
    cell.start_notification_view.hidden=YES;
    
    notification_type=[[_notifications_array objectAtIndex:indexPath.row] valueForKey:@"notification_type"];
     NSString*about_notification=[[_notifications_array objectAtIndex:indexPath.row] valueForKey:@"notification_about"];
    
    
    cell.acceptView.layer.borderWidth=1.0f;
    cell.reject_view.layer.borderWidth=1.0f;
    cell.acceptView.layer.borderColor=[[UIColor whiteColor] CGColor];
    cell.reject_view.layer.borderColor=[[UIColor whiteColor] CGColor];
    
    
    if ([notification_type isEqualToString:@"inline"]) {
        cell.inline_view.hidden=NO;
    cell.description_lbl.text = [[_notifications_array objectAtIndex:indexPath.row] valueForKey:@"notification_content"];
    
       
    }
   else if ([notification_type isEqualToString:@"invitation"]) {
      
       cell.invitation_notification_view.hidden=NO;
       
      
       if ([about_notification isEqualToString:@"friend"]) {
           cell.userName_label.text=[[_notifications_array objectAtIndex:indexPath.row] valueForKey:@"notification_line1"];
           cell.address_label.text=[[_notifications_array objectAtIndex:indexPath.row] valueForKey:@"notification_line2"];
           cell.notification_group_created_label.hidden=YES;
           
           
           NSString *profile_image_string1 = [NSString stringWithFormat:@"%@", [[_notifications_array objectAtIndex:indexPath.row] objectForKey:@"notification_image"]];
           NSURL*url=[NSURL URLWithString:profile_image_string1];
           [cell.profile_imageView sd_setImageWithURL:url
                                           placeholderImage:[UIImage imageNamed:@"user_Placeholder"]
                                                    options:SDWebImageRefreshCached];
           
           
           cell.requestView_widthConstraint.constant=8;
//           [cell.reject_view updateConstraints];
           cell.created_by_label.hidden=YES;
           cell.notification_ride_title.hidden=YES;
//
//           
           cell.profile_imageView.hidden=NO;
           cell.userName_label.hidden=NO;
           cell.location_imageView.hidden=NO;
           cell.address_label.hidden=NO;
           
       }
       else if ([about_notification isEqualToString:@"ride"])
       {
           cell.created_by_label.hidden=NO;
           cell.notification_ride_title.hidden=NO;
//           cell.notification_ride_title.text=[[_notifications_array objectAtIndex:indexPath.row] valueForKey:@"notification_content"];
//           cell.created_by_label.text=[NSString stringWithFormat:@"Invited by :%@",[[_notifications_array objectAtIndex:indexPath.row] valueForKey:@"notification_line1"]];
           
           cell.notification_ride_title.text=[[_notifications_array objectAtIndex:indexPath.row] valueForKey:@"notification_line1"];
           cell.created_by_label.text=[NSString stringWithFormat:@"Invited by :%@",[[_notifications_array objectAtIndex:indexPath.row] valueForKey:@"notification_line2"]];
           NSString *ride_image_string1 = [NSString stringWithFormat:@"%@", [[_notifications_array objectAtIndex:indexPath.row] objectForKey:@"notification_image"]];
           NSURL*url=[NSURL URLWithString:ride_image_string1];
           [cell.profile_imageView sd_setImageWithURL:url
                                     placeholderImage:[UIImage imageNamed:@"user_Placeholder"]
                                              options:SDWebImageRefreshCached];
           cell.requestView_widthConstraint.constant=8;
//           [cell.reject_view updateConstraints];
           cell.profile_imageView.hidden=NO;
           cell.userName_label.hidden=YES;
           cell.location_imageView.hidden=YES;
           cell.address_label.hidden=YES;
           cell.notification_group_created_label.hidden=YES;
       }
      else if ([about_notification isEqualToString:@"group"]) {
//           cell.userName_label.text=[[_notifications_array objectAtIndex:indexPath.row] valueForKey:@"notification_content"];
//           cell.notification_group_created_label.text=[NSString stringWithFormat:@"Created by : %@",[[_notifications_array objectAtIndex:indexPath.row] valueForKey:@"notification_line1"] ];
          
          cell.userName_label.text=[[_notifications_array objectAtIndex:indexPath.row] valueForKey:@"notification_line1"];
          cell.notification_group_created_label.text=[NSString stringWithFormat:@"Created by : %@",[[_notifications_array objectAtIndex:indexPath.row] valueForKey:@"notification_line2"] ];
          
          NSString *profile_image_string = [NSString stringWithFormat:@"%@", [[_notifications_array objectAtIndex:indexPath.row] objectForKey:@"notification_image"]];
          NSURL*url=[NSURL URLWithString:profile_image_string];
          [cell.profile_imageView sd_setImageWithURL:url
                                    placeholderImage:[UIImage imageNamed:@"user_Placeholder"]
                                             options:SDWebImageRefreshCached];
          cell.requestView_widthConstraint.constant=8;
//          [cell.reject_view updateConstraints];
           cell.created_by_label.hidden=YES;
           cell.notification_ride_title.hidden=YES;
          cell.location_imageView.hidden=YES;
          cell.address_label.hidden=YES;
           cell.notification_group_created_label.hidden=NO;
           cell.profile_imageView.hidden=NO;
           cell.userName_label.hidden=NO;
          
          
           
       }

       
    }
   else if ([notification_type isEqualToString:@"start"]) {
      
       cell.start_notification_view.hidden=NO;
     
      
       cell.join_ride_label.text=[[_notifications_array objectAtIndex:indexPath.row] valueForKey:@"notification_content"];
       
   }
    readstatus= [[_notifications_array objectAtIndex:indexPath.row] objectForKey:@"read_status"];
    
    if ([readstatus isEqualToString:@"0"])
    {
        
        cell.contentView.backgroundColor = [UIColor colorWithRed:26/255.0 green:26/255.0 blue:26/255.0 alpha:1.0];
        
        cell.time_lbl.backgroundColor=[UIColor colorWithRed:26/255.0 green:26/255.0 blue:26/255.0 alpha:1.0];;
    }
    else if ([readstatus isEqualToString:@"1"])
    {
        cell.contentView.backgroundColor = [UIColor clearColor];
         cell.time_lbl.backgroundColor=[UIColor clearColor];
        
    }
    cell.description_lbl.textColor = APP_YELLOW_COLOR;
    timestatus =[[_notifications_array objectAtIndex:indexPath.row] objectForKey:@"notification_time"];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [df setTimeZone:timeZone];
    NSDate *notifi_date=[df dateFromString:timestatus];
    df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSTimeZone *time_zone=[NSTimeZone localTimeZone];
    [df setTimeZone:time_zone];
    NSString *local_time_=[df stringFromDate:notifi_date];
  
    
    NSString *mysqlDatetime = local_time_;
    NSString *timeAgoFormattedDate =[self dateDiff:mysqlDatetime];
    cell.time_lbl.text = timeAgoFormattedDate;
    notification_ID=[[_notifications_array objectAtIndex:indexPath.row] valueForKey:@"notificatioins_id"];
    cell.notification_accept_btn.tag=indexPath.row;
    cell.notification_reject_btn.tag=indexPath.row;
    return cell;
}


-(NSString *)dateDiff:(NSString *)origDate {
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setFormatterBehavior:NSDateFormatterBehavior10_4];
        [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *convertedDate = [df dateFromString:origDate];
        NSDate *todayDate = [NSDate date];
        double ti = [convertedDate timeIntervalSinceDate:todayDate];
        ti = ti * -1;
        if(ti < 1) {
            return @"never";
        } else  if (ti < 60) {
            return @"just now";
        } else if (ti < 3600) {
            int diff = round(ti / 60);
             if (diff >1) {
                  return [NSString stringWithFormat:@"%d minutes ago", diff];
             }
            else
            return [NSString stringWithFormat:@"%d minute ago", diff];
        } else if (ti < 86400) {
            int diff = round(ti / 60 / 60);
            if (diff >1) {
                return[NSString stringWithFormat:@"%d hours ago", diff];
            }
            else
            return[NSString stringWithFormat:@"%d hour ago", diff];
        } else if (ti < 2629743) {
            int diff = round(ti / 60 / 60 / 24);
            if (diff >1) {
                return[NSString stringWithFormat:@"%d days ago", diff];
            }
            else
                return[NSString stringWithFormat:@"%d day ago", diff];
            
        }
        else
        {
            int diff = round(ti / 60 / 60 / 24);
            return[NSString stringWithFormat:@"%d days ago", diff]; //this is for months
        }
    }


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    notification_ID=[[_notifications_array objectAtIndex:indexPath.row] valueForKey:@"notificatioins_id"];
    readstatus=[[_notifications_array objectAtIndex:indexPath.row] valueForKey:@"read_status"];
    
    NSLog(@" about This Notification %@",[_notifications_array objectAtIndex:indexPath.row]);
    
    
    NSString *status_touch=@"";
    NSString *about_notification=[[_notifications_array objectAtIndex:indexPath.row] valueForKey:@"notification_about"];
    if ([about_notification isEqualToString:@"ride"]) {
    NSString *notification_type_is=[[_notifications_array objectAtIndex:indexPath.row] valueForKey:@"notification_type"];
        if ([notification_type_is isEqualToString:@"start"]) {
            if (pending_rides_array.count>0) {
                //he has pending rides show alert and push home dashboard
                if (appDelegate.ridedashboard_home) {
                    [self rideGoingAlert:@"You cannot start the ride while the ride is going on."];
                }
                return;
            }
            else
            {
            status_touch=@"YES";
            NSString *ride_id=[NSString stringWithFormat:@"%@",[[_notifications_array objectAtIndex:indexPath.row] valueForKey:@"notification_type_id"]];
            RideDashboard *rideDashboard=[self.storyboard instantiateViewControllerWithIdentifier:@"RideDashboardStoryboard"];
            rideDashboard.is_From=@"NOTIFICATIONS";
            rideDashboard.ride_id_From_Notifications=ride_id;
            [self.navigationController pushViewController:rideDashboard animated:YES];
            }
        }
        else{
             status_touch=@"";
        }
    }
    else if ([about_notification isEqualToString:@"friend"])
    {
          NSString *notification_type_is=[[_notifications_array objectAtIndex:indexPath.row] valueForKey:@"notification_type"];
         if ([notification_type_is isEqualToString:@"invitation"]) {
            ProfileView*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileView"];
             NSString *friend_id=[NSString stringWithFormat:@"%@",[[_notifications_array objectAtIndex:indexPath.row] valueForKey:@"notification_type_id"]];
             controller.id_user=friend_id;
             controller.friend_status=@"";
             controller.is_From=@"Notifications";
             [self.navigationController pushViewController:controller animated:YES];
         }
    }
    else if ([about_notification isEqualToString:@"group"])
    {
        NSString *notification_type_is=[[_notifications_array objectAtIndex:indexPath.row] valueForKey:@"notification_type"];
        if ([notification_type_is isEqualToString:@"invitation"]) {
            GroupDetailViewController *cntlr = [self.storyboard instantiateViewControllerWithIdentifier:@"GroupDetailViewController"];
             NSString *group_id_str=[NSString stringWithFormat:@"%@",[[_notifications_array objectAtIndex:indexPath.row] valueForKey:@"notification_type_id"]];
            cntlr.group_id = group_id_str;
            cntlr.is_from=@"Notification_groupDetails";
            [self.navigationController pushViewController:cntlr animated:YES];
            
            
        }
    }

    
//    "notificatioins_id" = 304;
//    "notification_about" = ride;
//    "notification_content" = "srk ram is started FREE_RIDE 74";
//    "notification_image" = "<null>";
//    "notification_line1" = "srk ram";
//    "notification_line2" = "";
//    "notification_time" = "2017-07-26 14:12:37";
//    "notification_type" = start;
//    "notification_type_id" = 487;
//    "read_status" = 0;
//    "updated_at" = "<null>";
//    "user_id" = 46;
    
    
    if ([status_touch isEqualToString:@""]) {
    if ([readstatus isEqualToString:@"0"]) {
         [self notification_read_unread_status_request];
    }
    else if ([readstatus isEqualToString:@"1"])
    {
        NSLog(@"notification already seen");
    }
    }
    else{
        if ([readstatus isEqualToString:@"0"]) {
            [self notification_read_unread_status_request];
        }
    }
    
    }

-(void)getNotifications{
    [refreshControl endRefreshing];
if (![SHARED_HELPER checkIfisInternetAvailable])
{
    [self Show_Alert_Pop_Up:Nonetwork];
    return;
}

//    [activeIndicatore startAnimating];
    indicaterview.hidden=NO;

//[SHARED_API notificationsRequestsWithParams:user_token_id withSuccess:^(NSDictionary *response) {
    [SHARED_API notificationsRequestsWithParams:[Defaults valueForKey:User_ID] withSuccess:^(NSDictionary *response) {
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       NSLog(@"responce is %@",response);
                       if ([[response valueForKey:STATUS]isEqualToString:SUCCESS]) {
                       _notifications_array = [[NSMutableArray alloc]init];
                       _notifications_array=[response valueForKey:@"notifications"];
                       
                       if (_notifications_array.count >0 ) {
                           
                           [self.notification_table_view reloadData];
                            self.noData_notifications_label.hidden=YES;
                       }
                       else{
                            self.noData_notifications_label.hidden=NO;
                       }
                            _main_content_vw.hidden = NO;
                          
                       }
                       else{
                           [SHARED_HELPER showAlert:ServiceFail];
                           [self service_fail_OR_EmptyData];
                       }
//                        [activeIndicatore stopAnimating];
                       indicaterview.hidden=YES;
                   });
    
} onfailure:^(NSError *theError) {
    
    dispatch_async(dispatch_get_main_queue(), ^
                   {
//                       [activeIndicatore stopAnimating];
                       indicaterview.hidden=YES;
                       NSString *error_msg=[theError localizedDescription];
                       [self Show_Alert_Pop_Up:error_msg];
                   });
}];
}


-(void)search_clicked{
    
}

-(void)cancle_clicked
{
    if ([_is_from isEqualToString:@"Launching"]) {
        HomeDashboardView*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"HomeDashboardView"];
        [self.navigationController pushViewController:controller animated:YES];
        return;
    }
    else
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)service_fail_OR_EmptyData{
    int duration = 2; // duration in seconds
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        if ([_is_from isEqualToString:@"Launching"]) {
           HomeDashboardView *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"HomeDashboardView"];
            [self.navigationController pushViewController:controller animated:YES];
        }
        else
        [self.navigationController popViewControllerAnimated:YES];
    });
}
//"{
//""user_id"":"""",
//""notificatioins_id"":""""
//}"
-(void)notification_read_unread_status_request
{
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }

    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:notification_ID forKey:@"notificatioins_id"];
    [dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
    NSLog(@"%@",dict);
    [SHARED_API notification_read_unread_status_RequestsWithParams:dict withSuccess:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSLog(@"Notification read Status responce is %@",response);
            
            if ([[response valueForKey:STATUS] isEqualToString:SUCCESS]) {
//                [activeIndicatore stopAnimating];
                indicaterview.hidden=YES;
                [self getNotifications];
            }
            else{
                [self getNotifications];
            }
        });
        
    } onfailure:^(NSError *theError) {
        dispatch_async(dispatch_get_main_queue(), ^{
//            [activeIndicatore stopAnimating];
            indicaterview.hidden=YES;
            [SHARED_HELPER showAlert:ServerConnection];
        });
    }];
}


- (IBAction)onClick_notification_reject_btn:(UIButton *)sender {
//     [activeIndicatore startAnimating];
    indicaterview.hidden=NO;
    
    notification_ID=[[_notifications_array objectAtIndex:sender.tag] valueForKey:@"notificatioins_id"];
    readstatus=[[_notifications_array objectAtIndex:sender.tag] valueForKey:@"read_status"];
    
     NSLog(@"Reject Type notification");
    NSString*notification_type_id=[[_notifications_array objectAtIndex:sender.tag] valueForKey:@"notification_type_id"];
    NSLog(@"Notification_type_id : %@",notification_type_id);
     NSString*notification_reject_type=[[_notifications_array objectAtIndex:sender.tag] valueForKey:@"notification_type"];
     NSLog(@"Notification_type_id : %@",notification_reject_type);
    NSString*notification_about=[[_notifications_array objectAtIndex:sender.tag] valueForKey:@"notification_about"];
     NSLog(@"Notification_type_id : %@",notification_about);
    if ([notification_reject_type isEqualToString:@"invitation"]) {
        if ([notification_about isEqualToString:@"friend"]) {
            
            NSMutableDictionary *myFrnd_dict = [NSMutableDictionary new];
            [myFrnd_dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
            [myFrnd_dict setObject:[NSString stringWithFormat:@"%@",notification_type_id] forKey:@"user_friend_id"];
            [myFrnd_dict setValue:[NSString stringWithFormat:@"reject"] forKey:@"accept_or_reject_status"];
            NSLog(@"Friend reject Dict %@",myFrnd_dict);
            [SHARED_API accept_reject_frnd_request:myFrnd_dict withSuccess:^(NSDictionary *response)
             {
                 
                 NSLog(@"reject Frnd In notification Response %@",response);
                 if([[response objectForKey:STATUS] isEqualToString:SUCCESS])
                 {
                     if ([readstatus isEqualToString:@"0"]) {
                         [self notification_read_unread_status_request];
                     }
                     else{
                         [self getNotifications];
                     }
                    
                 }
                 else if([[response objectForKey:STATUS] isEqualToString:FAIL])
                 {
                     [SHARED_HELPER showAlert:ServiceFail2];
                   
                 }
                 else if([[response objectForKey:STATUS] isEqualToString:@"ride_started"])
                 {
                     if ([readstatus isEqualToString:@"0"]) {
                         [self notification_read_unread_status_request];
                     }
                     else{
                         [self getNotifications];
                     }
                 }
                 
                 indicaterview.hidden=YES;
                 
             } onfailure:^(NSError *theError)
             {
                 NSLog(@"Accept Frnd In Error %@",theError);
//                  [activeIndicatore stopAnimating];
                 indicaterview.hidden=YES;
             }];
            

            
        }
        else if ([notification_about isEqualToString:@"group"]) {
//            "{
//            ""group_id"":"""",
//            ""user_id"":"""",
//            ""accept_or_reject_status"":""accept / reject""
//        }"
            NSMutableDictionary *group_dict = [NSMutableDictionary new];
            [group_dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
            [group_dict setObject:[NSString stringWithFormat:@"%@",notification_type_id] forKey:@"group_id"];
            [group_dict setValue:[NSString stringWithFormat:@"reject"] forKey:@"accept_or_reject_status"];
            NSLog(@"Group Reject Dict %@",group_dict);
            [SHARED_API accept_reject_group_request:group_dict withSuccess:^(NSDictionary *response)
             {
                 
                 NSLog(@"reject Group In notification Response %@",response);
                 if([[response objectForKey:STATUS] isEqualToString:SUCCESS])
                 {
                     if ([readstatus isEqualToString:@"0"]) {
                         [self notification_read_unread_status_request];
                     }
                     else{
                         [self getNotifications];
                     }
                    
                 }
                 else if([[response objectForKey:STATUS] isEqualToString:FAIL])
                 {
                     [SHARED_HELPER showAlert:ServiceFail2];
                 }
//                   [activeIndicatore stopAnimating];
                 indicaterview.hidden=YES;
                 
             } onfailure:^(NSError *theError)
             {
                 NSLog(@"Accept Frnd In Error %@",theError);
//                  [activeIndicatore stopAnimating];
                 indicaterview.hidden=YES;
             }];
            
        }
        else if ([notification_about isEqualToString:@"ride"]) {
            NSMutableDictionary *ride_dict = [NSMutableDictionary new];
            
            [ride_dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
            
            [ride_dict setObject:[NSString stringWithFormat:@"%@",notification_type_id] forKey:@"ride_id"];
            
            [ride_dict setObject:[NSString stringWithFormat:@""] forKey:@"invited_user_id"];
            
            [ride_dict setValue:[NSString stringWithFormat:@"reject"] forKey:@"accept_or_reject_status"];
            
            NSLog(@"Group Accept Dict %@",ride_dict);
            
            [SHARED_API accept_reject_notification_ride_request:ride_dict withSuccess:^(NSDictionary *response)
             
             {
                 
                 NSLog(@"reject Ride In notification Response %@",response);
                 
                 if([[response objectForKey:STATUS] isEqualToString:SUCCESS])
                     
                 {
                     
                     if ([readstatus isEqualToString:@"0"]) {
                         [self notification_read_unread_status_request];
                     }
                     else{
                         [self getNotifications];
                     }
                    
                 }
                 
                 else if([[response objectForKey:STATUS] isEqualToString:FAIL])
                     
                 {
                     
                     [SHARED_HELPER showAlert:ServiceFail2];
                     
                 }
//                 [activeIndicatore stopAnimating];
                 indicaterview.hidden=YES;
                 
             } onfailure:^(NSError *theError)
             
             {
                 
                 NSLog(@"Accept Frnd In Error %@",theError);
                 
//                  [activeIndicatore stopAnimating];
                 indicaterview.hidden=YES;
                 
             }];
        }
    }
    
    
}
- (IBAction)onClick_notification_accept_btn:(UIButton *)sender {
   
//    [activeIndicatore startAnimating];
     indicaterview.hidden=NO;
    NSLog(@"Accept Type notification");
    notification_ID=[[_notifications_array objectAtIndex:sender.tag] valueForKey:@"notificatioins_id"];
    readstatus=[[_notifications_array objectAtIndex:sender.tag] valueForKey:@"read_status"];
    
    
    
    NSString*notification_type_id1=[[_notifications_array objectAtIndex:sender.tag] valueForKey:@"notification_type_id"];
    NSLog(@"Notification_type_id : %@",notification_type_id1);
    NSString*notification_accept_type=[[_notifications_array objectAtIndex:sender.tag] valueForKey:@"notification_type"];
    NSLog(@"Notification_type_id : %@",notification_accept_type);
    NSString*notification_about1=[[_notifications_array objectAtIndex:sender.tag] valueForKey:@"notification_about"];
    NSLog(@"Notification_type_id : %@",notification_about1);
    if ([notification_accept_type isEqualToString:@"invitation"]) {
        if ([notification_about1 isEqualToString:@"friend"]) {
            
//            Accept Frend Request
            NSMutableDictionary *myFrnd_dict = [NSMutableDictionary new];
            [myFrnd_dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
            [myFrnd_dict setObject:[NSString stringWithFormat:@"%@",notification_type_id1] forKey:@"user_friend_id"];
            [myFrnd_dict setValue:[NSString stringWithFormat:@"accept"] forKey:@"accept_or_reject_status"];
            NSLog(@"Friend accept Dict %@",myFrnd_dict);
            [SHARED_API accept_reject_frnd_request:myFrnd_dict withSuccess:^(NSDictionary *response)
             {
                 
                 NSLog(@"Accept Frnd In notification Response %@",response);
                 if([[response objectForKey:STATUS] isEqualToString:SUCCESS])
                 {
                        [activeIndicatore stopAnimating];
                     
                     if ([readstatus isEqualToString:@"0"]) {
                         [self notification_read_unread_status_request];
                     }
                     else{
                           [self getNotifications];
                     }
                 }
                 else if([[response objectForKey:STATUS] isEqualToString:FAIL])
                 {
                      [activeIndicatore stopAnimating];
                     [SHARED_HELPER showAlert:ServiceFail2];
                 }
                 
                 
             } onfailure:^(NSError *theError)
             {
                 NSLog(@"Accept Frnd In Error %@",theError);
//                  [activeIndicatore stopAnimating];
                 indicaterview.hidden=YES;
             }];
        }
        else if ([notification_about1 isEqualToString:@"group"]) {
            
            NSMutableDictionary *group_dict = [NSMutableDictionary new];
            [group_dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
            [group_dict setObject:[NSString stringWithFormat:@"%@",notification_type_id1] forKey:@"group_id"];
            [group_dict setValue:[NSString stringWithFormat:@"accept"] forKey:@"accept_or_reject_status"];
            NSLog(@"Group Accept Dict %@",group_dict);
            [SHARED_API accept_reject_group_request:group_dict withSuccess:^(NSDictionary *response)
             {
                 NSLog(@"reject Group In notification Response %@",response);
                 if([[response objectForKey:STATUS] isEqualToString:SUCCESS])
                 {
//                      [activeIndicatore stopAnimating];
                     indicaterview.hidden=YES;
                     if ([readstatus isEqualToString:@"0"]) {
                         [self notification_read_unread_status_request];
                     }
                     else{
                         [self getNotifications];
                     }
                 }
                 else if([[response objectForKey:STATUS] isEqualToString:FAIL])
                 {
//                      [activeIndicatore stopAnimating];
                     indicaterview.hidden=YES;
                     [SHARED_HELPER showAlert:ServiceFail2];
                 }
             } onfailure:^(NSError *theError)
             {
                 NSLog(@"Accept Frnd In Error %@",theError);
//                  [activeIndicatore stopAnimating];
                 indicaterview.hidden=YES;
             }];
            
        }
        else if ([notification_about1 isEqualToString:@"ride"]) {
            
            NSMutableDictionary *ride_dict = [NSMutableDictionary new];
            
            [ride_dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
            
            [ride_dict setObject:[NSString stringWithFormat:@"%@",notification_type_id1] forKey:@"ride_id"];
            
            [ride_dict setObject:[NSString stringWithFormat:@""] forKey:@"invited_user_id"];
            
            [ride_dict setValue:[NSString stringWithFormat:@"accept"] forKey:@"accept_or_reject_status"];
            
            NSLog(@"Group Accept Dict %@",ride_dict);
            
            [SHARED_API accept_reject_notification_ride_request:ride_dict withSuccess:^(NSDictionary *response)
             
             {
                 
                 NSLog(@"reject Ride In notification Response %@",response);
                 
                 if([[response objectForKey:STATUS] isEqualToString:SUCCESS])
                     
                 {
//                     [activeIndicatore stopAnimating];
                     indicaterview.hidden=YES;
                     if ([readstatus isEqualToString:@"0"]) {
                         [self notification_read_unread_status_request];
                     }
                     else{
                         [self getNotifications];
                     }
                 }
                 
                 else if([[response objectForKey:STATUS] isEqualToString:FAIL])
                     
                 {
                     [SHARED_HELPER showAlert:ServiceFail2];
//                      [activeIndicatore stopAnimating];
                     indicaterview.hidden=YES;
                 }
                 else if([[response objectForKey:STATUS] isEqualToString:@"ride_started"])
                     
                 {
//                     [activeIndicatore stopAnimating];
                     indicaterview.hidden=YES;
                     UIAlertController *alertview = [UIAlertController alertControllerWithTitle:@"Alert" message:@"You can't join now, ride already started." preferredStyle:UIAlertControllerStyleAlert];
                     
                     [alertview addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
                         
                         if ([readstatus isEqualToString:@"0"]) {
                             [self notification_read_unread_status_request];
                         }
                         else{
                             [self getNotifications];
                         }
                     }]];
                     [self presentViewController:alertview animated:YES completion:nil];
                 }
                 
             } onfailure:^(NSError *theError)
             
             {
//                  [activeIndicatore stopAnimating];
                 indicaterview.hidden=YES;
                 NSLog(@"Accept Frnd In Error %@",theError);
                 
             }];
            
        }
    }

}
-(void)Pending_Ride_Alert:(NSString *)aler_msg{
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:aler_msg preferredStyle:UIAlertControllerStyleAlert];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [self cancle_clicked];
        
    }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
}
-(void)Show_Alert_Pop_Up:(NSString *)aler_msg{
    
    UIAlertController *alertview = [UIAlertController alertControllerWithTitle:@"Alert" message:aler_msg preferredStyle:UIAlertControllerStyleAlert];
    
    [alertview addAction:[UIAlertAction actionWithTitle:@"retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self getNotifications];
       
    }]];
    [self presentViewController:alertview animated:YES completion:nil];
}
-(void)rideGoingAlert:(NSString *)alert_msg
{
    UIAlertController *alertview = [UIAlertController alertControllerWithTitle:@"Ride can't start" message:alert_msg preferredStyle:UIAlertControllerStyleAlert];
    
    [alertview addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
    
        
    }]];
    [self presentViewController:alertview animated:YES completion:nil];
}
#pragma mark - CoreData Check Pendig Rides
- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}
-(NSArray *)get_pending_Rides_From_DB{
    
    NSManagedObjectContext *myContext = [self managedObjectContext];
    NSFetchRequest *req=[NSFetchRequest fetchRequestWithEntityName:Rides_Table];
    NSArray *ride=[myContext executeFetchRequest:req error:nil];
    NSMutableArray *rides_results_array=[[NSMutableArray alloc]init];
    for (Rides *object in ride) {
        if ([object.ride_status isEqualToString:table_status_pending]) {
            [rides_results_array addObject:object];
        }
    }
    return rides_results_array;
}

- (IBAction)onClick_notifications_rideGoingBtn:(id)sender {
    
    self.notifications_scrollLabel.hidden=YES;
    self.notifications_rideGoingBtn.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }

}
@end
