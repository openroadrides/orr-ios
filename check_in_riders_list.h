//
//  check_in_riders_list.h
//  openroadrides
//
//  Created by SrkIosEbiz on 26/07/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface check_in_riders_list : UITableViewCell
@property (strong, nonatomic) IBOutlet UIView *bg_View;
@property (strong, nonatomic) IBOutlet UIImageView *user_Profile_Pic;
- (IBAction)check_box_Click_Action:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *user_Name_label;
@property (strong, nonatomic) IBOutlet UIButton *check_Box_Outlet;
@property (strong, nonatomic) IBOutlet UIImageView *adress_Icon;
@property (strong, nonatomic) IBOutlet UILabel *friend_Adress_Label;

@end
