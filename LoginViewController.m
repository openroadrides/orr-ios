//
//  LoginViewController.m
//  openroadrides
//
//  Created by apple on 01/05/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "LoginViewController.h"
#import "SignupViewController.h"
#import "ForgotpasswordViewController.h"
#import "DashboardViewController.h"

@interface LoginViewController ()<GIDSignInUIDelegate>{
    NSString *name;
    NSString *fullname,*Device_id;
}
@property (nonatomic, strong) NSDictionary *facebookUserResult;
@end

@implementation LoginViewController
-(void)viewWillAppear:(BOOL)animated{
      self.navigationController.navigationBarHidden = YES;
    
}
-(void)viewDidAppear:(BOOL)animated{
    [self.view endEditing:YES];
}
-(void)viewWillDisappear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden=NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [GIDSignIn sharedInstance].uiDelegate = self;
    appDelegate.loginview=self;
    _login_scrl_view.showsVerticalScrollIndicator = NO;
     self.navigationController.navigationBarHidden = YES;
        [self.Email_tf setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.Password_tf setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    // Do any additional setup after loading the view is.
    
    
    indicaterview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    activeIndicatore.backgroundColor=[UIColor clearColor];
    activeIndicatore = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.frame = CGRectMake(0.0, 0.0, 80, 80);
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    [indicaterview addSubview:activeIndicatore];
    [self.view addSubview:indicaterview];
    [indicaterview bringSubviewToFront:self.view];
    [activeIndicatore startAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    indicaterview.hidden=YES;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -100., self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +100., self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    if (textField == _Email_tf)
    {
        [_Password_tf becomeFirstResponder];
        return NO;
    }
      [textField resignFirstResponder];
    return YES;
}

- (IBAction)Login_click:(id)sender
{
    
    [self.view endEditing:YES];
    
    if (![SHARED_HELPER checkIfisInternetAvailable]) {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
    

        if ([self valide_Data] == YES)
        {
//            [self loadingView];
            [self Login_Dict];
        }
    else
    {
        [SHARED_HELPER showAlert:Nonetwork];
    }
}
- (IBAction)Forgot_password_click:(id)sender{
    
    ForgotpasswordViewController *Forgot_cntlr  = [self.storyboard instantiateViewControllerWithIdentifier:@"ForgotpasswordViewController"];
    [self.navigationController pushViewController:Forgot_cntlr animated:YES];
}
- (IBAction)Google_click:(id)sender {
    if (![SHARED_HELPER checkIfisInternetAvailable]) {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
    appDelegate.loginwithstrg=@"google";
    [[GIDSignIn sharedInstance] signIn];

}
- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
//    [myActivityIndicator stopAnimating];
    
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)Facebook_click:(id)sender
{
        _socialtype = @"facebook";
        if (![FBSDKAccessToken currentAccessToken])
        {
            FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
       
            login.loginBehavior = FBSDKLoginBehaviorSystemAccount;
            [login logInWithReadPermissions:@[@"email",@"user_friends",@"public_profile"]fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                if (error) {
                    // Process error
                } else if (result.isCancelled) {
                    // Handle cancellations
                } else
                {
                    // If you ask for multiple permissions at once, you
                    // should check if specific permissions missing
                    if ([result.grantedPermissions containsObject:@"email"])
                    {
                        // Do work
                        [self readCurrentuserProfileAndLogin];
                    }
                }
            }];
        }else
        {
            [self readCurrentuserProfileAndLogin];
        }

    
}
-(void)readCurrentuserProfileAndLogin
{
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{ @"fields":@"id,email,first_name,last_name,gender"}]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         if (!error) {
             NSLog(@"fetched user:%@", result);
             _facebookUserResult = result;
              name = [NSString stringWithFormat:@"%@ %@",[_facebookUserResult valueForKey:@"first_name"],[_facebookUserResult valueForKey:@"last_name"]];
             // Call the API to validate user, first try the Social_login, if it returns fail. Try the social_registration call and navigate to dashboard.
             if ([_facebookUserResult objectForKey:@"email"])
             {
                   [self socialSignIn];
             }
             else
             {
                 [SHARED_HELPER showAlert:EmailNotExists];
             }
         }
     }];

    
}
-(void)googlesociallogin
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    fullname = [NSString stringWithFormat:@"%@ %@",[defaults objectForKey:@"gglsgInefstname"],[defaults objectForKey:@"gglsgInelstname"]];
    
    
    _socialtype = @"google";
    [self socialSignIn];
    
}
-(void)socialSignIn
{
    Device_id = [NSString stringWithFormat:@"%@" ,[Defaults valueForKey:Device_ID]];
    if ([Device_id isEqualToString:@""]||Device_id==nil ||[Device_id isEqualToString:@"(null)"]) {
        Device_id = @"";
    }
    else{
        
    }
    
    
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
    indicaterview.hidden=NO;
    NSMutableDictionary *wsParams = [[NSMutableDictionary alloc]init];
    if([_socialtype isEqualToString:@"facebook"])
    {
        [wsParams setObject:[_facebookUserResult objectForKey:@"email"] forKey:@"email"];
        [wsParams setObject:_socialtype forKey:@"social_reg_type"];
        [wsParams setObject:[_facebookUserResult objectForKey:@"id"] forKey:@"id"];
        [wsParams setObject:@"ios" forKey:@"device_type"];
        [wsParams setObject:Device_id forKey:@"device_registration_id"];

        
    }
    else
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [wsParams setObject:[defaults objectForKey:@"gglsgInemail"] forKey:@"email"];
        [wsParams setObject:[defaults objectForKey:@"gglsgInId"] forKey:@"id"];
        [wsParams setObject:_socialtype forKey:@"social_reg_type"];
        [wsParams setObject:@"ios" forKey:@"device_type"];
        [wsParams setObject:Device_id forKey:@"device_registration_id"];

        
    }
    [SHARED_API social_loginRequestsWithParams:wsParams withSuccess:^(NSDictionary *response)
    {
        dispatch_async(dispatch_get_main_queue(), ^
        {
            NSLog(@"%@",response);
            if([[response objectForKey:STATUS] isEqualToString:SOCIALFAIL])
            {
                [self social_registration];
            }
            else if ([[response objectForKey:STATUS] isEqualToString:SOCIALLOGISUCCESS])
            {
                NSLog(@"userInfo%@",[response objectForKey:@"user_id"]);
                
                NSLog(@"Social Registration Response %@",response);
               
                _currentUserDetails = (UserDetails *)[response objectForKey:@"user_id"];
                SHARED_HELPER.userDetails = _currentUserDetails;
                //--------------------------------------------
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:[response objectForKey:@"user_id"] forKey:User_ID];
                NSLog(@"USer ID %@",[defaults valueForKey:@"User_ID"]);
                [defaults setObject:[response objectForKey:@"name"] forKey:@"UserName"];
                [defaults setObject:[response objectForKey:@"email"] forKey:@"EMAIL"];
                [defaults synchronize];
                
                
                
                //Navigate to Dashboard/
//                DashboardViewController *dashboard = [self.storyboard instantiateViewControllerWithIdentifier:@"DashboardViewController"];
//                [self.navigationController pushViewController:dashboard animated:YES];
                
                 //Navigate to HomeDashboardView/
                HomeDashboardView *cntrl = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeDashboardView"];
               [self.navigationController pushViewController:cntrl animated:YES];
                
              indicaterview.hidden=YES;
            }
        });
        
    } onfailure:^(NSError *theError) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
             indicaterview.hidden=YES;
             [SHARED_HELPER showAlert:ServiceFail];
            
            
        });
        
    }];
}

-(void)social_registration
{
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
    indicaterview.hidden=NO;
    NSMutableDictionary *wsParams = [[NSMutableDictionary alloc]init];
    if([_socialtype isEqualToString:@"facebook"])
    {
        [wsParams setObject:name forKey:@"name"];
        [wsParams setObject:[_facebookUserResult objectForKey:@"email"] forKey:@"email"];
        [wsParams setObject:[_facebookUserResult objectForKey:@"id"] forKey:@"id"];
        [wsParams setObject:@"" forKey:@"gender"];
        [wsParams setObject:@"" forKey:@"age"];
        [wsParams setObject:_socialtype forKey:@"social_reg_type"];
        [wsParams setObject:@"" forKey:@"phone_number"];
        [wsParams setObject:@"" forKey:@"profile_image"];
        [wsParams setObject:@"ios" forKey:@"device_type"];
        [wsParams setObject:Device_id forKey:@"device_registration_id"];
    }
    else
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
       
        [wsParams setObject:fullname forKey:@"name"];
        [wsParams setObject:[defaults objectForKey:@"gglsgInemail"] forKey:@"email"];
        [wsParams setObject:[defaults objectForKey:@"gglsgInId"] forKey:@"id"];
        [wsParams setObject:@"" forKey:@"gender"];
        [wsParams setObject:@"" forKey:@"age"];
        [wsParams setObject:_socialtype forKey:@"social_reg_type"];
        [wsParams setObject:@"" forKey:@"phone_number"];
        [wsParams setObject:@"" forKey:@"profile_image"];
        [wsParams setObject:@"ios" forKey:@"device_type"];
        [wsParams setObject:Device_id forKey:@"device_registration_id"];


    
    }
    
    [SHARED_API social_registrationRequestsWithParams:wsParams withSuccess:^(NSDictionary *response)
     {
         dispatch_async(dispatch_get_main_queue(), ^
                        {
                             NSLog(@"%@",response);
                             _userid = [response objectForKey:@"user_id"];
                            if([[response objectForKey:STATUS] isEqualToString:USER_EXIST])
                            {
                                [Defaults setObject:[wsParams objectForKey:@"name"] forKey:@"UserName"];
                                [self showpopup:socialupdate];
                            }
                            else if ([[response objectForKey:STATUS] isEqualToString:SOCILAREGSUCCESS])
                            {
                                
                                _currentUserDetails = (UserDetails *)[response objectForKey:@"user_id"];
                                SHARED_HELPER.userDetails = _currentUserDetails;
                                //--------------------------------------------
                                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//                                NSData *uData = [NSKeyedArchiver archivedDataWithRootObject:_currentUserDetails];
                                [defaults setObject:_userid forKey:User_ID];
                                [defaults setObject:[response objectForKey:@"user_id"] forKey:User_ID];
                                [defaults setObject:[wsParams objectForKey:@"name"] forKey:@"UserName"];
                                [defaults setObject:[wsParams objectForKey:@"email"] forKey:@"EMAIL"];
                                [defaults synchronize];
                                //Navigate to Dashboard/
//                                DashboardViewController *dashboard = [self.storyboard instantiateViewControllerWithIdentifier:@"DashboardViewController"];
//                                [self.navigationController pushViewController:dashboard animated:YES];
                                
                                
                                //Navigate to HomeDashboardView/
                               HomeDashboardView *cntrl = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeDashboardView"];
                               [self.navigationController pushViewController:cntrl animated:YES];
                                 indicaterview.hidden=YES;
                            }
                            else{
                                indicaterview.hidden=YES;
                                 [SHARED_HELPER showAlert:ServiceFail];
                            }
                            
                        });
         
     } onfailure:^(NSError *theError) {
         
         dispatch_async(dispatch_get_main_queue(), ^{
             indicaterview.hidden=YES;
              [SHARED_HELPER showAlert:ServiceFail];
             
         });
         
     }];

    
}
-(void)SocialUpdate
{
    
    if (![SHARED_HELPER checkIfisInternetAvailable]) {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
//    [indicator startAnimating];
    indicaterview.hidden=NO;
    NSMutableDictionary *social_dict = [[NSMutableDictionary alloc]init];
    if([_socialtype isEqualToString:@"facebook"])
    {
   
        
        [social_dict setObject:_userid forKey:@"user_id"];
        [social_dict setObject:REG_TYPE forKey:@"reg_type"];
        [social_dict setObject:[_facebookUserResult objectForKey:@"email"] forKey:@"email"];
        [social_dict setObject:_socialtype forKey:@"social_reg_type"];
        [social_dict setObject:[_facebookUserResult objectForKey:@"id"] forKey:@"social_id"];
         NSLog(@"%@",social_dict);
        
    }
    else{
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *socialID = [defaults objectForKey:@"gglsgInId"];
        [social_dict setObject:_socialtype forKey:@"social_reg_type"];
        [social_dict setObject:_userid forKey:@"user_id"];
        [social_dict setObject:REG_TYPE forKey:@"reg_type"];
        [social_dict setObject:socialID forKey:@"social_id"];
        [social_dict setObject:[defaults objectForKey:@"gglsgInemail"] forKey:@"email"];
        NSLog(@"%@",social_dict);

    }
        
        
        [SHARED_API socialUpdateRequestsWithParams:social_dict withSuccess:^(NSDictionary *response) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if(![[response objectForKey:STATUS] isEqualToString:FAIL])
                {
//                    [indicator stopAnimating];
                    
                    //--------------------------------------------
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    [defaults setObject:[response objectForKey:@"user_id"] forKey:_userid];

                    [defaults synchronize];
                    
                    //Navigate to Dashboard/
//                    DashboardViewController *dashboard = [self.storyboard instantiateViewControllerWithIdentifier:@"DashboardViewController"];
//                    [self.navigationController pushViewController:dashboard animated:YES];
                    
                     //Navigate to HomeDashboardView/
                    HomeDashboardView *cntrl = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeDashboardView"];
                   [self.navigationController pushViewController:cntrl animated:YES];
                   indicaterview.hidden=YES;
//                    [self successview];
                }
                else{
                    
                    [SHARED_HELPER showAlert:ServiceFail];
                    indicaterview.hidden=YES;
                    
                }
                
            });
            
        } onfailure:^(NSError *theError) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"mytheError%@",theError);
//                [indicator stopAnimating];
                indicaterview.hidden=YES;
                [SHARED_HELPER showAlert:ServerConnection];
            });
        }];
}


- (IBAction)Createone_click:(id)sender{
    SignupViewController *cntlr  = [self.storyboard instantiateViewControllerWithIdentifier:@"SignupViewController"];
    [self.navigationController pushViewController:cntlr animated:YES];
}
-(BOOL)NSStringIsValidEmail:(NSString *)checkString{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
-(void)Login_Dict
{
    
    
//    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//    activeIndicatore.center = self.view.center;
//    activeIndicatore.color = APP_YELLOW_COLOR;
//    activeIndicatore.hidesWhenStopped = TRUE;
//    [activeIndicatore startAnimating];
//    [self.view addSubview:activeIndicatore];
    indicaterview.hidden=NO;

    Device_id = [NSString stringWithFormat:@"%@" ,[Defaults valueForKey:Device_ID]];
    if ([Device_id isEqualToString:@""]||Device_id==nil ||[Device_id isEqualToString:@"(null)"]) {
        Device_id = @"";
    }
    else{
        
    }
   
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:[_Email_tf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"email"];
    [dict setObject:[_Password_tf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"password"];
    
    [dict setObject:@"app" forKey:@"type"];
    [dict setObject:@"ios" forKey:@"device_type"];
    
    [dict setObject:Device_id forKey:@"device_registration_id"];

    NSLog(@"Sending parameter to server for Normal login %@",dict);
    
    [SHARED_API signInRequestsWithParams:dict withSuccess:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if([[response objectForKey:STATUS] isEqualToString:SUCCESS])
            {
//                [indicator stopAnimating];
                _userid=[response objectForKey:@"user_id"];
               

                NSLog(@"Login response %@",response);
//                NSLog(@"userInfo%@",[response objectForKey:@"user_id"]);
                _currentUserDetails = (UserDetails *)[response objectForKey:@"user_id"];
                SHARED_HELPER.userDetails = _currentUserDetails;
                //--------------------------------------------
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:[response objectForKey:@"user_id"] forKey:User_ID];
                [defaults setObject:[response objectForKey:@"name"] forKey:@"UserName"];
                [defaults setObject:[response objectForKey:@"email"] forKey:@"EMAIL"];
                NSLog(@"USer ID %@",[defaults valueForKey:@"User_ID"]);

//                NSData *uData = [NSKeyedArchiver archivedDataWithRootObject:_currentUserDetails];
              
                [defaults synchronize];
                
//                if ([activeIndicatore isAnimating]) {
//                    [activeIndicatore stopAnimating];
//                    [activeIndicatore removeFromSuperview];
//                }
              indicaterview.hidden=YES;
                //Navigate to Dashboard/
//                DashboardViewController *dashboard = [self.storyboard instantiateViewControllerWithIdentifier:@"DashboardViewController"];
//                [self.navigationController pushViewController:dashboard animated:YES];
                
                
                 //Navigate to HomeDashboardView/
                HomeDashboardView *cntrl = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeDashboardView"];
               
               [self.navigationController pushViewController:cntrl animated:YES];
                
            }
            else
            {

                if ([[response objectForKey:STATUS] isEqualToString:FAIL])
                {

                    indicaterview.hidden=YES;
                    [SHARED_HELPER showAlert:Loginfailed];
                }
            }
        });
        
    } onfailure:^(NSError *theError) {
        
        dispatch_async(dispatch_get_main_queue(), ^{

            indicaterview.hidden=YES;
            [SHARED_HELPER showAlert:ServerConnection];
        });
        
    }];

}



-(BOOL)valide_Data
{
    NSString *email_str = [_Email_tf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *pwd_str=_Password_tf.text;
    
    if  ( [email_str length]==0)
        
        
    {
         [SHARED_HELPER showAlert:EmailText];
        
        return NO;
    }
    else if ([SHARED_HELPER checkEmailValidations:email_str]== NO)
    {
        [SHARED_HELPER showAlert:ValidEmailText];
        return NO;
    }
    else if ([pwd_str length] == 0)
    {
        [SHARED_HELPER showAlert:PasswordText];
        return NO;
    }
    return YES;
}
-(void)showpopup:(NSString *)message
{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:nil
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"CONNECT"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [self SocialUpdate];
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"CANCEL"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}


@end
