//
//  EditProfileViewController.m
//  openroadrides
//
//  Created by SrkIosEbiz on 12/07/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "EditProfileViewController.h"

@interface EditProfileViewController ()

@end

@implementation EditProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
     appDelegate.user_Profile_edited=@"";
    self.title = @"EDIT PROFILE";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    
    self.editProfile_scrollLabel.hidden=YES;
    self.editProfile_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.editProfile_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.editProfile_scrollLabel.textColor = [UIColor blackColor];
    self.editProfile_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.editProfile_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.editProfile_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.editProfile_scrollLabel.scrollSpeed = 60; // pixels per second
    self.editProfile_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.editProfile_scrollLabel.fadeLength = 0.f;
    self.editProfile_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.editProfile_scrollLabel observeApplicationNotifications];
    
    self.editProfile_rideGoingBtn.hidden=YES;
    if (appDelegate.ridedashboard_home) {
        self.editProfile_scrollLabel.hidden=NO;
        self.editProfile_rideGoingBtn.hidden=NO;
    }
    
    [self.user_Name setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.gmail_label setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.user_mobile setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.adress_label setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.state setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.city_tf setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.zip_Code setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.dob_Tf setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    
//    UIBarButtonItem *Save_Action=[[UIBarButtonItem alloc]initWithImage:
//                              [[UIImage imageNamed:@"SAVE"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
//                                                             style:UIBarButtonItemStylePlain target:self action:@selector(Save_Action)];
//    self.navigationItem.rightBarButtonItem=Save_Action;
    
    UIButton *saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [saveBtn addTarget:self action:@selector(Save_Action) forControlEvents:UIControlEventTouchUpInside];
    saveBtn.frame = CGRectMake(0, 0, 30, 30);
    [saveBtn setBackgroundImage:[UIImage imageNamed:@"SAVE"] forState:UIControlStateNormal];
    UIBarButtonItem * edit_Profile= [[UIBarButtonItem alloc] initWithCustomView:saveBtn];
    self.navigationItem.rightBarButtonItems=[NSArray arrayWithObjects:edit_Profile, nil];

    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [leftBtn addTarget:self action:@selector(back_Action) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.frame = CGRectMake(0, 0, 25, 25);
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"back_Image"] forState:UIControlStateNormal];
    UIBarButtonItem *leftBackButton=[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:leftBackButton, nil];
    
    
    NSURL*url=[NSURL URLWithString:_user_image_url];
    [self.User_images sd_setImageWithURL:url
                              placeholderImage:[UIImage imageNamed:@"signup_logo_icon"]
                                       options:SDWebImageRefreshCached];
    
    
    self.User_images.layer.cornerRadius=self.User_images.frame.size.width/2;
    self.User_images.clipsToBounds=YES;
    
    
    _Gender_view.layer.borderWidth = 2.0;
    _Gender_view.layer.borderColor = [UIColor cyanColor].CGColor;
    
    
    //Indication
    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    activeIndicatore.hidesWhenStopped = TRUE;
    [self.view addSubview:activeIndicatore];
    // Do any additional setup after loading the view.
    
    
    if ([_gender_Str isEqualToString:@""]||[_gender_Str isEqualToString:@"Male"]) {
       genderString=@"Male";
    }
    else{
        genderString=@"Female";
        _Male_outlet.backgroundColor=[UIColor clearColor];
        _Female_outlet.backgroundColor=[UIColor cyanColor];
        _Mailimageview.image=[UIImage imageNamed:@"signup_white_male_icon"];
        _Femailimageview.image=[UIImage imageNamed:@"signup_black_female_icon"];
        [_Male_outlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_Female_outlet setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    
     self.user_mobile.text=[_user_Data valueForKey:@"user_number_str"];
     self.user_Name.text=[_user_Data valueForKey:@"User_Name_Str"];
     self.adress_label.text=[_user_Data valueForKey:@"user_adress"];
     self.gmail_label.text=[_user_Data valueForKey:@"user_Mail"];
     self.city_tf.text=[_user_Data valueForKey:@"city_str"];
     self.state.text=[_user_Data valueForKey:@"state_str"];
    NSString *dob_str=[_user_Data valueForKey:@"dob_Str"];
    if (![dob_str isEqualToString:@""]) {
        NSDateFormatter *dateformater = [[NSDateFormatter alloc ]init];
        [dateformater setDateFormat:@"YYYY-MM-dd"];
        NSDate *dob_date=[dateformater dateFromString:dob_str];
        [dateformater setDateFormat:@"MM/dd/YYYY"];
        NSString *str_frm_date=[dateformater stringFromDate:dob_date];
        self.dob_Tf.text=str_frm_date;
    }
     self.zip_Code.text=[_user_Data valueForKey:@"zipcode_Str"];
    
     self.gmail_label.userInteractionEnabled=NO;
     
    
    stateArray = [[NSMutableArray alloc]init];
    dataArray = [[NSMutableArray alloc]init];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"States" ofType:@"plist"];
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
    stateArray = [dict objectForKey:@"States"];
    mapping = [dict objectForKey:@"Mapping"];
    [self creatingPickerView];
    [self setdate];
    
    
    
    
    
    UIToolbar* numberToolbar1 = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar1.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar1.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                             [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                             [[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStyleDone target:self action:@selector(Mobile_Next)]];
    [numberToolbar1 sizeToFit];
    _user_mobile.inputAccessoryView = numberToolbar1;
    
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(cancelNumberPad)]];
    [numberToolbar sizeToFit];
    _zip_Code.inputAccessoryView = numberToolbar;
    
    
    
    
 UITapGestureRecognizer *tap_On_View=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap_On_View)];
    [self.view addGestureRecognizer:tap_On_View];
    
}
-(void)tap_On_View{
    [self.view endEditing:YES];
}
-(void)cancelNumberPad{
    [self.view endEditing:YES];
}

-(void)Mobile_Next{
    [_dob_Tf becomeFirstResponder];
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if (textField==_user_Name) {
        [_user_mobile becomeFirstResponder];
    }
    else if (textField==_user_mobile)
    {
       [_dob_Tf becomeFirstResponder];
    }
    else if (textField==_dob_Tf)
    {
        [self.view endEditing:YES];
    }

    else if (textField==_adress_label)
    {
        [textField resignFirstResponder];
        [self State_Action:0];
        return YES;
    }
    else if (textField==_zip_Code)
    {
       [textField resignFirstResponder];
    }

    
    [textField resignFirstResponder];
    return YES;
}

-(void)Save_Action{
    [self.view endEditing:YES];
    _user_Name.text=[_user_Name.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

    if (![SHARED_HELPER checkIfisInternetAvailable]) {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
    if(_user_Name.text.length == 0 )
    {
        [SHARED_HELPER showAlert:UserNameText];
    }
    
    else if (_user_mobile.text.length !=12 && _user_mobile.text.length !=0)
    {
        [SHARED_HELPER showAlert:MobileText];
    }
   else
   {
    [activeIndicatore startAnimating];
    
    NSMutableDictionary *wsParams = [[NSMutableDictionary alloc]init];
    
     NSString *user_id=[Defaults valueForKey:User_ID];
    [wsParams setObject:user_id forKey:@"user_id"];
    [wsParams setObject:self.user_Name.text forKey:@"name"];
    [wsParams setObject:self.user_mobile.text forKey:@"phone_number"];
    [wsParams setObject:genderString forKey:@"gender"];
    [wsParams setObject:_zip_Code.text forKey:@"zipcode"];
    [wsParams setObject:_dob_Tf.text forKey:@"date_of_birth"];
    [wsParams setObject:_adress_label.text forKey:@"address"];
    [wsParams setObject:_city_tf.text forKey:@"city"];
    [wsParams setObject:_state.text forKey:@"state"];
       
    if (base64String) {
        [wsParams setObject:base64String forKey:@"profile_image"];
    }
    else{
        [wsParams setObject:@"" forKey:@"profile_image"];
    }
    [wsParams setObject:_user_image_url forKey:@"existing_profile_image"];

   [SHARED_API Update_User_Profile:wsParams withSuccess:^(NSDictionary *response) {
         [activeIndicatore stopAnimating];
       if ([[response valueForKey:STATUS] isEqualToString:SUCCESS]) {
           [SHARED_HELPER showAlert:@"Your profile is updated successfully."];
           [self.navigationController popViewControllerAnimated:YES];
           appDelegate.user_Profile_edited=@"YES";
       }
       else{
            [SHARED_HELPER showAlert:@"Failed to update your profile. Please try again."];
       }
   } onfailure:^(NSError *theError) {
       
       [SHARED_HELPER showAlert:ServiceFail];
        [activeIndicatore stopAnimating];
   }];
   }
}

-(void)back_Action{
    [self.navigationController popViewControllerAnimated:YES];
}
-(BOOL)NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)Profile_image_Action:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped do nothing.
        
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Take photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Choose photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }]];
    [self presentViewController:alert animated:YES completion:nil];
    
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    cameraImage = info[UIImagePickerControllerEditedImage];
    self.User_images.image=cameraImage;
    [picker dismissViewControllerAnimated:YES completion:NULL];
    CGFloat compression = 0.9f;
    CGFloat maxCompression = 0.1f;
    int maxFileSize = 250*1024;
    profileImgData = UIImageJPEGRepresentation(cameraImage,compression);
    while ([profileImgData length] > maxFileSize && compression > maxCompression)
    {
        compression -= 0.1;
        profileImgData = UIImageJPEGRepresentation(cameraImage,compression);
    }
    base64String = [profileImgData base64EncodedStringWithOptions:0];
    //        NSLog(@"base64String is %@", base64String);
    
}

- (IBAction)Gender_action:(UIButton *)sender
{
    NSInteger tid = sender.tag;
    if (tid==0)
    {
        genderString=@"Male";
        _Male_outlet.backgroundColor=[UIColor cyanColor];
        _Female_outlet.backgroundColor=[UIColor clearColor];
        _Mailimageview.image=[UIImage imageNamed:@"signup_black_male_icon"];
        _Femailimageview.image=[UIImage imageNamed:@"signup_white_female"];
        [_Male_outlet setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_Female_outlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    else
    {
        
        genderString=@"Female";
        _Male_outlet.backgroundColor=[UIColor clearColor];
        _Female_outlet.backgroundColor=[UIColor cyanColor];
        _Mailimageview.image=[UIImage imageNamed:@"signup_white_male_icon"];
        _Femailimageview.image=[UIImage imageNamed:@"signup_black_female_icon"];
        [_Male_outlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_Female_outlet setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
    }
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
   
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:.3];
        [UIView setAnimationBeginsFromCurrentState:TRUE];
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +160., self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    
 
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if ([pickerclick isEqualToString:@"state"]||[pickerclick isEqualToString:@"city"]) {
        [self hide_Picker_View];
    }
    
    
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:.3];
        [UIView setAnimationBeginsFromCurrentState:TRUE];
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -160., self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
  }

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == _user_mobile)
    {
        NSCharacterSet *numSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789-"];
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSInteger charCount = [newString length];
        
        if (charCount>12) {
            return NO;
        }
        
       //new
        NSString *stringWithoutSpaces = [newString
                                         stringByReplacingOccurrencesOfString:@"-" withString:@""];
        NSInteger newcharCount = [stringWithoutSpaces length];
        if (newcharCount>10) {
             stringWithoutSpaces = [stringWithoutSpaces substringToIndex:[stringWithoutSpaces length]-1];
            NSMutableString *mu = [NSMutableString stringWithString:stringWithoutSpaces];
            [mu insertString:@"-" atIndex:3];
            [mu insertString:@"-" atIndex:7];
            textField.text = mu;
            return NO;
            
        }
        else if (newcharCount==10)
        {
            NSMutableString *mu = [NSMutableString stringWithString:stringWithoutSpaces];
            [mu insertString:@"-" atIndex:3];
            [mu insertString:@"-" atIndex:7];
            textField.text = mu;
            return NO;
        }
     //
        
        
        
        
        
        if (charCount == 3 || charCount == 7) {
            if ([string isEqualToString:@""]){
                return YES;
            }else{
                newString = [newString stringByAppendingString:@"-"];
            }
        }
        
        if (charCount == 4 || charCount == 8) {
            if (![string isEqualToString:@"-"]){
                newString = [newString substringToIndex:[newString length]-1];
                newString = [newString stringByAppendingString:@"-"];
            }
        }
        
        if ([newString rangeOfCharacterFromSet:[numSet invertedSet]].location != NSNotFound
            || [string rangeOfString:@"-"].location != NSNotFound
            || charCount > 12) {
            return NO;
        }
        
        
        
        textField.text = newString;
        return NO;
    }
   
    return YES;
}

- (IBAction)State_Action:(id)sender {
    
    selected_state_row=-1;
    _bg_Scrole_View.scrollEnabled=NO;
    _bg_Scrole_View.userInteractionEnabled=NO;
    
    if (SCREEN_HEIGHT>568) {
   [_bg_Scrole_View setContentOffset:CGPointMake(0,pickerBaseView.bounds.size.height-45) animated:YES];
    }
    else
    [_bg_Scrole_View setContentOffset:CGPointMake(0,_bg_Scrole_View.contentSize.height-240) animated:YES];
    
    
    
    [self presentPickerView];
    pickerclick=@"state";
    dataArray = stateArray;
    int indexRow=0;
    if (_state.text.length !=0) {
        for (int i = 0; i<dataArray.count; i++ )
        {
            if ([_state.text isEqualToString:[dataArray objectAtIndex:i]])
                indexRow = i;
        }
    }
    if (indexRow==0) {
        
    }
    else
        selected_state_row=indexRow;
    
    
    [businessTypePickerView reloadAllComponents];
    [businessTypePickerView selectRow:indexRow inComponent:0 animated:NO];
}
- (void)presentPickerView
{
    self.tabBarController.tabBar.hidden = true;
    [UIView animateWithDuration:0.3 animations:^{
        pickerBaseView.frame = CGRectMake(0, self.view.bounds.size.height - pickerBaseView.bounds.size.height, pickerBaseView.bounds.size.width, pickerBaseView.bounds.size.height);
    }];
}
- (void)creatingPickerView
{
    pickerBaseView = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 250)];
    pickerBaseView.backgroundColor = [UIColor clearColor];
    //pickerBaseView.layer.backgroundColor =[UIColor colorWithRed:(35/255.0) green:(30/255.0) blue:(34/255.0) alpha:1].CGColor;
    UIView *doneview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, pickerBaseView.frame.size.width, 50)];
    doneview.backgroundColor = [UIColor colorWithRed:18/255.0 green:17/255.0 blue:14/255.0 alpha:1];
    doneButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [doneButton addTarget:self
                   action:@selector(pickerDoneButtonAction:)
         forControlEvents:UIControlEventTouchUpInside];
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    // doneButton.tintColor = [UIColor colorWithRed:18/255.0 green:17/255.0 blue:14/255.0 alpha:1];
    doneButton.titleLabel.font  = [UIFont fontWithName:@"arial" size:15];
    doneButton.frame = CGRectMake(self.view.frame.size.width-120, 5, 160.0, 40.0);
    [doneview addSubview:doneButton];
    
    cancel_Button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [cancel_Button addTarget:self
                      action:@selector(hide_Picker_View)
            forControlEvents:UIControlEventTouchUpInside];
    [cancel_Button setTitle:@"Cancel" forState:UIControlStateNormal];
    //cancel_Button.tintColor = [UIColor colorWithRed:18/255.0 green:17/255.0 blue:14/255.0 alpha:1];
    cancel_Button.titleLabel.font  = [UIFont fontWithName:@"arial" size:15];
    cancel_Button.frame = CGRectMake(5, 5, 80, 40.0);
    [doneview addSubview:cancel_Button];
    
    businessTypePickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(0,50, self.view.frame.size.width, 200)];
    businessTypePickerView.delegate = self;
    businessTypePickerView.dataSource = self;
    businessTypePickerView.backgroundColor = [UIColor colorWithRed:0.8784 green:0.8784 blue:0.8784 alpha:1.0f];
    Spickerchng=NO;
    Cpickerchng=NO;
    [pickerBaseView addSubview: doneview];
    [pickerBaseView addSubview:businessTypePickerView];
    [self.view addSubview:pickerBaseView];
}
-(void)hide_Picker_View{
    _bg_Scrole_View.scrollEnabled=YES;
    _bg_Scrole_View.userInteractionEnabled=YES;
    [self dismissPickerView];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent: (NSInteger)component
{
    return [dataArray count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row   forComponent:(NSInteger)component
{
    return [dataArray objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row   inComponent:(NSInteger)component
{
    if ([pickerclick isEqualToString:@"state"])
    {
        Spickerchng=YES;
        selected_state_row=row;
    }
  
    if ([pickerclick isEqualToString:@"city"])
    {
        Cpickerchng=YES;
       selected_state_row=row;
    }
    else if([pickerclick isEqualToString:@""])
    {
    }
}

- (IBAction)pickerDoneButtonAction:(id)sender
{
    _bg_Scrole_View.scrollEnabled=YES;
    _bg_Scrole_View.userInteractionEnabled=YES;
    if ([pickerclick isEqualToString:@"city"])
    {
        if (selected_state_row==-1) {
            _city_tf.text=[dataArray objectAtIndex:0];
        }
        else{
            _city_tf.text=[dataArray objectAtIndex:selected_state_row];
        }
        pickerclick=@"";
    }
    else
    {
        if (selected_state_row==-1) {
            _state.text=[dataArray objectAtIndex:0];
        }
        else{
            _state.text=[dataArray objectAtIndex:selected_state_row];
            _city_tf.text=@"";
        }
        pickerclick=@"";
        
    }
    [self dismissPickerView];
}

- (void)dismissPickerView
{
    [UIView animateWithDuration: 0.3 animations:^{ pickerBaseView.frame = CGRectMake(0, self.view.bounds.size.height, pickerBaseView.bounds.size.width, pickerBaseView.bounds.size.height);}
                     completion:^(BOOL finished) {
                         self.tabBarController.tabBar.hidden = false;
                     }];
    
    if (SCREEN_HEIGHT>_bg_Scrole_View.contentSize.height) {
        [_bg_Scrole_View setContentOffset:CGPointMake(0,0) animated:YES];
    }
    else{
        [_bg_Scrole_View setContentOffset:CGPointMake(0,100) animated:YES];
    }
    
}



- (IBAction)city_Action:(id)sender {
    if ([_state.text isEqualToString:@""])
    {
        [SHARED_HELPER showAlert:SelectState];
    }
    else
    {
        selected_state_row=-1;
        
        if (SCREEN_HEIGHT>568) {
        [_bg_Scrole_View setContentOffset:CGPointMake(0, pickerBaseView.bounds.size.height+5) animated:YES];
        }
        else
        [_bg_Scrole_View setContentOffset:CGPointMake(0,_bg_Scrole_View.contentSize.height-260) animated:YES];
        
       
        _bg_Scrole_View.scrollEnabled=NO;
        _bg_Scrole_View.userInteractionEnabled=NO;
        [self presentPickerView];
        pickerclick=@"city";
        dataArray = [mapping objectForKey:_state.text];
        int indexRow=0;
        if (_city_tf.text.length !=0) {
            for (int i = 0; i<dataArray.count; i++ )
            {
                if ([_city_tf.text isEqualToString:[dataArray objectAtIndex:i]])
                    indexRow = i;
            }
        }
        if (indexRow==0) {
            
        }
        else
            selected_state_row=indexRow;
        
        [businessTypePickerView reloadAllComponents];
        [businessTypePickerView selectRow:indexRow inComponent:0 animated:NO];
    }
}
-(void)setdate{
    
    date = [[UIDatePicker alloc]init];
    date.datePickerMode = UIDatePickerModeDate;
    [date setMaximumDate:[NSDate date]];
    [_dob_Tf setInputView:date];
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(showDate)]];
    [numberToolbar sizeToFit];
    _dob_Tf.inputAccessoryView = numberToolbar;
    
}
-(void)showDate{
    
    NSDateFormatter *dateformater = [[NSDateFormatter alloc ]init];
    [dateformater setDateFormat:@"MM/dd/YYYY"];
    _dob_Tf.text = [NSString stringWithFormat:@"%@",[dateformater stringFromDate:date.date]];
    [_dob_Tf resignFirstResponder];
    [_adress_label becomeFirstResponder];
}

- (IBAction)onClick_editProfile_rideGoingBtn:(id)sender {
    
    self.editProfile_scrollLabel.hidden=YES;
    self.editProfile_rideGoingBtn.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }

}
@end
