//
//  RideDetailTableViewCell.h
//  openroadrides
//
//  Created by apple on 21/06/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RideDetailTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *content_view;
@property (weak, nonatomic) IBOutlet UIImageView *profile_img;
@property (weak, nonatomic) IBOutlet UILabel *rider_name_lbl;
@property (weak, nonatomic) IBOutlet UILabel *location_lbl;
@property (weak, nonatomic) IBOutlet UIImageView *location_imageView;

@end
