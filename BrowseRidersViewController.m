//
//  BrowseRidersViewController.m
//  openroadrides
//
//  Created by Jyothsna on 7/24/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "BrowseRidersViewController.h"
#import "Constants.h"
#import "APIHelper.h"
#import "AppHelperClass.h"
#import "SMCalloutView.h"
#import <GooglePlaces/GooglePlaces.h>
#import "DashboardViewController.h"
#import "BrowsRiderCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <Google-Maps-iOS-Utils/GMUMarkerClustering.h>
#import "MPCoachMarks.h"


@interface POIItem : NSObject<GMUClusterItem>

@property(nonatomic, readonly) CLLocationCoordinate2D position;
@property(nonatomic, readonly) NSString *name;

- (instancetype)initWithPosition:(CLLocationCoordinate2D)position name:(NSString *)name;

@end
@implementation POIItem

- (instancetype)initWithPosition:(CLLocationCoordinate2D)position name:(NSString *)name {
    if ((self = [super init])) {
        _position = position;
        _name = [name copy];
    }
    return self;
}

@end

@import GoogleMaps;

static const CGFloat CalloutYOffset = 50.0f;

@interface BrowseRidersViewController () <GMSMapViewDelegate,CLLocationManagerDelegate,GMSAutocompleteViewControllerDelegate,UITableViewDelegate,UITableViewDataSource,GMUClusterManagerDelegate, GMUClusterRendererDelegate>
{
    CLLocationManager *map_locationManager;
    float map_currentLatitude,map_currentLongitude;
    int mapZoom;
    GMUClusterManager *_clusterManager;
}
@property (strong, nonatomic) GMSMapView *mapView;
@property (strong, nonatomic) SMCalloutView *calloutView;
@property (strong, nonatomic) UIView *emptyCalloutView;
@end

@implementation BrowseRidersViewController {
    GMSMarker *_marker;
    UIImageView *_markerView;
    CLLocationCoordinate2D northEast,southWest;
    UIImage *bottomImage;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"FIND RIDERS";
     _map_list_image_view.image=[UIImage imageNamed:@"rider_list_icon"];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    
    
    
    UIBarButtonItem *menuButton=[[UIBarButtonItem alloc]initWithImage:
                                 [[UIImage imageNamed:@"back_Image"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
                                                                style:UIBarButtonItemStylePlain target:self action:@selector(menuclicked)];
    
    self.navigationItem.leftBarButtonItem = menuButton;

//    UIBarButtonItem *add_btn=[[UIBarButtonItem alloc]initWithImage:
//                                 [[UIImage imageNamed:@"SEARCH"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
//                                                                style:UIBarButtonItemStylePlain target:self action:@selector(search)];
//    self.navigationItem.rightBarButtonItem=add_btn;
    
    self.browseRiders_scrollLabel.hidden=YES;
    self.browseRiders_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.browseRiders_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.browseRiders_scrollLabel.textColor = [UIColor blackColor];
    self.browseRiders_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.browseRiders_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.browseRiders_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.browseRiders_scrollLabel.scrollSpeed = 60; // pixels per second
    self.browseRiders_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.browseRiders_scrollLabel.fadeLength = 0.f;
    self.browseRiders_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.browseRiders_scrollLabel observeApplicationNotifications];
    
    self.browseRiders_rideGoingBtn.hidden=YES;
    if (appDelegate.ridedashboard_home) {
        
        self.browseRiders_scrollLabel.hidden=NO;
        self.browseRiders_rideGoingBtn.hidden=NO;
    }
    UIButton *searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [searchBtn addTarget:self action:@selector(search) forControlEvents:UIControlEventTouchUpInside];
    searchBtn.frame = CGRectMake(0, 0, 30, 30);
    [searchBtn setBackgroundImage:[UIImage imageNamed:@"SEARCH"] forState:UIControlStateNormal];
    UIBarButtonItem * search_Riders= [[UIBarButtonItem alloc] initWithCustomView:searchBtn];
    self.navigationItem.rightBarButtonItems=[NSArray arrayWithObjects:search_Riders, nil];

    searchstatus=NO;
    screenstrng=@"LIST";
    [self map_loadingMethod];
    [self check_Location_On_Or_Off];
    
     self.emptyCalloutView = [[UIView alloc] initWithFrame:CGRectZero];
    [self Calloutview];
    
    _list_tableview.backgroundColor=[UIColor blackColor];
    _listmainview.backgroundColor=[UIColor blackColor];
    
    isFromFindRiders = @"";
    
    //Display Annotation
    // Show coach marks
    BOOL coachMarksShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"MPCoachMarksShown_BrowseRiders"];
    if (coachMarksShown == NO) {
        // Don't show again
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MPCoachMarksShown_BrowseRiders"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        // Show coach marks
        [self showAnnotation];
    }
   
}

#pragma mark - Annotations
-(void)showAnnotation
{
    [self.view layoutIfNeeded];
    
    
    
    
    NSArray *coachMarks;
    
    if ([isFromFindRiders isEqualToString:@"YES"]) {
        
        // Setup coach marks
//        CGRect coachmark1 = CGRectMake ( ([UIScreen mainScreen].bounds.size.width - 53), 20, self.navigationController.navigationBar.frame.size.height, self.navigationController.navigationBar.frame.size.height);
        CGRect coachmark2 = CGRectMake( _map_list_image_view.frame.origin.x, _map_list_image_view.frame.origin.y+65, _map_list_image_view.frame.size.width, _map_list_image_view.frame.size.height);
        
        isFromFindRiders = @"";
        // Setup coach marks
        coachMarks = @[
//                                @{
//                                    @"rect": [NSValue valueWithCGRect:coachmark1],
//                                    @"caption": @"Search for the riders",
//                                    @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
//                                    @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
//                                    @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_RIGHT]
//                                    //@"showArrow":[NSNumber numberWithBool:YES]
//                                    },
                                @{
                                    @"rect": [NSValue valueWithCGRect:coachmark2],
                                    @"caption": @"Tap here to map view",
                                    @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                                    @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
                                    @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_RIGHT],
                                    //@"showArrow":[NSNumber numberWithBool:YES]
                                    },
                                ];
    }
    else
    {
        // Setup coach marks
        CGRect coachmark1 = CGRectMake ( ([UIScreen mainScreen].bounds.size.width - 53), 20, self.navigationController.navigationBar.frame.size.height, self.navigationController.navigationBar.frame.size.height);
        CGRect coachmark2 = CGRectMake( _map_list_image_view.frame.origin.x, _map_list_image_view.frame.origin.y, _map_list_image_view.frame.size.width, _map_list_image_view.frame.size.height);

        
        // Setup coach marks
        coachMarks = @[
                                @{
                                    @"rect": [NSValue valueWithCGRect:coachmark1],
                                    @"caption": @"Search for the riders",
                                    @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                                    @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                                    @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_RIGHT]
                                    //@"showArrow":[NSNumber numberWithBool:YES]
                                    },
                                @{
                                    @"rect": [NSValue valueWithCGRect:coachmark2],
                                    @"caption": @"Tap here to list view",
                                    @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                                    @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
                                    @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_RIGHT],
                                    //@"showArrow":[NSNumber numberWithBool:YES]
                                    },
                                ];
    }
    
    
    
    MPCoachMarks *coachMarksView = [[MPCoachMarks alloc] initWithFrame:self.navigationController.view.bounds coachMarks:coachMarks];
    [self.navigationController.view addSubview:coachMarksView];
    //[[UIApplication sharedApplication].keyWindow addSubview:coachMarksView];
    [coachMarksView start];
    
}

-(BOOL)check_Location_On_Or_Off{
    
    if ([CLLocationManager locationServicesEnabled]){
        NSLog(@"Location Services Enabled");
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
        {
            UIAlertController *alertview = [UIAlertController alertControllerWithTitle:@"LOCATION" message:@"Turn on your App Location" preferredStyle:UIAlertControllerStyleAlert];
            
            [alertview addAction:[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root=Privacy:Location_Services"]];
            }]];
            [self presentViewController:alertview animated:YES completion:nil];
            return NO;
        }
        else{
            NSLog(@"Location Services Enabled");
            
            [self get_browseriders];
            return YES;
        }
    }
    else{
        UIAlertController *alertview = [UIAlertController alertControllerWithTitle:@"LOCATION" message:@"Turn ON Your Location" preferredStyle:UIAlertControllerStyleAlert];
       
        [alertview addAction:[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
            
            // Distructive button tapped.
            //                [self dismissViewControllerAnimated:YES completion:^{
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root=Privacy:Location_Services"]];
            //                }];
        }]];
        
        [self presentViewController:alertview animated:YES completion:nil];
        
        return NO;
    }
}


-(void)menuclicked
{
    [self.navigationController popViewControllerAnimated:YES];

}
-(void)viewWillAppear:(BOOL)animated
{
     [[self navigationController] setNavigationBarHidden:NO animated:NO];
    
//    searchstatus=NO;
//    screenstrng=@"LIST";
//    [self map_loadingMethod];
//    if ([self check_Location_On_Or_Off]) {
//        [self get_browseriders];
//   }
//    self.emptyCalloutView = [[UIView alloc] initWithFrame:CGRectZero];
//    [self Calloutview];
//    
//    _list_tableview.backgroundColor=[UIColor blackColor];
//    _listmainview.backgroundColor=[UIColor blackColor];
}
-(void)search
{
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    acController.delegate = self;
   
    [self presentViewController:acController animated:YES completion:nil];
}
// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didUpdateAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}
- (void)resultsController:(GMSAutocompleteResultsViewController *)resultsController
 didAutocompleteWithPlace:(GMSPlace *)place
{
    // Do something with the selected place.
    NSLog(@"Place name %@", place.name);
    NSLog(@"Place address %@", place.formattedAddress);
    NSLog(@"Place attributions %@", place.attributions.string);
}
- (void)viewController:(GMSAutocompleteViewController *)viewController
didFailAutocompleteWithError:(NSError *)error
{

}

- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithPlace:(GMSPlace *)place
{
    
    NSLog(@"Place name %@", place.name);
    NSLog(@"Place address %@", place.formattedAddress);
    NSLog(@"Place attributions %@", place.attributions.string);
    NSLog(@"Place attributions %@", place.viewport);
    NSLog(@"Place lat %f", place.coordinate.latitude);
    NSLog(@"Place long %f", place.coordinate.longitude);
    
    search_Place_lat=[NSString stringWithFormat:@"%f", place.coordinate.latitude];
    search_place_Long=[NSString stringWithFormat:@"%f", place.coordinate.longitude];
    
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    bounds=place.viewport;
    southWest = CLLocationCoordinate2DMake(bounds.southWest.latitude, bounds.southWest.longitude);
    northEast = CLLocationCoordinate2DMake(bounds.northEast.latitude,bounds.northEast.longitude);
    NSLog(@"Place southWest latitude  %f", southWest.latitude);
    NSLog(@"Place southWest longitude %f", southWest.longitude);
    NSLog(@"Place northEast latitude  %f", northEast.latitude);
    NSLog(@"Place northEast longitude %f", northEast.longitude);
    [_mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds]];
    
    
    searchstatus=YES;
    if ([self check_Location_On_Or_Off]) {
         [self get_browseriders];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark mapLoading
-(void)map_loadingMethod
{
    [self.view layoutIfNeeded];
    map_locationManager = [[CLLocationManager alloc] init];
    map_locationManager.delegate=self;
    map_currentLatitude = map_locationManager.location.coordinate.latitude;
    map_currentLongitude = map_locationManager.location.coordinate.longitude;
    
    
   
    mapZoom=15;
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:map_currentLatitude
                                                            longitude:map_currentLongitude
                                                                 zoom:mapZoom];
    _mapView = [GMSMapView mapWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) camera:camera];
    
    
    if (map_currentLatitude==0.0 && map_currentLongitude ==0.0) {
       
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:CLLocationCoordinate2DMake(48.857229, -49.898547) coordinate:CLLocationCoordinate2DMake(23.955779,-127.945417)];
        [_mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds]];
    }
    else
    {
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithRegion:_mapView.projection.visibleRegion];
    [_mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds]];
}

//    self.view = _mapView;
    [self.listmainview addSubview:_mapView];
    [self.listmainview addSubview:_re_center_btn];

    _mapView.delegate=self;
    _mapView.myLocationEnabled = true;
    _list_tableview.dataSource=self;
    _list_tableview.delegate=self;
    
    
    [self get_browseriders];
    
}

//- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
//{
//    CLLocation *currentLocation = newLocation;
//    
//    if (currentLocation != nil)
//    {
//        
//        double current_lat;
//        double current_long;
//        current_lat  = newLocation.coordinate.latitude;
//        current_long = newLocation.coordinate.longitude;
//        if(current_lat != 0.0 && current_long != 0.0)
//        {
//            map_currentLatitude = manager.location.coordinate.latitude;
//            map_currentLongitude = manager.location.coordinate.longitude;
//            GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:map_currentLatitude
//                                                                    longitude:map_currentLongitude
//                                                                         zoom:mapZoom];
//           // [_mapView animateToCameraPosition:camera];
//        }
//        
//    }
//    
//    
//}
- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position
{
    NSLog(@"idleAtCameraPosition  %@",position);
    
//    if (openMarkerInfoWindow_boolValue == YES) {
//        
//    }
//    else
//    {
//        if (searchPlace_boolValue == YES) {
//            searchPlace_boolValue = NO;
//        }
//        else
//        {
//            GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithRegion:_mapView.projection.visibleRegion];
    
//            NSString *northEastLat_string = [NSString stringWithFormat:@"%f",bounds.northEast.latitude];
//            NSString *nortEastLong_string = [NSString stringWithFormat:@"%f",bounds.northEast.longitude];
//            NSString *southWestLat_string = [NSString stringWithFormat:@"%f",bounds.southWest.latitude];
//            NSString *southWestLong_string = [NSString stringWithFormat:@"%f",bounds.southWest.longitude];
//            
//            NSMutableDictionary *dict = [NSMutableDictionary new];
//            [dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
//            [dict setObject:[NSString stringWithFormat:@"%f",northEast.latitude] forKey:@"north_east_lat"];
//            [dict setObject:[NSString stringWithFormat:@"%f",northEast.longitude] forKey:@"north_east_long"];
//            [dict setObject:[NSString stringWithFormat:@"%f",southWest.latitude] forKey:@"south_west_lat"];
//            [dict setObject:[NSString stringWithFormat:@"%f",southWest.longitude] forKey:@"south_west_long"];
            
            //Service call for properties
//    if (searchstatus==YES)
//    {
//        [_mapView animateToLocation:CLLocationCoordinate2DMake([search_Place_lat doubleValue], [search_place_Long doubleValue])];
//        [SHARED_HELPER showAlert:NORIDERS_LOCATION];
//    }
            searchstatus=NO;
            if (![SHARED_HELPER checkIfisInternetAvailable]) {
                [SHARED_HELPER showAlert:Nonetwork];
                return;
            }
            else
            {
                [self get_browseriders];
            }
            
            
//        }
    //}
}



-(void)get_browseriders
{
    if (searchstatus==NO)
    {
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithRegion:_mapView.projection.visibleRegion];
        northEast = bounds.northEast;
        southWest = bounds.southWest;
    }
    
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
    [dict setObject:[NSString stringWithFormat:@"%f",northEast.latitude] forKey:@"north_east_lat"];
    [dict setObject:[NSString stringWithFormat:@"%f",northEast.longitude] forKey:@"north_east_long"];
    [dict setObject:[NSString stringWithFormat:@"%f",southWest.latitude] forKey:@"south_west_lat"];
    [dict setObject:[NSString stringWithFormat:@"%f",southWest.longitude] forKey:@"south_west_long"];
    
//    [dict setObject:[NSNumber numberWithDouble:northEast.longitude] forKey:@"north_east_lat"];
//    [dict setObject:[NSNumber numberWithDouble:northEast.longitude] forKey:@"north_east_long"];
//    [dict setObject:[NSNumber numberWithDouble:southWest.latitude] forKey:@"south_west_lat"];
//    [dict setObject:[NSNumber numberWithDouble:southWest.longitude] forKey:@"south_west_long"];
    
    
    
    NSLog(@"%@",dict);
    
    [SHARED_API browseRidersWithParams:dict withSuccess:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           NSLog(@"response is %@",response);
                           [_mapView clear];
                           riders_ary = [response valueForKey:@"riders"];
                           [_list_tableview reloadData];
                           if (riders_ary.count>0) {
                               
                               id<GMUClusterAlgorithm> algorithm = [[GMUNonHierarchicalDistanceBasedAlgorithm alloc] init];
                               id<GMUClusterIconGenerator> iconGenerator = [[GMUDefaultClusterIconGenerator alloc] init];
                               id<GMUClusterRenderer> renderer =
                               [[GMUDefaultClusterRenderer alloc] initWithMapView:_mapView
                                                             clusterIconGenerator:iconGenerator];
                               _clusterManager =
                               [[GMUClusterManager alloc] initWithMap:_mapView algorithm:algorithm renderer:renderer];
                               
                               ((GMUDefaultClusterRenderer *)renderer).delegate = self;
                               
                               
                                _no_Riders_Label.hidden=YES;
                               for (int i=0; i< [riders_ary count]; i++)
                               {
                                   
                                   NSString *currlat = [NSString stringWithFormat:@"%@",[[riders_ary  valueForKey:@"current_latitude"] objectAtIndex:i]];
                                   NSString *currlong = [NSString stringWithFormat:@"%@",[[riders_ary valueForKey:@"current_longitude"]objectAtIndex:i]];
                                   
                                    NSString *name = [NSString stringWithFormat:@"%@",[[riders_ary valueForKey:@"name"]objectAtIndex:i]];
                                   
                                    NSString *address = [NSString stringWithFormat:@"%@",[[riders_ary valueForKey:@"address"]objectAtIndex:i]];
                                   
                                   NSString *profile_image_string = [NSString stringWithFormat:@"%@",[[riders_ary valueForKey:@"profile_image"] objectAtIndex:i]];
                                   
                                    NSLog(@"%@",profile_image_string);
                                   
                                   // image
                                   UIImageView *iconView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 90, 90)];
                                  // iconView.image = [UIImage imageNamed:@"browseermapicon"];
                                   iconView.image = [UIImage imageNamed:@"location_icon"];
//                                   UIImageView *profileView = [[UIImageView alloc] initWithFrame:CGRectMake(22, 12, 50, 50)];
                                   UIImageView *profileView = [[UIImageView alloc] initWithFrame:CGRectMake(19.5, 8.5, 53, 52)];
                                   [profileView sd_setImageWithURL:[NSURL URLWithString:profile_image_string]
                                                  placeholderImage:[UIImage imageNamed:@"user_Placeholder"]
                                                           options:SDWebImageRefreshCached];
                                   profileView.layer.cornerRadius = profileView.frame.size.width / 2;
                                   profileView.clipsToBounds = YES;
                                   [iconView addSubview:profileView];
                                   
                                   dispatch_async(dispatch_get_main_queue(), ^
                                        {
//                                                      
//                                            CLLocationCoordinate2D position = CLLocationCoordinate2DMake([currlat doubleValue], [currlong doubleValue]);
//                                            _marker = [GMSMarker markerWithPosition:position];
//                                            _marker.title = name;
//                                            _marker.snippet = address;
//                                            _marker.iconView = iconView;
//                                            _marker.tracksViewChanges = YES;
//                                            _marker.map = self.mapView;
//                                            
//                                            //_marker.userData = markerInfo;
//                                            //                                   _marker.infoWindowAnchor = CGPointMake(0.5, 0.25);
//                                            _marker.groundAnchor = CGPointMake(0.5, 1.0);
                                            
                                        });
                                   
                                   name=[NSString stringWithFormat:@"%@,%@,%@",name,profile_image_string,address];
                                   
                                   id<GMUClusterItem> item =
                                   [[POIItem alloc] initWithPosition:CLLocationCoordinate2DMake([currlat doubleValue], [currlong doubleValue]) name:name];
                                   
                                   
                                   [_clusterManager addItem:item];
                                   
                               }
                               // Call cluster() after items have been added to perform the clustering and rendering on map.
                               [_clusterManager cluster];
                               
                               // Register self to listen to both GMUClusterManagerDelegate and GMSMapViewDelegate events.
                               [_clusterManager setDelegate:self mapDelegate:self];
                               
                               
                               searchstatus=NO;
                               _no_Riders_Label.hidden=YES;
                           }
                           else
                           {
                               _no_Riders_Label.hidden=NO;
                               _no_Riders_Label.textColor=[UIColor whiteColor];
                               _list_tableview.hidden=YES;
                                searchstatus=NO;
                              [self showAlert_is:NORIDERS_LOCATION];
                           }
                       });
        
    } onfailure:^(NSError *theError) {
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           
                           [SHARED_HELPER showAlert:ServiceFail];
                           //_groups_table_view.hidden = YES;
                           //                           if ([activeIndicatore isAnimating]) {
                           //                               [activeIndicatore stopAnimating];
                           //                               [activeIndicatore removeFromSuperview];
                           //                           }
                           //                           
                           
                           
                       });
    }];
}


-(void)Calloutview
{
    self.calloutView = [[SMCalloutView alloc] init];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    [button addTarget:self
               action:@selector(calloutAccessoryButtonTapped:)
     forControlEvents:UIControlEventTouchUpInside];
    self.calloutView.rightAccessoryView = button;
}

- (void)calloutAccessoryButtonTapped:(id)sender
{
    
    
}
#pragma mark GMUClusterManagerDelegate

- (BOOL) clusterManager:(GMUClusterManager *)clusterManager didTapCluster:(id<GMUCluster>)cluster {
    GMSCameraPosition *newCamera =
    [GMSCameraPosition cameraWithTarget:cluster.position zoom:_mapView.camera.zoom + 1];
    GMSCameraUpdate *update = [GMSCameraUpdate setCamera:newCamera];
    [_mapView moveCamera:update];
    
    return YES;
}

- (void)renderer:(id<GMUClusterRenderer>)renderer willRenderMarker:(GMSMarker *)marker {
    
    if ([marker.userData isKindOfClass:[POIItem class]]) {
        
        POIItem *poiItem = marker.userData;
        NSArray *elements=[poiItem.name componentsSeparatedByString:@","];
        NSLog(@"img%@",elements);
        marker.title = [elements objectAtIndex:0];
        UIImageView *iconView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 90, 90)];
        iconView.image = [UIImage imageNamed:@"location_icon"];
        UIImageView *profileView = [[UIImageView alloc] initWithFrame:CGRectMake(19.5, 8.5, 53, 52)];
        
        [profileView sd_setImageWithURL:[NSURL URLWithString:[elements objectAtIndex:1]]
                       placeholderImage:[UIImage imageNamed:@"user_Placeholder"]
                                options:SDWebImageRefreshCached];
        profileView.layer.cornerRadius = profileView.frame.size.width / 2;
        profileView.clipsToBounds = YES;
        [iconView addSubview:profileView];
        marker.iconView=iconView;
        marker.snippet=[elements objectAtIndex:2];
        
        
        NSLog(@"marker.title %@",marker.title);
        
        
    } else if ([marker.userData conformsToProtocol:@protocol(GMUCluster)]) {
        //  marker.icon = [UIImage imageNamed:@"marker_icon"];
    }
    
    
}

#pragma mark - GMSMapViewDelegate
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error{
    
    NSLog(@"service");
}
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray<CLLocation *> *)locations{
    
    if (locations.count>0) {
        CLLocation *loc=[locations objectAtIndex:0];
        map_currentLatitude=loc.coordinate.latitude;
        map_currentLongitude=loc.coordinate.longitude;
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:map_currentLatitude
                                                                longitude:map_currentLongitude
                                                                     zoom:mapZoom];
        [_mapView animateToCameraPosition:camera];
    }
}
- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker
{
    CLLocationCoordinate2D anchor = marker.position;
    
    CGPoint point = [mapView.projection pointForCoordinate:anchor];

    UIView *viewfordata=[[UIView alloc ] initWithFrame:CGRectMake(0, 0, 200, 60)];
    viewfordata.backgroundColor=[UIColor whiteColor];
    UILabel *namelabel=[[UILabel alloc] initWithFrame:CGRectMake(10, 5, 180,25 )];
    namelabel.text= marker.title;
    [namelabel setFont:[UIFont fontWithName:@"Antonio-Bold" size:15]];
    namelabel.textColor=APP_BLUE_COLOR;
    namelabel.backgroundColor=[UIColor clearColor];
    NSString *addressstr=marker.snippet;
    if (!([addressstr isEqualToString:@"<null>"] || [addressstr isEqualToString:@""] || [addressstr isEqualToString:@"null"] || addressstr == nil))
    {
        UIImageView *imageview=[[UIImageView alloc] initWithFrame:CGRectMake(5, 33, 20, 17)];
        imageview.image=[UIImage imageNamed:@"check_in_placeholder"];
        
        UILabel *addresslabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 30, 180,25 )];
        addresslabel.text=marker.snippet;
        [addresslabel setFont:[UIFont fontWithName:@"Antonio-Reguler" size:12]];
        
        addresslabel.textColor=[UIColor lightGrayColor];
        addresslabel.backgroundColor=[UIColor clearColor];
        [viewfordata addSubview:imageview];
        [viewfordata addSubview:addresslabel];
    }
    [viewfordata addSubview:namelabel];
    return viewfordata;
    
}

- (void)mapView:(GMSMapView *)pMapView didChangeCameraPosition:(GMSCameraPosition *)position {
    /* move callout with map drag */
    if (pMapView.selectedMarker != nil && !self.calloutView.hidden) {
        CLLocationCoordinate2D anchor = [pMapView.selectedMarker position];
        
        CGPoint arrowPt = self.calloutView.backgroundView.arrowPoint;
        
        CGPoint pt = [pMapView.projection pointForCoordinate:anchor];
        pt.x -= arrowPt.x;
        pt.y -= arrowPt.y + CalloutYOffset;
        
        self.calloutView.frame = (CGRect) {.origin = pt, .size = self.calloutView.frame.size };
    } else {
        self.calloutView.hidden = YES;
    }
//    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithRegion:_mapView.projection.visibleRegion];
//    northEast = bounds.northEast;
//    southWest = bounds.southWest;
//    [self get_browseriders];
}

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    self.calloutView.hidden = YES;
}

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker {
    /* don't move map camera to center marker on tap */
    mapView.selectedMarker = marker;
    return YES;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   return  riders_ary.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"BrowsRiderCell";
    BrowsRiderCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"BrowsRiderCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    NSDictionary *dic=[riders_ary objectAtIndex:indexPath.row];
    NSString *loctionstr=[NSString stringWithFormat:@"%@", [dic valueForKey:@"address"]];
    
    if ([loctionstr isEqualToString:@"<null>"] || [loctionstr isEqualToString:@""] || [loctionstr isEqualToString:@"null"] || loctionstr == nil)
    {
        cell.loactionimageview.hidden=YES;
        cell.address_label.hidden=YES;
    }
    else
    {
        cell.loactionimageview.hidden=NO;
        cell.address_label.hidden=NO;
        cell.address_label.text=loctionstr;
    }
    
     NSString *Namestr=[NSString stringWithFormat:@"%@", [dic valueForKey:@"name"]];
    if ([Namestr isEqualToString:@"<null>"] || [Namestr isEqualToString:@""] || [Namestr isEqualToString:@"null"] || Namestr == nil)
    {
        cell.name_label.hidden=YES;
    }
    else
    {
        cell.name_label.hidden=NO;
        cell.name_label.text=Namestr;
    }
    
    NSString *profile_image_string1 = [NSString stringWithFormat:@"%@", [dic valueForKey:@"profile_image"]];
    NSURL*url=[NSURL URLWithString:profile_image_string1];
    [cell.profileimage sd_setImageWithURL:url
                              placeholderImage:[UIImage imageNamed:@"user_Placeholder"]
                                       options:SDWebImageRefreshCached];
    
    if (indexPath.row%2==0)
    {
        cell.content_view.backgroundColor=[UIColor clearColor];
        cell.contentView.superview.backgroundColor = ROUTES_CELL_BG_COLOUR1;
    }
    else
    {
       cell.content_view.backgroundColor=[UIColor clearColor];
       cell.contentView.superview.backgroundColor = ROUTES_CELL_BG_COLOUR2;
    }
    return cell;
}

- (IBAction)map_list_action:(id)sender
{
    
    if ([screenstrng isEqualToString:@"MAP"])
    {
        screenstrng=@"LIST";
        _map_list_image_view.image=[UIImage imageNamed:@"rider_list_icon"];
        _mapView.hidden=NO;
        _no_Riders_Label.hidden=YES;
        _re_center_btn.hidden=NO;
        _list_tableview.hidden=YES;
    }
    else
    {
        screenstrng=@"MAP";
        _map_list_image_view.image=[UIImage imageNamed:@"rider_display_icon"];
              _mapView.hidden=YES;
        _list_tableview.hidden=NO;
         _re_center_btn.hidden=YES;
        if (riders_ary.count>0) {
            _no_Riders_Label.hidden=YES;
            _list_tableview.hidden=NO;
        }
        else{
            _no_Riders_Label.hidden=NO;
            _no_Riders_Label.textColor=[UIColor whiteColor];
            _list_tableview.hidden=YES;
        }
    }
    
    
    //Display Annotation
    // Show coach marks
    BOOL coachMarksShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"MPCoachMarksShown_FindRiders"];
    if (coachMarksShown == NO) {
        // Don't show again
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MPCoachMarksShown_FindRiders"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        isFromFindRiders = @"YES";
        // Show coach marks
        [self showAnnotation];
    }
   
}
- (IBAction)Re_center_Action:(id)sender {
    searchstatus=NO;
    if ([self check_Location_On_Or_Off]) {
    [map_locationManager requestLocation];
    }
}
-(void)showAlert_is:(NSString *)message
{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:message  preferredStyle:UIAlertControllerStyleActionSheet];
    UIView  *firstSubview = alert.view.subviews.firstObject;
    firstSubview.backgroundColor = APP_YELLOW_COLOR;
    UIView *alertContentView = firstSubview.subviews.firstObject;
    alertContentView.backgroundColor = APP_YELLOW_COLOR;
    alertContentView.tintColor = [UIColor blueColor];
    for (UIView *subSubView in alertContentView.subviews) {
        
        subSubView.backgroundColor = APP_YELLOW_COLOR;
    }
    // [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alert animated:YES completion:nil];
    [self presentViewController:alert animated:YES completion:nil];
    
    int duration = 1; // duration in seconds
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        
        [alert dismissViewControllerAnimated:YES completion:nil];
        
    });
    
}
- (IBAction)onClick_browseRiders_rideGoingBtn:(id)sender {
    
    self.browseRiders_scrollLabel.hidden=YES;
    self.browseRiders_rideGoingBtn.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }

    
    
}
@end
