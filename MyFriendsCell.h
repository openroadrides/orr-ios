//
//  MyFriendsCell.h
//  openroadrides
//
//  Created by apple on 26/06/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyFriendsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *contentview_myFriends;
@property (weak, nonatomic) IBOutlet UIButton *checkbox_myfriendsBtn;
@property (weak, nonatomic) IBOutlet UIImageView *profileUser_myFriends;
@property (weak, nonatomic) IBOutlet UILabel *userName_myFriends;

@property (weak, nonatomic) IBOutlet UILabel *address_myFriends;
@property (weak, nonatomic) IBOutlet UIView *viewForRequest_myfriends;
@property (weak, nonatomic) IBOutlet UIView *viewForAccept_myFriends;
@property (weak, nonatomic) IBOutlet UIView *viewForReject_myFriends;
@property (weak, nonatomic) IBOutlet UIView *viewForFriends_myFriends;
@property (weak, nonatomic) IBOutlet UIButton *acceptBtn_myFriends;
@property (weak, nonatomic) IBOutlet UIImageView *locationPlaceholde_imageView;
@property (weak, nonatomic) IBOutlet UIButton *rejectBtn_myFriends;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint_ViewForRequests;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewto_useranme_constraint;
@property (weak, nonatomic) IBOutlet UILabel *labe_friend_pending_myFriends;
@end
