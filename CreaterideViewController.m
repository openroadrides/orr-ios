//
//  CreaterideViewController.m
//  openroadrides
//
//  Created by apple on 22/05/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "CreaterideViewController.h"
#import "Constants.h"
#import "RideDashboard.h"
#import "MPCoachMarks.h"


#define startlocation @"Select your start location."
#define ride_Date @"Select your start date."
#define ride_Time @"Select your start time."
#define ridecreated @"Your scheduled ride is created successfully."
#define ridenameempty @"Ride name should not be empty."


@interface CreaterideViewController ()

@end

@implementation CreaterideViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    
//    [self setdate];
//    [self settime];
//    [self settime_start];
//    [self setdate_meeting];

    self.title = @"CREATE RIDE";
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    
    
    [[AppHelperClass appsharedInstance] setRideType:@"setup_ride"];
    
    UIButton *saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [saveBtn addTarget:self action:@selector(save_clicked) forControlEvents:UIControlEventTouchUpInside];
    saveBtn.frame = CGRectMake(0, 0, 30, 30);
    [saveBtn setBackgroundImage:[UIImage imageNamed:@"SAVE"] forState:UIControlStateNormal];
    UIBarButtonItem * create_Ride= [[UIBarButtonItem alloc] initWithCustomView:saveBtn];
    self.navigationItem.rightBarButtonItems=[NSArray arrayWithObjects:create_Ride, nil];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [leftBtn addTarget:self action:@selector(cancle_clicked:) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.frame = CGRectMake(0, 0, 25, 25);
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"close_icon"] forState:UIControlStateNormal];
    UIBarButtonItem *leftBackButton=[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    item0=leftBackButton;
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:item0, nil];
    
    self.ride_coverImage.layer.cornerRadius=self.ride_coverImage.frame.size.width/2;
    self.ride_coverImage.clipsToBounds=YES;
    
//    UIBarButtonItem *cancle_btn=[[UIBarButtonItem alloc]initWithTitle:@"X" style:UIBarButtonItemStylePlain target:self action:@selector(cancle_clicked)];
//    [cancle_btn setTitleTextAttributes:@{
//                                             NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:26.0],
//                                             NSForegroundColorAttributeName: [UIColor blackColor]
//                                             } forState:UIControlStateNormal];
//    
//    self.navigationItem.leftBarButtonItem = cancle_btn;
    
    self.createRide_scrollLabel.hidden=YES;
    self.createRide_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.createRide_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.createRide_scrollLabel.textColor = [UIColor blackColor];
    self.createRide_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.createRide_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.createRide_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.createRide_scrollLabel.scrollSpeed = 60; // pixels per second
    self.createRide_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.createRide_scrollLabel.fadeLength = 0.f;
    self.createRide_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.createRide_scrollLabel observeApplicationNotifications];
    
    self.createRide_rideGoingBtn.hidden=YES;
    if (appDelegate.ridedashboard_home) {
        self.createRide_scrollLabel.hidden=NO;
        self.createRide_rideGoingBtn.hidden=NO;
    }
    
    [self.Ridename_tf setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.Ridedate_tf setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.Placetostart_tf setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.Meetingtime_tf setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.Description_tf setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.upload_img_tf setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.meetingAddress_TF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.meetingDate_TF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.startTime_TF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];

    
    
    NSUInteger len =8;
    NSString *letterd =@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (NSUInteger i = 0; i < len; i++) {
        u_int32_t r = arc4random() % [letterd length];
        unichar c = [letterd characterAtIndex:r];
        [randomString appendFormat:@"%C", c];
    }
    random_number_string=[NSString stringWithFormat:@"%@",randomString];
    NSLog(@"random Number %@",random_number_string);
    user_token_id = [NSString stringWithFormat:@"%@",[Defaults valueForKey:User_ID]];
    self.ride_coverImage.layer.cornerRadius=self.ride_coverImage.frame.size.width/2;
    self.ride_coverImage.contentMode=UIViewContentModeScaleAspectFill;
    self.ride_coverImage.clipsToBounds=YES;
    
    
    //empty selectec route_id
    appDelegate.select_Route_Id_For_CreateAndFreeRide=@"";
    appDelegate.select_Route_Address_For_CreateAndFreeRide=@"";
    appDelegate.start_address_latitude=@"";
    appDelegate.start_address_longtitude=@"";
    
    self.Placetostart_tf.userInteractionEnabled=NO;
    _AddressTextString=@"NO";
     selected_route=@"";
    
    // Do any additional setup after loading the view.
    
    
    //if is from Start_Ride
    
    
    
    
}

#pragma mark - Annotations
-(void)showAnnotation
{
    [self.view layoutIfNeeded];
    
    
    // Setup coach marks
    CGRect coachmark1 = CGRectMake (5,20, self.navigationController.navigationBar.frame.size.height,self.navigationController.navigationBar.frame.size.height);
    CGRect coachmark2 = CGRectMake( ([UIScreen mainScreen].bounds.size.width - 53), 20, self.navigationController.navigationBar.frame.size.height, self.navigationController.navigationBar.frame.size.height);
    CGRect coachmark3 = CGRectMake( _rideimage_btn.frame.origin.x, _rideimage_btn.frame.origin.y+self.navigationController.navigationBar.frame.size.height+22, _rideimage_btn.frame.size.width, _rideimage_btn.frame.size.height);
    
    
    // Setup coach marks
    NSArray *coachMarks = @[
                            @{
                                @"rect": [NSValue valueWithCGRect:coachmark1],
                                @"caption": @"Tap to close ride",
                                @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                                @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                                @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_LEFT]
                                //@"showArrow":[NSNumber numberWithBool:YES]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:coachmark2],
                                @"caption": @"Tap to save ride",
                                @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                                @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                                @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_RIGHT],
                                //@"showArrow":[NSNumber numberWithBool:YES]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:coachmark3],
                                @"caption": @"Add the image here",
                                @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                                @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM]
                                
                                },
                            ];
    
    
    MPCoachMarks *coachMarksView = [[MPCoachMarks alloc] initWithFrame:self.navigationController.view.bounds coachMarks:coachMarks];
    [self.navigationController.view addSubview:coachMarksView];
    //[[UIApplication sharedApplication].keyWindow addSubview:coachMarksView];
    [coachMarksView start];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
        if ([_AddressTextString isEqualToString:@"YES"] && [appDelegate.select_Route_Id_For_CreateAndFreeRide isEqualToString:@""]) {
            if ([selected_route isEqualToString:@""]) {
                
            }
            else
            {
                _Placetostart_tf.text=@"";
               
            }
        }
        else  if (![appDelegate.select_Route_Id_For_CreateAndFreeRide isEqualToString:@""])
        {
            _Placetostart_tf.text=[NSString stringWithFormat:@"%@",appDelegate.select_Route_Address_For_CreateAndFreeRide];
            latitude=[appDelegate.start_address_latitude doubleValue];
            longitude=[appDelegate.start_address_longtitude doubleValue];
            
        }
        else if ([appDelegate.select_Route_Id_For_CreateAndFreeRide isEqualToString:@""])
        {
            _Placetostart_tf.text=@"";
        }
      selected_route=appDelegate.select_Route_Id_For_CreateAndFreeRide;
    
    //Display Annotation
    // Show coach marks
    BOOL coachMarksShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"MPCoachMarksShown_CreateRide"];
    if (coachMarksShown == NO) {
        // Don't show again
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MPCoachMarksShown_CreateRide"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        // Show coach marks
        [self showAnnotation];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)cancle_clicked:(UIButton *)sender
{
    appDelegate.arrayOfSelectedFrndsAndRiders=[[NSMutableArray alloc]init];
    appDelegate.arrayOfSelectedGroupsAndRiders=[[NSMutableArray alloc]init];
 [self.navigationController popViewControllerAnimated:YES];
}
-(void)setdate{
    
    date1 = [[UIDatePicker alloc]init];
    date1.datePickerMode = UIDatePickerModeDate;
    [date1 setMinimumDate:[NSDate date]];
    [date1 setDate:[NSDate date]];
    [_Ridedate_tf setInputView:date1];
    
    UIToolbar *toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolbar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *done = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(showDate)];
    done.tintColor=[UIColor blackColor];
    
    UIBarButtonItem *space = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolbar setItems:[NSArray arrayWithObjects:space,done,nil]];
    [_Ridedate_tf setInputAccessoryView:toolbar];
    
}
-(void)setdate_meeting{
    
    meeting_date = [[UIDatePicker alloc]init];
    meeting_date.datePickerMode = UIDatePickerModeDate;
    [meeting_date setMinimumDate:[NSDate date]];
    [meeting_date setMaximumDate:date1.date];
    
    [meeting_date setDate:[NSDate date]];
    [_meetingDate_TF setInputView:meeting_date];
    
    UIToolbar *toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolbar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *done = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(showDate_meeting)];
     done.tintColor=[UIColor blackColor];
    UIBarButtonItem *space = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolbar setItems:[NSArray arrayWithObjects:space,done,nil]];
    [_meetingDate_TF setInputAccessoryView:toolbar];
    
}

-(void)settime{
    NSDateFormatter *dateformate = [[NSDateFormatter alloc ]init];
    [dateformate setDateFormat:@"MM/dd/YYYY"];
    date = [[UIDatePicker alloc]init];
    date.datePickerMode = UIDatePickerModeTime;
    date.date=[NSDate date];
//    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"NL"];
//    [date setLocale:locale];
    [date setDate:[NSDate date]];
   
    if ([[dateformate stringFromDate:date1.date] compare:[dateformate stringFromDate:[NSDate date]]]==NSOrderedSame ) {
        [date setMinimumDate:[NSDate date]];
    }
    [_Meetingtime_tf setInputView:date];
    
    
    UIToolbar *toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolbar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *done = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(showTime)];
     done.tintColor=[UIColor blackColor];
    UIBarButtonItem *space = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [toolbar setItems:[NSArray arrayWithObjects:space,done,nil]];
    [_Meetingtime_tf setInputAccessoryView:toolbar];
    
}
-(void)settime_start{
    
    start_time = [[UIDatePicker alloc]init];
    start_time.datePickerMode = UIDatePickerModeTime;
    
//    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"NL"];
//    [start_time setLocale:locale];
    [start_time setDate:[NSDate date]];
    if ([_Ridedate_tf.text compare:_meetingDate_TF.text]==NSOrderedSame) {
        [start_time setMaximumDate:date.date];
    }
    [_startTime_TF setInputView:start_time];
    
    UIToolbar *toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolbar setTintColor:[UIColor blackColor]];
    
//    UIBarButtonItem *done = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(showTime_start)];
//    
//    
//     done.tintColor=[UIColor blackColor];
//    UIBarButtonItem *space = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
//    
//    [toolbar setItems:[NSArray arrayWithObjects:space,done,nil]];
    
    
    
    
    toolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad1)],
                                                        [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                                          [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(showTime_start)]];
    
    [_startTime_TF setInputAccessoryView:toolbar];
    
    

    
}
-(void)showTime{
    
    NSDateFormatter *dateformater = [[NSDateFormatter alloc ]init];
//    [dateformater setDateFormat:@"HH:mm"];
    [dateformater setDateFormat:@"hh:mm a"];
    _Meetingtime_tf.text = [NSString stringWithFormat:@"%@",[dateformater stringFromDate:date.date]];
    NSDateFormatter*rideMeetTimeFormat=[[NSDateFormatter alloc ]init];
    [rideMeetTimeFormat setDateFormat:@"HH:mm"];
    rideMeetingTimeString=[NSString stringWithFormat:@"%@",[rideMeetTimeFormat stringFromDate:date.date]];

    [_Meetingtime_tf resignFirstResponder];
    
    
}
-(void)showTime_start{
    [_startTime_TF resignFirstResponder];
    NSDateFormatter *dateformater = [[NSDateFormatter alloc ]init];
//    [dateformater setDateFormat:@"HH:mm"];
    [dateformater setDateFormat:@"hh:mm a"];
    
    NSDate *todayDate = meeting_date.date;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSDate *todayDate2 = date1.date;
    NSString *convertedDateString1 = [dateFormatter stringFromDate:todayDate];
    NSString *convertedDateString2 = [dateFormatter stringFromDate:todayDate2];
    if ([convertedDateString1 isEqualToString:convertedDateString2])
    {
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitMinute fromDate:date.date toDate:start_time.date options:0];
        NSInteger numberOfMinutes = [components minute];
        
        if (numberOfMinutes<=-60)
        {
            _startTime_TF.text = [NSString stringWithFormat:@"%@",[dateformater stringFromDate:start_time.date]];
            NSDateFormatter*rideStartTimeFormat=[[NSDateFormatter alloc ]init];
            [rideStartTimeFormat setDateFormat:@"HH:mm"];
            rideStartTimeString=[NSString stringWithFormat:@"%@",[rideStartTimeFormat stringFromDate:start_time.date]];
        }
        else
        {
            [SHARED_HELPER showAlert:@"Meeting time should be minimum one hour before ride time."];
        }
    }
    else
    {
        _startTime_TF.text = [NSString stringWithFormat:@"%@",[dateformater stringFromDate:start_time.date]];
        NSDateFormatter*rideStartTimeFormat=[[NSDateFormatter alloc ]init];
        [rideStartTimeFormat setDateFormat:@"HH:mm"];
        rideStartTimeString=[NSString stringWithFormat:@"%@",[rideStartTimeFormat stringFromDate:start_time.date]];
    }
    
    
}
-(void)showDate{
    
    NSDateFormatter *dateformater = [[NSDateFormatter alloc ]init];
    [dateformater setDateFormat:@"MM/dd/YYYY"];
    _Ridedate_tf.text = [NSString stringWithFormat:@"%@",[dateformater stringFromDate:date1.date]];
    [_Ridedate_tf resignFirstResponder];
    
}
-(void)showDate_meeting{
    
    NSDateFormatter *dateformater = [[NSDateFormatter alloc ]init];
    [dateformater setDateFormat:@"MM/dd/YYYY"];
    _meetingDate_TF.text = [NSString stringWithFormat:@"%@",[dateformater stringFromDate:meeting_date.date]];
    [_meetingDate_TF resignFirstResponder];
    
}
-(void)cancelNumberPad1{
    
    [self.view endEditing:YES];
    
}
-(void)save_clicked
{
   
    [self.view endEditing:YES];
    _Ridename_tf.text=[_Ridename_tf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
    
    if (_Ridename_tf.text.length == 0 ) {
        [SHARED_HELPER showAlert:ridenameempty];
        return;
    }
    if (_Placetostart_tf.text.length == 0) {
        [SHARED_HELPER showAlert:startlocation];
        return;
    }
   
    if (_Ridedate_tf.text.length == 0) {
        [SHARED_HELPER showAlert:ride_Date];
        return;
    }
    if (_Meetingtime_tf.text.length == 0) {
        [SHARED_HELPER showAlert:ride_Time];
        return;
    }
    [self create_ride_request];
}
//"{
//""user_id"":"""",
//""route_id"":"""",
//""ride_title"":"""",
//""ride_description"":"""",
//""ride_cover_image"":"""",
//""random_number"":"""",
//""ride_type"":"""",
//""ride_start_point_address"":"""",
//""ride_start_point_latitude"":"""",
//""ride_start_point_longitude"":"""",
//""ride_start_time"":"""",
//""ride_start_date"":"""",
//""meeting_point_address"":"""",
//""meeting_point_latitude"":"""",
//""meeting_point_longitude"":"""",
//""meeting_time"":"""",
//""meeting_date"":"""",
//""group_invite_ids"":[],
//""friend_invite_ids"":[]
//}"
-(void)create_ride_request{

    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    activeIndicatore.hidesWhenStopped = TRUE;
    [activeIndicatore startAnimating];
    [self.view addSubview:activeIndicatore];
    if(!base64String)
    {
        base64String=@"";
    }
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:[_Ridename_tf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"ride_title"];
    [dict setObject:[_Description_tf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"ride_description"];
    [dict setObject:[_Placetostart_tf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"ride_start_point_address"];
    [dict setObject:[NSNumber numberWithDouble:latitude] forKey:@"ride_start_point_latitude"];
    [dict setObject:[NSNumber numberWithDouble:longitude] forKey:@"ride_start_point_longitude"];
    
    [dict setObject:[_Ridedate_tf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"ride_start_date"];
//    [dict setObject:[_Meetingtime_tf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"ride_start_time"];
//     [dict setObject:[rideMeetingTimeString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"ride_start_time"];
    [dict setObject:[NSString stringWithFormat:@"%@",rideMeetingTimeString]  forKey:@"ride_start_time"];
    
    [dict setObject:[_meetingAddress_TF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"meeting_point_address"];
//    [dict setObject:[_startTime_TF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"meeting_time"];
//    [dict setObject:[rideStartTimeString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"meeting_time"];
    [dict setObject:[NSString stringWithFormat:@"%@",rideStartTimeString] forKey:@"meeting_time"];
    
    [dict setObject:[_meetingDate_TF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"meeting_date"];
    [dict setObject:[NSNumber numberWithDouble:meeting_latitude] forKey:@"meeting_point_latitude"];
    [dict setObject:[NSNumber numberWithDouble:meeting_longitude]  forKey:@"meeting_point_longitude"];
    [dict setObject:random_number_string forKey:@"random_number"];
    [dict setObject:[[AppHelperClass appsharedInstance] rideType] forKey:@"ride_type"];
    [dict setObject:@"" forKey:@"group_id"];
    [dict setObject:user_token_id forKey:@"user_id"];
    [dict setObject:appDelegate.select_Route_Id_For_CreateAndFreeRide forKey:@"route_id"];
    if ([appDelegate.arrayOfSelectedFrndsAndRiders count]>0) {
        [dict setObject:appDelegate.arrayOfSelectedFrndsAndRiders forKey:@"friend_invite_ids"];
        }
    else{
        appDelegate.arrayOfSelectedFrndsAndRiders=[[NSMutableArray alloc]init];
        [dict setObject:appDelegate.arrayOfSelectedFrndsAndRiders forKey:@"friend_invite_ids"];
    }
    if ([appDelegate.arrayOfSelectedGroupsAndRiders count]>0) {
         [dict setObject:appDelegate.arrayOfSelectedGroupsAndRiders forKey:@"group_invite_ids"];
        
    }
    else{
       appDelegate.arrayOfSelectedGroupsAndRiders=[[NSMutableArray alloc]init];
        [dict setObject:appDelegate.arrayOfSelectedGroupsAndRiders forKey:@"group_invite_ids"];
    }
   
    [dict setObject:base64String forKey:@"ride_cover_image"];
    
    NSLog(@"%@",dict);
    [SHARED_API createRideRequestsWithParams:dict withSuccess:^(NSDictionary *response) {
            NSLog(@"responce is %@",response);
            if ([[response valueForKey:STATUS] isEqualToString:SUCCESS]) {
                 [SHARED_HELPER showAlert:ridecreated];
                 [self create_Ride_Sucsess];
                 [activeIndicatore stopAnimating];
            }
        } onfailure:^(NSError *theError) {
            [activeIndicatore stopAnimating];
            [SHARED_HELPER showAlert:ServerConnection];
        }];
    }

-(void)create_Ride_Sucsess{
    
dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    appDelegate.arrayOfSelectedFrndsAndRiders=[[NSMutableArray alloc]init];
    appDelegate.arrayOfSelectedGroupsAndRiders=[[NSMutableArray alloc]init];
    [activeIndicatore stopAnimating];
    appDelegate.Ride_Detail_Edited=@"YES";
    if ([_is_From isEqualToString:@"Start_Ride"])
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Start_Ride_From_Ride_Scheduled" object:self];
    }
    [self.navigationController popViewControllerAnimated:YES];
      });
}

#pragma mark -  UI-Textfield delegate methods.....

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if (textField==_Meetingtime_tf ||textField==_Description_tf  || textField==_meetingDate_TF )
    {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:.3];
        [UIView setAnimationBeginsFromCurrentState:TRUE];
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -150., self.view.frame.size.width, self.view.frame.size.height);
        
        [UIView commitAnimations];
    }
    if (textField ==_Ridedate_tf)
    {
        _Meetingtime_tf.text=@"";
        _meetingDate_TF.text=@"";
        _startTime_TF.text=@"";
        [self setdate];
    }
    if (textField == _Meetingtime_tf)
    {
        if ([_Ridedate_tf.text length]>0)
        {
            _startTime_TF.text=@"";
            _meetingDate_TF.text=@"";
            [self settime];
        }
        else
        {
            [textField resignFirstResponder];
            [SHARED_HELPER showAlert:RideDate];
        }
    }
    if (textField == _meetingDate_TF)
    {
        if ([_Meetingtime_tf.text length]>0)
        {
            _startTime_TF.text=@"";
            [self setdate_meeting];
        }
        else
        {
            [textField resignFirstResponder];
            [SHARED_HELPER showAlert:ride_Time];
        }
        
    }
    if (textField == _startTime_TF) {
        
        if ([_meetingDate_TF.text length]>0)
        {
            [self settime_start];
        }
        else
        {
            [textField resignFirstResponder];
            [SHARED_HELPER showAlert:MeetingDate];
        }
    }
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
     if (textField==_Meetingtime_tf ||textField==_Description_tf || textField==_meetingDate_TF ) {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +150., self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
}
    
   }
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    if (textField == _Ridename_tf)
    {
        [textField resignFirstResponder];
        return NO;
    }
//    else if (textField == _Placetostart_tf)
//    {
//        [_Ridedate_tf becomeFirstResponder];
//        return NO;
//    }
//    else if (textField == _Ridedate_tf)
//    {
//        [_Meetingtime_tf becomeFirstResponder];
//        return NO;
//    }
//    else if (textField == _Meetingtime_tf)
//    {
//        [_Description_tf becomeFirstResponder];
//        return NO;
//    }
//    else if (textField == _meetingDate_TF)
//    {
//        [_meetingDate_TF becomeFirstResponder];
//        return NO;
//    }
//    else if (textField == _startTime_TF)
//    {
//        [_startTime_TF becomeFirstResponder];
//        return NO;
//    }
    else if (textField == _Description_tf)
    {
      [textField resignFirstResponder]; 
        return NO;
    }

    [textField resignFirstResponder];
    return YES;
}


- (IBAction)Select_Route_Action:(id)sender {
    
    RoutesVC *cntlr = [self.storyboard instantiateViewControllerWithIdentifier:@"RoutesVC"];
    cntlr.is_FROM_VC_OR_Start_Ride=@"CREATE RIDE";
    [self.navigationController pushViewController:cntlr animated:YES];
}
#pragma mark - google placepiker
- (void)placePicker:(GMSPlacePickerViewController *)viewController didPickPlace:(GMSPlace *)place {
    // Dismiss the place picker, as it cannot dismiss itself.
        _AddressTextString=@"YES";
      if ([_location_str_ride isEqualToString:@"start_location_ride"]) {
       
        if (place.coordinate.latitude==0.0 && place.coordinate.longitude == 0.0) {
            _Placetostart_tf.text =@"";
            }
        else
        {
            latitude=place.coordinate.latitude;
            longitude=place.coordinate.longitude;
            _Placetostart_tf.text = place.name;
        }
//        _Placetostart_tf.text = place.name;
//        latitude=place.coordinate.latitude;
//        longitude=place.coordinate.longitude;
        
        NSLog(@"Place name %@", place.name);
        NSLog(@"Place address %@", place.formattedAddress);
        NSLog(@"Place attributions %@", place.attributions.string);
    }
    else if ([_location_str_ride isEqualToString:@"meeting_location_ride"]){
        if (place.coordinate.latitude==0.0 && place.coordinate.longitude == 0.0) {
            _meetingAddress_TF.text =@"";
        }
        else
        {
            _meetingAddress_TF.text = place.name;
            meeting_latitude=place.coordinate.latitude;
            meeting_longitude=place.coordinate.longitude;
        }

        
       
        NSLog(@"Place meeting name %@", place.name);
        NSLog(@"Place meeting address %@", place.formattedAddress);
        NSLog(@"Place attributions %@", place.attributions.string);
    }
    
 //    latitude=place.coordinate.latitude;
//    longitude=place.coordinate.longitude;
//    NSLog(@"Place name %@", place.name);
//    _Placetostart_tf.text = place.name;
//    NSLog(@"Place address %@", place.formattedAddress);
//    NSLog(@"Place attributions %@", place.attributions.string);
 [viewController dismissViewControllerAnimated:YES completion:nil];
   
}

- (void)placePickerDidCancel:(GMSPlacePickerViewController *)viewController
{
    // Dismiss the place picker, as it cannot dismiss itself.
    [viewController dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"No place selected");
}

- (IBAction)pickPlace:(id)sender {
    
    if (![appDelegate.select_Route_Id_For_CreateAndFreeRide isEqualToString:@""])
    {
        return;
    }
    
    _location_str_ride=@"start_location_ride";
    GMSPlacePickerConfig *config = [[GMSPlacePickerConfig alloc] initWithViewport:nil];
    GMSPlacePickerViewController *placePicker =
    [[GMSPlacePickerViewController alloc] initWithConfig:config];
    placePicker.delegate = self;
    
    [self presentViewController:placePicker animated:YES completion:nil];
    
}
- (IBAction)upload_img_btn:(id)sender {
    
    
//    [self.view endEditing:YES];
//    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
//    
//    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
//        
//        // Cancel button tappped do nothing.
//        
//    }]];
//    
//    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//        
//        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//        picker.delegate = self;
//        picker.allowsEditing = NO;
//        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
//        [self presentViewController:picker animated:YES completion:NULL];
//        
//    }]];
//    
//    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//        
//        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//        picker.delegate = self;
//        picker.allowsEditing = NO;
//        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//        [self presentViewController:picker animated:YES completion:NULL];
//        
//    }]];
//    [self presentViewController:actionSheet animated:YES completion:nil];
//    
    
    
    
}
#pragma mark - Image placepiker Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info

{
//    cameraImage=info[UIImagePickerControllerOriginalImage];
//    cameraImage = image;
//    NSData *webData = UIImagePNGRepresentation(cameraImage);
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString *imgFormat = [NSString stringWithFormat:@"PNG"];
//    NSString *localFilePath = [documentsDirectory stringByAppendingPathComponent:imgFormat];
//    [webData writeToFile:[localFilePath lastPathComponent]atomically:YES];
//    NSLog(@"path is :%@",localFilePath);
//    _uploadimg_lbl.text = localFilePath;
//    NSLog(@"imgpath is:%@",_uploadimg_lbl.text );
    
    
    
    cameraImage = info[UIImagePickerControllerOriginalImage];
    _ride_coverImage.image=cameraImage;
    [picker dismissViewControllerAnimated:YES completion:NULL];
    CGFloat compression = 0.9f;
    CGFloat maxCompression = 0.1f;
    int maxFileSize = 250*1024;
    profileImgData = UIImageJPEGRepresentation(cameraImage,compression);
    while ([profileImgData length] > maxFileSize && compression > maxCompression)
    {
        compression -= 0.1;
        profileImgData = UIImageJPEGRepresentation(cameraImage,compression);
    }
    base64String = [profileImgData base64EncodedStringWithOptions:0];
    
    
    /*
    
    NSURL *refURL = [info valueForKey:UIImagePickerControllerReferenceURL];
    
    // define the block to call when we get the asset based on the url (below)
    ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *imageAsset)
    {
        ALAssetRepresentation *imageRep = [imageAsset defaultRepresentation];
        NSString *filename_str = [imageRep filename];
        _uploadimg_lbl.text = filename_str;
        
        if (_uploadimg_lbl.text.length >0) {
            _upload_img_tf.placeholder = nil;
        }
        
        NSLog(@"[imageRep filename] : %@", [imageRep filename]);
    };
    
    // get the asset library and fetch the asset based on the ref url (pass in block above)
    ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
    [assetslibrary assetForURL:refURL resultBlock:resultblock failureBlock:nil];
    
    if (_uploadimg_lbl.text.length >0) {
        _upload_img_tf.placeholder = nil;
    }
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    CGFloat compression = 0.9f;
    CGFloat maxCompression = 0.1f;
    int maxFileSize = 250*1024;
    profileImgData = UIImageJPEGRepresentation(cameraImage,compression);
    while ([profileImgData length] > maxFileSize && compression > maxCompression)
    {
        compression -= 0.1;
        profileImgData = UIImageJPEGRepresentation(cameraImage,compression);
    }
    base64String = [profileImgData base64EncodedStringWithOptions:0];
     */
    
    
}

- (IBAction)onClick_invite_riders_btn:(id)sender {
  
    SelectFriendsAndGroups*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"SelectFriendsAndGroups"];
    appDelegate.isFrom_RidersVC_Or_FreeRideRoutes=@"InviteRiders";
    controller.free_route_Frnds_groups_id_string=@"YES";
    [self.navigationController pushViewController:controller animated:YES];

}
- (IBAction)onClick_rideImage_btn:(id)sender {
    [self.view endEditing:YES];
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped do nothing.
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = NO;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = NO;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
    

}
- (IBAction)onClick_meetingAddress_btn:(id)sender {
    _location_str_ride=@"meeting_location_ride";
    GMSPlacePickerConfig *config1 = [[GMSPlacePickerConfig alloc] initWithViewport:nil];
    GMSPlacePickerViewController *placePicker1 =
    [[GMSPlacePickerViewController alloc] initWithConfig:config1];
    placePicker1.delegate = self;
    [self presentViewController:placePicker1 animated:YES completion:nil];
}
- (IBAction)onClick_createRide_rideGoinBtn:(id)sender {
    
    self.createRide_scrollLabel.hidden=YES;
    self.createRide_rideGoingBtn.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }
}
@end
