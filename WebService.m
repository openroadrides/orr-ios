//
//  ESWebService.m
//  Emmanuel Sanders
//
//  Created by MacBook Pro on 27/01/16.
//  Copyright © 2016 Emmanuel Sanders. All rights reserved.
//

#import "WebService.h"
#import "AFNetworking.h"
#import <objc/runtime.h>
#import "Constants.h"


@interface WebService ()

@property(nonatomic ,retain)NSString *baseURL;

@end

@implementation WebService


#pragma - BaseURL

-(NSString *)baseURL
{
    return [_baseUrlString stringByAppendingString:@"/"];
}



#pragma mark - Service Calls
#pragma - GetWithPath

-(void)getWithPath:(NSString*)path andParams:(NSDictionary*)params
    withCompletion:(WebServiceSuccess)completion failure:(WebServiceFailure)failure
{
    NSString *requestURL = [self.baseURL stringByAppendingString:path];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:requestURL parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        completion(responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];

}



#pragma - PostWithPath

-(void)postWithPath:(NSString*)path andParams:(NSDictionary*)params
     withCompletion:(WebServiceSuccess)completion failure:(WebServiceFailure)failure
{
    NSString *requestURL = [[self baseURL] stringByAppendingString:path];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/json", nil];
    [manager POST:requestURL parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject)
    {
        completion(responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error)
    {
        failure(error);
    }];
    
}



@end
