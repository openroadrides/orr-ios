//
//  Feedback.h
//  openroadrides
//
//  Created by apple on 15/09/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RateView.h"
#import "RatingBar.h"
#import "APIHelper.h"
#import "AppHelperClass.h"
#import "Constants.h"
#include <sys/types.h>
#include <sys/sysctl.h>
#import "AppDelegate.h"
#import "RideDashboard.h"
#import "CBAutoScrollLabel.h"
@interface Feedback : UIViewController
{
    float Rate;
    RatingBar *subRatingView;
    UIActivityIndicatorView  *activeIndicatore;
    UIView *indicaterview;
}
@property (weak, nonatomic) IBOutlet UIView *Feedback_ratingView;
@property (weak, nonatomic) IBOutlet UIScrollView *feedback_scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentview;
@property (weak, nonatomic) IBOutlet UITextView *feedback_commentTextview;
@property (weak, nonatomic) IBOutlet UIButton *feedback_submitBtn;
- (IBAction)onClick_feedback_submitBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *feedback_rideGoingBtn;
- (IBAction)onClick_feedback_rideGoingBtn:(id)sender;

@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *feedback_scrollLabel;

@end
