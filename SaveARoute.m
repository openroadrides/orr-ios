//
//  SaveARoute.m
//  openroadrides
//
//  Created by apple on 30/05/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "SaveARoute.h"
#import "BIZPopupViewController.h"
#import "BIZPopupViewControllerDelegate.h"
#import "MPCoachMarks.h"

#define EmptyImage @"empty_save_star"
#define RateImage @"full_Start_save"
@interface SaveARoute ()<BIZPopupViewControllerDelegate>
{
    int scrollWidth ;
    UIImageView *img;
    int x_axis;
     UIView *baseView_placesImages;
    NSMutableArray *img_array;
      int index,imgIndex,x;
     NSString *save_A_Route_Status,*almostDone_Staus;
    BIZPopupViewController *popupViewController;
}
@end

@implementation SaveARoute

- (void)viewDidLoad {
    [super viewDidLoad];
    
    index = 1;
    imgIndex = 1;
    x=0;
    self.action_btn_BG_View.layer.borderColor=[[UIColor blackColor]CGColor];
    self.action_btn_BG_View.layer.borderWidth=1.0f;
    
    self.Almost_Don_PubliCPrivate_view.layer.borderColor=[[UIColor blackColor]CGColor];
    self.Almost_Don_PubliCPrivate_view.layer.borderWidth=1.0f;
    
    self.tiltle_Page.font=[UIFont fontWithName:@"Antonio-Bold" size:20];
    [self.Almost_Done_Save_Action.titleLabel setFont:[UIFont fontWithName:@"Antonio-Bold" size:18]];
    [self.save_route_btnOutlet.titleLabel setFont:[UIFont fontWithName:@"Antonio-Bold" size:18]];
    
    self.Almost_Done_Save_Action.layer.cornerRadius=5;
    self.Almost_Done_Save_Action.clipsToBounds=YES;
    
    self.save_route_btnOutlet.layer.cornerRadius=5;
    self.save_route_btnOutlet.clipsToBounds=YES;
    
    
    
    
    

    Rate=0;
    img_array = [[NSMutableArray alloc]init];
    self.imges_Scrole_view.showsHorizontalScrollIndicator = NO;
    add_btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 15, 90,90)];
    [add_btn addTarget:self action:@selector(add_click:) forControlEvents:UIControlEventTouchUpInside];
    //    add_btn.backgroundColor = [UIColor greenColor];
    [add_btn setBackgroundImage:[UIImage imageNamed:plus_icon] forState:UIControlStateNormal];
    [self.imges_Scrole_view addSubview:add_btn];
    appDelegate.pop_UP_out_Side_Click_Hide=@"NO";
    save_A_Route_Status=@"public";
    almostDone_Staus=@"public";
    
    UIToolbar* numberToolbar1 = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 30)];
    numberToolbar1.barStyle = UIBarStyleBlack;
    numberToolbar1.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                             [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(keyboardGoAway:)]];
    [numberToolbar1 sizeToFit];
    self.coments_text_View.inputAccessoryView=numberToolbar1;
    
    starRatingView = [[StarRatingBar alloc]initWithSize:CGSizeMake (260, 50) AndPosition:CGPointMake(0, 5)];
    RBRatingss rating=[starRatingView getcurrentRatings];
    Rate=rating/2.0;
    starRatingView.backgroundColor=[UIColor clearColor];
    [self.Ratings_sub_view addSubview:starRatingView];
    [self.view layoutIfNeeded];
    
    
    if ([_is_From isEqualToString:@"HOME"]) {
        _cancel_Buton.hidden=YES;
    }
    
    //selected route time
    if ([self.seletced_route_title isEqualToString:@""]) {
        
    }
    else{
        _tiltle_Page.text=@"ROUTE FEEDBACK";
        _Route_Title_Label.text=self.seletced_route_title;
        _route_Des_Label.text=_route_des;
        _route_Des_Label.userInteractionEnabled=NO;
        if ([_ride_Owned_BY_ME isEqualToString:@"YES"]) {
            _route_Des_Label.userInteractionEnabled=YES;
        }
        _Route_Title_Label.userInteractionEnabled=NO;
        
        _rating_top_constraint.priority=250;
        _rating_totitle_constraint.priority=750;
        _Route_Title_Label.hidden=YES;
        _name_icon.hidden=YES;
        _route_name_line_.hidden=YES;
        _route_Des_Label.hidden=YES;
        _des_icon.hidden=YES;
        _des_line.hidden=YES;
    }
    
    //Display Annotation
    // Show coach marks
    BOOL coachMarksShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"MPCoachMarksShown_SaveYourRoute"];
    if (coachMarksShown == NO) {
        
        MPCoachMarksShown_isFromSaveARoute = @"YES";
        // Don't show again
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MPCoachMarksShown_SaveYourRoute"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self showAnnotation];
    }
}

#pragma mark - Annotations
-(void)showAnnotation
{
     NSArray *coachMarks;
    
    if ([MPCoachMarksShown_isFromSaveARoute isEqualToString:@"YES"]) {
        MPCoachMarksShown_isFromSaveARoute = @"";
        
        // Setup coach marks
        CGRect coachmark1 = CGRectMake (1, 2, 40, 40);
        CGRect coachmark2 = CGRectMake (_save_route_btnOutlet.frame.origin.x-45, 2, 40, 40);
        
        if ([_is_From isEqualToString:@"HOME"]) {
            // Setup coach marks
            coachMarks = @[
                           @{
                               @"rect": [NSValue valueWithCGRect:coachmark2],
                               @"caption": @"Tap to save the route",
                               @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                               @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                               @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_CENTER]
                               //@"showArrow":[NSNumber numberWithBool:YES]
                               },
                           ];
        }
        else
        {
            // Setup coach marks
            coachMarks = @[
                           @{
                               @"rect": [NSValue valueWithCGRect:coachmark1],
                               @"caption": @"Tap to close the route",
                               @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                               @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                               @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_LEFT]
                               //@"showArrow":[NSNumber numberWithBool:YES]
                               },
                           @{
                               @"rect": [NSValue valueWithCGRect:coachmark2],
                               @"caption": @"Tap to save the route",
                               @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                               @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                               @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_CENTER]
                               //@"showArrow":[NSNumber numberWithBool:YES]
                               },
                           ];

        }
        
        MPCoachMarks *coachMarksView = [[MPCoachMarks alloc] initWithFrame:self.view.bounds coachMarks:coachMarks];
        [_save_Route_View addSubview:coachMarksView];
        [coachMarksView start];
    }
    else
    {
        // Setup coach marks
        CGRect coachmark1 = CGRectMake (_Almost_Done_Save_Action.frame.origin.x-5, 2, 40, 40);
        CGRect coachmark2 = CGRectMake (_images_scroll_baseView.frame.origin.x, _images_scroll_baseView.frame.origin.y+15, 90,90);
        
        // Setup coach marks
        coachMarks = @[
                       @{
                           @"rect": [NSValue valueWithCGRect:coachmark1],
                           @"caption": @"Tap here to complete the ride",
                           @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                           @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                           @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_LEFT]
                           //@"showArrow":[NSNumber numberWithBool:YES]
                           },
                       @{
                           @"rect": [NSValue valueWithCGRect:coachmark2],
                           @"caption": @"Add the ride images here",
                           @"shape": [NSNumber numberWithInteger:SHAPE_SQUARE],
                           @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
                           @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_CENTER]
                           //@"showArrow":[NSNumber numberWithBool:YES]
                           },
                       ];
        MPCoachMarks *coachMarksView = [[MPCoachMarks alloc] initWithFrame:self.view.bounds coachMarks:coachMarks];
        [_Almost_Done_View addSubview:coachMarksView];
        [coachMarksView start];
    }
    
    
    
    
    
    [self.view layoutIfNeeded];
}

-(IBAction)keyboardGoAway:(id)sender
{
 [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)add_click:(UIButton *)sender
{
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped do nothing.
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = NO;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = NO;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}
- (void) imagePickerController:(UIImagePickerController *)picker
         didFinishPickingImage:(UIImage *)image
                   editingInfo:(NSDictionary *)editingInfo
{
    //    img.image = [UIImage imageNamed:[array objectAtIndex:index]];
    UIImage *photoTaken = image;
    if (photoTaken)
    {
        
        if (index <=5)
        {
            if (index == 1) {
                x = 0;
            }
            baseView_placesImages = [[UIView alloc]initWithFrame:CGRectMake(x, 0, 150, 120)];
            baseView_placesImages.tag = 100+index;
            baseView_placesImages.backgroundColor = [UIColor clearColor];
            [self.imges_Scrole_view addSubview:baseView_placesImages];
            
            img = [[UIImageView alloc] initWithFrame:CGRectMake(0,15,90, 90)];
            img.image=photoTaken;
            img.contentMode=UIViewContentModeScaleAspectFit;
            
            //image path from gallery
//            CGFloat compression = 0.9f;
//            CGFloat maxCompression = 0.1f;
//            int maxFileSize = 250*1024;
//            NSData *imageData1 = UIImageJPEGRepresentation(img.image,compression);
          
           NSData *imageData1=[self compressImage:photoTaken];
           
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"MM-dd-HH:mm:ss"];
            NSString *img_pick_time=[dateFormatter stringFromDate:[NSDate date]];
            NSString *imgFormat = [NSString stringWithFormat:@"Ride_image%@.png",img_pick_time];
            
            NSString *localFilePath = [documentsDirectory stringByAppendingPathComponent:imgFormat];
            [imageData1 writeToFile:localFilePath atomically:YES];
            [img_array addObject:localFilePath];

            imgIndex++;
            
            x = x+110;
            
            [baseView_placesImages addSubview:img];
            
            subtract_btn = [[UIButton alloc]initWithFrame:CGRectMake(img.frame.size.width-12.5, 0, 25, 25)];
//            [subtract_btn setTitle:@"X" forState:UIControlStateNormal];
//            [subtract_btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
             [subtract_btn setBackgroundImage:[UIImage imageNamed:@"wrong_icon.png"] forState:UIControlStateNormal];
            [subtract_btn.titleLabel setFont:[UIFont systemFontOfSize:20]];
            [subtract_btn addTarget:self action:@selector(sub_click:) forControlEvents:UIControlEventTouchUpInside];
            subtract_btn.tag=index;
            [baseView_placesImages addSubview:subtract_btn];
            
            
            add_btn.frame = CGRectMake(baseView_placesImages.frame.origin.x+baseView_placesImages.frame.size.width-40, img.frame.origin.y, 90, 90);
            self.imges_Scrole_view.contentSize = CGSizeMake(add_btn.frame.origin.x+add_btn.frame.size.width, 120);
            
            if (index == 5) {
                self.imges_Scrole_view.contentSize = CGSizeMake(add_btn.frame.origin.x-5, 120);
                add_btn.hidden = YES;
            }
            
            index++;
            
        }
        
        
    }
    else
    {
        NSLog(@"Selected photo is NULL");
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}
-(NSData *)compressImage:(UIImage *)image{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
//    float maxHeight = 1130.0f;
    float maxHeight = 816.0f;
    float maxWidth = 640.0f;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.9;//50 percent compression
    NSData *imageData = UIImageJPEGRepresentation(image, compressionQuality);
    while (actualHeight > maxHeight || actualWidth > maxWidth){
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
        CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
        UIGraphicsBeginImageContext(rect.size);
        [image drawInRect:rect];
        UIImage *img_is = UIGraphicsGetImageFromCurrentImageContext();
       imageData = UIImageJPEGRepresentation(img_is, compressionQuality);
        UIGraphicsEndImageContext();

    }
//        else{
//        actualHeight = maxHeight;
//        actualWidth = maxWidth;
//        compressionQuality = 1;
//    }
    
    
    return imageData;
}

-(IBAction)sub_click:(UIButton *)sender
{
    
    UIView *remove_baseView_placesImages = (UIView *)[self.imges_Scrole_view viewWithTag:sender.tag+100];
    [remove_baseView_placesImages removeFromSuperview];
    
    int xCoordinate = remove_baseView_placesImages.frame.origin.x;
    
    if (sender.tag < index-1)
    {
        for (int i = (int)sender.tag+1; i<= index-1 ; i++) {
            
            UIView *moveForward_placesImages = (UIView *)[self.imges_Scrole_view viewWithTag:i+100];
            moveForward_placesImages.frame = CGRectMake(xCoordinate, 0, 150, self.imges_Scrole_view.frame.size.height);
            UIButton *buttonTag = (UIButton *)[moveForward_placesImages viewWithTag:(int)moveForward_placesImages.tag-100];
            buttonTag.tag = buttonTag.tag-1;
            moveForward_placesImages.tag = 100+i-1;
            xCoordinate = xCoordinate+110;
            
        }
        
        index --;
        
        x = xCoordinate;
        
        add_btn.hidden = NO;
        add_btn.frame = CGRectMake(xCoordinate, img.frame.origin.y, 90, 90);
        self.imges_Scrole_view.contentSize = CGSizeMake(add_btn.frame.origin.x + add_btn.frame.size.width, 120);
        
    }
    else
    {
        x = x-110;
        index = (int)sender.tag;
        
        add_btn.frame = CGRectMake(remove_baseView_placesImages.frame.origin.x, img.frame.origin.y, 90, 90);
        self.imges_Scrole_view.contentSize = CGSizeMake(add_btn.frame.origin.x + add_btn.frame.size.width, 120);
        
    }
    
    [img_array removeObjectAtIndex:(int)sender.tag-1];
    
    if (sender.tag == 5) {
        
        add_btn.hidden = NO;
        add_btn.frame = CGRectMake(remove_baseView_placesImages.frame.origin.x, img.frame.origin.y, 90, 90);
        self.imges_Scrole_view.contentSize = CGSizeMake(add_btn.frame.origin.x + add_btn.frame.size.width, 120);
    }
    
}

- (IBAction)Save_Route_Action:(id)sender {
    
    
    [self.view endEditing:YES];
    _Route_Title_Label.text=[_Route_Title_Label.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([_Route_Title_Label.text isEqualToString:@""]) {
        [self showAlert:@"Please enter route title."];
        return;
    }
    RBRatingss rating=[starRatingView getcurrentRatings];
    Rate=rating/2.0;
    
   
    appDelegate.Save_Route_Data_Array=[[NSMutableArray alloc]init];
    
    NSMutableDictionary *data=[[NSMutableDictionary alloc]init];
    [data setValue:[NSString stringWithFormat:@"%.1f",Rate] forKey:@"Rating"];
    [data setValue:_coments_text_View.text forKey:@"Comments"];
    [data setValue:_Route_Title_Label.text forKey:@"Title"];
    [data setValue:_route_Des_Label.text forKey:@"Des"];
    [data setValue:save_A_Route_Status forKey:@"RouteStatus"];
    [appDelegate.Save_Route_Data_Array addObject:data];
    
    if ([_is_From isEqualToString:@"HOME"]) {
        
    }
    else
    {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Route_Sucsess" object:self];
    }

    
    self.view.frame=CGRectMake(20, 90, self.view.frame.size.width,350);
    
    _Almost_Title.text=appDelegate.FreeRide_Name;
    _almost_Des.text=_ride_description;
    
    if ([_ride_Owned_BY_ME isEqualToString:@"YES"]) {
        appDelegate.FreeRide_Name=@"";
        self.save_Route_View.hidden=YES;
        
    }
    else{
        _Almost_Title.userInteractionEnabled=NO;
        _almost_Des.userInteractionEnabled=NO;
        appDelegate.FreeRide_Name=@"";
        self.save_Route_View.hidden=YES;
        
    }
    
    //Display Annotation
    // Show coach marks
    BOOL coachMarksShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"MPCoachMarksShown_AlmostDone"];
    if (coachMarksShown == NO) {
        // Don't show again
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MPCoachMarksShown_AlmostDone"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self showAnnotation];
    }
    

 }
- (void)textViewDidBeginEditing:(UITextView *)textView{
    _comments_Label.text=@"";
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@""]) {
        _comments_Label.text=@"Comments";
    }
}

- (IBAction)public_Action:(id)sender {
    if ([self.seletced_route_title isEqualToString:@""]) {//seleted route
        
    }
    else{
        return;
    }
    self.save_Route_Public_View.backgroundColor=[UIColor whiteColor];
    self.save_Route_Private_V.backgroundColor=[UIColor clearColor];
    //self.save_route_public_lbl.textColor=[UIColor whiteColor];
    //self.save_route_private_lbl.textColor=[UIColor blackColor];
    //self.public_icon_image.image=[UIImage imageNamed:public_white_icon];
    //self.private_icon_image.image=[UIImage imageNamed:private_icon];
    save_A_Route_Status=@"public";
    
}

- (IBAction)private_Action:(id)sender {
    if ([self.seletced_route_title isEqualToString:@""]) {//seleted route
        
    }
    else{
        return;
    }
    self.save_Route_Public_View.backgroundColor=[UIColor clearColor];
    self.save_Route_Private_V.backgroundColor=[UIColor whiteColor];
    //self.save_route_public_lbl.textColor=[UIColor blackColor];
    //self.save_route_private_lbl.textColor=[UIColor whiteColor];
    //self.public_icon_image.image=[UIImage imageNamed:public_icon];
    //self.private_icon_image.image=[UIImage imageNamed:private_white_icon];
    save_A_Route_Status=@"private";
}

//- (IBAction)Rating_Action:(UIButton *)sender {
//    
//    if (sender.tag==1)
//    {
//        [self.first_rating_Img setImage:[UIImage imageNamed:RateImage]];
//        [self.Second_rate_Img setImage:[UIImage imageNamed:EmptyImage]];
//        [self.third_rate_img setImage:[UIImage imageNamed:EmptyImage]];
//        [self.Fourth_rate_Img setImage:[UIImage imageNamed:EmptyImage]];
//        [self.Fifth_rate_img setImage:[UIImage imageNamed:EmptyImage]];
//    }
//   else if (sender.tag==2)
//   {
//       [self.first_rating_Img setImage:[UIImage imageNamed:RateImage]];
//       [self.Second_rate_Img setImage:[UIImage imageNamed:RateImage]];
//       [self.third_rate_img setImage:[UIImage imageNamed:EmptyImage]];
//       [self.Fourth_rate_Img setImage:[UIImage imageNamed:EmptyImage]];
//       [self.Fifth_rate_img setImage:[UIImage imageNamed:EmptyImage]];
//    }
//   else if (sender.tag==3)
//   {
//       [self.first_rating_Img setImage:[UIImage imageNamed:RateImage]];
//       [self.Second_rate_Img setImage:[UIImage imageNamed:RateImage]];
//       [self.third_rate_img setImage:[UIImage imageNamed:RateImage]];
//       [self.Fourth_rate_Img setImage:[UIImage imageNamed:EmptyImage]];
//       [self.Fifth_rate_img setImage:[UIImage imageNamed:EmptyImage]];
//   }
//   else if (sender.tag==4)
//   {
//       [self.first_rating_Img setImage:[UIImage imageNamed:RateImage]];
//       [self.Second_rate_Img setImage:[UIImage imageNamed:RateImage]];
//       [self.third_rate_img setImage:[UIImage imageNamed:RateImage]];
//       [self.Fourth_rate_Img setImage:[UIImage imageNamed:RateImage]];
//       [self.Fifth_rate_img setImage:[UIImage imageNamed:EmptyImage]];
//   }
//   else if (sender.tag==5)
//   {
//       [self.first_rating_Img setImage:[UIImage imageNamed:RateImage]];
//       [self.Second_rate_Img setImage:[UIImage imageNamed:RateImage]];
//       [self.third_rate_img setImage:[UIImage imageNamed:RateImage]];
//       [self.Fourth_rate_Img setImage:[UIImage imageNamed:RateImage]];
//       [self.Fifth_rate_img setImage:[UIImage imageNamed:RateImage]];
//   }
//    Rate=sender.tag;
//}
- (IBAction)Almost_Public_Action:(id)sender {
    
    self.public_bg_view.backgroundColor=[UIColor blackColor];
    self.private_Bg_View.backgroundColor=[UIColor clearColor];
    self.Almost_Public_Labl.textColor=[UIColor whiteColor];
    self.almost_Private_Label.textColor=[UIColor blackColor];
    almostDone_Staus=@"public";
}

- (IBAction)Almost_private_Action:(id)sender {
    if ([self.seletced_route_title isEqualToString:@""]) {
        
    }
    else{
        return;
    }
    self.public_bg_view.backgroundColor=[UIColor clearColor];
    self.private_Bg_View.backgroundColor=[UIColor blackColor];
    self.almost_Private_Label.textColor=[UIColor whiteColor];
    self.Almost_Public_Labl.textColor=[UIColor blackColor];
    almostDone_Staus=@"private";
    
}

- (IBAction)Almost_Don_Save_Action:(id)sender {
    [self end_Ride_Service];
}
-(void)end_Ride_Service{
    [self.view endEditing:YES];
//    if (![SHARED_HELPER checkIfisInternetAvailable]) {
//        [self showAlert:Nonetwork];
//        return;
//    }
    _Almost_Title.text=[_Almost_Title.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if ([_Almost_Title.text isEqualToString:@""]) {
        [self showAlert:@"Please enter ride name."];
        return;
    }
   
    NSMutableDictionary *data=[[NSMutableDictionary alloc]init];
    [data setValue:almostDone_Staus forKey:@"RideStatus"];
    [data setValue:_Almost_Title.text forKey:@"RideTitle"];
    [data setValue:_almost_Des.text forKey:@"RideDescriptionDes"];
    
    [appDelegate.Save_Route_Data_Array addObject:data];
    [appDelegate.Save_Route_Data_Array addObject:img_array];
    appDelegate.pop_UP_out_Side_Click_Hide=@"YES";
    
    popupViewController = [[BIZPopupViewController alloc]init];
    popupViewController.delegate=self;
    [self dismissViewControllerAnimated:YES completion:nil];
    if ([_is_From isEqualToString:@"HOME"]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Save_Ride_From_Home" object:self];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SaveARoute" object:self];
    }
    // [img_array removeAllObjects];
}

-(void)showAlert:(NSString *)message
{
    
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:message  preferredStyle:UIAlertControllerStyleActionSheet];
        UIView  *firstSubview = alert.view.subviews.firstObject;
        firstSubview.backgroundColor = APP_YELLOW_COLOR;
        UIView *alertContentView = firstSubview.subviews.firstObject;
        alertContentView.backgroundColor = APP_YELLOW_COLOR;
        alertContentView.tintColor = [UIColor blueColor];
        for (UIView *subSubView in alertContentView.subviews) {
            
            subSubView.backgroundColor = APP_YELLOW_COLOR;
        }
        // [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alert animated:YES completion:nil];
        [self presentViewController:alert animated:YES completion:nil];
        
        int duration = 2; // duration in seconds
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            
            [alert dismissViewControllerAnimated:YES completion:nil];
            
        });
    

}

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if (textField==_Route_Title_Label) {
        [self.route_Des_Label becomeFirstResponder];
        return NO;
    }
    else if (textField==_Almost_Title){
        [self.almost_Des becomeFirstResponder];
        return NO;
    }
    else{
        [textField resignFirstResponder];
        return YES;
    }
    
}
- (IBAction)Save_Route_Cancel:(id)sender {
    
   [self dismissViewControllerAnimated:YES completion:nil];
    
}
@end
