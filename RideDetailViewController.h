//
//  RideDetailViewController.h
//  openroadrides
//
//  Created by apple on 21/06/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "RideDetailTableViewCell.h"
#import <GoogleMaps/GoogleMaps.h>
#import "CreaterideViewController.h"
#import "RidesController.h"
#import "EditRideController.h"
#import "RideMapViewController.h"
#import "CBAutoScrollLabel.h"
@interface RideDetailViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,GMSMapViewDelegate>{
    
    UIActivityIndicatorView *activeIndicatore;
    UIView *indicaterview;
    GMSMapView *ride_mapView;
    NSString *gpx_strng;
     NSMutableArray *add_places,*tracks_array,*way_array;
  
    GMSMutablePath *line_path;
    NSString *start_lat_long,*end_lat_long;
    GMSPolyline *polyline;
    double x,z;
    int mapZoom;

    NSString *edit_ride_name,*edit_placeToStart,*edit_ride_date,*edit_ride_time,*edit_meeting_address,*edit_meeting_date,*edit_meeting_time,*edit_ride_desciption,*edit_ride_image_url,*edit_ride_frndIDs,*edit_ride_gropuIDS,*ride_id_str,*route_id_str,*edit_ride_type_string;
    double edit_ride_start_latitude,edit_ride_start_longtitude,edit_ride_meeting_lat,edit_ride_meeting_log;
    
    
    NSString *encoded_path;
}
@property (strong, nonatomic) NSString *is_From;
@property (weak, nonatomic) IBOutlet UIView *delete_btn_view;
@property (weak, nonatomic) IBOutlet UITableView *ridedetail_tbl_vw;
@property (weak, nonatomic) IBOutlet UIView *main_view;
@property (weak, nonatomic) IBOutlet UIScrollView *scrl_view;
@property (weak, nonatomic) IBOutlet UIView *main_content_view;
@property (weak, nonatomic) IBOutlet MKMapView *map_view;
@property (weak, nonatomic) IBOutlet UILabel *title_name;
@property (weak, nonatomic) IBOutlet UILabel *ride_name;
@property (weak, nonatomic) IBOutlet UILabel *date_lbl;
@property (weak, nonatomic) IBOutlet UILabel *rider_count_lbl;
@property (weak, nonatomic) IBOutlet UILabel *avg_speed_lbl;
@property (weak, nonatomic) IBOutlet UILabel *max_speed_lbl;
@property (weak, nonatomic) IBOutlet UILabel *total_distance_lbl;
@property (weak, nonatomic) IBOutlet UILabel *ridetime_lbl;
@property (weak, nonatomic) IBOutlet UILabel *totaltime_lbl;
@property (weak, nonatomic) IBOutlet UILabel *elevation_lbl;
- (IBAction)add_action:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *ride_description_lbl;
- (IBAction)delete_ride_action:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *place_name_lbl;
@property (weak, nonatomic) IBOutlet UILabel *wether_lbl;
@property (weak, nonatomic) IBOutlet UILabel *condition_lbl;
@property (strong, nonatomic) NSString *scheduled_fullFilled_rideID_string,*ride_owner_id;
@property (strong, nonatomic) NSString *scheduled_fullFilled_user_ride_data_string;
@property (weak, nonatomic) IBOutlet UIView *mapShowing_view;
@property (strong, nonatomic) NSString *edit_frnds_string,*edit_groups_string;
@property (weak, nonatomic) IBOutlet UILabel *ride_route_name_label;
@property (weak, nonatomic) IBOutlet UILabel *minimu_elevation_label;

@property (weak, nonatomic) IBOutlet UIButton *rideDetail_rideGoingBtn;

- (IBAction)onClick_rideDetail_rideGoingBtn:(id)sender;

@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *rideDetail_scrollLabel;



@end


















