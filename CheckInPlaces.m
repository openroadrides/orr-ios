//
//  CheckInPlaces.m
//  openroadrides
//
//  Created by SrkIosEbiz on 05/06/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "CheckInPlaces.h"

#import "Checkin_Cell.h"
#import "Constants.h"

@interface CheckInPlaces ()

@end

@implementation CheckInPlaces
#pragma mark ViewStarting
- (void)viewDidLoad {
        [super viewDidLoad];
    
    [self.places_Table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.riders_Display_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        _placesClient = [GMSPlacesClient sharedClient];
        locationManager = [[CLLocationManager alloc] init];
        [locationManager requestAlwaysAuthorization];
        locationManager.delegate=self;
        Current_Lat=locationManager.location.coordinate.latitude;
        Current_Long=locationManager.location.coordinate.longitude;
        current_location = [[CLLocation alloc] initWithLatitude:Current_Lat longitude:Current_Long];
    
        self.places_Table.backgroundColor=[UIColor blackColor];
        self.places_Table.hidden=YES;
        near_Places_Array=[[NSMutableArray alloc]init];
        
        indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [indicator setCenter:self.view.center];
        indicator.color = APP_YELLOW_COLOR;
        indicator.hidesWhenStopped = TRUE;
        [self.view addSubview:indicator];
        [indicator startAnimating];
    
        [self get_Nearby_Places];
    
    
    
    [self.title_TF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.Des_TF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    self.Check_In_View.hidden=YES;
    // Do any additional setup after loading the view.
}
- (IBAction)key_Board_Go:(id)sender{
    
    [self.view endEditing:YES];
    
}

- (IBAction)friend_Check_Box_Action:(UIButton *)sender {
    NSString *rider_id=[[riders_list_array objectAtIndex:sender.tag] valueForKey:@"user_id"];
    if ([selected_riders_arr containsObject:rider_id]) {
        [selected_riders_arr removeObject:rider_id];
        [_riders_Display_table reloadData];
    }
    else{
        [selected_riders_arr addObject:rider_id];
        [_riders_Display_table reloadData];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark NearBy Service
-(void)get_Nearby_Places{
    if ([SHARED_HELPER checkIfisInternetAvailable]) {
        
    }
    else{
        [SHARED_HELPER showAlert:Nonetwork];
        
        int duration = 2; // duration in seconds
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self.navigationController popViewControllerAnimated:YES];
        });

        return;
    }
    
    [_placesClient currentPlaceWithCallback:^(GMSPlaceLikelihoodList *likelihoodList, NSError *error) {
        if (error != nil) {
            NSLog(@"Current Place error %@", [error localizedDescription]);
            return;
        }
        
        for (GMSPlaceLikelihood *likelihood in likelihoodList.likelihoods) {
            GMSPlace* place = likelihood.place;
            double place_Latitude=place.coordinate.latitude;
            double place_Long=place.coordinate.longitude;
            CLLocation   *Place_Loc = [[CLLocation alloc] initWithLatitude:place_Latitude longitude:place_Long];
            int dist=[current_location distanceFromLocation:Place_Loc];
            
            if (dist<1609) {
            NSMutableDictionary *place_Dic=[[NSMutableDictionary alloc]init];
            [place_Dic setObject:[NSString stringWithFormat:@"%d",dist] forKey:@"Distance_From_Location"];
            [place_Dic setObject:place.name forKey:@"Place_name"];
            [place_Dic setObject:[NSNumber numberWithDouble:place_Latitude] forKey:@"place_Latitude"];
            [place_Dic setObject:[NSNumber numberWithDouble:place_Long] forKey:@"place_Long"];
            [place_Dic setObject:place.placeID forKey:@"PlaceID"];
            [place_Dic setObject:place.formattedAddress forKey:@"Place_FullAdress"];
            
            [near_Places_Array addObject:place_Dic];
            NSLog(@"Current Place name %@ at likelihood %g", place.name, likelihood.likelihood);
            NSLog(@"Current Place attributions %@", place.attributions);
            }
        }
        [self.places_Table reloadData];
         self.places_Table.hidden=NO;
        [indicator stopAnimating];
        
        [self get_friend_riders_Service];
    }];
    
}
-(void)get_friend_riders_Service{
    
    if ([_ride_id_is isEqualToString:@""]) {
        return;
    }
    else{
        selected_riders_arr=[[NSMutableArray alloc]init];
        [self get_rider_friends_api];
    }
}

-(void)get_rider_friends_api{
   
    [SHARED_API get_Riders_List:_ride_id_is withSuccess:^(NSDictionary *response) {
        if ([[response valueForKey:STATUS]isEqualToString:SUCCESS]) {
            riders_list_array=[[NSMutableArray alloc]init];
            NSArray *data=[response valueForKey:@"riders"];
            [riders_list_array addObjectsFromArray:data];
            if (riders_list_array.count>0) {
                BOOL is_in=[[riders_list_array valueForKey:@"user_id"] indexOfObject:[NSString stringWithFormat:@"%@",[Defaults valueForKey:User_ID]]];
                
                if (is_in) {
                NSUInteger indexvalue = [[riders_list_array valueForKey:@"user_id"] indexOfObject:[Defaults valueForKey:User_ID]];
                    [riders_list_array  removeObjectAtIndex:indexvalue];
                }
                if (riders_list_array.count>0) {
                       [_riders_Display_table reloadData];
                }
            }
        }
        else{
            
        }
    } onfailure:^(NSError *theError) {
        
    }];
}
#pragma mark TableViewDelegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==_places_Table) {
    if (near_Places_Array.count>0) {
        NO_DATA.hidden=YES;
        return  near_Places_Array.count;
    }
    else
    {
        NO_DATA      = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        NO_DATA.numberOfLines=2;
        NO_DATA.textColor        = [UIColor whiteColor];
        [NO_DATA setFont:[UIFont fontWithName:@"Antonio-Bold" size:21]];
        NO_DATA.textAlignment    = NSTextAlignmentCenter;
        NO_DATA.text  = @"There is no places to check-in.";
        tableView.backgroundView = NO_DATA;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    return 0;
    }
    }
    else if(tableView==_riders_Display_table){
        return riders_list_array.count;
    }
    else{
        return 0;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
     if (tableView==_places_Table) {
         return 65;
     }
     else{
         return 110;
     }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_places_Table) {
        
    static NSString *simpleTableIdentifier = @"Checkin_Cell";
    Checkin_Cell *cell = (Checkin_Cell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
   
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"Checkin_Cell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.Place_Name.text=[[near_Places_Array objectAtIndex:indexPath.row] valueForKey:@"Place_name"];
    cell.distance.text=[NSString stringWithFormat:@"%@M",[[near_Places_Array objectAtIndex:indexPath.row] valueForKey:@"Distance_From_Location"]];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    
    return cell;
    }
    else{

        
//        address = "";
//        city = "";
//        name = "Janelle William";
//        "profile_image" = "https://s3-us-west-2.amazonaws.com/openroadrides/67/user_images/8tz0sQSGEXGj0v3Y2bxMw7sdu7d0rKWo.jpeg";
//        state = "";
//        "user_id" = 67;
//        zipcode = 0;
        
        
        tableView.allowsSelection=NO;
        static NSString *simpleTableIdentifier = @"check_in_riders_list";
        check_in_riders_list *cell = (check_in_riders_list *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"check_in_riders_list" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        if (indexPath.row%2==0) {
            cell.bg_View.backgroundColor=ROUTES_CELL_BG_COLOUR1;
        }
        else
            cell.bg_View.backgroundColor=ROUTES_CELL_BG_COLOUR2;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.check_Box_Outlet.tag=indexPath.row;
        
        NSString *rider_id=[[riders_list_array objectAtIndex:indexPath.row] valueForKey:@"user_id"];
        if ([selected_riders_arr containsObject:rider_id]) {
            [cell.check_Box_Outlet setBackgroundImage:[UIImage imageNamed:@"checkbox_fill"] forState:UIControlStateNormal];
           
        }
        else{
             [cell.check_Box_Outlet setBackgroundImage:[UIImage imageNamed:@"checkbox_empty"] forState:UIControlStateNormal];
        }
        
        
     //image
        NSString *profile_image_string1 = [NSString stringWithFormat:@"%@", [[riders_list_array objectAtIndex:indexPath.row] objectForKey:@"profile_image"]];
        NSURL*url=[NSURL URLWithString:profile_image_string1];
        [cell.user_Profile_Pic sd_setImageWithURL:url
                                        placeholderImage:[UIImage imageNamed:@"unfriend"]
                                                 options:SDWebImageRefreshCached];
        
        
    //adress
        NSString * areaName=[NSString stringWithFormat:@"%@",[[riders_list_array objectAtIndex:indexPath.row] objectForKey:@"address"]];
        NSString * cityName=[NSString stringWithFormat:@"%@",[[riders_list_array objectAtIndex:indexPath.row] objectForKey:@"city"]];
        NSString * stateName=[NSString stringWithFormat:@"%@",[[riders_list_array objectAtIndex:indexPath.row] objectForKey:@"state"]];
        NSString *zipCode=[NSString stringWithFormat:@"%@",[[riders_list_array objectAtIndex:indexPath.row] objectForKey:@"zipcode"]];
        
        NSMutableArray *empaty_array=[[NSMutableArray alloc]init];
        [empaty_array addObject:@"<null>"];
        [empaty_array addObject:@"null"];
        [empaty_array addObject:@""];
        [empaty_array addObject:@"(null)"];
        [empaty_array addObject:@"0"];
        
        NSMutableArray *Data_array=[[NSMutableArray alloc]init];
        if ([empaty_array containsObject:areaName]) {
            
        }
        else{
            [Data_array addObject:areaName];
        }
        if ([empaty_array containsObject:cityName]) {
            
        }
        else{
            [Data_array addObject:cityName];
        }
        
        if ([empaty_array containsObject:stateName]) {
            
        }
        else{
            [Data_array addObject:stateName];
        }
        
        if ([empaty_array containsObject:zipCode]) {
            
        }
        else{
            [Data_array addObject:zipCode];
        }
        
        NSMutableString *add_data=[[NSMutableString alloc]init];
        if (Data_array.count>0) {
             cell.adress_Icon.image=[UIImage imageNamed:@"check_in_placeholder"];
            for (int i=0; i<Data_array.count; i++) {
                NSString *adress=[Data_array objectAtIndex:i];
                if (i==0) {
                    [add_data appendFormat:@"%@", [NSString stringWithFormat:@"%@",adress]];
                }
                else
                    [add_data appendFormat:@"%@", [NSString stringWithFormat:@",%@",adress]];
            }
            cell.friend_Adress_Label.text=add_data;
        }
        else{
             cell.adress_Icon.image=[UIImage imageNamed:@""];
            cell.friend_Adress_Label.text=@"";
        }
//user name
        cell.user_Name_label.text=[[riders_list_array objectAtIndex:indexPath.row ]valueForKey:@"name"];
 
        return cell;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    self.title_TF .text=@"";
    self.Des_TF.text=@"";

    self.place_adress_TF.text=[[near_Places_Array objectAtIndex:indexPath.row] valueForKey:@"Place_name"];
    
    self.checkin_place_Dic=[near_Places_Array objectAtIndex:indexPath.row];
    
    index = 1;
    imgIndex = 1;
    x=0;
    
    //For_Check_in
    //Images_Scro;e_View
   
    img_array = [[NSMutableArray alloc]init];
    [self.images_Scrole_View removeFromSuperview];
    self.images_Scrole_View=[[UIScrollView alloc]initWithFrame:CGRectMake(10, 0, SCREEN_WIDTH-20, 120)];
    self.images_Scrole_View.showsHorizontalScrollIndicator = NO;
    add_btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 15, 100,100)];
    [add_btn addTarget:self action:@selector(add_click:) forControlEvents:UIControlEventTouchUpInside];
    //    add_btn.backgroundColor = [UIColor greenColor];
    [add_btn setBackgroundImage:[UIImage imageNamed:@"white_plus"] forState:UIControlStateNormal];
    [self.images_Scrole_View addSubview:add_btn];
    [self.images_View addSubview:self.images_Scrole_View];
    [self.view layoutIfNeeded];
   
    
    
#pragma mark RandonNumber
    checkin_random_number_string=appDelegate.ride_Random_Number;
    NSLog(@"random Number %@",checkin_random_number_string);
    
    
    [UIView transitionWithView:self.Check_In_View
                      duration:0.2
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void){
                        self.select_Place_view.hidden=YES;
                        self.Check_In_View.hidden=NO;
                       
                    }
                    completion:nil];
}
- (IBAction)Back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    //[self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)Check_In_Cancel:(id)sender {
    [self.view endEditing:YES];
    [UIView transitionWithView:self.select_Place_view
                      duration:0.2
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void){
                        self.Check_In_View.hidden=YES;
                        self.select_Place_view.hidden=NO;
                    }
                    completion:nil];
    
}

-(void)Checkin_Service_Call{
    
    if ([SHARED_HELPER checkIfisInternetAvailable]) {
        
    }
    else{
        [self showAlert:Nonetwork];
        return;
    }
    
    self.view.userInteractionEnabled=NO;
    NSMutableDictionary *ride_Dict = [NSMutableDictionary new];
    [ride_Dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
    [ride_Dict setObject:self.Des_TF.text forKey:@"checkin_description"];
    [ride_Dict setObject:self.title_TF.text forKey:@"checkin_title"];
    [ride_Dict setObject:[self.checkin_place_Dic valueForKey:@"Place_name"] forKey:@"checkin_address"];
    [ride_Dict setObject:[NSString stringWithFormat:@"%@",[self.checkin_place_Dic valueForKey:@"place_Long"]] forKey:@"checkin_longitude"];
    [ride_Dict setObject:[NSString stringWithFormat:@"%@",[self.checkin_place_Dic valueForKey:@"place_Latitude"]] forKey:@"checkin_latitude"];
    [ride_Dict setObject:img_array forKey:@"checkin_images"];
    [ride_Dict setObject:checkin_random_number_string forKey:@"random_number"];
    [ride_Dict setObject:_ride_id_is forKey:@"ride_id"];
    [ride_Dict setObject:[SHARED_HELPER rideType] forKey:@"ride_type"];
    if (selected_riders_arr.count>0) {
        [ride_Dict setObject:selected_riders_arr forKey:@"other_riders"];
    }
    else{
        selected_riders_arr=[[NSMutableArray alloc]init];
        [ride_Dict setObject:selected_riders_arr forKey:@"other_riders"];
    }
    
    NSLog(@"Add Check In Dict %@",ride_Dict);
    [SHARED_API store_ride_checkin:ride_Dict withSuccess:^(NSDictionary *response)
     {
         appDelegate.check_in_Clicked=@"YES";
         appDelegate.check_in_Lat=[self.checkin_place_Dic valueForKey:@"place_Latitude"];
         appDelegate.check_in_Long=[self.checkin_place_Dic valueForKey:@"place_Long"];
         appDelegate.checkin_marker_title=self.title_TF.text;
         
         NSLog(@"Add Check In Response %@",response);
          self.view.userInteractionEnabled=YES;
         [self.navigationController popViewControllerAnimated:YES];
        // [self dismissViewControllerAnimated:YES completion:nil];
         [indicator stopAnimating];
     } onfailure:^(NSError *theError)
     {
         self.view.userInteractionEnabled=YES;
         [self showAlert:ServiceFail];
         [indicator stopAnimating];
     }];
}

-(void)add_click:(UIButton *)sender
{
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped do nothing.
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
}
- (void) imagePickerController:(UIImagePickerController *)picker
         didFinishPickingImage:(UIImage *)image
                   editingInfo:(NSDictionary *)editingInfo
{
    //    img.image = [UIImage imageNamed:[array objectAtIndex:index]];
    UIImage *photoTaken = image;
    if (photoTaken)
    {
        
        if (index <=5)
        {
            if (index == 1) {
                x = 0;
            }
            baseView_placesImages = [[UIView alloc]initWithFrame:CGRectMake(x, 0, 100, 120)];
            baseView_placesImages.tag = 100+index;
            baseView_placesImages.backgroundColor = [UIColor clearColor];
            [self.images_Scrole_View addSubview:baseView_placesImages];
            
            img = [[UIImageView alloc] initWithFrame:CGRectMake(0,10,100, 100)];
            img.contentMode=UIViewContentModeScaleAspectFit;
            img.image=photoTaken;
            
            
            //image path from gallery
//            CGFloat compression = 0.9f;
//            CGFloat maxCompression = 0.1f;
//            int maxFileSize = 250*1024;
//            NSData *imageData1 = UIImageJPEGRepresentation(img.image,compression);
            
//            while ([imageData1 length] > maxFileSize && compression > maxCompression)
//            {
//                compression -= 0.1;
//                imageData1 = UIImageJPEGRepresentation(img.image,compression);
//            }
            NSData *imageData1=[self compressImage:photoTaken];
            NSString *  base64String = [imageData1 base64EncodedStringWithOptions:0];
            [img_array addObject:base64String];
            NSLog(@"images array is %@",img_array);
            
            imgIndex++;
            
            x = x+110;
            
            
            
            [baseView_placesImages addSubview:img];
            
            subtract_btn = [[UIButton alloc]initWithFrame:CGRectMake(img.frame.size.width-12.5, 0, 30, 30)];
//            [subtract_btn setTitle:@"X" forState:UIControlStateNormal];
//            [subtract_btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
             [subtract_btn setBackgroundImage:[UIImage imageNamed:@"wrong_icon.png"] forState:UIControlStateNormal];
            [subtract_btn.titleLabel setFont:[UIFont systemFontOfSize:20]];
            [subtract_btn addTarget:self action:@selector(sub_click:) forControlEvents:UIControlEventTouchUpInside];
            subtract_btn.tag=index;
            [baseView_placesImages addSubview:subtract_btn];
            
            
            add_btn.frame = CGRectMake(baseView_placesImages.frame.origin.x+baseView_placesImages.frame.size.width+10, img.frame.origin.y, 100, 100);
            self.images_Scrole_View.contentSize = CGSizeMake(add_btn.frame.origin.x+add_btn.frame.size.width, 120);
            
            if (index == 5) {
                self.images_Scrole_View.contentSize = CGSizeMake(add_btn.frame.origin.x-5, 120);
                add_btn.hidden = YES;
            }
            
            index++;
            
        }
        
        
    }
    else
    {
        NSLog(@"Selected photo is NULL");
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}
-(NSData *)compressImage:(UIImage *)image{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    //    float maxHeight = 1130.0f;
    float maxHeight = 816.0f;
    float maxWidth = 640.0f;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.9;//50 percent compression
    NSData *imageData = UIImageJPEGRepresentation(image, compressionQuality);
    while (actualHeight > maxHeight || actualWidth > maxWidth){
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
        CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
        UIGraphicsBeginImageContext(rect.size);
        [image drawInRect:rect];
        UIImage *img_is = UIGraphicsGetImageFromCurrentImageContext();
        imageData = UIImageJPEGRepresentation(img_is, compressionQuality);
        UIGraphicsEndImageContext();
        
    }
    //        else{
    //        actualHeight = maxHeight;
    //        actualWidth = maxWidth;
    //        compressionQuality = 1;
    //    }
    
    
    return imageData;
}

-(IBAction)sub_click:(UIButton *)sender
{
    
    UIView *remove_baseView_placesImages = (UIView *)[self.images_Scrole_View viewWithTag:sender.tag+100];
    [remove_baseView_placesImages removeFromSuperview];
    
    int xCoordinate = remove_baseView_placesImages.frame.origin.x;
    
    if (sender.tag < index-1)
    {
        for (int i = (int)sender.tag+1; i<= index-1 ; i++) {
            
            UIView *moveForward_placesImages = (UIView *)[self.images_Scrole_View viewWithTag:i+100];
            moveForward_placesImages.frame = CGRectMake(xCoordinate, 0, 150, self.images_Scrole_View.frame.size.height);
            UIButton *buttonTag = (UIButton *)[moveForward_placesImages viewWithTag:(int)moveForward_placesImages.tag-100];
            buttonTag.tag = buttonTag.tag-1;
            moveForward_placesImages.tag = 100+i-1;
            xCoordinate = xCoordinate+110;
            
        }
        
        index --;
        
        x = xCoordinate;
        
        add_btn.hidden = NO;
        add_btn.frame = CGRectMake(xCoordinate, img.frame.origin.y, 100, 100);
        self.images_Scrole_View.contentSize = CGSizeMake(add_btn.frame.origin.x + add_btn.frame.size.width, 120);
        
    }
    else
    {
        x = x-110;
        index = (int)sender.tag;
        
        add_btn.frame = CGRectMake(remove_baseView_placesImages.frame.origin.x, img.frame.origin.y, 100, 100);
        self.images_Scrole_View.contentSize = CGSizeMake(add_btn.frame.origin.x + add_btn.frame.size.width, 120);
        
    }
    
    [img_array removeObjectAtIndex:(int)sender.tag-1];
    
    if (sender.tag == 5) {
        
        add_btn.hidden = NO;
        add_btn.frame = CGRectMake(remove_baseView_placesImages.frame.origin.x, img.frame.origin.y, 100, 100);
        self.images_Scrole_View.contentSize = CGSizeMake(add_btn.frame.origin.x + add_btn.frame.size.width, 120);
    }
    
}


#pragma mark Check_In_Service

- (IBAction)Check_In_Sucsess_Action:(id)sender {
    
    [self.view endEditing:YES];
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [self showAlert:Nonetwork];
        return;
    }
    
    if ([self.title_TF.text isEqualToString:@""])
    {
        [self showAlert:TitleAlert];
        return;
    }
  
    [indicator startAnimating];
    [self Checkin_Service_Call];
}
-(void)showAlert:(NSString *)message
{
//    self.alert_Label.text=message;
//    [UIView animateWithDuration:0.5 delay:0.1 options:UIViewAnimationOptionCurveEaseIn
//                     animations:^{
//                         self.alert_View_bottom.constant=10;
//                         
//                         [self.view layoutIfNeeded];
//                     } completion:^(BOOL finished) {
//                         
//                     }];
//    int duration = 2; // duration in seconds
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
//        [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseIn
//                         animations:^{
//                            self.alert_View_bottom.constant=-60;
//                              [self.view layoutIfNeeded];
//                         } completion:^(BOOL finished) {
//                             
//                         }];
//        
//    });
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:message  preferredStyle:UIAlertControllerStyleActionSheet];
    UIView  *firstSubview = alert.view.subviews.firstObject;
    firstSubview.backgroundColor = APP_YELLOW_COLOR;
    UIView *alertContentView = firstSubview.subviews.firstObject;
    alertContentView.backgroundColor = APP_YELLOW_COLOR;
    alertContentView.tintColor = [UIColor blueColor];
    for (UIView *subSubView in alertContentView.subviews) {
        
        subSubView.backgroundColor = APP_YELLOW_COLOR;
    }
    // [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alert animated:YES completion:nil];
    [self presentViewController:alert animated:YES completion:nil];
    
    int duration = 2; // duration in seconds
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        
        [alert dismissViewControllerAnimated:YES completion:nil];
        
    });

}
-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if (textField==self.title_TF) {
        [self.Des_TF becomeFirstResponder];
        return NO;
    }
    else{
        [textField resignFirstResponder];
        return YES;
    }
    
}

@end
