//
//  CreatePostView.h
//  openroadrides
//
//  Created by apple on 05/10/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "APIHelper.h"
#import "AppHelperClass.h"
#import "AppDelegate.h"
#import <GooglePlacePicker/GooglePlacePicker.h>
#import "CBAutoScrollLabel.h"
@interface CreatePostView : UIViewController<UITextViewDelegate,UIScrollViewDelegate,
UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,GMSPlacePickerViewControllerDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
{
    UIBarButtonItem *item0;
    UIButton *add_btn;
    UIButton *subtract_btn;
    UITextField *currentTextField;
    UIActivityIndicatorView  *activeIndicatore;
    UIView *indicaterview;
    UIPickerView *typer_picker;
    NSString *postCategoryID;
}
@property (weak, nonatomic) IBOutlet UITextField *createPostTitleTF;
@property (weak, nonatomic) IBOutlet UITextField *createPostPriceTF;
@property (weak, nonatomic) IBOutlet UITextField *createPostAddressTF;
@property (weak, nonatomic) IBOutlet UITextField *createPostPhoneNumberTF;
@property (weak, nonatomic) IBOutlet UITextField *createPostEmailTF;
@property (weak, nonatomic) IBOutlet UITextView *createPostDescriptionTextView;
@property (weak, nonatomic) IBOutlet UIView *createPost_Images_view;
@property (weak, nonatomic) IBOutlet UIScrollView *multipleImages_scrollview;
@property (weak, nonatomic) IBOutlet UIButton *createPostAddressLocationBtn;
- (IBAction)OnClick_createPostAddressLocationBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *descriptionPlaceholder_Label;
@property (weak, nonatomic) IBOutlet UITextField *createPostTypeTF;
@property (strong, nonatomic) NSString *DisplayCatString,*displatCatID;
@property (weak, nonatomic) IBOutlet UIImageView *createPostTypeImageView;
//@property  NSMutableArray *arrayOfPostCategoryTypes;
//@property  NSMutableArray *arrayOfPostCategoryTypeIDs;
@property NSMutableArray *arrayOFCreatePostCategories;
@property (weak, nonatomic) IBOutlet UIButton *createPostView_rideGoingBtn;
- (IBAction)onClick_createPostView_rideGoingBtn:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomContraint_createPostImagesView;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *createPostView_scrollLabel;
@property (weak, nonatomic) IBOutlet UIView *price_bottomView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint_priceBottomView;
@property (weak, nonatomic) IBOutlet UIImageView *createPost_priceImage;

@end
