//
//  GroupsViewController.m
//  openroadrides
//
//  Created by apple on 31/05/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "GroupsViewController.h"
#import "GroupsTableViewCell.h"
#import "Constants.h"
#import "APIHelper.h"
#import "AppHelperClass.h"
#import "GroupDetailViewController.h"
#import "CreateGroupViewController.h"
#import "MPCoachMarks.h"

@interface GroupsViewController (){
    
    NSMutableArray *filteredString;
    BOOL isFilltered;
    
}

@end

@implementation GroupsViewController


-(void)viewWillAppear:(BOOL)animated{
    
    if ([appDelegate.leave_OR_Delete_Group_YES isEqualToString:@"YES"]) {
        [self get_groups_list];
    }
    else if ([appDelegate.edit_List_Groups isEqualToString:@"YES"]) {
        [self get_groups_list];
        appDelegate.edit_List_Groups=@"NO";
    }
    
//     _groups_table_view.hidden = YES;
//    _group_search.hidden=YES;
//    if ([activeIndicatore isAnimating])
//    {
//        [activeIndicatore stopAnimating];
//        [activeIndicatore removeFromSuperview];
//    }
//    
//    [self get_groups_list];
//    [_groups_table_view reloadData];
//    self.navigationController.navigationBar.hidden=NO;
//    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appDelegate.edit_Group=@"NO";
    appDelegate.edit_List_Groups=@"NO";
    self.title = @"GROUPS";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    
   _groups_table_view.hidden = YES;
    _group_search.hidden=NO;
    _group_search.delegate=self;
    self.group_search.tintColor = [UIColor whiteColor];
//    self.group_search.barTintColor=[UIColor blackColor];
    self.group_search.barTintColor= ROUTES_CELL_BG_COLOUR1;
    appDelegate.leave_OR_Delete_Group_YES=@"";
    
#pragma mark - Bar buttons on nav bar.....
    
    
//    UIBarButtonItem *add_btn=[[UIBarButtonItem alloc]initWithImage:
//                                                                [[UIImage imageNamed:@"sidemenuicon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
//                                                                                               style:UIBarButtonItemStylePlain target:self action:@selector(addclicked)];

//    UIBarButtonItem *add_btn=[[UIBarButtonItem alloc]initWithTitle:@"+" style:UIBarButtonItemStylePlain target:self action:@selector(addclicked)];
//    [add_btn setTitleTextAttributes:@{
//                                         NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:26.0],
//                                         NSForegroundColorAttributeName: [UIColor blackColor]
//                                         } forState:UIControlStateNormal];
//    self.navigationItem.rightBarButtonItem=add_btn;
    
    self.groups_scrollLabel.hidden=YES;
    self.groups_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.groups_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.groups_scrollLabel.textColor = [UIColor blackColor];
    self.groups_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.groups_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.groups_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.groups_scrollLabel.scrollSpeed = 60; // pixels per second
    self.groups_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.groups_scrollLabel.fadeLength = 0.f;
    self.groups_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.groups_scrollLabel observeApplicationNotifications];
    
    self.group_rideGoingBtn.hidden=YES;
     self.bottomConstraint_groupTAbleView.constant=-30;
    if (appDelegate.ridedashboard_home) {
        self.groups_scrollLabel.hidden=NO;
        self.group_rideGoingBtn.hidden=NO;
        self.bottomConstraint_groupTAbleView.constant=0;
    }
    
    UIButton *add_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [add_btn addTarget:self action:@selector(addclicked) forControlEvents:UIControlEventTouchUpInside];
    add_btn.frame = CGRectMake(0, 0, 30, 30);
    [add_btn setBackgroundImage:[UIImage imageNamed:@"Add_plus"] forState:UIControlStateNormal];
    UIBarButtonItem * create_Ride= [[UIBarButtonItem alloc] initWithCustomView:add_btn];
    self.navigationItem.rightBarButtonItems=[NSArray arrayWithObjects:create_Ride, nil];

    
    UIBarButtonItem *menuButton=[[UIBarButtonItem alloc]initWithImage:
                                 [[UIImage imageNamed:@"back_Image"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
                                                                style:UIBarButtonItemStylePlain target:self action:@selector(menuclicked)];
    
    self.navigationItem.leftBarButtonItem = menuButton;
    
    
    defaults = [NSUserDefaults standardUserDefaults];
    user_token_id = [NSString stringWithFormat:@"%@",[defaults valueForKey:User_ID]];
    
    
//    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//    activeIndicatore.center = self.view.center;
//    activeIndicatore.color = APP_YELLOW_COLOR;
//    activeIndicatore.hidesWhenStopped = TRUE;
//    [self.view addSubview:activeIndicatore];
    
    
    indicaterview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    activeIndicatore.backgroundColor=[UIColor clearColor];
    activeIndicatore = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.frame = CGRectMake(0.0, 0.0, 80, 80);
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    [indicaterview addSubview:activeIndicatore];
    [self.view addSubview:indicaterview];
    [indicaterview bringSubviewToFront:self.view];
    [activeIndicatore startAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    indicaterview.hidden=YES;

    
    
    [self get_groups_list];
    
    
    //Display Annotation
    // Show coach marks
    BOOL coachMarksShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"MPCoachMarksShown_GroupsView"];
    if (coachMarksShown == NO) {
        // Don't show again
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MPCoachMarksShown_GroupsView"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        // Show coach marks
        [self showAnnotation];
    }
    
    // Do any additional setup after loading the view.
}

#pragma mark - Annotations
-(void)showAnnotation
{
    [self.view layoutIfNeeded];
    
    // Setup coach marks
    CGRect coachmark1 = CGRectMake( ([UIScreen mainScreen].bounds.size.width - 53), 20, self.navigationController.navigationBar.frame.size.height, self.navigationController.navigationBar.frame.size.height);
    CGRect coachmark2 = CGRectMake(_group_search.frame.origin.x,_group_search.frame.origin.y+65, _group_search.frame.size.width,_group_search.frame.size.height);
    
    NSArray *coachMarks;
    
    // Setup coach marks
    coachMarks = @[
                   @{
                       @"rect": [NSValue valueWithCGRect:coachmark1],
                       @"caption": @"Create new group    ",
                       @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                       @"position":[NSNumber numberWithInteger:LABEL_POSITION_LEFT],
                       @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_LEFT]
                       //@"showArrow":[NSNumber numberWithBool:YES]
                       },
                   @{
                       @"rect": [NSValue valueWithCGRect:coachmark2],
                       @"caption": @"Tap here to discover group",
                       @"shape": [NSNumber numberWithInteger:SHAPE_SQUARE],
                       @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                       @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_CENTER],
                       //@"showArrow":[NSNumber numberWithBool:YES]
                       },
                   ];
    
    MPCoachMarks *coachMarksView = [[MPCoachMarks alloc] initWithFrame:self.navigationController.view.bounds coachMarks:coachMarks];
    //[self.navigationController.view addSubview:coachMarksView];
    [[UIApplication sharedApplication].keyWindow addSubview:coachMarksView];
    [coachMarksView start];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Tableview methods......
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (groups_ary.count>0) {
      tableView.backgroundView = nil;
        return 1;
        
    }
    else{
        
        no_Data_View=[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        NO_DATA      = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        NO_DATA.numberOfLines=2;
        NO_DATA.textColor        = [UIColor whiteColor];
        NO_DATA.textAlignment    = NSTextAlignmentCenter;
        NO_DATA.text  = @"No groups available";
        NO_DATA.font=[UIFont fontWithName:@"Antonio-Bold" size:20];
        [no_Data_View addSubview:NO_DATA];
        tableView.backgroundView = no_Data_View;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        return 0;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (groups_ary.count>0) {
        
        if(isFilltered)
        {
            return [filteredString count];
        }
        else
        {
            return groups_ary.count;
        }
        
    }
    
    else
    {
        return 0;
    }
    

  
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *simpleTableIdentifier = @"GroupsCell";
    GroupsTableViewCell *cell = (GroupsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    self.groups_table_view.separatorStyle=UITableViewCellSeparatorStyleNone;
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"GroupsTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    if (isFilltered) {
        
        if (indexPath.row%2==0) {
            
            cell.groups_content_view.backgroundColor = [UIColor colorWithRed:41/255.0 green:41/255.0 blue:41/255.0 alpha:1.0];
        }
        else{
            
            cell.groups_content_view.backgroundColor = [UIColor colorWithRed:26/255.0 green:26/255.0 blue:26/255.0 alpha:1.0];
            
            
            
        }
        group_id_str = [NSString stringWithFormat:@"%@", [[filteredString objectAtIndex:indexPath.row] valueForKey:@"group_id"]];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.title_lbl.text = [[filteredString objectAtIndex:indexPath.row] valueForKey:@"group_name"];
        
        NSInteger member_count=[[[filteredString objectAtIndex:indexPath.row] objectForKey :@"members_count"] integerValue];
        if (member_count>1) {
             cell.members_lbl.text= [NSString stringWithFormat:@"%@ members",[[filteredString objectAtIndex:indexPath.row] objectForKey :@"members_count"]];
        }
        else
             cell.members_lbl.text= [NSString stringWithFormat:@"%@ member",[[filteredString objectAtIndex:indexPath.row] objectForKey :@"members_count"]];
       
        
        NSString *group_image_string1 = [NSString stringWithFormat:@"%@", [[filteredString objectAtIndex:indexPath.row] objectForKey:@"group_image"]];
        NSURL*url=[NSURL URLWithString:group_image_string1];
        [cell.groups_img sd_setImageWithURL:url
                           placeholderImage:[UIImage imageNamed:@"groups-icon"]
                                    options:SDWebImageRefreshCached];
               
    }
    else{
        
        
        if (indexPath.row%2==0) {
            
            cell.groups_content_view.backgroundColor = [UIColor colorWithRed:41/255.0 green:41/255.0 blue:41/255.0 alpha:1.0];
        }
        else{
            
            cell.groups_content_view.backgroundColor = [UIColor colorWithRed:26/255.0 green:26/255.0 blue:26/255.0 alpha:1.0];
            
            
            
        }
        group_id_str = [NSString stringWithFormat:@"%@", [[groups_ary objectAtIndex:indexPath.row] valueForKey:@"group_id"]];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.title_lbl.text = [[groups_ary objectAtIndex:indexPath.row] valueForKey:@"group_name"];
        
        NSInteger member_count=[[[groups_ary objectAtIndex:indexPath.row] objectForKey :@"members_count"] integerValue];
        if (member_count>1) {
            cell.members_lbl.text= [NSString stringWithFormat:@"%@ members",[[groups_ary objectAtIndex:indexPath.row] objectForKey :@"members_count"]];
        }
        else
             cell.members_lbl.text= [NSString stringWithFormat:@"%@ member",[[groups_ary objectAtIndex:indexPath.row] objectForKey :@"members_count"]];
        
        
         NSString *group_image_string = [NSString stringWithFormat:@"%@", [[groups_ary objectAtIndex:indexPath.row] objectForKey:@"group_image"]];
        NSString*createdBy_string=[NSString stringWithFormat:@"%@",[[groups_ary objectAtIndex:indexPath.row] objectForKey:@"user_type"]];
        if ([createdBy_string isEqualToString:@"owner"]) {
            
             cell.createdBy_label.text= [NSString stringWithFormat:@"Created by me"];
        }
        else
        {
            cell.createdBy_label.text= [NSString stringWithFormat:@"Created by \"%@\"",[[groups_ary objectAtIndex:indexPath.row] objectForKey :@"group_owner"]];
        }
        
        NSURL*url=[NSURL URLWithString:group_image_string];
        [cell.groups_img sd_setImageWithURL:url
                                  placeholderImage:[UIImage imageNamed:@"groups-icon"]
                                           options:SDWebImageRefreshCached];


    }
        
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (isFilltered) {
        group_id_str = [NSString stringWithFormat:@"%@", [[filteredString objectAtIndex:indexPath.row] valueForKey:@"group_id"]];
        
    }
    else{
        group_id_str = [NSString stringWithFormat:@"%@", [[groups_ary objectAtIndex:indexPath.row] valueForKey:@"group_id"]];
    }
    GroupDetailViewController *cntlr = [self.storyboard instantiateViewControllerWithIdentifier:@"GroupDetailViewController"];
    cntlr.group_id = group_id_str;
    cntlr.group_Owner_Id=[NSString stringWithFormat:@"%@", [[groups_ary objectAtIndex:indexPath.row] valueForKey:@"group_owner_id"]];
    
    [self.navigationController pushViewController:cntlr animated:YES];
    
}
#pragma mark - Search methods
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)aSearchBar {
    self.group_search.placeholder = @"search";
    self.group_search.showsCancelButton = YES;
    return YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)SearchBar
{
    
    SearchBar.text = nil;
    self.group_search.placeholder = @"search";
    self.group_search.showsCancelButton = NO;
    [SearchBar resignFirstResponder];
    isFilltered = NO;
    [_groups_table_view setContentOffset:CGPointZero animated:NO];
    [_groups_table_view reloadData];
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.group_search resignFirstResponder];
    for (UIView *view in searchBar.subviews)
    {
        for (id subview in view.subviews)
        {
            if ( [subview isKindOfClass:[UIButton class]] )
            {
                [subview setEnabled:YES];
                
                NSLog(@"enableCancelButton");
                return;
            }
        }
    }
    
    [searchBar setShowsCancelButton:YES animated:YES];
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
    if(searchText.length == 0)
    {
        isFilltered = NO;
    }else
    {
        isFilltered = YES;
        filteredString = [[NSMutableArray alloc]init];
        
        for(NSDictionary *group_Dict in groups_ary)
        {
            NSString *group_Name=[group_Dict valueForKey:@"group_name"];
            
            NSRange stringRange = [group_Name rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if(stringRange.location != NSNotFound)
            {
                [filteredString addObject:group_Dict];
                
            }
        }
    }
    [_groups_table_view reloadData];
}


-(void)get_groups_list{
    
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [SHARED_HELPER showAlert:Nonetwork];
         indicaterview.hidden=YES;
        return;
    }
 
//    [activeIndicatore startAnimating];
    indicaterview.hidden=NO;
    
    [SHARED_API groupsRequestsWithParams:user_token_id withSuccess:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           NSLog(@"responce is %@",response);
                           
                           groups_ary = [response valueForKey:@"groups"];
                           
                           if (groups_ary.count>0) {
                               _groups_table_view.hidden = NO;
                               [self.groups_table_view reloadData];
                               
//                               if ([activeIndicatore isAnimating]) {
//                                   [activeIndicatore stopAnimating];
//
//                                   [activeIndicatore removeFromSuperview];
//                               }
                                indicaterview.hidden=YES;
//                               _group_search.hidden=NO;

                           }
                           else{
                               [self.groups_table_view reloadData];
                               _groups_table_view.hidden = NO;
//                               if ([activeIndicatore isAnimating]) {
//                                   [activeIndicatore stopAnimating];
//
//                                   [activeIndicatore removeFromSuperview];
//                               }
                                indicaterview.hidden=YES;
                               // [SHARED_HELPER showAlert:GroupsEmpty];
                           }
                       });
        
    } onfailure:^(NSError *theError) {
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           
                           [SHARED_HELPER showAlert:ServiceFail];
                           _groups_table_view.hidden = YES;
//                           if ([activeIndicatore isAnimating]) {
//                               [activeIndicatore stopAnimating];
//                               [activeIndicatore removeFromSuperview];
//                           }
                            indicaterview.hidden=YES;
                           
                           
                       });
    }];
}
-(void)addclicked{
    
    CreateGroupViewController *cntlr = [self.storyboard  instantiateViewControllerWithIdentifier:@"CreateGroupViewController"];
    [self.navigationController pushViewController:cntlr animated:YES];
    
}

-(void)menuclicked{
    
//    [self.navigationController popViewControllerAnimated:YES];
    DashboardViewController*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"DashboardViewController"];
    [self.navigationController pushViewController:controller animated:NO];
//
}



- (IBAction)onClick_group_rideGoingBtn:(id)sender {
    
    self.groups_scrollLabel.hidden=YES;
    self.group_rideGoingBtn.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }

}
@end
