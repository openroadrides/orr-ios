//
//  scheduleRideCollectionViewCell.h
//  openroadrides
//
//  Created by apple on 04/07/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface scheduleRideCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *ride_imageView;
@property (weak, nonatomic) IBOutlet UILabel *ride_title_name_label;
@property (weak, nonatomic) IBOutlet UIView *contentView_collectionViewCell;
@end
