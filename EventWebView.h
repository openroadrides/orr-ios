//
//  EventWebView.h
//  openroadrides
//
//  Created by apple on 19/09/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "APIHelper.h"
#import "AppHelperClass.h"
@interface EventWebView : UIViewController<UIWebViewDelegate>
{
     UIActivityIndicatorView  *activeIndicatore;
}
@property (weak, nonatomic) IBOutlet UIWebView *linkWebView;
@property (strong, nonatomic) NSString *websiteString;
@end
