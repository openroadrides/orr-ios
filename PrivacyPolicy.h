//
//  PrivacyPolicy.h
//  openroadrides
//
//  Created by apple on 27/07/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "AppDelegate.h"
#import "RideDashboard.h"
#import "CBAutoScrollLabel.h"
@interface PrivacyPolicy : UIViewController<UIWebViewDelegate>
{
    UIActivityIndicatorView *activeIndicatore;
}
@property (weak, nonatomic) IBOutlet UIWebView *privactPolicy_webview;
@property (weak, nonatomic) IBOutlet UIButton *privacy_rideGoingBtn;
- (IBAction)onClick_privacy_rideGoingBtn:(id)sender;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *privacy_scrollLabel;

@end
