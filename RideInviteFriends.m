//
//  RideInviteFriends.m
//  openroadrides
//
//  Created by apple on 03/07/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "RideInviteFriends.h"
#import "AppDelegate.h"
@interface RideInviteFriends ()
{
    UIBarButtonItem *item0;
    AppDelegate *app_delegate;
}
@end

@implementation RideInviteFriends

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appDelegate.edit_Group=@"NO"; //for edit
    
    
    
    self.title = @"SELECT FRIENDS";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    
    self.rideInviteFrnds_scrollLabel.hidden=YES;
    self.rideInviteFrnds_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.rideInviteFrnds_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.rideInviteFrnds_scrollLabel.textColor = [UIColor blackColor];
    self.rideInviteFrnds_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.rideInviteFrnds_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.rideInviteFrnds_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.rideInviteFrnds_scrollLabel.scrollSpeed = 60; // pixels per second
    self.rideInviteFrnds_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.rideInviteFrnds_scrollLabel.fadeLength = 0.f;
    self.rideInviteFrnds_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.rideInviteFrnds_scrollLabel observeApplicationNotifications];

    
    self.rideInviteFrnds_rideGoingBtn.hidden=YES;
    if (appDelegate.ridedashboard_home) {
        self.rideInviteFrnds_scrollLabel.hidden=NO;
        self.rideInviteFrnds_rideGoingBtn.hidden=NO;
        
    }

    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [leftBtn addTarget:self action:@selector(left_back_btn:) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.frame = CGRectMake(0, 0, 25, 25);
    //    [leftBtn setBackgroundImage:[UIImage imageNamed:@"Finalback-arrow.png"] forState:UIControlStateNormal];
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"back_Image"] forState:UIControlStateNormal];
    UIBarButtonItem *leftBackButton=[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    item0=leftBackButton;
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:item0, nil];
    
    
//    UIButton *saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [saveBtn addTarget:self action:@selector(save_clicked) forControlEvents:UIControlEventTouchUpInside];
//    saveBtn.frame = CGRectMake(0, 0, 50, 50);
//    [saveBtn setBackgroundImage:[UIImage imageNamed:@"SAVE"] forState:UIControlStateNormal];
//    [saveBtn setTitle:@"Done" forState:UIControlStateNormal];
//    [saveBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    UIBarButtonItem * create_Ride= [[UIBarButtonItem alloc] initWithCustomView:saveBtn];
//    self.navigationItem.rightBarButtonItems=[NSArray arrayWithObjects:create_Ride, nil];
    
    
    
    UIBarButtonItem *done_btn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(save_clicked)];
    
    [done_btn setTitleTextAttributes:@{
                                       NSFontAttributeName: [UIFont fontWithName:@"Antonio-Bold" size:17.0],
                                       NSForegroundColorAttributeName: [UIColor blackColor]
                                       } forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem = done_btn;


    
    
    CGFloat content_View_Width=SCREEN_WIDTH-20;
    self.widthConstraint_rideinviteFrnds_contentView.constant=content_View_Width*2;
    [self.view layoutIfNeeded];
    
    
    [self.rideInvites_public_frnds_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.rideInvites_myFrnds_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
//    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//    activeIndicatore.center = self.view.center;
//    activeIndicatore.color = APP_YELLOW_COLOR;
//    activeIndicatore.hidesWhenStopped = TRUE;
//    [self.view addSubview:activeIndicatore];
    
    indicaterview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    activeIndicatore.backgroundColor=[UIColor clearColor];
    activeIndicatore = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.frame = CGRectMake(0.0, 0.0, 80, 80);
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    [indicaterview addSubview:activeIndicatore];
    [self.view addSubview:indicaterview];
    [indicaterview bringSubviewToFront:self.view];
    [activeIndicatore startAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    indicaterview.hidden=YES;
    
    view_Status_rideInvite_frnds=@"";
    
    self.rideInvites_public_frnds_tableView.dataSource=self;
    self.rideInvites_public_frnds_tableView.delegate=self;
    self.rideInvites_myFrnds_tableView.dataSource=self;
    self.rideInvites_myFrnds_tableView.delegate=self;
    self.noData_label_myfrnds.hidden=YES;
    self.noData_label_public.hidden=YES;
    
    
    self.rideInvite_refreshControl = [[UIRefreshControl alloc]init];
    self.rideInvite_refreshControl.tintColor=APP_YELLOW_COLOR;
    
    self.rideInvite_refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
    [self.rideInvite_refreshControl addTarget:self action:@selector(rideInvites_refreshTable) forControlEvents:UIControlEventValueChanged];
    [self.rideInvites_public_frnds_tableView addSubview:self.rideInvite_refreshControl];
    
    self.rideInvite_refreshControl2 = [[UIRefreshControl alloc]init];
    self.rideInvite_refreshControl2.tintColor=APP_YELLOW_COLOR;
    
    
    self.rideInvite_refreshControl2.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
    [self.rideInvite_refreshControl2 addTarget:self action:@selector(rideInvites_refreshTable) forControlEvents:UIControlEventValueChanged];
    
    [self.rideInvites_myFrnds_tableView addSubview:self.rideInvite_refreshControl2];
    [self get_rideInviteFriends_Data];
    
    _rideInvites_searchBar.hidden=NO;
    _rideInvites_searchBar.delegate=self;
    self.rideInvites_searchBar.tintColor = [UIColor whiteColor];
//    self.rideInvites_searchBar.barTintColor=[UIColor blackColor];
    self.rideInvites_searchBar.barTintColor= ROUTES_CELL_BG_COLOUR1;
    
     arrayOfSelectedPublicFrndsID=[[NSMutableArray alloc ]init];
    if (appDelegate.createGroup_frndInvitations.count>0) {
        [arrayOfSelectedPublicFrndsID addObjectsFromArray:appDelegate.createGroup_frndInvitations];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)left_back_btn:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)save_clicked
{
   
    if (arrayOfSelectedPublicFrndsID.count>0) {
        if ([_isFrom isEqualToString:@"edit"]) {
             appDelegate.edit_Group=@"INVITE";
        }
        
        appDelegate.createGroup_frndInvitations=[[NSMutableArray alloc]init];
        appDelegate.createGroup_frndInvitations=arrayOfSelectedPublicFrndsID;
        NSLog(@"Saved Frnd Ids %@",appDelegate.createGroup_frndInvitations);
//        NSLog(@"Saved Frnd Name %@",appDelegate.createGroup_frndInvitations);
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
        if ([_isFrom isEqualToString:@"edit"]) {
             [SHARED_HELPER showAlert:CREATEGROUPMEMBERS];
            return;
        }
        [SHARED_HELPER showAlert:CREATEGROUPMEMBERS];
    }
}

- (void)rideInvites_refreshTable {
    //TODO: refresh your data
    if([view_Status_rideInvite_frnds isEqualToString:@""]||[view_Status_rideInvite_frnds isEqualToString:@"PUBLIC"])
    {
        rideinvites_Public_Friends_Called=@"YES";
        [self.rideInvite_refreshControl endRefreshing];
        //        [self.public_friends_tableView reloadData];
        arrayOfRideInvitePublicFriends=nil;
        arrayOfRideInvitePublicFriends=[[NSMutableArray alloc]init];
        rideInvitefilteredArray=nil;
        rideInvitefilteredArray=[[NSMutableArray alloc]init];
        [self.rideInvites_public_frnds_tableView reloadData];
        [self get_rideInviteFriends_Data];
        [self.rideInvites_searchBar setText:@""];
    }
    else{
        rideInvites_my_Friends_Called=@"YES";
        arrayOfRideInviteMyFriends=nil;
        arrayOfRideInviteMyFriends=[[NSMutableArray alloc]init];
        rideInvitefilteredArray2=nil;
        rideInvitefilteredArray2=[[NSMutableArray alloc]init];
        [self.rideInvites_myFrnds_tableView reloadData];
        [self.rideInvite_refreshControl2 endRefreshing];
        [self get_rideInviteFriends_Data];
         [self.rideInvites_searchBar setText:@""];
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onClick_rideinvite_public_frnds_btn:(id)sender {
    view_Status_rideInvite_frnds=@"PUBLIC";
    self.subView_segment_rideInvite_Public_frnd.backgroundColor=[UIColor whiteColor];
    self.subView_segment_rideInvite_myFrnds.backgroundColor=[UIColor blackColor];
    [self.rideInvites_ScrollView setContentOffset:CGPointMake((0), _rideInvites_ScrollView.contentOffset.y)];
    self.label_segemt_rideinvite_public_frnds.textColor=[UIColor whiteColor];
    self.label_segment_rideinvite_mtFrnds.textColor=APP_YELLOW_COLOR;
    if ([rideinvites_Public_Friends_Called isEqualToString:@"YES"]) {
        rideInvite_isFiltered=NO;
        [_rideInvites_searchBar resignFirstResponder];
        _rideInvites_searchBar.text=@"";
        _rideInvites_searchBar.showsCancelButton=NO;
        [_rideInvites_public_frnds_tableView reloadData];
    }
    else{
        rideInvite_isFiltered=NO;
        [_rideInvites_searchBar resignFirstResponder];
        _rideInvites_searchBar.text=@"";
        _rideInvites_searchBar.showsCancelButton=NO;
        [self get_rideInviteFriends_Data];
    }
    

}
- (IBAction)onClick_rideInvites_myFrnds_btn:(id)sender {
    view_Status_rideInvite_frnds=@"MY";
    self.subView_segment_rideInvite_Public_frnd.backgroundColor=[UIColor blackColor];
    self.subView_segment_rideInvite_myFrnds.backgroundColor=[UIColor whiteColor];
    [self.rideInvites_ScrollView setContentOffset:CGPointMake((SCREEN_WIDTH-20), self.rideInvites_ScrollView.contentOffset.y)];
    self.label_segment_rideinvite_mtFrnds.textColor=[UIColor whiteColor];
    self.label_segemt_rideinvite_public_frnds.textColor=APP_YELLOW_COLOR;
    
    if ([rideInvites_my_Friends_Called isEqualToString:@"YES"]) {
        rideInvite_isFiltered=NO;
        [_rideInvites_searchBar resignFirstResponder];
        _rideInvites_searchBar.text=@"";
        _rideInvites_searchBar.showsCancelButton=NO;
        [_rideInvites_myFrnds_tableView reloadData];
    }
    else{
        rideInvite_isFiltered=NO;
        [_rideInvites_searchBar resignFirstResponder];
        _rideInvites_searchBar.text=@"";
        _rideInvites_searchBar.showsCancelButton=NO;
        //  [self.my_Routes_Table reloadData];
        [self get_rideInviteFriends_Data];
    }

}
#pragma mark - ScrollView
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    self.subView_segment_rideInvite_Public_frnd.userInteractionEnabled=YES;
    self.subView_segment_rideInvite_myFrnds.userInteractionEnabled=YES;
    if (scrollView.tag==5) {
        if (scrollView.contentOffset.x==0) {
            [self onClick_rideinvite_public_frnds_btn:0];
        }
        if (scrollView.contentOffset.x==SCREEN_WIDTH-20) {
            [self onClick_rideInvites_myFrnds_btn:0];
        }
    }
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    if (scrollView==_rideInvites_public_frnds_tableView||scrollView==_rideInvites_myFrnds_tableView) {
        self.subView_segment_rideInvite_Public_frnd.userInteractionEnabled=NO;
        self.subView_segment_rideInvite_myFrnds.userInteractionEnabled=NO;
    }
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    self.subView_segment_rideInvite_Public_frnd.userInteractionEnabled=YES;
    self.subView_segment_rideInvite_myFrnds.userInteractionEnabled=YES;
    
}

-(void)get_rideInviteFriends_Data
{
//    [activeIndicatore startAnimating];
    indicaterview.hidden=NO;
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [SHARED_HELPER showAlert:Nonetwork];
//        [activeIndicatore stopAnimating];
          indicaterview.hidden=YES;
        return;
    }
    NSString *user_id=[Defaults valueForKey:User_ID];
   
    
    //    NSString *user_id=@"1";
    NSString *rideInvite_service_is;
    if ([view_Status_rideInvite_frnds isEqualToString:@""]||[view_Status_rideInvite_frnds isEqualToString:@"PUBLIC"]) {
        if ([_isFrom isEqualToString:@"edit"]) {
             rideInvite_service_is=WS_PUBLICFRIENDS_Edit_REQUEST;
            user_id=[NSString stringWithFormat:@"%@/%@",_group_id,user_id];
        }
        else
        {
             rideInvite_service_is=WS_PUBLICFRIENDS_REQUEST;
        }
       
        arrayOfRideInvitePublicFriends=[[NSMutableArray alloc]init];
        rideInvitefilteredArray=[[NSMutableArray alloc]init];
       
    }
    else{
        if ([_isFrom isEqualToString:@"edit"]) {
            rideInvite_service_is=WS_MYFRIENDS_Edit_REQUEST;
            user_id=[NSString stringWithFormat:@"%@/%@",_group_id,user_id];
        }
        else
        {
            rideInvite_service_is=WS_MYFRIENDS_REQUEST;
        }
       
        arrayOfRideInviteMyFriends=[[NSMutableArray alloc]init];
        rideInvitefilteredArray2=[[NSMutableArray alloc]init];
//        arrayOfSelectedMyFrndsID=[[NSMutableArray alloc]init];
    }
    [SHARED_API publicFriendsRequest:user_id public_myfriends:rideInvite_service_is withSuccess:^(NSDictionary *response) {
        NSLog(@"%@",response);
        [self rideInvite_Friends_Service_Sucsess:response];
        indicaterview.hidden=YES;
    } onfailure:^(NSError *theError) {
        [SHARED_HELPER showAlert:ServiceFail];
        indicaterview.hidden=YES;
    }];
    
}
-(void)rideInvite_Friends_Service_Sucsess:(NSDictionary *)respnse{
    
    
    if ([[respnse valueForKey:STATUS] isEqualToString:SUCCESS]) {
        NSArray *friends_data=[respnse valueForKey:@"friends"];
        if (friends_data.count>0) {
             if ([view_Status_rideInvite_frnds isEqualToString:@""]||[view_Status_rideInvite_frnds isEqualToString:@"PUBLIC"]) {
                 if ([_isFrom isEqualToString:@"edit"]) {
                     
                     [arrayOfRideInvitePublicFriends addObjectsFromArray:friends_data];
                     [rideInvitefilteredArray addObjectsFromArray:friends_data];
                 }
                 else
                 {
                     NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(status contains[c] %@)||(status contains[c] %@)||(status contains[c] %@)",@"notfriend",@"pending",@"invitation"];
                     arrayOfRideInvitePublicFriends = [NSMutableArray arrayWithArray:[friends_data filteredArrayUsingPredicate:predicate]];
                     rideInvitefilteredArray = [NSMutableArray arrayWithArray:[friends_data filteredArrayUsingPredicate:predicate]];
                 }
                 
                 view_Status_rideInvite_frnds=@"PUBLIC";
                 rideinvites_Public_Friends_Called=@"YES";
                 [self.rideInvites_public_frnds_tableView reloadData];
                 
                 
                 
                 if (arrayOfRideInvitePublicFriends.count>0)self.noData_label_public.hidden=YES;
                 else self.noData_label_public.hidden=NO;
           
             }
            else
            {
                if ([_isFrom isEqualToString:@"edit"]) {
                    
                    [arrayOfRideInviteMyFriends addObjectsFromArray:friends_data];
                    [rideInvitefilteredArray2 addObjectsFromArray:friends_data];
                }
                else
                {
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(status contains[c] %@)",@"friend"];
                    arrayOfRideInviteMyFriends = [NSMutableArray arrayWithArray:[friends_data filteredArrayUsingPredicate:predicate]];
                    rideInvitefilteredArray2 = [NSMutableArray arrayWithArray:[friends_data filteredArrayUsingPredicate:predicate]];
                }
                
                
                [self.rideInvites_myFrnds_tableView reloadData];
                rideInvites_my_Friends_Called=@"YES";
                
               
                if (arrayOfRideInviteMyFriends.count>0)self.noData_label_myfrnds.hidden=YES;
                else self.noData_label_myfrnds.hidden=NO;
            }
            
            
            self.rideInvites_ScrollView.hidden=NO;
           
        }
        else{
//            [SHARED_HELPER showAlert:NO_Friends];
            
            [self service_fail_OR_EmptyData];
        }
    }
    else{
        [SHARED_HELPER showAlert:ServiceFail];
        indicaterview.hidden=YES;
        [self service_fail_OR_EmptyData];
    }
}
-(void)service_fail_OR_EmptyData{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        //        [self.navigationController popViewControllerAnimated:YES];
        indicaterview.hidden=YES;
         if ([view_Status_rideInvite_frnds isEqualToString:@""]||[view_Status_rideInvite_frnds isEqualToString:@"PUBLIC"]) {
             _noData_label_public.hidden=NO;
             
         }
         else{
             _noData_label_myfrnds.hidden=NO;
         }
    });
}

-(RideInvitesPublicFrndsCell *) containingCellForView:(UIView *)view
{
    if (!view.superview)
        return nil;
    
    if ([view.superview isKindOfClass:[RideInvitesPublicFrndsCell class]])
        return (RideInvitesPublicFrndsCell *)view.superview;
    
    return [self containingCellForView:view.superview];
}
- (IBAction)onClick_rideInvites_public_frnds_checkBox_btn:(UIButton *)sender {

    
    if (rideInvite_isFiltered) {
        RideInvitesPublicFrndsCell  *containingCell = [self containingCellForView:sender];
        NSIndexPath *indexPath = [_rideInvites_public_frnds_tableView indexPathForCell:containingCell];
        
        NSString *select_publicFrnds_string=[NSString stringWithFormat:@"%@",[[rideInvitefilteredArray objectAtIndex:indexPath.row] objectForKey:@"friend_id"]];
        NSString *select_publicFrnds_name_string=[NSString stringWithFormat:@"%@",[[rideInvitefilteredArray objectAtIndex:indexPath.row] objectForKey:@"name"]];
        
        if ([arrayOfSelectedPublicFrndsID containsObject:[NSString stringWithFormat:@"%@,%@",select_publicFrnds_string,select_publicFrnds_name_string]])
            
        {
            [arrayOfSelectedPublicFrndsID removeObject:[NSString stringWithFormat:@"%@,%@",select_publicFrnds_string,select_publicFrnds_name_string]];
            
        }
        else
        {
            [arrayOfSelectedPublicFrndsID addObject:[NSString stringWithFormat:@"%@,%@",select_publicFrnds_string,select_publicFrnds_name_string]];
            
        }
        [self.rideInvites_public_frnds_tableView reloadData];
        
    }
else
{
    RideInvitesPublicFrndsCell  *containingCell = [self containingCellForView:sender];
    NSIndexPath *indexPath = [_rideInvites_public_frnds_tableView indexPathForCell:containingCell];

    NSString *select_publicFrnds_string=[NSString stringWithFormat:@"%@",[[arrayOfRideInvitePublicFriends objectAtIndex:indexPath.row] objectForKey:@"friend_id"]];
    NSString *select_publicFrnds_name_string=[NSString stringWithFormat:@"%@",[[arrayOfRideInvitePublicFriends objectAtIndex:indexPath.row] objectForKey:@"name"]];
    
    if ([arrayOfSelectedPublicFrndsID containsObject:[NSString stringWithFormat:@"%@,%@",select_publicFrnds_string,select_publicFrnds_name_string]])
        
    {
        [arrayOfSelectedPublicFrndsID removeObject:[NSString stringWithFormat:@"%@,%@",select_publicFrnds_string,select_publicFrnds_name_string]];
        
    }
    else
    {
        [arrayOfSelectedPublicFrndsID addObject:[NSString stringWithFormat:@"%@,%@",select_publicFrnds_string,select_publicFrnds_name_string]];
        
    }
    [self.rideInvites_public_frnds_tableView reloadData];
}
    
}
-(RideInvitesMyFrndsCell *) containingCellForView1:(UIView *)view
{
    if (!view.superview)
        return nil;
    
    if ([view.superview isKindOfClass:[RideInvitesMyFrndsCell class]])
        return (RideInvitesMyFrndsCell *)view.superview;
    
    return [self containingCellForView1:view.superview];
}
- (IBAction)onClick_rideinvites_myFrnds_checkBox_btn:(UIButton *)sender
{
    if (rideInvite_isFiltered) {
        RideInvitesMyFrndsCell  *containingCell = [self containingCellForView1:sender];
        NSIndexPath *indexPath = [_rideInvites_myFrnds_tableView indexPathForCell:containingCell];
        
        NSString *select_MyFrnds_string=[NSString stringWithFormat:@"%@",[[rideInvitefilteredArray2 objectAtIndex:indexPath.row] objectForKey:@"friend_id"]];
        NSString *select_myFrnds_name_string=[NSString stringWithFormat:@"%@",[[rideInvitefilteredArray2 objectAtIndex:indexPath.row] objectForKey:@"name"]];
        
        if ([arrayOfSelectedPublicFrndsID containsObject:[NSString stringWithFormat:@"%@,%@",select_MyFrnds_string,select_myFrnds_name_string]])
            
        {
            [arrayOfSelectedPublicFrndsID removeObject:[NSString stringWithFormat:@"%@,%@",select_MyFrnds_string,select_myFrnds_name_string]];
        }
        else
        {
            [arrayOfSelectedPublicFrndsID addObject:[NSString stringWithFormat:@"%@,%@",select_MyFrnds_string,select_myFrnds_name_string]];
            
        }
        [self.rideInvites_myFrnds_tableView reloadData];
    }
    else{
    RideInvitesMyFrndsCell  *containingCell = [self containingCellForView1:sender];
    NSIndexPath *indexPath = [_rideInvites_myFrnds_tableView indexPathForCell:containingCell];
    
    NSString *select_MyFrnds_string=[NSString stringWithFormat:@"%@",[[arrayOfRideInviteMyFriends objectAtIndex:indexPath.row] objectForKey:@"friend_id"]];
    NSString *select_myFrnds_name_string=[NSString stringWithFormat:@"%@",[[arrayOfRideInviteMyFriends objectAtIndex:indexPath.row] objectForKey:@"name"]];
    
    if ([arrayOfSelectedPublicFrndsID containsObject:[NSString stringWithFormat:@"%@,%@",select_MyFrnds_string,select_myFrnds_name_string]])
        
    {
        [arrayOfSelectedPublicFrndsID removeObject:[NSString stringWithFormat:@"%@,%@",select_MyFrnds_string,select_myFrnds_name_string]];
    }
    else
    {
        [arrayOfSelectedPublicFrndsID addObject:[NSString stringWithFormat:@"%@,%@",select_MyFrnds_string,select_myFrnds_name_string]];
        
    }
    [self.rideInvites_myFrnds_tableView reloadData];
    }
}
#pragma mark - TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([view_Status_rideInvite_frnds isEqualToString:@"PUBLIC"]) {
        
        //        if (arrayOfPublicFriends.count>0)
        //        {
        //            return arrayOfPublicFriends.count;
        //        }
        //        else{
        ////            NO_DATA      = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        //            NO_DATA.numberOfLines=2;
        //            NO_DATA.textColor        = APP_YELLOW_COLOR;
        //            NO_DATA.textAlignment    = NSTextAlignmentCenter;
        //            NO_DATA.text  = @"No Friends found";
        //            tableView.backgroundView = NO_DATA;
        //            tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        //            return 0;
        //        }
        
        if ([arrayOfRideInvitePublicFriends count]>0)
        {
            if(rideInvite_isFiltered)
            {
                
//                if ([rideInvitefilteredArray count]>0) {
//                     self.noData_label_public.hidden=NO;
//                }
//                else{
//                    self.noData_label_public.hidden=YES;
//                }
                return [rideInvitefilteredArray count];
            }
            
            return arrayOfRideInvitePublicFriends.count;
        }
//        else{
//            self.noData_label_public.hidden=NO;
//        }
  
    }
    else if ([view_Status_rideInvite_frnds isEqualToString:@"MY"])
    {
        //        if (arrayOfMyFriends.count>0)
        //        {
        //            return arrayOfMyFriends.count;
        //        }
        //        else{
        //            NO_DATA      = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        //            NO_DATA.numberOfLines=2;
        //            NO_DATA.textColor        = APP_YELLOW_COLOR;
        //            NO_DATA.textAlignment    = NSTextAlignmentCenter;
        //            NO_DATA.text  = @"No Friends found";
        //            tableView.backgroundView = NO_DATA;
        //            tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        ////            return 0;
        //        }
        
        
        
        if ([arrayOfRideInviteMyFriends count]>0)
        {
            if(rideInvite_isFiltered)
            {
                return [rideInvitefilteredArray2 count];
            }
            
            return arrayOfRideInviteMyFriends.count;
        }
//        else{
//            self.noData_label_myfrnds.hidden=NO;
//        }
        
        
    }
    else{
        return 0;
    }
    return 0;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([view_Status_rideInvite_frnds isEqualToString:@"PUBLIC"]) {
        
        
        
        RideInvitesPublicFrndsCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
        
        if (cell == nil)
        {
            cell = [[RideInvitesPublicFrndsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        }
        if (rideInvite_isFiltered) {
            
            
            
            
            if (indexPath.row%2==0) {
                cell.rideinvites_publicFrndsCell_bgView.backgroundColor=ROUTES_CELL_BG_COLOUR1;
            }
            else
                cell.rideinvites_publicFrndsCell_bgView.backgroundColor=ROUTES_CELL_BG_COLOUR2;
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            cell.rideDetails_publicFrndsCell_userName.text=[[rideInvitefilteredArray objectAtIndex:indexPath.row] objectForKey:@"name"];
            
            NSString * areaName=[NSString stringWithFormat:@"%@",[[rideInvitefilteredArray objectAtIndex:indexPath.row] objectForKey:@"address"]];
            NSString * cityName=[NSString stringWithFormat:@"%@",[[rideInvitefilteredArray objectAtIndex:indexPath.row] objectForKey:@"city"]];
            NSString * stateName=[NSString stringWithFormat:@"%@",[[rideInvitefilteredArray objectAtIndex:indexPath.row] objectForKey:@"state"]];
            NSString *zipCode=[NSString stringWithFormat:@"%@",[[rideInvitefilteredArray objectAtIndex:indexPath.row] objectForKey:@"zipcode"]];
            
            NSMutableArray *empaty_array_search_public=[[NSMutableArray alloc]init];
            [empaty_array_search_public addObject:@"<null>"];
            [empaty_array_search_public addObject:@"null"];
            [empaty_array_search_public addObject:@""];
            [empaty_array_search_public addObject:@"(null)"];
            [empaty_array_search_public addObject:@"0"];
            
            NSMutableArray *Data_array_search_public=[[NSMutableArray alloc]init];
            
            
            if ([empaty_array_search_public containsObject:areaName]) {
                
            }
            else{
                
                [Data_array_search_public addObject:areaName];
            }
            if ([empaty_array_search_public containsObject:cityName]) {
                
            }
            else{
                [Data_array_search_public addObject:cityName];
            }
            
            if ([empaty_array_search_public containsObject:stateName]) {
                
            }
            else{
                [Data_array_search_public addObject:stateName];
            }
            
            if ([empaty_array_search_public containsObject:zipCode]) {
                
            }
            else{
                [Data_array_search_public addObject:zipCode];
            }
            
            NSMutableString *add_data_search_public=[[NSMutableString alloc]init];
            if (Data_array_search_public.count>0) {
                cell.rideDetails_publicFrndsCell_location_imageView.image=[UIImage imageNamed:@"check_in_placeholder"];
                
                for (int i=0; i<Data_array_search_public.count; i++) {
                    
                    NSString *adress=[Data_array_search_public objectAtIndex:i];
                    if (i==0) {
                        [add_data_search_public appendFormat:@"%@", [NSString stringWithFormat:@"%@",adress]];
                    }
                    else
                        [add_data_search_public appendFormat:@"%@", [NSString stringWithFormat:@",%@",adress]];
                }
                cell.rideInvites_publicFrndsCell_addressLabel.text=add_data_search_public;
                cell.rideInvites_viewTop_address_constraint.priority=750;
                cell.viewTop_username_constraint.priority=250;
            }
            else{
                cell.rideDetails_publicFrndsCell_location_imageView.image=[UIImage imageNamed:@""];
                cell.rideInvites_publicFrndsCell_addressLabel.text=@"";
                
                cell.rideInvites_viewTop_address_constraint.priority=250;
                cell.viewTop_username_constraint.priority=750;
            }
            
            NSString *profile_image_string1 = [NSString stringWithFormat:@"%@", [[rideInvitefilteredArray objectAtIndex:indexPath.row] objectForKey:@"profile_image"]];
            NSURL*url=[NSURL URLWithString:profile_image_string1];
            [cell.rideDetails_publicFrndsCell_profileImage sd_setImageWithURL:url
                                      placeholderImage:[UIImage imageNamed:@"user_Placeholder"]
                                               options:SDWebImageRefreshCached];
            
            
            if ([_isFrom isEqualToString:@"edit"]) {
            cell.rideInvites_publicFrnds_checkBox_btn.tag=indexPath.row;
            NSString*publicFrndID=[NSString stringWithFormat:@"%@",[[rideInvitefilteredArray objectAtIndex:indexPath.row] objectForKey:@"friend_id"]];
            NSString*publicFrndName=[NSString stringWithFormat:@"%@",[[rideInvitefilteredArray objectAtIndex:indexPath.row] objectForKey:@"name"]];
            
            if ([arrayOfSelectedPublicFrndsID containsObject:[NSString stringWithFormat:@"%@,%@",publicFrndID,publicFrndName]])
            {
                [cell.rideInvites_publicFrnds_checkBox_btn setBackgroundImage:[UIImage imageNamed:@"checkbox_fill"] forState:UIControlStateNormal];
                
                
            }
            else
            {
                [cell.rideInvites_publicFrnds_checkBox_btn setBackgroundImage:[UIImage imageNamed:@"checkbox_empty"] forState:UIControlStateNormal];
            }
            }
            else
            {
                cell.rideInvites_publicFrnds_checkBox_btn.tag=indexPath.row;
                NSString*publicFrndID=[NSString stringWithFormat:@"%@",[[rideInvitefilteredArray objectAtIndex:indexPath.row] objectForKey:@"friend_id"]];
                NSString*publicFrndName=[NSString stringWithFormat:@"%@",[[rideInvitefilteredArray objectAtIndex:indexPath.row] objectForKey:@"name"]];
                
                
                
                
                if ([arrayOfSelectedPublicFrndsID containsObject:[NSString stringWithFormat:@"%@,%@",publicFrndID,publicFrndName]])
                {
                    [cell.rideInvites_publicFrnds_checkBox_btn setBackgroundImage:[UIImage imageNamed:@"checkbox_fill"] forState:UIControlStateNormal];
                    
                    if ([_isFrom isEqualToString:@"edit"]) {
                        
                        if ([appDelegate.createGroup_frndInvitations containsObject:[NSString stringWithFormat:@"%@,%@",publicFrndID,publicFrndName]])
                        {
                            NSString*statusString=[NSString stringWithFormat:@"%@",[[rideInvitefilteredArray objectAtIndex:indexPath.row] objectForKey:@"friend_status"]];
                            cell.rideInvites_publicFrnds_statusView.hidden=NO;
                            if ([statusString isEqualToString:@"Accept"] || [statusString isEqualToString:@"accept"]) {
                                cell.rideInvites_PublicFrnds_statusLabel.text=@"Already added to the group";
                            }
                            else if ([statusString isEqualToString:@"Pending"] || [statusString isEqualToString:@"pending"])
                            {
                                cell.rideInvites_PublicFrnds_statusLabel.text=@"Already invited to the group";
                            }
                            else
                            {
                                cell.rideInvites_publicFrnds_statusView.hidden=YES;
                            }
                            cell.rideInvites_publicFrnds_checkBox_btn.userInteractionEnabled=NO;
                            
                        }
                        else
                        {
                            cell.rideInvites_publicFrnds_checkBox_btn.userInteractionEnabled=YES;
                            cell.rideInvites_publicFrnds_statusView.hidden=YES;
                            
                        }
                    }
                    
                }
                else
                {
                    [cell.rideInvites_publicFrnds_checkBox_btn setBackgroundImage:[UIImage imageNamed:@"checkbox_empty"] forState:UIControlStateNormal];
                    cell.rideInvites_publicFrnds_checkBox_btn.userInteractionEnabled=YES;
                    cell.rideInvites_publicFrnds_statusView.hidden=YES;
                }
                
            }
        
        }
        else{
            //            without Search
            if (indexPath.row%2==0) {
                cell.rideinvites_publicFrndsCell_bgView.backgroundColor=ROUTES_CELL_BG_COLOUR1;
            }
            else
                cell.rideinvites_publicFrndsCell_bgView.backgroundColor=ROUTES_CELL_BG_COLOUR2;
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            
            
            cell.rideDetails_publicFrndsCell_userName.text=[[arrayOfRideInvitePublicFriends objectAtIndex:indexPath.row] objectForKey:@"name"];
            
            NSString * areaName=[NSString stringWithFormat:@"%@",[[arrayOfRideInvitePublicFriends objectAtIndex:indexPath.row] objectForKey:@"address"]];
            NSString * cityName=[NSString stringWithFormat:@"%@",[[arrayOfRideInvitePublicFriends objectAtIndex:indexPath.row] objectForKey:@"city"]];
            NSString * stateName=[NSString stringWithFormat:@"%@",[[arrayOfRideInvitePublicFriends objectAtIndex:indexPath.row] objectForKey:@"state"]];
            NSString *zipCode=[NSString stringWithFormat:@"%@",[[arrayOfRideInvitePublicFriends objectAtIndex:indexPath.row] objectForKey:@"zipcode"]];
            
            NSMutableArray *empaty_array=[[NSMutableArray alloc]init];
            [empaty_array addObject:@"<null>"];
            [empaty_array addObject:@"null"];
            [empaty_array addObject:@""];
            [empaty_array addObject:@"(null)"];
            [empaty_array addObject:@"0"];
            
            NSMutableArray *Data_array=[[NSMutableArray alloc]init];
            
            
            if ([empaty_array containsObject:areaName]) {
                
            }
            else{
                
                [Data_array addObject:areaName];
            }
            if ([empaty_array containsObject:cityName]) {
                
            }
            else{
                [Data_array addObject:cityName];
            }
            
            if ([empaty_array containsObject:stateName]) {
                
            }
            else{
                [Data_array addObject:stateName];
            }
            
            if ([empaty_array containsObject:zipCode]) {
                
            }
            else{
                [Data_array addObject:zipCode];
            }
            
            NSMutableString *add_data=[[NSMutableString alloc]init];
            if (Data_array.count>0) {
                cell.rideDetails_publicFrndsCell_location_imageView.image=[UIImage imageNamed:@"check_in_placeholder"];
                
                for (int i=0; i<Data_array.count; i++) {
                    
                    NSString *adress=[Data_array objectAtIndex:i];
                    if (i==0) {
                        [add_data appendFormat:@"%@", [NSString stringWithFormat:@"%@",adress]];
                    }
                    else
                        [add_data appendFormat:@"%@", [NSString stringWithFormat:@",%@",adress]];
                }
                cell.rideInvites_publicFrndsCell_addressLabel.text=add_data;
                cell.rideInvites_viewTop_address_constraint.priority=750;
                cell.viewTop_username_constraint.priority=250;
            }
            else{
                cell.rideDetails_publicFrndsCell_location_imageView.image=[UIImage imageNamed:@""];
                cell.rideInvites_publicFrndsCell_addressLabel.text=@"";
                
                cell.rideInvites_viewTop_address_constraint.priority=250;
                cell.viewTop_username_constraint.priority=750;
            }
            
            
            NSString *profile_image_string1 = [NSString stringWithFormat:@"%@", [[arrayOfRideInvitePublicFriends objectAtIndex:indexPath.row] objectForKey:@"profile_image"]];
            NSURL*url=[NSURL URLWithString:profile_image_string1];
            [cell.rideDetails_publicFrndsCell_profileImage sd_setImageWithURL:url
                                      placeholderImage:[UIImage imageNamed:@"user_Placeholder"]
                                               options:SDWebImageRefreshCached];
            
            cell.rideInvites_publicFrnds_checkBox_btn.tag=indexPath.row;
            
            
            NSString*publicFrndID=[NSString stringWithFormat:@"%@",[[arrayOfRideInvitePublicFriends objectAtIndex:indexPath.row] objectForKey:@"friend_id"]];
            NSString*publicFrndName=[NSString stringWithFormat:@"%@",[[arrayOfRideInvitePublicFriends objectAtIndex:indexPath.row] objectForKey:@"name"]];
            
                if ([arrayOfSelectedPublicFrndsID containsObject:[NSString stringWithFormat:@"%@,%@",publicFrndID,publicFrndName]])
                {
                    [cell.rideInvites_publicFrnds_checkBox_btn setBackgroundImage:[UIImage imageNamed:@"checkbox_fill"] forState:UIControlStateNormal];
                    
                     if ([_isFrom isEqualToString:@"edit"]) {
                         
                         if ([appDelegate.createGroup_frndInvitations containsObject:[NSString stringWithFormat:@"%@,%@",publicFrndID,publicFrndName]])
                         {
                             NSString*statusString=[NSString stringWithFormat:@"%@",[[arrayOfRideInvitePublicFriends objectAtIndex:indexPath.row] objectForKey:@"friend_status"]];
                             cell.rideInvites_publicFrnds_statusView.hidden=NO;
                             if ([statusString isEqualToString:@"accept"] || [statusString isEqualToString:@"Accept"]) {
                                 cell.rideInvites_PublicFrnds_statusLabel.text=@"Already added to the group";
                             }
                             else if ([statusString isEqualToString:@"pending"] || [statusString isEqualToString:@"Pending"])
                             {
                                 cell.rideInvites_PublicFrnds_statusLabel.text=@"Already invited to the group";
                             }
                             else
                             {
                                 cell.rideInvites_publicFrnds_statusView.hidden=YES;
                             }
                             cell.rideInvites_publicFrnds_checkBox_btn.userInteractionEnabled=NO;
                             
                         }
                         else
                         {
                             cell.rideInvites_publicFrnds_checkBox_btn.userInteractionEnabled=YES;
                             cell.rideInvites_publicFrnds_statusView.hidden=YES;

                         }
                     }
                    
                }
                else
                {
                    [cell.rideInvites_publicFrnds_checkBox_btn setBackgroundImage:[UIImage imageNamed:@"checkbox_empty"] forState:UIControlStateNormal];
                     cell.rideInvites_publicFrnds_checkBox_btn.userInteractionEnabled=YES;
                     cell.rideInvites_publicFrnds_statusView.hidden=YES;
                }
            
            
        }
        return cell;
        
    }
    else
    {
        RideInvitesMyFrndsCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
        
        if (cell == nil)
        {
            cell = [[RideInvitesMyFrndsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        }
        if (rideInvite_isFiltered) {
            
            if (indexPath.row%2==0) {
                cell.rideinvites_myFrndsCell_bgView.backgroundColor=ROUTES_CELL_BG_COLOUR1;
            }
            else
                cell.rideinvites_myFrndsCell_bgView.backgroundColor=ROUTES_CELL_BG_COLOUR2;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.rideInvites_mrFrndsCell_userName_Label.text=[[rideInvitefilteredArray2 objectAtIndex:indexPath.row] objectForKey:@"name"];
            cell.rideInvites_mrFrndsCell_userName_Label.textColor=APP_YELLOW_COLOR;
            
            NSString * areaName=[NSString stringWithFormat:@"%@",[[rideInvitefilteredArray2 objectAtIndex:indexPath.row] objectForKey:@"address"]];
            NSString * cityName=[NSString stringWithFormat:@"%@",[[rideInvitefilteredArray2 objectAtIndex:indexPath.row] objectForKey:@"city"]];
            NSString * stateName=[NSString stringWithFormat:@"%@",[[rideInvitefilteredArray2 objectAtIndex:indexPath.row] objectForKey:@"state"]];
            NSString *zipCode=[NSString stringWithFormat:@"%@",[[rideInvitefilteredArray2 objectAtIndex:indexPath.row] objectForKey:@"zipcode"]];
            NSMutableArray *empaty_array2=[[NSMutableArray alloc]init];
            [empaty_array2 addObject:@"<null>"];
            [empaty_array2 addObject:@"null"];
            [empaty_array2 addObject:@""];
            [empaty_array2 addObject:@"(null)"];
            [empaty_array2 addObject:@"0"];
            
            NSMutableArray *Data_array_location=[[NSMutableArray alloc]init];
            
            
            if ([empaty_array2 containsObject:areaName]) {
                
            }
            else{
                
                [Data_array_location addObject:areaName];
            }
            if ([empaty_array2 containsObject:cityName]) {
                
            }
            else{
                [Data_array_location addObject:cityName];
            }
            
            if ([empaty_array2 containsObject:stateName]) {
                
            }
            else{
                [Data_array_location addObject:stateName];
            }
            
            if ([empaty_array2 containsObject:zipCode]) {
                
            }
            else{
                [Data_array_location addObject:zipCode];
            }
            
            NSMutableString *add_data2=[[NSMutableString alloc]init];
            if (Data_array_location.count>0) {
                cell.rideInvites_myFrndsCell_location_imageView.image=[UIImage imageNamed:@"check_in_placeholder"];
                
                for (int i=0; i<Data_array_location.count; i++) {
                    
                    NSString *adress=[Data_array_location objectAtIndex:i];
                    if (i==0) {
                        [add_data2 appendFormat:@"%@", [NSString stringWithFormat:@"%@",adress]];
                    }
                    else
                        [add_data2 appendFormat:@"%@", [NSString stringWithFormat:@",%@",adress]];
                }
                cell.rideInvites_myFrndsCell_address_label.text=add_data2;
                cell.rideInvites_myFrnds_viewTop_address_constraint.priority=750;
                cell.rideInvites_myFrnds_viewTop_userName_constraint.priority=250;

            }
            else{
                cell.rideInvites_myFrndsCell_location_imageView.image=[UIImage imageNamed:@""];
                cell.rideInvites_myFrndsCell_address_label.text=@"";
                cell.rideInvites_myFrnds_viewTop_address_constraint.priority=250;
                cell.rideInvites_myFrnds_viewTop_userName_constraint.priority=750;
            }
            
            
            
            NSString *profile_image_string = [NSString stringWithFormat:@"%@", [[rideInvitefilteredArray2 objectAtIndex:indexPath.row] objectForKey:@"profile_image"]];
            NSURL*url1=[NSURL URLWithString:profile_image_string];
            [cell.rideInvites_myFrndCell_ProfileImage sd_setImageWithURL:url1
                                          placeholderImage:[UIImage imageNamed:@"user_Placeholder"]
                                                   options:SDWebImageRefreshCached];
            cell.rideInvites_mrFrndsCell_checkbox_btn.tag=indexPath.row;
            NSString*myFrndID=[NSString stringWithFormat:@"%@",[[rideInvitefilteredArray2 objectAtIndex:indexPath.row] objectForKey:@"friend_id"]];
            NSString*myFrndName=[NSString stringWithFormat:@"%@",[[rideInvitefilteredArray2 objectAtIndex:indexPath.row] objectForKey:@"name"]];
            
            
            
            
            if ([arrayOfSelectedPublicFrndsID containsObject:[NSString stringWithFormat:@"%@,%@",myFrndID,myFrndName]])
            {
                [cell.rideInvites_mrFrndsCell_checkbox_btn setBackgroundImage:[UIImage imageNamed:@"checkbox_fill"] forState:UIControlStateNormal];
                
                if ([_isFrom isEqualToString:@"edit"]) {
                    
                    if ([appDelegate.createGroup_frndInvitations containsObject:[NSString stringWithFormat:@"%@,%@",myFrndID,myFrndName]])
                    {
                        NSString*statusString=[NSString stringWithFormat:@"%@",[[rideInvitefilteredArray2 objectAtIndex:indexPath.row] objectForKey:@"group_status"]];
                        cell.rideInvites_myFrnds_statusView.hidden=NO;
                        if ([statusString isEqualToString:@"accept"] || [statusString isEqualToString:@"Accept"]) {
                            cell.rideInvites_myFrnds_statusLabel.text=@"Already added to the group";
                        }
                        else if ([statusString isEqualToString:@"pending"] || [statusString isEqualToString:@"Pending"])
                        {
                            cell.rideInvites_myFrnds_statusLabel.text=@"Already invited to the group";
                        }
                        else
                        {
                            cell.rideInvites_myFrnds_statusView.hidden=YES;
                        }
                        cell.rideInvites_mrFrndsCell_checkbox_btn.userInteractionEnabled=NO;
                        
                    }
                    else
                    {
                        cell.rideInvites_mrFrndsCell_checkbox_btn.userInteractionEnabled=YES;
                        cell.rideInvites_myFrnds_statusView.hidden=YES;
                        
                    }
                }
                
            }
            else
            {
                [cell.rideInvites_mrFrndsCell_checkbox_btn setBackgroundImage:[UIImage imageNamed:@"checkbox_empty"] forState:UIControlStateNormal];
                cell.rideInvites_mrFrndsCell_checkbox_btn.userInteractionEnabled=YES;
                cell.rideInvites_myFrnds_statusView.hidden=YES;
            }
            

        }
        else{
            
            //            without Search
            if (indexPath.row%2==0) {
                cell.rideinvites_myFrndsCell_bgView.backgroundColor=ROUTES_CELL_BG_COLOUR1;
            }
            else
                cell.rideinvites_myFrndsCell_bgView.backgroundColor=ROUTES_CELL_BG_COLOUR2;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.rideInvites_mrFrndsCell_userName_Label.text=[[arrayOfRideInviteMyFriends objectAtIndex:indexPath.row] objectForKey:@"name"];
            cell.rideInvites_mrFrndsCell_userName_Label.textColor=APP_YELLOW_COLOR;
            NSString * areaName=[NSString stringWithFormat:@"%@",[[arrayOfRideInviteMyFriends objectAtIndex:indexPath.row] objectForKey:@"address"]];
            NSString * cityName=[NSString stringWithFormat:@"%@",[[arrayOfRideInviteMyFriends objectAtIndex:indexPath.row] objectForKey:@"city"]];
            NSString * stateName=[NSString stringWithFormat:@"%@",[[arrayOfRideInviteMyFriends objectAtIndex:indexPath.row] objectForKey:@"state"]];
            NSString *zipCode=[NSString stringWithFormat:@"%@",[[arrayOfRideInviteMyFriends objectAtIndex:indexPath.row] objectForKey:@"zipcode"]];
            //            NSString *status_null;
            
            
            NSMutableArray *empaty_array1=[[NSMutableArray alloc]init];
            [empaty_array1 addObject:@"<null>"];
            [empaty_array1 addObject:@"null"];
            [empaty_array1 addObject:@""];
            [empaty_array1 addObject:@"(null)"];
            [empaty_array1 addObject:@"0"];
            
            NSMutableArray *Data_array=[[NSMutableArray alloc]init];
            
            
            if ([empaty_array1 containsObject:areaName]) {
                
            }
            else{
                
                [Data_array addObject:areaName];
            }
            if ([empaty_array1 containsObject:cityName]) {
                
            }
            else{
                [Data_array addObject:cityName];
            }
            
            if ([empaty_array1 containsObject:stateName]) {
                
            }
            else{
                [Data_array addObject:stateName];
            }
            
            if ([empaty_array1 containsObject:zipCode]) {
                
            }
            else{
                [Data_array addObject:zipCode];
            }
            
            NSMutableString *add_data1=[[NSMutableString alloc]init];
            if (Data_array.count>0) {
                cell.rideInvites_myFrndsCell_location_imageView.image=[UIImage imageNamed:@"check_in_placeholder"];
                
                for (int i=0; i<Data_array.count; i++) {
                    
                    NSString *adress=[Data_array objectAtIndex:i];
                    if (i==0) {
                        [add_data1 appendFormat:@"%@", [NSString stringWithFormat:@"%@",adress]];
                    }
                    else
                        [add_data1 appendFormat:@"%@", [NSString stringWithFormat:@",%@",adress]];
                }
                cell.rideInvites_myFrndsCell_address_label.text=add_data1;
                cell.rideInvites_myFrnds_viewTop_address_constraint.priority=750;
                cell.rideInvites_myFrnds_viewTop_userName_constraint.priority=250;
            }
            else{
                cell.rideInvites_myFrndsCell_location_imageView.image=[UIImage imageNamed:@""];
                cell.rideInvites_myFrndsCell_address_label.text=@"";
                
                cell.rideInvites_myFrnds_viewTop_address_constraint.priority=250;
                cell.rideInvites_myFrnds_viewTop_userName_constraint.priority=750;
            }
                 
            NSString *profile_image_string = [NSString stringWithFormat:@"%@", [[arrayOfRideInviteMyFriends objectAtIndex:indexPath.row] objectForKey:@"profile_image"]];
            NSURL*url1=[NSURL URLWithString:profile_image_string];
            [cell.rideInvites_myFrndCell_ProfileImage sd_setImageWithURL:url1
                                          placeholderImage:[UIImage imageNamed:@"user_Placeholder"]
                                                   options:SDWebImageRefreshCached];
            cell.rideInvites_mrFrndsCell_checkbox_btn.tag=indexPath.row;
            NSString*myFrndID=[NSString stringWithFormat:@"%@",[[arrayOfRideInviteMyFriends objectAtIndex:indexPath.row] objectForKey:@"friend_id"]];
            NSString*myFrndName=[NSString stringWithFormat:@"%@",[[arrayOfRideInviteMyFriends objectAtIndex:indexPath.row] objectForKey:@"name"]];
            
            
            
            
            if ([arrayOfSelectedPublicFrndsID containsObject:[NSString stringWithFormat:@"%@,%@",myFrndID,myFrndName]])
            {
                [cell.rideInvites_mrFrndsCell_checkbox_btn setBackgroundImage:[UIImage imageNamed:@"checkbox_fill"] forState:UIControlStateNormal];
                
                if ([_isFrom isEqualToString:@"edit"]) {
                    
                    if ([appDelegate.createGroup_frndInvitations containsObject:[NSString stringWithFormat:@"%@,%@",myFrndID,myFrndName]])
                    {
                        NSString*statusString=[NSString stringWithFormat:@"%@",[[arrayOfRideInviteMyFriends objectAtIndex:indexPath.row] objectForKey:@"group_status"]];
                        cell.rideInvites_myFrnds_statusView.hidden=NO;
                        if ([statusString isEqualToString:@"accept"] || [statusString isEqualToString:@"Accept"]) {
                            cell.rideInvites_myFrnds_statusLabel.text=@"Already added to the group";
                        }
                        else if ([statusString isEqualToString:@"pending"] || [statusString isEqualToString:@"Pending"])
                        {
                            cell.rideInvites_myFrnds_statusLabel.text=@"Already invited to the group";
                        }
                        else
                        {
                            cell.rideInvites_myFrnds_statusView.hidden=YES;
                        }
                        cell.rideInvites_mrFrndsCell_checkbox_btn.userInteractionEnabled=NO;
                        
                    }
                    else
                    {
                        cell.rideInvites_mrFrndsCell_checkbox_btn.userInteractionEnabled=YES;
                        cell.rideInvites_myFrnds_statusView.hidden=YES;
                        
                    }
                }
                
            }
            else
            {
                [cell.rideInvites_mrFrndsCell_checkbox_btn setBackgroundImage:[UIImage imageNamed:@"checkbox_empty"] forState:UIControlStateNormal];
                cell.rideInvites_mrFrndsCell_checkbox_btn.userInteractionEnabled=YES;
                cell.rideInvites_myFrnds_statusView.hidden=YES;
            }
//            if ([arrayOfSelectedPublicFrndsID containsObject:[NSString stringWithFormat:@"%@,%@",myFrndID,myFrndName]])
//            {
//                [cell.rideInvites_mrFrndsCell_checkbox_btn setBackgroundImage:[UIImage imageNamed:@"checkbox_fill"] forState:UIControlStateNormal];
//            }
//            else
//            {
//                [cell.rideInvites_mrFrndsCell_checkbox_btn setBackgroundImage:[UIImage imageNamed:@"checkbox_empty"] forState:UIControlStateNormal];
//            }
         }
        
        return cell;
    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //     if ([view_Status isEqualToString:@""]||[view_Status isEqualToString:@"PUBLIC"]) {
    //    if (isFiltered) {
    //        
    //    }
    //         
    //     }
    //     else{
    //         
    //     }
}

#pragma mark - Search methods
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)aSearchBar {
    self.rideInvites_searchBar.placeholder = @"search";
    self.rideInvites_searchBar.showsCancelButton = YES;
    return YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)SearchBar
{
    if([view_Status_rideInvite_frnds isEqualToString:@""]||[view_Status_rideInvite_frnds isEqualToString:@"PUBLIC"])
    {
        _rideInvites_searchBar.text = nil;
        self.rideInvites_searchBar.placeholder = @"search";
        self.rideInvites_searchBar.showsCancelButton = NO;
        [_rideInvites_searchBar resignFirstResponder];
        rideInvite_isFiltered = NO;
         [_rideInvites_public_frnds_tableView setContentOffset:CGPointZero animated:NO];
         [_rideInvites_public_frnds_tableView reloadData];
    }
    else{
        _rideInvites_searchBar.text = nil;
        self.rideInvites_searchBar.placeholder = @"search";
        self.rideInvites_searchBar.showsCancelButton = NO;
        [_rideInvites_searchBar resignFirstResponder];
        rideInvite_isFiltered = NO;
        [_rideInvites_myFrnds_tableView setContentOffset:CGPointZero animated:NO];
        [_rideInvites_myFrnds_tableView reloadData];
    }
    
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.rideInvites_searchBar resignFirstResponder];
    for (UIView *view in searchBar.subviews)
    {
        for (id subview in view.subviews)
        {
            if ( [subview isKindOfClass:[UIButton class]] )
            {
                [subview setEnabled:YES];
                
                NSLog(@"enableCancelButton");
                return;
            }
        }
    }
    
    [searchBar setShowsCancelButton:YES animated:YES];
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
    if(searchText.length == 0)
    {
        rideInvite_isFiltered = NO;
    }
    else
    {
        if([view_Status_rideInvite_frnds isEqualToString:@""]||[view_Status_rideInvite_frnds isEqualToString:@"PUBLIC"])
        {
            
            
            
            rideInvite_isFiltered = YES;
            rideInvitefilteredArray = [[NSMutableArray alloc]init];
            
            for(NSDictionary *frnds_Dict in arrayOfRideInvitePublicFriends)
            {
                NSString *friend_Name=[frnds_Dict valueForKey:@"name"];
                
                NSRange stringRange = [friend_Name rangeOfString:searchText options:NSCaseInsensitiveSearch];
                
                if(stringRange.location != NSNotFound)
                {
                    [rideInvitefilteredArray addObject:frnds_Dict];
                    
                }
            }
            
            [_rideInvites_public_frnds_tableView reloadData];
        }
        else
        {
            rideInvite_isFiltered = YES;
            rideInvitefilteredArray2 = [[NSMutableArray alloc]init];
            
            for(NSDictionary *frnds_Dict1 in arrayOfRideInviteMyFriends)
            {
                NSString *friend_Name1=[frnds_Dict1 valueForKey:@"name"];
                
                NSRange stringRange = [friend_Name1 rangeOfString:searchText options:NSCaseInsensitiveSearch];
                
                if(stringRange.location != NSNotFound)
                {
                    [rideInvitefilteredArray2 addObject:frnds_Dict1];
                    
                }
            }
            
            [_rideInvites_myFrnds_tableView reloadData];
        }
    }
    
    
}

- (IBAction)onClick_rideInviteFrnds_rideGoingBtn:(id)sender {
    
    self.rideInviteFrnds_scrollLabel.hidden=YES;
    self.rideInviteFrnds_rideGoingBtn.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }

}
@end
