//
//  Promotion_DetailViewController.m
//  openroadrides
//
//  Created by SrkIosEbiz on 27/09/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "Promotion_DetailViewController.h"

@interface Promotion_DetailViewController ()

@end

@implementation Promotion_DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"FEATURED PROMOTION";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    
    self.promotion_detail_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.promotion_detail_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.promotion_detail_scrollLabel.textColor = [UIColor blackColor];
    self.promotion_detail_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.promotion_detail_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.promotion_detail_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.promotion_detail_scrollLabel.scrollSpeed = 60; // pixels per second
    self.promotion_detail_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.promotion_detail_scrollLabel.fadeLength = 0.f;
    self.promotion_detail_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.promotion_detail_scrollLabel observeApplicationNotifications];
    if (_promotion_detail_onGoingRide) {
        self.promotion_detail_scrollLabel.hidden=YES;
        
        
        self.promotion_detail_rideGoingBtn.hidden=YES;
    }
    else
    {
        
        if (appDelegate.ridedashboard_home) {
            self.promotion_detail_scrollLabel.hidden=NO;
            self.promotion_detail_rideGoingBtn.hidden=NO;
            
        }
        else
        {
            self.promotion_detail_scrollLabel.hidden=YES;
            self.promotion_detail_rideGoingBtn.hidden=YES;
        }

       
    }
    
    [self.navigationController.navigationBar setTranslucent:NO];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [leftBtn addTarget:self action:@selector(back_Action) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.frame = CGRectMake(0, 0, 25, 25);
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"back_Image"] forState:UIControlStateNormal];
    UIBarButtonItem *leftBackButton=[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:leftBackButton, nil];

    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.frame=CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
    activeIndicatore.color = APP_YELLOW_COLOR;
    activeIndicatore.hidesWhenStopped = TRUE;
    [activeIndicatore startAnimating];
    [self.view addSubview:activeIndicatore];
    
    
    [self get_promotions_Details];

}

-(void)back_Action{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)get_promotions_Details
{
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
    
    [SHARED_API get_feature_promotion:_promotion_id withSuccess:^(NSDictionary *response) {
         NSLog(@"%@", response);
        if ([[response valueForKey:STATUS] isEqualToString:SUCCESS]) {
        [activeIndicatore stopAnimating];
            
        NSDateFormatter *  YMD_formatter = [[NSDateFormatter alloc] init];
        YMD_formatter.dateFormat = @"yyyy-MM-dd";
        NSDateFormatter *  MDY_formatter = [[NSDateFormatter alloc] init];
        MDY_formatter.dateFormat = @"MM/dd/yyyy";
        [MDY_formatter setTimeZone:[NSTimeZone localTimeZone]];
            
            
        NSDictionary *data=[[response valueForKey:@"featured_promotion_details"]objectAtIndex:0];
        NSString *img_str=[data valueForKey:@"featured_promotion_image"];
        NSString *title=[data valueForKey:@"title"];
        NSString *offer_name=[data valueForKey:@"bike_name"];
        NSString *offer_model=[data valueForKey:@"bike_model"];
        NSString *ofer_details=[NSString stringWithFormat:@"%@/%@",offer_name,offer_model];
            
            
        NSString *start_date=[data valueForKey:@"featured_start_date"];
        NSString *end_date=[data valueForKey:@"featured_end_date"];
        NSDate *TZ_date = [YMD_formatter dateFromString:start_date];
        start_date = [MDY_formatter stringFromDate:TZ_date];
            
        NSDate *TZ_date2 = [YMD_formatter dateFromString:end_date];
        end_date = [MDY_formatter stringFromDate:TZ_date2];
        
        NSString *date=[NSString stringWithFormat:@"Schedule:%@ to %@",start_date,end_date];
         
        NSString *des=[data valueForKey:@"description"];
            
        _promotion_des.text=des;
        _promotion_end_date.text=date;
        _offer_model_name.text=ofer_details;
        _promotion_title.text=title;
        NSURL*url=[NSURL URLWithString:img_str];
        [_promotion_image sd_setImageWithURL:url
                                   placeholderImage:[UIImage imageNamed:@"add_places_placeholder"]
                                            options:SDWebImageRefreshCached];
            
            
            
        }
        else
        {
            [SHARED_HELPER showAlert:ServiceFail];
            int duration = 1; // duration in seconds
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [self back_Action];
            });
        }
        
    } onfailure:^(NSError *theError) {
        [self network_Alert:@""];
        [activeIndicatore stopAnimating];
         NSLog(@"%@", theError);
        
    }];
}
-(void)network_Alert:(NSString *)call_from{
    UIAlertController *alertview = [UIAlertController alertControllerWithTitle:@"Network Error" message:Nonetwork preferredStyle:UIAlertControllerStyleAlert];
    
    [alertview addAction:[UIAlertAction actionWithTitle:@"retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self get_promotions_Details];
    }]];
    
    [self presentViewController:alertview animated:YES completion:nil];
}

- (IBAction)onClick_promotion_detail_rideGoingBtn:(id)sender {
    
    self.promotion_detail_scrollLabel.hidden=YES;
    self.promotion_detail_rideGoingBtn.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }

}
@end
