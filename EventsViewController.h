//
//  EventsViewController.h
//  openroadrides
//
//  Created by apple on 06/06/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "MenuTableViewCell.h"
#import "RidesController.h"
#import "FriendsViewController.h"
#import "DashboardViewController.h"
#import "GroupsViewController.h"
#import "ProfileView.h"
#import "CBAutoScrollLabel.h"

@interface EventsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>{
    UIActivityIndicatorView  *activeIndicatore;
    NSString *Event_id_str,*view_Status,*Events_Called,*Charatable_Called,*my_events_called;
     int friends_count,groups_count;
    
    UISwipeGestureRecognizer *swipe_right;
    UISwipeGestureRecognizer *swipe_left;
}
@property (weak, nonatomic) IBOutlet UIView *main_content_vw;
@property (weak, nonatomic) IBOutlet UITableView *events_table_vw;
@property (weak, nonatomic) IBOutlet UIScrollView *events_scrollView;
@property (weak, nonatomic) IBOutlet UIView *events_mainView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *events_mainView_width_constraint;
@property (weak, nonatomic) IBOutlet UIView *events_baseView;
@property (weak, nonatomic) IBOutlet UIView *charitableEvents_baseView;
@property (weak, nonatomic) IBOutlet UITableView *charitableEvents_tableView;
@property (weak, nonatomic) IBOutlet UILabel *noCharitableEventsLabel;
@property UIRefreshControl *refreshControl,*refreshControl2,*refreshControl3;
@property (strong, nonatomic) IBOutlet UIView *side_Menu_view;
@property (strong, nonatomic) IBOutlet UIImageView *User_image;
@property (strong, nonatomic) IBOutlet UILabel *User_Name;
@property (strong, nonatomic) IBOutlet UITableView *side_Menu_Table;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *menu_Leading_Constraint;
- (IBAction)Profile_Action:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *no_Data_Label;
@property (weak, nonatomic) IBOutlet UILabel *noEVENTSLabel;
@property (weak, nonatomic) IBOutlet UIView *segementHeadersView;
@property (weak, nonatomic) IBOutlet UIView *event_segement_view;
@property (weak, nonatomic) IBOutlet UILabel *event_segment_label;
@property (weak, nonatomic) IBOutlet UIButton *event_segment_btn;
- (IBAction)onClick_event_segment_btn:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *charitable_segement_view;
@property (weak, nonatomic) IBOutlet UILabel *charitable_segement_label;
@property (weak, nonatomic) IBOutlet UIButton *charitable_segment_btn;
@property (weak, nonatomic) IBOutlet UILabel *my_events_label;

- (IBAction)onClick_charitable_segment_btn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *my_events_action;
- (IBAction)my_events_action:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *my_events_view;
@property (weak, nonatomic) IBOutlet UIView *my_events_view_is;
@property (weak, nonatomic) IBOutlet UITableView *my_events_table;
@property (weak, nonatomic) IBOutlet UILabel *my_events_no_data_label;

@property (weak, nonatomic) IBOutlet UIButton *events_rideGoingBtn;
- (IBAction)onClick_rideGoingBtn:(id)sender;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *events_scrollLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint_eventsScrollView;


@end
