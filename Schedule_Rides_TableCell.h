//
//  Schedule_Rides_TableCell.h
//  openroadrides
//
//  Created by SrkIosEbiz on 18/07/18.
//  Copyright © 2018 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Schedule_Rides_TableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *bg_View;
@property (weak, nonatomic) IBOutlet UIImageView *image_view;
@property (weak, nonatomic) IBOutlet UILabel *ride_Name;
@property (weak, nonatomic) IBOutlet UILabel *start_Place;
@property (weak, nonatomic) IBOutlet UILabel *start_Date;
@property (weak, nonatomic) IBOutlet UIImageView *time_Icon;
@property (weak, nonatomic) IBOutlet UILabel *time_Start;
@property (weak, nonatomic) IBOutlet UIButton *go_Button;

@end
