//
//  GroupsTableViewCell.h
//  openroadrides
//
//  Created by apple on 31/05/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *groups_content_view;
@property (weak, nonatomic) IBOutlet UIImageView *groups_img;
@property (weak, nonatomic) IBOutlet UILabel *title_lbl;
@property (weak, nonatomic) IBOutlet UILabel *members_lbl;
@property (weak, nonatomic) IBOutlet UILabel *createdBy_label;

@end
