//
//  MarketPlace.h
//  openroadrides
//
//  Created by apple on 26/09/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MarketPlaceCollectionViewCell.h"
#import "MyPostsCollectionViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "APIHelper.h"
#import "AppHelperClass.h"
#import "Constants.h"
#import <GooglePlaces/GooglePlaces.h>
#import "MarketPlaceDetail.h"
#import "MypostDetailView.h"
#import "CreatePostView.h"
#import "CBAutoScrollLabel.h"
@interface MarketPlace : UIViewController<CLLocationManagerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UIActionSheetDelegate,UIScrollViewDelegate>
{
    CLLocationManager *marketPlace_locationManager;
    UIActivityIndicatorView  *activeIndicatore;
    UIView *indicaterview;
    NSString *toggleString,*marketPlacePostIDString,*categoryIDString,*myPostCategoryIDString,*AddCatString, *isPostAvailable_annotation;
    
    
}
@property (weak, nonatomic) IBOutlet UIView *marketPlaceContentView;
@property (weak, nonatomic) IBOutlet UICollectionView *marketPlace_collectionView;
@property (strong, nonatomic) NSMutableArray *arrayOfMarketPlaceList,*arrayOfMyPostsList,*arrayOfCategoryTypes,*arrayOfCategoryTypeIDs;
@property (weak, nonatomic) IBOutlet UIView *myPostsContentView;
@property (weak, nonatomic) IBOutlet UIButton *marketPlaceToggleButton;
- (IBAction)onClickMarketPlaceToggleBtn:(id)sender;

@property  BOOL boolvalue_marketPlaceToggle;
@property (weak, nonatomic) IBOutlet UIImageView *toogle_market_post_imageView;
@property (weak, nonatomic) IBOutlet UICollectionView *myPost_collectionView;
@property (weak, nonatomic) IBOutlet UILabel *noPostsDataLabel;
@property (weak, nonatomic) IBOutlet UILabel *noMarketPlacesData_Label;
@property (weak, nonatomic) IBOutlet UILabel *categoryTypeLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *categoryLabelHeightConstraint;
- (IBAction)onClick_categoryBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *categoryBtn;
@property (weak, nonatomic) IBOutlet UIView *marketPlace_filterView;
@property (weak, nonatomic) IBOutlet UIView *myPost_filterView;
@property (weak, nonatomic) IBOutlet UILabel *myPost_filterLabel;
@property (weak, nonatomic) IBOutlet UIButton *myPost_filterBtn;
- (IBAction)onClick_filterButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *marketPlace_rideGoingBtn;
- (IBAction)onClick_marketPlace_rideGoingBtn:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomContraint_marketPlaceTglBtnImage;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *marketPlace_scrollLabel;
@property (weak, nonatomic) IBOutlet UIImageView *marketPlace_filter_imageView;


@end
