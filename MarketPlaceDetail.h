//
//  MarketPlaceDetail.h
//  openroadrides
//
//  Created by apple on 28/09/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "APIHelper.h"
#import "AppHelperClass.h"
#import "Constants.h"
#import "ConnectSellerView.h"
#import "BIZPopupViewController.h"
#import "CBAutoScrollLabel.h"
@interface MarketPlaceDetail : UIViewController<UIScrollViewDelegate>
{
    UIActivityIndicatorView  *activeIndicatore;
    NSMutableDictionary *marketPlaceDetailDict;
    NSArray *imagearayy;
    NSMutableArray *marketImagesArray;
    UIPageControl *pageControl;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIScrollView *multipleImages_scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *marketProfieImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *postedLabel;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *address_imageView;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIImageView *phoneNumberImageView;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberLabel;
@property (weak, nonatomic) IBOutlet UIImageView *emailImageView;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIView *descriptionBorderLineView;
@property (weak, nonatomic) IBOutlet UIView *titleBorderlineView;
@property (weak, nonatomic) IBOutlet UIButton *intrestedBtn;
- (IBAction)onClickIntrestedBtn:(id)sender;
@property (strong, nonatomic) NSString *marketPlaceId;
@property (weak, nonatomic) IBOutlet UILabel *categoryTypeLabel;
@property (weak, nonatomic) IBOutlet UIButton *marketPlaceDetail_rideGoinBtn;
- (IBAction)onClick_marketPlaceDeatil_rideGoingBtn:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint_intrestedBtn;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *marketPlaceDetail_scrollLabel;

@end
