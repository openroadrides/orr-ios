//
//  EditProfileViewController.h
//  openroadrides
//
//  Created by SrkIosEbiz on 12/07/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "AppHelperClass.h"
#import "APIHelper.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AppDelegate.h"
#import "CBAutoScrollLabel.h"
@interface EditProfileViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPickerViewDelegate, UIPickerViewDataSource,UITextFieldDelegate>
{
    NSString *genderString;
    UIImage *cameraImage;
    NSData *profileImgData;
    NSString *base64String,*frame;
    UIActivityIndicatorView *activeIndicatore;
    NSMutableArray *dataArray, *stateArray;
    NSDictionary *mapping ;
    
    UIView *pickerBaseView, *contentView, *popUpBaseView;
    UIButton *doneButton,*cancel_Button;
    UIPickerView *businessTypePickerView;
    BOOL Spickerchng;
    BOOL Cpickerchng;
    NSString *pickerclick;
    NSInteger selectedindexv;
    UIDatePicker *date;
    
    NSInteger selected_state_row;
    
}
@property(strong,nonatomic)NSString *user_image_url,*gender_Str;
@property(strong,nonatomic)NSDictionary *user_Data;
@property (strong, nonatomic) IBOutlet UIImageView *User_images;
- (IBAction)Profile_image_Action:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *gmail_label;
@property (strong, nonatomic) IBOutlet UITextField *user_mobile;
@property (strong, nonatomic) IBOutlet UITextField *user_Name;
@property (weak, nonatomic) IBOutlet UIImageView *Mailimageview;
@property (weak, nonatomic) IBOutlet UIImageView *Femailimageview;
- (IBAction)Gender_action:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *Male_outlet;

@property (weak, nonatomic) IBOutlet UIView *Gender_view;
@property (weak, nonatomic) IBOutlet UIButton *Female_outlet;
@property (strong, nonatomic) IBOutlet UITextField *adress_label;
@property (strong, nonatomic) IBOutlet UIScrollView *bg_Scrole_View;
@property (strong, nonatomic) IBOutlet UITextField *dob_Tf;
@property (strong, nonatomic) IBOutlet UITextField *state;
@property (strong, nonatomic) IBOutlet UITextField *city_tf;
- (IBAction)State_Action:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *City_Action;
- (IBAction)city_Action:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *zip_Code;
@property (weak, nonatomic) IBOutlet UIButton *editProfile_rideGoingBtn;
- (IBAction)onClick_editProfile_rideGoingBtn:(id)sender;


@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *editProfile_scrollLabel;


@end
