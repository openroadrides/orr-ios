//
//  SaveARoute.h
//  openroadrides
//
//  Created by apple on 30/05/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BIZPopupViewController.h"
#import "StarRatingBar.h"
#import "StarRatingView.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "BIZPopupViewControllerDelegate.h"

@interface SaveARoute : UIViewController<UITextViewDelegate,UIScrollViewDelegate,
UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate>
{
    float Rate;
//  UIInputViewControllerer *popupViewController;
    
    //Almost_Done_View
    UIButton *add_btn;
    UIButton *subtract_btn;
    StarRatingBar *starRatingView;
    
    NSString *MPCoachMarksShown_isFromSaveARoute;
}
@property (strong,nonatomic)NSString *seletced_route_title,*ride_Title_is,*ride_Owned_BY_ME,*route_des,*have_Ride_id,*ride_description;

@property (strong, nonatomic) NSString *is_From;
@property (strong, nonatomic) IBOutlet UIView *action_btn_BG_View;
@property (strong, nonatomic) IBOutlet UIView *Rating_View;
@property (strong, nonatomic) IBOutlet UIButton *first_rate_btn;
@property (strong, nonatomic) IBOutlet UIButton *second_rate_btn;
@property (strong, nonatomic) IBOutlet UIButton *third_rate_btn;
@property (strong, nonatomic) IBOutlet UIButton *fourth_rate_btn;
@property (strong, nonatomic) IBOutlet UIButton *fifth_rate_btn;
@property (strong, nonatomic) IBOutlet UIImageView *first_rating_Img;
@property (strong, nonatomic) IBOutlet UIImageView *Second_rate_Img;
@property (strong, nonatomic) IBOutlet UIImageView *third_rate_img;
@property (strong, nonatomic) IBOutlet UIImageView *Fourth_rate_Img;

@property (strong, nonatomic) IBOutlet UIImageView *Fifth_rate_img;
@property (strong, nonatomic) IBOutlet UITextField *Route_Title_Label;
@property (strong, nonatomic) IBOutlet UITextField *route_Des_Label;
@property (weak, nonatomic) IBOutlet UIButton *save_route_btnOutlet;


- (IBAction)Save_Route_Action:(id)sender;
- (IBAction)public_Action:(id)sender;
- (IBAction)private_Action:(id)sender;
-(IBAction)keyboardGoAway:(id)sender;
@property (strong, nonatomic) IBOutlet UITextView *coments_text_View;
@property (strong, nonatomic) IBOutlet UIView *save_route_View;
@property (strong, nonatomic) IBOutlet UIView *Almost_Done_View;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *save_Route_Leading;
@property (strong, nonatomic) IBOutlet UIView *save_Route_View;
@property (strong, nonatomic) IBOutlet UIView *save_Route_Public_View;
@property (strong, nonatomic) IBOutlet UIView *save_Route_Private_V;


//- (IBAction)Rating_Action:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UILabel *comments_Label;
@property (strong, nonatomic) IBOutlet UILabel *save_route_public_lbl;
@property (strong, nonatomic) IBOutlet UILabel *save_route_private_lbl;
@property (strong, nonatomic) IBOutlet UIImageView *public_icon_image;

@property (strong, nonatomic) IBOutlet UIImageView *private_icon_image;

@property (strong, nonatomic) IBOutlet UIView *Ratings_sub_view;
@property (weak, nonatomic) IBOutlet UIButton *cancel_Buton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rating_totitle_constraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rating_top_constraint;
@property (weak, nonatomic) IBOutlet UIView *route_name_line_;
@property (weak, nonatomic) IBOutlet UIView *des_line;
@property (weak, nonatomic) IBOutlet UIImageView *name_icon;
@property (weak, nonatomic) IBOutlet UIImageView *des_icon;
@property (weak, nonatomic) IBOutlet UILabel *tiltle_Page;


//All Most Done View

@property (strong, nonatomic) IBOutlet UIScrollView *imges_Scrole_view;
@property (weak, nonatomic) IBOutlet UIView *images_scroll_baseView;

@property (strong, nonatomic) IBOutlet UIButton *Almost_Done_Save_Action;
@property (strong, nonatomic) IBOutlet UITextField *Almost_Title;
@property (strong, nonatomic) IBOutlet UITextField *almost_Des;
@property (strong, nonatomic) IBOutlet UIView *public_bg_view;
@property (strong, nonatomic) IBOutlet UIView *private_Bg_View;
@property (strong, nonatomic) IBOutlet UIButton *Public_Action;
@property (strong, nonatomic) IBOutlet UIButton *Private_Action;
- (IBAction)Almost_Public_Action:(id)sender;
- (IBAction)Almost_private_Action:(id)sender;
- (IBAction)Almost_Don_Save_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *Almost_Don_PubliCPrivate_view;
@property (strong, nonatomic) IBOutlet UILabel *Almost_Public_Labl;
@property (strong, nonatomic) IBOutlet UILabel *almost_Private_Label;
@property (strong, nonatomic) IBOutlet UIView *alert_View;
@property (strong, nonatomic) IBOutlet UILabel *alert_Label;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *alert_View_Top;
- (IBAction)Save_Route_Cancel:(id)sender;


@end
