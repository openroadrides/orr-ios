//
//  EditRideController.m
//  openroadrides
//
//  Created by apple on 14/07/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "EditRideController.h"
#import "SelectFriendsAndGroups.h"
#define startlocation @"Select your start location."
#define ridenameempty @"Ride name should not be empty."
#define ride_Date @"Select your start date."
#define ride_Time @"Select your start time."
#define ridecreated @"Your schuduled ride is updated successfully."

@interface EditRideController ()

@end



@implementation EditRideController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"EDIT RIDE";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    
    appDelegate.Ride_Detail_Edited=@"";
    
    self.editRide_scrollLabel.hidden=YES;
    self.editRide_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.editRide_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.editRide_scrollLabel.textColor = [UIColor blackColor];
    self.editRide_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.editRide_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.editRide_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.editRide_scrollLabel.scrollSpeed = 60; // pixels per second
    self.editRide_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.editRide_scrollLabel.fadeLength = 0.f;
    self.editRide_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.editRide_scrollLabel observeApplicationNotifications];
    
    self.editRide_rideGoingBtn.hidden=YES;
    if (appDelegate.ridedashboard_home) {
        
        self.editRide_scrollLabel.hidden=NO;
        self.editRide_rideGoingBtn.hidden=NO;
    }
    UIButton *saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [saveBtn addTarget:self action:@selector(save_clicked) forControlEvents:UIControlEventTouchUpInside];
    saveBtn.frame = CGRectMake(0, 0, 30, 30);
    [saveBtn setBackgroundImage:[UIImage imageNamed:@"SAVE"] forState:UIControlStateNormal];
    UIBarButtonItem * create_Ride= [[UIBarButtonItem alloc] initWithCustomView:saveBtn];
    self.navigationItem.rightBarButtonItems=[NSArray arrayWithObjects:create_Ride, nil];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [leftBtn addTarget:self action:@selector(cancle_clicked:) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.frame = CGRectMake(0, 0, 25, 25);
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"close_icon"] forState:UIControlStateNormal];
    UIBarButtonItem *leftBackButton=[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    item0=leftBackButton;
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:item0, nil];
    
    
    [self.editRide_titleTF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.editRide_placeToStartTF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.editRide_startDateTF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.editRide_startTimeTF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.editRide_meetingAddress setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.editRide_meetingDateTF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.editRide_meetingTimeTF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.editRide_descriptionTF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    //Date Picker Allocation
    date1 = [[UIDatePicker alloc]init];
    
    
    
    
    self.editride_profile_imageView.layer.cornerRadius=self.editride_profile_imageView.frame.size.width/2;
    self.editride_profile_imageView.contentMode=UIViewContentModeScaleAspectFill;
    self.editride_profile_imageView.clipsToBounds=YES;
    arrayOfRemoveFrndsIDs= [[NSMutableArray alloc]init];
    
    NSUInteger len =8;
    NSString *letterd =@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (NSUInteger i = 0; i < len; i++) {
        u_int32_t r = arc4random() % [letterd length];
        unichar c = [letterd characterAtIndex:r];
        [randomString appendFormat:@"%C", c];
    }
    random_number_string=[NSString stringWithFormat:@"%@",randomString];
    NSLog(@"random Number %@",random_number_string);
    
    
    editRide_titleTF = [NSString stringWithFormat:@"%@",[_edit_Ride_Data valueForKey:@"ride_name"]];
    if ([editRide_titleTF isEqualToString:@"<null>"] || [editRide_titleTF isEqualToString:@""] || [editRide_titleTF isEqualToString:@"null"] || editRide_titleTF == nil)
    {
        self.editRide_titleTF.text = @"";
    }
    else{
        self.editRide_titleTF.text = editRide_titleTF;
    }
    
    editRide_placeToStartTF = [NSString stringWithFormat:@"%@",[_edit_Ride_Data valueForKey:@"ride_start_address"]];
    
    if ([editRide_placeToStartTF isEqualToString:@"<null>"] || [editRide_placeToStartTF isEqualToString:@""] || [editRide_placeToStartTF isEqualToString:@"null"] || editRide_placeToStartTF == nil)
    {
        self.editRide_placeToStartTF.text = @"";
        
    }
    else{
        self.editRide_placeToStartTF.text = editRide_placeToStartTF;
    }
    
    editRide_startDateTF = [NSString stringWithFormat:@"%@",[_edit_Ride_Data valueForKey:@"ride_start_date"]];
    if ([editRide_startDateTF isEqualToString:@"<null>"] || [editRide_startDateTF isEqualToString:@""] || [editRide_startDateTF isEqualToString:@"null"] || editRide_startDateTF == nil || [editRide_startDateTF isEqualToString:@"(null)"])
    {
        self.editRide_startDateTF.text = @"";
        
    }
    else{
        NSDateFormatter *dateformater = [[NSDateFormatter alloc ]init];
        [dateformater setDateFormat:@"YYYY-MM-dd"];
        NSDate *dob_date=[dateformater dateFromString:editRide_startDateTF];
        [dateformater setDateFormat:@"MM/dd/YYYY"];
        NSString *str_frm_date=[dateformater stringFromDate:dob_date];
        self.editRide_startDateTF.text = str_frm_date;
        date1.date=dob_date;
    }
    
    editRide_startTimeTF = [NSString stringWithFormat:@"%@",[_edit_Ride_Data valueForKey:@"ride_start_time"]];
    if ([editRide_startTimeTF isEqualToString:@"<null>"] || [editRide_startTimeTF isEqualToString:@""] || [editRide_startTimeTF isEqualToString:@"null"] || editRide_startTimeTF == nil)
    {
        self.editRide_startTimeTF.text = @"";
        
    }
    else{
        
        self.editRide_startTimeTF.text = editRide_startTimeTF;
    }
    
    editRide_meetingAddress = [NSString stringWithFormat:@"%@",[_edit_Ride_Data valueForKey:@"ride_meeting_address"]];
    if ([editRide_meetingAddress isEqualToString:@"<null>"] || [editRide_meetingAddress isEqualToString:@""] || [editRide_meetingAddress isEqualToString:@"null"] || editRide_meetingAddress == nil )
    {
        self.editRide_meetingAddress.text = @"";
    }
    else{
        self.editRide_meetingAddress.text = editRide_meetingAddress;
        
    }
    route_id_str = [NSString stringWithFormat:@"%@",[_edit_Ride_Data valueForKey:@"route_id"]];
    appDelegate.select_Route_Id_For_CreateAndFreeRide=route_id_str;
    appDelegate.select_Route_Address_For_CreateAndFreeRide=_editRide_placeToStartTF.text;
    appDelegate.start_address_latitude= [NSString stringWithFormat:@"%f",edit_start_latitude];
    appDelegate.start_address_longtitude=[NSString stringWithFormat:@"%f",edit_start_longitude];
    editRide_meetingDateTF = [NSString stringWithFormat:@"%@",[_edit_Ride_Data valueForKey:@"ride_meeting_date"]];
    if ([editRide_meetingDateTF isEqualToString:@"<null>"] || [editRide_meetingDateTF isEqualToString:@""] || [editRide_meetingDateTF isEqualToString:@"null"] || editRide_meetingDateTF == nil||[editRide_meetingDateTF isEqualToString:@"0000-00-00"])
    {
        self.editRide_meetingDateTF.text = @"";
    }
    else{
        NSDateFormatter *dateformater = [[NSDateFormatter alloc ]init];
        [dateformater setDateFormat:@"YYYY-MM-dd"];
        NSDate *dob_date=[dateformater dateFromString:editRide_meetingDateTF];
        [dateformater setDateFormat:@"MM/dd/YYYY"];
        NSString *str_frm_date=[dateformater stringFromDate:dob_date];
        self.editRide_meetingDateTF.text = str_frm_date;
    }
    
    editRide_meetingTimeTF = [NSString stringWithFormat:@"%@",[_edit_Ride_Data valueForKey:@"ride_meeting_time"]];
    if ([editRide_meetingTimeTF isEqualToString:@"<null>"] || [editRide_meetingTimeTF isEqualToString:@""] || [editRide_meetingTimeTF isEqualToString:@"null"] || editRide_meetingTimeTF == nil||[editRide_meetingTimeTF isEqualToString:@"00:00:00"])
    {
        self.editRide_meetingTimeTF.text = @"";
    }
    else{
        self.editRide_meetingTimeTF.text = editRide_meetingTimeTF;
        
    }
    editRide_descriptionTF = [NSString stringWithFormat:@"%@",[_edit_Ride_Data valueForKey:@"ride_description"]];
    if ([editRide_descriptionTF isEqualToString:@"<null>"] || [editRide_descriptionTF isEqualToString:@""] || [editRide_descriptionTF isEqualToString:@"null"] || editRide_descriptionTF == nil)
    {
        self.editRide_descriptionTF.text = @"";
    }
    else
    {
        self.editRide_descriptionTF.text = editRide_descriptionTF;
        
    }
      imageurlstrng = [NSString stringWithFormat:@"%@",[_edit_Ride_Data valueForKey:@"ride_image_url"]];
    if ([imageurlstrng isEqualToString:@"<null>"] || [imageurlstrng isEqualToString:@""] || [imageurlstrng isEqualToString:@"null"] || imageurlstrng == nil)
    {
        imageurlstrng=@"";
    }
    else
    {
        [_editride_profile_imageView sd_setImageWithURL:[NSURL URLWithString:imageurlstrng]
                                       placeholderImage:[UIImage imageNamed:@""]
                                                options:SDWebImageRefreshCached];
        
    }
    _editride_profile_imageView.layer.cornerRadius=_editride_profile_imageView.frame.size.width/2;
    self.editride_profile_imageView.contentMode=UIViewContentModeScaleAspectFill;
    _editride_profile_imageView.clipsToBounds=YES;
    
    edit_start_longitude=[[_edit_Ride_Data valueForKey:@"ride_start_log"] doubleValue];
    edit_start_latitude=[[_edit_Ride_Data valueForKey:@"ride_start_lat"] doubleValue];
    edit_meeting_latitude=[[_edit_Ride_Data valueForKey:@"ride_meeting_lat"] doubleValue];
    edit_meeting_longitude=[[_edit_Ride_Data valueForKey:@"ride_meeting_log"] doubleValue];
    _ride_id = [NSString stringWithFormat:@"%@",[_edit_Ride_Data valueForKey:@"Ride_id"]];
    edit_ride_type=[NSString stringWithFormat:@"%@",[_edit_Ride_Data valueForKey:@"type"]];
    
    _arrayOfgroupsInEditRide=[[NSMutableArray alloc]init];
    _arrayOfFrndsInEditRide=[[NSMutableArray alloc]init];
    _arrayOfFrndsInEditRide=[_edit_Ride_Data valueForKey:@"selected_friends"];
    _arrayOfgroupsInEditRide=[_edit_Ride_Data valueForKey:@"selected_groups"];
       selected_route=appDelegate.select_Route_Id_For_CreateAndFreeRide;
     appDelegate.arrayOfSelectedFrndsAndRiders=[[NSMutableArray alloc]init];
     appDelegate.arrayOfSelectedGroupsAndRiders=[[NSMutableArray alloc]init];
    appDelegate.arrayOfSelectedFrndsAndRiders=_arrayOfFrndsInEditRide;
    appDelegate.arrayOfSelectedGroupsAndRiders=_arrayOfgroupsInEditRide;
    
}
-(void)viewWillAppear:(BOOL)animated{
    if (![appDelegate.select_Route_Id_For_CreateAndFreeRide isEqualToString:@""]) {
         _editRide_placeToStartTF.text=[NSString stringWithFormat:@"%@",appDelegate.select_Route_Address_For_CreateAndFreeRide];
        edit_start_latitude=[appDelegate.start_address_latitude doubleValue];
        edit_start_longitude=[appDelegate.start_address_longtitude doubleValue];
    }
    else
    {
        if ([selected_route isEqualToString:@""]);
        else _editRide_placeToStartTF.text=@"";
    }
    selected_route=appDelegate.select_Route_Id_For_CreateAndFreeRide;
}

-(void)setdate{
    
    
    date1.datePickerMode = UIDatePickerModeDate;
    [date1 setMinimumDate:[NSDate date]];
    [date1 setDate:[NSDate date]];
    [_editRide_startDateTF setInputView:date1];
    
    UIToolbar *toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolbar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *done = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(showDate)];
    done.tintColor=[UIColor blackColor];
    UIBarButtonItem *space = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolbar setItems:[NSArray arrayWithObjects:space,done,nil]];
    [_editRide_startDateTF setInputAccessoryView:toolbar];
    
}
-(void)setdate_meeting{
    
    meeting_date = [[UIDatePicker alloc]init];
    meeting_date.datePickerMode = UIDatePickerModeDate;
    [meeting_date setMinimumDate:[NSDate date]];
    [meeting_date setMaximumDate:date1.date];
    [meeting_date setDate:[NSDate date]];
    [_editRide_meetingDateTF setInputView:meeting_date];
    
    UIToolbar *toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolbar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *done = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(showDate_meeting)];
     done.tintColor=[UIColor blackColor];
    UIBarButtonItem *space = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolbar setItems:[NSArray arrayWithObjects:space,done,nil]];
    [_editRide_meetingDateTF setInputAccessoryView:toolbar];
    
}

-(void)settime{
    NSDateFormatter *dateformate = [[NSDateFormatter alloc ]init];
    [dateformate setDateFormat:@"MM/dd/YYYY"];
    date = [[UIDatePicker alloc]init];
    date.datePickerMode = UIDatePickerModeTime;
    date.date=[NSDate date];
//    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"NL"];
//    [date setLocale:locale];
    [date setDate:[NSDate date]];
    if ([[dateformate stringFromDate:date1.date] compare:[dateformate stringFromDate:[NSDate date]]]==NSOrderedSame ) {
        [date setMinimumDate:[NSDate date]];
    }
    [_editRide_startTimeTF setInputView:date];
    
    
    UIToolbar *toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolbar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *done = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(showTime)];
     done.tintColor=[UIColor blackColor];
    UIBarButtonItem *space = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [toolbar setItems:[NSArray arrayWithObjects:space,done,nil]];
    [_editRide_startTimeTF setInputAccessoryView:toolbar];
    
}
-(void)settime_start{
    
    start_time = [[UIDatePicker alloc]init];
    start_time.datePickerMode = UIDatePickerModeTime;
//    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"NL"];
//    [start_time setLocale:locale];
    [start_time setDate:[NSDate date]];
    if ([_editRide_startDateTF.text compare:_editRide_meetingDateTF.text]==NSOrderedSame) {
        [start_time setMinimumDate:date.date];
    }
    [_editRide_meetingTimeTF setInputView:start_time];
    
    
    UIToolbar *toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolbar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *done = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(showTime_start)];
     done.tintColor=[UIColor blackColor];
    UIBarButtonItem *space = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [toolbar setItems:[NSArray arrayWithObjects:space,done,nil]];
    [_editRide_meetingTimeTF setInputAccessoryView:toolbar];
}
-(void)showTime{
    
    NSDateFormatter *dateformater = [[NSDateFormatter alloc ]init];
//    [dateformater setDateFormat:@"HH:mm"];
    [dateformater setDateFormat:@"hh:mm a"];
    _editRide_startTimeTF.text = [NSString stringWithFormat:@"%@",[dateformater stringFromDate:date.date]];
    NSDateFormatter *edit_startTimeformater = [[NSDateFormatter alloc ]init];
    [edit_startTimeformater setDateFormat:@"HH:mm"];
    edit_startTimeString=[NSString stringWithFormat:@"%@",[edit_startTimeformater stringFromDate:date.date]];
    [_editRide_startTimeTF resignFirstResponder];
    
}
-(void)showTime_start{
    {
         [_editRide_meetingTimeTF resignFirstResponder];
        NSDateFormatter *dateformater = [[NSDateFormatter alloc ]init];
//        [dateformater setDateFormat:@"HH:mm"];
         [dateformater setDateFormat:@"hh:mm a"];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        NSDate *todayDate = [dateFormatter dateFromString:_editRide_meetingDateTF.text];
        NSDate *todayDate2 = [dateFormatter dateFromString:_editRide_startDateTF.text];
        NSString *convertedDateString1 = [dateFormatter stringFromDate:todayDate];
        NSString *convertedDateString2 = [dateFormatter stringFromDate:todayDate2];
        if ([convertedDateString1 isEqualToString:convertedDateString2])
        {
            NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitMinute fromDate:[dateformater dateFromString:_editRide_startTimeTF.text] toDate:start_time.date options:0];
            NSInteger numberOfMinutes = [components minute];
            
            if (numberOfMinutes<=-60)
            {
                _editRide_meetingTimeTF.text = [NSString stringWithFormat:@"%@",[dateformater stringFromDate:start_time.date]];
                NSDateFormatter *editMeetTimeformater = [[NSDateFormatter alloc ]init];
                [editMeetTimeformater setDateFormat:@"HH:mm"];
                edit_meetingTimeString=[NSString stringWithFormat:@"%@",[editMeetTimeformater stringFromDate:start_time.date]];
            }
            else
            {
                [SHARED_HELPER showAlert:@"Meeting time should be minimum one hour before ride time."];
            }
        }
        else
        {
            
            _editRide_meetingTimeTF.text = [NSString stringWithFormat:@"%@",[dateformater stringFromDate:start_time.date]];
            NSDateFormatter *editMeetTimeformater = [[NSDateFormatter alloc ]init];
            [editMeetTimeformater setDateFormat:@"HH:mm"];
            edit_meetingTimeString=[NSString stringWithFormat:@"%@",[editMeetTimeformater stringFromDate:start_time.date]];
            
        }
       
        
    }
    
}
-(void)showDate{
    
    NSDateFormatter *dateformater = [[NSDateFormatter alloc ]init];
    [dateformater setDateFormat:@"MM/dd/YYYY"];
    _editRide_startDateTF.text = [NSString stringWithFormat:@"%@",[dateformater stringFromDate:date1.date]];
    [_editRide_startDateTF resignFirstResponder];
    
}
-(void)showDate_meeting{
    
    NSDateFormatter *dateformater = [[NSDateFormatter alloc ]init];
    [dateformater setDateFormat:@"MM/dd/YYYY"];
    _editRide_meetingDateTF.text = [NSString stringWithFormat:@"%@",[dateformater stringFromDate:meeting_date.date]];
    [_editRide_meetingDateTF resignFirstResponder];
    
}
#pragma mark - google placepiker
- (void)placePicker:(GMSPlacePickerViewController *)viewController didPickPlace:(GMSPlace *)place {
    // Dismiss the place picker, as it cannot dismiss itself.
    [viewController dismissViewControllerAnimated:YES completion:nil];
    
    
    if ([_edit_location_str_ride isEqualToString:@"start_location_ride"]) {
        
        if(place.coordinate.latitude==0.0 && place.coordinate.longitude==0.0)
        {
            _editRide_placeToStartTF.text =@"";
        }
        else
        {
            _editRide_placeToStartTF.text = place.name;
            edit_start_latitude=place.coordinate.latitude;
            edit_start_longitude=place.coordinate.longitude;
        }
            
      
        
        NSLog(@"Place name %@", place.name);
        NSLog(@"Place address %@", place.formattedAddress);
        NSLog(@"Place attributions %@", place.attributions.string);
    }
    else if ([_edit_location_str_ride isEqualToString:@"meeting_location_ride"]){
        if(place.coordinate.latitude==0.0 && place.coordinate.longitude==0.0)
        {
            _editRide_meetingAddress.text =@"";
        }
        else
        {
            _editRide_meetingAddress.text = place.name;
            edit_meeting_latitude=place.coordinate.latitude;
            edit_meeting_longitude=place.coordinate.longitude;
        }
        
      
        NSLog(@"Place meeting name %@", place.name);
        NSLog(@"Place meeting address %@", place.formattedAddress);
        NSLog(@"Place attributions %@", place.attributions.string);
    }
    
    //    latitude=place.coordinate.latitude;
    //    longitude=place.coordinate.longitude;
    //    NSLog(@"Place name %@", place.name);
    //    _Placetostart_tf.text = place.name;
    //    NSLog(@"Place address %@", place.formattedAddress);
    //    NSLog(@"Place attributions %@", place.attributions.string);
    
}

- (void)placePickerDidCancel:(GMSPlacePickerViewController *)viewController {
    // Dismiss the place picker, as it cannot dismiss itself.
    [viewController dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"No place selected");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//tF deligate methods....
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField==_editRide_meetingTimeTF ||textField==_editRide_descriptionTF) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:.3];
        [UIView setAnimationBeginsFromCurrentState:TRUE];
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -150., self.view.frame.size.width, self.view.frame.size.height);
        
        [UIView commitAnimations];
    }
    if (textField ==_editRide_startDateTF) {
        _editRide_startTimeTF.text=@"";
        _editRide_meetingDateTF.text=@"";
        _editRide_meetingTimeTF.text=@"";
        [self setdate];
    }
    if (textField == _editRide_startTimeTF) {
        
        if ([_editRide_startDateTF.text length]>0)
        {
            _editRide_meetingDateTF.text=@"";
            _editRide_meetingTimeTF.text=@"";
            [self settime];
        }
        else
        {
            [textField resignFirstResponder];
            [SHARED_HELPER showAlert:RideDate];
        }
    }
    if (textField == _editRide_meetingDateTF) {
        if ([_editRide_startTimeTF.text length]>0)
        {
            _editRide_meetingTimeTF.text=@"";
            [self setdate_meeting];
        }
        else{
            [textField resignFirstResponder];
            [SHARED_HELPER showAlert:ride_Time];
        }
        
    }
    if (textField == _editRide_meetingTimeTF) {
        
        if ([_editRide_meetingDateTF.text length]>0) {
            [self settime_start];
        }
        else{
            [textField resignFirstResponder];
            [SHARED_HELPER showAlert:MeetingDate];
        }
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField==_editRide_meetingTimeTF ||textField==_editRide_descriptionTF)
    {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:.3];
        [UIView setAnimationBeginsFromCurrentState:TRUE];
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +150., self.view.frame.size.width, self.view.frame.size.height);
        
        [UIView commitAnimations];
    }
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    if (textField == _editRide_titleTF)
    {
        [_editRide_placeToStartTF becomeFirstResponder];
        return NO;
    }
    else if (textField == _editRide_descriptionTF)
    {
        [textField resignFirstResponder];
        return NO;
    }
     [textField resignFirstResponder];
    return YES;
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(void)cancle_clicked:(UIButton *)sender
{
    //    appDelegate.arrayOfSelectedFrndsAndRiders=[[NSMutableArray alloc]init];
    //    appDelegate.arrayOfSelectedGroupsAndRiders=[[NSMutableArray alloc]init];
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)save_clicked
{
    
    [self.view endEditing:YES];
    _editRide_titleTF.text=[_editRide_titleTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

        if (![SHARED_HELPER checkIfisInternetAvailable])
        {
            [SHARED_HELPER showAlert:Nonetwork];
            return;
        }
    
        if (_editRide_titleTF.text.length == 0) {
            [SHARED_HELPER showAlert:ridenameempty];
            return;
        }
        if (_editRide_placeToStartTF.text.length == 0) {
            [SHARED_HELPER showAlert:startlocation];
            
            return;
        }
        if (_editRide_startDateTF.text.length == 0) {
              [SHARED_HELPER showAlert:ride_Date];
            
            return;
        }
        if (_editRide_startTimeTF.text.length == 0) {
            [SHARED_HELPER showAlert:ride_Time];
            return;
        }
     
    [self edit_ride_request];
    
}

- (IBAction)onClick_editride_profile_btn:(id)sender {
    [self.view endEditing:YES];
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped do nothing.
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = NO;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = NO;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
}


-(void)edit_ride_request{
    
    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    activeIndicatore.hidesWhenStopped = TRUE;
    [activeIndicatore startAnimating];
    [self.view addSubview:activeIndicatore];
    //    "{
    //    ""user_id"":"""",
    //    ""ride_id"":"""",
    //    ""route_id"":"""",
    //    ""ride_title"":"""",
    //    ""ride_description"":"""",
    //    ""ride_cover_image"":"""",
    //    ""ride_previous_cover_image"":"""",
    //    ""random_number"":"""",
    //    ""ride_type"":"""",
    //    ""ride_start_point_address"":"""",
    //    ""ride_start_point_latitude"":"""",
    //    ""ride_start_point_longitude"":"""",
    //    ""ride_start_time"":"""",
    //    ""ride_start_date"":"""",
    //    ""meeting_point_address"":"""",
    //    ""meeting_point_latitude"":"""",
    //    ""meeting_point_longitude"":"""",
    //    ""meeting_time"":"""",
    //    ""meeting_date"":"""",
    //    ""group_invite_ids"":[],
    //    ""friend_invite_ids"":[],
    //    ""remove_group_invite_ids"":[],
    //    ""remove_friend_invite_ids"":[]
    //}"
    
    
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:[_editRide_titleTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"ride_title"];
    [dict setObject:[_editRide_descriptionTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"ride_description"];
    [dict setObject:[_editRide_placeToStartTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"ride_start_point_address"];
    [dict setObject:[NSNumber numberWithDouble:edit_start_latitude] forKey:@"ride_start_point_latitude"];
    [dict setObject:[NSNumber numberWithDouble:edit_start_longitude] forKey:@"ride_start_point_longitude"];
    [dict setObject:[_editRide_startDateTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"ride_start_date"];
//    [dict setObject:[_editRide_startTimeTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"ride_start_time"];
     [dict setObject:[edit_startTimeString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"ride_start_time"];
    [dict setObject:[_editRide_meetingAddress.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"meeting_point_address"];
    
//    [dict setObject:[_editRide_meetingTimeTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"meeting_time"];
    [dict setObject:[edit_meetingTimeString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"meeting_time"];
    
    [dict setObject:[_editRide_meetingDateTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"meeting_date"];
    [dict setObject:[NSNumber numberWithDouble:edit_meeting_latitude] forKey:@"meeting_point_latitude"];
    [dict setObject:[NSNumber numberWithDouble:edit_meeting_longitude]  forKey:@"meeting_point_longitude"];
    [dict setObject:random_number_string forKey:@"random_number"];
    [dict setObject:[NSString stringWithFormat:@"%@",[[AppHelperClass appsharedInstance] rideType]] forKey:@"ride_type"];
    [dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
    [dict setObject:appDelegate.select_Route_Id_For_CreateAndFreeRide forKey:@"route_id"];
    [dict setObject:edit_ride_type forKey:@"request_type"];
    [dict setObject:_ride_id forKey:@"ride_id"];
    [dict setObject:arrayOfRemoveFrndsIDs forKey:@"remove_friend_invite_ids"];
    
    if ([appDelegate.arrayOfSelectedFrndsAndRiders count]>0)
    {
        
        [dict setObject:appDelegate.arrayOfSelectedFrndsAndRiders forKey:@"friend_invite_ids"];
       
        
    }
    else
    {
       
        [dict setObject:appDelegate.arrayOfSelectedFrndsAndRiders forKey:@"friend_invite_ids"];
    }
    if ([appDelegate.arrayOfSelectedGroupsAndRiders count]>0) {
        [dict setObject:appDelegate.arrayOfSelectedGroupsAndRiders forKey:@"group_invite_ids"];
        
    }
    else{
        appDelegate.arrayOfSelectedGroupsAndRiders=[[NSMutableArray alloc]init];
        [dict setObject:appDelegate.arrayOfSelectedGroupsAndRiders forKey:@"group_invite_ids"];
    }
    if ([appDelegate.arrayOFRemoveFrndsID count]>0) {
        [dict setObject:appDelegate.arrayOFRemoveFrndsID forKey:@"remove_friend_invite_ids"];
    }
    else{
        appDelegate.arrayOFRemoveFrndsID=[[NSMutableArray alloc]init];
        [dict setObject:appDelegate.arrayOFRemoveFrndsID forKey:@"remove_friend_invite_ids"];
    }

    
    if (base64String) {
        [dict setObject:base64String forKey:@"ride_cover_image"];
    }
    else{
        [dict setObject:@"" forKey:@"ride_cover_image"];
    }
    [dict setObject:imageurlstrng forKey:@"ride_previous_cover_image"];
    NSLog(@"%@",dict);
    [SHARED_API editRideRequestsWithParams:dict withSuccess:^(NSDictionary *response) {
        NSLog(@"responce is %@",response);
        if ([[response valueForKey:STATUS] isEqualToString:SUCCESS]) {
                        [SHARED_HELPER showAlert:ridecreated];
            [self edit_Ride_Sucsess];
            [activeIndicatore stopAnimating];
        }
    } onfailure:^(NSError *theError) {
        [activeIndicatore stopAnimating];
        [SHARED_HELPER showAlert:ServerConnection];
    }];
}

-(void)edit_Ride_Sucsess{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [activeIndicatore stopAnimating];
        appDelegate.Ride_Detail_Edited=@"YES";
        [self.navigationController popViewControllerAnimated:YES];
    });
}



#pragma mark - Image placepiker Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info

{
    cameraImage = info[UIImagePickerControllerOriginalImage];
    _editride_profile_imageView.image=cameraImage;
    [picker dismissViewControllerAnimated:YES completion:NULL];
    CGFloat compression = 0.9f;
    CGFloat maxCompression = 0.1f;
    int maxFileSize = 250*1024;
    profileImgData = UIImageJPEGRepresentation(cameraImage,compression);
    while ([profileImgData length] > maxFileSize && compression > maxCompression)
    {
        compression -= 0.1;
        profileImgData = UIImageJPEGRepresentation(cameraImage,compression);
    }
    base64String = [profileImgData base64EncodedStringWithOptions:0];
}

- (IBAction)onClick_editRide_selectRoute_btn:(id)sender {
    RoutesVC *cntlr = [self.storyboard instantiateViewControllerWithIdentifier:@"RoutesVC"];
    cntlr.is_FROM_VC_OR_Start_Ride=@"EDIT RIDE";
    cntlr.checkboxSelecteString=[NSString stringWithFormat:@"%@",route_id_str];
    //    cntlr.is_FROM_VC_OR_Start_Ride=@"YES";
    [self.navigationController pushViewController:cntlr animated:YES];
}
- (IBAction)onClick_editRide_placeTostart_btn:(id)sender {
    if (![appDelegate.select_Route_Id_For_CreateAndFreeRide isEqualToString:@""])
    {
        return;
    }
    _edit_location_str_ride=@"start_location_ride";
    GMSPlacePickerConfig *config = [[GMSPlacePickerConfig alloc] initWithViewport:nil];
    GMSPlacePickerViewController *placePicker =
    [[GMSPlacePickerViewController alloc] initWithConfig:config];
    placePicker.delegate = self;
    [self presentViewController:placePicker animated:YES completion:nil];
}
- (IBAction)onClick_editRide_inviteRiders_btn:(id)sender {
    
    SelectFriendsAndGroups*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"SelectFriendsAndGroups"];
    controller.free_route_Frnds_groups_id_string=@"EDIT RIDE FRIENDS AND GROUP";
    appDelegate.isFrom_RidersVC_Or_FreeRideRoutes=@"InviteRiders";
   
    [self.navigationController pushViewController:controller animated:YES];
}
- (IBAction)onClick_editRide_meetingAddress_btn:(id)sender
{
    _edit_location_str_ride=@"meeting_location_ride";
    GMSPlacePickerConfig *config1 = [[GMSPlacePickerConfig alloc] initWithViewport:nil];
    GMSPlacePickerViewController *placePicker1 =
    [[GMSPlacePickerViewController alloc] initWithConfig:config1];
    placePicker1.delegate = self;
    [self presentViewController:placePicker1 animated:YES completion:nil];
}
- (IBAction)onClick_editride_rideGoingBtn:(id)sender {
    
    self.editRide_scrollLabel.hidden=YES;
    self.editRide_rideGoingBtn.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }

}
@end
