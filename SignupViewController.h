//
//  SignupViewController.h
//  openroadrides
//
//  Created by apple on 01/05/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "APIHelper.h"
#import "UserDetails.h"
@interface SignupViewController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource, UIImagePickerControllerDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate,UINavigationControllerDelegate>
{
    NSString *genderString;
    UIImage *cameraImage;
    NSData *profileImgData;
    NSString *base64String,*reg_str;
    UITextField *currentTextField;
    UIDatePicker *date;
    NSMutableArray *dataArray, *stateArray;
    NSDictionary *mapping ;
    UIView *pickerBaseView, *contentView, *popUpBaseView;
 UIButton *doneButton,*cancel_Button;
    UIPickerView *businessTypePickerView;
    BOOL Spickerchng;
    BOOL Cpickerchng;
     NSString *pickerclick;
     NSInteger selectedindexv;
    UIActivityIndicatorView *activeIndicatore;

}
@property (weak, nonatomic) IBOutlet UIScrollView *sign_up_scrl_vw;
@property (weak, nonatomic) IBOutlet UIImageView *Profileimageview;
- (IBAction)Signin_click:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *Mailimageview;
@property (weak, nonatomic) IBOutlet UIImageView *Femailimageview;
- (IBAction)logo_click:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *Username_tf;
@property (weak, nonatomic) IBOutlet UITextField *Email_tf;
@property (weak, nonatomic) IBOutlet UITextField *Dob_tf;
@property (weak, nonatomic) IBOutlet UITextField *Phoneno_tf;
@property (weak, nonatomic) IBOutlet UITextField *Zipcode_tf;
@property (weak, nonatomic) IBOutlet UITextField *State_tf;
@property (weak, nonatomic) IBOutlet UITextField *City_tf;
@property (weak, nonatomic) IBOutlet UITextField *Address_tf;
- (IBAction)Gender_action:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *Male_outlet;

@property (weak, nonatomic) IBOutlet UIView *Gender_view;
@property (weak, nonatomic) IBOutlet UIButton *Female_outlet;
- (IBAction)SingUp_action:(id)sender;
@property (nonatomic,strong)UserDetails *currentUserDetails;
@property (strong, nonatomic) NSString *userid;
@property (weak, nonatomic) IBOutlet UIView *gender_middleLine_view;


-(void)normalupdatecall;

@end
