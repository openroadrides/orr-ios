//
//  SelectFriendsCell.h
//  openroadrides
//
//  Created by apple on 06/07/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectFriendsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *selectFrndCell_bgView;
@property (weak, nonatomic) IBOutlet UIImageView *selectFrnd_profileImage;
@property (weak, nonatomic) IBOutlet UIButton *selectFrnd_checkbox_btn;
@property (weak, nonatomic) IBOutlet UILabel *selectFrnd_userName_label;
@property (weak, nonatomic) IBOutlet UILabel *selectFrnd_address_label;
@property (weak, nonatomic) IBOutlet UIImageView *selectFrnds_locationPlaceholder_imageView;
@property (weak, nonatomic) IBOutlet UIView *selectFrnd_status_view;
@property (weak, nonatomic) IBOutlet UILabel *selectFrnd_statusLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewtop_userName_constraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewTop_address_constraint;

@end
