//
//  ProfileView.h
//  openroadrides
//
//  Created by apple on 10/07/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "AppHelperClass.h"
#import "APIHelper.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "EditProfileViewController.h"
#import "AppDelegate.h"
#import "CBAutoScrollLabel.h"
@interface ProfileView : UIViewController
{
   
    UIActivityIndicatorView *activeIndicatore;
    NSMutableArray *check_strings_array;
    NSString *profile_image_url,*gender_Str,*user_Mail_str,*user_adress_Str,*user_number_str,*city_str,*state_str,*zipcode_Str,*dob_Str,*service_sucsess;
}
@property (strong, nonatomic) IBOutlet UILabel *check_Ins_Count;

@property (strong, nonatomic)NSString *is_From,*id_user,*friend_status;
@property (strong, nonatomic) IBOutlet UIImageView *User_Profile_Imag;
@property (strong, nonatomic) IBOutlet UILabel *User_name_label;
@property (strong, nonatomic) IBOutlet UILabel *total_Rides_Count;
@property (strong, nonatomic) IBOutlet UILabel *friends_Count;
@property (strong, nonatomic) IBOutlet UILabel *total_Miles_Count;
@property (strong, nonatomic) IBOutlet UILabel *avg_Spead;
@property (strong, nonatomic) IBOutlet UILabel *groups_Count;
@property (strong, nonatomic) IBOutlet UILabel *adress_label;
@property (strong, nonatomic) IBOutlet UILabel *mobiles_Numner_Label;
@property (strong, nonatomic) IBOutlet UILabel *gmail_Label;
@property (weak, nonatomic) IBOutlet UIButton *profile_rideGoingBtn;
- (IBAction)onClick_rideGoingBtn:(id)sender;

@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *viewProfile_scrollLabel;

@property (weak, nonatomic) IBOutlet UILabel *totalMilesLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalRidesLabel;



@end
