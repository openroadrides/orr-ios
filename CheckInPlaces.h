//
//  CheckInPlaces.h
//  openroadrides
//
//  Created by SrkIosEbiz on 05/06/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GooglePlaces/GooglePlaces.h>
#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CoreLocation.h>
#import "AppHelperClass.h"
#import "APIHelper.h"
#import "AppDelegate.h"
#import "check_in_riders_list.h"
#import <SDWebImage/UIImageView+WebCache.h>


@interface CheckInPlaces : UIViewController<CLLocationManagerDelegate,UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,
UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate>
{
     GMSPlacesClient *_placesClient;
    CLLocationManager *locationManager;
    double Current_Lat,Current_Long;
    CLLocationDistance distance;
    CLLocation *current_location,*Place_Location;
    NSMutableArray *near_Places_Array,*selected_riders_arr;
    NSMutableArray *riders_list_array;
    UIActivityIndicatorView *indicator;
    UILabel *NO_DATA;
    
    
    //Check_in
    //Almost_Done_View
    UIButton *add_btn;
    UIButton *subtract_btn;
   
    UIBarButtonItem * Checkin_Service;
    NSString *checkin_random_number_string;
    
    
    
    int scrollWidth ;
    UIImageView *img;
    int x_axis;
    UIView *baseView_placesImages;
    NSMutableArray *img_array;
    int index,imgIndex,x;
    
    
}
@property (strong, nonatomic)  NSString *ride_id_is;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (strong, nonatomic) IBOutlet UITableView *places_Table;
@property (strong, nonatomic) IBOutlet UIView *select_Place_view;
@property (strong, nonatomic) IBOutlet UIView *Check_In_View;
- (IBAction)Back_Action:(id)sender;
//Check_in
@property (strong, nonatomic)  NSDictionary *checkin_place_Dic;
@property (strong, nonatomic) IBOutlet UITableView *riders_Display_table;

@property (strong, nonatomic) IBOutlet UIView *images_View;
@property (strong, nonatomic) IBOutlet UITextField *title_TF;
@property (strong, nonatomic) IBOutlet UILabel *place_adress_TF;
@property (strong, nonatomic) IBOutlet UITextField *Des_TF;
@property (strong, nonatomic) IBOutlet UIScrollView *images_Scrole_View;
- (IBAction)Check_In_Cancel:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *Check_In_Done_Action;
- (IBAction)Check_In_Sucsess_Action:(id)sender;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *alert_View_bottom;
@property (strong, nonatomic) IBOutlet UILabel *alert_Label;
- (IBAction)key_Board_Go:(id)sender;

- (IBAction)friend_Check_Box_Action:(id)sender;



@end
