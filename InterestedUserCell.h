//
//  InterestedUserCell.h
//  openroadrides
//
//  Created by apple on 04/10/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InterestedUserCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *bgViewForUserCell;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *user_Name_label;
@property (weak, nonatomic) IBOutlet UIImageView *user_email_imageView;
@property (weak, nonatomic) IBOutlet UILabel *user_email_label;
@property (weak, nonatomic) IBOutlet UIImageView *user_phonenumber_imageView;
@property (weak, nonatomic) IBOutlet UILabel *user_phoneNumber_label;
@property (weak, nonatomic) IBOutlet UILabel *user_posted_label;

@end
