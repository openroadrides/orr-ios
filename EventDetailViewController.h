//
//  EventDetailViewController.h
//  openroadrides
//
//  Created by apple on 07/06/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "EventWebView.h"
#import "EditEventViewController.h"
#import "CBAutoScrollLabel.h"
@interface EventDetailViewController : UIViewController<UIScrollViewDelegate>
{
    UIActivityIndicatorView *activeIndicatore;
    NSDictionary *event_details_dict;
    UIBarButtonItem *item0;
    NSString *image_str;
    NSArray *imagearayy;
    UIPageControl *pageControl;
    NSString*eventLinkString;
    NSString*link_str_is;
    NSString *event_type_is,*event_id,*event_lat,*event_long;
    BOOL is24h;
    
}
@property (weak, nonatomic) IBOutlet UILabel *main_title_labl;
@property (weak, nonatomic) IBOutlet UILabel *start_location_lbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *description_top;
@property (weak, nonatomic) IBOutlet UILabel *end_location_lbl;
@property (weak, nonatomic) IBOutlet UILabel *start_date_lbl;
@property (weak, nonatomic) IBOutlet UILabel *end_date_lbl;
@property (weak, nonatomic) IBOutlet UILabel *description_lbl;
@property (weak, nonatomic) IBOutlet UILabel *start_time_lbl;
@property (weak, nonatomic) IBOutlet UILabel *end_time_lbl;
@property (weak, nonatomic) IBOutlet UIView *main_view;
@property (weak, nonatomic) IBOutlet UIImageView *event_background_img_view;
@property (weak, nonatomic) IBOutlet UIScrollView *event_scrl_view;
@property (weak, nonatomic) IBOutlet UIImageView *event_detail_bg_img_view;
@property (weak, nonatomic) IBOutlet UIView *content_view;
@property (nonatomic, strong) IBOutlet NSString *Event_id;
@property (weak, nonatomic) IBOutlet UIScrollView *multipeImagesScrollView;
@property (weak, nonatomic) IBOutlet UIView *associattedBorderLineView;
@property (weak, nonatomic) IBOutlet UILabel *associatedLabel;
@property (weak, nonatomic) IBOutlet UILabel *associatedDescriptionTextLabel;
@property (weak, nonatomic) IBOutlet UIView *descriptionBorderLineView;
@property (weak, nonatomic) IBOutlet UILabel *desc_title_label;
@property (weak, nonatomic) IBOutlet UIButton *clcikHereBtn;
- (IBAction)onClick_clickHere_btn:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *startAndEndDateBorderLineView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *start_date_topConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *start_date_noLoc_topConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descBorderline_topConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *desctitle_topConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *desctitle_noText_topConstraint;
@property (weak, nonatomic) IBOutlet UILabel *locationTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *locationStartImage;
@property (weak, nonatomic) IBOutlet UIButton *eventDetail_rideGoingBtn;
- (IBAction)onClick_eventDetail_rideGoingBtn:(id)sender;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *eventDetail_scrollLabel;

@end
