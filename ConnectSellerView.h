//
//  ConnectSellerView.h
//  openroadrides
//
//  Created by apple on 03/10/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APIHelper.h"
#import "AppHelperClass.h"
#import "Constants.h"
#import "AppDelegate.h"
@interface ConnectSellerView : UIViewController<UITextFieldDelegate>
{
    UIActivityIndicatorView  *activeIndicatore;
}
@property (weak, nonatomic) IBOutlet UITextField *mobileNumberTF;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;
- (IBAction)onClick_sendBtn_action:(id)sender;
@property (strong, nonatomic) NSString *intrestedPostIdString;
@property (weak, nonatomic) IBOutlet UIButton *intrested_closeBtn;
- (IBAction)onClick_intrestd_closeBtn:(id)sender;

@end
