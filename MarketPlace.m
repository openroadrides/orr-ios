//
//  MarketPlace.m
//  openroadrides
//
//  Created by apple on 26/09/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "MarketPlace.h"
#import "MPCoachMarks.h"
@interface MarketPlace ()
{
    double marketPlace_currentLatitude,marketPlace_currentLongitude;
}
@end

@implementation MarketPlace

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appDelegate.edit_posted=@"NO";
    appDelegate.my_post_Changed=@"NO";
    
    
    // Do any additional setup after loading the view.
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    self.title = @"MARKETPLACE";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [leftBtn addTarget:self action:@selector(back_Action) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.frame = CGRectMake(0, 0, 25, 25);
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"back_Image"] forState:UIControlStateNormal];
    UIBarButtonItem *leftBackButton=[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:leftBackButton, nil];
    
    
    UIButton *add_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [add_btn addTarget:self action:@selector(addclicked) forControlEvents:UIControlEventTouchUpInside];
    add_btn.frame = CGRectMake(0, 0, 30, 30);
    [add_btn setBackgroundImage:[UIImage imageNamed:@"Add_plus"] forState:UIControlStateNormal];
    UIBarButtonItem * create_Ride= [[UIBarButtonItem alloc] initWithCustomView:add_btn];
    self.navigationItem.rightBarButtonItems=[NSArray arrayWithObjects:create_Ride, nil];
    
    self.bottomContraint_marketPlaceTglBtnImage.constant=-25;
    self.marketPlace_scrollLabel.hidden=YES;
    self.marketPlace_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.marketPlace_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.marketPlace_scrollLabel.textColor = [UIColor blackColor];
    self.marketPlace_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.marketPlace_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.marketPlace_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.marketPlace_scrollLabel.scrollSpeed = 60; // pixels per second
    self.marketPlace_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.marketPlace_scrollLabel.fadeLength = 0.f;
    self.marketPlace_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.marketPlace_scrollLabel observeApplicationNotifications];
    
    self.marketPlace_rideGoingBtn.hidden=YES;
    
    if (appDelegate.ridedashboard_home) {
        
        self.bottomContraint_marketPlaceTglBtnImage.constant=0;
        self.marketPlace_scrollLabel.hidden=NO;
        self.marketPlace_rideGoingBtn.hidden=NO;
    }

    
    
    
    marketPlace_locationManager = [[CLLocationManager alloc] init];
    marketPlace_locationManager.delegate=self;
        marketPlace_currentLatitude = marketPlace_locationManager.location.coordinate.latitude;
    marketPlace_currentLongitude = marketPlace_locationManager.location.coordinate.longitude;
    
    _marketPlace_collectionView.dataSource=self;
    _marketPlace_collectionView.delegate=self;
    _arrayOfMarketPlaceList=[[NSMutableArray alloc]init];
    _arrayOfMyPostsList=[[NSMutableArray alloc]init];
    categoryIDString=@"0";
    myPostCategoryIDString=@"0";
    _categoryTypeLabel.text=@"All";
    _myPost_filterLabel.text=@"All";
    [self marketPlaceListMenthod];
    [self myPostsListMethod];
    
    
    toggleString=@"MYPOSTS";
    _toogle_market_post_imageView.image=[UIImage imageNamed:@"myposts_toggle"];
    _myPostsContentView.hidden=YES;
    AddCatString=@"YES";
//    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//    activeIndicatore.center = self.view.center;
//    activeIndicatore.color = APP_YELLOW_COLOR;
//    activeIndicatore.hidesWhenStopped = TRUE;
//    [self.view addSubview:activeIndicatore];
    
    
    
    indicaterview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    activeIndicatore.backgroundColor=[UIColor clearColor];
    activeIndicatore = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.frame = CGRectMake(0.0, 0.0, 80, 80);
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    [indicaterview addSubview:activeIndicatore];
    [self.view addSubview:indicaterview];
    [indicaterview bringSubviewToFront:self.view];
    [activeIndicatore startAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;

    
    
    
    self.noMarketPlacesData_Label.hidden=YES;
    self.noPostsDataLabel.hidden=YES;
    self.marketPlace_filterView.hidden=YES;
    self.myPost_filterView.hidden=YES;
    
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(CreatePostReload) name:@"CreatePostReload" object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ClosedPostReload) name:@"ClosedPostReload" object:nil];
    

    
}
#pragma mark - Annotations
-(void)showAnnotation
{
    [self.view layoutIfNeeded];
    
   
    NSArray *coachMarks;
    
    if ([isPostAvailable_annotation isEqualToString:@"YES"]) {
        
        CGRect coachmark1 = CGRectMake ( ([UIScreen mainScreen].bounds.size.width - 53), 20, self.navigationController.navigationBar.frame.size.height, self.navigationController.navigationBar.frame.size.height);
        CGRect coachmark2 = CGRectMake( _marketPlace_filter_imageView.frame.origin.x, _marketPlace_filter_imageView.frame.origin.y+60, _marketPlace_filter_imageView.frame.size.width, _marketPlace_filter_imageView.frame.size.height+5);
        CGRect coachmark3 = CGRectMake( _toogle_market_post_imageView.frame.origin.x, _toogle_market_post_imageView.frame.origin.y+65, _toogle_market_post_imageView.frame.size.width, _toogle_market_post_imageView.frame.size.height);
        coachMarks = @[
                       @{
                           @"rect": [NSValue valueWithCGRect:coachmark1],
                           @"caption": @"Tap here to create post",
                           @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                           @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                           @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_RIGHT]
                           //@"showArrow":[NSNumber numberWithBool:YES]
                           },
                       @{
                           @"rect": [NSValue valueWithCGRect:coachmark2],
                           @"caption": @"Filter by type of the post",
                           @"shape": [NSNumber numberWithInteger:SHAPE_SQUARE],
                           @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                           @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_LEFT]
                           //@"showArrow":[NSNumber numberWithBool:YES]
                           },
                       @{
                           @"rect": [NSValue valueWithCGRect:coachmark3],
                           @"caption": @"Tap here to go to your posts",
                           @"shape": [NSNumber numberWithInteger:SHAPE_SQUARE],
                           @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
                           @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_RIGHT],
                           //@"showArrow":[NSNumber numberWithBool:YES]
                           },
                       ];
    }
    else{
        
        CGRect coachmark1 = CGRectMake ( ([UIScreen mainScreen].bounds.size.width - 53), 20, self.navigationController.navigationBar.frame.size.height, self.navigationController.navigationBar.frame.size.height);
        CGRect coachmark3 = CGRectMake( _toogle_market_post_imageView.frame.origin.x, _toogle_market_post_imageView.frame.origin.y+65, _toogle_market_post_imageView.frame.size.width, _toogle_market_post_imageView.frame.size.height);
        coachMarks = @[
                                @{
                                    @"rect": [NSValue valueWithCGRect:coachmark1],
                                    @"caption": @"Tap here to create post",
                                    @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                                    @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                                    @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_RIGHT]
                                    //@"showArrow":[NSNumber numberWithBool:YES]
                                    },
                             
                                @{
                                    @"rect": [NSValue valueWithCGRect:coachmark3],
                                    @"caption": @"Tap here to go to your post",
                                    @"shape": [NSNumber numberWithInteger:SHAPE_SQUARE],
                                    @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
                                    @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_RIGHT],
                                    //@"showArrow":[NSNumber numberWithBool:YES]
                                    },
                                ];
    }
    
            // Setup coach marks
    
        
    
        // Setup coach marks
    
    
    MPCoachMarks *coachMarksView = [[MPCoachMarks alloc] initWithFrame:self.navigationController.view.bounds coachMarks:coachMarks];
    [self.navigationController.view addSubview:coachMarksView];
    //[[UIApplication sharedApplication].keyWindow addSubview:coachMarksView];
    [coachMarksView start];
    
}
-(void)viewWillAppear:(BOOL)animated{
    if ([appDelegate.my_post_Changed isEqualToString:@"YES"]) {
        [self myPostsListMethod];
        appDelegate.my_post_Changed=@"NO";
    }
}
-(void)CreatePostReload
{
    [self myPostsListMethod];
}
-(void)ClosedPostReload
{
    [self myPostsListMethod];
}
-(void)back_Action{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)addclicked{
    
//    CreatePostView*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"CreatePostView"];
    if ([AddCatString isEqualToString:@"YES"]) {
        
        CreatePostView*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"CreatePostView"];
        if ([_categoryTypeLabel.text isEqualToString:@"All"]) {
            
        }
        else
        {
            controller.DisplayCatString=_categoryTypeLabel.text;
            controller.displatCatID=categoryIDString;
        }
//        AddCatString=@"NO";
         [self.navigationController pushViewController:controller animated:YES];
       
    }
    else if([AddCatString isEqualToString:@"NO"])
    {
        
        CreatePostView*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"CreatePostView"];
        if ([_myPost_filterLabel.text isEqualToString:@"All"]) {
            
        }
        else
        {
            controller.DisplayCatString=_myPost_filterLabel.text;
            controller.displatCatID=myPostCategoryIDString;
        }
        [self.navigationController pushViewController:controller animated:YES];
//        AddCatString=@"YES";
    }
   
//    [self.navigationController pushViewController:controller animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillDisappear:(BOOL)animated{
    marketPlace_locationManager=nil;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - CollectionView

-(CGSize)collectionView:(UICollectionView *)collectionView
                 layout:(UICollectionViewLayout *)collectionViewLayout
 sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(collectionView.frame.size.width/2 , 174);
    
}
//

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    if (collectionView ==_marketPlace_collectionView) {
        return [_arrayOfMarketPlaceList count];
    }
    else if (collectionView == _myPost_collectionView)
    {
        return [_arrayOfMyPostsList count];
    }
    else
    {
        return 0;
    }
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (collectionView==_marketPlace_collectionView) {
        
   
    MarketPlaceCollectionViewCell *cell = [self.marketPlace_collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    NSString*titleNameString=[NSString stringWithFormat:@"%@",[[_arrayOfMarketPlaceList objectAtIndex:indexPath.row] objectForKey:@"post_title"]];
    if ([titleNameString isEqualToString:@"<null>"] || [titleNameString isEqualToString:@""] || [titleNameString isEqualToString:@"null"] || titleNameString == nil) {
        
        titleNameString=@"";
        cell.marketPlace_item_title_label.text=titleNameString;
    }
    else{
        cell.marketPlace_item_title_label.text=titleNameString;
        cell.marketPlace_item_title_label.numberOfLines=1;
    }
//    NSString*postedString=[NSString stringWithFormat:@"%@",[[_arrayOfMarketPlaceList objectAtIndex:indexPath.row] objectForKey:@"created_date_time"]];
//    if ([postedString isEqualToString:@"<null>"] || [postedString isEqualToString:@""] || [postedString isEqualToString:@"null"] || postedString == nil) {
//        
//        postedString=@"";
//        cell.marketPlace_posted_label.text=postedString;
//       
//        
//    }
//    else{
//        cell.marketPlace_posted_label.text=[NSString stringWithFormat:@"Posted %@",postedString];
//        cell.marketPlace_posted_label.numberOfLines=3;
//       
//    }
        
        NSString *convertedString;
        NSString *date_str = [NSString stringWithFormat:@"%@",[[_arrayOfMarketPlaceList objectAtIndex:indexPath.row] objectForKey:@"created_date_time"]]; /// here this is your date with format yyyy-MM-dd
        
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"]; //// here set format of date which is in your output date (means above str with format)
            [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
            NSDate *date = [dateFormatter dateFromString: date_str];
            [dateFormatter setDateFormat:@"MM/dd/yyyy"];
            [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        
           NSString *curent_date=[dateFormatter stringFromDate:[NSDate date]];
            convertedString = [dateFormatter stringFromDate:date]; //here convert date in
        
        if ([curent_date compare:convertedString]==NSOrderedSame) {
            
//            [dateFormatter setDateFormat:@"HH:mm"];
             [dateFormatter setDateFormat:@"hh:mm a"];
            [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
            convertedString = [dateFormatter stringFromDate:date];
            }
      
        if ([convertedString isEqualToString:@"<null>"] || [convertedString isEqualToString:@""] || [convertedString isEqualToString:@"null"] || convertedString == nil) {
            
            cell.marketPlace_posted_label.text=@"";
            
        }
        else{
            cell.marketPlace_posted_label.text=[NSString stringWithFormat:@"On %@",convertedString];
            cell.marketPlace_posted_label.numberOfLines=3;
        }
        
        
        
    NSString*priceString=[NSString stringWithFormat:@"%@",[[_arrayOfMarketPlaceList objectAtIndex:indexPath.row] objectForKey:@"post_price"]];
    if ([priceString isEqualToString:@"<null>"] || [priceString isEqualToString:@""] || [priceString isEqualToString:@"null"] || priceString == nil || [priceString isEqualToString:@"0.00"] || [priceString isEqualToString:@"0"]) {
        priceString=@"";
        cell.marketPlace_price_label.text=priceString;
        cell.marketPlace_posted_label.hidden=NO;
        
    }
    else
    {

            cell.marketPlace_posted_label.hidden=YES;
        cell.marketPlace_price_label.numberOfLines=3;
            cell.marketPlace_price_label.text=[NSString stringWithFormat:@"$ %@",priceString];
        
    }
    
    
    NSString *item_image_string1 = [NSString stringWithFormat:@"%@", [[_arrayOfMarketPlaceList objectAtIndex:indexPath.row] objectForKey:@"post_image"]];
    NSURL*url=[NSURL URLWithString:item_image_string1];
    [cell.marketPlace_item_imageView sd_setImageWithURL:url
                            placeholderImage:[UIImage imageNamed:@"add_places_placeholder"]
                                     options:SDWebImageRefreshCached];
        cell.marketPlace_item_imageView.contentMode=UIViewContentModeScaleAspectFill;
        cell.marketPlace_item_imageView.clipsToBounds=YES;
        
        
    
    
    return cell;
    }
    else
    {
        MyPostsCollectionViewCell *cell = [self.myPost_collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        
        NSString*titleNameString1=[NSString stringWithFormat:@"%@",[[_arrayOfMyPostsList objectAtIndex:indexPath.row] objectForKey:@"post_title"]];
        if ([titleNameString1 isEqualToString:@"<null>"] || [titleNameString1 isEqualToString:@""] || [titleNameString1 isEqualToString:@"null"] || titleNameString1 == nil) {
            
            titleNameString1=@"";
            cell.myPosts_title_label.text=titleNameString1;
        }
        else{
            cell.myPosts_title_label.text=titleNameString1;
            cell.myPosts_title_label.numberOfLines=1;
        }
//        NSString*postedString=[NSString stringWithFormat:@"%@",[[_arrayOfMyPostsList objectAtIndex:indexPath.row] objectForKey:@"created_date_time"]];
//        if ([postedString isEqualToString:@"<null>"] || [postedString isEqualToString:@""] || [postedString isEqualToString:@"null"] || postedString == nil) {
//            
//            postedString=@"";
//            cell.myPosts_posted_label.text=postedString;
//          
//        }
//        else{
//            cell.myPosts_posted_label.text=[NSString stringWithFormat:@"Posted %@",postedString];
//            cell.myPosts_posted_label.numberOfLines=3;
//            
//        }
        
        
        
        
        NSString *convertedString1;
        NSString *date_str1 = [NSString stringWithFormat:@"%@",[[_arrayOfMyPostsList objectAtIndex:indexPath.row] objectForKey:@"created_date_time"]]; /// here this is your date with format yyyy-MM-dd
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"]; //// here set format of date which is in your output date (means above str with format)
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        NSDate *date1 = [dateFormatter dateFromString: date_str1]; // here you can fetch date from string with define format
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
         NSString *curent_date=[dateFormatter stringFromDate:[NSDate date]];
        convertedString1 = [dateFormatter stringFromDate:date1]; //here convert date in NSString
//        NSLog(@" myPost Converted String : %@",convertedString1);
        if ([curent_date compare:convertedString1]==NSOrderedSame) {
           
//            [dateFormatter setDateFormat:@"HH:mm"];
             [dateFormatter setDateFormat:@"hh:mm a"];
            [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
            convertedString1 = [dateFormatter stringFromDate:date1];
        }
        
        if ([convertedString1 isEqualToString:@"<null>"] || [convertedString1 isEqualToString:@""] || [convertedString1 isEqualToString:@"null"] || convertedString1 == nil) {
            
            cell.myPosts_posted_label.text=@"";
            
        }
        else{
            cell.myPosts_posted_label.text=[NSString stringWithFormat:@"On %@",convertedString1];
            cell.myPosts_posted_label.numberOfLines=2;
        }

        NSString*priceString=[NSString stringWithFormat:@"%@",[[_arrayOfMyPostsList objectAtIndex:indexPath.row] objectForKey:@"post_price"]];
        if ([priceString isEqualToString:@"<null>"] || [priceString isEqualToString:@""] || [priceString isEqualToString:@"null"] || priceString == nil || [priceString isEqualToString:@"0.00"] || [priceString isEqualToString:@"0"]) {
            priceString=@"";
            cell.myPosts_price_label.text=priceString;
            cell.myPosts_posted_label.hidden=NO;
        }
        else
        {
            cell.myPosts_posted_label.hidden=YES;
            cell.myPosts_price_label.numberOfLines=3;
            cell.myPosts_price_label.text=[NSString stringWithFormat:@"$ %@",priceString];
        }
        
        
        NSString *item_image_string1 = [NSString stringWithFormat:@"%@", [[_arrayOfMyPostsList objectAtIndex:indexPath.row] objectForKey:@"post_image"]];
        NSURL*url=[NSURL URLWithString:item_image_string1];
        [cell.myPosts_item_imageView sd_setImageWithURL:url
                                           placeholderImage:[UIImage imageNamed:@"add_places_placeholder"]
                                                    options:SDWebImageRefreshCached];
        cell.myPosts_item_imageView.contentMode=UIViewContentModeScaleAspectFill;
        cell.myPosts_item_imageView.clipsToBounds=YES;
        
        
        return cell;

    }
    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (collectionView==_marketPlace_collectionView) {
        
        MarketPlaceDetail*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"MarketPlaceDetail"];
        marketPlacePostIDString=[NSString stringWithFormat:@"%@",[[_arrayOfMarketPlaceList objectAtIndex:indexPath.row] objectForKey:@"post_id"]];
        controller.marketPlaceId=[NSString stringWithFormat:@"%@",marketPlacePostIDString];
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if (collectionView==_myPost_collectionView)
    {
        MypostDetailView*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"MypostDetailView"];
        marketPlacePostIDString=[NSString stringWithFormat:@"%@",[[_arrayOfMyPostsList objectAtIndex:indexPath.row] objectForKey:@"post_id"]];
//        marketPlacePostIDString=[NSString stringWithFormat:@"2"];
        controller.myPostIDString=[NSString stringWithFormat:@"%@",marketPlacePostIDString];
        [self.navigationController pushViewController:controller animated:YES];
    }
    
    
}


-(void)marketPlaceListMenthod{
    if (![SHARED_HELPER checkIfisInternetAvailable]) {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
//    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//    activeIndicatore.center = self.view.center;
//    activeIndicatore.color = APP_YELLOW_COLOR;
//    activeIndicatore.hidesWhenStopped = TRUE;
//    [self.view addSubview:activeIndicatore];
//    [activeIndicatore startAnimating];
//    indicaterview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
//    activeIndicatore.backgroundColor=[UIColor clearColor];
//    activeIndicatore = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//    activeIndicatore.frame = CGRectMake(0.0, 0.0, 80, 80);
//    activeIndicatore.center = self.view.center;
//    activeIndicatore.color = APP_YELLOW_COLOR;
//    [indicaterview addSubview:activeIndicatore];
//    [self.view addSubview:indicaterview];
//    [indicaterview bringSubviewToFront:self.view];
//    [activeIndicatore startAnimating];
//    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    

    indicaterview.hidden=NO;

       NSMutableDictionary *marketPlace_dict = [NSMutableDictionary new];
  
    [marketPlace_dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
    [marketPlace_dict setObject:[NSNumber numberWithDouble:marketPlace_currentLatitude] forKey:@"post_latitute"];
    [marketPlace_dict setObject:[NSNumber numberWithDouble:marketPlace_currentLongitude] forKey:@"post_longitude"];
    
//    [marketPlace_dict setObject:@"17.52774775029861" forKey:@"post_latitute"];
//    [marketPlace_dict setObject:@"78.48384171731628" forKey:@"post_longitude"];
    [marketPlace_dict setObject:categoryIDString forKey:@"categories_id"];
    
//     "{
//     ""user_id"":"""",
//     ""post_latitute"":"""",
//     ""post_longitude"":""""
//     }"
    
    [SHARED_API marketPlaceListRequestsWithParams:marketPlace_dict withSuccess:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           NSLog(@"Market Place List Response %@",response);
                           if([[response objectForKey:STATUS] isEqualToString:SUCCESS] || [[response objectForKey:STATUS] isEqualToString:@"Successs"])
                           {
                              ;
                              
                               _arrayOfMarketPlaceList=[response valueForKey:@"Near_by_market_places"];
                               
                               if (_arrayOfMarketPlaceList.count>0) {
                                   NSLog(@"self.noMarketPlacesData_Label.hidden=YES");
                                   self.noMarketPlacesData_Label.hidden=YES;
                                   self.marketPlace_filterView.hidden=NO;
                                    [self MaeketPlace_Category_Service];
                                   
                                   isPostAvailable_annotation = @"YES";
                               }
                               else
                               {
                                   NSLog(@"self.noMarketPlacesData_Label.hidden=NO");
                                   self.noMarketPlacesData_Label.hidden=NO;
                                   
                               }
                              
                               if (_arrayOfMarketPlaceList.count > 0) {
                                    [_marketPlace_collectionView setContentOffset:CGPointZero animated:NO];
                               }
                               [_marketPlace_collectionView reloadData];
//                               [activeIndicatore stopAnimating];
                               indicaterview.hidden=YES;
//                               [self MaeketPlace_Category_Service];
                               
                               
                               //Display Annotation
                               // Show coach marks
                               BOOL coachMarksShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"MPCoachMarksShown_MarketPlace"];
                               if (coachMarksShown == NO) {
                                   // Don't show again
                                   [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MPCoachMarksShown_MarketPlace"];
                                   [[NSUserDefaults standardUserDefaults] synchronize];
                                   
                                   // Show coach marks
                                   [self showAnnotation];
                               }
                               
                           }
                            else if ([[response objectForKey:STATUS] isEqualToString:FAIL])
                           {
                               _arrayOfMarketPlaceList=[response valueForKey:@"Near_by_market_places"];
                               if (_arrayOfMarketPlaceList.count==0)
                               {
                                   self.noMarketPlacesData_Label.hidden=NO;
                                   _categoryTypeLabel.hidden=YES;
                                   _categoryBtn.hidden=YES;
                               }

                              else if ([[response valueForKey:@"Near_by_market_places"]isEqualToString:@""]) {
                                   
                                   self.noMarketPlacesData_Label.hidden=NO;
                               }
                               else
                               {
                                    self.noMarketPlacesData_Label.hidden=YES;
                                   _categoryTypeLabel.hidden=NO;
                                   _categoryBtn.hidden=NO;
                               }
                              
//                                   [activeIndicatore stopAnimating];
                               indicaterview.hidden=YES;
                               
                               
//                               [SHARED_HELPER showAlert:Forgotfail];
                           }
//                           [activeIndicatore stopAnimating];
                           indicaterview.hidden=YES;
                       });
        
    } onfailure:^(NSError *theError) {
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           
//                           if ([activeIndicatore isAnimating]) {
//                               [activeIndicatore stopAnimating];
//                               [activeIndicatore removeFromSuperview];
//                           }
                           indicaterview.hidden=YES;
                           [SHARED_HELPER showAlert:ServiceFail];
                           
                           
                       });
    }];
    
    
}
-(void)myPostsListMethod
{
    if (![SHARED_HELPER checkIfisInternetAvailable]) {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
    
//    [activeIndicatore startAnimating];
    indicaterview.hidden=NO;
   [SHARED_API myPosts_Request_List:[Defaults valueForKey:User_ID] cat_ID:myPostCategoryIDString
                        withSuccess:^(NSDictionary *response) {
       
       
//    [SHARED_API myPosts_Request_List:@"18" withSuccess:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           NSLog(@"My Posts List Response %@",response);
                           if([[response objectForKey:STATUS] isEqualToString:SUCCESS])
                           {
                               
                               _arrayOfMyPostsList=[response valueForKey:@"posts"];
                               
                               
                               if (_arrayOfMyPostsList.count >0) {
                                   self.noPostsDataLabel.hidden=YES;
                                   self.myPost_filterView.hidden=NO;
                                   
                                   [self MaeketPlace_Category_Service];
                               }
                               else
                               {
                                   self.noPostsDataLabel.hidden=NO;
                                   self.myPost_filterView.hidden=YES;
                               }
                               
                               
//                                [activeIndicatore stopAnimating];
                               indicaterview.hidden=YES;
                               if (_arrayOfMyPostsList.count>0) {
                                [_myPost_collectionView setContentOffset:CGPointZero animated:NO];
                               }
                               [_myPost_collectionView reloadData];
                               
                               
                              
                           }
                           else if([[response objectForKey:STATUS] isEqualToString:FAIL])
                           
                           {
                               
                               
                               if ([[response valueForKey:@"posts"]isEqualToString:@""]) {
                                   self.noPostsDataLabel.hidden=NO;
                               }
                               else
                               {
                                    self.noPostsDataLabel.hidden=YES;
                               }
                               
//                                 [activeIndicatore stopAnimating];
                               indicaterview.hidden=YES;
//
                           }
                           
                       });

        
        
    } onfailure:^(NSError *theError) {
       
//        [activeIndicatore stopAnimating];
        indicaterview.hidden=YES;
    }];
}

- (IBAction)onClickMarketPlaceToggleBtn:(id)sender {
    if ([toggleString isEqualToString:@"MARKETPLACE"]) {
        toggleString=@"MYPOSTS";
        self.title = @"MARKETPLACE";
        _toogle_market_post_imageView.image=[UIImage imageNamed:@"myposts_toggle"];
        AddCatString=@"YES";
        _marketPlaceContentView.hidden=NO;
//
       
    }
    else
    {
        toggleString=@"MARKETPLACE";
        self.title = @"MY POSTS";
        [self.navigationController.navigationBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:[UIColor blackColor],
           NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
        AddCatString=@"NO";
        _marketPlaceContentView.hidden=YES;
        
//        [UIView transitionWithView:self.myPostsContentView
//                          duration:0.5
//                           options:UIViewAnimationOptionAutoreverse
//                        animations:^(void){
        
                            _myPostsContentView.hidden=NO;
//                            [self.view layoutIfNeeded];
//                        }
//                        completion:nil];
        
         _toogle_market_post_imageView.image=[UIImage imageNamed:@"marketPlace_toggle"];
        if (_arrayOfMarketPlaceList.count==0) {
                      self.noMarketPlacesData_Label.hidden=NO;
                  }

       
    }
}
#pragma mark - Category_Type_Service
-(void)MaeketPlace_Category_Service{

    if (![SHARED_HELPER checkIfisInternetAvailable]) {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
//    [activeIndicatore startAnimating];
    indicaterview.hidden=NO;
    
      [SHARED_API get_marketPlace_Category_Type:@"" withSuccess:^(NSDictionary *response) {
        
        if ([[response valueForKey:STATUS]isEqualToString:SUCCESS]) {
            _arrayOfCategoryTypes=[[NSMutableArray alloc]init];
            _arrayOfCategoryTypeIDs=[[NSMutableArray alloc]init];
            
            NSArray *cat_names=[[response valueForKey:@"categories"] valueForKey:@"category_name"];
            NSArray * cat_ids=[[response valueForKey:@"categories"] valueForKey:@"id"];
            
            [_arrayOfCategoryTypes addObject:@"All"];
            [_arrayOfCategoryTypeIDs addObject:@"0"];
            [_arrayOfCategoryTypes addObjectsFromArray:cat_names];
            [_arrayOfCategoryTypeIDs addObjectsFromArray:cat_ids];
            
            
            
//            [activeIndicatore stopAnimating];
            indicaterview.hidden=YES;
            }
        else{
            
            [SHARED_HELPER showAlert:ServiceFail];
//            [activeIndicatore stopAnimating];
            indicaterview.hidden=YES;
        }
        }
     
        
     onfailure:^(NSError *theError) {
        [SHARED_HELPER showAlert:ServerConnection];
//         [activeIndicatore stopAnimating];
         indicaterview.hidden=YES;
    }];

}

- (IBAction)onClick_categoryBtn:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"categories"
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    // ObjC Fast Enumeration
    for (NSString *title in _arrayOfCategoryTypes) {
        [actionSheet addButtonWithTitle:title];
    }
    actionSheet.tag=100;
    actionSheet.cancelButtonIndex = [actionSheet addButtonWithTitle:@"Cancel"];
    [actionSheet showInView:self.view];

}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag==100) {
//        AddCatString=@"YES";
        if (buttonIndex>=_arrayOfCategoryTypes.count) {
            
        }
        else
        {
            _categoryTypeLabel.text=[NSString stringWithFormat:@"%@",[_arrayOfCategoryTypes objectAtIndex:buttonIndex]];
            NSString* prev_catID=[NSString stringWithFormat:@"%@",[_arrayOfCategoryTypeIDs objectAtIndex:buttonIndex]];
            if ([prev_catID isEqualToString:categoryIDString]) {
                
            }
            else
            {
                categoryIDString=prev_catID;
                [self marketPlaceListMenthod];
            }
           
        }
     }
    else
    {
//        AddCatString=@"YES";
        if (buttonIndex>=_arrayOfCategoryTypes.count) {
            
        }
        else
        {
            _myPost_filterLabel.text=[NSString stringWithFormat:@"%@",[_arrayOfCategoryTypes objectAtIndex:buttonIndex]];
            NSString* prev_post_catID=[NSString stringWithFormat:@"%@",[_arrayOfCategoryTypeIDs objectAtIndex:buttonIndex]];
            if ([prev_post_catID isEqualToString:myPostCategoryIDString]) {
                
            }
            else
            {
                myPostCategoryIDString=prev_post_catID;
                [self myPostsListMethod];
            }
        }
    }
   }
- (IBAction)onClick_filterButton:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"categories"
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    // ObjC Fast Enumeration
    for (NSString *title in _arrayOfCategoryTypes) {
        [actionSheet addButtonWithTitle:title];
    }
    actionSheet.cancelButtonIndex = [actionSheet addButtonWithTitle:@"Cancel"];
    [actionSheet showInView:self.view];
}
- (IBAction)onClick_marketPlace_rideGoingBtn:(id)sender {
    
    self.marketPlace_scrollLabel.hidden=YES;
    self.marketPlace_rideGoingBtn.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }

}
@end
