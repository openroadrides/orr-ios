//
//  SelectGroupsCell.h
//  openroadrides
//
//  Created by apple on 06/07/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectGroupsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *selectgroupell_bgView;
@property (weak, nonatomic) IBOutlet UIImageView *selectGroup_groupImageView;
@property (weak, nonatomic) IBOutlet UIButton *selectGroup_checkbox_btn;
@property (weak, nonatomic) IBOutlet UILabel *selectGroup_createdBy_label;
@property (weak, nonatomic) IBOutlet UILabel *selectGroup_userName_label;
@property (weak, nonatomic) IBOutlet UILabel *selectGroup_members_label;

@end
