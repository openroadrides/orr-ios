//
//  NewsDetailViewController.h
//  openroadrides
//
//  Created by apple on 30/05/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "RideDashboard.h"
@interface NewsDetailViewController : UIViewController{
    UIActivityIndicatorView  *activeIndicatore;

}
@property (weak, nonatomic) IBOutlet UIImageView *news_detail_img;
@property (weak, nonatomic) IBOutlet UILabel *mounth_lbl;
@property (weak, nonatomic) IBOutlet UILabel *day_lbl;
@property (weak, nonatomic) IBOutlet UILabel *title_lbl;
@property (weak, nonatomic) IBOutlet UILabel *description_lbl;
@property (nonatomic, strong) NSString *news_id,*news_image_share_string;
@property (weak, nonatomic) IBOutlet UIView *content_view;
@property (weak, nonatomic) IBOutlet UIButton *newsDetail_rideGoingBtn;
- (IBAction)onClick_newsDetail_rideGoingBtn:(id)sender;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *newsDetail_scrollLabel;
@end
