//
//  ForgotpasswordViewController.m
//  
//
//  Created by apple on 01/05/17.
//
//

#import "ForgotpasswordViewController.h"
#import "ForgotPasswordViewController.h"
#import "Constants.h"
#import "AppHelperClass.h"
#import "APIHelper.h"
#import "LoginViewController.h"

@interface ForgotpasswordViewController ()

@end

@implementation ForgotpasswordViewController



- (void)viewDidLoad {
    
    
    [super viewDidLoad];
       
    [self.navigationController.navigationBar setBarTintColor:APP_YELLOW_COLOR];
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController.navigationBar setTranslucent:NO];
    self.navigationController.navigationBar.backItem.title=@"";
    [self.view layoutIfNeeded];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 120, 30)];
    label.textAlignment = NSTextAlignmentCenter;
    [label setFont:[UIFont fontWithName:@"Antonio-Bold" size:20]];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:[UIColor blackColor]];
    [label setText:@"FORGOT PASSWORD"];
    [self.navigationController.navigationBar.topItem setTitleView:label];
    [self.Email_tf setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.Email_tf setDelegate:self];
  [self.Email_tf setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)Send_mail_click:(id)sender {
    
    if (![SHARED_HELPER checkIfisInternetAvailable]) {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
    [self.view endEditing:YES];
    if(_Email_tf.text.length == 0)
    {
        [SHARED_HELPER showAlert:EmailText];
    }
    else
    {
        BOOL isValidEmail = [SHARED_HELPER checkEmailValidations:_Email_tf.text];
        if(isValidEmail)
        {
            //Call email submit service for reset password and on success direct to login screen.
            [self email_send];
        }
        else
        {
            [SHARED_HELPER showAlert:ValidEmailText];
        }
    }

    
    
}

-(void)email_send{
    
    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    activeIndicatore.hidesWhenStopped = TRUE;
    [activeIndicatore startAnimating];
    [self.view addSubview:activeIndicatore];
    
    
   NSMutableDictionary *email_dict = [NSMutableDictionary new];
    
    [email_dict setObject:_Email_tf.text forKey:@"email"];

    [SHARED_API forgotPwdRequestsWithParams:email_dict withSuccess:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           NSLog(@"%@",response);
                           
                           NSLog(@"userInfo%@",response);
//                           _userid=[response objectForKey:@"user_id"];
                           if([[response objectForKey:STATUS] isEqualToString:SUCCESS])
                           {
                               
                               if ([activeIndicatore isAnimating]) {
                                   [activeIndicatore stopAnimating];
                                   [activeIndicatore removeFromSuperview];
                               }

                               [self.navigationController popViewControllerAnimated:YES];
                               [SHARED_HELPER showAlert:Check_mail];
                               
                               
                           }
                           else
                           {
                               if ([activeIndicatore isAnimating]) {
                                   [activeIndicatore stopAnimating];
                                   [activeIndicatore removeFromSuperview];
                               }
                               
                               [SHARED_HELPER showAlert:Forgotfail];
                           }
                           
                       });
        
    } onfailure:^(NSError *theError) {
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           
                           if ([activeIndicatore isAnimating]) {
                               [activeIndicatore stopAnimating];
                               [activeIndicatore removeFromSuperview];
                           }
                           
                           [SHARED_HELPER showAlert:ServiceFail];
                           
                           
                       });
    }];

    
}

- (IBAction)Login_click:(id)sender {
    
     [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma - mark TextField Delegate Methods

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -100., self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +100., self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
       [textField resignFirstResponder];
    return YES;
}



@end
