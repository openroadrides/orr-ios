//
//  APIHelper.h
//  Thredz
//
//  Created by Jyothsna on 7/8/16.
//  Copyright © 2016 ebiz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebService.h"
// *******************************
// |-+ API HELPER SUCCESS CALL-BACK
// *******************************

typedef void (^APIHelperSuccess) (id json);

@interface APIHelper : NSObject

// *************************************************
// /* Thredz
// /* Thredz - MOBILE SERVICES
// /* APIHELPER
// *************************************************

+ (APIHelper *)sharedClient;

//Methods

-(void)signInRequestsWithParams:(NSDictionary *)params
                    withSuccess:(void (^)(NSDictionary *response))result
                      onfailure:(WebServiceFailure)failure;
-(void)signupRequestsWithParams:(NSDictionary *)params
                    withSuccess:(void (^)(NSDictionary *response))result
                      onfailure:(WebServiceFailure)failure;
-(void)social_loginRequestsWithParams:(NSDictionary *)params
                          withSuccess:(void (^)(NSDictionary *response))result
                            onfailure:(WebServiceFailure)failure;
-(void)social_registrationRequestsWithParams:(NSDictionary *)params
                                 withSuccess:(void (^)(NSDictionary *response))result
                                   onfailure:(WebServiceFailure)failure;
-(void)socialUpdateRequestsWithParams:(NSDictionary *)params
                          withSuccess:(void (^)(NSDictionary *response))result
                            onfailure:(WebServiceFailure)failure;

-(void)normalUpdateRequestsWithParams:(NSDictionary *)params
                          withSuccess:(void (^)(NSDictionary *response))result
                            onfailure:(WebServiceFailure)failure;
-(void)forgotPwdRequestsWithParams:(NSDictionary *)params
                       withSuccess:(void (^)(NSDictionary *response))result
                         onfailure:(WebServiceFailure)failure;

-(void)newsRequestsWithParams:(NSDictionary *)params
                  withSuccess:(void (^)(NSDictionary *response))result
                    onfailure:(WebServiceFailure)failure;


-(void)Routes_Api:(NSDictionary *)params
      withSuccess:(void (^)(NSDictionary *response))result
        onfailure:(WebServiceFailure)failure;

-(void)newsdetailRequestsWithParams:(NSString *)news_id
                        withSuccess:(void (^)(NSDictionary *response))result
                          onfailure:(WebServiceFailure)failure;

-(void)groupsRequestsWithParams:(NSString *)user_token_id
                    withSuccess:(void (^)(NSDictionary *response))result
                      onfailure:(WebServiceFailure)failure;

-(void)groupsdetailRequestsWithParams:(NSString *)group_id user_ID:(NSString *)userid
                          withSuccess:(void (^)(NSDictionary *response))result
                            onfailure:(WebServiceFailure)failure;
-(void)createGroupRequestsWithParams:(NSDictionary *)params
                         withSuccess:(void (^)(NSDictionary *response))result
                           onfailure:(WebServiceFailure)failure;
-(void)notificationsRequestsWithParams:(NSString *)group_id
                           withSuccess:(void (^)(NSDictionary *response))result
                             onfailure:(WebServiceFailure)failure;
-(void)store_ride_checkin:(NSDictionary *)params
              withSuccess:(void (^)(NSDictionary *response))result
                onfailure:(WebServiceFailure)failure;
-(void)eventsRequestsWithParams:(NSString *)user_id
                    withSuccess:(void (^)(NSDictionary *response))result
                      onfailure:(WebServiceFailure)failure;
-(void)endRideRequestsWithParams:(NSDictionary *)params
                     withSuccess:(void (^)(NSDictionary *response))result
                       onfailure:(WebServiceFailure)failure;
-(void)createRideRequestsWithParams:(NSDictionary *)params
                        withSuccess:(void (^)(NSDictionary *response))result
                          onfailure:(WebServiceFailure)failure;
-(void)eventsDetailRequestsWithParams:(NSString *)event_id
                          withSuccess:(void (^)(NSDictionary *response))result
                            onfailure:(WebServiceFailure)failure;
-(void)createEventRequestsWithParams:(NSDictionary *)params
                         withSuccess:(void (^)(NSDictionary *response))result
                           onfailure:(WebServiceFailure)failure;

-(void)publicFriendsRequest:(NSString *)userid public_myfriends:(NSString *)Public_OR_myfriends_Service
                withSuccess:(void (^)(NSDictionary *response))result
                  onfailure:(WebServiceFailure)failure;

-(void)add_a_friend_Request1:(NSDictionary *)params
                 withSuccess:(void (^)(NSDictionary *response))result
                   onfailure:(WebServiceFailure)failure;
-(void)accept_reject_frnd_request:(NSDictionary *)params
                      withSuccess:(void (^)(NSDictionary *response))result
                        onfailure:(WebServiceFailure)failure;
-(void)scheduleRideRequestsWithParams:(NSString *)user_token_id
                          withSuccess:(void (^)(NSDictionary *response))result
                            onfailure:(WebServiceFailure)failure;
-(void)selectedFriendsAndGroupsRequest:(NSString *)userid public_myfriends:(NSString *)Friends_OR_groups_Service
                           withSuccess:(void (^)(NSDictionary *response))result
                             onfailure:(WebServiceFailure)failure;
-(void)myRidesRequest:(NSString *)userid scheduled_fullFilled:(NSString *)Scheduled_OR_Fullfilled_Service
          withSuccess:(void (^)(NSDictionary *response))result
            onfailure:(WebServiceFailure)failure;
-(void)rideDetailRequestsWithParams:(NSDictionary *)params
                        withSuccess:(void (^)(NSDictionary *response))result
                          onfailure:(WebServiceFailure)failure;
-(void)updateUserlocationRequestsWithParams:(NSDictionary *)params
                                withSuccess:(void (^)(NSDictionary *response))result
                                  onfailure:(WebServiceFailure)failure;

-(void)deleteRideRequestsWithParams:(NSString *)ride_id user_ID:(NSString *)userid
                        withSuccess:(void (^)(NSDictionary *response))result
                          onfailure:(WebServiceFailure)failure;
-(void)DisplayUserProfile:(NSString *)user_Id
              withSuccess:(void (^)(NSDictionary *response))result
                onfailure:(WebServiceFailure)failure;

-(void)Update_User_Profile:(NSDictionary *)params
               withSuccess:(void (^)(NSDictionary *response))result
                 onfailure:(WebServiceFailure)failure;

-(void)routedetailRequestsWithParams:(NSString *)Route_id
                         withSuccess:(void (^)(NSDictionary *response))result
                           onfailure:(WebServiceFailure)failure;
-(void)Un_Frined_service:(NSDictionary *)params
             withSuccess:(void (^)(NSDictionary *response))result
               onfailure:(WebServiceFailure)failure;
-(void)notify_users_RequestsWithParams:(NSDictionary *)params
                           withSuccess:(void (^)(NSDictionary *response))result
                             onfailure:(WebServiceFailure)failure;
-(void)editRideRequestsWithParams:(NSDictionary *)params
                      withSuccess:(void (^)(NSDictionary *response))result
                        onfailure:(WebServiceFailure)failure;
-(void)dashboardRequestsWithParams:(NSString *)user_id
                       withSuccess:(void (^)(NSDictionary *response))result
                         onfailure:(WebServiceFailure)failure;

-(void)leave_ride_RequestsWithParams:(NSDictionary *)params
                         withSuccess:(void (^)(NSDictionary *response))result
                           onfailure:(WebServiceFailure)failure;
-(void)startimediateRideRequestsWithParams:(NSDictionary *)params
                               withSuccess:(void (^)(NSDictionary *response))result
                                 onfailure:(WebServiceFailure)failure;
-(void)leaveGroupRequestsWithParams:(NSDictionary *)params
                        withSuccess:(void (^)(NSDictionary *response))result
                          onfailure:(WebServiceFailure)failure;
-(void)get_Riders_List:(NSString *)ride_id

           withSuccess:(void (^)(NSDictionary *response))result

             onfailure:(WebServiceFailure)failure;
-(void)accept_reject_group_request:(NSDictionary *)params

                       withSuccess:(void (^)(NSDictionary *response))result

                         onfailure:(WebServiceFailure)failure;

-(void)accept_reject_ride_request:(NSDictionary *)params

                      withSuccess:(void (^)(NSDictionary *response))result

                        onfailure:(WebServiceFailure)failure;
-(void)notification_read_unread_status_RequestsWithParams:(NSDictionary *)params

                                              withSuccess:(void (^)(NSDictionary *response))result

                                                onfailure:(WebServiceFailure)failure;
-(void)accept_reject_notification_ride_request:(NSDictionary *)params

                                   withSuccess:(void (^)(NSDictionary *response))result

                                     onfailure:(WebServiceFailure)failure;
-(void)Join_RIdes:(NSDictionary *)params

      withSuccess:(void (^)(NSDictionary *response))result

        onfailure:(WebServiceFailure)failure;
-(void)browseRidersWithParams:(NSDictionary *)params
                  withSuccess:(void (^)(NSDictionary *response))result
                    onfailure:(WebServiceFailure)failure;
-(void)promotionsWithParams:(NSDictionary *)params
                withSuccess:(void (^)(NSDictionary *response))result
                  onfailure:(WebServiceFailure)failure;

-(void)feedbackRequestsWithParams:(NSDictionary *)params
                      withSuccess:(void (^)(NSDictionary *response))result
                        onfailure:(WebServiceFailure)failure;

-(void)marketPlaceListRequestsWithParams:(NSDictionary *)params
                             withSuccess:(void (^)(NSDictionary *response))result
                               onfailure:(WebServiceFailure)failure;
-(void)myPosts_Request_List:(NSString *)user_id  cat_ID:(NSString *)catID
                withSuccess:(void (^)(NSDictionary *response))result
                  onfailure:(WebServiceFailure)failure;
-(void)getMarketPlace_Details_Request:(NSString *)post_id user_ID:(NSString *)userid
                          withSuccess:(void (^)(NSDictionary *response))result
                            onfailure:(WebServiceFailure)failure;
-(void)getMyPost_Details_Request:(NSString *)post_id
                     withSuccess:(void (^)(NSDictionary *response))result
                       onfailure:(WebServiceFailure)failure;
-(void)close_Post_Request:(NSString *)post_id user_ID:(NSString *)userid
              withSuccess:(void (^)(NSDictionary *response))result
                onfailure:(WebServiceFailure)failure;

-(void)marketPlace_intrested_RequestsWithParams:(NSDictionary *)params
                                    withSuccess:(void (^)(NSDictionary *response))result
                                      onfailure:(WebServiceFailure)failure;
-(void)get_feature_promotion:(NSString *)promotion_id
                 withSuccess:(void (^)(NSDictionary *response))result
                   onfailure:(WebServiceFailure)failure;

-(void)get_promotion_detail_page:(NSString *)promotion_id
                     withSuccess:(void (^)(NSDictionary *response))result
                       onfailure:(WebServiceFailure)failure;
-(void)createMyPostRequestWithParams:(NSDictionary *)params
                         withSuccess:(void (^)(NSDictionary *response))result
                           onfailure:(WebServiceFailure)failure;
-(void)Edit_EventRequestsWithParams:(NSDictionary *)params
                        withSuccess:(void (^)(NSDictionary *response))result
                          onfailure:(WebServiceFailure)failure;
-(void)get_Delete_event_page:(NSString *)event_id
                 withSuccess:(void (^)(NSDictionary *response))result
                   onfailure:(WebServiceFailure)failure;
-(void)get_Social_Links_page:(NSString *)not_requaired
                 withSuccess:(void (^)(NSDictionary *response))result
                   onfailure:(WebServiceFailure)failure;
-(void)get_marketPlace_Category_Type:(NSString *)not_requaired
                         withSuccess:(void (^)(NSDictionary *response))result
                           onfailure:(WebServiceFailure)failure;
-(void)UpdateMyPostRequestWithParams:(NSDictionary *)params
                         withSuccess:(void (^)(NSDictionary *response))result
                           onfailure:(WebServiceFailure)failure;
-(void)UpdateGroupRequestsWithParams:(NSDictionary *)params
                         withSuccess:(void (^)(NSDictionary *response))result
                           onfailure:(WebServiceFailure)failure;

@end
