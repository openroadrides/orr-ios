//
//  ProfileView.m
//  openroadrides
//
//  Created by apple on 10/07/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "ProfileView.h"

@interface ProfileView ()

@end

@implementation ProfileView

- (void)viewDidLoad {
    [super viewDidLoad];
    
     appDelegate.un_frind_in_Profile=@"";
    if ([_is_From isEqualToString:@"Friends"]||[_is_From isEqualToString:@"Groups"]) {
         self.title = @"FRIEND PROFILE";
       
    }
    else{
         self.title = @"PROFILE";
    }
    
    _total_Miles_Count.text=@"0";
    _avg_Spead.text=@"0";
    _check_Ins_Count.text=@"0";
    _friends_Count.text=@"0";
    _groups_Count.text=@"0";
  
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    
    self.viewProfile_scrollLabel.hidden=YES;
    self.viewProfile_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.viewProfile_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.viewProfile_scrollLabel.textColor = [UIColor blackColor];
    self.viewProfile_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.viewProfile_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.viewProfile_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.viewProfile_scrollLabel.scrollSpeed = 60; // pixels per second
    self.viewProfile_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.viewProfile_scrollLabel.fadeLength = 0.f;
    self.viewProfile_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.viewProfile_scrollLabel observeApplicationNotifications];
    
    self.profile_rideGoingBtn.hidden=YES;
    if (appDelegate.ridedashboard_home) {
        self.viewProfile_scrollLabel.hidden=NO;
        self.profile_rideGoingBtn.hidden=NO;
    }
    
     if ([_is_From isEqualToString:@"Friends"]||[_is_From isEqualToString:@"Groups"]||[_is_From isEqualToString:@"Notifications"]) {
         
         if ([_friend_status isEqualToString:@"friend"]) {
             UIButton *Un_Friend_but = [UIButton buttonWithType:UIButtonTypeSystem];
             [Un_Friend_but addTarget:self action:@selector(un_Frined_Action) forControlEvents:UIControlEventTouchUpInside];
             Un_Friend_but.frame = CGRectMake(0, 0, 25, 25);
             [Un_Friend_but setBackgroundImage:[UIImage imageNamed:@"remove_frnd"] forState:UIControlStateNormal];
             UIBarButtonItem *rightBackButton=[[UIBarButtonItem alloc] initWithCustomView:Un_Friend_but];
             self.navigationItem.rightBarButtonItems=[NSArray arrayWithObjects:rightBackButton, nil];
         }
         
    }
    else{
        UIButton *Editbtn = [UIButton buttonWithType:UIButtonTypeSystem];
        [Editbtn addTarget:self action:@selector(edit_Action) forControlEvents:UIControlEventTouchUpInside];
        Editbtn.frame = CGRectMake(0, 0, 25, 25);
        [Editbtn setBackgroundImage:[UIImage imageNamed:@"setup-ride"] forState:UIControlStateNormal];
        UIBarButtonItem *rightBackButton=[[UIBarButtonItem alloc] initWithCustomView:Editbtn];
        self.navigationItem.rightBarButtonItems=[NSArray arrayWithObjects:rightBackButton, nil];
    }
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [leftBtn addTarget:self action:@selector(back_Action) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.frame = CGRectMake(0, 0, 25, 25);
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"back_Image"] forState:UIControlStateNormal];
    UIBarButtonItem *leftBackButton=[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:leftBackButton, nil];
    
    
    self.User_Profile_Imag.layer.cornerRadius=self.User_Profile_Imag.frame.size.width/2;
    self.User_Profile_Imag.clipsToBounds=YES;
    // Do any additional setup after loading the view.
    
    
    //Indication
    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    activeIndicatore.hidesWhenStopped = TRUE;
    [self.view addSubview:activeIndicatore];
    
    
    check_strings_array=[[NSMutableArray alloc]init];
    [check_strings_array addObject:@"[NSNull null]"];
    [check_strings_array addObject:@"null"];
    [check_strings_array addObject:@""];
    [check_strings_array addObject:@"(null)"];
    [check_strings_array addObject:@"<null>"];
    [check_strings_array addObject:@"0"];
    [check_strings_array addObject:@"0000-00-00"];
    
//    if (IS_IPHONE_5_OR_LESS) {
//        [self.totalMilesLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:20]];
//        [self.totalRidesLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:20]];
//        
//        self.totalMilesLabel.font=[UIFont fontWithName:@"soho-std-regular" size:5];
//        self.totalRidesLabel.font=[UIFont fontWithName:@"soho-std-regular" size:5];
//    }
    
     service_sucsess=@"";
     appDelegate.user_Profile_edited=@"";
     [self display_user_Profile];
   
    
}

-(void)viewWillAppear:(BOOL)animated{
    if ([appDelegate.user_Profile_edited isEqualToString:@"YES"]) {
        service_sucsess=@"";
         [self display_user_Profile];
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)display_user_Profile{
    if (![SHARED_HELPER checkIfisInternetAvailable]) {
        [SHARED_HELPER showAlert:Nonetwork];
        int duration = 2; // duration in seconds
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
          [self.navigationController popViewControllerAnimated:YES];
        });
    }
    else{
     [activeIndicatore startAnimating];
        NSString *user_id;
        if ([_is_From isEqualToString:@"Friends"]||[_is_From isEqualToString:@"Groups"]) {
            user_id=_id_user;
        }
        else
        {
            user_id=[Defaults valueForKey:User_ID];
        }
        
   [SHARED_API DisplayUserProfile:user_id withSuccess:^(NSDictionary *response) {
       NSLog(@"%@", response);
       [self user_Profile_Data_Sucsess:response];
        [activeIndicatore stopAnimating];
   } onfailure:^(NSError *theError) {
        [activeIndicatore stopAnimating];
       [SHARED_HELPER showAlert:ServiceFail];
       
       int duration = 2; // duration in seconds
       dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
           [self.navigationController popViewControllerAnimated:YES];
       });
       
   }];
    }
}
//address = "Hyderabad ";
//"average_speed" = 0;
//city = Addison;
//"date_of_birth" = "2017-07-14";
//email = "iossrk@gmail.com";
//gender = Male;
//name = iossrk575ebiz;
//"phone_number" = "824-725-8569";
//privacy = "<null>";
//"profile_image" = "https://s3-us-west-2.amazonaws.com/openroadrides/94/user_images/pmIYmQHHBkK7sQFOH7HdFlf8883TLji5.jpeg";
//state = Alabama;
//"total_checkins" = 0;
//"total_completed_rides" = 3;
//"total_distance_value" = 0;
//"total_friends" = 6;
//"total_groups" = 2;
//"total_routes" = 4;
//"user_id" = 94;
//zipcode = 500018;
-(void)user_Profile_Data_Sucsess:(NSDictionary *)Profile_data{
    if ([[Profile_data valueForKey:STATUS] isEqualToString:SUCCESS]) {
        NSDictionary *user_data=[Profile_data valueForKey:@"data"];
        self.User_name_label.text=[user_data valueForKey:@"name"];
        self.gmail_Label.text=[user_data valueForKey:@"email"];
       NSString * profile_image_str=[user_data valueForKey:@"profile_image"];
        if (![check_strings_array containsObject:profile_image_str]&&![profile_image_str isEqual:[NSNull null]]) {
            profile_image_url=profile_image_str;
            NSURL*url=[NSURL URLWithString:profile_image_str];
            [self.User_Profile_Imag sd_setImageWithURL:url
                                      placeholderImage:[UIImage imageNamed:@"user_Placeholder"]
                                               options:SDWebImageRefreshCached];
        }
        else{
            profile_image_url=@"";
        }
     
        NSString * gender=[user_data valueForKey:@"gender"];
        if (![check_strings_array containsObject:gender]&&![gender isEqual:[NSNull null]]) {
            gender_Str=gender;
        }
        else{
            gender_Str=@"";
        }
        
        NSString *mobile=[NSString stringWithFormat:@"%@",[user_data valueForKey:@"phone_number"]];
        if (![check_strings_array containsObject:mobile]&&![mobile isEqual:[NSNull null]])
        {
            user_number_str=mobile;
            _mobiles_Numner_Label.text=mobile;
        }
        else{
            user_number_str=@"";
            _mobiles_Numner_Label.text=@"N/A";
        }
        
        NSString *address=[user_data valueForKey:@"address"];
        if (![check_strings_array containsObject:address]&&![address isEqual:[NSNull null]])
        {
            user_adress_Str=address;
            _adress_label.text=address;
        }
        else{
            user_adress_Str=@"";
            _adress_label.text=@"";
        }
        
        NSString *city=[user_data valueForKey:@"city"];
        if (![check_strings_array containsObject:city]&&![city isEqual:[NSNull null]])
        {
            city_str=city;
            if ([_adress_label.text isEqualToString:@""]) {
                _adress_label.text=[NSString stringWithFormat:@"%@",city];
            }
            else{
            _adress_label.text=[NSString stringWithFormat:@"%@ %@",_adress_label.text,city];
            }
        }
        else{
            city_str=@"";
           
        }
        
        NSString *date_of_birth=[user_data valueForKey:@"date_of_birth"];
        if (![check_strings_array containsObject:date_of_birth]&&![date_of_birth isEqual:[NSNull null]])
        {
            dob_Str=date_of_birth;
            
        }
        else{
            dob_Str=@"";
            
        }
        
        NSString *state=[user_data valueForKey:@"state"];
        if (![check_strings_array containsObject:state]&&![state isEqual:[NSNull null]])
        {
            state_str=state;
            if ([_adress_label.text isEqualToString:@""]) {
                _adress_label.text=[NSString stringWithFormat:@"%@",state];
            }
            else{
                _adress_label.text=[NSString stringWithFormat:@"%@, %@",_adress_label.text,state];
            }
        }
        else{
            state_str=@"";
            
        }
        
        NSInteger zip_code=[[NSString stringWithFormat:@"%@",[user_data valueForKey:@"zipcode"]] integerValue];
        if (zip_code!=0)
        {
            zipcode_Str=[NSString stringWithFormat:@"%ld",(long)zip_code];
            if ([_adress_label.text isEqualToString:@""]) {
                _adress_label.text=[NSString stringWithFormat:@"%ld",(long)zip_code];
//                _adress_label.text=[NSString stringWithFormat:@"N/A"];
            }
            else{
                _adress_label.text=[NSString stringWithFormat:@"%@ %ld",_adress_label.text,(long)zip_code];
            }
        }
        else{
            zipcode_Str=@"";
            _adress_label.text=@"N/A";
        }
        
        float total_miles= [[NSString stringWithFormat:@"%@",[[Profile_data valueForKey:@"data"] valueForKey:@"total_distance_value"]] floatValue];
        int rounded_iles = roundf(total_miles);
        _total_Miles_Count.text=[NSString stringWithFormat:@"%d",rounded_iles];
        
        float total_avg= [[NSString stringWithFormat:@"%@",[[Profile_data valueForKey:@"data"] valueForKey:@"average_speed"]] floatValue];
        int rounded_avg = roundf(total_avg);
        _avg_Spead.text=[NSString stringWithFormat:@"%d",rounded_avg];
        
        int check_ins = [[NSString stringWithFormat:@"%@",[[Profile_data valueForKey:@"data"] valueForKey:@"total_checkins"]] intValue];
        _check_Ins_Count.text=[NSString stringWithFormat:@"%d",check_ins];
        
        
        int friends_count = [[NSString stringWithFormat:@"%@",[[Profile_data valueForKey:@"data"] valueForKey:@"total_friends"]] intValue];
        _friends_Count.text=[NSString stringWithFormat:@"%d",friends_count];
        
        int groups_count = [[NSString stringWithFormat:@"%@",[[Profile_data valueForKey:@"data"] valueForKey:@"total_groups"]] intValue];
        _groups_Count.text=[NSString stringWithFormat:@"%d",groups_count];
        
        int rides_count = [[NSString stringWithFormat:@"%@",[[Profile_data valueForKey:@"data"] valueForKey:@"total_completed_rides"]] intValue];
        _total_Rides_Count.text=[NSString stringWithFormat:@"%d",rides_count];
        
        
        service_sucsess=@"YES";
        
    }
    else{
        [SHARED_HELPER showAlert:@"Failed to get your profile. Please try again."];
        int duration = 0; // duration in seconds
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self.navigationController popViewControllerAnimated:YES];
        });
    }
}
-(void)un_Frined_Action{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:@"Are you sure you want to unfriend?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"YES"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [self Un_Friend_Service];
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"CANCEL"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)Un_Friend_Service{
    if (![SHARED_HELPER checkIfisInternetAvailable]) {
        [SHARED_HELPER showAlert:Nonetwork];
        int duration = 2; // duration in seconds
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self.navigationController popViewControllerAnimated:YES];
        });
    }
    else{
        [activeIndicatore startAnimating];
        NSString *user_id,*friend_Id;
        friend_Id=_id_user;
        user_id=[Defaults valueForKey:User_ID];
        NSMutableDictionary *data_dict=[[NSMutableDictionary alloc]init];
        [data_dict setValue:user_id forKey:@"user_id"];
        [data_dict setValue:friend_Id forKey:@"user_friend_id"];
        [SHARED_API Un_Frined_service:data_dict withSuccess:^(NSDictionary *response) {
            [activeIndicatore stopAnimating];
            if ([[response valueForKey:STATUS]isEqualToString:SUCCESS]) {
                 appDelegate.un_frind_in_Profile=@"YES";
                int duration = 0; // duration in seconds
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }
            else{
                 [SHARED_HELPER showAlert:@"Unable to make unfriend. please try again."];
            }
            
        } onfailure:^(NSError *theError) {
            [activeIndicatore stopAnimating];
            [SHARED_HELPER showAlert:ServiceFail];
            
        }];
    }
    
}
-(void)edit_Action{
    
    if ([service_sucsess isEqualToString:@"YES"]) {
        
    EditProfileViewController *edit_VC=[self.storyboard instantiateViewControllerWithIdentifier:@"EditProfileViewController"];
    NSDictionary *user_dict=@{@"gender_Str":gender_Str,@"profile_image_url":profile_image_url,@"user_Mail":self.gmail_Label.text,@"User_Name_Str":self.User_name_label.text,@"user_adress":user_adress_Str,@"user_number_str":user_number_str,@"state_str":state_str,@"dob_Str":dob_Str,@"city_str":city_str,@"zipcode_Str":zipcode_Str};
    
    edit_VC.user_Data=user_dict;
    
    edit_VC.user_image_url=profile_image_url;
    edit_VC.gender_Str=gender_Str;
    
    [self.navigationController pushViewController:edit_VC animated:YES];
    }
}

-(void)back_Action{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)onClick_rideGoingBtn:(id)sender {
    
    self.viewProfile_scrollLabel.hidden=YES;
    self.profile_rideGoingBtn.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }

}
@end
