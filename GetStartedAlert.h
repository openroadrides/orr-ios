//
//  GetStartedAlert.h
//  openroadrides
//
//  Created by apple on 26/04/18.
//  Copyright © 2018 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GetStartedAlert : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
- (IBAction)onClick_closeBtn:(id)sender;

@end
