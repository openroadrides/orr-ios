//
//  NewsViewController.h
//  openroadrides
//
//  Created by apple on 29/05/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ProfileView.h"
#import "MenuTableViewCell.h"
#import "DashboardViewController.h"
#import "EventsViewController.h"
#import "GroupsViewController.h"
#import "FriendsViewController.h"
#import "RidesController.h"
#import "RoutesVC.h"



@interface NewsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>{
    
    
    NSString *news_id_str;
    int friends_count,groups_count;
    
    UISwipeGestureRecognizer *swipe_right;
    UISwipeGestureRecognizer *swipe_left;
}
@property (strong, nonatomic) IBOutlet UILabel *no_Data_Label;
@property (weak, nonatomic) IBOutlet UITableView *News_tableview;
@property (strong, nonatomic) IBOutlet UIView *side_Menu_view;
@property (strong, nonatomic) IBOutlet UIImageView *User_image;
@property (strong, nonatomic) IBOutlet UILabel *User_Name;
@property (strong, nonatomic) IBOutlet UITableView *side_Menu_Table;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *menu_Leading_Constraint;
- (IBAction)Profile_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *news_rideGoingBtn;
- (IBAction)onClick_news_rideGoingBtn:(id)sender;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *news_scrollLabel;

@end
