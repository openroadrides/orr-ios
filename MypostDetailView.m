//
//  MypostDetailView.m
//  openroadrides
//
//  Created by apple on 04/10/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "MypostDetailView.h"

@interface MypostDetailView ()

@end

@implementation MypostDetailView

- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate.edit_posted=@"NO";
    
    
    // Do any additional setup after loading the view.
    self.title = @"POST";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    
    
    self.myPostDetail_scrollLabel.hidden=YES;
    self.myPostDetail_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.myPostDetail_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.myPostDetail_scrollLabel.textColor = [UIColor blackColor];
    self.myPostDetail_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.myPostDetail_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.myPostDetail_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.myPostDetail_scrollLabel.scrollSpeed = 60; // pixels per second
    self.myPostDetail_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.myPostDetail_scrollLabel.fadeLength = 0.f;
    self.myPostDetail_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.myPostDetail_scrollLabel observeApplicationNotifications];
    
    self.myPostDetail_rideGoingBtn.hidden=YES;
    self.bottomConstraint_closePostBtn.constant=-30;
    if (appDelegate.ridedashboard_home) {
        self.myPostDetail_scrollLabel.hidden=NO;
        self.bottomConstraint_closePostBtn.constant=0;
        self.myPostDetail_rideGoingBtn.hidden=NO;
    }
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [leftBtn addTarget:self action:@selector(back_Action) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.frame = CGRectMake(0, 0, 25, 25);
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"back_Image"] forState:UIControlStateNormal];
    UIBarButtonItem *leftBackButton=[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:leftBackButton, nil];
    self.closePostButton.hidden=YES;
    self.postContentView.hidden=YES;
    self.interestedUsersTableview.dataSource=self;
    self.interestedUsersTableview.delegate=self;
    [self.interestedUsersTableview setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self myPostDetailMethod];
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([appDelegate.edit_posted isEqualToString:@"YES"]) {
        [self myPostDetailMethod];
        appDelegate.edit_posted=@"NO";
        appDelegate.my_post_Changed=@"YES";
       
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)back_Action{
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)myPostDetailMethod
{
//    getMyPost_Details_Request
    if (![SHARED_HELPER checkIfisInternetAvailable]) {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    activeIndicatore.hidesWhenStopped = TRUE;
    [self.view addSubview:activeIndicatore];
    [activeIndicatore startAnimating];
    
    
    
    //    [SHARED_API myPosts_Request_List:[Defaults valueForKey:User_ID] withSuccess:^(NSDictionary *response) {
    [SHARED_API getMyPost_Details_Request:_myPostIDString withSuccess:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           NSLog(@"Mypost Detail  Response %@",response);
                           if([[response objectForKey:STATUS] isEqualToString:SUCCESS])
                           {
                               //Edit Button Display
                               UIButton *edit_btn = [UIButton buttonWithType:UIButtonTypeCustom];
                               [edit_btn addTarget:self action:@selector(Edit_Clicked) forControlEvents:UIControlEventTouchUpInside];
                               edit_btn.frame = CGRectMake(0, 0, 30, 30);
                               [edit_btn setBackgroundImage:[UIImage imageNamed:@"setup-ride"] forState:UIControlStateNormal];
                               UIBarButtonItem * edit_Ride= [[UIBarButtonItem alloc] initWithCustomView:edit_btn];
                               self.navigationItem.rightBarButtonItems=[NSArray arrayWithObjects:edit_Ride, nil];
                               mypostDetailDict=[response valueForKey:@"post_details"];
                               
                               
                               
                               NSString*PostedByStr=[NSString stringWithFormat:@"%@",[mypostDetailDict valueForKey:@"created_at"]];
                               if ( [PostedByStr isEqualToString:@""] || [PostedByStr isEqualToString:@"null"] || [PostedByStr isEqualToString:@"<null>"] || PostedByStr == nil) {
                                   
                                   PostedByStr=@"";
//                                   self.postedDateLabel.text=PostedByStr;
                               }
                               else{
                                   
                                   NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                                   [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                                    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
                                   NSDate *date = [dateFormatter dateFromString: PostedByStr];
                                   [dateFormatter setDateFormat:@"MM/dd/yyyy"];
                                   [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
                                   NSString *curent_date=[dateFormatter stringFromDate:[NSDate date]];
                                   PostedByStr = [dateFormatter stringFromDate:date];
                                   if ([curent_date compare:PostedByStr]==NSOrderedSame) {
//                                       [dateFormatter setDateFormat:@"HH:mm"];
                                       [dateFormatter setDateFormat:@"hh:mm a"];
                                       PostedByStr = [dateFormatter stringFromDate:date];
                                   }
                                   self.postedDateLabel.text=[NSString stringWithFormat:@"Posted %@",PostedByStr];
  
                                   
                               }
                               
                              
                               
                               
                               NSString*PriceCostStr=[NSString stringWithFormat:@"%@",[mypostDetailDict valueForKey:@"post_price"]];
                               if ([PriceCostStr isEqualToString:@"<null>"] || [PriceCostStr isEqualToString:@""] || [PriceCostStr isEqualToString:@"null"] || PriceCostStr == nil || [PriceCostStr isEqualToString:@"0.00"] || [PriceCostStr isEqualToString:@"0"]) {
                                   
                                   PriceCostStr=@"";
                                   self.postPriceLabel.text=PriceCostStr;
                               }
                               else{
                                   self.postPriceLabel.text=[NSString stringWithFormat:@" $ %@",PriceCostStr];
                               }
                               
                               
                               
                               NSString*titleNameStr=[NSString stringWithFormat:@"%@",[mypostDetailDict valueForKey:@"post_title"]];
                               if ([titleNameStr isEqualToString:@"<null>"] || [titleNameStr isEqualToString:@""] || [titleNameStr isEqualToString:@"null"] || titleNameStr == nil) {
                                   
                                   titleNameStr=@"";
                                   _postTitleLabel.text=titleNameStr;
                               }
                               else{
                                   _postTitleLabel.text=titleNameStr;
                               }
                               
                               NSString*postCatTypeStr=[NSString stringWithFormat:@"%@",[mypostDetailDict valueForKey:@"category_name"]];
                               if ([postCatTypeStr isEqualToString:@"<null>"] || [postCatTypeStr isEqualToString:@"category_name"] || [postCatTypeStr isEqualToString:@"null"] || postCatTypeStr == nil) {
                                   
                                   postCatTypeStr=@"";
                                   _postCategoryTypeLabel.text=postCatTypeStr;
                               }
                               else{
                                   _postCategoryTypeLabel.text=postCatTypeStr;
                               }

                               
                               
                               NSString * areaName=[NSString stringWithFormat:@"%@",[mypostDetailDict valueForKey:@"post_address"]];
                               NSString * cityName=[NSString stringWithFormat:@"%@",[mypostDetailDict valueForKey:@"city"]];
                               NSString * stateName=[NSString stringWithFormat:@"%@",[mypostDetailDict valueForKey:@"state"]];
                               NSString *zipCode=[NSString stringWithFormat:@"%@",[mypostDetailDict valueForKey:@"zipcode"]];
                               
                               NSMutableArray *empaty_array_search_public=[[NSMutableArray alloc]init];
                               [empaty_array_search_public addObject:@"<null>"];
                               [empaty_array_search_public addObject:@"null"];
                               [empaty_array_search_public addObject:@""];
                               [empaty_array_search_public addObject:@"(null)"];
                               [empaty_array_search_public addObject:@"0"];
                               
                               NSMutableArray *Data_array_search_public=[[NSMutableArray alloc]init];
                               
                               
                               if ([empaty_array_search_public containsObject:areaName]) {
                                   
                               }
                               else{
                                   
                                   [Data_array_search_public addObject:areaName];
                               }
                               if ([empaty_array_search_public containsObject:cityName]) {
                                   
                               }
                               else{
                                   [Data_array_search_public addObject:cityName];
                               }
                               
                               if ([empaty_array_search_public containsObject:stateName]) {
                                   
                               }
                               else{
                                   [Data_array_search_public addObject:stateName];
                               }
                               
                               if ([empaty_array_search_public containsObject:zipCode]) {
                                   
                               }
                               else{
                                   [Data_array_search_public addObject:zipCode];
                               }
                               NSMutableString *add_data_search_public=[[NSMutableString alloc]init];
                               if (Data_array_search_public.count>0) {
                                   self.postAddressImageView.image=[UIImage imageNamed:@"check_in_placeholder"];
                                   
                                   for (int i=0; i<Data_array_search_public.count; i++) {
                                       
                                       NSString *adress=[Data_array_search_public objectAtIndex:i];
                                       if (i==0) {
                                           [add_data_search_public appendFormat:@"%@", [NSString stringWithFormat:@"%@",adress]];
                                       }
                                       else
                                       {
                                           int zip=[[Data_array_search_public objectAtIndex:i] intValue];
                                           if (zip>0) {
                                               [add_data_search_public appendFormat:@"%@", [NSString stringWithFormat:@" %@",adress]];
                                           }
                                           else
                                               [add_data_search_public appendFormat:@"%@", [NSString stringWithFormat:@", %@",adress]];
                                       }
                                       
                                   }
                                   self.postAddressLabel.text=add_data_search_public;
                               }
                               else{
                                   self.postAddressImageView.image=[UIImage imageNamed:@""];
                                   self.postAddressLabel.text=@"";
                                   
                                   
                               }
                               NSString*contactNumStr=[NSString stringWithFormat:@"%@",[mypostDetailDict valueForKey:@"phone_number"]];
                               if ([contactNumStr isEqualToString:@"<null>"] || [contactNumStr isEqualToString:@""] || [contactNumStr isEqualToString:@"null"] || contactNumStr == nil) {
                                   
                                   contactNumStr=@"";
                                   self.postPhoneNumberImageView.hidden=YES;
                                   _postPhoneNumberLabel.text=contactNumStr;
                               }
                               else{
                                   self.postPhoneNumberImageView.hidden=NO;
                                   _postPhoneNumberLabel.text=contactNumStr;
                               }
                               NSString*contactEmailStr=[NSString stringWithFormat:@"%@",[mypostDetailDict valueForKey:@"email"]];
                               if ([contactEmailStr isEqualToString:@"<null>"] || [contactEmailStr isEqualToString:@""] || [contactEmailStr isEqualToString:@"null"] || contactEmailStr == nil) {
                                   
                                   contactEmailStr=@"";
                                   self.postEmailImageView.hidden=YES;
                                   _postEmailLabel.text=contactEmailStr;
                               }
                               else{
                                   _postEmailLabel.text=contactEmailStr;
                                   self.postEmailImageView.hidden=NO;
                               }
                               
                               NSString*descStr=[NSString stringWithFormat:@"%@",[mypostDetailDict valueForKey:@"description"]];
                               if ([descStr isEqualToString:@"<null>"] || [descStr isEqualToString:@""] || [descStr isEqualToString:@"null"] || descStr == nil) {
                                   
                                   descStr=@"";
                                   _postDescriptionLabel.text=descStr;
                                   
                                   self.interestedUserTopView.hidden=YES;
                               }
                               else{
                                   _postDescriptionLabel.text=descStr;
                                   self.interestedUserTopView.hidden=NO;
                               }
                               
                               
                               NSString*intrestedStatusStr=[NSString stringWithFormat:@"%@",[mypostDetailDict valueForKey:@"post_status"]];
                               if ([intrestedStatusStr isEqualToString:@"<null>"] || [intrestedStatusStr isEqualToString:@""] || [intrestedStatusStr isEqualToString:@"null"] || intrestedStatusStr == nil) {
                                   
                                   intrestedStatusStr=@"";
                                   
                               }
                               else{
                                   
                                   if ([intrestedStatusStr isEqualToString:@"Running"]) {
                                       self.closePostButton.hidden=NO;
                                   }
                                   else if (![intrestedStatusStr isEqualToString:@"Running"])
                                   {
                                       self.closePostButton.hidden=YES;
                                   }
                                   else if ([intrestedStatusStr isEqualToString:@"Closed"])
                                   {
                                       self.closePostButton.hidden=YES;
                                   }
                                   
                               }
                               myPostImagesArray=[[NSMutableArray alloc] init];
                               
                               _multipleImages_scrollView.pagingEnabled = YES;
                               myPostImagesArray=[mypostDetailDict valueForKey:@"images"];
                               
                               for (UIView *subview in self.multipleImages_scrollView.subviews) {
                                   [subview removeFromSuperview];
                               }
                               if ((myPostImagesArray.count!=0))
                               {
                                   for (int i=0; i<myPostImagesArray.count; i++)
                                   {
                                       UIImageView *imagviews=[[UIImageView alloc] initWithFrame:CGRectMake(i*self.multipleImages_scrollView.frame.size.width, 0, self.multipleImages_scrollView.frame.size.width, self.multipleImages_scrollView.frame.size.height)];
                                       
                                       [imagviews setImageWithURL:[NSURL URLWithString:[[myPostImagesArray objectAtIndex:i] valueForKey:@"post_image_names"] ] placeholderImage:[UIImage imageNamed:@"add_places_placeholder"]];
                                       imagviews.contentMode = UIViewContentModeScaleAspectFit;
                                       [self.multipleImages_scrollView addSubview:imagviews];
                                       
                                       UIView  * transparentview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, imagviews.frame.size.width, imagviews.frame.size.height)];
                                       transparentview.backgroundColor = [UIColor blackColor];
                                       transparentview.alpha = 0.1;
                                       [imagviews addSubview:transparentview];
                                       
                                   }
                                   self.multipleImages_scrollView.contentSize=CGSizeMake(myPostImagesArray.count*self.multipleImages_scrollView.frame.size.width, self.multipleImages_scrollView.frame.size.height);
                                   self.multipleImages_scrollView.showsHorizontalScrollIndicator = NO;
                                   self.multipleImages_scrollView.delegate=self;
                                   [pageControl removeFromSuperview];
                                   if ([myPostImagesArray count] > 1)
                                   {
                                       
                                       pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(10, self.multipleImages_scrollView.frame.size.height-12, self.multipleImages_scrollView.frame.size.width, 10)];
                                       
                                       pageControl.currentPageIndicatorTintColor =APP_YELLOW_COLOR;
                                       pageControl.pageIndicatorTintColor =[UIColor whiteColor] ;
                                       pageControl.numberOfPages = myPostImagesArray.count;
                                       [_postContentView addSubview:pageControl];
                                   }
                               }
                               else
                               {
                                   UIImageView *imagviews=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.multipleImages_scrollView.frame.size.width, self.multipleImages_scrollView.frame.size.height)];
                                   
                                   [imagviews setImageWithURL:[NSURL URLWithString:@""] placeholderImage:[UIImage imageNamed:@"add_places_placeholder"]];
                                   imagviews.contentMode = UIViewContentModeScaleAspectFit;
                                   [self.multipleImages_scrollView addSubview:imagviews];
                                   UIView  * transparentview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, imagviews.frame.size.width, imagviews.frame.size.height)];
                                   transparentview.backgroundColor = [UIColor blackColor];
                                   transparentview.alpha = 0.1;
                                   [imagviews addSubview:transparentview];
                                   
                               }
                               interestedUserArray=[mypostDetailDict valueForKey:@"marketplace_interested_users"];
                               
                               self.interestedUsersTableview.dataSource=self;
                               self.interestedUsersTableview.delegate=self;
                               if ([interestedUserArray count]>0) {
                                   
                                   self.interestedUsersLabel.hidden=NO;
//                                   self.interestedUserTopView.hidden=NO;
                                   self.interestedUsersTableview.hidden=NO;
                                   
                                   NSString*descStr=[NSString stringWithFormat:@"%@",[mypostDetailDict valueForKey:@"description"]];
                                   if ([descStr isEqualToString:@"<null>"] || [descStr isEqualToString:@""] || [descStr isEqualToString:@"null"] || descStr == nil) {
                                       
                                       descStr=@"";
                                        self.interestedUserTopView.hidden=YES;
                                   }
                                   else{
                                       
                                       self.interestedUserTopView.hidden=NO;
                                   }

                               }
                               else
                               {
                                   self.interestedUsersTableview.hidden=YES;
                                   self.interestedUsersLabel.hidden=YES;
                                   self.interestedUserTopView.hidden=YES;
                               }
                               _interestedUserTableView_topConstraint.constant=[interestedUserArray count]*110;
                               [self.interestedUsersTableview reloadData];
                               
                               [activeIndicatore stopAnimating];
                               self.postContentView.hidden=NO;
                               
                           }
                           else if([[response objectForKey:STATUS] isEqualToString:FAIL])
                               
                           {
                               
                               self.postContentView.hidden=YES;

                               [activeIndicatore stopAnimating];
                               [SHARED_HELPER showAlert:Forgotfail];
                           }
                           
});
    } onfailure:^(NSError *theError) {
        self.postContentView.hidden=YES;
        [activeIndicatore stopAnimating];
        
        [SHARED_HELPER showAlert:ServiceFail];

        
    }];
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)sender
{
    if (sender==_multipleImages_scrollView)
    {
        CGFloat pageWidth = _multipleImages_scrollView.frame.size.width;
        NSInteger offsetLooping = 1;
        int ppage = floor((_multipleImages_scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + offsetLooping;
        pageControl.currentPage = ppage %myPostImagesArray.count;
        //        pageControl.currentPage = ppage %4;
        
    }
}
#pragma mark TableViewDelegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [interestedUserArray count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        return 110;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    InterestedUserCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil)
    {
        cell = [[InterestedUserCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }

        if (indexPath.row%2==0) {
            cell.bgViewForUserCell.backgroundColor=ROUTES_CELL_BG_COLOUR1;
        }
        else
            cell.bgViewForUserCell.backgroundColor=ROUTES_CELL_BG_COLOUR2;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSString*userNameStr=[NSString stringWithFormat:@"%@",[[interestedUserArray objectAtIndex:indexPath.row ]valueForKey:@"name"]];
    if ([userNameStr isEqualToString:@"<null>"] || [userNameStr isEqualToString:@""] || [userNameStr isEqualToString:@"null"] || userNameStr == nil) {
        
        userNameStr=@"";
        cell.user_Name_label.text=userNameStr;
    }
    else{
        cell.user_Name_label.text=userNameStr;
    }
    
    NSString*usermailStr=[NSString stringWithFormat:@"%@",[[interestedUserArray objectAtIndex:indexPath.row ]valueForKey:@"email"]];
    if ([usermailStr isEqualToString:@"<null>"] || [usermailStr isEqualToString:@""] || [usermailStr isEqualToString:@"null"] || usermailStr == nil) {
        
        usermailStr=@"";
        cell.user_email_label.text=usermailStr;
        cell.user_email_imageView.hidden=YES;
    }
    else{
        cell.user_email_label.text=usermailStr;
        cell.user_email_imageView.hidden=NO;
    }
    NSString*usermobileStr=[NSString stringWithFormat:@"%@",[[interestedUserArray objectAtIndex:indexPath.row ]valueForKey:@"phone_number"]];
    if ([usermobileStr isEqualToString:@"<null>"] || [usermobileStr isEqualToString:@""] || [usermobileStr isEqualToString:@"null"] || usermobileStr == nil) {
        
        usermobileStr=@"";
        cell.user_phoneNumber_label.text=usermobileStr;
        cell.user_phonenumber_imageView.hidden=YES;
    }
    else{
        cell.user_phoneNumber_label.text=usermobileStr;
        cell.user_phonenumber_imageView.hidden=NO;
    }
//    NSString*userrequestedStr=[NSString stringWithFormat:@"Requested at %@",[[interestedUserArray objectAtIndex:indexPath.row ]valueForKey:@"requested_at"]];
//    if ([userrequestedStr isEqualToString:@"<null>"] || [userrequestedStr isEqualToString:@""] || [userrequestedStr isEqualToString:@"null"] || userrequestedStr == nil) {
//        
//        userrequestedStr=@"";
//        cell.user_posted_label.text=userrequestedStr;
//        
//    }
//    else{
//        cell.user_posted_label.text=userrequestedStr;
//        
//    }
    
    
    
    
    NSString *convertedString1;
    NSString *userrequestedStr = [NSString stringWithFormat:@"%@",[[interestedUserArray objectAtIndex:indexPath.row ]valueForKey:@"requested_at"]];
 /// here this is your date with format yyyy-MM-dd
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"]; //// here set format of date which is in your output date (means above str with format)
     [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSDate *date1 = [dateFormatter dateFromString: userrequestedStr];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
     [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *curent_date=[dateFormatter stringFromDate:[NSDate date]];
    convertedString1 = [dateFormatter stringFromDate:date1]; //here convert date in
    
    if ([curent_date compare:convertedString1]==NSOrderedSame) {
        [dateFormatter setDateFormat:@"HH:mm"];
        convertedString1 = [dateFormatter stringFromDate:date1];
    }
    //            NSLog(@"Converted String : %@",convertedString);
    
    if ([convertedString1 isEqualToString:@"<null>"] || [convertedString1 isEqualToString:@""] || [convertedString1 isEqualToString:@"null"] || convertedString1 == nil) {
        
        cell.user_posted_label.text=@"";
        
    }
    else{
        cell.user_posted_label.text=[NSString stringWithFormat:@"Requested at %@",convertedString1];
        cell.user_posted_label.numberOfLines=3;
    }
    

    
        //image
        NSString *profile_image_string1 = [NSString stringWithFormat:@"%@", [[interestedUserArray objectAtIndex:indexPath.row] objectForKey:@"profile_image"]];
        NSURL*url=[NSURL URLWithString:profile_image_string1];
        [cell.userImageView sd_setImageWithURL:url
                                 placeholderImage:[UIImage imageNamed:@"user_Placeholder"]
                                          options:SDWebImageRefreshCached];
        
        
    
        return cell;
   
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (IBAction)onClick_closePost_btn:(id)sender {
    UIAlertController *alertview = [UIAlertController alertControllerWithTitle:nil message:@"Are you sure you want close this post?" preferredStyle:UIAlertControllerStyleAlert];
    
    [alertview addAction:[UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self closePostMethod];
       
        
    }]];
    [alertview addAction:[UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
       
        
    }]];
    
    
    [self presentViewController:alertview animated:YES completion:nil];

}
-(void)closePost_Alert{
    
}
-(void)Edit_Clicked{
    
    if (![SHARED_HELPER checkIfisInternetAvailable]) {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
    
    if (myPostImagesArray.count>1) {
        [self.multipleImages_scrollView setContentOffset:CGPointMake(0,0)];
        pageControl.currentPage=0;
    }
    if (myPostImagesArray.count>0) {
        
    }
    else
    {
        myPostImagesArray =[[NSMutableArray alloc]init];
    }
    
    
    
    EditPostView *cntlr = [self.storyboard instantiateViewControllerWithIdentifier:@"EditPostView"];
    cntlr.post_details=@{@"post_id":[mypostDetailDict valueForKey:@"post_id"],@"post_latitute":[mypostDetailDict valueForKey:@"post_latitute"],@"post_longitude":[mypostDetailDict valueForKey:@"post_longitude"],@"post_title":_postTitleLabel.text,@"post_price":[mypostDetailDict valueForKey:@"post_price"],@"post_address":_postAddressLabel.text,@"phone_number":_postPhoneNumberLabel.text,@"categories_id":[mypostDetailDict valueForKey:@"categories_id"],@"email":_postEmailLabel.text,@"description":_postDescriptionLabel.text,@"status":[mypostDetailDict valueForKey:@"post_status"],@"post_image_names":myPostImagesArray};
    [self.navigationController pushViewController:cntlr animated:YES];
}
-(void)closePostMethod
{
    
    if (![SHARED_HELPER checkIfisInternetAvailable]) {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    activeIndicatore.hidesWhenStopped = TRUE;
    [self.view addSubview:activeIndicatore];
    [activeIndicatore startAnimating];
   
    [SHARED_API close_Post_Request:_myPostIDString user_ID:[Defaults valueForKey:User_ID] withSuccess:^(NSDictionary *response) {
//     [SHARED_API close_Post_Request:_myPostIDString user_ID:@"18" withSuccess:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           NSLog(@"Close post  Response %@",response);
                           if([[response objectForKey:STATUS] isEqualToString:SUCCESS])
                           {
                             
                               self.closePostButton.hidden=YES;
                               [self.navigationController popViewControllerAnimated:YES];
                               
                               [activeIndicatore stopAnimating];
                               [[NSNotificationCenter defaultCenter] postNotificationName:@"ClosedPostReload" object:self];
                           }
                           else if([[response objectForKey:STATUS] isEqualToString:FAIL])
                               
                           {
                               
                               
                               [activeIndicatore stopAnimating];
                               [SHARED_HELPER showAlert:Forgotfail];
                           }
                           
                       });
        
        
        
    } onfailure:^(NSError *theError) {
        [activeIndicatore stopAnimating];
        [SHARED_HELPER showAlert:ServiceFail];
    }];

}

- (IBAction)onClick_myPostDeatil_rideGoingBtn:(id)sender {
    
    self.myPostDetail_scrollLabel.hidden=YES;
    self.myPostDetail_rideGoingBtn.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }

}
@end
