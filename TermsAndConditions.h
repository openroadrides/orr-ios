//
//  TermsAndConditions.h
//  openroadrides
//
//  Created by apple on 27/07/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "RideDashboard.h"
#import "CBAutoScrollLabel.h"
@interface TermsAndConditions : UIViewController<UIWebViewDelegate>
{
    UIActivityIndicatorView *activeIndicatore;
}
@property (weak, nonatomic) IBOutlet UIWebView *termsAndConditions_webView;
@property (weak, nonatomic) IBOutlet UIButton *terms_rideGoingBtn;
- (IBAction)onClick_terms_rideGoingBtn:(id)sender;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *terms_scrollLabel;

@end
