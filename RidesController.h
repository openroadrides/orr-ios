//
//  RidesController.h
//  openroadrides
//
//  Created by apple on 06/07/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "APIHelper.h"
#import "AppHelperClass.h"
#import "AppDelegate.h"
#import "ScheduledRidesCell.h"
#import "FullFilledRidesCell.h"
#import "RideDetailViewController.h"
#import "MenuTableViewCell.h"
#import "NewsViewController.h"
#import "EventsViewController.h"
#import "GroupsViewController.h"
#import "HomeDashboardView.h"
#import "CBAutoScrollLabel.h"
@interface RidesController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>
{
      UIActivityIndicatorView  *activeIndicatore;
      UIView *indicaterview;
     
        UILabel *NO_DATA;
        NSString *view_Status_secheduledAndFullFilled,*select_Scheduled_Called,*select_fullFilled_Called,*select_scheduled_rideID,*select_fullfilled_rideID,*menu_Opend;
        NSMutableArray *arrayOfScheduledRides,*arratOfFullfilledRides;
    int friends_count,groups_count;
    
    UISwipeGestureRecognizer *swipe_right;
    UISwipeGestureRecognizer *swipe_left;
    BOOL is24h;
    
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *search_Bar_Top;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *segment_Top;
@property(strong,nonatomic)NSString *is_For_Only;


@property (weak, nonatomic) IBOutlet UIView *rides_segement_view;
@property (weak, nonatomic) IBOutlet UIView *scheduled_segment_view;
@property (weak, nonatomic) IBOutlet UIView *fullfilled_segment_view;
@property (weak, nonatomic) IBOutlet UILabel *scheduled_segment_label;
@property (weak, nonatomic) IBOutlet UIButton *scheduled_segment_btn;
- (IBAction)onClick_scheduled_segment_btn:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *fullFilled_segement_label;
@property (weak, nonatomic) IBOutlet UIButton *fullfilled_segment_btn;
- (IBAction)onClick_fullFilled_segment_btn:(id)sender;
@property (weak, nonatomic) IBOutlet UISearchBar *rides_searchBar;

@property (weak, nonatomic) IBOutlet UIScrollView *rides_scrollView;
@property (weak, nonatomic) IBOutlet UIView *rides_contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthConstraint_rides_contentView;
@property (weak, nonatomic) IBOutlet UIView *scheduled_contentView;
@property (weak, nonatomic) IBOutlet UIView *fullfilledContentView;
@property (weak, nonatomic) IBOutlet UITableView *scheduled_tableView;
@property (weak, nonatomic) IBOutlet UITableView *fullFilled_tableView;
@property UIRefreshControl *scheduled_refreshControl,*fullfilled_refreshControl;
@property (weak, nonatomic) IBOutlet UILabel *noData_label_scheduled;
@property (weak, nonatomic) IBOutlet UILabel *noData_label_fullFilled;
@property (strong, nonatomic) IBOutlet UIView *side_Menu_view;
@property (strong, nonatomic) IBOutlet UIImageView *User_image;
@property (strong, nonatomic) IBOutlet UILabel *User_Name;
@property (strong, nonatomic) IBOutlet UITableView *side_Menu_Table;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *menu_Leading_Constraint;
- (IBAction)Profile_Action:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *rides_Bg_View;
@property (weak, nonatomic) IBOutlet UIButton *rides_rideGoingBtn;
- (IBAction)onClick_rides_rideGoingBtn:(id)sender;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *rides_scrolllLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint_ridesScrollView;

@end
