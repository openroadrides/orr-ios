//
//  GetStartedViewController.m
//  REbeacon
//
//  Created by MobileTeam Ebiz on 23/06/17.
//  Copyright © 2017 ebiz solutions. All rights reserved.
//

#import "GetStartedViewController.h"
#import "LoginViewController.h"
#import "HomeDashboardView.h"

@interface GetStartedViewController ()

@end

@implementation GetStartedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor=[UIColor clearColor];
    self.navigationController.navigationBar.hidden = YES;
    
    
   /* UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    GetStartedAlert*alert = [storyboard instantiateViewControllerWithIdentifier:@"GetStartedAlert"];
    BIZPopupViewController *popupView = [[BIZPopupViewController alloc] initWithContentViewController:alert contentSize:CGSizeMake(self.view.frame.size.width-40,200)];
    [self presentViewController:popupView animated:NO completion:nil];
    */
    
    screenRect = [[UIScreen mainScreen] bounds];
    screenWidth = screenRect.size.width;
    screenHeight = screenRect.size.height;
    
    
    
    
    
    
    
    
    
    
    
    
    //Get Started
    UIView *letsGoBaseView = [[UIView alloc]initWithFrame:CGRectMake(0, screenHeight-50, screenWidth, 50)];
    letsGoBaseView.backgroundColor = APP_YELLOW_COLOR;
    [self.view addSubview:letsGoBaseView];
    
    
    UILabel *letsGoLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, letsGoBaseView.frame.size.width, 50)];
    letsGoLabel.text = @"GET STARTED";
    //letsGoLabel.font=[UIFont fontWithName:@"Poppins-Regular" size:17];
    letsGoLabel.font=[UIFont fontWithName:@"Antonio-Bold" size:20];
    letsGoLabel.textColor=[UIColor blackColor];
    letsGoLabel.textAlignment=NSTextAlignmentCenter;
    [letsGoBaseView addSubview:letsGoLabel];
    
    UIButton *letsGoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [letsGoBtn addTarget:self
                  action:@selector(letsGoBtnAction:)
        forControlEvents:UIControlEventTouchUpInside];
    [letsGoBtn setTitle:@"" forState:UIControlStateNormal];
    letsGoBtn.backgroundColor = [UIColor clearColor];
    letsGoBtn.frame = CGRectMake(0, 0, screenWidth, 55);
    [letsGoBaseView addSubview:letsGoBtn];
    
    
    
    
    //Scrolling images
    pageControllerIndex = 0;
    
    _getStarted_scrollView.hidden=YES;
    _getStarted_scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, -20, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height-30)];
    [_getStarted_scrollView setBackgroundColor:[UIColor blackColor]];
    _getStarted_scrollView.showsHorizontalScrollIndicator = false;
    _getStarted_scrollView.delegate = self;
    _getStarted_scrollView.pagingEnabled = YES;
    [self.view addSubview:_getStarted_scrollView];
    
    getStartedImages_Array = [[NSMutableArray alloc]init];
    [getStartedImages_Array addObject:@"screen1"];
    [getStartedImages_Array addObject:@"screen2"];
    [getStartedImages_Array addObject:@"screen3"];
    [getStartedImages_Array addObject:@"screen4"];
    [getStartedImages_Array addObject:@"screen5"];
    [getStartedImages_Array addObject:@"screen6"];
    

    for (int i = 0; i < [getStartedImages_Array count]; i++)
    {
        
        UIImageView *getStartedImageView = [[UIImageView alloc] initWithFrame:CGRectMake(screenWidth*i, 0, screenWidth, screenHeight)];
        
        getStartedImageView.contentMode = UIViewContentModeScaleAspectFit;
        
        getStartedImageView.image = [UIImage imageNamed:[getStartedImages_Array objectAtIndex:i]];
        
        getStartedImageView.clipsToBounds = YES;
        [_getStarted_scrollView addSubview:getStartedImageView];
        
        /*if (i == [getStartedImages_Array count]-1) {
            
            UIView *letsGoBaseView = [[UIView alloc]initWithFrame:CGRectMake((screenWidth*i)+20, getStartedImageView.frame.size.height-85, screenWidth-40, 50)];
            letsGoBaseView.backgroundColor = APP_YELLOW_COLOR;
            [self.view addSubview:letsGoBaseView];
            [_getStarted_scrollView addSubview:letsGoBaseView];
            
            UILabel *letsGoLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, letsGoBaseView.frame.size.width, 50)];
            letsGoLabel.text = @"RIDE NOW";
            //letsGoLabel.font=[UIFont fontWithName:@"Poppins-Regular" size:17];
            letsGoLabel.font=[UIFont fontWithName:@"Antonio-Bold" size:20];
            letsGoLabel.textColor=[UIColor blackColor];
            letsGoLabel.textAlignment=NSTextAlignmentCenter;
            [letsGoBaseView addSubview:letsGoLabel];
            
            UIButton *letsGoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [letsGoBtn addTarget:self
                       action:@selector(letsGoBtnAction:)
             forControlEvents:UIControlEventTouchUpInside];
            [letsGoBtn setTitle:@"" forState:UIControlStateNormal];
            letsGoBtn.backgroundColor = [UIColor clearColor];
            letsGoBtn.frame = CGRectMake(0, 0, screenWidth, 55);
            [letsGoBaseView addSubview:letsGoBtn];
        }*/
    }
  
    _getStarted_scrollView.contentSize = CGSizeMake(screenWidth*[getStartedImages_Array count],100);
    CGPoint scrollPoint = CGPointMake(0, 0);
    [_getStarted_scrollView setContentOffset:scrollPoint animated:YES];
    [self.view layoutIfNeeded];
    

    
    //UIPageViewController
    
    _pageControl.currentPageIndicatorTintColor =[UIColor whiteColor];
    _pageControl.tintColor = [UIColor clearColor];
    _pageControl.numberOfPages = [getStartedImages_Array count];
    [self.view addSubview:_pageControl];
    [self.view bringSubviewToFront:_pageControl];
    _pageControl.transform = CGAffineTransformMakeScale(1.4, 1.4);
    
    
    
    //Started Screen
    
    welcome_View = [[UIView alloc] initWithFrame:CGRectMake(0, -20, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height+20)];
    welcome_View.backgroundColor = [UIColor clearColor];
    [self.view addSubview:welcome_View];
    
    
    UIImageView *StartedImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    
    StartedImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    StartedImageView.image = [UIImage imageNamed:@"welcome_screen"];
    
    StartedImageView.clipsToBounds = YES;
    [welcome_View addSubview:StartedImageView];
    
    
    
    
    UIButton *StartGoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [StartGoBtn addTarget:self
                   action:@selector(StartAction:)
         forControlEvents:UIControlEventTouchUpInside];
    [StartGoBtn setTitle:@"" forState:UIControlStateNormal];
    [StartGoBtn.titleLabel setFont:[UIFont fontWithName:@"Antonio-Bold" size:20]];
    StartGoBtn.titleLabel.textColor=[UIColor blackColor];
    StartGoBtn.backgroundColor = APP_YELLOW_COLOR;
    StartGoBtn.frame = CGRectMake(0, welcome_View.frame.size.height-50, screenWidth, 50);
    [welcome_View addSubview:StartGoBtn];
    
    UILabel *STARTLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, welcome_View.frame.size.height-50, screenWidth, 50)];
    STARTLabel.text = @"START";
    //letsGoLabel.font=[UIFont fontWithName:@"Poppins-Regular" size:17];
    STARTLabel.font=[UIFont fontWithName:@"Antonio-Bold" size:20];
    STARTLabel.textColor=[UIColor blackColor];
    STARTLabel.textAlignment=NSTextAlignmentCenter;
    [welcome_View addSubview:STARTLabel];
    
    
    
    
    [self.view layoutIfNeeded];
}


-(void)viewDidAppear:(BOOL)animated
{
    for (int i = 0; i < _pageControl.numberOfPages; i++)
    {
        UIView* dot = [_pageControl.subviews objectAtIndex:i];
        if (i == 0)
        {
            dot.backgroundColor = [UIColor whiteColor];
            dot.layer.cornerRadius = dot.frame.size.height / 2;
            dot.layer.borderColor = [UIColor darkGrayColor].CGColor; dot.layer.borderWidth = 1;
        }
        else
        {
            dot.backgroundColor = [UIColor clearColor];
            dot.layer.cornerRadius = dot.frame.size.height / 2;
            dot.layer.borderColor = [UIColor darkGrayColor].CGColor; dot.layer.borderWidth = 1;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ScrollView Delegate Methods


- (void)scrollViewDidEndDecelerating:(UIScrollView *)sender
{
    
    CGFloat pageWidth = _getStarted_scrollView.frame.size.width;
    NSInteger offsetLooping = 1;
    int page = floor((_getStarted_scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + offsetLooping;
    
    pageControllerIndex = page;
    
    if ([getStartedImages_Array count]  != 0) {
        _pageControl.currentPage = page % [getStartedImages_Array count];
    }
    
    if (_pageControl.currentPage == 0)
    {
        
    }
    else if (_pageControl.currentPage == [getStartedImages_Array count]-1)
    {
        //_pageControl.hidden = YES;
    }
    else
    {
        //_pageControl.hidden = NO;
    }

}

#pragma mark -button action
- (IBAction)letsGoBtnAction:(id)sender
{
    NSString *getStarted = @"YES";
    [[NSUserDefaults standardUserDefaults] setObject:getStarted forKey:@"getStartedName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    LoginViewController *loginView = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self.navigationController pushViewController:loginView animated:YES];
}
- (IBAction)StartAction:(id)sender
{
    welcome_View.hidden=YES;
    _getStarted_scrollView.hidden=NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
