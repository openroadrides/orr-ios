//
//  EventsViewController.m
//  openroadrides
//
//  Created by apple on 06/06/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "EventsViewController.h"
#import "Constants.h"
#import "APIHelper.h"
#import "AppHelperClass.h"
#import "EventsTableViewCell.h"
#import "CreateEventViewController.h"
#import "EventDetailViewController.h"
#import "MPCoachMarks.h"

#define NO_EVENTS @"You Don't have events"
@interface EventsViewController (){
    NSMutableArray *events_ary,*check_strings_array,*charitableEvents_ary,*my_events_array;
    NSArray *menu_Items_Array;
    NSString *menu_Status;
}

@end

@implementation EventsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    menu_Status=@"LEFT";
    self.title = @"EVENTS";
    
     appDelegate.create_event_or_edit_event=@"NO";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    
    self.events_scrollLabel.hidden=YES;
    self.events_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.events_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.events_scrollLabel.textColor = [UIColor blackColor];
    self.events_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.events_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.events_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.events_scrollLabel.scrollSpeed = 60; // pixels per second
    self.events_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.events_scrollLabel.fadeLength = 0.f;
    self.events_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.events_scrollLabel observeApplicationNotifications];
    
    
    self.events_rideGoingBtn.hidden=YES;
    if (appDelegate.ridedashboard_home) {
        self.events_scrollLabel.hidden=NO;
        self.events_rideGoingBtn.hidden=NO;
        self.bottomConstraint_eventsScrollView.constant=35;
    }
    
    //    UIBarButtonItem *back_btn=[[UIBarButtonItem alloc]initWithImage:
    //                                 [[UIImage imageNamed:@"sidemenuicon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
    //                                                                style:UIBarButtonItemStylePlain target:self action:@selector(backclicked)];
//    UIBarButtonItem *add_btn=[[UIBarButtonItem alloc]initWithTitle:@"+" style:UIBarButtonItemStylePlain target:self action:@selector(add_clicked)];
//    [add_btn setTitleTextAttributes:@{
//                                      NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:26.0],
//                                      NSForegroundColorAttributeName: [UIColor blackColor]
//                                      } forState:UIControlStateNormal];
//    
//
//    
//    
//    
//    self.navigationItem.rightBarButtonItem=add_btn;
    
    
    UIButton *add_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [add_btn addTarget:self action:@selector(addclicked) forControlEvents:UIControlEventTouchUpInside];
    add_btn.frame = CGRectMake(0, 0, 30, 30);
    [add_btn setBackgroundImage:[UIImage imageNamed:@"Add_plus"] forState:UIControlStateNormal];
    UIBarButtonItem * create_event= [[UIBarButtonItem alloc] initWithCustomView:add_btn];
    self.navigationItem.rightBarButtonItems=[NSArray arrayWithObjects:create_event, nil];

    
    UIBarButtonItem *menuButton=[[UIBarButtonItem alloc]initWithImage:
                                 [[UIImage imageNamed:@"sidemenuicon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
                                                                style:UIBarButtonItemStylePlain target:self action:@selector(Menu_Action:)];
    
    self.navigationItem.leftBarButtonItem = menuButton;
    
    //Side Menu
    swipe_right=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(menuright)];
    swipe_right.direction=UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipe_right];
    
    
    swipe_left=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(menuleft)];
    swipe_left.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipe_left];
    
    self.User_image.layer.cornerRadius=self.User_image.frame.size.width/2;
    self.User_image.clipsToBounds=YES;
    self.User_image.layer.borderColor = [UIColor whiteColor].CGColor;
    self.User_image.layer.borderWidth = 4.0;
//    menu_Items_Array=@[@"Dashboard",@"Rides",@"Routes",@"Events",@"News",@"Friends",@"Groups",@"Find Riders",@"Privacy Policy",@"Terms & Conditions",@"Logout"];
   
    menu_Items_Array=@[@"Home",@"Dashboard",@"Privacy Policy",@"Terms & Conditions",@"Logout"];
    
    [_side_Menu_Table reloadData];
    self.User_Name.text=[NSString stringWithFormat:@"%@",[Defaults valueForKey:@"UserName"]];
    //
    
    CGFloat content_View_Width=SCREEN_WIDTH;
    self.events_mainView_width_constraint.constant=content_View_Width*3;
    [self.view layoutIfNeeded];
    
    
    self.charitableEvents_tableView.dataSource=self;
    self.charitableEvents_tableView.delegate=self;
    
    [self.my_events_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.events_table_vw setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.charitableEvents_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.noEVENTSLabel.hidden=YES;
    self.noCharitableEventsLabel.hidden=YES;
    self.my_events_no_data_label.hidden=YES;
    
    view_Status=@"";
    
    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    activeIndicatore.hidesWhenStopped = TRUE;
    [self.view addSubview:activeIndicatore];
    
    
    events_ary=[[NSMutableArray alloc]init];
//    _main_content_vw.hidden = YES;
    _no_Data_Label.hidden=YES;
    
//    self.refreshControl = [[UIRefreshControl alloc]init];
//    self.refreshControl.tintColor=APP_YELLOW_COLOR;
//    
//    self.refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
//    [self.refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
//    [self.events_table_vw addSubview:self.refreshControl];
//    
//    self.refreshControl2 = [[UIRefreshControl alloc]init];
//    self.refreshControl2.tintColor=APP_YELLOW_COLOR;
//    
//    self.refreshControl2.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
//    [self.refreshControl2 addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
//    
//    [self.charitableEvents_tableView addSubview:self.refreshControl2];
//    
//    self.refreshControl3 = [[UIRefreshControl alloc]init];
//    self.refreshControl3.tintColor=APP_YELLOW_COLOR;
//    self.refreshControl3.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
//    [self.refreshControl3 addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
//    [self.my_events_table addSubview:self.refreshControl3];
    
    [self getevents_list];

    check_strings_array=[[NSMutableArray alloc]init];
    [check_strings_array addObject:@"<null>"];
    [check_strings_array addObject:@"null"];
    [check_strings_array addObject:@""];
    [check_strings_array addObject:@"(null)"];
    
    //Display Annotation
    // Show coach marks
    BOOL coachMarksShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"MPCoachMarksShown_Events"];
    if (coachMarksShown == NO) {
        // Don't show again
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MPCoachMarksShown_Events"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self showAnnotation];
    }
    
    // Do any additional setup after loading the view.
}

#pragma mark - Annotations
-(void)showAnnotation
{
    [self.view layoutIfNeeded];
    
    // Setup coach marks
    CGRect coachmark1 = CGRectMake (5,20, self.navigationController.navigationBar.frame.size.height,self.navigationController.navigationBar.frame.size.height);
    CGRect coachmark2 = CGRectMake( ([UIScreen mainScreen].bounds.size.width - 53), 20, self.navigationController.navigationBar.frame.size.height, self.navigationController.navigationBar.frame.size.height);
    
    
    // Setup coach marks
    NSArray *coachMarks = @[
                            @{
                                @"rect": [NSValue valueWithCGRect:coachmark1],
                                @"caption": @"Menu",
                                @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                                @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                                @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_LEFT]
                                //@"showArrow":[NSNumber numberWithBool:YES]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:coachmark2],
                                @"caption": @"Add new event",
                                @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                                @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                                @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_RIGHT],
                                //@"showArrow":[NSNumber numberWithBool:YES]
                                },
                            ];
    
    
    MPCoachMarks *coachMarksView = [[MPCoachMarks alloc] initWithFrame:self.navigationController.view.bounds coachMarks:coachMarks];
    //[self.navigationController.view addSubview:coachMarksView];
    [[UIApplication sharedApplication].keyWindow addSubview:coachMarksView];
    [coachMarksView start];
}

- (void)refreshTable {
    [self.refreshControl endRefreshing];
    //TODO: refresh your data
    if([view_Status isEqualToString:@""]||[view_Status isEqualToString:@"EVENTS"])
    {
        [self.refreshControl endRefreshing];
        [self getevents_list];
    }
    else if([view_Status isEqualToString:@"CHARITABLEEVENTS"] )
    {
        [self.refreshControl2 endRefreshing];
        [self getevents_list];

    }
    else{
        [self.refreshControl3 endRefreshing];
        [self getevents_list];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    _side_Menu_view.hidden=YES;
    self.navigationController.navigationBar.hidden=NO;
     [[self navigationController] setNavigationBarHidden:NO animated:NO];
    [self display_user_Profile];
    
    if ([appDelegate.delete_event isEqualToString:@"YES"]) {
        appDelegate.delete_event=@"NO";
        [self getevents_list];
    }
    if ([appDelegate.create_event_or_edit_event isEqualToString:@"YES"]) {
        
        if([view_Status isEqualToString:@""]||[view_Status isEqualToString:@"EVENTS"])
        {
            
        }
        else if([view_Status isEqualToString:@"CHARITABLEEVENTS"] )
        {
            
            
        }
        else{
            appDelegate.create_event_or_edit_event=@"NO";
            [self getevents_list];
        }
    }
    
    
}

-(void)display_user_Profile{
    
    NSString *user_id=[Defaults valueForKey:User_ID];
    [SHARED_API DisplayUserProfile:user_id withSuccess:^(NSDictionary *response) {
        
        [self user_Profile_Data_Sucsess:response];
        
    } onfailure:^(NSError *theError) {
        
    }];
    
}
-(void)user_Profile_Data_Sucsess:(NSDictionary *)Profile_data{
    if ([[Profile_data valueForKey:STATUS] isEqualToString:SUCCESS]) {
        NSDictionary *user_data=[Profile_data valueForKey:@"data"];
        self.User_Name.text=[user_data valueForKey:@"name"];
        [Defaults setObject:self.User_Name.text forKey:@"UserName"];
        
        NSString *profile_image_str=[NSString stringWithFormat:@"%@",[user_data valueForKey:@"profile_image"]];
        
        if (![check_strings_array containsObject:profile_image_str] && ![profile_image_str isEqualToString:@"<null>"])
        {
            NSURL*url=[NSURL URLWithString:profile_image_str];
            [self.User_image sd_setImageWithURL:url
                               placeholderImage:[UIImage imageNamed:@"user_Placeholder"]
                                        options:SDWebImageRefreshCached];
        }
        
        friends_count = [[NSString stringWithFormat:@"%@",[[Profile_data valueForKey:@"data"] valueForKey:@"total_friends"]] intValue];
        
        
        groups_count = [[NSString stringWithFormat:@"%@",[[Profile_data valueForKey:@"data"] valueForKey:@"total_groups"]] intValue];
        
        [_side_Menu_Table reloadData];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Tableview methods......

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==_side_Menu_Table) {
        return menu_Items_Array.count;
    }
    if (tableView ==self.events_table_vw)
    {
        
        if (events_ary.count>0)
        {
            return events_ary.count;
        }
        
        else{
            return 0;
        }
        
    }
    //    else if ([view_Status isEqualToString:@"MY"])
    else if (tableView ==self.charitableEvents_tableView)
    {
        if (charitableEvents_ary.count>0)
        {
            return charitableEvents_ary.count;
        }
        else{
            return 0;
        }
        
    }
    else if (tableView==_my_events_table){
        return my_events_array.count;
    }
    else{
        return 0;
    }

//    return [events_ary count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView==_side_Menu_Table)
    {
        static NSString *simpleTableIdentifier = @"MenuTableViewCell";
        MenuTableViewCell *cell = (MenuTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        self.side_Menu_Table.separatorStyle=UITableViewCellSeparatorStyleNone;
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MenuTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.menu_Item_Label.text=[menu_Items_Array objectAtIndex:indexPath.row];
        cell.menu_Item_Label.textColor=[UIColor colorWithRed:255/255.0 green:222/255.0 blue:0/255.0 alpha:1.0];
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = [UIColor colorWithRed:255/255.0 green:222/255.0 blue:0/255.0 alpha:1.0];
        [cell setSelectedBackgroundView:bgColorView];
        cell.menu_Item_Label.highlightedTextColor=[UIColor blackColor];
        tableView.allowsMultipleSelection=NO;
        
        
        cell.menu_item_Count_Lab.hidden=YES;
        if (indexPath.row==0)
        {
            cell.menu_Item_iCon.image=[UIImage imageNamed:@"homeicon"];
            
        }
        else if (indexPath.row==1)
        {
            cell.menu_Item_iCon.image=[UIImage imageNamed:@"Dashboard_blue"];
        }
        else if (indexPath.row==2)
            
        {
            cell.menu_Item_iCon.image=[UIImage imageNamed:@"privacy"];
        }
        else if (indexPath.row==3)
            
        {
            cell.menu_Item_iCon.image=[UIImage imageNamed:@"terms"];
        }
        else if (indexPath.row==4)
            
        {
            cell.menu_Item_iCon.image=[UIImage imageNamed:@"Logout"];
        }
        
        return cell;
    }
//    static NSString *simpleTableIdentifier = @"EventsTableViewCell";
//    EventsTableViewCell *cell = (EventsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
//    self.charitableEvents_tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
//    if (cell == nil)
//    {
//        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EventsTableViewCell" owner:self options:nil];
//        cell = [nib objectAtIndex:0];
//    }
    
    
//    EventsTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"EventsTableViewCell"];
//    
//    if (cell == nil)
//    {
//        cell = [[EventsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
//    }
//
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    if (tableView==_events_table_vw) {
//        static NSString *simpleTableIdentifier = @"EventsTableViewCell";
//        EventsTableViewCell *cell = (EventsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
//        self.events_table_vw.separatorStyle=UITableViewCellSeparatorStyleNone;
//        if (cell == nil)
//        {
//            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EventsTableViewCell" owner:self options:nil];
//            cell = [nib objectAtIndex:0];
//        }
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        EventsTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"EventsTableViewCell"];
        
        if (cell == nil)
        {
            cell = [[EventsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.title_lbl.text=[[events_ary objectAtIndex:indexPath.row] objectForKey:@"event_name"];
        
        NSString*event_starting_loc_add_str=[NSString stringWithFormat:@"%@",[[events_ary objectAtIndex:indexPath.row] objectForKey:@"event_starting_location_address"]];
        if ([event_starting_loc_add_str isEqualToString:@"<null>"] || [event_starting_loc_add_str isEqualToString:@""] || [event_starting_loc_add_str isEqualToString:@"null"] || event_starting_loc_add_str == nil) {
            
            cell.start_address_lbl.text=@"";
            cell.ridescount_imageview.hidden=YES;
            cell.start_address_lbl.hidden=YES;
            cell.date_topConstraint.priority=250;
            cell.date_notext_topConstraint.priority=750;
        }
        else{
            cell.ridescount_imageview.hidden=NO;
            cell.start_address_lbl.hidden=NO;
            cell.date_topConstraint.priority=750;
            cell.date_notext_topConstraint.priority=250;
            
            cell.start_address_lbl.text=[NSString stringWithFormat:@"%@",event_starting_loc_add_str];
            
            cell.start_address_lbl.numberOfLines=2;
        }
        NSString*event_ending_loc_add_str=[NSString stringWithFormat:@"%@",[[events_ary objectAtIndex:indexPath.row] objectForKey:@"event_ending_location_address"]];
        if ([event_ending_loc_add_str isEqualToString:@"<null>"] || [event_ending_loc_add_str isEqualToString:@""] || [event_ending_loc_add_str isEqualToString:@"null"] || event_ending_loc_add_str == nil) {
            
            cell.end_address_lbl.text=@"";
        }
        else{
            cell.end_address_lbl.text=[NSString stringWithFormat:@"%@",event_ending_loc_add_str];
        }
//        cell.start_address_lbl.text=[[events_ary objectAtIndex:indexPath.row] objectForKey:@"event_starting_location_address"];
//        cell.end_address_lbl.text=[[events_ary objectAtIndex:indexPath.row] objectForKey:@"event_ending_location_address"];
        
        NSString *convertedString;
        NSString *date_str = [NSString stringWithFormat:@"%@",[[events_ary objectAtIndex:indexPath.row] valueForKey:@"event_start_date"]]; /// here this is your date with format yyyy-MM-dd
        if ([date_str isEqualToString:@"1970-01-01"]||[date_str isEqualToString:@"0000-00-00"] || [date_str isEqualToString:@"<null>"] || [date_str isEqualToString:@""] || [date_str isEqualToString:@"null"] || date_str == nil || [date_str isEqualToString:@"(null)"]) {
            convertedString=@"N/A";
        }
        else{
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
            [dateFormatter setDateFormat:@"yyyy-MM-dd"]; //// here set format of date which is in your output date (means above str with format)
            
            NSDate *date = [dateFormatter dateFromString: date_str]; // here you can fetch date from string with define format
            
            dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"MMMM dd,yyyy"];// here set format which you want...
            convertedString = [dateFormatter stringFromDate:date]; //here convert date in NSString
            NSLog(@"Converted String : %@",convertedString);
        }
        
        
        NSString *convertedString1;
        NSString *date_str1 = [NSString stringWithFormat:@"%@",[[events_ary objectAtIndex:indexPath.row] valueForKey:@"event_end_date"]]; /// here this is your date with format yyyy-MM-dd
        if ([date_str1 isEqualToString:@"1970-01-01"]||[date_str1 isEqualToString:@"0000-00-00"] || [date_str1 isEqualToString:@"<null>"] || [date_str1 isEqualToString:@""] || [date_str1 isEqualToString:@"null"] || date_str1 == nil || [date_str1 isEqualToString:@"(null)"]) {
            convertedString1=@"N/A";
        }
        else{
            NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
            [dateFormatter1 setDateFormat:@"yyyy-MM-dd"]; //// here set format of date which is in your output date (means above str with format)
            
            NSDate *date1 = [dateFormatter1 dateFromString: date_str1]; // here you can fetch date from string with define format
            
            dateFormatter1 = [[NSDateFormatter alloc] init];
            [dateFormatter1 setDateFormat:@"MMMM dd,yyyy"];// here set format which you want...
            convertedString1 = [dateFormatter1 stringFromDate:date1]; //here convert date in NSString
            NSLog(@"Converted String : %@",convertedString1);
        }
        
        
        
        cell.content_view.backgroundColor = [UIColor blackColor];
        
        if ([convertedString isEqualToString:@"<null>"] || [convertedString isEqualToString:@""] || [convertedString isEqualToString:@"null"] || convertedString == nil || [convertedString isEqualToString:@"(null)"]) {
            cell.date_lbl.text=@"";
            cell.eventDateImageView.hidden=YES;
            
        }
        else{
            cell.date_lbl.text=[NSString stringWithFormat:@" %@ - %@",convertedString,convertedString1];
            cell.eventDateImageView.hidden=NO;
        }
        
        //   [self photoForCell:cell user:[[events_ary objectAtIndex:indexPath.row] objectForKey:@"event_image"] withIndexPath:indexPath];
        cell.content_view.backgroundColor = [UIColor blackColor];
        NSString*event_image=[NSString stringWithFormat:@"%@",[[events_ary objectAtIndex:indexPath.row] objectForKey:@"event_image"]];
        NSURL*url=[NSURL URLWithString:event_image];
        [cell.image_view sd_setImageWithURL:url
                           placeholderImage:[UIImage imageNamed:@"eventBg_Placeholder"]
                                    options:SDWebImageRefreshCached];
        return cell;

    }
    else if(tableView==_my_events_table)
    {
        //        static NSString *simpleTableIdentifier = @"EventsTableViewCell";
        //        EventsTableViewCell *cell = (EventsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        //        self.events_table_vw.separatorStyle=UITableViewCellSeparatorStyleNone;
        //        if (cell == nil)
        //        {
        //            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EventsTableViewCell" owner:self options:nil];
        //            cell = [nib objectAtIndex:0];
        //        }
        //        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        EventsTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"EventsTableViewCell"];
       
        
        if (cell == nil)
        {
            cell = [[EventsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.title_lbl.text=[[my_events_array objectAtIndex:indexPath.row] objectForKey:@"event_name"];
        
        NSString*event_starting_loc_add_str=[NSString stringWithFormat:@"%@",[[my_events_array objectAtIndex:indexPath.row] objectForKey:@"event_starting_location_address"]];
        if ([event_starting_loc_add_str isEqualToString:@"<null>"] || [event_starting_loc_add_str isEqualToString:@""] || [event_starting_loc_add_str isEqualToString:@"null"] || event_starting_loc_add_str == nil) {
            
            cell.start_address_lbl.text=@"";
            cell.ridescount_imageview.hidden=YES;
            cell.start_address_lbl.hidden=YES;
            cell.date_topConstraint.priority=250;
            cell.date_notext_topConstraint.priority=750;
        }
        else{
            cell.ridescount_imageview.hidden=NO;
            cell.start_address_lbl.hidden=NO;
            cell.date_topConstraint.priority=750;
            cell.date_notext_topConstraint.priority=250;
            
            cell.start_address_lbl.text=[NSString stringWithFormat:@"%@",event_starting_loc_add_str];
             cell.start_address_lbl.numberOfLines=2;
        }
        NSString*event_ending_loc_add_str=[NSString stringWithFormat:@"%@",[[my_events_array objectAtIndex:indexPath.row] objectForKey:@"event_ending_location_address"]];
        if ([event_ending_loc_add_str isEqualToString:@"<null>"] || [event_ending_loc_add_str isEqualToString:@""] || [event_ending_loc_add_str isEqualToString:@"null"] || event_ending_loc_add_str == nil) {
            
            cell.end_address_lbl.text=@"";
        }
        else{
            cell.end_address_lbl.text=[NSString stringWithFormat:@"%@",event_ending_loc_add_str];
        }
        //        cell.start_address_lbl.text=[[events_ary objectAtIndex:indexPath.row] objectForKey:@"event_starting_location_address"];
        //        cell.end_address_lbl.text=[[events_ary objectAtIndex:indexPath.row] objectForKey:@"event_ending_location_address"];
        
        NSString *convertedString;
        NSString *date_str = [NSString stringWithFormat:@"%@",[[my_events_array objectAtIndex:indexPath.row] valueForKey:@"event_start_date"]]; /// here this is your date with format yyyy-MM-dd
        if ([date_str isEqualToString:@"1970-01-01"]||[date_str isEqualToString:@"0000-00-00"]) {
            convertedString=@"N/A";
        }
        else{
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
            [dateFormatter setDateFormat:@"yyyy-MM-dd"]; //// here set format of date which is in your output date (means above str with format)
            
            NSDate *date = [dateFormatter dateFromString: date_str]; // here you can fetch date from string with define format
            
            dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"MMMM dd,yyyy"];// here set format which you want...
            convertedString = [dateFormatter stringFromDate:date]; //here convert date in NSString
            NSLog(@"Converted String : %@",convertedString);
        }
        
        
        NSString *convertedString1;
        NSString *date_str1 = [NSString stringWithFormat:@"%@",[[my_events_array objectAtIndex:indexPath.row] valueForKey:@"event_end_date"]]; /// here this is your date with format yyyy-MM-dd
        if ([date_str1 isEqualToString:@"1970-01-01"]||[date_str1 isEqualToString:@"0000-00-00"]) {
            convertedString1=@"N/A";
        }
        else{
            NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
            [dateFormatter1 setDateFormat:@"yyyy-MM-dd"]; //// here set format of date which is in your output date (means above str with format)
            
            NSDate *date1 = [dateFormatter1 dateFromString: date_str1]; // here you can fetch date from string with define format
            
            dateFormatter1 = [[NSDateFormatter alloc] init];
            [dateFormatter1 setDateFormat:@"MMMM dd,yyyy"];// here set format which you want...
            convertedString1 = [dateFormatter1 stringFromDate:date1]; //here convert date in NSString
            NSLog(@"Converted String : %@",convertedString1);
        }
        
        
        
        cell.content_view.backgroundColor = [UIColor blackColor];
        
        if ([convertedString isEqualToString:@"<null>"] || [convertedString isEqualToString:@""] || [convertedString isEqualToString:@"null"] || convertedString == nil) {
            cell.date_lbl.text=@"";
            cell.eventDateImageView.hidden=YES;
            
        }
        else{
            cell.date_lbl.text=[NSString stringWithFormat:@" %@ - %@",convertedString,convertedString1];
            cell.eventDateImageView.hidden=NO;
        }
        
        //   [self photoForCell:cell user:[[events_ary objectAtIndex:indexPath.row] objectForKey:@"event_image"] withIndexPath:indexPath];
        cell.content_view.backgroundColor = [UIColor blackColor];
        NSString*event_image=[NSString stringWithFormat:@"%@",[[my_events_array objectAtIndex:indexPath.row] objectForKey:@"event_image"]];
        NSURL*url=[NSURL URLWithString:event_image];
        [cell.image_view sd_setImageWithURL:url
                           placeholderImage:[UIImage imageNamed:@"eventBg_Placeholder"]
                                    options:SDWebImageRefreshCached];
        return cell;
        
    }
    else
    {
//        static NSString *simpleTableIdentifier = @"EventsTableViewCell";
//        EventsTableViewCell *cell = (EventsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
//        self.charitableEvents_tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
//        if (cell == nil)
//        {
//            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EventsTableViewCell" owner:self options:nil];
//            cell = [nib objectAtIndex:0];
//        }
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        EventsTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"EventsTableViewCell"];
        
        if (cell == nil)
        {
            cell = [[EventsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.title_lbl.text=[[charitableEvents_ary objectAtIndex:indexPath.row] objectForKey:@"event_name"];
        
        NSString*event_starting_loc_add_str=[NSString stringWithFormat:@"%@",[[charitableEvents_ary objectAtIndex:indexPath.row] objectForKey:@"event_starting_location_address"]];
        if ([event_starting_loc_add_str isEqualToString:@"<null>"] || [event_starting_loc_add_str isEqualToString:@""] || [event_starting_loc_add_str isEqualToString:@"null"] || event_starting_loc_add_str == nil) {
          
            cell.ridescount_imageview.hidden=YES;
            cell.start_address_lbl.hidden=YES;
            cell.date_topConstraint.priority=250;
            cell.date_notext_topConstraint.priority=750;
            cell.start_address_lbl.text=@"";
        }
        else{
            cell.ridescount_imageview.hidden=NO;
            cell.start_address_lbl.hidden=NO;
            cell.date_topConstraint.priority=750;
            cell.date_notext_topConstraint.priority=250;

            cell.start_address_lbl.text=[NSString stringWithFormat:@"%@",event_starting_loc_add_str];
             cell.start_address_lbl.numberOfLines=2;
        }
        NSString*event_ending_loc_add_str=[NSString stringWithFormat:@"%@",[[charitableEvents_ary objectAtIndex:indexPath.row] objectForKey:@"event_ending_location_address"]];
        if ([event_ending_loc_add_str isEqualToString:@"<null>"] || [event_ending_loc_add_str isEqualToString:@""] || [event_ending_loc_add_str isEqualToString:@"null"] || event_ending_loc_add_str == nil) {
            
            cell.end_address_lbl.text=@"";
        }
        else{
            cell.end_address_lbl.text=[NSString stringWithFormat:@"%@",event_ending_loc_add_str];
        }

//        cell.start_address_lbl.text=[[charitableEvents_ary objectAtIndex:indexPath.row] objectForKey:@"event_starting_location_address"];
//        cell.end_address_lbl.text=[[charitableEvents_ary objectAtIndex:indexPath.row] objectForKey:@"event_ending_location_address"];
//        
        NSString *convertedString;
        NSString *date_str = [NSString stringWithFormat:@"%@",[[charitableEvents_ary objectAtIndex:indexPath.row] valueForKey:@"event_start_date"]]; /// here this is your date with format yyyy-MM-dd
        if ([date_str isEqualToString:@"1970-01-01"]||[date_str isEqualToString:@"0000-00-00"]) {
            convertedString=@"N/A";
        }
        else{
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
            [dateFormatter setDateFormat:@"yyyy-MM-dd"]; //// here set format of date which is in your output date (means above str with format)
            
            NSDate *date = [dateFormatter dateFromString: date_str]; // here you can fetch date from string with define format
            
            dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"MMMM dd,yyyy"];// here set format which you want...
            convertedString = [dateFormatter stringFromDate:date]; //here convert date in NSString
            NSLog(@"Converted String : %@",convertedString);
        }
        
        
        NSString *convertedString1;
        NSString *date_str1 = [NSString stringWithFormat:@"%@",[[charitableEvents_ary objectAtIndex:indexPath.row] valueForKey:@"event_end_date"]]; /// here this is your date with format yyyy-MM-dd
        if ([date_str1 isEqualToString:@"1970-01-01"]||[date_str1 isEqualToString:@"0000-00-00"]) {
            convertedString1=@"N/A";
        }
        else{
            NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
            [dateFormatter1 setDateFormat:@"yyyy-MM-dd"]; //// here set format of date which is in your output date (means above str with format)
            
            NSDate *date1 = [dateFormatter1 dateFromString: date_str1]; // here you can fetch date from string with define format
            
            dateFormatter1 = [[NSDateFormatter alloc] init];
            [dateFormatter1 setDateFormat:@"MMMM dd,yyyy"];// here set format which you want...
            convertedString1 = [dateFormatter1 stringFromDate:date1]; //here convert date in NSString
            NSLog(@"Converted String : %@",convertedString1);
        }
        
        
        
        cell.content_view.backgroundColor = [UIColor blackColor];
        
        if ([convertedString isEqualToString:@"<null>"] || [convertedString isEqualToString:@""] || [convertedString isEqualToString:@"null"] || convertedString == nil) {
            cell.date_lbl.text=@"";
            cell.eventDateImageView.hidden=YES;
        }
        else{
            cell.date_lbl.text=[NSString stringWithFormat:@" %@ - %@",convertedString,convertedString1];
            cell.eventDateImageView.hidden=NO;
        }
        
        //   [self photoForCell:cell user:[[events_ary objectAtIndex:indexPath.row] objectForKey:@"event_image"] withIndexPath:indexPath];
        cell.content_view.backgroundColor = [UIColor blackColor];
        NSString*event_image=[NSString stringWithFormat:@"%@",[[charitableEvents_ary objectAtIndex:indexPath.row] objectForKey:@"event_image"]];
        NSURL*url=[NSURL URLWithString:event_image];
        [cell.image_view sd_setImageWithURL:url
                           placeholderImage:[UIImage imageNamed:@"eventBg_Placeholder"]
                                    options:SDWebImageRefreshCached];
        return cell;
 
    }
    /*
    static NSString *simpleTableIdentifier = @"EventsTableViewCell";
    EventsTableViewCell *cell = (EventsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    self.events_table_vw.separatorStyle=UITableViewCellSeparatorStyleNone;
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EventsTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.title_lbl.text=[[events_ary objectAtIndex:indexPath.row] objectForKey:@"event_name"];
    cell.start_address_lbl.text=[[events_ary objectAtIndex:indexPath.row] objectForKey:@"event_starting_location_address"];
    cell.end_address_lbl.text=[[events_ary objectAtIndex:indexPath.row] objectForKey:@"event_ending_location_address"];
    
    NSString *convertedString;
    NSString *date_str = [NSString stringWithFormat:@"%@",[[events_ary objectAtIndex:indexPath.row] valueForKey:@"event_start_date"]]; /// here this is your date with format yyyy-MM-dd
    if ([date_str isEqualToString:@"1970-01-01"]||[date_str isEqualToString:@"0000-00-00"]) {
        convertedString=@"N/A";
    }
    else{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
    [dateFormatter setDateFormat:@"yyyy-MM-dd"]; //// here set format of date which is in your output date (means above str with format)
    
    NSDate *date = [dateFormatter dateFromString: date_str]; // here you can fetch date from string with define format
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMMM dd,yyyy"];// here set format which you want...
    convertedString = [dateFormatter stringFromDate:date]; //here convert date in NSString
    NSLog(@"Converted String : %@",convertedString);
    }
    
    
    NSString *convertedString1;
    NSString *date_str1 = [NSString stringWithFormat:@"%@",[[events_ary objectAtIndex:indexPath.row] valueForKey:@"event_end_date"]]; /// here this is your date with format yyyy-MM-dd
    if ([date_str1 isEqualToString:@"1970-01-01"]||[date_str1 isEqualToString:@"0000-00-00"]) {
        convertedString1=@"N/A";
    }
    else{
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
    [dateFormatter1 setDateFormat:@"yyyy-MM-dd"]; //// here set format of date which is in your output date (means above str with format)
    
    NSDate *date1 = [dateFormatter1 dateFromString: date_str1]; // here you can fetch date from string with define format
    
    dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"MMMM dd,yyyy"];// here set format which you want...
    convertedString1 = [dateFormatter1 stringFromDate:date1]; //here convert date in NSString
    NSLog(@"Converted String : %@",convertedString1);
    }
    
    
    
    cell.content_view.backgroundColor = [UIColor blackColor];
    
    if ([convertedString isEqualToString:@"<null>"] || [convertedString isEqualToString:@""] || [convertedString isEqualToString:@"null"] || convertedString == nil) {
        cell.date_lbl.text=@"";
    }
    else{
    cell.date_lbl.text=[NSString stringWithFormat:@" %@ - %@",convertedString,convertedString1];
    }
    
 //   [self photoForCell:cell user:[[events_ary objectAtIndex:indexPath.row] objectForKey:@"event_image"] withIndexPath:indexPath];
    cell.content_view.backgroundColor = [UIColor blackColor];
    NSString*event_image=[NSString stringWithFormat:@"%@",[[events_ary objectAtIndex:indexPath.row] objectForKey:@"event_image"]];
    NSURL*url=[NSURL URLWithString:event_image];
    [cell.image_view sd_setImageWithURL:url
                       placeholderImage:[UIImage imageNamed:@""]
                                options:SDWebImageRefreshCached];
    return cell;
    */
    return nil;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_side_Menu_Table) {
        return 45;
    }
    
    return SCREEN_WIDTH/2;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_side_Menu_Table) {
//        if (indexPath.row==3) {
            [cell setSelected:NO animated:NO];
//        }
//        else{
//            [cell setSelected:NO animated:NO];
//        }
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView==_side_Menu_Table) {
        if (indexPath.row==0)
        {
            HomeDashboardView*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"HomeDashboardView"];
            [self.navigationController pushViewController:controller animated:YES];
        }
        
        if (indexPath.row == 1)
        {
            DashboardViewController*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"DashboardViewController"];
            [self.navigationController pushViewController:controller animated:YES];
        }
        else if (indexPath.row == 2)
        {
            PrivacyPolicy*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"PrivacyPolicy"];
            [self.navigationController pushViewController:controller animated:YES];
        }
        else if (indexPath.row == 3)
        {
            TermsAndConditions *cntrl = [self.storyboard instantiateViewControllerWithIdentifier:@"TermsAndConditions"];
            [self.navigationController pushViewController:cntrl animated:YES];
        }
        else  if (indexPath.row == 4) {
            
            if (appDelegate.ridedashboard_home) {
                [self logoutRideGoingAlert:@"You cannot logout while the ride is going on."];
            }
            
            else
            {
                [self showpopup:Terminate];
            }
            [self.side_Menu_Table reloadData];
        }
        
         [self menuleft];
    }
    else{
        if(tableView == _events_table_vw)
        {
            Event_id_str = [NSString stringWithFormat:@"%@",[[events_ary objectAtIndex:indexPath.row] valueForKey:@"event_id"]];
            EventDetailViewController *cntlr = [self.storyboard instantiateViewControllerWithIdentifier:@"EventDetailViewController"];
            cntlr.Event_id = Event_id_str;
            [self.navigationController pushViewController:cntlr animated:YES];
        }
        else if (tableView==_my_events_table){
            Event_id_str = [NSString stringWithFormat:@"%@",[[my_events_array objectAtIndex:indexPath.row] valueForKey:@"event_id"]];
            EventDetailViewController *cntlr = [self.storyboard instantiateViewControllerWithIdentifier:@"EventDetailViewController"];
            cntlr.Event_id = Event_id_str;
            [self.navigationController pushViewController:cntlr animated:YES];
        }
        else
        {
            Event_id_str = [NSString stringWithFormat:@"%@",[[charitableEvents_ary objectAtIndex:indexPath.row] valueForKey:@"event_id"]];
            EventDetailViewController *cntlr = [self.storyboard instantiateViewControllerWithIdentifier:@"EventDetailViewController"];
            cntlr.Event_id = Event_id_str;
            [self.navigationController pushViewController:cntlr animated:YES];
        }
            
//    Event_id_str = [NSString stringWithFormat:@"%@",[[events_ary objectAtIndex:indexPath.row] valueForKey:@"event_id"]];
//    EventDetailViewController *cntlr = [self.storyboard instantiateViewControllerWithIdentifier:@"EventDetailViewController"];
//    cntlr.Event_id = Event_id_str;
//    [self.navigationController pushViewController:cntlr animated:YES];
    }
}


-(void)getevents_list{
    
    
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
    
      [activeIndicatore startAnimating];
    
    [SHARED_API eventsRequestsWithParams:nil withSuccess:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                            NSLog(@"responce is %@",response);
                           
                           [self events_Service_Sucsess:response];
                           
//                           if ([[response valueForKey:@"status"] isEqualToString:FAIL]) {
//                                 [SHARED_HELPER showAlert:ServiceFail];
//                           }
//                           else
//                           {
//                          NSString *respons_str=[NSString stringWithFormat:@"%@",[response valueForKey:@"events"]];
//                          if (respons_str ==nil|| [respons_str isEqualToString:@""]) {
//                                [SHARED_HELPER showAlert:NotificatiosEmpty];
//                            }
//                          else{
//                              events_ary=[response valueForKey:@"events"];
//                              if (events_ary.count>0) {
//                                  _main_content_vw.hidden = NO;
//                                  [self.events_table_vw reloadData];
//                              }
//                              else{
//                                  self.events_table_vw.separatorStyle=UITableViewCellSeparatorStyleNone;
//                                  _no_Data_Label.hidden=NO;
//                              }
//                          }
//                        }
                        [activeIndicatore stopAnimating];
                });
        
    } onfailure:^(NSError *theError) {
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           [SHARED_HELPER showAlert:ServiceFail];
                           [activeIndicatore stopAnimating];
                           _main_content_vw.hidden = YES;
                         
                       });
    }];
}
-(void)events_Service_Sucsess:(NSDictionary *)respnse{
    if ([[respnse valueForKey:STATUS] isEqualToString:SUCCESS]) {
        if ([view_Status isEqualToString:@""]||[view_Status isEqualToString:@"EVENTS"])
        {
            NSArray*events_list_array=[respnse valueForKey:@"events"];
            events_ary=[[NSMutableArray alloc]init];
            if (events_list_array.count>0) {
                for (int i=0; i<events_list_array.count; i++) {
                    NSDictionary *data_Dict_is=[events_list_array objectAtIndex:i];
                    NSString *event_status1=[NSString stringWithFormat:@"%@",[data_Dict_is valueForKey:@"event_type"] ];
                    NSString *event_user_id=[NSString stringWithFormat:@"%@",[data_Dict_is valueForKey:@"user_id"] ];
                    if ([event_user_id isEqualToString:[NSString stringWithFormat:@"%@",[Defaults valueForKey:User_ID]]])
                    {
                    }
                    else
                    {
                      //  [event_status1 isEqualToString:@"normal"]
                    if ([event_status1 caseInsensitiveCompare:@"normal"]== NSOrderedSame)
                    {
                        [events_ary addObject:data_Dict_is];
                    }
                    }
                   
                    NSLog(@"EVENTS Array=====%lu",(unsigned long)events_ary.count);
                }
                 [self.events_table_vw reloadData];
                self.events_scrollView.hidden=NO;
               
                view_Status=@"EVENTS";
                Events_Called=@"YES";
                if (events_ary.count>0) {
                    self.noEVENTSLabel.hidden=YES;
                }
                else
                    self.noEVENTSLabel.hidden=NO;


            }
            else
            {
                [self service_fail_OR_EmptyData];
            }
        }
        else if([view_Status isEqualToString:@"CHARITABLEEVENTS"] )
        {
            NSArray*events_list_array=[respnse valueForKey:@"events"];
             charitableEvents_ary=[[NSMutableArray alloc]init];
            if (events_list_array.count>0) {
                for (int i=0; i<events_list_array.count; i++) {
                    NSDictionary *data_Dict_is=[events_list_array objectAtIndex:i];
                    NSString *event_status=[NSString stringWithFormat:@"%@",[data_Dict_is valueForKey:@"event_type"] ];
                     NSString *event_user_id=[NSString stringWithFormat:@"%@",[data_Dict_is valueForKey:@"user_id"] ];
                    if ([event_user_id isEqualToString:[NSString stringWithFormat:@"%@",[Defaults valueForKey:User_ID]]])
                    {
                    }
                    else
                    {
                    if ([event_status caseInsensitiveCompare:@"charitable"]== NSOrderedSame)
                    {
                        [charitableEvents_ary addObject:data_Dict_is];
                    }
                    }
                    NSLog(@"Charitable Events Array=====%lu",(unsigned long)charitableEvents_ary.count);
                }
                [self.charitableEvents_tableView reloadData];
                self.events_scrollView.hidden=NO;
                
                view_Status=@"CHARITABLEEVENTS";
                Charatable_Called=@"YES";
                if (charitableEvents_ary.count>0) {
                    self.noCharitableEventsLabel.hidden=YES;
                }
                else
                    self.noCharitableEventsLabel.hidden=NO;
                

            }
            else
            {
                [self service_fail_OR_EmptyData];
            }

        }
        else{
            NSArray*events_list_array=[respnse valueForKey:@"events"];
            my_events_array=[[NSMutableArray alloc]init];
            if (events_list_array.count>0) {
                for (int i=0; i<events_list_array.count; i++) {
                    NSDictionary *data_Dict_is=[events_list_array objectAtIndex:i];
                    NSString *event_status=[NSString stringWithFormat:@"%@",[data_Dict_is valueForKey:@"user_id"] ];
                    if ([event_status isEqualToString:[NSString stringWithFormat:@"%@",[Defaults valueForKey:User_ID]]])
                    {
                        [my_events_array addObject:data_Dict_is];
                    }
                }
                [self.my_events_table reloadData];
                self.events_scrollView.hidden=NO;
                
                view_Status=@"MYEVENTS";
                my_events_called=@"YES";
                if (my_events_array.count>0) {
                    self.my_events_no_data_label.hidden=YES;
                }
                else
                    self.my_events_no_data_label.hidden=NO;
                
                
            }
            else
            {
                [self service_fail_OR_EmptyData];
            }
            
        }
        
    }
    else
    {
        [SHARED_HELPER showAlert:ServiceFail];
        [self service_fail_OR_EmptyData];
    }
    
}
-(void)service_fail_OR_EmptyData{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        //        [self.navigationController popViewControllerAnimated:YES];
        if ([view_Status isEqualToString:@""]||[view_Status isEqualToString:@"EVENTS"]) {
            view_Status=@"EVENTS";
            Events_Called=@"YES";
            self.noEVENTSLabel.hidden=NO;
        }
        else if([view_Status isEqualToString:@"CHARITABLEEVENTS"] )
        {
            Charatable_Called=@"YES";
            self.noCharitableEventsLabel.hidden=NO;
        }
        else{
            
            my_events_called=@"YES";
            self.my_events_no_data_label.hidden=NO;
           
        }
    });
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    self.event_segement_view.userInteractionEnabled=YES;
    self.charitable_segement_view.userInteractionEnabled=YES;
     self.my_events_table.userInteractionEnabled=YES;
    if (scrollView.tag==5) {
        if (scrollView.contentOffset.x==0)
        {
            [self onClick_event_segment_btn:0];
        }
        if (scrollView.contentOffset.x==SCREEN_WIDTH)
        {
            [self onClick_charitable_segment_btn:0];
        }
        if (scrollView.contentOffset.x==SCREEN_WIDTH*2)
        {
            [self my_events_action:0];
        }
    }
}

-(void)addclicked
{
    CreateEventViewController *cntlr = [self.storyboard instantiateViewControllerWithIdentifier:@"CreateEventViewController"];
    [self.navigationController pushViewController:cntlr animated:YES];

}


- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES,image);
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
}

- (void)photoForCell:(EventsTableViewCell *)cell user:(NSString *)user withIndexPath:(NSIndexPath *)indexPath
{
    NSURL *url = [NSURL URLWithString:user];
    [self downloadImageWithURL:url completionBlock:^(BOOL succeeded, UIImage *image) {
        if (succeeded) {
            
            if (image==nil) {
                //cell.userImageView.image=[UIImage imageNamed:@"background"];
            }
            else{
                
                cell.image_view.image=image;
                
            }
            
        }
    }];
    
}
-(void)menuleft
{
    
    menu_Status=@"LEFT";
    [UIView animateWithDuration:0.5 delay:0.3 options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.menu_Leading_Constraint.constant=-300;
                         [self.view layoutIfNeeded];
                     } completion:^(BOOL finished) {
                         self.main_content_vw.alpha=1;
                         
                         self.main_content_vw.userInteractionEnabled=YES;
                     }];
}
-(void)menuright
{
   
    menu_Status=@"RIGHT";
    _side_Menu_view.hidden=NO;
    [UIView animateWithDuration:0.5 delay:0.3 options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.menu_Leading_Constraint.constant=0;
                         [self.view layoutIfNeeded];
                     } completion:^(BOOL finished)
     {
         
         self.main_content_vw.alpha=0.8;
         self.main_content_vw.userInteractionEnabled=NO;
     }];
}


- (IBAction)Menu_Action:(id)sender {
    _side_Menu_view.hidden=NO;
    if ([menu_Status isEqualToString:@"LEFT"]) {
        [self menuright];
        
    }
    else{
        [self menuleft];
    }
}
- (IBAction)Profile_Action:(id)sender {
    
    ProfileView*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileView"];
    [self.navigationController pushViewController:controller animated:YES];
    [self menuleft];
}
-(void)logoutRideGoingAlert:(NSString *)alert_msg
{
    UIAlertController *alertview = [UIAlertController alertControllerWithTitle:@"Can't Logout" message:alert_msg preferredStyle:UIAlertControllerStyleAlert];
    
    [alertview addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        
    }]];
    [self presentViewController:alertview animated:YES completion:nil];
}

-(void)showpopup:(NSString *)message
{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"LOGOUT"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                             NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                             NSData *uData = [NSKeyedArchiver archivedDataWithRootObject:@""];
                             [defaults setObject:uData forKey:User_ID];
                             NSString *user_data;
                             user_data = [defaults objectForKey:User_ID];
                             [defaults removeObjectForKey:User_ID];
                             [defaults removeObjectForKey:@"UserName"];
                             [[FBSDKLoginManager new] logOut];
                             [[GIDSignIn sharedInstance] signOut];
                             [defaults synchronize];
                             
                             LoginViewController *login = [self.storyboard  instantiateViewControllerWithIdentifier:@"LoginViewController"];
                             [self.navigationController pushViewController:login animated:YES];
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"CANCEL"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}




- (IBAction)onClick_event_segment_btn:(id)sender {
    view_Status=@"EVENTS";
    self.event_segement_view.backgroundColor=[UIColor whiteColor];
     self.my_events_view.backgroundColor=[UIColor blackColor];
    self.charitable_segement_view.backgroundColor=[UIColor blackColor];
    [self.events_scrollView setContentOffset:CGPointMake((0), _events_scrollView.contentOffset.y)];
    self.event_segment_label.textColor=[UIColor whiteColor];
    self.charitable_segement_label.textColor=APP_YELLOW_COLOR;
     self.my_events_label.textColor=APP_YELLOW_COLOR;
    if ([Events_Called isEqualToString:@"YES"]) {
        
    }
    else{
        
        [self getevents_list];
    }

}
- (IBAction)onClick_charitable_segment_btn:(id)sender {
    
    view_Status=@"CHARITABLEEVENTS";
    self.event_segement_view.backgroundColor=[UIColor blackColor];
     self.my_events_view.backgroundColor=[UIColor blackColor];
    self.charitable_segement_view.backgroundColor=[UIColor whiteColor];
    [self.events_scrollView setContentOffset:CGPointMake((SCREEN_WIDTH), self.events_scrollView.contentOffset.y)];
    self.charitable_segement_label.textColor=[UIColor whiteColor];
    self.event_segment_label.textColor=APP_YELLOW_COLOR;
    self.my_events_label.textColor=APP_YELLOW_COLOR;
    if ([Charatable_Called isEqualToString:@"YES"]) {
        
    }
    else{
        [self getevents_list];
    }

}
- (IBAction)my_events_action:(id)sender {
    
    view_Status=@"MYEVENTS";
    self.my_events_view.backgroundColor=[UIColor whiteColor];
    self.charitable_segement_view.backgroundColor=[UIColor blackColor];
    self.event_segement_view.backgroundColor=[UIColor blackColor];
    [self.events_scrollView setContentOffset:CGPointMake((SCREEN_WIDTH *2), self.events_scrollView.contentOffset.y)];
    self.my_events_label.textColor=[UIColor whiteColor];
    self.event_segment_label.textColor=APP_YELLOW_COLOR;
    self.charitable_segement_label.textColor=APP_YELLOW_COLOR;
    if ([appDelegate.create_event_or_edit_event isEqualToString:@"YES"]) {
        appDelegate.create_event_or_edit_event=@"NO";
        [self getevents_list];
    }
    else if ([my_events_called isEqualToString:@"YES"]) {
        
    }
    else{
        [self getevents_list];
    }
    
}
- (IBAction)onClick_rideGoingBtn:(id)sender {
    
    self.events_scrollLabel.hidden=YES;
    self.events_rideGoingBtn.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }

}
@end
