//
//  EditEventViewController.m
//  openroadrides
//
//  Created by SrkIosEbiz on 05/10/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "EditEventViewController.h"

@interface EditEventViewController ()

@end

@implementation EditEventViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    index = 1;
    imgIndex = 1;
    x=0;

    
    
    service_call_running=NO;
    appDelegate.update_event=@"NO";
    self.event_type_view.hidden=YES;
    self.title = @"EDIT EVENT";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    
    UIButton *saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [saveBtn addTarget:self action:@selector(save_clicked) forControlEvents:UIControlEventTouchUpInside];
    saveBtn.frame = CGRectMake(0, 0, 30, 30);
    [saveBtn setBackgroundImage:[UIImage imageNamed:@"SAVE"] forState:UIControlStateNormal];
    UIBarButtonItem * create_Ride= [[UIBarButtonItem alloc] initWithCustomView:saveBtn];
    self.navigationItem.rightBarButtonItems=[NSArray arrayWithObjects:create_Ride, nil];
    
    
    self.editEvent_scrollLabel.hidden=YES;
    self.editEvent_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.editEvent_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.editEvent_scrollLabel.textColor = [UIColor blackColor];
    self.editEvent_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.editEvent_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.editEvent_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.editEvent_scrollLabel.scrollSpeed = 60; // pixels per second
    self.editEvent_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.editEvent_scrollLabel.fadeLength = 0.f;
    self.editEvent_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.editEvent_scrollLabel observeApplicationNotifications];
    
    self.editEvent_rideGoingBtn.hidden=YES;
    self.bottomConstraint_images_base_view.constant=-32;
    if (appDelegate.ridedashboard_home) {
        self.editEvent_scrollLabel.hidden=NO;
        self.editEvent_rideGoingBtn.hidden=NO;
        self.bottomConstraint_images_base_view.constant=2;
    }
//    UIBarButtonItem *cancle_btn=[[UIBarButtonItem alloc]initWithTitle:@"X" style:UIBarButtonItemStylePlain target:self action:@selector(cancle_clicked)];
//    [cancle_btn setTitleTextAttributes:@{
//                                         NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:26.0],
//                                         NSForegroundColorAttributeName: [UIColor blackColor]
//                                         } forState:UIControlStateNormal];
//    self.navigationItem.leftBarButtonItem = cancle_btn;
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [leftBtn addTarget:self action:@selector(cancle_clicked) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.frame = CGRectMake(0, 0, 25, 25);
    //    [leftBtn setBackgroundImage:[UIImage imageNamed:@"Finalback-arrow.png"] forState:UIControlStateNormal];
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"close_icon"] forState:UIControlStateNormal];
    UIBarButtonItem *leftBackButton=[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    item0=leftBackButton;
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:item0, nil];
    
    NSDateFormatter *ymd_formeter = [[NSDateFormatter alloc] init]; // here we
    [ymd_formeter setDateFormat:@"MMMM dd,yyyy"];
    NSDateFormatter * mdy_formeter = [[NSDateFormatter alloc] init];
    [mdy_formeter setDateFormat:@"MM/dd/YYYY"];
    
    [self.event_name_TF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.event_adress_TF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.event_type_TF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.description_tf setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
   
    [self.start_ride_TF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.start_time_TF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.end_ride_TF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.end_time_TF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.assosiated_ride_TF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.web_link_TF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    self.event_type_TF.text=@"Charitable";
    
    
    if (_event_details.count>0) {
        self.event_name_TF.text=[_event_details valueForKey:@"event_name"];
        self.event_adress_TF.text=[_event_details valueForKey:@"event_adress"];
        event_lat=[_event_details valueForKey:@"event_lat"];
        event_long=[_event_details valueForKey:@"event_long"];
        if ( [self.event_adress_TF.text isEqualToString:@""]) {
            event_lat=@"";
            event_long=@"";
        }
        
        if ([[_event_details valueForKey:@"event_type"] isEqualToString:@"normal"]) {
             self.event_type_TF.text=@"Normal";
        }
        else{
             self.event_type_TF.text=@"Charitable";
        }
        self.description_tf.text=[_event_details valueForKey:@"description"];
       
        if ([[_event_details valueForKey:@"start_date"] isEqualToString:@"N/A"]) {
             self.start_ride_TF.text=@"";
        }
        else{
            NSString *date_str=[_event_details valueForKey:@"start_date"];
            NSDate *date_is = [ymd_formeter dateFromString: date_str];
            date_str = [mdy_formeter stringFromDate:date_is];
            self.start_ride_TF.text=date_str;
        }
        
        if ([[_event_details valueForKey:@"end_date"] isEqualToString:@"N/A"]) {
            self.end_ride_TF.text=@"";
        }
        else{
            NSString *date_str=[_event_details valueForKey:@"end_date"];
            NSDate *date_is = [ymd_formeter dateFromString: date_str];
            date_str = [mdy_formeter stringFromDate:date_is];
            self.end_ride_TF.text=date_str;
        }
        self.start_time_TF.text=[_event_details valueForKey:@"start_time"];
        if ([self.start_time_TF.text isEqualToString:@"N/A"]) {
            self.start_time_TF.text=@"";
        }
       
        self.end_time_TF.text=[_event_details valueForKey:@"end_time"];
        if ([self.end_time_TF.text isEqualToString:@"N/A"]) {
            self.end_time_TF.text=@"";
        }
        self.assosiated_ride_TF.text=[_event_details valueForKey:@"assosiated_ride"];
        self.web_link_TF.text=[_event_details valueForKey:@"web_link"];
        event_id_is=[_event_details valueForKey:@"event_id"];
       
    }
    
    
    
    img_array = [[NSMutableArray alloc]init];
    deleted_ids=[[NSMutableArray alloc]init];
    base_64_image_array= [[NSMutableArray alloc]init];
    [img_array addObjectsFromArray:[_event_details valueForKey:@"event_images"]];
    self.images_scroleview.showsHorizontalScrollIndicator = NO;
    add_btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 15, 90,90)];
    [add_btn addTarget:self action:@selector(add_click:) forControlEvents:UIControlEventTouchUpInside];
    //    add_btn.backgroundColor = [UIColor greenColor];
    [add_btn setBackgroundImage:[UIImage imageNamed:plus_icon] forState:UIControlStateNormal];
    [self.images_scroleview addSubview:add_btn];
    if (img_array.count>0) {
        
        for (int i=0; i<img_array.count; i++) {
            if (index == 1) {
                x = 0;
            }
            baseView_placesImages = [[UIView alloc]initWithFrame:CGRectMake(x, 0, 150, 120)];
            baseView_placesImages.tag = 100+index;
            baseView_placesImages.backgroundColor = [UIColor clearColor];
            [self.images_scroleview addSubview:baseView_placesImages];
            
            img = [[UIImageView alloc] initWithFrame:CGRectMake(0,15,90, 90)];
         [img setImageWithURL:[NSURL URLWithString:[[img_array objectAtIndex:i] valueForKey:@"event_image_name"]] placeholderImage:[UIImage imageNamed:@"add_places_placeholder"]];
            img.contentMode=UIViewContentModeScaleAspectFit;
            
            [baseView_placesImages addSubview:img];
            subtract_btn = [[UIButton alloc]initWithFrame:CGRectMake(img.frame.size.width-12.5, 0, 25, 25)];
            [subtract_btn setBackgroundImage:[UIImage imageNamed:@"wrong_icon.png"] forState:UIControlStateNormal];
            [subtract_btn.titleLabel setFont:[UIFont systemFontOfSize:20]];
            [subtract_btn addTarget:self action:@selector(sub_click:) forControlEvents:UIControlEventTouchUpInside];
            subtract_btn.tag=index;
            [baseView_placesImages addSubview:subtract_btn];
            
            
            add_btn.frame = CGRectMake(baseView_placesImages.frame.origin.x+baseView_placesImages.frame.size.width-40, img.frame.origin.y, 90, 90);
            self.images_scroleview.contentSize = CGSizeMake(add_btn.frame.origin.x+add_btn.frame.size.width, 120);
            imgIndex++;
            x = x+110;
            index++;
 
        }
        if (img_array.count>=5)
        {
        self.images_scroleview.contentSize = CGSizeMake(add_btn.frame.origin.x-5, 120);
        add_btn.hidden = YES;
        }
        
        
    }
    
    
    
    
    
    activeIndicatore = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2-50, SCREEN_HEIGHT/2-100, 100, 100) ];
    activeIndicatore.activityIndicatorViewStyle=UIActivityIndicatorViewStyleWhiteLarge;
    activeIndicatore.color = APP_YELLOW_COLOR;
    activeIndicatore.hidesWhenStopped = TRUE;
    [self.view addSubview:activeIndicatore];

    
}
-(void)cancle_clicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)save_clicked
{
     _event_name_TF.text=[_event_name_TF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
     _description_tf.text=[_description_tf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    self.event_type_view.hidden=YES;
    NSLog(@"base_64_image_array %lu", (unsigned long)base_64_image_array.count);
    
    
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
    
    if (_event_name_TF.text.length == 0) {
        [SHARED_HELPER showAlert:eventnameempty];
        return;
    }
    if (!([_description_tf.text length]>0)) {
        [SHARED_HELPER showAlert:event_des];
        return;
    }
    
//    if((img_array.count>0))
//    {
//        
//    }
//    else{
//        if (base_64_image_array.count>0) {
//            
//        }
//        else{
//            [SHARED_HELPER showAlert:event_img];
//            return;
//        }
//    }
    
    [self Edit_Event_service];
    
    
}
-(void)Edit_Event_service{
    //update_event
    
//    "{
//    ""request_from"":""web / app"",
//    ""event_id"":"""",
//    ""event_name"":"""",
//    ""event_description"":"""",
//    ""associated_ride"":"""",
//    ""link"":"""",
//    ""event_type"":""charitable/normal"",
//    ""event_start_date"":"""",
//    ""event_end_date"":"""",
//    ""event_start_time"":"""",
//    ""event_end_time"":"""",
//    ""event_starting_location_address"":"""",
//    ""event_starting_location_latitude"":"""",
//    ""event_starting_location_longitude"":"""",
//    ""event_ending_location_address"":"""",
//    ""event_ending_location_latitude"":"""",
//    ""event_ending_location_longitude"":"""",
//    ""event_image"":"""",
//    ""previoues_event_image"":"""",
//    ""event_image_names"":[""""],
//    ""remove_event_image_names"":[""""],
//    ""cover_image"":
//    {
//        ""type"":""0/1"",
//        ""image"":"""",
//        ""id"":""""
//    }
//}"
    if (service_call_running) {
        return;
    }
     service_call_running=YES;
    [activeIndicatore startAnimating];
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:event_id_is forKey:@"event_id"];
    [dict setObject:[_event_name_TF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"event_name"];
    [dict setObject:_event_adress_TF.text forKey:@"event_starting_location_address"];
    [dict setObject:@"" forKey:@"event_ending_location_address"];
    [dict setObject:@"" forKey:@"event_ending_location_latitude"];
    [dict setObject:@"" forKey:@"event_ending_location_longitude"];
    [dict setObject:[_assosiated_ride_TF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"associated_ride"];
    [dict setObject:[_web_link_TF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"link"];
    [dict setObject:[_start_ride_TF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"event_start_date"];
    [dict setObject:[_end_ride_TF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"event_end_date"];
    
//    [dict setObject:[_start_time_TF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"event_start_time"];
//    [dict setObject:[_end_time_TF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"event_end_time"];
//    [dict setObject:[eventStartTimeString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"event_start_time"];
//    [dict setObject:[eventEndTimeString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"event_end_time"];
    [dict setObject:[NSString stringWithFormat:@"%@",eventStartTimeString] forKey:@"event_start_time"];
    [dict setObject:[NSString stringWithFormat:@"%@",eventEndTimeString] forKey:@"event_end_time"];
    
    [dict setObject:[_description_tf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"event_description"];
    if ([_event_type_TF.text isEqualToString:@"Charitable"]) {
        [dict setObject:@"charitable" forKey:@"event_type"];
    }
    else{
        [dict setObject:@"normal" forKey:@"event_type"];
    }
    
    if (event_lat==0) {
        [dict setObject:@"" forKey:@"event_starting_location_latitude"];
    }
    else
    {
         [dict setObject:event_lat forKey:@"event_starting_location_latitude"];
    }
    if (event_long==0) {
        [dict setObject:@"" forKey:@"event_starting_location_longitude"];
    }
    else
    {
        [dict setObject:event_long forKey:@"event_starting_location_longitude"];
    }
   
    
    [dict setObject:deleted_ids forKey:@"remove_event_image_names"];
    if (img_array.count>0) {
        NSMutableDictionary *cover_img_dict=[[NSMutableDictionary alloc]init];
        [cover_img_dict setObject:[[img_array objectAtIndex:0]valueForKey:@"event_image_name"] forKey:@"image"];
        [cover_img_dict setObject:[[img_array objectAtIndex:0]valueForKey:@"event_images_id"] forKey:@"id"];
        [cover_img_dict setObject:@"0" forKey:@"type"];
        [dict setObject:cover_img_dict forKey:@"cover_image"];
        
//        [dict setObject:[[img_array objectAtIndex:0]valueForKey:@""] forKey:@""];
    }
    else
    {
        if (base_64_image_array.count>0) {
            
            NSMutableDictionary *cover_img_dict=[[NSMutableDictionary alloc]init];
            base64_del_image=[base_64_image_array objectAtIndex:0];
            [cover_img_dict setObject:[base_64_image_array objectAtIndex:0] forKey:@"image"];
            [cover_img_dict setObject:@"" forKey:@"id"];
            [cover_img_dict setObject:@"1" forKey:@"type"];
            [dict setObject:cover_img_dict forKey:@"cover_image"];
            [base_64_image_array removeObjectAtIndex:0];

        }
        else
        {
             [dict setObject:@"" forKey:@"event_image"];
            NSMutableDictionary *cover_img_dict=[[NSMutableDictionary alloc]init];
//            base64_del_image=[base_64_image_array objectAtIndex:0];
//            [cover_img_dict setObject:[base_64_image_array objectAtIndex:0] forKey:@"image"];
            [cover_img_dict setObject:@"" forKey:@"image"];
            [cover_img_dict setObject:@"" forKey:@"id"];
            [cover_img_dict setObject:@"0" forKey:@"type"];
            [dict setObject:cover_img_dict forKey:@"cover_image"];
           // [base_64_image_array removeObjectAtIndex:0];
        }
        
//        NSMutableDictionary *cover_img_dict=[[NSMutableDictionary alloc]init];
//        base64_del_image=[base_64_image_array objectAtIndex:0];
//        [cover_img_dict setObject:[base_64_image_array objectAtIndex:0] forKey:@"image"];
//        [cover_img_dict setObject:@"" forKey:@"id"];
//        [cover_img_dict setObject:@"1" forKey:@"type"];
//        [dict setObject:cover_img_dict forKey:@"cover_image"];
//        [base_64_image_array removeObjectAtIndex:0];
//        
        
//        if (base_64_image_array.count>0) {
//            NSMutableDictionary *cover_img_dict=[[NSMutableDictionary alloc]init];
//            base64_del_image=[base_64_image_array objectAtIndex:0];
//            [cover_img_dict setObject:[base_64_image_array objectAtIndex:0] forKey:@"image"];
//            [cover_img_dict setObject:@"" forKey:@"id"];
//            [cover_img_dict setObject:@"1" forKey:@"type"];
//            [dict setObject:cover_img_dict forKey:@"cover_image"];
//            [base_64_image_array removeObjectAtIndex:0];
//        }
//        else
//        {
//            [dict setObject:@"" forKey:@"post_image"];
//            NSMutableDictionary *cover_img_dict=[[NSMutableDictionary alloc]init];
//            [cover_img_dict setObject:@"" forKey:@"image"];
//            [cover_img_dict setObject:@"0" forKey:@"id"];
//            [cover_img_dict setObject:@"0" forKey:@"type"];
//            [dict setObject:cover_img_dict forKey:@"cover_image"];
//        }
//
        
        
    }
    if (base_64_image_array.count>0) {
        [dict setObject:base_64_image_array forKey:@"event_image_names"];
    }
    else
        
    {
        NSMutableArray *sample=[[NSMutableArray alloc]init];
         [dict setObject:sample forKey:@"event_image_names"];
    }
    
    
    [dict setObject:@"app" forKey:@"request_from"];
    [dict setObject:@"" forKey:@"previoues_event_image"];
    [dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
    NSLog(@"Edit Event dict is %@",dict);
   
    [SHARED_API Edit_EventRequestsWithParams:dict withSuccess:^(NSDictionary *response) {
        
         NSLog(@"response is %@",response);
        if ([[response valueForKey:STATUS] isEqualToString:SUCCESS])
        {
            [SHARED_HELPER showAlert:edit_event_sucsess];
           
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [activeIndicatore stopAnimating];
                appDelegate.update_event=@"YES";
                [self cancle_clicked];
            });
        }
        else{
            if ([base64_del_image length]>0) {
                [base_64_image_array insertObject:base64_del_image atIndex:0];
            }
            [activeIndicatore stopAnimating];
            [SHARED_HELPER showAlert:ServerConnection];
             service_call_running=NO;
        }
    } onfailure:^(NSError *theError) {
        if ([base64_del_image length]>0) {
            [base_64_image_array insertObject:base64_del_image atIndex:0];
        }
        [activeIndicatore stopAnimating];
        [SHARED_HELPER showAlert:ServerConnection];
         service_call_running=NO;
    }];
    
}
-(void)add_click:(UIButton *)sender
{
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped do nothing.
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}
- (void) imagePickerController:(UIImagePickerController *)picker
         didFinishPickingImage:(UIImage *)image
                   editingInfo:(NSDictionary *)editingInfo
{
    //    img.image = [UIImage imageNamed:[array objectAtIndex:index]];
    UIImage *photoTaken = image;
    if (photoTaken)
    {
        
        if (index <=5)
        {
            if (index == 1) {
                x = 0;
            }
            baseView_placesImages = [[UIView alloc]initWithFrame:CGRectMake(x, 0, 150, 120)];
            baseView_placesImages.tag = 100+index;
            baseView_placesImages.backgroundColor = [UIColor clearColor];
            [self.images_scroleview addSubview:baseView_placesImages];
            
            img = [[UIImageView alloc] initWithFrame:CGRectMake(0,15,90, 90)];
            img.image=photoTaken;
            img.contentMode=UIViewContentModeScaleAspectFit;
            
            //image path from gallery
//            CGFloat compression = 0.9f;
//            CGFloat maxCompression = 0.1f;
//            int maxFileSize = 250*1024;
//            NSData *imageData1 = UIImageJPEGRepresentation(img.image,compression);
//            
//            while ([imageData1 length] > maxFileSize && compression > maxCompression)
//            {
//                compression -= 0.1;
//                imageData1 = UIImageJPEGRepresentation(img.image,compression);
//            }
            NSData *imageData1=[self compressImage:photoTaken];
            base64String = [imageData1 base64EncodedStringWithOptions:0];
            [base_64_image_array addObject:base64String];
            //[img_array addObject:base64String];
            
            imgIndex++;
            
            x = x+110;
            
            [baseView_placesImages addSubview:img];
            subtract_btn = [[UIButton alloc]initWithFrame:CGRectMake(img.frame.size.width-12.5, 0, 25, 25)];
            //            [subtract_btn setTitle:@"X" forState:UIControlStateNormal];
            //            [subtract_btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
            [subtract_btn setBackgroundImage:[UIImage imageNamed:@"wrong_icon.png"] forState:UIControlStateNormal];
            [subtract_btn.titleLabel setFont:[UIFont systemFontOfSize:20]];
            [subtract_btn addTarget:self action:@selector(sub_click:) forControlEvents:UIControlEventTouchUpInside];
            subtract_btn.tag=index;
            [baseView_placesImages addSubview:subtract_btn];
            
            
            add_btn.frame = CGRectMake(baseView_placesImages.frame.origin.x+baseView_placesImages.frame.size.width-40, img.frame.origin.y, 90, 90);
            self.images_scroleview.contentSize = CGSizeMake(add_btn.frame.origin.x+add_btn.frame.size.width, 120);
            
            if (index == 5) {
                self.images_scroleview.contentSize = CGSizeMake(add_btn.frame.origin.x-5, 120);
                add_btn.hidden = YES;
            }
            
            index++;
            
        }
        
        
    }
    else
    {
        NSLog(@"Selected photo is NULL");
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}
-(NSData *)compressImage:(UIImage *)image{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    //    float maxHeight = 1130.0f;
    float maxHeight = 816.0f;
    float maxWidth = 640.0f;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.9;//50 percent compression
    NSData *imageData = UIImageJPEGRepresentation(image, compressionQuality);
    while (actualHeight > maxHeight || actualWidth > maxWidth){
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
        CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
        UIGraphicsBeginImageContext(rect.size);
        [image drawInRect:rect];
        UIImage *img_is = UIGraphicsGetImageFromCurrentImageContext();
        imageData = UIImageJPEGRepresentation(img_is, compressionQuality);
        UIGraphicsEndImageContext();
        
    }
    //        else{
    //        actualHeight = maxHeight;
    //        actualWidth = maxWidth;
    //        compressionQuality = 1;
    //    }
    
    
    return imageData;
}

-(IBAction)sub_click:(UIButton *)sender
{
    
    UIView *remove_baseView_placesImages = (UIView *)[self.images_scroleview viewWithTag:sender.tag+100];
    [remove_baseView_placesImages removeFromSuperview];
    
    int xCoordinate = remove_baseView_placesImages.frame.origin.x;
    
    if (sender.tag < index-1)
    {
        for (int i = (int)sender.tag+1; i<= index-1 ; i++) {
            
            UIView *moveForward_placesImages = (UIView *)[self.images_scroleview viewWithTag:i+100];
            moveForward_placesImages.frame = CGRectMake(xCoordinate, 0, 150, self.images_scroleview.frame.size.height);
            UIButton *buttonTag = (UIButton *)[moveForward_placesImages viewWithTag:(int)moveForward_placesImages.tag-100];
            buttonTag.tag = buttonTag.tag-1;
            moveForward_placesImages.tag = 100+i-1;
            xCoordinate = xCoordinate+110;
            
        }
        
        index --;
        
        x = xCoordinate;
        
        add_btn.hidden = NO;
        add_btn.frame = CGRectMake(xCoordinate, img.frame.origin.y, 90, 90);
        self.images_scroleview.contentSize = CGSizeMake(add_btn.frame.origin.x + add_btn.frame.size.width, 120);
        
    }
    else
    {
        x = x-110;
        index = (int)sender.tag;
        
        add_btn.frame = CGRectMake(remove_baseView_placesImages.frame.origin.x, img.frame.origin.y, 90, 90);
        self.images_scroleview.contentSize = CGSizeMake(add_btn.frame.origin.x + add_btn.frame.size.width, 120);
        
    }
    
    ;
    int inde=(int)sender.tag-1;
    NSString *status=@"NO";
    if (img_array.count>0) {
        int value=(int)sender.tag;
        if (img_array.count>=value) {
            NSString *deleted_id=[[img_array objectAtIndex:inde] valueForKey:@"event_images_id"];
            [deleted_ids addObject:deleted_id];
            [img_array removeObjectAtIndex:(int)sender.tag-1];
             NSLog(@"img_array is %ld",img_array.count);
            status=@"YES";
        }
    }
    
    if (![status isEqualToString:@"YES"]) {
        NSInteger count=img_array.count;
         NSInteger value=(int)sender.tag-1;
         count=value-count;
        [base_64_image_array removeObjectAtIndex:count];
        NSLog(@"sub tag is %ld",(long)sender.tag);
        NSLog(@"base_64_image_array count is %ld",base_64_image_array.count);
    }
    
    
    if (sender.tag == 5) {
        
        add_btn.hidden = NO;
        add_btn.frame = CGRectMake(remove_baseView_placesImages.frame.origin.x, img.frame.origin.y, 90, 90);
        self.images_scroleview.contentSize = CGSizeMake(add_btn.frame.origin.x + add_btn.frame.size.width, 120);
    }
    
}




-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.event_type_view.hidden=YES;
    self.images_base_view.alpha=0.1;
    if (textField==_event_type_TF || textField==_start_ride_TF || textField==_start_time_TF) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:.3];
        [UIView setAnimationBeginsFromCurrentState:TRUE];
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -100., self.view.frame.size.width, self.view.frame.size.height);
        
        [UIView commitAnimations];
        
    }
    if (textField==_description_tf || textField==_end_ride_TF || textField==_end_time_TF) {
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:.3];
        [UIView setAnimationBeginsFromCurrentState:TRUE];
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -180., self.view.frame.size.width, self.view.frame.size.height);
        
        [UIView commitAnimations];
        
    }
    if (textField==_start_ride_TF) {
        _start_ride_TF.text=@"";
        _start_time_TF.text=@"";
        _end_ride_TF.text=@"";
        _end_time_TF.text=@"";
        [self set_start_date];
    }
    else if (textField==_start_time_TF) {
        if (!([_start_ride_TF.text length]>0)) {
            [textField resignFirstResponder];
            [SHARED_HELPER showAlert:EventDate];
            
            return;
        }
        else
        {
            _end_ride_TF.text=@"";
            _end_time_TF.text=@"";
            [self setstarttime];
        }
    }
    else if (textField==_end_ride_TF)
    {
        if (!([_start_ride_TF.text length]>0)) {
            [textField resignFirstResponder];
            [SHARED_HELPER showAlert:EventDate];
           
            return;
        }
        else{
            
            _end_time_TF.text=@"";
            [self set_end_date];
        }
    }
    else if (textField==_end_time_TF)
    {
        if (!([_end_ride_TF.text length]>0)) {
            [textField resignFirstResponder];
             [SHARED_HELPER showAlert:Event_endDate];
            return;
        }
        else{
            
            [self setendtime];
        }
    }
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    self.images_base_view.alpha=1;
    if (textField==_event_type_TF || textField==_start_ride_TF || textField==_start_time_TF) {
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:.3];
        [UIView setAnimationBeginsFromCurrentState:TRUE];
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +100., self.view.frame.size.width, self.view.frame.size.height);
        
        [UIView commitAnimations];
    }
    if (textField==_description_tf || textField==_end_ride_TF || textField==_end_time_TF) {
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:.3];
        [UIView setAnimationBeginsFromCurrentState:TRUE];
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +180., self.view.frame.size.width, self.view.frame.size.height);
        
        [UIView commitAnimations];
        
    }
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    if (textField == _event_name_TF)
    {
        [textField resignFirstResponder];
        return NO;
    }
    else if (textField == _assosiated_ride_TF)
    {
        [_web_link_TF becomeFirstResponder];
        return NO;
    }
    else if (textField == _web_link_TF)
    {
        [textField resignFirstResponder];
        return NO;
    }
    
    
    [textField resignFirstResponder];
    return YES;
}

-(void)set_start_date{
    
    date = [[UIDatePicker alloc]init];
    date.datePickerMode = UIDatePickerModeDate;
    date.date=[NSDate date];
    [date setMinimumDate:[NSDate date]];
    [date setDate:[NSDate date]];
    [_start_ride_TF setInputView:date];
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad1)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(showDate)]];
    [numberToolbar sizeToFit];
    _start_ride_TF.inputAccessoryView = numberToolbar;
    
}

-(void)setstarttime{
    
    start_time = [[UIDatePicker alloc]init];
    start_time.datePickerMode = UIDatePickerModeTime;
    start_time.date=[NSDate date];
    [start_time setDate:[NSDate date]];
    [_start_time_TF setInputView:start_time];
    
    UIToolbar *toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolbar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *done = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(showstartTime)];
    
    UIBarButtonItem *space = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [toolbar setItems:[NSArray arrayWithObjects:space,done,nil]];
    [_start_time_TF setInputAccessoryView:toolbar];
    
}
-(void)showstartTime{
    
    NSDateFormatter *dateformater = [[NSDateFormatter alloc ]init];
//    [dateformater setDateFormat:@"HH:mm"];
    [dateformater setDateFormat:@"hh:mm a"];
    _start_time_TF.text = [NSString stringWithFormat:@"%@",[dateformater stringFromDate:start_time.date]];
    NSDateFormatter *startTimeformater = [[NSDateFormatter alloc ]init];
    [startTimeformater setDateFormat:@"HH:mm"];
    eventStartTimeString=[NSString stringWithFormat:@"%@",[startTimeformater stringFromDate:start_time.date]];
    [_start_time_TF resignFirstResponder];
    
}

-(void)setendtime{
    
    end_Time = [[UIDatePicker alloc]init];
    end_Time.datePickerMode = UIDatePickerModeTime;
    end_Time.date=[NSDate date];
    if ([_start_ride_TF.text compare:_end_ride_TF.text]==NSOrderedSame) {
        
        [end_Time setMinimumDate:start_time.date];
    }
    else
    {
//        [end_Time setMinimumDate:[NSDate date]];
    }
    
    [_end_time_TF setInputView:end_Time];
    
    UIToolbar *toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolbar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *done = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(showendTime)];
    
    UIBarButtonItem *space = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [toolbar setItems:[NSArray arrayWithObjects:space,done,nil]];
    [_end_time_TF setInputAccessoryView:toolbar];
    
}
-(void)showendTime{
    
    NSDateFormatter *dateformater = [[NSDateFormatter alloc ]init];
//    [dateformater setDateFormat:@"HH:mm"];
    [dateformater setDateFormat:@"hh:mm a"];
    _end_time_TF.text = [NSString stringWithFormat:@"%@",[dateformater stringFromDate:end_Time.date]];
    
    NSDateFormatter *endTimeformater = [[NSDateFormatter alloc ]init];
    [endTimeformater setDateFormat:@"HH:mm"];
    eventEndTimeString=[NSString stringWithFormat:@"%@",[endTimeformater stringFromDate:end_Time.date]];
    [_end_time_TF resignFirstResponder];
    
}


-(void)set_end_date{
    
    end_date = [[UIDatePicker alloc]init];
    end_date.datePickerMode = UIDatePickerModeDate;
    end_date.date=[NSDate date];
    [end_date setMinimumDate:date.date];
    [_end_ride_TF setInputView:end_date];
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad1)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(showDate1)]];
    [numberToolbar sizeToFit];
    _end_ride_TF.inputAccessoryView = numberToolbar;
    
}

-(void)showDate{
    
    NSDateFormatter *dateformater = [[NSDateFormatter alloc ]init];
    [dateformater setDateFormat:@"MM/dd/YYYY"];
    _start_ride_TF.text = [NSString stringWithFormat:@"%@",[dateformater stringFromDate:date.date]];
    [_start_ride_TF resignFirstResponder];
    //[_start_time_TF becomeFirstResponder];
}

-(void)showDate1{
    
    NSDateFormatter *dateformater = [[NSDateFormatter alloc ]init];
    [dateformater setDateFormat:@"MM/dd/YYYY"];
    _end_ride_TF.text = [NSString stringWithFormat:@"%@",[dateformater stringFromDate:end_date.date]];
    [_end_ride_TF resignFirstResponder];
   // [_end_time_TF becomeFirstResponder];
}

-(void)cancelNumberPad1{
    
    [self.view endEditing:YES];
    
}

- (IBAction)Event_Adress_Action:(id)sender {
    location_str = @"start_location_btn";
    GMSPlacePickerConfig *config = [[GMSPlacePickerConfig alloc] initWithViewport:nil];
    GMSPlacePickerViewController *placePicker =
    [[GMSPlacePickerViewController alloc] initWithConfig:config];
    placePicker.delegate = self;
    [self presentViewController:placePicker animated:YES completion:nil];
}
- (void)placePicker:(GMSPlacePickerViewController *)viewController didPickPlace:(GMSPlace *)place {
    // Dismiss the place picker, as it cannot dismiss itself.
    [viewController dismissViewControllerAnimated:YES completion:nil];
    
    
    if ([location_str isEqualToString:@"start_location_btn"]) {
        
        
        NSString*loc_str_name=[NSString stringWithFormat:@"%@",place.name];
        if ([loc_str_name isEqualToString:@"(null)"] || [loc_str_name isEqualToString:@"<null>"] || [loc_str_name isEqualToString:@""] || [loc_str_name isEqualToString:@"null"] || loc_str_name == nil) {
            loc_str_name=@"";
            
        }
        NSString*loc_str_address=[NSString stringWithFormat:@"%@",place.formattedAddress];
        if ([loc_str_address isEqualToString:@"(null)"] || [loc_str_address isEqualToString:@"<null>"] || [loc_str_address isEqualToString:@""] || [loc_str_address isEqualToString:@"null"] || loc_str_address == nil) {
            loc_str_address=@"";
            
        }
        NSString*loc_name_address_str=[NSString stringWithFormat:@"%@ %@",loc_str_name,loc_str_address];

        if ([loc_name_address_str isEqualToString:@"(null)"] || [loc_name_address_str isEqualToString:@"<null>"] || [loc_name_address_str isEqualToString:@""] || [loc_name_address_str isEqualToString:@"null"] || loc_name_address_str == nil) {
            loc_name_address_str=@"";
            
        }
        else
        {
            _event_adress_TF.text=loc_name_address_str;
        }
        
//        _event_adress_TF.text = [NSString stringWithFormat:@"%@ %@",place.name,place.formattedAddress];
        start_latitude=place.coordinate.latitude;
        start_longitude=place.coordinate.longitude;
        event_lat=[NSString stringWithFormat:@"%f",start_latitude];
        event_long=[NSString stringWithFormat:@"%f",start_longitude];
        
        NSLog(@"Place name %@", place.name);
        NSLog(@"Place address %@", place.formattedAddress);
        NSLog(@"Place attributions %@", place.attributions.string);
    }
    else
    {
        NSLog(@"Place name %@", place.name);
        NSLog(@"Place address %@", place.formattedAddress);
        NSLog(@"Place attributions %@", place.attributions.string);
    }
    
}

- (void)placePickerDidCancel:(GMSPlacePickerViewController *)viewController {
    // Dismiss the place picker, as it cannot dismiss itself.
    [viewController dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"No place selected");
}

- (IBAction)event_type_Action:(UIButton *)sender {
    self.event_type_view.hidden=NO;
}
- (IBAction)type_Action:(UIButton *)sender {
    if (sender.tag==0) {
        self.event_type_view.hidden=YES;
        _event_type_TF.text=@"Charitable";
    }
    else{
        _event_type_TF.text=@"Normal";
        self.event_type_view.hidden=YES;
    }
    
    
}
- (IBAction)onClick_editEvent_rideGoinBtn:(id)sender {
    
    self.editEvent_scrollLabel.hidden=YES;
    self.editEvent_rideGoingBtn.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }

}
@end
