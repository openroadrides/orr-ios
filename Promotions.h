//
//  Promotions.h
//  openroadrides
//
//  Created by apple on 28/07/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APIHelper.h"
#import "Constants.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AppHelperClass.h"
#import <GooglePlaces/GooglePlaces.h>
#import "promotionsTableViewCell.h"
#import "Promotion_DetailViewController.h" 
#import "Promotion_detailpage_ViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "CBAutoScrollLabel.h"

@interface Promotions : UIViewController <CLLocationManagerDelegate,UITableViewDataSource,UITableViewDelegate>
{
    CLLocationManager *promotions_locationManager;
   
    NSMutableArray *promotion_data_array,*business_names_array;
    UIActivityIndicatorView *activeIndicatore;
    NSString *feature_promotion_id;
//  UIInputViewControllerer *popupViewController;
    
}
@property (weak, nonatomic) IBOutlet UIView *featured_promotion_view;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *Promotion_Table_Top;

@property (weak, nonatomic) IBOutlet UILabel *promotion_end_date;
@property (weak, nonatomic) IBOutlet UILabel *promotion_offer_product;
@property (weak, nonatomic) IBOutlet UIImageView *featured_Promotion_img;
@property (weak, nonatomic) IBOutlet UILabel *featured_promotion_title;

@property (weak, nonatomic) IBOutlet UITableView *Promotions_List_Table;

@property (weak, nonatomic) IBOutlet UILabel *no_data_promotions_label;
- (IBAction)Featured_Promotion_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *promotions_rideGoingBtn;
- (IBAction)onClick_promotions_rideGoingBtn:(id)sender;

@property (strong, nonatomic) NSString *is_from_rideDashboard;
@property BOOL promotionDetail_ride_going;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *promotions_scrollLabel;

@end
