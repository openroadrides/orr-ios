//
//  MapViewController.m
//  openroadrides
//
//  Created by apple on 14/07/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "MapViewController.h"
#import "Constants.h"
#import "XMLReader.h"
@interface MapViewController ()

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"ROUTE";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    
    //    UIBarButtonItem *search_btn=[[UIBarButtonItem alloc]initWithImage:
    //                                 [[UIImage imageNamed:@"SEARCH"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
    //                                                                style:UIBarButtonItemStylePlain target:self action:@selector(edit_routeclicked)];
    //    self.navigationItem.rightBarButtonItem=search_btn;
    
    UIBarButtonItem *BackButton=[[UIBarButtonItem alloc]initWithImage:
                                 [[UIImage imageNamed:@"back_Image"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
                                                                style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    
    self.navigationItem.leftBarButtonItem = BackButton;
    
  
   
    [self.view layoutIfNeeded];
    self.routeMap_scrollLabel.hidden=YES;
    self.routeMap_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.routeMap_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.routeMap_scrollLabel.textColor = [UIColor blackColor];
    self.routeMap_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.routeMap_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.routeMap_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.routeMap_scrollLabel.scrollSpeed = 60; // pixels per second
    self.routeMap_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.routeMap_scrollLabel.fadeLength = 0.f;
    self.routeMap_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.routeMap_scrollLabel observeApplicationNotifications];
    
    self.routeMap_rideGoingBtn.hidden=YES;
    if (appDelegate.ridedashboard_home) {
        
        self.routeMap_scrollLabel.hidden=NO;
        self.routeMap_rideGoingBtn.hidden=NO;
    }
    
    mapZoom=0;
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:0                                                           longitude:0
                                                                 zoom:mapZoom];
    mapView = [GMSMapView mapWithFrame:CGRectMake(0, 0, self.view_map.frame.size.width, self.view_map.frame.size.height) camera:camera];
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:CLLocationCoordinate2DMake(48.857229, -49.898547) coordinate:CLLocationCoordinate2DMake(23.955779,-127.945417)];
    [mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds]];
    mapView.delegate=self;
    [self.view_map layoutIfNeeded];
    [self.view_map addSubview:mapView];
    line_path = [GMSMutablePath path];
    
    
    
    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    activeIndicatore.hidesWhenStopped = TRUE;
//  [activeIndicatore startAnimating];
    [self.view addSubview:activeIndicatore];
    

    GMSPath *path=[GMSPath pathFromEncodedPath:_encoded_str];
    if (path.count>0) {
    polyline = [GMSPolyline polylineWithPath:path];
    polyline.strokeColor = [UIColor yellowColor];
    polyline.strokeWidth = 5.0f;
    polyline.geodesic = YES;
    polyline.map = mapView;
    GMSCoordinateBounds *bounds2 = [[GMSCoordinateBounds alloc] initWithPath:path];
    [mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds2]];
    
    
    GMSMarker *start_marker = [[GMSMarker alloc] init];
    start_marker.position = CLLocationCoordinate2DMake([path coordinateAtIndex:0].latitude,[path coordinateAtIndex:0].longitude);
    start_marker.appearAnimation = kGMSMarkerAnimationPop;
    start_marker.title=Start_Point_Title;
    start_marker.icon = [UIImage imageNamed:@"red_marker.png"];
    start_marker.map = mapView;
    
    GMSMarker *end_marker = [[GMSMarker alloc] init];
    end_marker.position =CLLocationCoordinate2DMake([path coordinateAtIndex:path.count-1].latitude,[path coordinateAtIndex:path.count-1].longitude);
    end_marker.appearAnimation = kGMSMarkerAnimationPop;
    end_marker.title=End_Point_title;
    end_marker.icon = [UIImage imageNamed:@"finish_marker.png"];
    end_marker.map = mapView;

    //    Waypoints
    for (int q=0; q<_way_points_array.count; q++) {
        NSString *waypoint_lat_str = [NSString stringWithFormat:@"%@",[[_way_points_array objectAtIndex:q] valueForKey:@"lat"]];
        NSString *watpoint_lon_str = [NSString stringWithFormat:@"%@",[[_way_points_array objectAtIndex:q] valueForKey:@"lon"]];
        NSString *watpoint_title_str = [NSString stringWithFormat:@"%@",[[[_way_points_array objectAtIndex:q] valueForKey:@"name"] valueForKey:@"text"]];
        
        double y = [waypoint_lat_str doubleValue];
        double e = [watpoint_lon_str doubleValue];
        GMSMarker *start_marker = [[GMSMarker alloc] init];
        start_marker.position = CLLocationCoordinate2DMake(y,e);
        start_marker.appearAnimation = kGMSMarkerAnimationPop;
        start_marker.title=watpoint_title_str;
        start_marker.icon = [UIImage imageNamed:@"default_marker"];
        start_marker.map = mapView;
        
    }
    }
    
    
   // [self gpx_parsing];
                
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


#pragma mark - GPX PARSING........
-(void)gpx_parsing
{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
    NSURL *url1 = [NSURL URLWithString:_gpx_file];
    NSData*fileData2 = [NSData dataWithContentsOfURL:url1];
    
    add_places =[[NSMutableArray alloc]init];
    tracks_array=[[NSMutableArray alloc]init];
    way_array=[[NSMutableArray alloc]init];
    NSError *error = nil;
    NSDictionary *dict = [XMLReader dictionaryForXMLData:fileData2
                                                 options:XMLReaderOptionsProcessNamespaces
                                                   error:&error];
        dispatch_async(dispatch_get_main_queue(), ^
                       {
    /*
    NSString *add_place_str = [[[[dict valueForKey:@"gpx"] valueForKey:@"extensions"] valueForKey:@"addplace"] valueForKey:@"place"];
   
    if ([add_place_str isKindOfClass:[NSArray class]])
    {
        NSArray *places_is = [[[[dict valueForKey:@"gpx"] valueForKey:@"extensions"] valueForKey:@"addplace"] valueForKey:@"place"];
        [add_places addObjectsFromArray:places_is];
        
    }
    else if ([add_place_str isKindOfClass:[NSDictionary class]]){
        
        NSDictionary *data = [[[[dict valueForKey:@"gpx"] valueForKey:@"extensions"] valueForKey:@"addplace"] valueForKey:@"place"];
        
        [add_places addObject:data];
    }
   //Add A Places
   for (int i=0; i<add_places.count; i++)
   {
       NSString *lat_str = [NSString stringWithFormat:@"%@",[[[add_places objectAtIndex:i] valueForKey:@"latitude"] valueForKey:@"text"]];
       NSString *long_str = [NSString stringWithFormat:@"%@",[[[add_places objectAtIndex:i] valueForKey:@"longitude"] valueForKey:@"text"]];
       NSString *add_pace_title = [NSString stringWithFormat:@"%@",[[[add_places objectAtIndex:i] valueForKey:@"title"] valueForKey:@"text"]];
       
       GMSMarker *add_place_marker = [[GMSMarker alloc] init];
       add_place_marker.position = CLLocationCoordinate2DMake([lat_str floatValue],[long_str floatValue]);
       add_place_marker.appearAnimation = kGMSMarkerAnimationPop;
       add_place_marker.title=add_pace_title;
       add_place_marker.icon = [UIImage imageNamed:@"addplace_marker_bg.png"];
       add_place_marker.map = mapView;
   }*/
                   
                           
   NSString *traks_str=[[[[dict valueForKey:@"gpx"] valueForKey:@"trk"] valueForKey:@"trkseg"] valueForKey:@"trkpt"];
   NSString*way_str=[[dict valueForKey:@"gpx"] valueForKey:@"wpt"];
    if ([traks_str isKindOfClass:[NSArray class]])
    {
        NSArray *places_is = [[[[dict valueForKey:@"gpx"] valueForKey:@"trk"] valueForKey:@"trkseg"] valueForKey:@"trkpt"];
        [tracks_array addObjectsFromArray:places_is];
        
    }
    else if ([traks_str isKindOfClass:[NSDictionary class]]){
        
        NSDictionary *data = [[[[dict valueForKey:@"gpx"] valueForKey:@"trk"] valueForKey:@"trkseg"] valueForKey:@"trkpt"];
        
        [tracks_array addObject:data];
    }
    
    
    if ([way_str isKindOfClass:[NSArray class]])
    {
        NSArray *places_is = [[dict valueForKey:@"gpx"] valueForKey:@"wpt"];
        [way_array addObjectsFromArray:places_is];
        
    }
    else if ([way_str isKindOfClass:[NSDictionary class]]){
        
        NSDictionary *data = [[dict valueForKey:@"gpx"] valueForKey:@"wpt"];
        
        [way_array addObject:data];
    }
      
    if (tracks_array.count>0) {
        
        x = [[[tracks_array firstObject] valueForKey:@"lat"] doubleValue];
        z = [[[tracks_array firstObject] valueForKey:@"lon"] doubleValue];
        GMSMarker *start_marker = [[GMSMarker alloc] init];
        start_marker.position = CLLocationCoordinate2DMake(x,z);
        start_marker.appearAnimation = kGMSMarkerAnimationPop;
        start_marker.title=Start_Point_Title;
        start_marker.icon = [UIImage imageNamed:@"red_marker.png"];
        start_marker.map = mapView;

        
        x = [[[tracks_array lastObject] valueForKey:@"lat"] doubleValue];
        z = [[[tracks_array lastObject] valueForKey:@"lon"] doubleValue];
        GMSMarker *end_marker = [[GMSMarker alloc] init];
        end_marker.position = CLLocationCoordinate2DMake(x,z);
        end_marker.appearAnimation = kGMSMarkerAnimationPop;
        end_marker.title=End_Point_title;
        end_marker.icon = [UIImage imageNamed:@"finish_marker.png"];
        end_marker.map = mapView;
        
        for (NSDictionary *track in tracks_array) {
            x = [[track valueForKey:@"lat"] doubleValue];
            z = [[track valueForKey:@"lon"] doubleValue];
            [line_path addCoordinate:CLLocationCoordinate2DMake(x, z)];
        }
    }
    polyline = [GMSPolyline polylineWithPath:line_path];
    polyline.strokeColor = [UIColor yellowColor];
    polyline.strokeWidth = 5.0f;
    polyline.geodesic = YES;
    polyline.map = mapView;
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithPath:line_path];
    [mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds]];
    [line_path removeAllCoordinates];
    
    
    //    Waypoints
    for (int q=0; q<way_array.count; q++) {
        NSString *waypoint_lat_str = [NSString stringWithFormat:@"%@",[[way_array objectAtIndex:q] valueForKey:@"lat"]];
        NSString *watpoint_lon_str = [NSString stringWithFormat:@"%@",[[way_array objectAtIndex:q] valueForKey:@"lon"]];
        NSString *watpoint_title_str = [NSString stringWithFormat:@"%@",[[[way_array objectAtIndex:q] valueForKey:@"name"] valueForKey:@"text"]];
        
        double y = [waypoint_lat_str doubleValue];
        double e = [watpoint_lon_str doubleValue];
        GMSMarker *start_marker = [[GMSMarker alloc] init];
        start_marker.position = CLLocationCoordinate2DMake(y,e);
        start_marker.appearAnimation = kGMSMarkerAnimationPop;
        start_marker.title=watpoint_title_str;
        start_marker.icon = [UIImage imageNamed:@"default_marker"];
        start_marker.map = mapView;
    }
    
       //    Tracks
        
    [way_array removeAllObjects];
    [add_places removeAllObjects];
    [tracks_array removeAllObjects];
    [activeIndicatore stopAnimating];
                           
    });
    });
}



-(void)back{
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)onClick_routeMap_rideGoingBtn:(id)sender {
    
    self.routeMap_scrollLabel.hidden=YES;
    self.routeMap_rideGoingBtn.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }

}
@end
