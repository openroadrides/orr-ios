//
//  HomeDashboardCollectionCell.h
//  openroadrides
//
//  Created by apple on 25/09/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeDashboardCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *dashboardItem_imageview;
@property (weak, nonatomic) IBOutlet UILabel *dashboardHometitle_label;

@end
