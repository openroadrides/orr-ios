//
//  FriendsViewController.h
//  openroadrides
//
//  Created by apple on 26/06/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "PublicFriends.h"
#import "MyFriends.h"
#import "AppDelegate.h"
#import "PublicFriendsCell.h"
#import "MyFriendsCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ProfileView.h"
#import "CBAutoScrollLabel.h"

@interface FriendsViewController : UIViewController<UIGestureRecognizerDelegate,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,UISearchBarDelegate>
{
    UIActivityIndicatorView  *activeIndicatore;
    UILabel *NO_DATA;
    NSString *view_Status,*my_Friends_Called,*Public_Friends_Called,*friendID,*myFrnd_FriendID,*accept_reject_string;
    NSMutableArray *arrayOfPublicFriends,*arrayOfMyFriends,*filteredArray,*filteredArray2;
    
   UIView *indicaterview;
    BOOL isFiltered;
}



@property (strong, nonatomic) IBOutlet UILabel *no_Data_Label_MY;

@property (weak, nonatomic) IBOutlet UIView *friendsView;
@property UIGestureRecognizer *gesture;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthConstraint_friendsview;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UIView *publicView;
@property (weak, nonatomic) IBOutlet UIView *myFriendsView;
@property (weak, nonatomic) IBOutlet UITableView *public_friends_tableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *my_friends_tableView;
@property (weak, nonatomic) IBOutlet UIView *public_segement_view;
@property (weak, nonatomic) IBOutlet UIView *myfriends_segment_view;
@property (weak, nonatomic) IBOutlet UIButton *publicFrnds_btn;
- (IBAction)onClick_publicfnds_btn:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *public_title_label;
@property (weak, nonatomic) IBOutlet UILabel *myfriends_title_label;
@property (weak, nonatomic) IBOutlet UIButton *myFrnds_btn;
- (IBAction)onClick_myFrnds_btn:(id)sender;
@property UIRefreshControl *refreshControl,*refreshControl2;

@property (weak, nonatomic) IBOutlet UIButton *inviteFriends_btn;
- (IBAction)onClick_inviteFrnds_btn:(id)sender;
- (IBAction)onClick_acceptBtn:(UIButton *)sender;
- (IBAction)onClick_rejectBtn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *frnds_rideGoingBtn;
- (IBAction)onClick_frnds_rideGoingBtn:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint_inviteFrndsBtn;

- (IBAction)onClick_statusBtn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *frnds_scrollLabel;
@end
