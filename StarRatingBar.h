//
//  StarRatingBar.h
//  sample rating
//
//  Created by SrkIosEbiz on 30/05/17.
//  Copyright © 2017 com.ebiz. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum
{
    rb00,
    rb1_hh,
    rb11,
    rb2_hh,
    rb22,
    rb3_hh,
    rb33,
    rb4_hh,
    rb44,
    rb5_hh,
    rb55
}RBRatingss;
@interface StarRatingBar : UIView
{
@private
    CGSize imageSize;
    RBRatingss ratingPoint;
}
-(id)initWithSize:(CGSize)size AndPosition:(CGPoint)position;
-(RBRatingss)getcurrentRatings;
-(void)setRatings:(RBRatingss)rate;
- (void)initRateBar;
@end
