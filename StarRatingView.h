//
//  StarRatingView.h
//  sample rating
//
//  Created by SrkIosEbiz on 30/05/17.
//  Copyright © 2017 com.ebiz. All rights reserved.
//

#import <UIKit/UIKit.h>
@class StarRatingView;

@protocol RateViewDelegate2
- (void)rateView_is:(StarRatingView *)rateView ratingDidChange:(float)rating;
@end
@interface StarRatingView : UIView
@property (strong, nonatomic) UIImage *notSelectedImage;
@property (strong, nonatomic) UIImage *halfSelectedImage;
@property (strong, nonatomic) UIImage *fullSelectedImage;
@property (assign, nonatomic) float rating;
@property (assign) BOOL editable;
@property (strong) NSMutableArray * imageViews;
@property (assign, nonatomic) int maxRating;
@property (assign) int midMargin;
@property (assign) int leftMargin;
@property (assign) CGSize minImageSize;
@property (assign) id <RateViewDelegate2> delegate;
@end
