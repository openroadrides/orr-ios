//
//  EditPostView.h
//  openroadrides
//
//  Created by apple on 05/12/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "AppDelegate.h"
#import "APIHelper.h"
#import "AppHelperClass.h"
#import <GooglePlacePicker/GooglePlacePicker.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "CBAutoScrollLabel.h"
@interface EditPostView : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,GMSPlacePickerViewControllerDelegate,UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UITextViewDelegate>
{
    int index,imgIndex,x;
    NSMutableArray *img_array,*base_64_image_array,*deleted_ids;
    UIButton *add_btn,*subtract_btn;
    UIView *baseView_placesImages;
    UIImageView *img;
    NSString *event_id_is,*event_lat,*event_long;
    BOOL service_call_running;
    NSString *base64_del_image;
    UIBarButtonItem *item0;
    NSString *base64String;
    double post_latitude,post_longitude;
    UIActivityIndicatorView  *activeIndicatore;
    UIView *indicaterview;
    NSString *post_status,*post_id,*categere_id;
   
    UIPickerView *typer_picker;
}
@property(strong,nonatomic)NSArray *categeries_Names_ids;
@property(strong,nonatomic)NSDictionary *post_details;
@property (weak, nonatomic) IBOutlet UIScrollView *editPost_scrollView;
@property (weak, nonatomic) IBOutlet UIView *editPost_contentView;
@property (weak, nonatomic) IBOutlet UIImageView *editPostTitleImageView;
@property (weak, nonatomic) IBOutlet UITextField *editPost_titleTF;
@property (weak, nonatomic) IBOutlet UIImageView *editPost_typeImageView;
@property (weak, nonatomic) IBOutlet UITextField *editPost_typeTF;
@property (weak, nonatomic) IBOutlet UIImageView *editPost_priceImageView;
@property (weak, nonatomic) IBOutlet UITextField *editPost_priceTF;
@property (weak, nonatomic) IBOutlet UIImageView *editPost_addressImageView;
@property (weak, nonatomic) IBOutlet UITextField *editPost_addressTF;
- (IBAction)onClick_editPost_addressBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *editPost_phoneNumberImageView;
@property (weak, nonatomic) IBOutlet UITextField *editPost_phoneNumberTF;
@property (weak, nonatomic) IBOutlet UIImageView *editPost_emailImageView;
@property (weak, nonatomic) IBOutlet UITextField *editPost_emailTF;
@property (weak, nonatomic) IBOutlet UIImageView *editPost_descriptionImageView;
@property (weak, nonatomic) IBOutlet UITextView *editPost_descriptionTextView;
@property (weak, nonatomic) IBOutlet UILabel *editPost_descriptionLabel;
@property (weak, nonatomic) IBOutlet UIView *editPost_Images_view;
@property (weak, nonatomic) IBOutlet UIScrollView *editPost_multipleImages_scrollview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint_editPostImagesView;
@property (weak, nonatomic) IBOutlet UIButton *editPost_rideGoingBtn;
- (IBAction)onClick_editPost_rideGoingBtn:(id)sender;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *editPostView_scrollLabel;
@property (weak, nonatomic) IBOutlet UIView *editPost_price_bottomView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint_editPost_priceBottomView;
@property (weak, nonatomic) IBOutlet UIImageView *editpost_priceImage;
@end
