//
//  Promotions.m
//  openroadrides
//
//  Created by apple on 28/07/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "Promotions.h"
#import "BIZPopupViewControllerDelegate.h"
#import "BIZPopupViewController.h"

#define IOS_GREATERTHAN_7   [[[UIDevice currentDevice] systemVersion] floatValue] >= 7
@interface Promotions ()<BIZPopupViewControllerDelegate>
{
   
    double promotions_currentLatitude,Promotions_currentLongitude;
     BIZPopupViewController *popupViewController;
}
@end

@implementation Promotions

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"PROMOTIONS";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    
   
    [self.navigationController.navigationBar setTranslucent:NO];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [leftBtn addTarget:self action:@selector(back_Action) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.frame = CGRectMake(0, 0, 25, 25);
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"back_Image"] forState:UIControlStateNormal];
    UIBarButtonItem *leftBackButton=[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:leftBackButton, nil];
   
    self.promotions_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.promotions_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.promotions_scrollLabel.textColor = [UIColor blackColor];
    self.promotions_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.promotions_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.promotions_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.promotions_scrollLabel.scrollSpeed = 60; // pixels per second
    self.promotions_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.promotions_scrollLabel.fadeLength = 0.f;
    self.promotions_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.promotions_scrollLabel observeApplicationNotifications];
    
    if ([_is_from_rideDashboard isEqualToString:@"RideDashboard"]) {
        self.promotions_scrollLabel.hidden=YES;
       
       
         self.promotions_rideGoingBtn.hidden=YES;
        _promotionDetail_ride_going=YES;
    }
    else
    {
        if (appDelegate.ridedashboard_home) {
            self.promotions_rideGoingBtn.hidden=NO;
            self.promotions_scrollLabel.hidden=NO;
            
        }
        else
        {
            self.promotions_scrollLabel.hidden=YES;
            self.promotions_rideGoingBtn.hidden=YES;
            _promotionDetail_ride_going=NO;
        }
    }
    
   

    
    promotions_locationManager = [[CLLocationManager alloc] init];
    promotions_currentLatitude = promotions_locationManager.location.coordinate.latitude;
    Promotions_currentLongitude = promotions_locationManager.location.coordinate.longitude;
   
    self.view.backgroundColor=[UIColor clearColor];
    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.frame=CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
    activeIndicatore.color = APP_YELLOW_COLOR;
    activeIndicatore.hidesWhenStopped = TRUE;
    [activeIndicatore startAnimating];
    
    [self.view addSubview:activeIndicatore];
    self.Promotions_List_Table.separatorStyle=UITableViewCellSeparatorStyleNone;
    [self get_promotions];

}
-(void)viewWillDisappear:(BOOL)animated
{
    promotions_locationManager=nil;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
     [[self navigationController] setNavigationBarHidden:NO animated:NO];
}

//{ "user_id":"", "current_latitude":"", "current_longitude":"" }
-(void)get_promotions
{
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [SHARED_HELPER showAlert:Nonetwork];
        [activeIndicatore stopAnimating];
        return;
    }
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
    [dict setObject:[NSNumber numberWithDouble:promotions_currentLatitude] forKey:@"current_latitude"];
    [dict setObject:[NSNumber numberWithDouble:Promotions_currentLongitude] forKey:@"current_longitude"];
//    [dict setObject:@"17.5429" forKey:@"current_latitude"];
//    [dict setObject:@"78.4814" forKey:@"current_longitude"];
    
    
    [SHARED_API promotionsWithParams:dict withSuccess:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           NSLog(@"Promotions response is %@",response);
                           if ([[response valueForKey:STATUS] isEqualToString:SUCCESS]) {
                            NSString *promotions_Status;
                               
                            NSArray *data;
                            data=[response valueForKey:@"feature_promotion"];
                            if (data.count>0)
                            {
                              NSDateFormatter *  YMD_formatter = [[NSDateFormatter alloc] init];
                                YMD_formatter.dateFormat = @"yyyy-MM-dd";
                               NSDateFormatter *  MDY_formatter = [[NSDateFormatter alloc] init];
                                MDY_formatter.dateFormat = @"MM/dd/yyyy";
                                [MDY_formatter setTimeZone:[NSTimeZone localTimeZone]];
                                
   
                                
                                feature_promotion_id=[NSString stringWithFormat:@"%d",[[[data objectAtIndex:0] valueForKey:@"id"] intValue]];
                                NSString *img_str=[[data objectAtIndex:0] valueForKey:@"featured_promotion_image"];
                                
                               NSString *title=[[data objectAtIndex:0] valueForKey:@"title"];
                               NSURL*url=[NSURL URLWithString:img_str];
                                
                               NSString *start_date=[[data objectAtIndex:0] valueForKey:@"featured_start_date"];
                               NSString *end_date=[[data objectAtIndex:0] valueForKey:@"featured_end_date"];
                                
                               NSDate *TZ_date = [YMD_formatter dateFromString:start_date];
                               start_date = [MDY_formatter stringFromDate:TZ_date];
                                
                               NSDate *TZ_date2 = [YMD_formatter dateFromString:end_date];
                               end_date = [MDY_formatter stringFromDate:TZ_date2];

                                
                                
                               NSString *date=[NSString stringWithFormat:@"Schedule:%@ to %@",start_date,end_date];
                               NSString *offer_name=[[data objectAtIndex:0] valueForKey:@"bike_name"];
                               NSString *offer_model=[[data objectAtIndex:0] valueForKey:@"bike_model"];
                               NSString *ofer_details=[NSString stringWithFormat:@"(Model:%@ %@)",offer_name,offer_model];
                                
                        [_featured_Promotion_img sd_setImageWithURL:url
                                                          placeholderImage:[UIImage imageNamed:@"add_places_placeholder"]
                                                                   options:SDWebImageRefreshCached];
//                           old PlaceHolderImage
//                            scheduledride_placeholder
                                [_featured_Promotion_img setContentMode:UIViewContentModeScaleAspectFill];
                                _featured_Promotion_img.clipsToBounds=YES;
                                _featured_promotion_title.text=title;
                                _promotion_offer_product.text=ofer_details;
                                _promotion_end_date.text=date;
                                
                               
                                promotions_Status=@"YES";
                                _Promotion_Table_Top.constant=150;
                                self.featured_promotion_view.hidden=NO;
                                [self.view layoutIfNeeded];
                                
                            }
   
                            data=[response valueForKey:@"promotion_data"];
                            if (data.count>0) {
                            promotion_data_array =[[NSMutableArray alloc]init];
                            business_names_array=[[NSMutableArray alloc]init];
                                for (int i=0; i<data.count; i++) {
                                    
                                    NSDictionary *promotion_dict=[data objectAtIndex:i];
                                    NSString *business_name=[promotion_dict valueForKey:@"business_name"];
                                    
                                    NSArray *inside_promotions=[promotion_dict valueForKey:@"promotions"];
                                    if (inside_promotions.count>0) {
                                        [promotion_data_array addObjectsFromArray:inside_promotions];
                                         for (int j=0; j<inside_promotions.count; j++) {
                                        [business_names_array addObject:business_name];
                                         }
                                    }
                                }
                                
                           if (promotion_data_array.count>0)
                           {
                               self.no_data_promotions_label.hidden=YES;
                               [activeIndicatore stopAnimating];
                               [_Promotions_List_Table reloadData];
                               
                           }
                            else
                            {
                                [activeIndicatore stopAnimating];
                                if (![promotions_Status isEqualToString:@"YES"]) {
                                    [self showAlert:@"No promotions available"];
                                    self.no_data_promotions_label.hidden=NO;
                                    [self.view addSubview:self.no_data_promotions_label];
                                }
                            }
                             
                           }
                            else
                            {
                               
                                [activeIndicatore stopAnimating];
                                if (![promotions_Status isEqualToString:@"YES"]) {
                                    [self showAlert:@"No promotions available"];
                                    self.no_data_promotions_label.hidden=NO;
                                    [self.view addSubview:self.no_data_promotions_label];
                                }
                            }
                           }
                           else
                           {
                               [SHARED_HELPER showAlert:ServiceFail];
                               [activeIndicatore stopAnimating];
                               int duration = 2; // duration in seconds
                               
                               dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                   [self.navigationController popViewControllerAnimated:YES];
                               });
                           }
                       });
        
    } onfailure:^(NSError *theError) {
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           [SHARED_HELPER showAlert:ServiceFail];
                           //[self network_Alert:@""];
                           [activeIndicatore stopAnimating];
                           
                       });
    }];
}

-(void)back_Action{
    
    [self.navigationController popViewControllerAnimated:YES];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *promotion_title=[NSString stringWithFormat:@"%@",[[promotion_data_array objectAtIndex:indexPath.row] valueForKey:@"promotion_name"]];
    UIFont *myFont = [UIFont fontWithName:@"Arial" size:20];
    CGFloat height=[Promotions findHeightForText:promotion_title havingWidth:self.view.bounds.size.width-40 andFont:myFont]+45;
    if (height>85) {
        height=85;
    }
    return height;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return promotion_data_array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    promotionsTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil)
    {
        cell = [[promotionsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    cell.Promotion_Title.text=[NSString stringWithFormat:@"%@",[[promotion_data_array objectAtIndex:indexPath.row] valueForKey:@"promotion_name"]];
    
    cell.by_Name.text=[NSString stringWithFormat:@"By %@",[business_names_array objectAtIndex:indexPath.row] ];
    
    NSString *schedule_enddate=[[promotion_data_array objectAtIndex:indexPath.row] valueForKey:@"schedule_enddate"];
    NSDateFormatter *  YMD_formatter = [[NSDateFormatter alloc] init];
    YMD_formatter.dateFormat = @"yyyy-MM-dd";
    NSDateFormatter *  MDY_formatter = [[NSDateFormatter alloc] init];
    NSDate *TZ_date = [YMD_formatter dateFromString:schedule_enddate];
    MDY_formatter.dateFormat = @"dd";
    [MDY_formatter setTimeZone:[NSTimeZone localTimeZone]];
    schedule_enddate = [MDY_formatter stringFromDate:TZ_date];
    if ([schedule_enddate isEqualToString:@"01"]||[schedule_enddate isEqualToString:@"21"]||[schedule_enddate isEqualToString:@"31"])
    {
        MDY_formatter.dateFormat = @"d'st' MMM,yyyy";
    }
    else if ([schedule_enddate isEqualToString:@"02"]||[schedule_enddate isEqualToString:@"22"])
    {
        MDY_formatter.dateFormat = @"d'nd' MMM,yyyy";
    }
    else if ([schedule_enddate isEqualToString:@"03"]||[schedule_enddate isEqualToString:@"23"])
    {
        MDY_formatter.dateFormat = @"d'rd' MMM,yyyy";
    }
    else
    {
        MDY_formatter.dateFormat = @"d'th' MMM,yyyy";
    }
    
    schedule_enddate = [MDY_formatter stringFromDate:TZ_date];
    schedule_enddate=[NSString stringWithFormat:@"End Date: %@",schedule_enddate];
    cell.promotion_end_date.text=schedule_enddate;
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    Promotion_detailpage_ViewController*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"Promotion_detailpage_ViewController"];
    controller.promotion_id=[NSString stringWithFormat:@"%d",[[[promotion_data_array objectAtIndex:indexPath.row] valueForKey:@"promotion_id"] intValue]];
    controller.promotion_detailPage_onGoing=_promotionDetail_ride_going;
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)network_Alert:(NSString *)call_from{
    UIAlertController *alertview = [UIAlertController alertControllerWithTitle:@"Network Error" message:Nonetwork preferredStyle:UIAlertControllerStyleAlert];
    
    [alertview addAction:[UIAlertAction actionWithTitle:@"retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self get_promotions];
    }]];
    
    [self presentViewController:alertview animated:YES completion:nil];
}



- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    return size.height;
}
+ (CGFloat)findHeightForText:(NSString *)text havingWidth:(CGFloat)widthValue andFont:(UIFont *)font
{
    CGFloat result = font.pointSize + 4;
    if (text)
    {
        CGSize textSize = { widthValue, CGFLOAT_MAX };       //Width and height of text area
        CGSize size;
        if (IOS_GREATERTHAN_7)
        {
            //iOS 7
            CGRect frame = [text boundingRectWithSize:textSize
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:@{ NSFontAttributeName:font }
                                              context:nil];
            size = CGSizeMake(frame.size.width, frame.size.height+1);
        }
        result = MAX(size.height, result); //At least one row
    }
    return result;
}

- (IBAction)onClick_promotions_close_btn:(id)sender {
    
}


-(void)showAlert:(NSString *)message
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:message  preferredStyle:UIAlertControllerStyleActionSheet];
    UIView  *firstSubview = alert.view.subviews.firstObject;
    firstSubview.backgroundColor = APP_YELLOW_COLOR;
    UIView *alertContentView = firstSubview.subviews.firstObject;
    alertContentView.backgroundColor = APP_YELLOW_COLOR;
    alertContentView.tintColor = [UIColor blueColor];
    for (UIView *subSubView in alertContentView.subviews) {
        
        subSubView.backgroundColor = APP_YELLOW_COLOR;
    }
    [self presentViewController:alert animated:YES completion:nil];
    
    int duration = 1; // duration in seconds
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        
        [alert dismissViewControllerAnimated:YES completion:nil];
        [self dismissViewControllerAnimated:YES completion:nil];
    });
}


- (IBAction)Featured_Promotion_Action:(id)sender {
    
    Promotion_DetailViewController*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"Promotion_DetailViewController"];
    controller.promotion_id=feature_promotion_id;
    controller.promotion_detail_onGoingRide=_promotionDetail_ride_going;
    [self.navigationController pushViewController:controller animated:YES];
    
}
- (IBAction)onClick_promotions_rideGoingBtn:(id)sender {
    
    self.promotions_scrollLabel.hidden=YES;
    self.promotions_rideGoingBtn.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }

}
@end
