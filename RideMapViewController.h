//
//  RideMapViewController.h
//  openroadrides
//
//  Created by apple on 24/07/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "XMLReader.h"
#import "Constants.h"
#import "AppHelperClass.h"
#import "AppDelegate.h"
#import "RideDashboard.h"
#import "CBAutoScrollLabel.h"
@interface RideMapViewController : UIViewController<GMSMapViewDelegate>{

NSMutableArray *add_places,*tracks_array,*way_array;
GMSMapView *mapView;
GMSMutablePath *line_path;
NSString *start_lat_long,*end_lat_long;
GMSPolyline *polyline;
double x,z;
int mapZoom;
    UIActivityIndicatorView *activeIndicatore;
}
@property (weak, nonatomic) IBOutlet UIView *view_map;
@property(strong,nonatomic)NSString *encoded_string;
@property(strong,nonatomic)NSArray *places_data_array,*way_points_array;
@property (nonatomic, strong) NSString *gpx_file;
@property (weak, nonatomic) IBOutlet UIButton *rideMap_rideGoingBtn;
- (IBAction)onClick_rideMAp_rideGoingBtn:(id)sender;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *rideMapView_scrollLAbel;
@end
