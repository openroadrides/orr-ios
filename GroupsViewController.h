//
//  GroupsViewController.h
//  openroadrides
//
//  Created by apple on 31/05/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "DashboardViewController.h"
#import "CBAutoScrollLabel.h"
@interface GroupsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>{
    NSUserDefaults *defaults;
    NSString *user_token_id;
    NSString *group_id_str;
    NSArray *groups_ary;
     UIActivityIndicatorView  *activeIndicatore;
    UIView *indicaterview;
    UILabel *NO_DATA;
    UIView *no_Data_View;
}
@property (weak, nonatomic) IBOutlet UITableView *groups_table_view;

@property (weak, nonatomic) IBOutlet UISearchBar *group_search;
@property (weak, nonatomic) IBOutlet UIButton *group_rideGoingBtn;
- (IBAction)onClick_group_rideGoingBtn:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint_groupTAbleView;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *groups_scrollLabel;
@end
