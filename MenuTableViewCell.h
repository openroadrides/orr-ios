//
//  MenuTableViewCell.h
//  openroadrides
//
//  Created by SrkIosEbiz on 29/05/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *menu_Item_Label;
@property (strong, nonatomic) IBOutlet UIImageView *menu_Item_iCon;
@property (strong, nonatomic) IBOutlet UILabel *menu_item_Count_Lab;

@end
