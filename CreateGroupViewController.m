//
//  CreateGroupViewController.m
//  openroadrides
//
//  Created by apple on 05/06/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "CreateGroupViewController.h"
#import "Constants.h"
#import "APIHelper.h"
#import "AppHelperClass.h"
#include "GroupsViewController.h"
#import "AppDelegate.h"
#import "MPCoachMarks.h"

@interface CreateGroupViewController ()
{
    UIActivityIndicatorView *activeIndicatore;
    UIView *indicaterview;
}
@end

@implementation CreateGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"CREATE GROUP";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    
    
    self.createGroup_scrollLabel.hidden=YES;
    self.createGroup_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.createGroup_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.createGroup_scrollLabel.textColor = [UIColor blackColor];
    self.createGroup_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.createGroup_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.createGroup_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.createGroup_scrollLabel.scrollSpeed = 60; // pixels per second
    self.createGroup_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.createGroup_scrollLabel.fadeLength = 0.f;
    self.createGroup_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.createGroup_scrollLabel observeApplicationNotifications];
    
    self.createGroup_rideGoingBtn.hidden=YES;
    if (appDelegate.ridedashboard_home) {
        self.createGroup_scrollLabel.hidden=NO;
        self.createGroup_rideGoingBtn.hidden=NO;
        
    }
#pragma mark - Bar buttons on nav bar.....
    
    UIButton *saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [saveBtn addTarget:self action:@selector(save_clicked) forControlEvents:UIControlEventTouchUpInside];
    saveBtn.frame = CGRectMake(0, 0, 30, 30);
    [saveBtn setBackgroundImage:[UIImage imageNamed:@"SAVE"] forState:UIControlStateNormal];
    UIBarButtonItem * create_Ride= [[UIBarButtonItem alloc] initWithCustomView:saveBtn];
    self.navigationItem.rightBarButtonItems=[NSArray arrayWithObjects:create_Ride, nil];
 
//    UIBarButtonItem *cancle_btn=[[UIBarButtonItem alloc]initWithTitle:@"X" style:UIBarButtonItemStylePlain target:self action:@selector(cancle_clicked)];
//    [cancle_btn setTitleTextAttributes:@{
//                                         NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:26.0],
//                                         NSForegroundColorAttributeName: [UIColor blackColor]
//                                         } forState:UIControlStateNormal];
//    
//    self.navigationItem.leftBarButtonItem = cancle_btn;
    
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [leftBtn addTarget:self action:@selector(left_closed_btn:) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.frame = CGRectMake(0, 0, 25, 25);
    //    [leftBtn setBackgroundImage:[UIImage imageNamed:@"Finalback-arrow.png"] forState:UIControlStateNormal];
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"close_icon"] forState:UIControlStateNormal];
    UIBarButtonItem *leftBackButton=[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    item0=leftBackButton;
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:item0, nil];
    
    [self.groupname_tf setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.description_tf setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    defaults = [NSUserDefaults standardUserDefaults];
    user_token_id = [NSString stringWithFormat:@"%@",[defaults valueForKey:User_ID]];

   
    
   self.select_members_name_label.hidden=YES;
    appDelegate.createGroup_frndInvitations=[[NSMutableArray alloc]init];
    
    //Display Annotation
    // Show coach marks
    BOOL coachMarksShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"MPCoachMarksShown_CreateGroup"];
    if (coachMarksShown == NO) {
        // Don't show again
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MPCoachMarksShown_CreateGroup"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        // Show coach marks
        [self showAnnotation];
    }
    
    // Do any additional setup after loading the view.
}

#pragma mark - Annotations
-(void)showAnnotation
{
    [self.view layoutIfNeeded];
    
    
    // Setup coach marks
    CGRect coachmark1 = CGRectMake (6,20, self.navigationController.navigationBar.frame.size.height,self.navigationController.navigationBar.frame.size.height);
    CGRect coachmark2 = CGRectMake( ([UIScreen mainScreen].bounds.size.width - 53), 20, self.navigationController.navigationBar.frame.size.height, self.navigationController.navigationBar.frame.size.height);
    CGRect coachmark3 = CGRectMake( _logo_img.frame.origin.x, _logo_img.frame.origin.y+65, _logo_img.frame.size.width, _logo_img.frame.size.height);
    
    // Setup coach marks
    NSArray *coachMarks = @[
                            @{
                                @"rect": [NSValue valueWithCGRect:coachmark1],
                                @"caption": @"Tap to close the group",
                                @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                                @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                                @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_LEFT]
                                //@"showArrow":[NSNumber numberWithBool:YES]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:coachmark2],
                                @"caption": @"Tap to save the group",
                                @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                                @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                                @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_RIGHT],
                                //@"showArrow":[NSNumber numberWithBool:YES]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:coachmark3],
                                @"caption": @"Add your group picture here",
                                @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                                @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM]
                                
                                },
                            ];
    
    
    MPCoachMarks *coachMarksView = [[MPCoachMarks alloc] initWithFrame:self.navigationController.view.bounds coachMarks:coachMarks];
    [self.navigationController.view addSubview:coachMarksView];
    //[[UIApplication sharedApplication].keyWindow addSubview:coachMarksView];
    [coachMarksView start];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)left_closed_btn:(UIButton *)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
    
    if (appDelegate.createGroup_frndInvitations.count>0) {
//        self.select_members_name_label.hidden=NO;
        self.select_members_name_label.hidden=YES;
        NSMutableString*membersnameString=[[NSMutableString alloc]init];
        
//        for (int i=0; i<appDelegate.createGroup_frndInvitations.count; i++) {
//            
//            NSString *name=[appDelegate.createGroup_frndInvitations objectAtIndex:i];
//            if (i==1) {
//                [membersnameString appendFormat:@"%@", [NSString stringWithFormat:@"%@",name]];
//            }
//            else
//                [membersnameString appendFormat:@"%@", [NSString stringWithFormat:@",%@",name]];
//        }
//        self.select_members_name_label.text=membersnameString;
        
        for (NSString * snameString in appDelegate.createGroup_frndInvitations)
        {

        NSArray *Teams_details=[snameString componentsSeparatedByString:@","];
            
                if (membersnameString.length==0)
                {
                    membersnameString=[NSMutableString stringWithFormat:@"%@",[Teams_details objectAtIndex:1]];
                }
                else
                {
                    [membersnameString appendString:[NSString stringWithFormat:@",%@",[Teams_details objectAtIndex:1]]];
                    
                }
        }
        
         self.select_members_name_label.text=[NSString stringWithFormat:@"%@",membersnameString];
    }
}
//- (void)drawTextInRect:(CGRect)rect {
//    UIEdgeInsets insets = {0, 5, 0, 5};
//    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
//}
-(void)save_clicked{
    
    [self.view endEditing:YES];
     _groupname_tf.text=[_groupname_tf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
    
    if (_groupname_tf.text.length == 0) {
        [SHARED_HELPER showAlert:groupnamefail];
        return;
    }
    else if (_description_tf.text.length >256)
    {
        [SHARED_HELPER showAlert:@"Max Character limit is 256"];
        return;
    }
    else if ([appDelegate.createGroup_frndInvitations count]==0)
    {
        self.select_members_name_label.hidden=YES;
        [SHARED_HELPER showAlert:CREATEGROUPMEMBERS];
        return;
    }
    
    [self create_Group_dict];
  
}

//"{
//""existing_group_id"":"""",
//""user_id"":"""",
//""group_name"":"""",
//""group_description"":"""",
//""group_image"":"""",
//""group_members"":[]
//}"



-(void)create_Group_dict{
    
    
    
//    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//    activeIndicatore.center = self.view.center;
//    activeIndicatore.color = APP_YELLOW_COLOR;
//    activeIndicatore.hidesWhenStopped = TRUE;
//    [activeIndicatore startAnimating];
//    [self.view addSubview:activeIndicatore];
    
    
    indicaterview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    activeIndicatore.backgroundColor=[UIColor clearColor];
    activeIndicatore = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.frame = CGRectMake(0.0, 0.0, 80, 80);
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    [indicaterview addSubview:activeIndicatore];
    [self.view addSubview:indicaterview];
    [indicaterview bringSubviewToFront:self.view];
    [activeIndicatore startAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    indicaterview.hidden=NO;

    
    if(!base64String)
    {
        base64String=@"";
    }

    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:[_groupname_tf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"group_name"];
    [dict setObject:[_description_tf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"group_description"];
    [dict setObject:base64String forKey:@"group_image"];
    [dict setObject:appDelegate.createGroup_frndInvitations forKey:@"group_members"];
    [dict setObject:@"" forKey:@"existing_group_id"];
    [dict setObject:user_token_id forKey:@"user_id"];
    NSLog(@"%@",dict);
       
    [SHARED_API createGroupRequestsWithParams:dict withSuccess:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSLog(@"responce is %@",response);
            
            if ([[response valueForKey:STATUS] isEqualToString:SUCCESS]) {
                
//                if ([activeIndicatore isAnimating]) {
//                    [activeIndicatore stopAnimating];
//                    [activeIndicatore removeFromSuperview];
//                }
                 indicaterview.hidden=YES;
                 [SHARED_HELPER showAlert:groupcreated];
                int duration = 3; // duration in seconds
                [appDelegate.createGroup_frndInvitations removeAllObjects];
                self.select_members_name_label.hidden=YES;
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    
//                    [self.navigationController popViewControllerAnimated:YES];
                GroupsViewController*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"GroupsViewController"];
                [self.navigationController pushViewController:controller animated:NO];
                    
                });
               
            }
            
        });
        
    } onfailure:^(NSError *theError) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
//            if ([activeIndicatore isAnimating]) {
//                [activeIndicatore stopAnimating];
//                [activeIndicatore removeFromSuperview];
//            }
             indicaterview.hidden=YES;
            [SHARED_HELPER showAlert:ServerConnection];
        });
        
    }];

    
}

#pragma mark -Textfileds_deligate_methods............
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -100., self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +100., self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    if (textField == _groupname_tf)
    {
        [_description_tf becomeFirstResponder];
        return NO;
    }
    
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField ==_description_tf) {
        NSString *newString = [_description_tf.text stringByReplacingCharactersInRange:range withString:string];
        return !([newString length] >= 256);
        return NO;
     
    }
    return YES;
    
}

#pragma mark -Image_picker_view............


- (IBAction)logo_action:(id)sender
{
    [self.view endEditing:YES];
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped do nothing.
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *cameraPicker = [[UIImagePickerController alloc] init];
        cameraPicker.delegate = self;
        
        [cameraPicker setAllowsEditing:NO];
        cameraPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:cameraPicker animated:YES completion:NULL];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UIImagePickerController *galleryPicker = [[UIImagePickerController alloc] init];
        galleryPicker.delegate = self;
        galleryPicker.allowsEditing = NO;
        
        galleryPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:galleryPicker animated:YES completion:NULL];
    }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    self.logo_img.layer.cornerRadius = self.logo_img.frame.size.width / 2;
    self.logo_img.clipsToBounds = YES;
    cameraImage = info[UIImagePickerControllerOriginalImage];
    _logo_img.image=cameraImage;
    [picker dismissViewControllerAnimated:YES completion:NULL];
    CGFloat compression = 0.9f;
    CGFloat maxCompression = 0.1f;
    int maxFileSize = 250*1024;
    profileImgData = UIImageJPEGRepresentation(cameraImage,compression);
    while ([profileImgData length] > maxFileSize && compression > maxCompression)
    {
        compression -= 0.1;
        profileImgData = UIImageJPEGRepresentation(cameraImage,compression);
    }
    base64String = [profileImgData base64EncodedStringWithOptions:0];
    
}

- (IBAction)onClick_createGroup_inviteFrnds_btn:(id)sender {
    RideInviteFriends*rideInviteFrnds=[self.storyboard instantiateViewControllerWithIdentifier:@"RideInviteFriends"];
    [self.navigationController pushViewController:rideInviteFrnds animated:YES];
}
- (IBAction)onClick_createGroup_rideGoingBtn:(id)sender {
    
    self.createGroup_scrollLabel.hidden=YES;
    self.createGroup_rideGoingBtn.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }
}
@end
