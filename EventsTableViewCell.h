//
//  EventsTableViewCell.h
//  openroadrides
//
//  Created by apple on 06/06/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *content_view;
@property (weak, nonatomic) IBOutlet UIImageView *image_view;
@property (weak, nonatomic) IBOutlet UILabel *title_lbl;
@property (weak, nonatomic) IBOutlet UILabel *date_lbl;
@property (weak, nonatomic) IBOutlet UILabel *start_address_lbl;
@property (weak, nonatomic) IBOutlet UILabel *end_address_lbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *date_topConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *date_notext_topConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *ridescount_imageview;
@property (weak, nonatomic) IBOutlet UIImageView *eventDateImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *eventTitle_topConstraint;
@end
