//
//  RideDetailViewController.m
//  openroadrides
//
//  Created by apple on 21/06/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "RideDetailViewController.h"
#import "RideDetailTableViewCell.h"
#import "Constants.h"
#import "AppHelperClass.h"
#import "APIHelper.h"
#import "AppDelegate.h"
@interface RideDetailViewController (){
    UIBarButtonItem *item0,*item1;
    NSDictionary *ride_data,*route_details;
    NSMutableArray *riders,*userData;
    NSArray *itemsfrdid,*itemsgroupsid;
}

@end

@implementation RideDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     [self.navigationController.navigationBar setHidden:NO];
    self.title = @"RIDE";
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
   
    
    self.ridedetail_tbl_vw.dataSource=self;
    self.ridedetail_tbl_vw.delegate=self;
    
    self.rideDetail_scrollLabel.hidden=YES;
    self.rideDetail_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.rideDetail_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.rideDetail_scrollLabel.textColor = [UIColor blackColor];
    self.rideDetail_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.rideDetail_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.rideDetail_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.rideDetail_scrollLabel.scrollSpeed = 60; // pixels per second
    self.rideDetail_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.rideDetail_scrollLabel.fadeLength = 0.f;
    self.rideDetail_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.rideDetail_scrollLabel observeApplicationNotifications];
    
    self.rideDetail_rideGoingBtn.hidden=YES;
    if (appDelegate.ridedashboard_home) {
        self.rideDetail_scrollLabel.hidden=NO;
        self.rideDetail_rideGoingBtn.hidden=NO;
    }
    [self.view layoutIfNeeded];
    mapZoom=0;
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:0
                                                            longitude:0
                                                                 zoom:mapZoom];
    
    
    
    ride_mapView = [GMSMapView mapWithFrame:CGRectMake(0, 0, self.mapShowing_view.frame.size.width, self.mapShowing_view.frame.size.height) camera:camera];
    // Insets are specified in this order: top, left, bottom, right
    UIEdgeInsets mapInsets = UIEdgeInsetsMake(0.0, 0.0, 20.0, 0.0);
    ride_mapView.padding = mapInsets;
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:CLLocationCoordinate2DMake(48.857229, -49.898547) coordinate:CLLocationCoordinate2DMake(23.955779,-127.945417)];
    [ride_mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds]];
    ride_mapView.delegate=self;
    line_path = [GMSMutablePath path];
    [self.mapShowing_view addSubview:ride_mapView];
    [self.mapShowing_view addSubview:_ride_route_name_label];
    
    // self.ride_route_name_label.textColor=[UIColor whiteColor];
    //    UIBarButtonItem *Savebtn=[[UIBarButtonItem alloc]initWithImage:
    //                              [[UIImage imageNamed:@"notificationicon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
    //                                                             style:UIBarButtonItemStylePlain target:self action:@selector(notificationClicked)];
    //    self.navigationItem.rightBarButtonItem=Savebtn;
    //    UIBarButtonItem *menuButton=[[UIBarButtonItem alloc]initWithImage:
    //                                 [[UIImage imageNamed:@"sidemenuicon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
    //                                                                style:UIBarButtonItemStylePlain target:self action:@selector(Menu_Action)];
    //
    //    self.navigationItem.leftBarButtonItem = menuButton;
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [leftBtn addTarget:self action:@selector(leftBtn:) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.frame = CGRectMake(0, 0, 25, 25);
    //    [leftBtn setBackgroundImage:[UIImage imageNamed:@"Finalback-arrow.png"] forState:UIControlStateNormal];
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"back_Image"] forState:UIControlStateNormal];
    UIBarButtonItem *leftBackButton=[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    item0=leftBackButton;
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:item0, nil];
    _delete_btn_view.layer.borderWidth = 1.0;
    _delete_btn_view.layer.borderColor = [UIColor colorWithRed:255.0/255.0 green:2.0/255.0 blue:7.0/255.0 alpha:1.0].CGColor;
    //    riders=[[NSMutableArray alloc]init];
    
    self.ridedetail_tbl_vw.separatorStyle=UITableViewCellSeparatorStyleNone;
    
    
//    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//    activeIndicatore.center = self.view.center;
//    activeIndicatore.color = APP_YELLOW_COLOR;
//    activeIndicatore.hidesWhenStopped = TRUE;
//    [self.view addSubview:activeIndicatore];
    
    indicaterview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    activeIndicatore.backgroundColor=[UIColor clearColor];
    activeIndicatore = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.frame = CGRectMake(0.0, 0.0, 80, 80);
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    [indicaterview addSubview:activeIndicatore];
    [self.view addSubview:indicaterview];
    [indicaterview bringSubviewToFront:self.view];
    [activeIndicatore startAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    indicaterview.hidden=YES;

    
    appDelegate.Ride_Detail_Edited=@"";
    
    [self get_ride_deatais];
    
}
-(void)viewWillAppear:(BOOL)animated{
    if ([appDelegate.Ride_Detail_Edited isEqualToString:@"YES"]) {
        [self get_ride_deatais];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark - Tableview methods......

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return riders.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RideDetailTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil)
    {
        cell = [[RideDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    if (indexPath.row%2==0) {
        cell.content_view.backgroundColor=ROUTES_CELL_BG_COLOUR1;
    }
    else
        cell.content_view.backgroundColor=ROUTES_CELL_BG_COLOUR2;
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.rider_name_lbl.text=[NSString stringWithFormat:@"%@",[[riders objectAtIndex:indexPath.row] objectForKey:@"name"]];
    
    NSString *profile_image_string = [NSString stringWithFormat:@"%@", [[riders objectAtIndex:indexPath.row] objectForKey:@"profile_image"]];
    NSURL*url=[NSURL URLWithString:profile_image_string];
    [cell.profile_img sd_setImageWithURL:url
                        placeholderImage:[UIImage imageNamed:@"user_Placeholder"]
                                 options:SDWebImageRefreshCached];
    
    
    
    NSString * areaName=[NSString stringWithFormat:@"%@",[[riders objectAtIndex:indexPath.row] objectForKey:@"address"]];
    NSString * cityName=[NSString stringWithFormat:@"%@",[[riders objectAtIndex:indexPath.row] objectForKey:@"city"]];
    NSString * stateName=[NSString stringWithFormat:@"%@",[[riders objectAtIndex:indexPath.row] objectForKey:@"state"]];
    NSString *zipCode=[NSString stringWithFormat:@"%@",[[riders objectAtIndex:indexPath.row] objectForKey:@"zipcode"]];
    
    NSMutableArray *empaty_array_search_public=[[NSMutableArray alloc]init];
    [empaty_array_search_public addObject:@"<null>"];
    [empaty_array_search_public addObject:@"null"];
    [empaty_array_search_public addObject:@""];
    [empaty_array_search_public addObject:@"(null)"];
    [empaty_array_search_public addObject:@"0"];
    
    NSMutableArray *Data_array_search_public=[[NSMutableArray alloc]init];
    if ([empaty_array_search_public containsObject:areaName]) {
        
    }
    else{
        
        [Data_array_search_public addObject:areaName];
    }
    if ([empaty_array_search_public containsObject:cityName]) {
        
    }
    else{
        [Data_array_search_public addObject:cityName];
    }
    
    if ([empaty_array_search_public containsObject:stateName]) {
        
    }
    else{
        [Data_array_search_public addObject:stateName];
    }
    
    if ([empaty_array_search_public containsObject:zipCode]) {
        
    }
    else{
        [Data_array_search_public addObject:zipCode];
    }
    
    NSMutableString *add_data_search_public=[[NSMutableString alloc]init];
    if (Data_array_search_public.count>0) {
        //        cell.location_imageview.image=[UIImage imageNamed:@"check_in_placeholder"];
        
        for (int i=0; i<Data_array_search_public.count; i++) {
            
            NSString *adress=[Data_array_search_public objectAtIndex:i];
            if (i==0) {
                [add_data_search_public appendFormat:@"%@", [NSString stringWithFormat:@"%@",adress]];
            }
            else
                [add_data_search_public appendFormat:@"%@", [NSString stringWithFormat:@",%@",adress]];
        }
        cell.location_lbl.text=add_data_search_public;
    }
    else{
        cell.location_imageView.image=[UIImage imageNamed:@""];
        cell.location_lbl.text=@"";
    }
    return cell;
}

//webservice for RideDetails
-(void)notificationClicked
{
    
    
}
//-(void)Menu_Action
//{
//    [self.navigationController popViewControllerAnimated:YES];
//}
-(void)leftBtn:(UIButton *)sender{
    if ([_is_From isEqualToString:@"RIDE_DASHBOARD"]) {
        //Navigate to Dashboard/
        DashboardViewController *dashboard = [self.storyboard instantiateViewControllerWithIdentifier:@"DashboardViewController"];
        [self.navigationController pushViewController:dashboard animated:YES];
        return;
    }
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)editBtn:(UIButton *)sender
{
    edit_ride_type_string=@"edit";
    EditRideController *editRide = [self.storyboard instantiateViewControllerWithIdentifier:@"EditRideController"];
    
    NSDictionary *user_dict=@{@"ride_name":edit_ride_name,@"ride_image_url":[NSString stringWithFormat:@"%@",edit_ride_image_url],@"ride_start_address":[NSString stringWithFormat:@"%@",edit_placeToStart ],@"ride_start_date":[NSString stringWithFormat:@"%@",edit_ride_date ],@"ride_start_time":[NSString stringWithFormat:@"%@", edit_ride_time],@"ride_meeting_address":[NSString stringWithFormat:@"%@", edit_meeting_address ],@"ride_meeting_date":[NSString stringWithFormat:@"%@",edit_meeting_date],@"ride_meeting_time":[NSString stringWithFormat:@"%@",edit_meeting_time],@"ride_description":[NSString stringWithFormat:@"%@",edit_ride_desciption],@"ride_frndID":@"",@"ride_groupID":@"",@"ride_start_lat":[NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:edit_ride_start_latitude] ],@"ride_start_log":[NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:edit_ride_start_longtitude] ],@"ride_meeting_log":[NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:edit_ride_meeting_log] ],@"ride_meeting_lat":[NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:edit_ride_meeting_lat] ],@"Ride_id":ride_id_str,@"route_id":route_id_str,@"type":edit_ride_type_string,@"selected_friends":itemsfrdid,@"selected_groups":itemsgroupsid};
    
    editRide.edit_Ride_Data=user_dict;
    
    [self.navigationController pushViewController:editRide animated:YES];
    
}
-(void)get_ride_deatais
{
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
//    [activeIndicatore startAnimating];
    indicaterview.hidden=NO;
    NSMutableDictionary *dict = [NSMutableDictionary new];
  
  
    [dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
//    [dict setObject:@"61" forKey:@"user_id"];
    [dict setObject:_scheduled_fullFilled_rideID_string forKey:@"ride_id"];
    
    if ([_scheduled_fullFilled_user_ride_data_string isEqualToString:@"FUllFILLED"]) {
          [dict setObject:@"1" forKey:@"type"];
    }
    else{
          [dict setObject:@"0" forKey:@"type"];
    }
   
    
    [SHARED_API rideDetailRequestsWithParams:dict withSuccess:^(NSDictionary *response) {
        
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           
                           NSLog(@" Ride_Detail responce is : %@",response);
                           if ([[response valueForKey:STATUS]isEqualToString:SUCCESS]) {
                               
                           
                           NSLog(@"Ride ID %@",_scheduled_fullFilled_rideID_string);
                               route_details=[[NSDictionary alloc]init];
                           ride_data = [response valueForKey:@"ride_data"];
                           route_details =[ride_data valueForKey:@"route_details"];
                           
                           
                           riders=[[NSMutableArray alloc]init];
                           riders=[ride_data valueForKey:@"riders"];
                           userData=[[NSMutableArray alloc]init];
                           userData=[ride_data valueForKey:@"user_ride_data"];
                           [self.ridedetail_tbl_vw reloadData];
                           
                               
                           if ([_scheduled_fullFilled_user_ride_data_string isEqualToString:@"SCHEDULED"]) {
                               
                               UIButton *editBtn = [UIButton buttonWithType:UIButtonTypeSystem];
                               
                               [editBtn addTarget:self action:@selector(editBtn:) forControlEvents:UIControlEventTouchUpInside];
                               
                               editBtn.frame = CGRectMake(0, 0, 25, 25);
                               
                               //    [leftBtn setBackgroundImage:[UIImage imageNamed:@"Finalback-arrow.png"] forState:UIControlStateNormal];
                               
                               [editBtn setBackgroundImage:[UIImage imageNamed:@"setup-ride"] forState:UIControlStateNormal];
                               
                               UIBarButtonItem *rightButton=[[UIBarButtonItem alloc] initWithCustomView:editBtn];
                               
                               item1=rightButton;
                               NSString *ride_type_in_schedules=[NSString stringWithFormat:@"%@",[ride_data valueForKey:@"ride_type"]];
                               if ([ride_type_in_schedules isEqualToString:@"free_ride"]) {
                                   
                               }
                               else
                               {
                               self.navigationItem.rightBarButtonItems=[NSArray arrayWithObjects:item1, nil];
                               }
                               
                               
                               
                               edit_ride_image_url = [NSString stringWithFormat:@"%@",[ride_data valueForKey:@"ride_cover_image"]];
                               ride_id_str = [NSString stringWithFormat:@"%@",[ride_data valueForKey:@"ride_id"]];
                             
                               NSString *route_id=[NSString stringWithFormat:@"%@",[ride_data valueForKey:@"route_id"]];
                               if ([route_id isEqualToString:@"<null>"] || [route_id isEqualToString:@""] || [route_id isEqualToString:@"null"] || route_id == nil ||[route_id isEqualToString:@"0"])
                               {
                                   [line_path removeAllCoordinates];
                                   [ride_mapView clear];
                                   route_id_str = @"";
//                                    [activeIndicatore stopAnimating];
                                   indicaterview.hidden=YES;
                               }
                               else{
                                   if ([route_id_str isEqualToString:route_id]) {
//                                       [activeIndicatore stopAnimating];
                                       indicaterview.hidden=YES;
                                   }
                                   else
                                   {
                                 gpx_strng = [NSString stringWithFormat:@"%@",[[ride_data valueForKey:@"route_details"]valueForKey:@"route_gpx_file"]];
                                   route_id_str=route_id;
                                      
                                    [self gpx_parsing];
                                }
                               }
                               
                               [[AppHelperClass appsharedInstance] setRideType:[NSString stringWithFormat:@"%@",[ride_data valueForKey:@"ride_type"]]];
                               
                               self.ride_name.text=[ride_data valueForKey:@"ride_title"];
                               
                               edit_ride_name=[NSString stringWithFormat:@"%@",self.ride_name.text];
                               
                               edit_placeToStart=[NSString stringWithFormat:@"%@",[ride_data valueForKey:@"ride_start_address"]];
                               edit_ride_start_latitude=[[NSString stringWithFormat:@"%@",[ride_data valueForKey:@"ride_start_latitude"]] doubleValue];
                               edit_ride_start_longtitude=[[NSString stringWithFormat:@"%@",[ride_data valueForKey:@"ride_start_longitude"]] doubleValue];
                               
                               edit_ride_meeting_lat=[[NSString stringWithFormat:@"%@",[ride_data valueForKey:@"meeting_point_latitude"]] doubleValue];
                               edit_ride_meeting_log=[[NSString stringWithFormat:@"%@",[ride_data valueForKey:@"meeting_point_longitude"]] doubleValue];
                               
                               NSString*edit_start_time=[NSString stringWithFormat:@"%@",[ride_data valueForKey:@"scheduled_start_time"]];
                               
                               
                               
                               if ([edit_start_time isEqualToString:@"<null>"] || [edit_start_time isEqualToString:@""] || [edit_start_time isEqualToString:@"null"] || edit_start_time == nil) {
                                   
                                   edit_ride_time=@"";
                                   
                                   
                                   
                               }
                               
                               
                               
                               else{
                                   
                                   NSDateFormatter *dateFormatter5 = [[NSDateFormatter alloc] init] ;
                                   [dateFormatter5 setDateFormat:@"HH:mm:ss"];
                                   NSDate *time_str = [dateFormatter5 dateFromString:edit_start_time];
                                   [dateFormatter5 setDateFormat:@"hh:mm a"];
                                    edit_ride_time = [dateFormatter5 stringFromDate:time_str];
                                   
//                                   edit_ride_time=[NSString stringWithFormat:@"%@",[ride_data valueForKey:@"scheduled_start_time"]];
                                   
                               }
                               
                               
                               
                               NSString*editMeeting_address=[NSString stringWithFormat:@"%@",[ride_data valueForKey:@"meeting_point_address"]];
                               
                               
                               
                               if ([editMeeting_address isEqualToString:@"<null>"] || [editMeeting_address isEqualToString:@""] || [editMeeting_address isEqualToString:@"null"] || editMeeting_address == nil) {
                                   
                                   edit_meeting_address=@"";
                                   
                                   
                                   
                               }
                               
                               
                               
                               else{
                                   
                                   edit_meeting_address=[NSString stringWithFormat:@"%@",[ride_data valueForKey:@"meeting_point_address"]];
                                   
                               }
                               
                               
                               
                               NSString*editMeeting_date=[NSString stringWithFormat:@"%@",[ride_data valueForKey:@"meeting_date"]];
                               
                               
                               
                               if ([editMeeting_date isEqualToString:@"<null>"] || [editMeeting_date isEqualToString:@""] || [editMeeting_date isEqualToString:@"null"] || editMeeting_date == nil) {
                                   
                                   edit_meeting_date=@"";
                                   
                                   
                                   
                               }
                               
                               
                               
                               else{
                                   
                                   edit_meeting_date=[NSString stringWithFormat:@"%@",[ride_data valueForKey:@"meeting_date"]];
                                   
                               }
                               
                               NSString*editMeeting_time=[NSString stringWithFormat:@"%@",[ride_data valueForKey:@"meeting_time"]];
                               
                               
                               
                               if ([editMeeting_time isEqualToString:@"<null>"] || [editMeeting_time isEqualToString:@""] || [editMeeting_time isEqualToString:@"null"] || editMeeting_time == nil || [editMeeting_time isEqualToString:@"00:00:00"]) {
                                   
                                   edit_meeting_time=@"";
                                   
                                   
                               }
                               
                               else{
                                   NSDateFormatter *dateFormatter5 = [[NSDateFormatter alloc] init] ;
                                   [dateFormatter5 setDateFormat:@"HH:mm:ss"];
                                   NSDate *time_str = [dateFormatter5 dateFromString:editMeeting_time];
                                   [dateFormatter5 setDateFormat:@"hh:mm a"];
                                   edit_meeting_time = [dateFormatter5 stringFromDate:time_str];
                                   
//                                   edit_meeting_time=[NSString stringWithFormat:@"%@",[ride_data valueForKey:@"meeting_time"]];
                                   
                               }
                               
                               edit_ride_desciption=[ride_data valueForKey:@"ride_description"];
                               
                               
                               
                               self.ride_description_lbl.text=[ride_data valueForKey:@"ride_description"];
                               
                               self.rider_count_lbl.text=[NSString stringWithFormat:@"%@",[ride_data valueForKey:@"riders_count"] ];
                               
                               self.elevation_lbl.text=[NSString stringWithFormat:@"0"];
                               
                               
                               
                               NSString*scheduled_start_date=[NSString stringWithFormat:@"%@",[ride_data valueForKey:@"scheduled_start_date"]];
                               
                               
                               
                               if ([scheduled_start_date isEqualToString:@"<null>"] || [scheduled_start_date isEqualToString:@""] || [scheduled_start_date isEqualToString:@"null"] || scheduled_start_date == nil) {
                                   
                                   self.date_lbl.text=@"";
                                   
                               }
                               
                               else{
                                   
                                   NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
                                   
                                   [dateFormatter2 setDateFormat:@"yyyy-MM-dd"];
                                   
                                   NSDate *date2 = [dateFormatter2 dateFromString:scheduled_start_date];
                                   
                                   dateFormatter2 = [[NSDateFormatter alloc] init];
                                   
                                   [dateFormatter2 setDateFormat:@"EEEE, LLLL d, yyyy"];// here set format
                                   
                                   NSString *convertedString2 = [dateFormatter2 stringFromDate:date2];
                                   
                                   NSLog(@"Converted String : %@",convertedString2);
                                   
                                   self.date_lbl.text=[NSString stringWithFormat:@"%@",convertedString2];
                                   
                                   edit_ride_date=[NSString stringWithFormat:@"%@",[ride_data valueForKey:@"scheduled_start_date"]];
                                   
                               }
                               
                               NSString*frndsIds=[NSString stringWithFormat:@"%@",[ride_data valueForKey:@"invited_friends"]];
                               
                               if ([frndsIds isEqualToString:@"<null>"] || [frndsIds isEqualToString:@""] || [frndsIds isEqualToString:@"null"] || frndsIds == nil) {
                                   
                                   itemsfrdid=[[NSArray alloc]init];
                                   
                               }
                               
                               else{
                                   _edit_frnds_string=[NSString stringWithFormat:@"%@",frndsIds];
                                   itemsfrdid = [_edit_frnds_string componentsSeparatedByString:@","];
                                   
                               
                               }
                               
                               
                               NSString*scheduled_routes_Name_string=[NSString stringWithFormat:@"%@",[route_details valueForKey:@"route_title"]];
                               
                               if ([scheduled_routes_Name_string isEqualToString:@"<null>"] || [scheduled_routes_Name_string isEqualToString:@""] || [scheduled_routes_Name_string isEqualToString:@"null"] || scheduled_routes_Name_string == nil) {
                                   
                                   self.ride_route_name_label.text=[NSString stringWithFormat:@""];
                                   
                               }
                               
                               else{
                                   self.ride_route_name_label.text=[NSString stringWithFormat:@"%@",[route_details valueForKey:@"route_title"]];
                                   
                               }

                               
                               NSString*groupsIds=[NSString stringWithFormat:@"%@",[ride_data valueForKey:@"invited_groups"]];
                               
                               if ([groupsIds isEqualToString:@"<null>"] || [groupsIds isEqualToString:@""] || [groupsIds isEqualToString:@"null"] || groupsIds == nil) {
                                   
                                   itemsgroupsid=[[NSArray alloc]init];
                                   
                               }
                               
                               else{
                                   _edit_groups_string=[NSString stringWithFormat:@"%@",groupsIds];
                                   itemsgroupsid=[_edit_groups_string componentsSeparatedByString:@","];
                                   
                               }

                               NSString *user_=[NSString stringWithFormat:@"%@",[Defaults valueForKey:User_ID]];
                               if ([user_ isEqualToString:_ride_owner_id]) {
                                   _delete_btn_view.hidden=YES;
                               }
                               else{
                                   self.navigationItem.rightBarButtonItems=nil;
                               }
                               
                           }
                           
                           else if ([_scheduled_fullFilled_user_ride_data_string isEqualToString:@"FUllFILLED"])
                           {
                               
                                self.delete_btn_view.hidden=YES;
                               self.ride_name.text=[ride_data valueForKey:@"ride_title"];
                               self.ride_description_lbl.text=[ride_data valueForKey:@"ride_description"];
                               self.rider_count_lbl.text=[NSString stringWithFormat:@"%@",[ride_data valueForKey:@"riders_count"] ];
                              
                               gpx_strng = [NSString stringWithFormat:@"%@",[userData valueForKey:@"route_gpx_file"]];
                               NSString*routes_Name_string_fullFilled=[NSString stringWithFormat:@"%@",[userData valueForKey:@""]];
                               
                               if ([routes_Name_string_fullFilled isEqualToString:@"<null>"] || [routes_Name_string_fullFilled isEqualToString:@""] || [routes_Name_string_fullFilled isEqualToString:@"null"] || routes_Name_string_fullFilled == nil) {
                                   
                                   self.ride_route_name_label.text=[NSString stringWithFormat:@""];
                                   
                               }
                               
                               else{
                                   self.ride_route_name_label.text=[NSString stringWithFormat:@"%@",[userData valueForKey:@"route_title"]];
                                   
                               }

                               NSString*fullFilled_start_date=[NSString stringWithFormat:@"%@",[userData valueForKey:@"ride_start_date"]];
                               
                               if ([fullFilled_start_date isEqualToString:@"<null>"] || [fullFilled_start_date isEqualToString:@""] || [fullFilled_start_date isEqualToString:@"null"] || fullFilled_start_date == nil) {
                                   self.date_lbl.text=@"";
                                   
                               }
                               
                               else{
                                   NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
                                   [dateFormatter1 setDateFormat:@"yyyy-MM-dd"];
                                   NSDate *date1 = [dateFormatter1 dateFromString:fullFilled_start_date];
                                   dateFormatter1 = [[NSDateFormatter alloc] init];
                                   [dateFormatter1 setDateFormat:@"EEEE, LLLL d, yyyy"];// here set format
                                   NSString *convertedString = [dateFormatter1 stringFromDate:date1];
                                   NSLog(@"Converted String : %@",convertedString);
                                   self.date_lbl.text=[NSString stringWithFormat:@"%@",convertedString];
                               }
                               
                               
                               
                               //                    if (userData==nil) {
                               //
                               //                    }
                               //                    else{
                               
                               
                               float avg_s=[[userData valueForKey:@"avg_speed"] floatValue];
                               NSString*avgSpeed=[NSString stringWithFormat:@"%.2f",avg_s];
                               if ([avgSpeed isEqualToString:@"<null>"] || [avgSpeed isEqualToString:@""] || [avgSpeed isEqualToString:@"null"] || avgSpeed == nil) {
                                   self.avg_speed_lbl.text=@"0";
                                   
                               }
                               else
                               {
                                   self.avg_speed_lbl.text=avgSpeed;
                               }
                               
                               
                               
                               
                               NSString*maxSpeed=[NSString stringWithFormat:@"%@",[userData valueForKey:@"max_speed"]];
                               
                               if ([maxSpeed isEqualToString:@"<null>"] || [maxSpeed isEqualToString:@""] || [maxSpeed isEqualToString:@"null"] || maxSpeed == nil) {
                                   self.max_speed_lbl.text=@"0";
                                   
                               }
                               else{
                                   
                                   self.max_speed_lbl.text=[NSString stringWithFormat:@"%@",[userData valueForKey:@"max_speed"]];
                               }
                               
                               float total_dist=[[userData valueForKey:@"total_distance"] floatValue];
                               NSString*totalDistance_is=[NSString stringWithFormat:@"%.2f",total_dist];
                              self.total_distance_lbl.text = totalDistance_is;
                            
                               
                               NSString*rideTime=[NSString stringWithFormat:@"%@",[userData valueForKey:@"ride_time"]];
                               
                               if ([rideTime isEqualToString:@"<null>"] || [rideTime isEqualToString:@""] || [rideTime isEqualToString:@"null"] || rideTime == nil) {
                                   self.ridetime_lbl.text=@"00:00:00";
                                   
                               }
                               
                               else{
                                   
                                   self.ridetime_lbl.text=[NSString stringWithFormat:@"%@",[userData valueForKey:@"ride_time"]];
                               }
                               
                               NSString*ridetotalTime=[NSString stringWithFormat:@"%@",[userData valueForKey:@"ride_total_time"]];
                               
                               if ([ridetotalTime isEqualToString:@"<null>"] || [ridetotalTime isEqualToString:@""] || [ridetotalTime isEqualToString:@"null"] || ridetotalTime == nil) {
                                   self.totaltime_lbl.text=@"00:00:00";
                                   
                               }
                               
                               else{
                                   
                                   self.totaltime_lbl.text=[NSString stringWithFormat:@"%@",[userData valueForKey:@"ride_total_time"]];
                               }
                               
                               NSString*elevation=[NSString stringWithFormat:@"%@",[userData valueForKey:@"route_max_elevation"]];
                               
                               if ([elevation isEqualToString:@"<null>"] || [elevation isEqualToString:@""] || [elevation isEqualToString:@"null"] || elevation == nil) {
                                   self.elevation_lbl.text=@"0";
                                   
                               }
                               
                               else{
                                   
                                   self.elevation_lbl.text=[NSString stringWithFormat:@"%ld",(long)[[userData valueForKey:@"route_max_elevation"] integerValue]];
                               }
                               //                }
                               
                               
                               NSString*min_elevation=[NSString stringWithFormat:@"%@",[userData valueForKey:@"route_min_elevation"]];
                               
                               if ([min_elevation isEqualToString:@"<null>"] || [min_elevation isEqualToString:@""] || [min_elevation isEqualToString:@"null"] || min_elevation == nil) {
                                   self.minimu_elevation_label.text=@"0";
                                   
                               }
                               
                               else{
                                   
                                   self.minimu_elevation_label.text=[NSString stringWithFormat:@"%ld",(long)[[userData valueForKey:@"route_min_elevation"] integerValue]];
                               }

                               
                              
                            [self gpx_parsing];
                                             
//                               [activeIndicatore stopAnimating];
                           }
                           
//                           if ([activeIndicatore isAnimating]) {
                              
//                               [activeIndicatore removeFromSuperview];
//                           }
                           }
                           else{
                               [SHARED_HELPER showAlert:ServiceFail];
                               int duration = 2; // duration in seconds
//                               [activeIndicatore stopAnimating];
                               indicaterview.hidden=YES;
                               dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                   [self leftBtn:0];
                               });
                           }
                       });
        
    } onfailure:^(NSError *theError) {
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           
                           [SHARED_HELPER showAlert:ServiceFail];
                           
//                           if ([activeIndicatore isAnimating]) {
//                               [activeIndicatore stopAnimating];
//                               [activeIndicatore removeFromSuperview];
//                           }
                           indicaterview.hidden=YES;
                           
                           
                       });
    }];
    
}

#pragma mark - GPX PARSING........
-(void)gpx_parsing
{
    
    ride_mapView.userInteractionEnabled=NO;
//    [activeIndicatore startAnimating];
    indicaterview.hidden=NO;
    [line_path removeAllCoordinates];
    [ride_mapView clear];
    line_path = [GMSMutablePath path];
    
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
    NSURL *url1 = [NSURL URLWithString:gpx_strng];
    NSData*fileData2 = [NSData dataWithContentsOfURL:url1];
    
   
   
    add_places =[[NSMutableArray alloc]init];
    tracks_array=[[NSMutableArray alloc]init];
    way_array=[[NSMutableArray alloc]init];
    
    NSError *error = nil;
    NSDictionary *dict = [XMLReader dictionaryForXMLData:fileData2
                                                 options:XMLReaderOptionsProcessNamespaces
                                                   error:&error];
        dispatch_async(dispatch_get_main_queue(), ^
      {
     /*
    NSString *add_place_str = [[[[dict valueForKey:@"gpx"] valueForKey:@"extensions"] valueForKey:@"addplace"] valueForKey:@"place"];
     
    
    if ([add_place_str isKindOfClass:[NSArray class]])
    {
        NSArray *places_is = [[[[dict valueForKey:@"gpx"] valueForKey:@"extensions"] valueForKey:@"addplace"] valueForKey:@"place"];
        [add_places addObjectsFromArray:places_is];
        
    }
    else if ([add_place_str isKindOfClass:[NSDictionary class]]){
        
        NSDictionary *data = [[[[dict valueForKey:@"gpx"] valueForKey:@"extensions"] valueForKey:@"addplace"] valueForKey:@"place"];
        
        [add_places addObject:data];
    }
                           
   //Add A Places
   for (int i=0; i<add_places.count; i++)
   {
       NSString *lat_str = [NSString stringWithFormat:@"%@",[[[add_places objectAtIndex:i] valueForKey:@"latitude"] valueForKey:@"text"]];
       NSString *long_str = [NSString stringWithFormat:@"%@",[[[add_places objectAtIndex:i] valueForKey:@"longitude"] valueForKey:@"text"]];
       NSString *add_pace_title = [NSString stringWithFormat:@"%@",[[[add_places objectAtIndex:i] valueForKey:@"title"] valueForKey:@"text"]];
       
       
       
       GMSMarker *add_place_marker = [[GMSMarker alloc] init];
       add_place_marker.position = CLLocationCoordinate2DMake([lat_str floatValue],[long_str floatValue]);
       add_place_marker.appearAnimation = kGMSMarkerAnimationPop;
       add_place_marker.title=add_pace_title;
       add_place_marker.icon = [UIImage imageNamed:@"addplace_marker_bg.png"];
       add_place_marker.map = ride_mapView;
   }*/
                         
    
   NSString *traks_str=[[[[dict valueForKey:@"gpx"] valueForKey:@"trk"] valueForKey:@"trkseg"] valueForKey:@"trkpt"];
   NSString*way_str=[[dict valueForKey:@"gpx"] valueForKey:@"wpt"];
                         
    if ([traks_str isKindOfClass:[NSArray class]])
    {
        NSArray *places_is = [[[[dict valueForKey:@"gpx"] valueForKey:@"trk"] valueForKey:@"trkseg"] valueForKey:@"trkpt"];
        [tracks_array addObjectsFromArray:places_is];
        
    }
    else if ([traks_str isKindOfClass:[NSDictionary class]]){
        
        NSDictionary *data = [[[[dict valueForKey:@"gpx"] valueForKey:@"trk"] valueForKey:@"trkseg"] valueForKey:@"trkpt"];
        
        [tracks_array addObject:data];
    }
    
    
    if ([way_str isKindOfClass:[NSArray class]])
    {
        NSArray *places_is = [[dict valueForKey:@"gpx"] valueForKey:@"wpt"];
        [way_array addObjectsFromArray:places_is];
        
    }
    else if ([way_str isKindOfClass:[NSDictionary class]]){
        
        NSDictionary *data = [[dict valueForKey:@"gpx"] valueForKey:@"wpt"];
        
        [way_array addObject:data];
    }
    
    
    //    Waypoints
    for (int q=0; q<way_array.count; q++) {
        NSString *waypoint_lat_str = [NSString stringWithFormat:@"%@",[[way_array objectAtIndex:q] valueForKey:@"lat"]];
        NSString *watpoint_lon_str = [NSString stringWithFormat:@"%@",[[way_array objectAtIndex:q] valueForKey:@"lon"]];
        NSString *watpoint_title_str = [NSString stringWithFormat:@"%@",[[[way_array objectAtIndex:q] valueForKey:@"name"] valueForKey:@"text"]];
        
        double y = [waypoint_lat_str doubleValue];
        double e = [watpoint_lon_str doubleValue];
        GMSMarker *start_marker = [[GMSMarker alloc] init];
        start_marker.position = CLLocationCoordinate2DMake(y,e);
        start_marker.appearAnimation = kGMSMarkerAnimationPop;
        start_marker.title=watpoint_title_str;
        start_marker.icon = [UIImage imageNamed:@"default_marker"];
        start_marker.map = ride_mapView;
      
    }
    
       //    Tracks
    
 
    for (int p=0; p<tracks_array.count; p++) {
        NSString *lat_str = [NSString stringWithFormat:@"%@",[[tracks_array objectAtIndex:p] valueForKey:@"lat"]];
        NSString *lon_str = [NSString stringWithFormat:@"%@",[[tracks_array objectAtIndex:p] valueForKey:@"lon"]];
        
        x = [lat_str doubleValue];
        z = [lon_str doubleValue];
        if (p==0) {
            GMSMarker *start_marker = [[GMSMarker alloc] init];
            start_marker.position = CLLocationCoordinate2DMake(x,z);
            start_marker.appearAnimation = kGMSMarkerAnimationPop;
            start_marker.title=Start_Point_Title;
            start_marker.icon = [UIImage imageNamed:@"red_marker.png"];
            start_marker.map = ride_mapView;


        }
        else if (p==tracks_array.count-1)
        {
            GMSMarker *end_marker = [[GMSMarker alloc] init];
            end_marker.position = CLLocationCoordinate2DMake(x,z);
            end_marker.appearAnimation = kGMSMarkerAnimationPop;
            end_marker.title=End_Point_title;
            end_marker.icon = [UIImage imageNamed:@"finish_marker.png"];
            end_marker.map = ride_mapView;

        }
       
       
        [line_path addCoordinate:CLLocationCoordinate2DMake(x, z)];
       
    }
    encoded_path=[line_path encodedPath];
    polyline = [GMSPolyline polylineWithPath:line_path];
    polyline.strokeColor = [UIColor yellowColor];
    polyline.strokeWidth = 5.0f;
    polyline.geodesic = YES;
    polyline.map = ride_mapView;
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithPath:line_path];
    [ride_mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds]];
    [line_path removeAllCoordinates];
    ride_mapView.userInteractionEnabled=YES;
   
//    [activeIndicatore stopAnimating];
          indicaterview.hidden=YES;
    [tracks_array removeAllObjects];
    [add_places removeAllObjects];
   // [way_array removeAllObjects];
          
      });
    });
    
}


- (IBAction)add_action:(id)sender {
}
- (IBAction)delete_ride_action:(id)sender {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Leave Ride"
                                  message:@"Are you sure you want to leave ride?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *Cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    [alert addAction:Cancel];
    
    UIAlertAction *delete = [UIAlertAction
                             actionWithTitle:@"Yes"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [self Delete_ride_request];
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    [alert addAction:delete];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)Delete_ride_request
{
//    Deleted Ride Api
    /*
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    activeIndicatore.hidesWhenStopped = TRUE;
    [activeIndicatore startAnimating];
    [self.view addSubview:activeIndicatore];
    
    
    
    [SHARED_API deleteRideRequestsWithParams:_scheduled_fullFilled_rideID_string user_ID:[Defaults valueForKey:User_ID] withSuccess:^(NSDictionary *response) {
        
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           
                           NSLog(@" Ride_Detail responce is : %@",response);
                           RidesController*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"RidesController"];
                           [self.navigationController pushViewController:controller animated:NO];
                           
                           if ([activeIndicatore isAnimating]) {
                               [activeIndicatore stopAnimating];
                               [activeIndicatore removeFromSuperview];
                           }
                           
                       });
        
    } onfailure:^(NSError *theError) {
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           
                           [SHARED_HELPER showAlert:ServiceFail];
                           
                           if ([activeIndicatore isAnimating]) {
                               [activeIndicatore stopAnimating];
                               [activeIndicatore removeFromSuperview];
                           }
                       });
    }];
     */
    
//    http://54.70.46.133/api/rides/reject_accepted_ride
    
    
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
  
//    [activeIndicatore startAnimating];
    indicaterview.hidden=NO;
 
    NSMutableDictionary *dict = [NSMutableDictionary new];
    
    [dict setObject:_scheduled_fullFilled_rideID_string forKey:@"ride_id"];
    [dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
    
    NSLog(@"Leave ride %@",dict);

    [SHARED_API leave_ride_RequestsWithParams:dict withSuccess:^(NSDictionary *response) {
        
    
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           NSLog(@" Leave_ride responce is : %@",response);
                           RidesController*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"RidesController"];
                           [self.navigationController pushViewController:controller animated:NO];
                           
//                           if ([activeIndicatore isAnimating]) {
//                               [activeIndicatore stopAnimating];
//                               [activeIndicatore removeFromSuperview];
//                           }
                           indicaterview.hidden=YES;
//                           
                       });
        
    } onfailure:^(NSError *theError) {
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           
                           [SHARED_HELPER showAlert:ServiceFail];
                           
//                           if ([activeIndicatore isAnimating]) {
//                               [activeIndicatore stopAnimating];
//                               [activeIndicatore removeFromSuperview];
//                           }
                           indicaterview.hidden=YES;
                       });
    }];
  
    
}
- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate

{
    RideMapViewController *map_cntrl = [self.storyboard instantiateViewControllerWithIdentifier:@"RideMapViewController"];
    map_cntrl.gpx_file = gpx_strng;
    map_cntrl.encoded_string=encoded_path;
    map_cntrl.way_points_array=way_array;
    [self.navigationController pushViewController:map_cntrl animated:YES];
    
}
- (IBAction)onClick_rideDetail_rideGoingBtn:(id)sender {
    
    self.rideDetail_rideGoingBtn.hidden=YES;
    self.rideDetail_scrollLabel.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }

}
@end
