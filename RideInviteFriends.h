//
//  RideInviteFriends.h
//  openroadrides
//
//  Created by apple on 03/07/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RideInvitesMyFrndsCell.h"
#import "RideInvitesPublicFrndsCell.h"
#import "Constants.h"
#import "APIHelper.h"
#import "AppHelperClass.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "CBAutoScrollLabel.h"
@interface RideInviteFriends : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UIScrollViewDelegate>
{
    UIActivityIndicatorView  *activeIndicatore;
    UIView *indicaterview;
    UILabel *NO_DATA;
    NSString *view_Status_rideInvite_frnds,*rideInvites_my_Friends_Called,*rideinvites_Public_Friends_Called,*rideInvites_friendID,*rideInvites_myFrnd_FriendID,*accept_reject_string,*addingPublicFrndIdString;
    NSMutableArray *arrayOfRideInvitePublicFriends,*arrayOfRideInviteMyFriends,*rideInvitefilteredArray,*rideInvitefilteredArray2,*arrayOfSelectedPublicFrndsID,*arrayOfSelectedMyFrndsID;
    BOOL rideInvite_isFiltered,boolValueForselectPublicFrnds;
    
}
@property (weak, nonatomic) NSString *isFrom,*group_id;
@property (weak, nonatomic) IBOutlet UIView *view_rideinvite_frnds_segment;
@property (weak, nonatomic) IBOutlet UIView *subView_segment_rideInvite_Public_frnd;
@property (weak, nonatomic) IBOutlet UIView *subView_segment_rideInvite_myFrnds;
@property (weak, nonatomic) IBOutlet UILabel *label_segemt_rideinvite_public_frnds;
@property (weak, nonatomic) IBOutlet UIButton *rideinvite_segment_public_frnds_btn;
- (IBAction)onClick_rideinvite_public_frnds_btn:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *label_segment_rideinvite_mtFrnds;
@property (weak, nonatomic) IBOutlet UIButton *rideinvite_segment_myFrnds_btn;
- (IBAction)onClick_rideInvites_myFrnds_btn:(id)sender;
@property (weak, nonatomic) IBOutlet UISearchBar *rideInvites_searchBar;
@property (weak, nonatomic) IBOutlet UIScrollView *rideInvites_ScrollView;
@property (weak, nonatomic) IBOutlet UIView *rideInvites_contentView;
@property (weak, nonatomic) IBOutlet UIView *rideInvites_Public_frnds_contentView;
@property (weak, nonatomic) IBOutlet UIView *rideInvites_myFrnds_contentView;
@property (weak, nonatomic) IBOutlet UITableView *rideInvites_public_frnds_tableView;
@property (weak, nonatomic) IBOutlet UITableView *rideInvites_myFrnds_tableView;
- (IBAction)onClick_rideinvites_myFrnds_checkBox_btn:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthConstraint_rideinviteFrnds_contentView;
- (IBAction)onClick_rideInvites_public_frnds_checkBox_btn:(UIButton *)sender;
@property UIRefreshControl *rideInvite_refreshControl,*rideInvite_refreshControl2;
@property (weak, nonatomic) IBOutlet UILabel *noData_label_public;
@property (weak, nonatomic) IBOutlet UILabel *noData_label_myfrnds;
@property (weak, nonatomic) IBOutlet UIButton *rideInviteFrnds_rideGoingBtn;
- (IBAction)onClick_rideInviteFrnds_rideGoingBtn:(id)sender;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *rideInviteFrnds_scrollLabel;

@end
