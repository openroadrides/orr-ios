//
//  ConnectSellerView.m
//  openroadrides
//
//  Created by apple on 03/10/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "ConnectSellerView.h"
#import "BIZPopupViewController.h"
#import "BIZPopupViewControllerDelegate.h"
@interface ConnectSellerView ()<BIZPopupViewControllerDelegate>
{
    BIZPopupViewController *popupViewController;
}

@end

@implementation ConnectSellerView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appDelegate.pop_UP_out_Side_Click_Hide=@"NO";
    [self.mobileNumberTF setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    self.mobileNumberTF.delegate=self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == _mobileNumberTF)
    {
        NSCharacterSet *numSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789-"];
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSInteger charCount = [newString length];
        if (charCount>12) {
            return NO;
        }
        
        //new
        NSString *stringWithoutSpaces = [newString
                                         stringByReplacingOccurrencesOfString:@"-" withString:@""];
        NSInteger newcharCount = [stringWithoutSpaces length];
        if (newcharCount>10) {
            stringWithoutSpaces = [stringWithoutSpaces substringToIndex:[stringWithoutSpaces length]-1];
            NSMutableString *mu = [NSMutableString stringWithString:stringWithoutSpaces];
            [mu insertString:@"-" atIndex:3];
            [mu insertString:@"-" atIndex:7];
            textField.text = mu;
            return NO;
            
        }
        else if (newcharCount==10)
        {
            NSMutableString *mu = [NSMutableString stringWithString:stringWithoutSpaces];
            [mu insertString:@"-" atIndex:3];
            [mu insertString:@"-" atIndex:7];
            textField.text = mu;
            return NO;
        }
        //
        
        
        
        
        
        if (charCount == 3 || charCount == 7) {
            if ([string isEqualToString:@""]){
                return YES;
            }else{
                newString = [newString stringByAppendingString:@"-"];
            }
        }
        
        if (charCount == 4 || charCount == 8) {
            if (![string isEqualToString:@"-"]){
                newString = [newString substringToIndex:[newString length]-1];
                newString = [newString stringByAppendingString:@"-"];
            }
        }
        
        if ([newString rangeOfCharacterFromSet:[numSet invertedSet]].location != NSNotFound
            || [string rangeOfString:@"-"].location != NSNotFound
            || charCount > 12) {
            return NO;
        }
        
        textField.text = newString;
        return NO;
    }
    return YES;
}

-(void)showAlert:(NSString *)message
{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:message  preferredStyle:UIAlertControllerStyleActionSheet];
    UIView  *firstSubview = alert.view.subviews.firstObject;
    firstSubview.backgroundColor = APP_YELLOW_COLOR;
    UIView *alertContentView = firstSubview.subviews.firstObject;
    alertContentView.backgroundColor = APP_YELLOW_COLOR;
    alertContentView.tintColor = [UIColor blueColor];
    for (UIView *subSubView in alertContentView.subviews) {
        
        subSubView.backgroundColor = APP_YELLOW_COLOR;
    }
    // [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alert animated:YES completion:nil];
    [self presentViewController:alert animated:YES completion:nil];
    
    int duration = 2; // duration in seconds
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        
        [alert dismissViewControllerAnimated:YES completion:nil];
        
    });
    
    
}

- (IBAction)onClick_sendBtn_action:(id)sender {
     _mobileNumberTF.text=[_mobileNumberTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (_mobileNumberTF.text.length ==0) {
        [self showAlert:ConnectMobileNumber];
    }
    else if (_mobileNumberTF.text.length !=12 && _mobileNumberTF.text.length !=0)
    {
        [self showAlert:MobileText];
    }
    else
    {
        [self marketPlaceIntrestedMethod];
        self.sendBtn.userInteractionEnabled=NO;
    }

}
//"{
//""post_id"":"""",
//""post_interested_user_id"":"""",
//""status"":""interested"",
//""phone_number"":""""
//}"
-(void)marketPlaceIntrestedMethod
{
    
    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    activeIndicatore.hidesWhenStopped = TRUE;
    [activeIndicatore startAnimating];
    [self.view addSubview:activeIndicatore];
    
    
    NSMutableDictionary *intrested_dict = [NSMutableDictionary new];
    
    [intrested_dict setObject:_mobileNumberTF.text forKey:@"phone_number"];
    [intrested_dict setObject:_intrestedPostIdString forKey:@"post_id"];
    [intrested_dict setObject:[Defaults valueForKey:User_ID] forKey:@"post_interested_user_id"];
    [intrested_dict setObject:@"interested" forKey:@"status"];
    
    
    NSLog(@"intrested dict %@",intrested_dict);
    [SHARED_API marketPlace_intrested_RequestsWithParams:intrested_dict withSuccess:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           NSLog(@"Intrested Market Place Response : %@",response);
                           
                          
                           if([[response objectForKey:STATUS] isEqualToString:SUCCESS])
                           {
                               
                               if ([activeIndicatore isAnimating]) {
                                   [activeIndicatore stopAnimating];
                                   [activeIndicatore removeFromSuperview];
                               }
                               
                              
                               popupViewController = [[BIZPopupViewController alloc]init];
                               popupViewController.delegate=self;
//                               [self showAlert:@"Your interest on this post is submitted."];
                               [self dismissViewControllerAnimated:YES completion:nil];
                              appDelegate.pop_UP_out_Side_Click_Hide=@"YES";
                               
                               [[NSNotificationCenter defaultCenter] postNotificationName:@"intrestdMarketplace_notification" object:self];

                           }
                           else{
                               if ([activeIndicatore isAnimating]) {
                                   [activeIndicatore stopAnimating];
                                   [activeIndicatore removeFromSuperview];
                               }
                               
                               [self showAlert:Forgotfail];
                           }
                           
                       });
        
    } onfailure:^(NSError *theError) {
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           
                           if ([activeIndicatore isAnimating]) {
                               [activeIndicatore stopAnimating];
                               [activeIndicatore removeFromSuperview];
                           }
                           
                           [self showAlert:ServiceFail];
                           
                           
                       });
    }];
}
//-(void)showAlert:(NSString *)message
//{
//    
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:message  preferredStyle:UIAlertControllerStyleActionSheet];
//    UIView  *firstSubview = alert.view.subviews.firstObject;
//    firstSubview.backgroundColor = APP_YELLOW_COLOR;
//    UIView *alertContentView = firstSubview.subviews.firstObject;
//    alertContentView.backgroundColor = APP_YELLOW_COLOR;
//    alertContentView.tintColor = [UIColor blueColor];
//    for (UIView *subSubView in alertContentView.subviews) {
//        
//        subSubView.backgroundColor = APP_YELLOW_COLOR;
//    }
//    // [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alert animated:YES completion:nil];
//    [self presentViewController:alert animated:YES completion:nil];
//    
//    int duration = 2; // duration in seconds
//    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
//        
//        [alert dismissViewControllerAnimated:YES completion:nil];
//        
//    });
//}

- (IBAction)onClick_intrestd_closeBtn:(id)sender {
//    popupViewController = [[BIZPopupViewController alloc]init];
//    popupViewController.delegate=self;
//    [self dismissViewControllerAnimated:YES completion:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
