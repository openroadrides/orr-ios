//
//  MarketPlaceDetail.m
//  openroadrides
//
//  Created by apple on 28/09/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "MarketPlaceDetail.h"

@interface MarketPlaceDetail ()

@end

@implementation MarketPlaceDetail

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"POST";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    
    self.marketPlaceDetail_scrollLabel.hidden=YES;
    self.marketPlaceDetail_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.marketPlaceDetail_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.marketPlaceDetail_scrollLabel.textColor = [UIColor blackColor];
    self.marketPlaceDetail_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.marketPlaceDetail_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.marketPlaceDetail_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.marketPlaceDetail_scrollLabel.scrollSpeed = 60; // pixels per second
    self.marketPlaceDetail_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.marketPlaceDetail_scrollLabel.fadeLength = 0.f;
    self.marketPlaceDetail_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.marketPlaceDetail_scrollLabel observeApplicationNotifications];
    
    
    self.marketPlaceDetail_rideGoinBtn.hidden=YES;
    self.bottomConstraint_intrestedBtn.constant=-30;
    if (appDelegate.ridedashboard_home) {
        self.marketPlaceDetail_scrollLabel.hidden=NO;
        self.bottomConstraint_intrestedBtn.constant=0;
        self.marketPlaceDetail_rideGoinBtn.hidden=NO;
    }

    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [leftBtn addTarget:self action:@selector(back_Action) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.frame = CGRectMake(0, 0, 25, 25);
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"back_Image"] forState:UIControlStateNormal];
    UIBarButtonItem *leftBackButton=[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:leftBackButton, nil];
    self.intrestedBtn.hidden=YES;
    
    self.contentView.hidden=YES;
    [self marketPlaceDetailMethod];
    
    _marketProfieImageView.layer.cornerRadius=_marketProfieImageView.frame.size.width/2;
    _marketProfieImageView.layer.cornerRadius=_marketProfieImageView.frame.size.height/2;
    _marketProfieImageView.clipsToBounds=YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(intrestdMarketplace_notification) name:@"intrestdMarketplace_notification" object:nil];
   
}
-(void)back_Action{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)intrestdMarketplace_notification
{
    
    self.intrestedBtn.hidden=YES;
    [SHARED_HELPER showAlert:@"Your interest on this post is submitted."];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)marketPlaceDetailMethod
{
  
        if (![SHARED_HELPER checkIfisInternetAvailable]) {
            [SHARED_HELPER showAlert:Nonetwork];
            return;
        }
    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    activeIndicatore.hidesWhenStopped = TRUE;
    [self.view addSubview:activeIndicatore];
        [activeIndicatore startAnimating];
        //    [SHARED_API myPosts_Request_List:[Defaults valueForKey:User_ID] withSuccess:^(NSDictionary *response) {
    [SHARED_API getMarketPlace_Details_Request:_marketPlaceId user_ID:[Defaults valueForKey:User_ID] withSuccess:^(NSDictionary *response) {
            dispatch_async(dispatch_get_main_queue(), ^
                           {
                               NSLog(@"Market Place Detail  Response %@",response);
                               if([[response objectForKey:STATUS] isEqualToString:SUCCESS])
                               {
                                   marketPlaceDetailDict=[response valueForKey:@"marketplace_details"];
                                   
                                   NSString*userNameStr=[NSString stringWithFormat:@"%@",[marketPlaceDetailDict valueForKey:@"name"]];
                                   if ([userNameStr isEqualToString:@"<null>"] || [userNameStr isEqualToString:@""] || [userNameStr isEqualToString:@"null"] || userNameStr == nil) {
                                       
                                       userNameStr=@"";
                                       self.userNameLabel.text=userNameStr;
                                   }
                                   else{
                                       self.userNameLabel.text=userNameStr;
                                   }
                                   
                                   
                                   NSString*PostedByStr=[NSString stringWithFormat:@"%@",[marketPlaceDetailDict valueForKey:@"created_date_time"]];
                                   if ([PostedByStr isEqualToString:@"<null>"] || [PostedByStr isEqualToString:@""] || [PostedByStr isEqualToString:@"null"] || PostedByStr == nil) {
                                       
                                       PostedByStr=@"";
                                       self.postedLabel.text=PostedByStr;
                                   }
                                   else{
                                       
                                       NSDateFormatter *TZ_dateFormatter = [[NSDateFormatter alloc] init];
                                       [TZ_dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                                       [TZ_dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
                                       NSDate *TZ_date = [TZ_dateFormatter dateFromString:PostedByStr]; // creat
                                       [TZ_dateFormatter setDateFormat:@"MM/dd/yyyy"];
                                       [TZ_dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
                                       NSString *PostedByStr = [TZ_dateFormatter stringFromDate:TZ_date];
                                       
                                       NSString *curent_date=[TZ_dateFormatter stringFromDate:[NSDate date]];
                                       if ([curent_date compare:PostedByStr]==NSOrderedSame) {
//                                           [TZ_dateFormatter setDateFormat:@"HH:mm"];
                                           [TZ_dateFormatter setDateFormat:@"hh:mm a"];
                                           [TZ_dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
                                           PostedByStr = [TZ_dateFormatter stringFromDate:TZ_date];
                                       }
                                       self.postedLabel.text=[NSString stringWithFormat:@"Posted %@",PostedByStr];
                                   }
                                   
                                   self.priceLabel.text=[NSString stringWithFormat:@" $ %@",[marketPlaceDetailDict valueForKey:@"post_price"]];
                                   
                                   
                                   NSString*PriceCostStr=[NSString stringWithFormat:@"%@",[marketPlaceDetailDict valueForKey:@"post_price"]];
                                   if ([PriceCostStr isEqualToString:@"<null>"] || [PriceCostStr isEqualToString:@""] || [PriceCostStr isEqualToString:@"null"] || PriceCostStr == nil || [PriceCostStr isEqualToString:@"0.00"] || [PriceCostStr isEqualToString:@"0"]) {
                                       
                                       PriceCostStr=@"";
                                       self.priceLabel.text=PriceCostStr;
                                   }
                                   else{
                                       self.priceLabel.text=[NSString stringWithFormat:@" $ %@",PriceCostStr];
                                   }

                                   
                                   
                                  NSString*titleNameStr=[NSString stringWithFormat:@"%@",[marketPlaceDetailDict valueForKey:@"post_title"]];
                                   if ([titleNameStr isEqualToString:@"<null>"] || [titleNameStr isEqualToString:@""] || [titleNameStr isEqualToString:@"null"] || titleNameStr == nil) {
                                       
                                       titleNameStr=@"";
                                       _titleLabel.text=titleNameStr;
                                   }
                                   else{
                                       _titleLabel.text=titleNameStr;
                                   }

                                   
                                   NSString *item_image_string1 = [NSString stringWithFormat:@"%@", [marketPlaceDetailDict valueForKey:@"profile_image"]];
                                   
                                   if ([item_image_string1 isEqual:[NSNull null]] || [item_image_string1 isEqualToString:@""] || item_image_string1 == nil|| [item_image_string1 isEqualToString:@"<null>"])
                                   {
                                       item_image_string1=@"";
                                   }
                                   else
                                   {
                                       _marketProfieImageView.contentMode= UIViewContentModeScaleToFill;
                                       NSURL*url=[NSURL URLWithString:item_image_string1];
                                       [self.marketProfieImageView sd_setImageWithURL:url
                                                                     placeholderImage:[UIImage imageNamed:@"user_Placeholder"]
                                                                              options:SDWebImageRefreshCached];
                                       _marketProfieImageView.layer.borderWidth=2.0f;
                                       _marketProfieImageView.layer.borderColor=[UIColor whiteColor].CGColor;
                                       _marketProfieImageView.clipsToBounds = YES;
                                   }
                                  
                                   
                                   NSString * areaName=[NSString stringWithFormat:@"%@",[marketPlaceDetailDict valueForKey:@"post_address"]];
                                   NSString * cityName=[NSString stringWithFormat:@"%@",[marketPlaceDetailDict valueForKey:@"city"]];
                                   NSString * stateName=[NSString stringWithFormat:@"%@",[marketPlaceDetailDict valueForKey:@"state"]];
                                   NSString *zipCode=[NSString stringWithFormat:@"%@",[marketPlaceDetailDict valueForKey:@"zipcode"]];
                                   
                                   NSMutableArray *empaty_array_search_public=[[NSMutableArray alloc]init];
                                   [empaty_array_search_public addObject:@"<null>"];
                                   [empaty_array_search_public addObject:@"null"];
                                   [empaty_array_search_public addObject:@""];
                                   [empaty_array_search_public addObject:@"(null)"];
                                   [empaty_array_search_public addObject:@"0"];
                                   
                                   NSMutableArray *Data_array_search_public=[[NSMutableArray alloc]init];
                                   
                                   
                                   if ([empaty_array_search_public containsObject:areaName]) {
                                       
                                   }
                                   else{
                                       
                                       [Data_array_search_public addObject:areaName];
                                   }
                                   if ([empaty_array_search_public containsObject:cityName]) {
                                       
                                   }
                                   else{
                                       [Data_array_search_public addObject:cityName];
                                   }
                                   
                                   if ([empaty_array_search_public containsObject:stateName]) {
                                       
                                   }
                                   else{
                                       [Data_array_search_public addObject:stateName];
                                   }
                                   
                                   if ([empaty_array_search_public containsObject:zipCode]) {
                                       
                                   }
                                   else{
                                       [Data_array_search_public addObject:zipCode];
                                   }
                                   NSMutableString *add_data_search_public=[[NSMutableString alloc]init];
                                   if (Data_array_search_public.count>0) {
                                       self.address_imageView.image=[UIImage imageNamed:@"check_in_placeholder"];
                                       
                                       for (int i=0; i<Data_array_search_public.count; i++) {
                                           
                                           NSString *adress=[Data_array_search_public objectAtIndex:i];
                                           if (i==0) {
                                               [add_data_search_public appendFormat:@"%@", [NSString stringWithFormat:@"%@",adress]];
                                           }
                                           else
                                           {
                                               int zip=[[Data_array_search_public objectAtIndex:i] intValue];
                                               if (zip>0) {
                                                   [add_data_search_public appendFormat:@"%@", [NSString stringWithFormat:@" %@",adress]];
                                               }
                                               else
                                                   [add_data_search_public appendFormat:@"%@", [NSString stringWithFormat:@", %@",adress]];
                                           }
                                           
                                       }
                                       self.addressLabel.text=add_data_search_public;
                                   }
                                   else{
                                       self.address_imageView.image=[UIImage imageNamed:@""];
                                       self.addressLabel.text=@"";
                                       
                                       
                                   }
                                   NSString*contactNumStr=[NSString stringWithFormat:@"%@",[marketPlaceDetailDict valueForKey:@"phone_number"]];
                                   if ([contactNumStr isEqualToString:@"<null>"] || [contactNumStr isEqualToString:@""] || [contactNumStr isEqualToString:@"null"] || contactNumStr == nil) {
                                       
                                       contactNumStr=@"";
                                       self.phoneNumberImageView.hidden=YES;
                                       _phoneNumberLabel.text=contactNumStr;
                                   }
                                   else{
                                        self.phoneNumberImageView.hidden=NO;
                                       _phoneNumberLabel.text=contactNumStr;
                                   }
                                   NSString*contactEmailStr=[NSString stringWithFormat:@"%@",[marketPlaceDetailDict valueForKey:@"email"]];
                                   if ([contactEmailStr isEqualToString:@"<null>"] || [contactEmailStr isEqualToString:@""] || [contactEmailStr isEqualToString:@"null"] || contactEmailStr == nil) {
                                       
                                       contactEmailStr=@"";
                                       self.emailImageView.hidden=YES;
                                       _emailLabel.text=contactEmailStr;
                                   }
                                   else{
                                       _emailLabel.text=contactEmailStr;
                                       self.emailImageView.hidden=NO;
                                   }

                                   NSString*descStr=[NSString stringWithFormat:@"%@",[marketPlaceDetailDict valueForKey:@"description"]];
                                   if ([descStr isEqualToString:@"<null>"] || [descStr isEqualToString:@""] || [descStr isEqualToString:@"null"] || descStr == nil) {
                                       
                                       descStr=@"";
                                       _descriptionLabel.text=descStr;
                                   }
                                   else{
                                       _descriptionLabel.text=descStr;
                                   }
                                   NSString*catTypeStr=[NSString stringWithFormat:@"%@",[marketPlaceDetailDict valueForKey:@"cat_name"]];
                                   if ([catTypeStr isEqualToString:@"<null>"] || [catTypeStr isEqualToString:@""] || [catTypeStr isEqualToString:@"null"] || catTypeStr == nil) {
                                       
                                       catTypeStr=@"";
                                       _categoryTypeLabel.text=catTypeStr;
                                   }
                                   else{
                                       _categoryTypeLabel.text=catTypeStr;
                                   }

                                   
                                   NSString*intrestedStatusStr=[NSString stringWithFormat:@"%@",[marketPlaceDetailDict valueForKey:@"interested_status"]];
                                   if ([intrestedStatusStr isEqualToString:@"<null>"] || [intrestedStatusStr isEqualToString:@""] || [intrestedStatusStr isEqualToString:@"null"] || intrestedStatusStr == nil) {
                                       
                                       intrestedStatusStr=@"";
                                       
                                   }
                                   else{
                                       
                                       if ([intrestedStatusStr isEqualToString:@"0"]) {
                                           self.intrestedBtn.hidden=NO;
                                       }
                                       else if ([intrestedStatusStr isEqualToString:@"1"])
                                       {
                                           self.intrestedBtn.hidden=YES;
                                       }
                                       
                                   }
                                   _multipleImages_scrollView.pagingEnabled = YES;
                                   marketImagesArray=[[NSMutableArray alloc]init];
                                   imagearayy=[marketPlaceDetailDict valueForKey:@"images"];
                                   
                                   NSString*imageStr=[marketPlaceDetailDict valueForKey:@"post_image"];;
                                   [marketImagesArray addObject:imageStr];
                                   
                                   if (imagearayy.count!=0) {
                                       
                                       for (int j=0; j<imagearayy.count; j++)
                                       {
                                           
                                           NSString*imageStr=[[imagearayy objectAtIndex:j] valueForKey:@"post_image_names"];
                                           
                                           
                                           [marketImagesArray addObject:imageStr];
                                       }
                                   
                                   }
                                  
                                   if ((marketImagesArray.count!=0))
                                   {
                                       for (int i=0; i<marketImagesArray.count; i++)
                                       {
                                           UIImageView *imagviews=[[UIImageView alloc] initWithFrame:CGRectMake(i*self.multipleImages_scrollView.frame.size.width, 0, self.multipleImages_scrollView.frame.size.width, self.multipleImages_scrollView.frame.size.height)];
                                           [imagviews setImageWithURL:[NSURL URLWithString:[marketImagesArray objectAtIndex:i]]placeholderImage:[UIImage imageNamed:@"add_places_placeholder"]];
                                           imagviews.contentMode = UIViewContentModeScaleAspectFit;
                                           [self.multipleImages_scrollView addSubview:imagviews];
                                           
                                           UIView  * transparentview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, imagviews.frame.size.width, imagviews.frame.size.height)];
                                           transparentview.backgroundColor = [UIColor blackColor];
                                           transparentview.alpha = 0.1;
                                           [imagviews addSubview:transparentview];
                                           
                                       }
                                       self.multipleImages_scrollView.contentSize=CGSizeMake(marketImagesArray.count*self.multipleImages_scrollView.frame.size.width, self.multipleImages_scrollView.frame.size.height);
                                       self.multipleImages_scrollView.showsHorizontalScrollIndicator = NO;
                                       self.multipleImages_scrollView.delegate=self;
                                       if ([marketImagesArray count] > 1)
                                       {
                                           pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(10, self.multipleImages_scrollView.frame.size.height-12, self.multipleImages_scrollView.frame.size.width, 10)];
                                           
                                           pageControl.currentPageIndicatorTintColor =APP_YELLOW_COLOR;
                                           pageControl.pageIndicatorTintColor =[UIColor whiteColor] ;
                                           pageControl.numberOfPages = marketImagesArray.count;
                                           [_contentView addSubview:pageControl];
                                       }
                                   }
                                   else
                                   {
                                       UIImageView *imagviews=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.multipleImages_scrollView.frame.size.width, self.multipleImages_scrollView.frame.size.height)];
                                       
                                       [imagviews setImageWithURL:[NSURL URLWithString:[marketPlaceDetailDict valueForKey:@"post_image"]] placeholderImage:[UIImage imageNamed:@"add_places_placeholder"]];
                                       imagviews.contentMode = UIViewContentModeScaleAspectFit;
                                       [self.multipleImages_scrollView addSubview:imagviews];
                                       UIView  * transparentview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, imagviews.frame.size.width, imagviews.frame.size.height)];
                                       transparentview.backgroundColor = [UIColor blackColor];
                                       transparentview.alpha = 0.1;
                                       [imagviews addSubview:transparentview];
                                       
                                   }

                                   self.contentView.hidden=NO;

                                   
                                   [activeIndicatore stopAnimating];
                                  
                               }
                               else if([[response objectForKey:STATUS] isEqualToString:FAIL])
                                   
                               {
                                   
                                   self.contentView.hidden=YES;
                                    [activeIndicatore stopAnimating];
                                    [SHARED_HELPER showAlert:Forgotfail];
                               }
                               
                           });
            
            
            
        } onfailure:^(NSError *theError) {
            self.contentView.hidden=YES;
            [activeIndicatore stopAnimating];
            [SHARED_HELPER showAlert:ServerConnection];
        }];
   

}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)sender
{
    if (sender==_multipleImages_scrollView)
    {
        CGFloat pageWidth = _multipleImages_scrollView.frame.size.width;
        NSInteger offsetLooping = 1;
        int ppage = floor((_multipleImages_scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + offsetLooping;
        pageControl.currentPage = ppage %marketImagesArray.count;
        //        pageControl.currentPage = ppage %4;
        
    }
}

- (IBAction)onClickIntrestedBtn:(id)sender {
    if (IS_IPHONE_5_OR_LESS) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ConnectSellerView*seller = [storyboard instantiateViewControllerWithIdentifier:@"ConnectSellerView"];
        seller.intrestedPostIdString=_marketPlaceId;
        BIZPopupViewController *promotionsPopupView = [[BIZPopupViewController alloc] initWithContentViewController:seller contentSize:CGSizeMake(SCREEN_WIDTH-20,SCREEN_HEIGHT-200)];
        [self presentViewController:promotionsPopupView animated:NO completion:nil];
    }
    else
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ConnectSellerView*seller = [storyboard instantiateViewControllerWithIdentifier:@"ConnectSellerView"];
        seller.intrestedPostIdString=_marketPlaceId;
        BIZPopupViewController *promotionsPopupView = [[BIZPopupViewController alloc] initWithContentViewController:seller contentSize:CGSizeMake(SCREEN_WIDTH-20,SCREEN_HEIGHT-300)];
        [self presentViewController:promotionsPopupView animated:NO completion:nil];
    }
   
    
}
- (IBAction)onClick_marketPlaceDeatil_rideGoingBtn:(id)sender {
    
    self.marketPlaceDetail_scrollLabel.hidden=YES;
    self.marketPlaceDetail_rideGoinBtn.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }

}
@end
