//
//  Public_Route_Cell.h
//  openroadrides
//
//  Created by SrkIosEbiz on 30/05/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RatingBar.h"
#import "RateView.h"

@interface Public_Route_Cell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIView *back_Ground_view;
@property (strong, nonatomic) IBOutlet UILabel *title_Label;
@property (strong, nonatomic) IBOutlet RatingBar *rating_Bar_View;
@property (strong, nonatomic) IBOutlet UILabel *description_label;
@property (strong, nonatomic) IBOutlet UILabel *comments_Label;
@property (strong, nonatomic) IBOutlet UIImageView *des_icon;
@property (weak, nonatomic) IBOutlet UIButton *checkbox_btn_routes;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingConstraint_title_routes;

@end
