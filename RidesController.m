//
//  RidesController.m
//  openroadrides
//
//  Created by apple on 06/07/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "RidesController.h"
#import "CreaterideViewController.h"
#import "MPCoachMarks.h"

@interface RidesController ()
{
    UIBarButtonItem *item0;
    NSString *menu_Status;
    NSArray *menu_Items_Array;
    NSMutableArray *check_strings_array;
}
@end

@implementation RidesController

static CBAutoScrollLabel * extracted(RidesController *object) {
    return object.rides_scrolllLabel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
     menu_Status=@"LEFT";
    self.title = @"MY RIDES";
    appDelegate.Ride_Detail_Edited=@"";
     self.navigationController.navigationBar.hidden=NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    
    UIBarButtonItem *menuButton=[[UIBarButtonItem alloc]initWithImage:
                                 [[UIImage imageNamed:@"sidemenuicon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
                                                                style:UIBarButtonItemStylePlain target:self action:@selector(Menu_Action:)];
    
    self.navigationItem.leftBarButtonItem = menuButton;
    
    
//    UIBarButtonItem *add_btn=[[UIBarButtonItem alloc]initWithTitle:@"+" style:UIBarButtonItemStylePlain target:self action:@selector(addclicked)];
//    [add_btn setTitleTextAttributes:@{
//                                      NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:26.0],
//                                      NSForegroundColorAttributeName: [UIColor blackColor]
//                                      } forState:UIControlStateNormal];
//    self.navigationItem.rightBarButtonItem=add_btn;
    self.rides_scrolllLabel.hidden=YES;
    extracted(self).text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.rides_scrolllLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.rides_scrolllLabel.textColor = [UIColor blackColor];
    self.rides_scrolllLabel.backgroundColor=APP_YELLOW_COLOR;
    self.rides_scrolllLabel.labelSpacing = 30; // distance between start and end labels
    self.rides_scrolllLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.rides_scrolllLabel.scrollSpeed = 60; // pixels per second
    self.rides_scrolllLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.rides_scrolllLabel.fadeLength = 0.f;
    self.rides_scrolllLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.rides_scrolllLabel observeApplicationNotifications];

    self.rides_rideGoingBtn.hidden=YES;
    if (appDelegate.ridedashboard_home) {
        self.rides_rideGoingBtn.hidden=NO;
        self.rides_scrolllLabel.hidden=NO;
        self.bottomConstraint_ridesScrollView.constant=30;
    }
    UIButton *add_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [add_btn addTarget:self action:@selector(addclicked:) forControlEvents:UIControlEventTouchUpInside];
    add_btn.frame = CGRectMake(0, 0, 30, 30);
    [add_btn setBackgroundImage:[UIImage imageNamed:@"Add_plus"] forState:UIControlStateNormal];
    UIBarButtonItem * create_Ride= [[UIBarButtonItem alloc] initWithCustomView:add_btn];
    self.navigationItem.rightBarButtonItems=[NSArray arrayWithObjects:create_Ride, nil];
    
    CGFloat content_View_Width=SCREEN_WIDTH-20;
    self.widthConstraint_rides_contentView.constant=content_View_Width*2;
    [self.view layoutIfNeeded];
    
    
    //Side Menu
    
    swipe_right=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(menuright)];
    swipe_right.direction=UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipe_right];
    
    
    swipe_left=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(menuleft)];
    swipe_left.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipe_left];

    
    
    self.User_image.layer.cornerRadius=self.User_image.frame.size.width/2;
    self.User_image.clipsToBounds=YES;
    self.User_image.layer.borderColor = [UIColor whiteColor].CGColor;
    self.User_image.layer.borderWidth = 4.0;
//   menu_Items_Array=@[@"Dashboard",@"Rides",@"Routes",@"Events",@"News",@"Friends",@"Groups",@"Find Riders",@"Privacy Policy",@"Terms & Conditions",@"Logout"];
    menu_Items_Array=@[@"Home",@"Dashboard",@"Privacy Policy",@"Terms & Conditions",@"Logout"];
    _side_Menu_Table.delegate=self;
    _side_Menu_Table.dataSource=self;
    [_side_Menu_Table reloadData];
    self.User_Name.text=[NSString stringWithFormat:@"%@",[Defaults valueForKey:@"UserName"]];
    //
    
    
    [self.scheduled_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.fullFilled_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
   
    indicaterview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    activeIndicatore.backgroundColor=[UIColor clearColor];
    activeIndicatore = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.frame = CGRectMake(0.0, 0.0, 80, 80);
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    [indicaterview addSubview:activeIndicatore];
    [self.view addSubview:indicaterview];
    [indicaterview bringSubviewToFront:self.view];
    [activeIndicatore startAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    indicaterview.hidden=YES;

    
    
    
    
    view_Status_secheduledAndFullFilled=@"";
    
    self.scheduled_tableView.dataSource=self;
    self.scheduled_tableView.delegate=self;
    self.fullFilled_tableView.dataSource=self;
    self.fullFilled_tableView.delegate=self;
    self.noData_label_scheduled.hidden=YES;
    self.noData_label_fullFilled.hidden=YES;
    
    
//        self.scheduled_refreshControl = [[UIRefreshControl alloc]init];
//        self.scheduled_refreshControl.tintColor=APP_YELLOW_COLOR;
//    
//        self.scheduled_refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
//        [self.scheduled_refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
//        [self.scheduled_tableView addSubview:self.scheduled_refreshControl];
//    
//        self.fullfilled_refreshControl = [[UIRefreshControl alloc]init];
//        self.fullfilled_refreshControl.tintColor=APP_YELLOW_COLOR;
//    
//    
//        self.fullfilled_refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
//        [self.fullfilled_refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
//        [self.fullFilled_tableView addSubview:self.fullfilled_refreshControl];
    
    check_strings_array=[[NSMutableArray alloc]init];
    [check_strings_array addObject:@"<null>"];
    [check_strings_array addObject:@"null"];
    [check_strings_array addObject:@""];
    [check_strings_array addObject:@"(null)"];
    
    
    if ([_is_For_Only isEqualToString:@"My_Rides"]) {
        self.rides_scrollView.scrollEnabled=NO;
        self.segment_Top.constant=-49;
        self.rides_segement_view.hidden=YES;
        [self.view layoutIfNeeded];
        
        [self onClick_fullFilled_segment_btn:0];
    }
    else if ([_is_For_Only isEqualToString:@"Scheduled_Rides"])
    {
        self.title = @"SCHEDULED RIDES";
        self.rides_scrollView.scrollEnabled=NO;
        self.segment_Top.constant=-49;
        self.rides_segement_view.hidden=YES;
        [self.view layoutIfNeeded];
        [self onClick_scheduled_segment_btn:0];
    }
    else
    {
         [self get_scheduledAndFullfilledRides_Data];
    }
   
}
-(void)viewWillAppear:(BOOL)animated
{
     if ([appDelegate.Ride_Detail_Edited isEqualToString:@"YES"]) {
          if ([view_Status_secheduledAndFullFilled isEqualToString:@""]||[view_Status_secheduledAndFullFilled isEqualToString:@"PUBLIC"]) {
                [self get_scheduledAndFullfilledRides_Data];
          }
         else
         {
             [self get_scheduledAndFullfilledRides_Data];
         }
     }
    
    _side_Menu_view.hidden=YES;
    [self.navigationController.navigationBar setHidden:NO];
     [[self navigationController] setNavigationBarHidden:NO animated:NO];
    [self display_user_Profile];
    
    //Display Annotation
    // Show coach marks
    BOOL coachMarksShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"MPCoachMarksShown_MyRides"];
    if (coachMarksShown == NO) {
        // Don't show again
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MPCoachMarksShown_MyRides"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        // Show coach marks
        [self showAnnotation];
    }
}

#pragma mark - Annotations
-(void)showAnnotation
{
    [self.view layoutIfNeeded];
    
    
    // Setup coach marks
    CGRect coachmark1 = CGRectMake (5,20, self.navigationController.navigationBar.frame.size.height,self.navigationController.navigationBar.frame.size.height);
    CGRect coachmark2 = CGRectMake( ([UIScreen mainScreen].bounds.size.width - 53), 20, self.navigationController.navigationBar.frame.size.height, self.navigationController.navigationBar.frame.size.height);
    
    
    // Setup coach marks
    NSArray *coachMarks = @[
                            @{
                                @"rect": [NSValue valueWithCGRect:coachmark1],
                                @"caption": @"Menu",
                                @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                                @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                                @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_LEFT]
                                //@"showArrow":[NSNumber numberWithBool:YES]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:coachmark2],
                                @"caption": @"Add New ride",
                                @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                                @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                                @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_RIGHT],
                                //@"showArrow":[NSNumber numberWithBool:YES]
                                },
                            ];
    
    
    MPCoachMarks *coachMarksView = [[MPCoachMarks alloc] initWithFrame:self.navigationController.view.bounds coachMarks:coachMarks];
    [self.navigationController.view addSubview:coachMarksView];
    //[[UIApplication sharedApplication].keyWindow addSubview:coachMarksView];
    [coachMarksView start];
    
}

-(void)display_user_Profile{
    
    NSString *user_id=[Defaults valueForKey:User_ID];
    [SHARED_API DisplayUserProfile:user_id withSuccess:^(NSDictionary *response) {
        
        [self user_Profile_Data_Sucsess:response];
        
    } onfailure:^(NSError *theError) {
        
    }];
    
}
-(void)user_Profile_Data_Sucsess:(NSDictionary *)Profile_data{
    if ([[Profile_data valueForKey:STATUS] isEqualToString:SUCCESS]) {
        NSDictionary *user_data=[Profile_data valueForKey:@"data"] ;
        self.User_Name.text=[user_data valueForKey:@"name"];
        [Defaults setObject:self.User_Name.text forKey:@"UserName"];
        
        NSString *profile_image_str=[NSString stringWithFormat:@"%@",[user_data valueForKey:@"profile_image"]];
        
        if (![check_strings_array containsObject:profile_image_str] && ![profile_image_str isEqualToString:@"<null>"])
        {
            NSURL*url=[NSURL URLWithString:profile_image_str];
            [self.User_image sd_setImageWithURL:url
                               placeholderImage:[UIImage imageNamed:@"user_Placeholder"]
                                        options:SDWebImageRefreshCached];
        }
         friends_count = [[NSString stringWithFormat:@"%@",[[Profile_data valueForKey:@"data"] valueForKey:@"total_friends"]] intValue];
       
        
         groups_count = [[NSString stringWithFormat:@"%@",[[Profile_data valueForKey:@"data"] valueForKey:@"total_groups"]] intValue];
       
        [_side_Menu_Table reloadData];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)back{
    
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)addclicked:(UIButton *)sender{
    
    CreaterideViewController *createride = [self.storyboard instantiateViewControllerWithIdentifier:@"CreaterideViewController"];
    [self.navigationController pushViewController:createride animated:YES];

    
}
- (void)refreshTable {
    //TODO: refresh your data
    if([view_Status_secheduledAndFullFilled isEqualToString:@""]||[view_Status_secheduledAndFullFilled isEqualToString:@"PUBLIC"])
    {
        select_Scheduled_Called=@"YES";
        [self.scheduled_refreshControl endRefreshing];
        
        arrayOfScheduledRides=nil;
        arrayOfScheduledRides=[[NSMutableArray alloc]init];
        [self.scheduled_tableView reloadData];
        [self get_scheduledAndFullfilledRides_Data];
    }
    else{
        select_fullFilled_Called=@"YES";
        arratOfFullfilledRides=nil;
        arratOfFullfilledRides=[[NSMutableArray alloc]init];
        
        [self.fullFilled_tableView reloadData];
        [self.fullfilled_refreshControl endRefreshing];
        [self get_scheduledAndFullfilledRides_Data];
        
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onClick_scheduled_segment_btn:(id)sender {
    
    view_Status_secheduledAndFullFilled=@"PUBLIC";
    self.scheduled_segment_view.backgroundColor=[UIColor whiteColor];
    self.fullfilled_segment_view.backgroundColor=[UIColor blackColor];
    [self.rides_scrollView setContentOffset:CGPointMake((0), _rides_scrollView.contentOffset.y)];
    self.scheduled_segment_label.textColor=[UIColor whiteColor];
    self.fullFilled_segement_label.textColor=APP_YELLOW_COLOR;
    if ([select_Scheduled_Called isEqualToString:@"YES"]) {
        
    }
    else{
        
        [self get_scheduledAndFullfilledRides_Data];
    }

}
- (IBAction)onClick_fullFilled_segment_btn:(id)sender {
    
    view_Status_secheduledAndFullFilled=@"MY";
    self.scheduled_segment_view.backgroundColor=[UIColor blackColor];
    self.fullfilled_segment_view.backgroundColor=[UIColor whiteColor];
    [self.rides_scrollView setContentOffset:CGPointMake((SCREEN_WIDTH-20), self.rides_scrollView.contentOffset.y)];
    self.fullFilled_segement_label.textColor=[UIColor whiteColor];
    self.scheduled_segment_label.textColor=APP_YELLOW_COLOR;
    
    if ([select_fullFilled_Called isEqualToString:@"YES"]) {
        
    }
    else{
        
        //  [self.my_Routes_Table reloadData];
        [self get_scheduledAndFullfilledRides_Data];
    }

}


#pragma mark - ScrollView
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView==_scheduled_tableView||scrollView==_fullFilled_tableView) {
        self.rides_segement_view.userInteractionEnabled=NO;
      
    }
    else if (scrollView.tag==5)
    {
        
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    self.rides_segement_view.userInteractionEnabled=YES;
   
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    _rides_segement_view.userInteractionEnabled=YES;
    if (scrollView.tag==5) {
       /* if (scrollView.contentOffset.x==0) {
            [self onClick_scheduled_segment_btn:0];
        }
        if (scrollView.contentOffset.x==SCREEN_WIDTH-20) {
            [self onClick_fullFilled_segment_btn:0];
        }*/
    }
}
-(void)get_scheduledAndFullfilledRides_Data
{
    
    indicaterview.hidden=NO;
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [SHARED_HELPER showAlert:Nonetwork];
        indicaterview.hidden=YES;
        return;
    }
    NSString *user_id=[Defaults valueForKey:User_ID];
//    NSString *user_id=@"61";
    NSString *scheduledAndfullfilled_service_is;
    if ([view_Status_secheduledAndFullFilled isEqualToString:@""]||[view_Status_secheduledAndFullFilled isEqualToString:@"PUBLIC"]) {
        scheduledAndfullfilled_service_is=WS_SCHEDULERIDE_REQUEST;
        arrayOfScheduledRides=[[NSMutableArray alloc]init];
        
    }
    else{
        scheduledAndfullfilled_service_is=WS_FULLFILLEDRIDE_REQUEST;
        arratOfFullfilledRides=[[NSMutableArray alloc]init];
    }
    
    [SHARED_API myRidesRequest:user_id scheduled_fullFilled:scheduledAndfullfilled_service_is withSuccess:^(NSDictionary *response) {
        NSLog(@"%@",response);
        [self scheduled_fullFilled_Service_Sucsess:response];
        indicaterview.hidden=YES;
        
        
    } onfailure:^(NSError *theError) {
        [SHARED_HELPER showAlert:ServiceFail];
        indicaterview.hidden=YES;
        
    }];
 
    
}
-(void)scheduled_fullFilled_Service_Sucsess:(NSDictionary *)respnse{
    if ([[respnse valueForKey:STATUS] isEqualToString:SUCCESS]) {
        NSArray *rides_data=[respnse valueForKey:@"rides"];
        if (rides_data.count>0) {
            for (int i=0; i<rides_data.count; i++) {
                NSDictionary *data_Dict_is=[rides_data objectAtIndex:i];
                //                NSString *route_Privacy=[data_Dict_is valueForKey:@"friends"];
                //                if ([route_Privacy caseInsensitiveCompare:@"public"]== NSOrderedSame&&([view_Status isEqualToString:@""]||[view_Status isEqualToString:@"PUBLIC"])) {
                if ([view_Status_secheduledAndFullFilled isEqualToString:@""]||[view_Status_secheduledAndFullFilled isEqualToString:@"PUBLIC"]) {
                     [arrayOfScheduledRides addObject:data_Dict_is];
                }
                
                else {
                     [arratOfFullfilledRides addObject:data_Dict_is];
                }
                
            }
            
            self.rides_scrollView.hidden=NO;
            if ([view_Status_secheduledAndFullFilled isEqualToString:@""]||[view_Status_secheduledAndFullFilled isEqualToString:@"PUBLIC"]) {
                view_Status_secheduledAndFullFilled=@"PUBLIC";
                select_Scheduled_Called=@"YES";
                //                _searchBar.hidden=NO;
                [self.scheduled_tableView reloadData];
                self.noData_label_scheduled.hidden=YES;
            }
            else{
                [self.fullFilled_tableView reloadData];
                select_fullFilled_Called=@"YES";
                self.noData_label_fullFilled.hidden=YES;
            }
        }
        else{
            
            
            
//            [SHARED_HELPER showAlert:NO_Friends];
                       //            NO_DATA = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.public_friends_tableView.bounds.size.width, self.public_friends_tableView.bounds.size.height)];
            //                        NO_DATA.numberOfLines=2;
            //                        NO_DATA.textColor        = APP_YELLOW_COLOR;
            //                        NO_DATA.textAlignment    = NSTextAlignmentCenter;
            //                        NO_DATA.text  = @"No Friends found";
            //                        NO_DATA.font=[UIFont fontWithName:@"Antonio-Bold" size:20];
            //                        self.my_friends_tableView.backgroundView = NO_DATA;
            //                        self.my_friends_tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            
            [self service_fail_OR_EmptyData];
        }
    }
    else{
        [SHARED_HELPER showAlert:NORIDES];
        [self service_fail_OR_EmptyData];
    }

    
}
-(void)service_fail_OR_EmptyData{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        //        [self.navigationController popViewControllerAnimated:YES];
        if ([view_Status_secheduledAndFullFilled isEqualToString:@""]||[view_Status_secheduledAndFullFilled isEqualToString:@"PUBLIC"]) {
            view_Status_secheduledAndFullFilled=@"PUBLIC";
            select_Scheduled_Called=@"YES";
            self.noData_label_scheduled.hidden=NO;
            //                _searchBar.hidden=NO;
            
        }
        else{
            self.noData_label_fullFilled.hidden=NO;
            select_fullFilled_Called=@"YES";
        }
        
    });
}
#pragma mark - TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==_side_Menu_Table) {
        return menu_Items_Array.count;
    }
   
    if ([view_Status_secheduledAndFullFilled isEqualToString:@"PUBLIC"]) {
        
        if ([arrayOfScheduledRides count]>0)
        {
            return arrayOfScheduledRides.count;
        }
//        else{
//            self.noData_label_scheduled.hidden=NO;
//        }
        
    }
    else if ([view_Status_secheduledAndFullFilled isEqualToString:@"MY"])
    {
        
        
        if ([arratOfFullfilledRides count]>0)
        {
            return arratOfFullfilledRides.count;
        }
//        else{
//            self.noData_label_fullFilled.hidden=NO;
//        }
        
    }
    else{
        
        return 0;
    }
    return 0;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_side_Menu_Table) {
        return 45;
    }
    if (tableView ==_scheduled_tableView) {
        float height=150;
       
        NSString*addressString=[NSString stringWithFormat:@"%@",[[arrayOfScheduledRides objectAtIndex:indexPath.row] objectForKey:@"ride_start_address"]];
        NSString*descriptionString=[NSString stringWithFormat:@"%@",[[arrayOfScheduledRides objectAtIndex:indexPath.row] objectForKey:@"ride_description"]];
//        Address
        if ([addressString isEqualToString:@"<null>"] || [addressString isEqualToString:@""] || [addressString isEqualToString:@"null"] || addressString == nil) {
            height =height-30;
           
        }
        
        if ([descriptionString isEqualToString:@"<null>"] || [descriptionString isEqualToString:@""] || [descriptionString isEqualToString:@"null"] || descriptionString == nil) {
            height=height-44;
        }
        
        return height;
        }
    else
    {
        float height1=150;
        
        NSString*addressString=[NSString stringWithFormat:@"%@",[[arratOfFullfilledRides objectAtIndex:indexPath.row] objectForKey:@"ride_start_address"]];
        NSString*descriptionString=[NSString stringWithFormat:@"%@",[[arratOfFullfilledRides objectAtIndex:indexPath.row] objectForKey:@"ride_description"]];
        //        Address
        if ([addressString isEqualToString:@"<null>"] || [addressString isEqualToString:@""] || [addressString isEqualToString:@"null"] || addressString == nil) {
            height1 =height1-30;
            
        }
        
        if ([descriptionString isEqualToString:@"<null>"] || [descriptionString isEqualToString:@""] || [descriptionString isEqualToString:@"null"] || descriptionString == nil) {
            height1=height1-44;
        }
        
        return height1;
        
    }
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_side_Menu_Table)
    {
        static NSString *simpleTableIdentifier = @"MenuTableViewCell";
        MenuTableViewCell *cell = (MenuTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        self.side_Menu_Table.separatorStyle=UITableViewCellSeparatorStyleNone;
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MenuTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.menu_Item_Label.text=[menu_Items_Array objectAtIndex:indexPath.row];
        cell.menu_Item_Label.textColor=[UIColor colorWithRed:255/255.0 green:222/255.0 blue:0/255.0 alpha:1.0];
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = [UIColor colorWithRed:255/255.0 green:222/255.0 blue:0/255.0 alpha:1.0];
        [cell setSelectedBackgroundView:bgColorView];
        cell.menu_Item_Label.highlightedTextColor=[UIColor blackColor];
        
        tableView.allowsMultipleSelection=NO;
        
        
        cell.menu_item_Count_Lab.hidden=YES;
        if (indexPath.row==0)
        {
            cell.menu_Item_iCon.image=[UIImage imageNamed:@"homeicon"];
            
        }
        else if (indexPath.row==1)
        {
            cell.menu_Item_iCon.image=[UIImage imageNamed:@"Dashboard_blue"];
        }
        else if (indexPath.row==2)
            
        {
            cell.menu_Item_iCon.image=[UIImage imageNamed:@"privacy"];
        }
        else if (indexPath.row==3)
            
        {
            cell.menu_Item_iCon.image=[UIImage imageNamed:@"terms"];
        }
        else if (indexPath.row==4)
            
        {
            cell.menu_Item_iCon.image=[UIImage imageNamed:@"Logout"];
        }
//        else if (indexPath.row==5)
//            
//        {
//            cell.menu_Item_iCon.image=[UIImage imageNamed:@"Friends_icon_Blue"];
//            cell.menu_item_Count_Lab.hidden=NO;
//           cell.menu_item_Count_Lab.text=[NSString stringWithFormat:@"%d",friends_count];
//        }
//        else if (indexPath.row==6)
//            
//        {
//            cell.menu_Item_iCon.image=[UIImage imageNamed:@"Groups_Icon_Blue"];
//            cell.menu_item_Count_Lab.hidden=NO;
//            cell.menu_item_Count_Lab.text=[NSString stringWithFormat:@"%d",groups_count];
//        }
//        
//        else if (indexPath.row==7)
//        {
//            cell.menu_Item_iCon.image=[UIImage imageNamed:@"browse_Search"];
//        }
//        else if (indexPath.row==8)
//        {
//            cell.menu_Item_iCon.image=[UIImage imageNamed:@"privacy"];
//        }
//        else if (indexPath.row==9)
//        {
//            cell.menu_Item_iCon.image=[UIImage imageNamed:@"terms"];
//        }
//        
//        else if (indexPath.row==10)
//            
//        {
//            cell.menu_Item_iCon.image=[UIImage imageNamed:@"Logout"];
//        }
        
        return cell;
    }
  else if ([view_Status_secheduledAndFullFilled isEqualToString:@"PUBLIC"]) {
      
        ScheduledRidesCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
        
        if (cell == nil)
        {
            cell = [[ScheduledRidesCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        }
                    //            without Search
            cell.scheduledRidesCell_bgview.backgroundColor=ROUTES_CELL_BG_COLOUR1;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSString*titleString=[NSString stringWithFormat:@"%@",[[arrayOfScheduledRides objectAtIndex:indexPath.row] objectForKey:@"ride_title"]];
        if ([titleString isEqualToString:@"<null>"] || [titleString isEqualToString:@""] || [titleString isEqualToString:@"null"] || titleString == nil) {
            cell.scheduled_ridetitle.text=@"Title";
            
        }
        
        else{
            
            cell.scheduled_ridetitle.text=[[arrayOfScheduledRides objectAtIndex:indexPath.row] objectForKey:@"ride_title"];
        }
      NSString*descriptionString=[NSString stringWithFormat:@"%@",[[arrayOfScheduledRides objectAtIndex:indexPath.row] objectForKey:@"ride_description"]];
        NSString*addressString=[NSString stringWithFormat:@"%@",[[arrayOfScheduledRides objectAtIndex:indexPath.row] objectForKey:@"ride_start_address"]];
        if ([addressString isEqualToString:@"<null>"] || [addressString isEqualToString:@""] || [addressString isEqualToString:@"null"] || addressString == nil) {
            
            cell.addressView.hidden=YES;
            cell.scheduled_ride_address.text=@"";
            cell.dateAndTimeViewTopConstraint.constant=-30;
            
        }
        
        else{
            
            cell.addressView.hidden=NO;
            cell.dateAndTimeViewTopConstraint.constant=0;
            cell.scheduled_ride_address.text=[[arrayOfScheduledRides objectAtIndex:indexPath.row] objectForKey:@"ride_start_address"];

        }

        NSString*startDate=[NSString stringWithFormat:@"%@",[[arrayOfScheduledRides objectAtIndex:indexPath.row] objectForKey:@"ride_start_date"]];
        if ([startDate isEqualToString:@"<null>"] || [startDate isEqualToString:@""] || [startDate isEqualToString:@"null"] || startDate == nil) {
            cell.scheduled_ride_date.text=@"date";
            
            
        }
        
        else{
            
            NSString*startDate_string=[NSString stringWithFormat:@"%@",[[arrayOfScheduledRides objectAtIndex:indexPath.row] objectForKey:@"ride_start_date"]];
            NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
            [dateFormatter1 setDateFormat:@"yyyy-MM-dd"];
            NSDate *date1 = [dateFormatter1 dateFromString: startDate_string];
            dateFormatter1 = [[NSDateFormatter alloc] init];
            [dateFormatter1 setDateFormat:@"MMM dd,yyyy"];// here set format
            NSString *convertedString1 = [dateFormatter1 stringFromDate:date1];
            NSLog(@"Converted String : %@",convertedString1);
            cell.scheduled_ride_date.text=[NSString stringWithFormat:@"%@",convertedString1];
            
        }
      
      NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
      [formatter setLocale:[NSLocale currentLocale]];
      [formatter setDateStyle:NSDateFormatterNoStyle];
      [formatter setTimeZone:[NSTimeZone localTimeZone]];
      [formatter setTimeStyle:NSDateFormatterLongStyle];
      NSString *dateString = [formatter stringFromDate:[NSDate date]];
      NSRange amRange = [dateString rangeOfString:[formatter AMSymbol]];
      NSRange pmRange = [dateString rangeOfString:[formatter PMSymbol]];
      is24h = (amRange.location == NSNotFound && pmRange.location == NSNotFound);
      NSLog(@"%@\n",(is24h ? @"YES" : @"NO"));

        NSString*startTime=[NSString stringWithFormat:@"%@",[[arrayOfScheduledRides objectAtIndex:indexPath.row] objectForKey:@"ride_start_time"]];
        if ([startTime isEqualToString:@"<null>"] || [startTime isEqualToString:@""] || [startTime isEqualToString:@"null"] || startTime == nil) {
            cell.scheduled_ride_time.text=@"time";
            
        }
        
        else{
            
            if (is24h==YES) {
                NSDateFormatter *dateFormatter4 = [[NSDateFormatter alloc] init] ;
                [dateFormatter4 setDateFormat:@"HH:mm:ss"];
                NSDate *start_time = [dateFormatter4 dateFromString:startTime];
                // [dateFormatter4 setDateFormat:@"HH:mm"];
                NSString *starttime = [dateFormatter4 stringFromDate:start_time];
                cell.scheduled_ride_time.text=[NSString stringWithFormat:@"%@",starttime];

            }
            else if(is24h==NO)
            {
                NSDateFormatter *dateFormatter4 = [[NSDateFormatter alloc] init] ;
                [dateFormatter4 setDateFormat:@"HH:mm:ss"];
                NSDate *start_time = [dateFormatter4 dateFromString:startTime];
                // [dateFormatter4 setDateFormat:@"HH:mm"];
                [dateFormatter4 setDateFormat:@"hh:mm a"];
                NSString *starttime = [dateFormatter4 stringFromDate:start_time];
                cell.scheduled_ride_time.text=[NSString stringWithFormat:@"%@",starttime];

            }
            
//            NSDateFormatter *dateFormatter4 = [[NSDateFormatter alloc] init] ;
//            [dateFormatter4 setDateFormat:@"HH:mm:ss"];
//            NSDate *start_time = [dateFormatter4 dateFromString:startTime];
//            // [dateFormatter4 setDateFormat:@"HH:mm"];
//            [dateFormatter4 setDateFormat:@"hh:mm a"];
//            NSString *starttime = [dateFormatter4 stringFromDate:start_time];
//            cell.scheduled_ride_time.text=[NSString stringWithFormat:@"%@",starttime];
//            cell.scheduled_ride_time.text=[[arrayOfScheduledRides objectAtIndex:indexPath.row] objectForKey:@"ride_start_time"];
        }

      
        if ([descriptionString isEqualToString:@"<null>"] || [descriptionString isEqualToString:@""] || [descriptionString isEqualToString:@"null"] || descriptionString == nil) {
            cell.scheduled_ride_description.text=@"";
            cell.dateAndTimeLineImage.hidden=YES;
            cell.descriptionView.hidden=YES;
            
        }
        
        else{
             cell.scheduled_ride_description.text=[[arrayOfScheduledRides objectAtIndex:indexPath.row] objectForKey:@"ride_description"];
            cell.descriptionView.hidden=NO;
            cell.dateAndTimeLineImage.hidden=NO;
            
        }
        
        return cell;
        
    }
    else
    {
        FullFilledRidesCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
        
        if (cell == nil)
        {
            cell = [[FullFilledRidesCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        }

            
            cell.fullFilledRidesCell_bgView.backgroundColor=ROUTES_CELL_BG_COLOUR1;
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            NSString*titleString1=[NSString stringWithFormat:@"%@",[[arratOfFullfilledRides objectAtIndex:indexPath.row] objectForKey:@"ride_title"]];
        
            if ([titleString1 isEqualToString:@"<null>"] || [titleString1 isEqualToString:@""] || [titleString1 isEqualToString:@"null"] || titleString1 == nil) {
                
                cell.fullFilled_ride_title.text=@"Title";
            }
            
            else{
                
                cell.fullFilled_ride_title.text=[[arratOfFullfilledRides objectAtIndex:indexPath.row] objectForKey:@"ride_title"];
            }
            NSString*addressString=[NSString stringWithFormat:@"%@",[[arratOfFullfilledRides objectAtIndex:indexPath.row] objectForKey:@"ride_start_address"]];
            if ([addressString isEqualToString:@"<null>"] || [addressString isEqualToString:@""] || [addressString isEqualToString:@"null"] || addressString == nil) {
                
                
                cell.fullFilled_addressView.hidden=YES;
                cell.fullFilledDateAndTimeViewTopConstraint.constant=-30;
            }
            
            else{
                cell.fullFilled_ride_address.text=[[arratOfFullfilledRides objectAtIndex:indexPath.row] objectForKey:@"ride_start_address"];
                cell.fullFilled_addressView.hidden=NO;
                cell.fullFilledDateAndTimeViewTopConstraint.constant=0;
            }
            
            NSString*startDate=[NSString stringWithFormat:@"%@",[[arratOfFullfilledRides objectAtIndex:indexPath.row] objectForKey:@"ride_start_date"]];
            if ([startDate isEqualToString:@"<null>"] || [startDate isEqualToString:@""] || [startDate isEqualToString:@"null"] || startDate == nil) {
                
                cell.fullfilled_ride_date.text=@"date";
            }
            
            else{
                
                NSString*fullFilled_startDate_string=[NSString stringWithFormat:@"%@",[[arratOfFullfilledRides objectAtIndex:indexPath.row] objectForKey:@"ride_start_date"]];
                NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
                [dateFormatter2 setDateFormat:@"yyyy-MM-dd"];
                NSDate *date2 = [dateFormatter2 dateFromString:fullFilled_startDate_string];
                dateFormatter2 = [[NSDateFormatter alloc] init];
                [dateFormatter2 setDateFormat:@"MMM dd,yyyy"];// here set format
                NSString *convertedString2 = [dateFormatter2 stringFromDate:date2];
                NSLog(@"Converted String : %@",convertedString2);
                cell.fullfilled_ride_date.text=[NSString stringWithFormat:@"%@",convertedString2];
            }
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setLocale:[NSLocale currentLocale]];
        [formatter setDateStyle:NSDateFormatterNoStyle];
        [formatter setTimeZone:[NSTimeZone localTimeZone]];
        [formatter setTimeStyle:NSDateFormatterLongStyle];
        NSString *dateString = [formatter stringFromDate:[NSDate date]];
        NSRange amRange = [dateString rangeOfString:[formatter AMSymbol]];
        NSRange pmRange = [dateString rangeOfString:[formatter PMSymbol]];
        is24h = (amRange.location == NSNotFound && pmRange.location == NSNotFound);
        NSLog(@"%@\n",(is24h ? @"YES" : @"NO"));
        
            
            NSString*startTime=[NSString stringWithFormat:@"%@",[[arratOfFullfilledRides objectAtIndex:indexPath.row] objectForKey:@"ride_start_time"]];
            if ([startTime isEqualToString:@"<null>"] || [startTime isEqualToString:@""] || [startTime isEqualToString:@"null"] || startTime == nil) {
                
                cell.fullfilled_ride_time.text=@"time";
            }
            
            else{
                if (is24h==YES) {
                    NSDateFormatter *dateFormatter4 = [[NSDateFormatter alloc] init] ;
                    [dateFormatter4 setDateFormat:@"HH:mm:ss"];
                    NSDate *start_time = [dateFormatter4 dateFromString:startTime];
                     [dateFormatter4 setDateFormat:@"HH:mm"];
//                    [dateFormatter4 setDateFormat:@"hh:mm a"];
                    NSString *starttime = [dateFormatter4 stringFromDate:start_time];
                    cell.fullfilled_ride_time.text=[NSString stringWithFormat:@"%@",starttime];
                }
                else if (is24h ==NO)
                {
                    NSDateFormatter *dateFormatter4 = [[NSDateFormatter alloc] init] ;
                    [dateFormatter4 setDateFormat:@"HH:mm:ss"];
                    NSDate *start_time = [dateFormatter4 dateFromString:startTime];
                    // [dateFormatter4 setDateFormat:@"HH:mm"];
                    [dateFormatter4 setDateFormat:@"hh:mm a"];
                    NSString *starttime = [dateFormatter4 stringFromDate:start_time];
                    cell.fullfilled_ride_time.text=[NSString stringWithFormat:@"%@",starttime];
                }
                
//                NSDateFormatter *dateFormatter4 = [[NSDateFormatter alloc] init] ;
//                [dateFormatter4 setDateFormat:@"HH:mm:ss"];
//                NSDate *start_time = [dateFormatter4 dateFromString:startTime];
//                // [dateFormatter4 setDateFormat:@"HH:mm"];
//                [dateFormatter4 setDateFormat:@"hh:mm a"];
//                NSString *starttime = [dateFormatter4 stringFromDate:start_time];
//                 cell.fullfilled_ride_time.text=[NSString stringWithFormat:@"%@",starttime];

//           
//            cell.fullfilled_ride_time.text=[NSString stringWithFormat:@"%@",[[arratOfFullfilledRides objectAtIndex:indexPath.row] objectForKey:@"ride_start_time"]];

            }
            
            NSString*descriptionString=[NSString stringWithFormat:@"%@",[[arratOfFullfilledRides objectAtIndex:indexPath.row] objectForKey:@"ride_description"]];
            if ([descriptionString isEqualToString:@"<null>"] || [descriptionString isEqualToString:@""] || [descriptionString isEqualToString:@"null"] || descriptionString == nil) {
                cell.fullfilled_ride_description.text=@"";
                cell.fullFilledDateAndTimeLineImageView.hidden=YES;
                cell.fullFilled_descriptionView.hidden=YES;
               
                
            }
            
            else{
                cell.fullfilled_ride_description.text=[[arratOfFullfilledRides objectAtIndex:indexPath.row] objectForKey:@"ride_description"];
                cell.fullFilledDateAndTimeLineImageView.hidden=NO;
                cell.fullFilled_descriptionView.hidden=NO;
                
                
            }
            
            return cell;
        }
    
//         return cell;
   
   
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_side_Menu_Table) {
//        if (indexPath.row==1) {
            [cell setSelected:NO animated:NO];
//        }
//        else{
//            [cell setSelected:NO animated:NO];
//        }
    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_side_Menu_Table) {
        if (indexPath.row==1)
        {
            
            DashboardViewController*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"DashboardViewController"];
            [self.navigationController pushViewController:controller animated:YES];
        }
        if (indexPath.row == 0)
        {
            HomeDashboardView*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"HomeDashboardView"];
            [self.navigationController pushViewController:controller animated:YES];
        }
        else if (indexPath.row == 2)
        {

            PrivacyPolicy*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"PrivacyPolicy"];
            [self.navigationController pushViewController:controller animated:YES];
        }
        else if (indexPath.row == 3)
        {

            TermsAndConditions *cntrl = [self.storyboard instantiateViewControllerWithIdentifier:@"TermsAndConditions"];
            [self.navigationController pushViewController:cntrl animated:YES];
        }
        else  if (indexPath.row == 4) {
            
            if (appDelegate.ridedashboard_home) {
                [self logoutRideGoingAlert:@"You cannot logout while the ride is going on."];
            }

            else
            {
                [self showpopup:Terminate];
            }
            [self.side_Menu_Table reloadData];
        }

        
         [self menuleft];
    }
    else{
          RideDetailViewController*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"RideDetailViewController"];
         if ([view_Status_secheduledAndFullFilled isEqualToString:@""]||[view_Status_secheduledAndFullFilled isEqualToString:@"PUBLIC"]) {
            
                appDelegate.select_Ride_ID_RideList=[NSString stringWithFormat:@"%@",[[arrayOfScheduledRides objectAtIndex:indexPath.row] objectForKey:@"ride_id"]];
             appDelegate.isFrom_ride_scheduled_Or_fullFilled=@"SCHEDULED";
             controller.ride_owner_id =[NSString stringWithFormat:@"%@",[[arrayOfScheduledRides objectAtIndex:indexPath.row] objectForKey:@"created_user_id"]];
            
         }
         else{
                appDelegate.select_Ride_ID_RideList=[NSString stringWithFormat:@"%@",[[arratOfFullfilledRides objectAtIndex:indexPath.row] objectForKey:@"ride_id"]];
             appDelegate.isFrom_ride_scheduled_Or_fullFilled=@"FUllFILLED";
             controller.ride_owner_id =[NSString stringWithFormat:@"%@",[[arratOfFullfilledRides objectAtIndex:indexPath.row] objectForKey:@"created_user_id"]];
         }
    
  
        
    controller.scheduled_fullFilled_rideID_string=appDelegate.select_Ride_ID_RideList;
    controller.scheduled_fullFilled_user_ride_data_string=appDelegate.isFrom_ride_scheduled_Or_fullFilled;
    [self.navigationController pushViewController:controller animated:YES];
    }
}
-(void)menuleft
{
    
    menu_Status=@"LEFT";
    [UIView animateWithDuration:0.5 delay:0.3 options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.menu_Leading_Constraint.constant=-300;
                         [self.view layoutIfNeeded];
                     } completion:^(BOOL finished) {
                         self.rides_Bg_View.alpha=1;
                         
                         self.rides_Bg_View.userInteractionEnabled=YES;
                     }];
}

-(void)menuright
{
  
    menu_Status=@"RIGHT";
    [UIView animateWithDuration:0.5 delay:0.3 options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.menu_Leading_Constraint.constant=0;
                         [self.view layoutIfNeeded];
                     } completion:^(BOOL finished)
     {
         
         self.rides_Bg_View.alpha=0.8;
         self.rides_Bg_View.userInteractionEnabled=NO;
     }];
}


- (IBAction)Menu_Action:(id)sender {
   _side_Menu_view.hidden=NO;
    if ([menu_Status isEqualToString:@"LEFT"]) {
        [self menuright];
       
    }
    else{
        [self menuleft];
       
    }
    
}
- (IBAction)Profile_Action:(id)sender {
    
    ProfileView*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileView"];
    [self.navigationController pushViewController:controller animated:YES];
    [self menuleft];
}
-(void)logoutRideGoingAlert:(NSString *)alert_msg
{
    UIAlertController *alertview = [UIAlertController alertControllerWithTitle:@"Can't Logout" message:alert_msg preferredStyle:UIAlertControllerStyleAlert];
    
    [alertview addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        
    }]];
    [self presentViewController:alertview animated:YES completion:nil];
}
-(void)showpopup:(NSString *)message
{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"LOGOUT"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                             NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                             NSData *uData = [NSKeyedArchiver archivedDataWithRootObject:@""];
                             [defaults setObject:uData forKey:User_ID];
                             NSString *user_data;
                             user_data = [defaults objectForKey:User_ID];
                             [defaults removeObjectForKey:User_ID];
                             [defaults removeObjectForKey:@"UserName"];
                             [[FBSDKLoginManager new] logOut];
                             [[GIDSignIn sharedInstance] signOut];
                             [defaults synchronize];
                             
                             LoginViewController *login = [self.storyboard  instantiateViewControllerWithIdentifier:@"LoginViewController"];
                             [self.navigationController pushViewController:login animated:YES];;
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"CANCEL"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}



- (IBAction)onClick_rides_rideGoingBtn:(id)sender {
    
    self.rides_rideGoingBtn.hidden=YES;
    self.rides_scrolllLabel.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }

}
@end
