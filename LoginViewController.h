//
//  LoginViewController.h
//  openroadrides
//
//  Created by apple on 01/05/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <Google/SignIn.h>
#import "Constants.h"
#import "APIHelper.h"
#import "Reachability.h"
#import "AppHelperClass.h"
#import "UserDetails.h"
#import "AppDelegate.h"
@interface LoginViewController : UIViewController<UITextFieldDelegate,GIDSignInUIDelegate,UIActionSheetDelegate>{
    BOOL Networkstatus;
     UIActivityIndicatorView  *activeIndicatore;
    UIView *indicaterview;
}
@property (weak, nonatomic) IBOutlet UITextField *Email_tf;
@property (weak, nonatomic) IBOutlet UITextField *Password_tf;
@property (strong, nonatomic) NSString *socialtype;
@property (nonatomic,strong)UserDetails *currentUserDetails;
@property (nonatomic,strong)NSData *uData;
@property (weak, nonatomic) IBOutlet UIScrollView *login_scrl_view;
@property (strong, nonatomic) NSString *userid;
- (IBAction)Login_click:(id)sender;
- (IBAction)Forgot_password_click:(id)sender;
- (IBAction)Google_click:(id)sender;
- (IBAction)Facebook_click:(id)sender;
- (IBAction)Createone_click:(id)sender;
-(void)googlesociallogin;

@end
