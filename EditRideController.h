//
//  EditRideController.h
//  openroadrides
//
//  Created by apple on 14/07/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "AppHelperClass.h"
#import "APIHelper.h"
#import "RoutesVC.h"
#import <GooglePlacePicker/GooglePlacePicker.h>
#import "SelectFriendsAndGroups.h"
#import "AppDelegate.h"
#import "CBAutoScrollLabel.h"
@interface EditRideController : UIViewController<GMSPlacePickerViewControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate>
{
    UIBarButtonItem *item0;
    UIDatePicker *date,*date1,*meeting_date,*start_time;
    double edit_start_latitude,edit_start_longitude,edit_meeting_latitude,edit_meeting_longitude;
    UIImage *cameraImage;
    NSString *base64String,*editRide_titleTF,*editRide_placeToStartTF,*editRide_startDateTF,*editRide_startTimeTF,*editRide_descriptionTF,*editRide_meetingAddress,*editRide_meetingDateTF,*editRide_meetingTimeTF,*random_number_string,*imageurlstrng,*route_id_str,*edit_ride_type,*edit_startTimeString,*edit_meetingTimeString;
    NSData *profileImgData;
    UIActivityIndicatorView *activeIndicatore;
    NSMutableArray *arrayOfRemoveFrndsIDs;
    
    NSString *selected_route;
    
}
@property(strong,nonatomic)NSDictionary *edit_Ride_Data;
@property(strong,nonatomic)NSString *ride_id;
@property (weak, nonatomic) IBOutlet UIScrollView *editRide_scrollView;
@property (weak, nonatomic) IBOutlet UIView *editRide_contentView;
@property (weak, nonatomic) IBOutlet UIImageView *editride_profile_imageView;
@property (weak, nonatomic) IBOutlet UIButton *editRide_profile_btn;
- (IBAction)onClick_editride_profile_btn:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *editRide_titleTF;
@property (weak, nonatomic) IBOutlet UITextField *editRide_placeToStartTF;
@property (weak, nonatomic) IBOutlet UITextField *editRide_startDateTF;
@property (weak, nonatomic) IBOutlet UITextField *editRide_startTimeTF;
@property (weak, nonatomic) IBOutlet UITextField *editRide_descriptionTF;
@property (weak, nonatomic) IBOutlet UIButton *editRide_selectRoute_btn;
- (IBAction)onClick_editRide_selectRoute_btn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *editRide_placeToStart_btn;
- (IBAction)onClick_editRide_placeTostart_btn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *editRide_inviteRiders_btn;
- (IBAction)onClick_editRide_inviteRiders_btn:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *editRide_meetingAddress;
@property (weak, nonatomic) IBOutlet UIButton *editride_meetingAddress_btn;
- (IBAction)onClick_editRide_meetingAddress_btn:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *editRide_meetingDateTF;
@property (weak, nonatomic) IBOutlet UITextField *editRide_meetingTimeTF;
@property (weak, nonatomic) NSString *edit_location_str_ride;
@property NSMutableArray *arrayOfFrndsInEditRide;
@property NSMutableArray *arrayOfgroupsInEditRide;
@property (weak, nonatomic) IBOutlet UIButton *editRide_rideGoingBtn;
- (IBAction)onClick_editride_rideGoingBtn:(id)sender;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *editRide_scrollLabel;
@end
