//
//  ScheduledRidesCell.h
//  openroadrides
//
//  Created by apple on 06/07/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScheduledRidesCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *scheduledRidesCell_bgview;

@property (weak, nonatomic) IBOutlet UILabel *scheduled_ridetitle;
@property (weak, nonatomic) IBOutlet UILabel *scheduled_ride_address;
@property (weak, nonatomic) IBOutlet UILabel *scheduled_ride_date;
@property (weak, nonatomic) IBOutlet UILabel *scheduled_ride_time;
@property (weak, nonatomic) IBOutlet UILabel *scheduled_ride_description;
@property (weak, nonatomic) IBOutlet UIImageView *scheduled_ride_address_image;
@property (weak, nonatomic) IBOutlet UIImageView *addressLineImage;
@property (weak, nonatomic) IBOutlet UIImageView *dateImage;
@property (weak, nonatomic) IBOutlet UIImageView *timeLineImage;
@property (weak, nonatomic) IBOutlet UIImageView *timeImage;
@property (weak, nonatomic) IBOutlet UIImageView *dateAndTimeLineImage;
@property (weak, nonatomic) IBOutlet UIImageView *descriptionImage;
@property (weak, nonatomic) IBOutlet UIView *addressView;
@property (weak, nonatomic) IBOutlet UIView *dateAndTimeView;
@property (weak, nonatomic) IBOutlet UIView *descriptionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *withAddressTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *withoutAddressTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *withoutDateAndTimeTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *withDateAndTimeTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dateAndTimeViewTopConstraint;
@end
