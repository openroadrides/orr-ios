//
//  CreateEventViewController.m
//  openroadrides
//
//  Created by apple on 16/06/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "CreateEventViewController.h"
#import "Constants.h"
#import "APIHelper.h"
#import "AppHelperClass.h"
#import "MPCoachMarks.h"
#define startlocation @"Please Select Your Start Location."
#define endlocation @"Please Select Your End Location."
@interface CreateEventViewController ()

@end

@implementation CreateEventViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    index = 1;
    imgIndex = 1;
    x=0;
    
    appDelegate.create_event_or_edit_event=@"NO";
    self.title = @"CREATE EVENT";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    
    UIButton *saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [saveBtn addTarget:self action:@selector(save_clicked) forControlEvents:UIControlEventTouchUpInside];
    saveBtn.frame = CGRectMake(0, 0, 30, 30);
    [saveBtn setBackgroundImage:[UIImage imageNamed:@"SAVE"] forState:UIControlStateNormal];
    UIBarButtonItem * create_Ride= [[UIBarButtonItem alloc] initWithCustomView:saveBtn];
    self.navigationItem.rightBarButtonItems=[NSArray arrayWithObjects:create_Ride, nil];
    
    
    self.createEvent_scrollLabel.hidden=YES;
    self.createEvent_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.createEvent_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.createEvent_scrollLabel.textColor = [UIColor blackColor];
    self.createEvent_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.createEvent_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.createEvent_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.createEvent_scrollLabel.scrollSpeed = 60; // pixels per second
    self.createEvent_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.createEvent_scrollLabel.fadeLength = 0.f;
    self.createEvent_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.createEvent_scrollLabel observeApplicationNotifications];
    
    self.createEvent_rideGoingBtn.hidden=YES;
    self.bottomConstraint_createEvent_image_baseView.constant=-32;
    if (appDelegate.ridedashboard_home) {
        self.createEvent_scrollLabel.hidden=NO;
         self.bottomConstraint_createEvent_image_baseView.constant=2;
        self.createEvent_rideGoingBtn.hidden=NO;
    }
//    UIBarButtonItem *cancle_btn=[[UIBarButtonItem alloc]initWithTitle:@"X" style:UIBarButtonItemStylePlain target:self action:@selector(cancle_clicked)];
//    [cancle_btn setTitleTextAttributes:@{
//                                         NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:26.0],
//                                         NSForegroundColorAttributeName: [UIColor blackColor]
//                                         } forState:UIControlStateNormal];
    
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [leftBtn addTarget:self action:@selector(cancle_clicked) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.frame = CGRectMake(0, 0, 25, 25);
    //    [leftBtn setBackgroundImage:[UIImage imageNamed:@"Finalback-arrow.png"] forState:UIControlStateNormal];
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"close_icon"] forState:UIControlStateNormal];
    UIBarButtonItem *leftBackButton=[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    item0=leftBackButton;
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:item0, nil];
    
//    self.navigationItem.leftBarButtonItem = cancle_btn;
    _upload_img_lbl.textColor = [UIColor whiteColor];
    [self.event_name_tf setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.start_location_tf setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.end_location_tf setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.description_tf setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.upload_img_tf setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.start_date_tf setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.start_time_tf setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.end_date_tf setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.end_time_tf setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.assosiated_ride_TF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.website_link setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    
    start_latitude=0;
    start_longitude=0;
    self.start_location_tf.userInteractionEnabled=NO;
    self.end_location_tf.userInteractionEnabled=NO;
    self.start_date_tf.tintColor  = [UIColor clearColor];
    self.end_date_tf.tintColor = [UIColor clearColor];
    self.start_time_tf.tintColor = [UIColor clearColor];
    self.end_time_tf.tintColor = [UIColor clearColor];
    self.end_location_tf.text=@"Charitable";
    
    
    img_array = [[NSMutableArray alloc]init];
    self.imges_Scrole_view.showsHorizontalScrollIndicator = NO;
    self.imges_Scrole_view.backgroundColor=APP_YELLOW_COLOR;
    add_btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 15, 90,90)];
    [add_btn addTarget:self action:@selector(add_click:) forControlEvents:UIControlEventTouchUpInside];
    //    add_btn.backgroundColor = [UIColor greenColor];
    [add_btn setBackgroundImage:[UIImage imageNamed:plus_icon] forState:UIControlStateNormal];
    [self.imges_Scrole_view addSubview:add_btn];
    
    
    activeIndicatore = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2-50, SCREEN_HEIGHT/2-100, 100, 100) ];
    activeIndicatore.activityIndicatorViewStyle=UIActivityIndicatorViewStyleWhiteLarge;
    activeIndicatore.color = APP_YELLOW_COLOR;
    activeIndicatore.hidesWhenStopped = TRUE;
    [self.view addSubview:activeIndicatore];
    
    //Display Annotation
    // Show coach marks
    BOOL coachMarksShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"MPCoachMarksShown_CreateEvent"];
    if (coachMarksShown == NO) {
        // Don't show again
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MPCoachMarksShown_CreateEvent"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self showAnnotation];
    }
    
   
    // Do any additional setup after loading the view.
}

#pragma mark - Annotations
-(void)showAnnotation
{
    [self.view layoutIfNeeded];
    
    // Setup coach marks
    CGRect coachmark1 = CGRectMake (7,20, self.navigationController.navigationBar.frame.size.height,self.navigationController.navigationBar.frame.size.height);
    CGRect coachmark2 = CGRectMake( ([UIScreen mainScreen].bounds.size.width - 53), 20, self.navigationController.navigationBar.frame.size.height, self.navigationController.navigationBar.frame.size.height);
    CGRect coachmark3 = CGRectMake(_images_Baseview.frame.origin.x, _images_Baseview.frame.origin.y+15, add_btn.frame.size.width, add_btn.frame.size.height);
    
    
    // Setup coach marks
    NSArray *coachMarks = @[
                            @{
                                @"rect": [NSValue valueWithCGRect:coachmark1],
                                @"caption": @"Tap to close the event",
                                @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                                @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                                @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_LEFT]
                                //@"showArrow":[NSNumber numberWithBool:YES]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:coachmark2],
                                @"caption": @"Tap to save the event",
                                @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                                @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                                @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_RIGHT],
                                //@"showArrow":[NSNumber numberWithBool:YES]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:coachmark3],
                                @"caption": @"Add the event image here",
                                @"shape": [NSNumber numberWithInteger:SHAPE_SQUARE],
                                @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
                                @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_CENTER]
                                //@"showArrow":[NSNumber numberWithBool:YES]
                                },
                            ];
    
    
    MPCoachMarks *coachMarksView = [[MPCoachMarks alloc] initWithFrame:self.navigationController.view.bounds coachMarks:coachMarks];
    //[self.navigationController.view addSubview:coachMarksView];
    [[UIApplication sharedApplication].keyWindow addSubview:coachMarksView];
    [coachMarksView start];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)add_click:(UIButton *)sender
{
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped do nothing.
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}
- (void) imagePickerController:(UIImagePickerController *)picker
         didFinishPickingImage:(UIImage *)image
                   editingInfo:(NSDictionary *)editingInfo
{
    //    img.image = [UIImage imageNamed:[array objectAtIndex:index]];
    UIImage *photoTaken = image;
    if (photoTaken)
    {
        
        if (index <=5)
        {
            if (index == 1) {
                x = 0;
            }
            baseView_placesImages = [[UIView alloc]initWithFrame:CGRectMake(x, 0, 150, 120)];
            baseView_placesImages.tag = 100+index;
            baseView_placesImages.backgroundColor = [UIColor clearColor];
            [self.imges_Scrole_view addSubview:baseView_placesImages];
            
            img = [[UIImageView alloc] initWithFrame:CGRectMake(0,15,90, 90)];
            img.image=photoTaken;
            img.contentMode=UIViewContentModeScaleAspectFit;
            
            //image path from gallery
//            CGFloat compression = 0.9f;
//            CGFloat maxCompression = 0.1f;
//            int maxFileSize = 250*1024;
//            NSData *imageData1 = UIImageJPEGRepresentation(img.image,compression);
//            
//            while ([imageData1 length] > maxFileSize && compression > maxCompression)
//            {
//                compression -= 0.1;
//                imageData1 = UIImageJPEGRepresentation(img.image,compression);
//            }
            NSData *imageData1=[self compressImage:photoTaken];
            base64String = [imageData1 base64EncodedStringWithOptions:0];
            [img_array addObject:base64String];
            
            imgIndex++;
            
            x = x+110;
            
            [baseView_placesImages addSubview:img];
            subtract_btn = [[UIButton alloc]initWithFrame:CGRectMake(img.frame.size.width-12.5, 0, 25, 25)];
            //            [subtract_btn setTitle:@"X" forState:UIControlStateNormal];
            //            [subtract_btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
            [subtract_btn setBackgroundImage:[UIImage imageNamed:@"wrong_icon.png"] forState:UIControlStateNormal];
            [subtract_btn.titleLabel setFont:[UIFont systemFontOfSize:20]];
            [subtract_btn addTarget:self action:@selector(sub_click:) forControlEvents:UIControlEventTouchUpInside];
            subtract_btn.tag=index;
            [baseView_placesImages addSubview:subtract_btn];
            
            
            add_btn.frame = CGRectMake(baseView_placesImages.frame.origin.x+baseView_placesImages.frame.size.width-40, img.frame.origin.y, 90, 90);
            self.imges_Scrole_view.contentSize = CGSizeMake(add_btn.frame.origin.x+add_btn.frame.size.width, 120);
            
            if (index == 5) {
                self.imges_Scrole_view.contentSize = CGSizeMake(add_btn.frame.origin.x-5, 120);
                add_btn.hidden = YES;
            }
            
            index++;
            
        }
        
        
    }
    else
    {
        NSLog(@"Selected photo is NULL");
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}
-(NSData *)compressImage:(UIImage *)image{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    //    float maxHeight = 1130.0f;
    float maxHeight = 816.0f;
    float maxWidth = 640.0f;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.9;//50 percent compression
    NSData *imageData = UIImageJPEGRepresentation(image, compressionQuality);
    while (actualHeight > maxHeight || actualWidth > maxWidth){
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
        CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
        UIGraphicsBeginImageContext(rect.size);
        [image drawInRect:rect];
        UIImage *img_is = UIGraphicsGetImageFromCurrentImageContext();
        imageData = UIImageJPEGRepresentation(img_is, compressionQuality);
        UIGraphicsEndImageContext();
        
    }
    //        else{
    //        actualHeight = maxHeight;
    //        actualWidth = maxWidth;
    //        compressionQuality = 1;
    //    }
    
    
    return imageData;
}

-(IBAction)sub_click:(UIButton *)sender
{
    
    UIView *remove_baseView_placesImages = (UIView *)[self.imges_Scrole_view viewWithTag:sender.tag+100];
    [remove_baseView_placesImages removeFromSuperview];
    
    int xCoordinate = remove_baseView_placesImages.frame.origin.x;
    
    if (sender.tag < index-1)
    {
        for (int i = (int)sender.tag+1; i<= index-1 ; i++) {
            
            UIView *moveForward_placesImages = (UIView *)[self.imges_Scrole_view viewWithTag:i+100];
            moveForward_placesImages.frame = CGRectMake(xCoordinate, 0, 150, self.imges_Scrole_view.frame.size.height);
            UIButton *buttonTag = (UIButton *)[moveForward_placesImages viewWithTag:(int)moveForward_placesImages.tag-100];
            buttonTag.tag = buttonTag.tag-1;
            moveForward_placesImages.tag = 100+i-1;
            xCoordinate = xCoordinate+110;
            
        }
        
        index --;
        
        x = xCoordinate;
        
        add_btn.hidden = NO;
        add_btn.frame = CGRectMake(xCoordinate, img.frame.origin.y, 90, 90);
        self.imges_Scrole_view.contentSize = CGSizeMake(add_btn.frame.origin.x + add_btn.frame.size.width, 120);
        
    }
    else
    {
        x = x-110;
        index = (int)sender.tag;
        
        add_btn.frame = CGRectMake(remove_baseView_placesImages.frame.origin.x, img.frame.origin.y, 90, 90);
        self.imges_Scrole_view.contentSize = CGSizeMake(add_btn.frame.origin.x + add_btn.frame.size.width, 120);
        
    }
    
    [img_array removeObjectAtIndex:(int)sender.tag-1];
    
    if (sender.tag == 5) {
        
        add_btn.hidden = NO;
        add_btn.frame = CGRectMake(remove_baseView_placesImages.frame.origin.x, img.frame.origin.y, 90, 90);
        self.imges_Scrole_view.contentSize = CGSizeMake(add_btn.frame.origin.x + add_btn.frame.size.width, 120);
    }
    
}





-(void)create_Event_service{
    
//    "user_id":"",
//    "request_from":"web / app",
//    "event_name":"",
//    "event_type":"charitable/normal",
//    "event_description":"",
//    "associated_ride":"",
//    "link":"",
//    "event_start_date":"",
//    "event_end_date":"",
//    "event_start_time":"",
//    "event_end_time":"",
//    "event_starting_location_address":"",
//    "event_starting_location_latitude":"",
//    "event_starting_location_longitude":"",
//    "event_ending_location_address":"",
//    "event_ending_location_latitude":"",
//    "event_ending_location_longitude":"",
//    "event_image_names":[""],
    
    
    [activeIndicatore startAnimating];
    
//    if(!base64String)
//    {
//        base64String=@"";
//    }
   
     NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:[_event_name_tf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"event_name"];
    [dict setObject:[_start_location_tf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"event_starting_location_address"];
    [dict setObject:@"" forKey:@"event_ending_location_address"];
    [dict setObject:@"" forKey:@"event_ending_location_latitude"];
    [dict setObject:@"" forKey:@"event_ending_location_longitude"];
    [dict setObject:[_assosiated_ride_TF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"associated_ride"];
    [dict setObject:[_website_link.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"link"];
    [dict setObject:[_start_date_tf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"event_start_date"];
    [dict setObject:[_end_date_tf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"event_end_date"];
//    [dict setObject:[_start_time_tf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"event_start_time"];
//    [dict setObject:[_end_time_tf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"event_end_time"];
    [dict setObject:[NSString stringWithFormat:@"%@",eventStartTimeString] forKey:@"event_start_time"];
    [dict setObject:[NSString stringWithFormat:@"%@",eventEndTimeString] forKey:@"event_end_time"];
//    [dict setObject:[eventEndTimeString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"event_end_time"];
    
    
    [dict setObject:[_description_tf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"event_description"];
    if ([_end_location_tf.text isEqualToString:@"Charitable"]) {
        [dict setObject:@"charitable" forKey:@"event_type"];
    }
    else{
        [dict setObject:@"normal" forKey:@"event_type"];
    }
    
    if (start_latitude==0) {
         [dict setObject:@"" forKey:@"event_starting_location_latitude"];
    }
    else
    {
         [dict setObject:[NSNumber numberWithDouble:start_latitude] forKey:@"event_starting_location_latitude"];
    }
    if (start_longitude==0) {
         [dict setObject:@"" forKey:@"event_starting_location_longitude"];
    }
    else
    {
         [dict setObject:[NSNumber numberWithDouble:start_longitude] forKey:@"event_starting_location_longitude"];
    }
   
    [dict setObject:img_array forKey:@"event_image_names"];
    [dict setObject:@"app" forKey:@"request_from"];
    [dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
    NSLog(@"Create Event %@",dict);
    
    [SHARED_API createEventRequestsWithParams:dict withSuccess:^(NSDictionary *response) {
        NSLog(@"responce is %@",response);
        if ([[response valueForKey:STATUS] isEqualToString:SUCCESS])
        {
            [SHARED_HELPER showAlert:create_event_sucsess];
            [self create_Event_Sucsess];
            
        }
        else{
             [activeIndicatore stopAnimating];
            [SHARED_HELPER showAlert:ServerConnection];
        }
    } onfailure:^(NSError *theError) {
        [activeIndicatore stopAnimating];
        [SHARED_HELPER showAlert:ServiceFail];
    }];
}




-(void)create_Event_Sucsess{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [activeIndicatore stopAnimating];
        appDelegate.create_event_or_edit_event=@"YES";
        
        [self cancle_clicked];
    });
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -  UI-Textfield delegate methods.....

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.event_type_view.hidden=YES;
    self.images_Baseview.alpha=0.1;
    if (textField==_end_location_tf || textField==_start_date_tf || textField==_start_time_tf) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:.3];
        [UIView setAnimationBeginsFromCurrentState:TRUE];
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -100., self.view.frame.size.width, self.view.frame.size.height);
        
        [UIView commitAnimations];
        
    }
    if (textField==_description_tf || textField==_end_date_tf || textField==_end_time_tf) {
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:.3];
        [UIView setAnimationBeginsFromCurrentState:TRUE];
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -180., self.view.frame.size.width, self.view.frame.size.height);
        
        [UIView commitAnimations];
        
    }
    if (textField==_start_date_tf) {
        _start_date_tf.text=@"";
        _start_time_tf.text=@"";
        _end_date_tf.text=@"";
        _end_time_tf.text=@"";
        [self set_start_date];
    }
    else if (textField==_start_time_tf) {
        if (!([_start_date_tf.text length]>0)) {
            [textField resignFirstResponder];
             [SHARED_HELPER showAlert:EventDate];
            return;
        }
        else
        {
            _end_date_tf.text=@"";
            _end_time_tf.text=@"";
            [self setstarttime];
        }
    }
    else if (textField==_end_date_tf)
    {
        if (!([_start_date_tf.text length]>0)) {
            [textField resignFirstResponder];
            [SHARED_HELPER showAlert:EventDate];
            return;
        }
        else{
           
            _end_time_tf.text=@"";
            [self set_end_date];
        }
    }
    else if (textField==_end_time_tf)
    {
        if (!([_end_date_tf.text length]>0)) {
            [textField resignFirstResponder];
             [SHARED_HELPER showAlert:Event_endDate];
            return;
        }
        else{
           
            [self setendtime];
        }
    }

}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    self.images_Baseview.alpha=1;
    if (textField==_end_location_tf || textField==_start_date_tf || textField==_start_time_tf) {
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:.3];
        [UIView setAnimationBeginsFromCurrentState:TRUE];
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +100., self.view.frame.size.width, self.view.frame.size.height);
        
        [UIView commitAnimations];
    }
    if (textField==_description_tf || textField==_end_date_tf || textField==_end_time_tf) {
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:.3];
        [UIView setAnimationBeginsFromCurrentState:TRUE];
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +180., self.view.frame.size.width, self.view.frame.size.height);
        
        [UIView commitAnimations];
        
    }
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    if (textField == _event_name_tf)
    {
        [textField resignFirstResponder];
        return NO;
    }
    else if (textField == _assosiated_ride_TF)
    {
        [_website_link becomeFirstResponder];
        return NO;
    }
    else if (textField == _website_link)
    {
        [textField resignFirstResponder];
        return NO;
    }
    
    
    [textField resignFirstResponder];
    return YES;
}
-(void)set_start_date{
    
    date = [[UIDatePicker alloc]init];
    date.datePickerMode = UIDatePickerModeDate;
     date.date=[NSDate date];
    [date setMinimumDate:[NSDate date]];
    [date setDate:[NSDate date]];
    [_start_date_tf setInputView:date];
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad1)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(showDate)]];
    [numberToolbar sizeToFit];
    _start_date_tf.inputAccessoryView = numberToolbar;
    
}

-(void)setstarttime{
    
    start_time = [[UIDatePicker alloc]init];
    start_time.datePickerMode = UIDatePickerModeTime;
    start_time.date=[NSDate date];
    [start_time setDate:[NSDate date]];
    [_start_time_tf setInputView:start_time];
    
    UIToolbar *toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolbar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *done = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(showstartTime)];
    
    UIBarButtonItem *space = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [toolbar setItems:[NSArray arrayWithObjects:space,done,nil]];
    [_start_time_tf setInputAccessoryView:toolbar];
    
}
-(void)showstartTime{
    
    NSDateFormatter *dateformater = [[NSDateFormatter alloc ]init];
//    [dateformater setDateFormat:@"HH:mm"];
    
    [dateformater setDateFormat:@"hh:mm a"];
    
    _start_time_tf.text = [NSString stringWithFormat:@"%@",[dateformater stringFromDate:start_time.date]];
    NSDateFormatter*startTimeFormat=[[NSDateFormatter alloc ]init];
    [startTimeFormat setDateFormat:@"HH:mm"];
    eventStartTimeString=[NSString stringWithFormat:@"%@",[startTimeFormat stringFromDate:start_time.date]];
//    eventStartTimeString=[NSString stringWithFormat:@"%@",_start_time_tf.text];
    [_start_time_tf resignFirstResponder];
    
}

-(void)setendtime{
    
    end_Time = [[UIDatePicker alloc]init];
    end_Time.datePickerMode = UIDatePickerModeTime;
    end_Time.date=[NSDate date];
    if ([_start_date_tf.text compare:_end_date_tf.text]==NSOrderedSame) {
        
         [end_Time setMinimumDate:start_time.date];
    }
    else
    {
//        [end_Time setMinimumDate:[NSDate date]];
    }
   
    [_end_time_tf setInputView:end_Time];
    
    UIToolbar *toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolbar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *done = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(showendTime)];
    
    UIBarButtonItem *space = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [toolbar setItems:[NSArray arrayWithObjects:space,done,nil]];
    [_end_time_tf setInputAccessoryView:toolbar];
    
}
-(void)showendTime{
    
    NSDateFormatter *dateformater = [[NSDateFormatter alloc ]init];
//    [dateformater setDateFormat:@"HH:mm"];
    [dateformater setDateFormat:@"hh:mm a"];
    _end_time_tf.text = [NSString stringWithFormat:@"%@",[dateformater stringFromDate:end_Time.date]];
    NSDateFormatter*endTimeFormat=[[NSDateFormatter alloc ]init];
    [endTimeFormat setDateFormat:@"HH:mm"];
    eventEndTimeString=[NSString stringWithFormat:@"%@",[endTimeFormat stringFromDate:end_Time.date]];
    [_end_time_tf resignFirstResponder];
    
}


-(void)set_end_date{
    
    end_date = [[UIDatePicker alloc]init];
    end_date.datePickerMode = UIDatePickerModeDate;
    end_date.date=[NSDate date];
    [end_date setMinimumDate:date.date];
    [_end_date_tf setInputView:end_date];
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad1)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(showDate1)]];
    [numberToolbar sizeToFit];
    _end_date_tf.inputAccessoryView = numberToolbar;
    
}

-(void)showDate{
    
    NSDateFormatter *dateformater = [[NSDateFormatter alloc ]init];
    [dateformater setDateFormat:@"MM/dd/YYYY"];
    _start_date_tf.text = [NSString stringWithFormat:@"%@",[dateformater stringFromDate:date.date]];
    [_start_date_tf resignFirstResponder];
   // [_start_time_tf becomeFirstResponder];
}

-(void)showDate1{
    
    NSDateFormatter *dateformater = [[NSDateFormatter alloc ]init];
    [dateformater setDateFormat:@"MM/dd/YYYY"];
    _end_date_tf.text = [NSString stringWithFormat:@"%@",[dateformater stringFromDate:end_date.date]];
    [_end_date_tf resignFirstResponder];
   // [_end_time_tf becomeFirstResponder];
}

-(void)cancle_clicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)save_clicked
{
     _event_name_tf.text=[_event_name_tf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
     _description_tf.text=[_description_tf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    self.event_type_view.hidden=YES;
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }

    if (_event_name_tf.text.length == 0) {
        [SHARED_HELPER showAlert:eventnameempty];
        return;
    }
//    if (!([_start_location_tf.text length]>0)) {
//        [SHARED_HELPER showAlert:event_adress];
//        return;
//    }
//    if (!([_end_location_tf.text length]>0)) {
//        [SHARED_HELPER showAlert:event_type];
//        return;
//    }
    if (!([_description_tf.text length]>0)) {
        [SHARED_HELPER showAlert:event_des];
         return;
    }
    
//    if(!(img_array.count>0))
//    {
//        [SHARED_HELPER showAlert:event_img];
//        return;
//    }

        [self create_Event_service];
}


- (IBAction)start_location_btn:(id)sender {
    location_str = @"start_location_btn";
    GMSPlacePickerConfig *config = [[GMSPlacePickerConfig alloc] initWithViewport:nil];
    GMSPlacePickerViewController *placePicker =
    [[GMSPlacePickerViewController alloc] initWithConfig:config];
    placePicker.delegate = self;
    [self presentViewController:placePicker animated:YES completion:nil];
  
    
}

- (IBAction)end_location_btn:(id)sender {
//    location_str = @"end_location_btn";
//    GMSPlacePickerConfig *config = [[GMSPlacePickerConfig alloc] initWithViewport:nil];
//    GMSPlacePickerViewController *placePicker =
//    [[GMSPlacePickerViewController alloc] initWithConfig:config];
//    placePicker.delegate = self;
//    [self presentViewController:placePicker animated:YES completion:nil];
    
    self.event_type_view.hidden=NO;
    
}


#pragma mark -Image_picker_view............
- (IBAction)upload_img_btn:(id)sender {
    [self.view endEditing:YES];
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:nil
                                  message:nil
                                  preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *Camera = [UIAlertAction
                             actionWithTitle:@"Take photo"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 [self camera_action];
                             }];
    [alert addAction:Camera];
    
    UIAlertAction *Gallery = [UIAlertAction
                              actionWithTitle:@"Choose image"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  [alert dismissViewControllerAnimated:YES completion:nil];
                                  [self Gallery_action];
                              }];
    [alert addAction:Gallery];
    [self presentViewController:alert animated:YES completion:nil];
    
}


-(void)cancelNumberPad1{
    
    [self.view endEditing:YES];
    
}







-(void)camera_action
{
    UIImagePickerController *cameraPicker = [[UIImagePickerController alloc] init];
    cameraPicker.delegate = self;
    cameraPicker.allowsEditing = YES;
    [cameraPicker setAllowsEditing:YES];
    cameraPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:cameraPicker animated:YES completion:NULL];
}

-(void)Gallery_action
{
    UIImagePickerController *galleryPicker = [[UIImagePickerController alloc] init];
    galleryPicker.delegate = self;
    galleryPicker.allowsEditing = YES;
    [galleryPicker setAllowsEditing:YES];
    galleryPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:galleryPicker animated:YES completion:NULL];
}
//- (void) imagePickerController:(UIImagePickerController *)picker
//         didFinishPickingImage:(UIImage *)image
//                   editingInfo:(NSDictionary *)editingInfo
//{
//    
//    cameraImage = image;
//    NSData *webData = UIImagePNGRepresentation(cameraImage);
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString *imgFormat = [NSString stringWithFormat:@"PNG"];
//    NSString *localFilePath = [documentsDirectory stringByAppendingPathComponent:imgFormat];
//    [webData writeToFile:localFilePath atomically:YES];
//    NSLog(@"path is :%@",localFilePath);
//    _upload_img_lbl.text = localFilePath;
//    NSLog(@"imgpath is:%@",_upload_img_lbl.text );
//    
//    if (_upload_img_lbl.text.length >0) {
//        _upload_img_tf.placeholder = nil;
//    }
//    [picker dismissViewControllerAnimated:YES completion:NULL];
//    
//    CGFloat compression = 0.9f;
//    CGFloat maxCompression = 0.1f;
//    int maxFileSize = 250*1024;
//    profileImgData = UIImageJPEGRepresentation(cameraImage,compression);
//    while ([profileImgData length] > maxFileSize && compression > maxCompression)
//    {
//        compression -= 0.1;
//        profileImgData = UIImageJPEGRepresentation(cameraImage,compression);
//    }
//    base64String = [profileImgData base64EncodedStringWithOptions:0];
//
//}

- (void)placePicker:(GMSPlacePickerViewController *)viewController didPickPlace:(GMSPlace *)place {
    // Dismiss the place picker, as it cannot dismiss itself.
    [viewController dismissViewControllerAnimated:YES completion:nil];
    
    
    if ([location_str isEqualToString:@"start_location_btn"]) {
//        _start_location_tf.text = [NSString stringWithFormat:@"%@ %@",place.name,place.formattedAddress];
        NSString*loc_str_name=[NSString stringWithFormat:@"%@",place.name];
        if ([loc_str_name isEqualToString:@"(null)"] || [loc_str_name isEqualToString:@"<null>"] || [loc_str_name isEqualToString:@""] || [loc_str_name isEqualToString:@"null"] || loc_str_name == nil) {
            loc_str_name=@"";
            
        }
        NSString*loc_str_address=[NSString stringWithFormat:@"%@",place.formattedAddress];
        if ([loc_str_address isEqualToString:@"(null)"] || [loc_str_address isEqualToString:@"<null>"] || [loc_str_address isEqualToString:@""] || [loc_str_address isEqualToString:@"null"] || loc_str_address == nil) {
            loc_str_address=@"";
            
        }
        NSString*loc_name_address_str=[NSString stringWithFormat:@"%@ %@",loc_str_name,loc_str_address];
        
        if ([loc_name_address_str isEqualToString:@"(null)"] || [loc_name_address_str isEqualToString:@"<null>"] || [loc_name_address_str isEqualToString:@""] || [loc_name_address_str isEqualToString:@"null"] || loc_name_address_str == nil) {
            loc_name_address_str=@"";
            
        }
        else
        {
            _start_location_tf.text=loc_name_address_str;
        }

        start_latitude=place.coordinate.latitude;
        start_longitude=place.coordinate.longitude;
       
        NSLog(@"Place name %@", place.name);
        NSLog(@"Place address %@", place.formattedAddress);
        NSLog(@"Place attributions %@", place.attributions.string);
    }
    else if ([location_str isEqualToString:@"end_location_btn"]){
        
        _end_location_tf.text = place.name;
        end_latitude=place.coordinate.latitude;
        end_longitude=place.coordinate.longitude;
        NSLog(@"Place name %@", place.name);
        NSLog(@"Place address %@", place.formattedAddress);
        NSLog(@"Place attributions %@", place.attributions.string);
    }
    else
    {
        NSLog(@"Place name %@", place.name);
        NSLog(@"Place address %@", place.formattedAddress);
        NSLog(@"Place attributions %@", place.attributions.string);
    }
    
}

- (void)placePickerDidCancel:(GMSPlacePickerViewController *)viewController {
    // Dismiss the place picker, as it cannot dismiss itself.
    [viewController dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"No place selected");
}





- (IBAction)onClick_startDate:(id)sender {
    
    
   
}
- (IBAction)onClick_startTime:(id)sender {
     
}

- (IBAction)onClick_endDate:(id)sender {
    

}

- (IBAction)onClick_endTime:(id)sender {
    
}
- (IBAction)event_type_action_view:(UIButton *)sender {
    if (sender.tag==0) {
         self.event_type_view.hidden=YES;
        _end_location_tf.text=@"Charitable";
    }
    else{
         _end_location_tf.text=@"Normal";
         self.event_type_view.hidden=YES;
    }
    
}
- (IBAction)onClick_createEvent_rideGoingBtn:(id)sender {
    
    self.createEvent_scrollLabel.hidden=YES;
    self.createEvent_rideGoingBtn.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }

}
@end
