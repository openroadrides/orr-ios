//
//  NotificationCell.h
//  openroadrides
//
//  Created by apple on 05/06/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *content_view;
@property (weak, nonatomic) IBOutlet UILabel *description_lbl;
@property (weak, nonatomic) IBOutlet UILabel *time_lbl;
@property (weak, nonatomic) IBOutlet UIView *inline_view;
@property (weak, nonatomic) IBOutlet UIView *start_notification_view;
@property (weak, nonatomic) IBOutlet UILabel *join_ride_label;
@property (weak, nonatomic) IBOutlet UIImageView *start_imageView;
@property (weak, nonatomic) IBOutlet UIView *invitation_notification_view;
@property (weak, nonatomic) IBOutlet UIImageView *profile_imageView;
@property (weak, nonatomic) IBOutlet UILabel *userName_label;
@property (weak, nonatomic) IBOutlet UIImageView *location_imageView;
@property (weak, nonatomic) IBOutlet UILabel *address_label;
@property (weak, nonatomic) IBOutlet UILabel *created_by_label;
@property (weak, nonatomic) IBOutlet UIView *requestView;
@property (weak, nonatomic) IBOutlet UIView *acceptView;
@property (weak, nonatomic) IBOutlet UIButton *notification_accept_btn;
@property (weak, nonatomic) IBOutlet UIView *reject_view;
@property (weak, nonatomic) IBOutlet UIButton *notification_reject_btn;
@property (weak, nonatomic) IBOutlet UILabel *notification_ride_title;
@property (weak, nonatomic) IBOutlet UILabel *notification_group_created_label;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *requestView_widthConstraint;

@end
