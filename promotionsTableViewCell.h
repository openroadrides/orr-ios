//
//  promotionsTableViewCell.h
//  openroadrides
//
//  Created by SrkIosEbiz on 26/09/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface promotionsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *Promotion_Title;
@property (weak, nonatomic) IBOutlet UILabel *by_Name;
@property (weak, nonatomic) IBOutlet UILabel *promotion_end_date;

@end
