//
//  Notifications.h
//  openroadrides
//
//  Created by apple on 05/06/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "DashboardViewController.h"
#import "RideDashboard.h"
#import "GroupDetailViewController.h"
#import "HomeDashboardView.h"
#import "CBAutoScrollLabel.h"
@interface Notifications : UIViewController<UITableViewDelegate,UITableViewDataSource>{
     UIActivityIndicatorView  *activeIndicatore;
    UIView *indicaterview;
    NSUserDefaults *defaults;
    NSString *user_token_id;
    
    UIRefreshControl *refreshControl;
    NSArray *pending_rides_array;
}
@property(strong,nonatomic)NSString *is_from;
@property (weak, nonatomic) IBOutlet UITableView *notification_table_view;
@property (weak, nonatomic) IBOutlet UIView *main_content_vw;
@property (strong, nonatomic) NSMutableArray *notifications_array;
- (IBAction)onClick_notification_reject_btn:(id)sender;
- (IBAction)onClick_notification_accept_btn:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *noData_notifications_label;
@property (weak, nonatomic) IBOutlet UIButton *notifications_rideGoingBtn;
- (IBAction)onClick_notifications_rideGoingBtn:(id)sender;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *notifications_scrollLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint_notificationsTV;
@end
