//
//  RoutesVC.h
//  openroadrides
//
//  Created by SrkIosEbiz on 30/05/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "RateView.h"
#import "RatingBar.h"
#import "AppHelperClass.h"
#import "APIHelper.h"
#import "RideDashboard.h"
#import "CreaterideViewController.h"
#import "SelectFriendsAndGroups.h"
#import "Public_Route_Cell.h"
#import "RouteDetailViewController.h"
#import "CBAutoScrollLabel.h"
@interface RoutesVC : UIViewController<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,UISearchBarDelegate>
{
    NSString *view_Status,*my_Routes_Called,*Public_Called,*route_id_str;
    NSMutableArray *titles_For_Public_Routes,*titles_For_My_Routes,*ratings_array;
    UIActivityIndicatorView  *activeIndicatore;
    UIView *indicaterview;
    UILabel *NO_DATA;
    UIBarButtonItem *item0;
    NSString*selectedRouteIDString_checkbox,*selectedRouteStartAddressString,*selectedRouteStartAddressString_checkbox,*selectedRouteAddressLatitude,*selectedRouteAddressLongtitude;
     int friends_count,groups_count;
    
       BOOL changed_route_status,routesIsFilterd;
    
    UISwipeGestureRecognizer *swipe_right;
    UISwipeGestureRecognizer *swipe_left;
    
    NSString *selected_Route_Name;
    
}

@property (strong, nonatomic)NSString *is_FROM_VC_OR_Start_Ride;
@property (strong, nonatomic)NSString *is_Free_Ride;

@property (strong, nonatomic) IBOutlet UIScrollView *main_Scrole_View;

@property (strong, nonatomic) IBOutlet UIView *content_View;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *content_View_Width_Constraint;
@property (strong, nonatomic) IBOutlet UITableView *p_Routes_Table;
@property (strong, nonatomic) IBOutlet UITableView *my_Routes_Table;
@property (strong, nonatomic) IBOutlet UIView *p_Routes_Btn_View;
@property (strong, nonatomic) IBOutlet UIView *my_Routes_btn_View;
@property (strong, nonatomic) IBOutlet UIButton *publice_btn;
@property (strong, nonatomic) IBOutlet UILabel *my_Routes_Label;

@property (strong, nonatomic) IBOutlet UILabel *public_Routes_Title_lavel;

@property (strong, nonatomic) IBOutlet UIButton *my_Route_btn;
- (IBAction)Public_Route_Action:(id)sender;
- (IBAction)My_Route_Action:(id)sender;
@property (strong, nonatomic) NSString *checkboxSelecteString,*checkboxSelectedAddressString;

@property NSMutableArray *arrayOfFrndsInRoutes,*titles_Filter_Public_Routes,*titles_Filter_My_Routes;
@property NSMutableArray *arrayOfgroupsInRoutes;

@property (strong, nonatomic) IBOutlet UIView *side_Menu_view;
@property (strong, nonatomic) IBOutlet UIImageView *User_image;
@property (strong, nonatomic) IBOutlet UILabel *User_Name;
@property (strong, nonatomic) IBOutlet UITableView *side_Menu_Table;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *menu_Leading_Constraint;
- (IBAction)Profile_Action:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *routes_Bg_View;
@property (weak, nonatomic) IBOutlet UILabel *noData_label_myroutes;

@property (weak, nonatomic) IBOutlet UILabel *noData_label_punlic_routes_label;

@property (weak, nonatomic) IBOutlet UISearchBar *routes_searchBar;
@property (weak, nonatomic) IBOutlet UIButton *routes_rideGoingBtn;
- (IBAction)onClick_routes_rideGoingBtn:(id)sender;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *routes_scrollLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint_routesScrollView;


@end
