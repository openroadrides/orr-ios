//
//  SignupViewController.m
//  openroadrides
//
//  Created by apple on 01/05/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "SignupViewController.h"
#import "AppHelperClass.h"
#import "APIHelper.h"
#import "LoginViewController.h"
#import "DashboardViewController.h"
@interface SignupViewController (){
    NSString *Device_id;
}

@end

@implementation SignupViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    Device_id = [NSString stringWithFormat:@"%@" ,[defaults valueForKey:UDID]];
    
    [self setdate];
    [self.navigationController.navigationBar setBarTintColor:APP_YELLOW_COLOR];
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController.navigationBar setTranslucent:NO];
    self.navigationController.navigationBar.backItem.title=@"";
    [self.view layoutIfNeeded];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 120, 30)];
    label.textAlignment = NSTextAlignmentCenter;
    [label setFont:[UIFont fontWithName:@"Antonio-Bold" size:20]];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:[UIColor blackColor]];
    [label setText:@"REGISTRATION"];
    [self.navigationController.navigationBar.topItem setTitleView:label];
    
//    _Profileimageview.layer.cornerRadius = _Profileimageview.frame.size.width / 2;
//    _Profileimageview.clipsToBounds = YES;
//    _Profileimageview.layer.cornerRadius=_Profileimageview.frame.size.height/2;
    
    _Gender_view.layer.borderWidth = 2.0;
    _Gender_view.layer.borderColor = [UIColor cyanColor].CGColor;
    _gender_middleLine_view.backgroundColor=[UIColor cyanColor];
//    genderString=@"Male";
      genderString=@"";
    
     [self.Username_tf setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
     [self.Email_tf setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
     [self.Dob_tf setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
     [self.Zipcode_tf setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
     [self.State_tf setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
     [self.City_tf setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
     [self.Address_tf setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
     [self.Phoneno_tf setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    stateArray = [[NSMutableArray alloc]init];
    dataArray = [[NSMutableArray alloc]init];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"States" ofType:@"plist"];
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
    stateArray = [dict objectForKey:@"States"];
    mapping = [dict objectForKey:@"Mapping"];
     [self creatingPickerView];
    // Do any additional setup after loading the view.
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    _Zipcode_tf.inputAccessoryView = numberToolbar;
    
    
    UIToolbar* numberToolbar1 = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar1.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar1.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad1)],
                             [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                             [[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad1)]];
    [numberToolbar1 sizeToFit];
    _Phoneno_tf.inputAccessoryView = numberToolbar1;
    
}
-(void)cancelNumberPad{
    
    [_Zipcode_tf resignFirstResponder];
    
}

-(void)doneWithNumberPad{
    [_Zipcode_tf resignFirstResponder];
}

-(void)cancelNumberPad1{
    
    [self.view endEditing:YES];
   
}

-(void)doneWithNumberPad1{
    
    [_Dob_tf becomeFirstResponder];
}


- (void)creatingPickerView
{
    pickerBaseView = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 250)];
    pickerBaseView.backgroundColor = [UIColor clearColor];
    //pickerBaseView.layer.backgroundColor =[UIColor colorWithRed:(35/255.0) green:(30/255.0) blue:(34/255.0) alpha:1].CGColor;
    UIView *doneview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, pickerBaseView.frame.size.width, 50)];
    doneview.backgroundColor = [UIColor colorWithRed:18/255.0 green:17/255.0 blue:14/255.0 alpha:1];
    doneButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [doneButton addTarget:self
                   action:@selector(pickerDoneButtonAction:)
         forControlEvents:UIControlEventTouchUpInside];
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
   // doneButton.tintColor = [UIColor colorWithRed:18/255.0 green:17/255.0 blue:14/255.0 alpha:1];
    doneButton.titleLabel.font  = [UIFont fontWithName:@"arial" size:15];
    doneButton.frame = CGRectMake(self.view.frame.size.width-120, 5, 160.0, 40.0);
    [doneview addSubview:doneButton];
    
    cancel_Button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [cancel_Button addTarget:self
                   action:@selector(hide_Picker_View)
         forControlEvents:UIControlEventTouchUpInside];
    [cancel_Button setTitle:@"Cancel" forState:UIControlStateNormal];
    //cancel_Button.tintColor = [UIColor colorWithRed:18/255.0 green:17/255.0 blue:14/255.0 alpha:1];
    cancel_Button.titleLabel.font  = [UIFont fontWithName:@"arial" size:15];
    cancel_Button.frame = CGRectMake(5, 5, 80, 40.0);
    [doneview addSubview:cancel_Button];
    
    businessTypePickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(0,50, self.view.frame.size.width, 200)];
    businessTypePickerView.delegate = self;
    businessTypePickerView.dataSource = self;
    businessTypePickerView.backgroundColor = [UIColor colorWithRed:0.8784 green:0.8784 blue:0.8784 alpha:1.0f];
    Spickerchng=NO;
    Cpickerchng=NO;
    [pickerBaseView addSubview: doneview];
    [pickerBaseView addSubview:businessTypePickerView];
    [self.view addSubview:pickerBaseView];
}
-(void)hide_Picker_View{
    _sign_up_scrl_vw.scrollEnabled=YES;
    _sign_up_scrl_vw.userInteractionEnabled=YES;
     [self dismissPickerView];
}
#pragma mark - PickerView Delegate Methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent: (NSInteger)component
{
    return [dataArray count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row   forComponent:(NSInteger)component
{
    return [dataArray objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row   inComponent:(NSInteger)component
{
    if ([pickerclick isEqualToString:@"state"])
    {
        Spickerchng=YES;
        _State_tf.text = [NSString stringWithFormat:@"%@",[dataArray objectAtIndex:row]];
        _City_tf.text=@"";
    }
    selectedindexv=row;
    if ([pickerclick isEqualToString:@"city"])
    {
        Cpickerchng=YES;
        _City_tf.text = [NSString stringWithFormat:@"%@",[dataArray objectAtIndex:row]];
    }
    else if([pickerclick isEqualToString:@""])
    {
    }
}
- (IBAction)pickerDoneButtonAction:(id)sender
{
    _sign_up_scrl_vw.scrollEnabled=YES;
    _sign_up_scrl_vw.userInteractionEnabled=YES;
    if ([pickerclick isEqualToString:@"city"])
    {
        if (Spickerchng==YES)
        {
            if (Cpickerchng==YES)
            {
                _City_tf.text = [NSString stringWithFormat:@"%@",[dataArray objectAtIndex:selectedindexv]];
                Cpickerchng=NO;
            }
            else
            {
                _City_tf.text = [NSString stringWithFormat:@"%@",[dataArray objectAtIndex:0]];
            }
            Spickerchng=NO;
        }
        else
        {
            if ( [_City_tf.text isEqualToString:@""] )
            {
               _City_tf.text = [NSString stringWithFormat:@"%@",[dataArray objectAtIndex:0]];
            }
        }
        pickerclick=@"";
    }
    else
    {
        if ( [_State_tf.text isEqualToString:@""] )
        {
            _State_tf.text=[dataArray objectAtIndex:0];
        }
        pickerclick=@"";
        
    }
    [self dismissPickerView];
}

- (IBAction)citybtn_action:(id)sender
{
    if ([_State_tf.text isEqualToString:@""])
    {
        [SHARED_HELPER showAlert:SelectState];
    }
    else
    {
       
        [_sign_up_scrl_vw setContentOffset:CGPointMake(0, pickerBaseView.bounds.size.height+5) animated:YES];
        _sign_up_scrl_vw.scrollEnabled=NO;
        _sign_up_scrl_vw.userInteractionEnabled=NO;
        [self presentPickerView];
        pickerclick=@"city";
        dataArray = [mapping objectForKey:_State_tf.text];
        int indexRow=0;
        if (_City_tf.text.length !=0) {
            for (int i = 0; i<dataArray.count; i++ )
            {
                if ([_City_tf.text isEqualToString:[dataArray objectAtIndex:i]])
                    indexRow = i;
            }
        }
        [businessTypePickerView reloadAllComponents];
        [businessTypePickerView selectRow:indexRow inComponent:0 animated:NO];
    }
    
}
- (IBAction)statebtnAcion:(id)sender
{
    _sign_up_scrl_vw.scrollEnabled=NO;
    _sign_up_scrl_vw.userInteractionEnabled=NO;
    
     [_sign_up_scrl_vw setContentOffset:CGPointMake(0,pickerBaseView.bounds.size.height-45) animated:YES];
    
    [self presentPickerView];
    pickerclick=@"state";
    dataArray = stateArray;
    int indexRow=0;
    if (_State_tf.text.length !=0) {
        for (int i = 0; i<dataArray.count; i++ )
        {
            if ([_State_tf.text isEqualToString:[dataArray objectAtIndex:i]])
                indexRow = i;
        }
    }
    [businessTypePickerView reloadAllComponents];
    [businessTypePickerView selectRow:indexRow inComponent:0 animated:NO];
}


- (void)presentPickerView
{
    self.tabBarController.tabBar.hidden = true;
    [UIView animateWithDuration:0.3 animations:^{
        pickerBaseView.frame = CGRectMake(0, self.view.bounds.size.height - pickerBaseView.bounds.size.height, pickerBaseView.bounds.size.width, pickerBaseView.bounds.size.height);
    }];
}
- (void)dismissPickerView
{
    [UIView animateWithDuration: 0.3 animations:^{ pickerBaseView.frame = CGRectMake(0, self.view.bounds.size.height, pickerBaseView.bounds.size.width, pickerBaseView.bounds.size.height);}
                     completion:^(BOOL finished) {
                         self.tabBarController.tabBar.hidden = false;
                     }];
     [_sign_up_scrl_vw setContentOffset:CGPointMake(0,0) animated:YES];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.view endEditing:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.view endEditing:YES];
    
    // Disable iOS 7 back gesture
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.view endEditing:YES];
    [super viewWillDisappear:animated];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma - mark TextField Delegate Methods
-(void)textstartedting
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -150., self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];
}
-(void)textendedting
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +150., self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
      [self textendedting];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    currentTextField = textField;
    
        [self textstartedting];
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if (textField== _Username_tf)
    {
        [_Email_tf becomeFirstResponder];
        return NO;
    }
    else if (textField== _Email_tf)
    {
        [_Phoneno_tf becomeFirstResponder];
        return NO;
    }
    else if (textField== _Phoneno_tf)
    {
        [_Dob_tf becomeFirstResponder];
        return NO;
    }
    else if (textField== _Dob_tf)
    {
        [_Address_tf becomeFirstResponder];
        return NO;
    }
    
    else if (textField== _Address_tf)
    {
        [textField resignFirstResponder];
        [self statebtnAcion:0];
        return YES;
    }
    else if (textField== _State_tf)
    {
        [textField resignFirstResponder];
        [self citybtn_action:0];
        return YES;
    }
    else if (textField== _City_tf)
    {
        [_Zipcode_tf becomeFirstResponder];
        return NO;
    }
    
    [textField resignFirstResponder];
    return YES;
}


- (IBAction)logo_click:(id)sender
{
//    UIAlertController * alert=   [UIAlertController
//                                  alertControllerWithTitle:nil
//                                  message:@"Choose image."
//                                  preferredStyle:UIAlertControllerStyleActionSheet];
//    
//    UIAlertAction *Camera = [UIAlertAction
//                         actionWithTitle:@"Camera"
//                         style:UIAlertActionStyleDefault
//                         handler:^(UIAlertAction * action)
//                         {
//                             [alert dismissViewControllerAnimated:YES completion:nil];
//                              [self camera_action];
//                         }];
//    [alert addAction:Camera];
//    
//    UIAlertAction *Gallery = [UIAlertAction
//                         actionWithTitle:@"Gallery"
//                         style:UIAlertActionStyleDefault
//                         handler:^(UIAlertAction * action)
//                         {
//                             [alert dismissViewControllerAnimated:YES completion:nil];
//                              [self Gallery_action];
//                         }];
//    [alert addAction:Gallery];
//    [self presentViewController:alert animated:YES completion:nil];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped do nothing.
        
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Take photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Choose photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }]];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)camera_action
{
    UIImagePickerController *cameraPicker = [[UIImagePickerController alloc] init];
    cameraPicker.delegate = self;
    cameraPicker.allowsEditing = YES;
    [cameraPicker setAllowsEditing:YES];
    cameraPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:cameraPicker animated:YES completion:NULL];
}

-(void)Gallery_action
{
    UIImagePickerController *galleryPicker = [[UIImagePickerController alloc] init];
    galleryPicker.delegate = self;
    galleryPicker.allowsEditing = YES;
    [galleryPicker setAllowsEditing:YES];
    galleryPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:galleryPicker animated:YES completion:NULL];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    _Profileimageview.layer.cornerRadius=_Profileimageview.frame.size.height/2;
    _Profileimageview.layer.cornerRadius = _Profileimageview.frame.size.width / 2;
    _Profileimageview.clipsToBounds = YES;
    
    
    cameraImage = info[UIImagePickerControllerEditedImage];
    _Profileimageview.image=cameraImage;
    [picker dismissViewControllerAnimated:YES completion:NULL];
    CGFloat compression = 0.9f;
    CGFloat maxCompression = 0.1f;
    int maxFileSize = 250*1024;
    profileImgData = UIImageJPEGRepresentation(cameraImage,compression);
    while ([profileImgData length] > maxFileSize && compression > maxCompression)
    {
        compression -= 0.1;
        profileImgData = UIImageJPEGRepresentation(cameraImage,compression);
    }
    base64String = [profileImgData base64EncodedStringWithOptions:0];
//        NSLog(@"base64String is %@", base64String);
    
 }

- (IBAction)Gender_action:(id)sender
{
    NSInteger tid = ((UIButton *) sender).tag;
    if (tid==0)
    {
        genderString=@"Male";
        _Male_outlet.backgroundColor=[UIColor cyanColor];
        _Female_outlet.backgroundColor=[UIColor clearColor];
        _Mailimageview.image=[UIImage imageNamed:@"signup_black_male_icon"];
        _Femailimageview.image=[UIImage imageNamed:@"signup_white_female"];
       [_Male_outlet setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
       [_Female_outlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    else
    {
        
         genderString=@"Female";
        _Male_outlet.backgroundColor=[UIColor clearColor];
        _Female_outlet.backgroundColor=[UIColor cyanColor];
        _Mailimageview.image=[UIImage imageNamed:@"signup_white_male_icon"];
        _Femailimageview.image=[UIImage imageNamed:@"signup_black_female_icon"];
        [_Male_outlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_Female_outlet setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
    }
}
- (IBAction)SingUp_action:(id)sender

{
     _Username_tf.text=[_Username_tf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (![SHARED_HELPER checkIfisInternetAvailable]) {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }

    [currentTextField resignFirstResponder];
    if(_Username_tf.text.length == 0 )
    {
        [SHARED_HELPER showAlert:UserNameText];
    }
    else if(_Email_tf.text.length ==0)
    {
        [SHARED_HELPER showAlert:EmailText];
    }
    else if ([self NSStringIsValidEmail:_Email_tf.text]== NO)
    {
        [SHARED_HELPER showAlert:ValidEmailText];
        
    }
//    else if (!_Phoneno_tf.text || _Phoneno_tf.text.length ==0)
//    {
//        [SHARED_HELPER showAlert:EmptyMobileNumber];
//    }
    else if (_Phoneno_tf.text.length !=12 && _Phoneno_tf.text.length !=0)
    {
        [SHARED_HELPER showAlert:MobileText];
    }
    
//    else if (_Dob_tf.text.length == 0)
//    {
//        [SHARED_HELPER showAlert:DOB_EMPTY];
//    }
    
//    else if (_Address_tf.text.length ==0)
//    {
//        [SHARED_HELPER showAlert:ADDRES1Text];
//    }
//    else if (_State_tf.text .length == 0)
//    {
//        [SHARED_HELPER showAlert:StateText];
//    }
//    else if (_City_tf.text.length == 0)
//    {
//        [SHARED_HELPER showAlert:CityText];
//    }
//    else if (!_Zipcode_tf.text || _Zipcode_tf.text.length ==0)
//    {
//        [SHARED_HELPER showAlert:EmptyZipcode];
//    }
    else if (_Zipcode_tf.text.length !=5 && _Zipcode_tf.text.length !=0)
    {
        [SHARED_HELPER showAlert:ZipcodeText];
    }
    else
    {
        
        if(!base64String)
        {
         base64String=@"";
        }
        activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        activeIndicatore.center = self.view.center;
        activeIndicatore.color = APP_YELLOW_COLOR;
        activeIndicatore.hidesWhenStopped = TRUE;
        [activeIndicatore startAnimating];
        [self.view addSubview:activeIndicatore];
        
        NSMutableDictionary *dict = [NSMutableDictionary new];
        [dict setObject:_Username_tf.text forKey:@"name"];
        [dict setObject:_Email_tf.text forKey:@"email"];
        [dict setObject:genderString forKey:@"gender"];
        [dict setObject:_Zipcode_tf.text forKey:@"zipcode"];
        [dict setObject:_Phoneno_tf.text forKey:@"phone_number"];
        [dict setObject:_Dob_tf.text forKey:@"date_of_birth"];
        [dict setObject:_Address_tf.text forKey:@"address"];
        [dict setObject:_City_tf.text forKey:@"city"];
        [dict setObject:_State_tf.text forKey:@"state"];
        [dict setObject:base64String forKey:@"profile_image"];
        [dict setObject:@"ios" forKey:@"device_type"];
        [dict setObject:Device_id forKey:@"device_registration_id"];

        NSLog(@"%@",dict);
        [SHARED_API signupRequestsWithParams:dict withSuccess:^(NSDictionary *response) {
            dispatch_async(dispatch_get_main_queue(), ^
                           {
                               NSLog(@"%@",response);
                               
                               NSLog(@"userInfo%@",response);
                               _userid=[response objectForKey:@"user_id"];
                               
                        if ([[response objectForKey:@"reg_type"] isEqualToString:@"social"]) {
                            reg_str = [response objectForKey:@"reg_type"];
                            
                            }
                            if([[response objectForKey:STATUS] isEqualToString:REGSUCCESS])
                        {
                            if ([activeIndicatore isAnimating]) {
                                [activeIndicatore stopAnimating];
                                [activeIndicatore removeFromSuperview];
                            }

                            
                            [self.navigationController popViewControllerAnimated:YES];
                                   [SHARED_HELPER showAlert:SignUpSuccess];
                            
                            
                        }
                               
                        else if
                         ([[response objectForKey:STATUS] isEqualToString:USER_EXIST])
                         {
                              [self showpopup:normalupdate];
                         }
                        else{
                            if ([activeIndicatore isAnimating]) {
                                [activeIndicatore stopAnimating];
                                [activeIndicatore removeFromSuperview];
                            }

                             [SHARED_HELPER showAlert:SignUpfail];
                        }
                            
    });
            
        } onfailure:^(NSError *theError) {
            
            dispatch_async(dispatch_get_main_queue(), ^
                           {
                               if ([activeIndicatore isAnimating]) {
                                   [activeIndicatore stopAnimating];
                                   [activeIndicatore removeFromSuperview];
                               }

                                 [SHARED_HELPER showAlert:ServiceFail];
                               
                               
                           });
        }];
    }
}


-(BOOL)NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

-(void)normalupdatecall
{
    
    if (![SHARED_HELPER checkIfisInternetAvailable]) {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
    
 //    indicaterview.hidden=NO;
    NSMutableDictionary *wsParams = [[NSMutableDictionary alloc]init];
    [wsParams setObject:_userid forKey:@"user_id"];
    [wsParams setObject:_Email_tf.text  forKey:@"email"];
    [wsParams setObject:REG_TYPENORMAL forKey:@"reg_type"];
    [wsParams setObject:@"" forKey:@"social_type"];
    [wsParams setObject:@"" forKey:@"social_id"];
    NSLog(@"%@",wsParams);
       [SHARED_API socialUpdateRequestsWithParams:wsParams withSuccess:^(NSDictionary *response) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if([[response objectForKey:STATUS] isEqualToString:SUCCESS])
            {
                
                if ([activeIndicatore isAnimating]) {
                    [activeIndicatore stopAnimating];
                    [activeIndicatore removeFromSuperview];
                }
                
                if ([reg_str isEqualToString:@"social"]){
                    reg_str = @"";
                    [SHARED_HELPER showAlert:Check_mail];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                         //Navigate to Login//
                        [self.navigationController popViewControllerAnimated:YES];
                    });
                }
                else
                {
                    //Navigate to Login//
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        [self.navigationController popViewControllerAnimated:YES];
                    });
                    
                }

               
//                [self successview];
            }
            else{
                
                if ([activeIndicatore isAnimating]) {
                    [activeIndicatore stopAnimating];
                    [activeIndicatore removeFromSuperview];
                }
                [SHARED_HELPER showAlert:ServiceFail];
            }
            
            
//            indicaterview.hidden=YES;
        });
        
    } onfailure:^(NSError *theError) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"mytheError%@",theError);
//            indicaterview.hidden=YES;
            if ([activeIndicatore isAnimating]) {
                [activeIndicatore stopAnimating];
                [activeIndicatore removeFromSuperview];
            }
            [SHARED_HELPER showAlert:ServerConnection];
            
        });
        
    }];


}

//-(void)successview{
//    
//}

- (IBAction)Signin_click:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)showpopup:(NSString *)message
{
   
       UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:nil
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"CONNECT"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [self normalupdatecall];
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"CANCEL"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    [activeIndicatore stopAnimating];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

//-(void)Back_btn_click{
//    [self.navigationController popViewControllerAnimated:YES];
//
//}

-(void)setdate{
    
    date = [[UIDatePicker alloc]init];
    date.datePickerMode = UIDatePickerModeDate;
    [_Dob_tf setInputView:date];
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad1)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(showDate)]];
    [numberToolbar sizeToFit];
    _Dob_tf.inputAccessoryView = numberToolbar;
    
}
-(void)showDate{
    
    NSDateFormatter *dateformater = [[NSDateFormatter alloc ]init];
    [dateformater setDateFormat:@"MM/dd/YYYY"];
    _Dob_tf.text = [NSString stringWithFormat:@"%@",[dateformater stringFromDate:date.date]];
    [_Dob_tf resignFirstResponder];
    [_Address_tf becomeFirstResponder];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == _Phoneno_tf)
    {
        NSCharacterSet *numSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789-"];
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSInteger charCount = [newString length];
        if (charCount>12) {
            return NO;
        }
        
        //new
        NSString *stringWithoutSpaces = [newString
                                         stringByReplacingOccurrencesOfString:@"-" withString:@""];
        NSInteger newcharCount = [stringWithoutSpaces length];
        if (newcharCount>10) {
            stringWithoutSpaces = [stringWithoutSpaces substringToIndex:[stringWithoutSpaces length]-1];
            NSMutableString *mu = [NSMutableString stringWithString:stringWithoutSpaces];
            [mu insertString:@"-" atIndex:3];
            [mu insertString:@"-" atIndex:7];
            textField.text = mu;
            return NO;
            
        }
        else if (newcharCount==10)
        {
            NSMutableString *mu = [NSMutableString stringWithString:stringWithoutSpaces];
            [mu insertString:@"-" atIndex:3];
            [mu insertString:@"-" atIndex:7];
            textField.text = mu;
            return NO;
        }
        //

        
        
        
        
        if (charCount == 3 || charCount == 7) {
            if ([string isEqualToString:@""]){
                return YES;
            }else{
                newString = [newString stringByAppendingString:@"-"];
            }
        }
        
        if (charCount == 4 || charCount == 8) {
            if (![string isEqualToString:@"-"]){
                newString = [newString substringToIndex:[newString length]-1];
                newString = [newString stringByAppendingString:@"-"];
            }
        }
        
        if ([newString rangeOfCharacterFromSet:[numSet invertedSet]].location != NSNotFound
            || [string rangeOfString:@"-"].location != NSNotFound
            || charCount > 12) {
            return NO;
        }
        
        textField.text = newString;
        return NO;
    }
    if (textField == _Zipcode_tf)
    {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERS_ONLY] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        return (([string isEqualToString:filtered])&&(newLength <= CHARACTER_LIMIT_ZIPCODE));
        
        
    }
    
    return YES;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [self.view endEditing:YES];
    
}

@end
