//
//  RoutesVC.m
//  openroadrides
//
//  Created by SrkIosEbiz on 30/05/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "RoutesVC.h"
#import "Public_Route_Cell.h"
#import "MPCoachMarks.h"

@interface RoutesVC ()
{
    NSArray *menu_Items_Array;
    NSMutableArray *check_strings_array;
    NSString *menu_Status;
    
    UIBarButtonItem *skip_btn_is;
}
@end

@implementation RoutesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    //[route_Privacy caseInsensitiveCompare:@"public"] == NSOrderedSame
    self.title = @"ROUTES";
     menu_Status=@"LEFT";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    
    self.routes_scrollLabel.hidden=YES;
    self.routes_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.routes_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.routes_scrollLabel.textColor = [UIColor blackColor];
    self.routes_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.routes_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.routes_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.routes_scrollLabel.scrollSpeed = 60; // pixels per second
    self.routes_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.routes_scrollLabel.fadeLength = 0.f;
    self.routes_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.routes_scrollLabel observeApplicationNotifications];
    
    self.routes_rideGoingBtn.hidden=YES;
    if (appDelegate.ridedashboard_home) {
        self.routes_scrollLabel.hidden=NO;
        self.routes_rideGoingBtn.hidden=NO;
        self.bottomConstraint_routesScrollView.constant=45;
    }
    //Side Menu
    swipe_right=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(menuright)];
    swipe_right.direction=UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipe_right];
    
    
    swipe_left=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(menuleft)];
    swipe_left.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipe_left];
    
    
    self.User_image.layer.cornerRadius=self.User_image.frame.size.width/2;
    self.User_image.clipsToBounds=YES;
    self.User_image.layer.borderColor = [UIColor whiteColor].CGColor;
    self.User_image.layer.borderWidth = 4.0;
    
      menu_Items_Array=@[@"Home",@"Dashboard",@"Privacy Policy",@"Terms & Conditions",@"Logout"];
    
    [_side_Menu_Table reloadData];
    self.User_Name.text=[NSString stringWithFormat:@"%@",[Defaults valueForKey:@"UserName"]];
    //
    
     changed_route_status=NO;
    
    
    
    
    
    NSLog(@"Array Of Frnds in Routes %@",_arrayOfFrndsInRoutes);
    NSLog(@"Array Of Groups in Routes %@",_arrayOfgroupsInRoutes);
//       appDelegate.select_Route_Id_For_CreateAndFreeRide=@"";
    NSLog(@"Not Selected Root %@",appDelegate.select_Route_Id_For_CreateAndFreeRide);
    
    
    
    if ([self.is_FROM_VC_OR_Start_Ride isEqualToString:@"YES"]) { //When Start Ride Clicking Time Only it Will Enabled
        
        _checkboxSelecteString=appDelegate.select_Route_Id_For_CreateAndFreeRide;
        _checkboxSelectedAddressString=appDelegate.select_Route_Address_For_CreateAndFreeRide;
        
        if ([_checkboxSelecteString length]==0 ||[_checkboxSelecteString isEqualToString:@"0"]) {
            _checkboxSelecteString=@"";
           
        }
        else{
           
        }
        UIButton *right_Done_Button=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
        right_Done_Button.backgroundColor=[UIColor blackColor];
        right_Done_Button.layer.cornerRadius=5;
        right_Done_Button.clipsToBounds=YES;
        [right_Done_Button setTitle:@"Done" forState:UIControlStateNormal];
        [right_Done_Button addTarget:self action:@selector(skip_clicked) forControlEvents:UIControlEventTouchUpInside];
        [right_Done_Button setTintColor:[UIColor whiteColor]];
        [right_Done_Button.titleLabel setFont:[UIFont fontWithName:@"Antonio-Regular" size:17.0]];
        skip_btn_is =[[UIBarButtonItem alloc] initWithCustomView:right_Done_Button];
    
        self.navigationItem.rightBarButtonItem = skip_btn_is;
        
        
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [leftBtn addTarget:self action:@selector(leftBackBtn:) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.frame = CGRectMake(0, 0, 25, 25);
   
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"back_Image"] forState:UIControlStateNormal];
       UIBarButtonItem *leftBackButton=[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    item0=leftBackButton;
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:item0, nil];
        
     
    }
    else if ([self.is_FROM_VC_OR_Start_Ride isEqualToString:@"CREATE RIDE"]||[self.is_FROM_VC_OR_Start_Ride isEqualToString:@"EDIT RIDE"])
    {
        
        UIBarButtonItem *skip_btn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(skip_clicked)];
        
        [skip_btn setTitleTextAttributes:@{
                                           NSFontAttributeName: [UIFont fontWithName:@"Antonio-Bold" size:17.0],
                                           NSForegroundColorAttributeName: [UIColor blackColor]
                                           } forState:UIControlStateNormal];
        
        self.navigationItem.rightBarButtonItem = skip_btn;
        
        
        UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        [leftBtn addTarget:self action:@selector(leftBackBtn:) forControlEvents:UIControlEventTouchUpInside];
        leftBtn.frame = CGRectMake(0, 0, 25, 25);
        [leftBtn setBackgroundImage:[UIImage imageNamed:@"back_Image"] forState:UIControlStateNormal];
        UIBarButtonItem *leftBackButton=[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
        item0=leftBackButton;
        self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:item0, nil];
        _checkboxSelecteString=appDelegate.select_Route_Id_For_CreateAndFreeRide;
        _checkboxSelectedAddressString=appDelegate.select_Route_Address_For_CreateAndFreeRide;
        
        //Display Annotation
        // Show coach marks
//        BOOL coachMarksShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"MPCoachMarksShown_SelectRoute"];
//        if (coachMarksShown == NO) {
//            // Don't show again
//            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MPCoachMarksShown_SelectRoute"];
//            [[NSUserDefaults standardUserDefaults] synchronize];
//            
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
//                 [self showAnnotation];
//            });
//        }

    }
    else{
        UIBarButtonItem *menuButton=[[UIBarButtonItem alloc]initWithImage:
                                     [[UIImage imageNamed:@"sidemenuicon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
                                                                    style:UIBarButtonItemStylePlain target:self action:@selector(Menu_Action:)];
        
        self.navigationItem.leftBarButtonItem = menuButton;
        
        
        //Display Annotation
        // Show coach marks
//        BOOL coachMarksShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"MPCoachMarksShown_Routes"];
//        if (coachMarksShown == NO) {
//            // Don't show again
//            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MPCoachMarksShown_Routes"];
//            [[NSUserDefaults standardUserDefaults] synchronize];
//            
//            [self showAnnotation];
//        }
    }
    
    CGFloat content_View_Width=SCREEN_WIDTH-40;
    self.content_View_Width_Constraint.constant=content_View_Width*2;
    [self.view layoutIfNeeded];
    
    
    [self.p_Routes_Table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.my_Routes_Table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    

    
    
    indicaterview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    activeIndicatore.backgroundColor=[UIColor clearColor];
    activeIndicatore = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.frame = CGRectMake(0.0, 0.0, 80, 80);
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    [indicaterview addSubview:activeIndicatore];
    [self.view addSubview:indicaterview];
    [indicaterview bringSubviewToFront:self.view];
    [activeIndicatore startAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    indicaterview.hidden=YES;

    
    check_strings_array=[[NSMutableArray alloc]init];
    [check_strings_array addObject:@"<null>"];
    [check_strings_array addObject:@"null"];
    [check_strings_array addObject:@""];
    [check_strings_array addObject:@"(null)"];
    
    
    
    self.noData_label_punlic_routes_label.hidden=YES;
    self.noData_label_myroutes.hidden=YES;
    
//    self.main_Scrole_View.hidden=YES;
    view_Status=@"";
    [self get_Routes_Data];
    
    _routes_searchBar.hidden=NO;
    _routes_searchBar.delegate=self;
    self.routes_searchBar.tintColor = [UIColor whiteColor];
//    self.routes_searchBar.barTintColor=[UIColor lightGrayColor];
    self.routes_searchBar.barTintColor= ROUTES_CELL_BG_COLOUR1;
}
-(void)callAnnotationsMethod
{
    if ([self.is_FROM_VC_OR_Start_Ride isEqualToString:@"YES"] || [self.is_FROM_VC_OR_Start_Ride isEqualToString:@"CREATE RIDE"]||[self.is_FROM_VC_OR_Start_Ride isEqualToString:@"EDIT RIDE"]) {
        BOOL coachMarksShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"MPCoachMarksShown_StartRide"];
        if (coachMarksShown == NO) {
            // Don't show again
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MPCoachMarksShown_StartRide"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [self showAnnotation];
//            });
        }
    }
//    else if ([self.is_FROM_VC_OR_Start_Ride isEqualToString:@"CREATE RIDE"]||[self.is_FROM_VC_OR_Start_Ride isEqualToString:@"EDIT RIDE"])
//    {
//        BOOL coachMarksShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"MPCoachMarksShown_SelectRoute"];
//        if (coachMarksShown == NO) {
//            // Don't show again
//            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MPCoachMarksShown_SelectRoute"];
//            [[NSUserDefaults standardUserDefaults] synchronize];
//            
////            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
//                [self showAnnotation];
////            });
//            
//            
//        }
//    }
    else
    {
        //Display Annotation
        // Show coach marks
        BOOL coachMarksShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"MPCoachMarksShown_Routes"];
        if (coachMarksShown == NO) {
            // Don't show again
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MPCoachMarksShown_Routes"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self showAnnotation];
        }
    }


}
#pragma mark - Annotations
-(void)showAnnotation
{
    [self.view layoutIfNeeded];
    
    // Setup coach marks
    CGRect coachmark1 = CGRectMake (_routes_searchBar.frame.origin.x,_routes_searchBar.frame.origin.y+65, _routes_searchBar.frame.size.width,_routes_searchBar.frame.size.height);
    CGRect coachmark2 = CGRectMake( _p_Routes_Table.frame.origin.x+21, _routes_searchBar.frame.origin.y+_routes_searchBar.frame.size.height+89, 40, 40);
    CGRect coachmark3 = CGRectMake( ([UIScreen mainScreen].bounds.size.width - 45), 20, self.navigationController.navigationBar.frame.size.height, self.navigationController.navigationBar.frame.size.height);

    NSArray *coachMarks;
    
    if ([self.is_FROM_VC_OR_Start_Ride isEqualToString:@"YES"])
    {
        if (titles_For_Public_Routes.count==0) {
            // Setup coach marks
            // Setup coach marks
            coachMarks = @[
                           @{
                               @"rect": [NSValue valueWithCGRect:coachmark1],
                               @"caption": @"Search here to discover new routes",
                               @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
                               @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_CENTER]
                               //@"showArrow":[NSNumber numberWithBool:YES]
                               },
                           @{
                               @"rect": [NSValue valueWithCGRect:coachmark3],
                               @"caption": @"Tap here to skip",
                               @"shape": [NSNumber numberWithInteger:SHAPE_SQUARE],
                               @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                               @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_RIGHT],
                               //@"showArrow":[NSNumber numberWithBool:YES]
                               },
                           ];

        }
        else
        {
            
            // Setup coach marks
            coachMarks = @[
                           @{
                               @"rect": [NSValue valueWithCGRect:coachmark1],
                               @"caption": @"Search here to discover new routes",
                               @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
                               @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_CENTER]
                               //@"showArrow":[NSNumber numberWithBool:YES]
                               },
                           @{
                               @"rect": [NSValue valueWithCGRect:coachmark2],
                               @"caption": @"Select the route here",
                               @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                               @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                               @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_LEFT],
                               //@"showArrow":[NSNumber numberWithBool:YES]
                               },
                           @{
                               @"rect": [NSValue valueWithCGRect:coachmark3],
                               @"caption": @"Tap here to skip",
                               @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                               @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                               @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_RIGHT],
                               //@"showArrow":[NSNumber numberWithBool:YES]
                               },

                           ];
  
        }
    }
    else if ([self.is_FROM_VC_OR_Start_Ride isEqualToString:@"CREATE RIDE"]||[self.is_FROM_VC_OR_Start_Ride isEqualToString:@"EDIT RIDE"])
    {
        
        if (titles_For_Public_Routes.count==0) {
            // Setup coach marks
            // Setup coach marks
            coachMarks = @[
                           @{
                               @"rect": [NSValue valueWithCGRect:coachmark1],
                               @"caption": @"Search here to discover new routes",
                               @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
                               @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_CENTER]
                               //@"showArrow":[NSNumber numberWithBool:YES]
                               },
                           @{
                               @"rect": [NSValue valueWithCGRect:coachmark3],
                               @"caption": @"Tap here to complete",
                               @"shape": [NSNumber numberWithInteger:SHAPE_SQUARE],
                               @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                               @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_RIGHT],
                               //@"showArrow":[NSNumber numberWithBool:YES]
                               },
                           ];
            
        }
        else
        // Setup coach marks
        coachMarks = @[
                       @{
                           @"rect": [NSValue valueWithCGRect:coachmark1],
                           @"caption": @"Search here to discover new routes",
                           @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
                           @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_CENTER]
                           //@"showArrow":[NSNumber numberWithBool:YES]
                           },
                       @{
                           @"rect": [NSValue valueWithCGRect:coachmark2],
                           @"caption": @"Select the route here",
                           @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                           @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                           @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_LEFT],
                           //@"showArrow":[NSNumber numberWithBool:YES]
                           },
                       @{
                           @"rect": [NSValue valueWithCGRect:coachmark3],
                           @"caption": @"Tap here to complete",
                           @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                           @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                           @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_RIGHT],
                           //@"showArrow":[NSNumber numberWithBool:YES]
                           },
                       ];
    }
    else
    {
        // Setup coach marks
        coachMarks = @[
                                @{
                                    @"rect": [NSValue valueWithCGRect:coachmark1],
                                    @"caption": @"Search here to discover new routes",
                                    @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
                                    @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_CENTER]
                                    //@"showArrow":[NSNumber numberWithBool:YES]
                                    },
                            ];
    }
    
    MPCoachMarks *coachMarksView = [[MPCoachMarks alloc] initWithFrame:self.navigationController.view.bounds coachMarks:coachMarks];
    //[self.navigationController.view addSubview:coachMarksView];
    [[UIApplication sharedApplication].keyWindow addSubview:coachMarksView];
    [coachMarksView start];
}

-(void)viewWillAppear:(BOOL)animated{
     _side_Menu_view.hidden=YES;
     self.navigationController.navigationBar.hidden=NO;
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
     [self display_user_Profile];
}


-(void)display_user_Profile{
    
    NSString *user_id=[Defaults valueForKey:User_ID];
    [SHARED_API DisplayUserProfile:user_id withSuccess:^(NSDictionary *response) {
        
        [self user_Profile_Data_Sucsess:response];
        
    } onfailure:^(NSError *theError) {
    }];
    
}
-(void)user_Profile_Data_Sucsess:(NSDictionary *)Profile_data{
    if ([[Profile_data valueForKey:STATUS] isEqualToString:SUCCESS]) {
        NSDictionary *user_data=[Profile_data valueForKey:@"data"];
        self.User_Name.text=[user_data valueForKey:@"name"];
        [Defaults setObject:self.User_Name.text forKey:@"UserName"];
        
        NSString *profile_image_str=[NSString stringWithFormat:@"%@",[user_data valueForKey:@"profile_image"]];
        
        if (![check_strings_array containsObject:profile_image_str] && ![profile_image_str isEqualToString:@"<null>"])
        {
            NSURL*url=[NSURL URLWithString:profile_image_str];
            [self.User_image sd_setImageWithURL:url
                               placeholderImage:[UIImage imageNamed:@"user_Placeholder"]
                                        options:SDWebImageRefreshCached];
        }
        
        
        friends_count = [[NSString stringWithFormat:@"%@",[[Profile_data valueForKey:@"data"] valueForKey:@"total_friends"]] intValue];
        
        
        groups_count = [[NSString stringWithFormat:@"%@",[[Profile_data valueForKey:@"data"] valueForKey:@"total_groups"]] intValue];
        
        [_side_Menu_Table reloadData];
    }
}

-(void)skip_clicked
{
    if ([self.is_FROM_VC_OR_Start_Ride isEqualToString:@"YES"])
    {
        
        if ([_checkboxSelecteString isEqualToString:appDelegate.select_Route_Id_For_CreateAndFreeRide]) {
            
        }
        else
        {
            appDelegate.select_Route_Id_For_CreateAndFreeRide=[NSString stringWithFormat:@"%@",_checkboxSelecteString];
            appDelegate.select_Route_Address_For_CreateAndFreeRide=[NSString stringWithFormat:@"%@",_checkboxSelectedAddressString];
            
            [[NSNotificationCenter defaultCenter]postNotificationName:@"Start_Ride_From_Route_Select_Deselect" object:self];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if ([self.is_FROM_VC_OR_Start_Ride isEqualToString:@"CREATE RIDE"])
    {
        [self.navigationController popViewControllerAnimated:YES];
        NSLog(@"Slected Route ID From Create Ride %@",_checkboxSelecteString);
        appDelegate.select_Route_Id_For_CreateAndFreeRide=[NSString stringWithFormat:@"%@",_checkboxSelecteString];
        appDelegate.select_Route_Address_For_CreateAndFreeRide=[NSString stringWithFormat:@"%@",selectedRouteStartAddressString_checkbox];
        appDelegate.start_address_latitude=[NSString stringWithFormat:@"%@",selectedRouteAddressLatitude];
        appDelegate.start_address_longtitude=[NSString stringWithFormat:@"%@",selectedRouteAddressLongtitude];
    }
    else if ([self.is_FROM_VC_OR_Start_Ride isEqualToString:@"EDIT RIDE"])
    {
        [self.navigationController popViewControllerAnimated:YES];
        NSLog(@"Slected Route ID From Create Ride %@",_checkboxSelecteString);
        appDelegate.select_Route_Id_For_CreateAndFreeRide=[NSString stringWithFormat:@"%@",_checkboxSelecteString];
        appDelegate.select_Route_Address_For_CreateAndFreeRide=[NSString stringWithFormat:@"%@",selectedRouteStartAddressString_checkbox];
        appDelegate.start_address_latitude=[NSString stringWithFormat:@"%@",selectedRouteAddressLatitude];
        appDelegate.start_address_longtitude=[NSString stringWithFormat:@"%@",selectedRouteAddressLongtitude];
    }
}


-(void)back{
    
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)leftBackBtn:(UIButton *)sender
{
    self.side_Menu_view.hidden=YES;
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)get_Routes_Data
{
    
    indicaterview.hidden=NO;
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [SHARED_HELPER showAlert:Nonetwork];
        indicaterview.hidden=YES;

        return;
    }
    NSString *user_id=[Defaults valueForKey:User_ID];

 
    NSMutableDictionary *routes_dict=[[NSMutableDictionary alloc]init];
    [routes_dict setValue:user_id forKey:@"user_id"];
    
    if ([_is_FROM_VC_OR_Start_Ride isEqualToString:@"YES"]) {
        [routes_dict setObject:[NSNumber numberWithDouble:appDelegate.app_current_lattitude] forKey:@"latitude"];
        [routes_dict setObject:[NSNumber numberWithDouble:appDelegate.app_current_longitude] forKey:@"longitude"];
    }
    else
    {
        [routes_dict setValue:@"" forKey:@"latitude"];
        [routes_dict setValue:@"" forKey:@"longitude"];
    }
    if ([view_Status isEqualToString:@""]||[view_Status isEqualToString:@"PUBLIC"]) {
       [routes_dict setValue:@"0" forKey:@"request_type"];
        titles_For_Public_Routes=[[NSMutableArray alloc]init];
        _titles_Filter_Public_Routes=[[NSMutableArray alloc]init];
    }
    else{
        [routes_dict setValue:@"1" forKey:@"request_type"];
         titles_For_My_Routes=[[NSMutableArray alloc]init];
        _titles_Filter_My_Routes=[[NSMutableArray alloc]init];
    }
    
    [SHARED_API Routes_Api:routes_dict withSuccess:^(NSDictionary *response) {
        NSLog(@"%@",response);
        [self Route_Service_Sucsess:response];
        indicaterview.hidden=YES;
    } onfailure:^(NSError *theError) {
        [SHARED_HELPER showAlert:ServiceFail];
        indicaterview.hidden=YES;
    }];
    
}
-(void)Route_Service_Sucsess:(NSDictionary *)respnse{
   
   
    if ([[respnse valueForKey:STATUS] isEqualToString:SUCCESS]) {
    NSArray *Routes_data=[respnse valueForKey:@"routes"];
    if (Routes_data.count>0) {
        for (int i=0; i<Routes_data.count; i++) {
            NSDictionary *data_Dict_is=[Routes_data objectAtIndex:i];
            NSString *route_Privacy=[data_Dict_is valueForKey:@"route_privacy"];
            if ([route_Privacy caseInsensitiveCompare:@"public"]== NSOrderedSame&&([view_Status isEqualToString:@""]||[view_Status isEqualToString:@"PUBLIC"])) {
                [titles_For_Public_Routes addObject:data_Dict_is];
                [_titles_Filter_Public_Routes addObject:data_Dict_is];
            }
            else{
                [titles_For_My_Routes addObject:data_Dict_is];
                [_titles_Filter_My_Routes addObject:data_Dict_is];
            }
        }
       
        self.main_Scrole_View.hidden=NO;
         if ([view_Status isEqualToString:@""]||[view_Status isEqualToString:@"PUBLIC"]) {
              view_Status=@"PUBLIC";
              Public_Called=@"YES";
              [self.p_Routes_Table reloadData];
         }
         else{
              [self.my_Routes_Table reloadData];
              my_Routes_Called=@"YES";
         }
    }
    else{
        if ([view_Status isEqualToString:@""]||[view_Status isEqualToString:@"PUBLIC"]) {
            if (titles_For_Public_Routes.count>0) {
                self.noData_label_punlic_routes_label.hidden=YES;
            }
            else{
                self.noData_label_punlic_routes_label.hidden=NO;
            }
            
        }
        else{
            if (titles_For_My_Routes.count>0) {
                self.noData_label_myroutes.hidden=YES;
            }
            else{
                self.noData_label_myroutes.hidden=NO;
            }
         
        }
        
//        [SHARED_HELPER showAlert:NO_Routes];
        [self service_fail_OR_EmptyData];
     }
        [self callAnnotationsMethod];
    }
    else if ([[respnse valueForKey:STATUS] isEqualToString:FAIL])
    {
        if ([view_Status isEqualToString:@""]||[view_Status isEqualToString:@"PUBLIC"]) {
            if (titles_For_Public_Routes.count>0) {
                self.noData_label_punlic_routes_label.hidden=YES;
            }
            else
            {
                self.noData_label_punlic_routes_label.hidden=NO;
            }
        }
        else{
            if (titles_For_My_Routes.count>0) {
                self.noData_label_myroutes.hidden=YES;
            }
            else
            {
                self.noData_label_myroutes.hidden=NO;
            }
        }
        
//        [SHARED_HELPER showAlert:NO_Routes];
        [self callAnnotationsMethod];
    }
    else{
        [SHARED_HELPER showAlert:ServiceFail];
         [self service_fail_OR_EmptyData];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    self.p_Routes_Btn_View.userInteractionEnabled=YES;
    self.my_Routes_btn_View.userInteractionEnabled=YES;
    if (scrollView.tag==5) {
        if (scrollView.contentOffset.x==0)
        {
            [self Public_Route_Action:0];
        }
        if (scrollView.contentOffset.x==SCREEN_WIDTH-40)
        {
            [self My_Route_Action:0];
        }
    }
}

- (IBAction)Public_Route_Action:(id)sender {
    
    
    
    view_Status=@"PUBLIC";
    self.p_Routes_Btn_View.backgroundColor=[UIColor whiteColor];
    self.my_Routes_btn_View.backgroundColor=[UIColor blackColor];
    [self.main_Scrole_View setContentOffset:CGPointMake((0), _main_Scrole_View.contentOffset.y)];
    self.public_Routes_Title_lavel.textColor=[UIColor whiteColor];
    self.my_Routes_Label.textColor=APP_YELLOW_COLOR;
    if ([Public_Called isEqualToString:@"YES"]) {
        
            routesIsFilterd=NO;
            [_routes_searchBar resignFirstResponder];
            _routes_searchBar.text=@"";
            _routes_searchBar.showsCancelButton=NO;
            [_p_Routes_Table reloadData];
        
    }
    else{
        routesIsFilterd=NO;
        _routes_searchBar.text=@"";
        [_routes_searchBar resignFirstResponder];
        _routes_searchBar.showsCancelButton=NO;
       
        [self get_Routes_Data];
    }
    
}

- (IBAction)My_Route_Action:(id)sender {
    view_Status=@"MY";
    self.p_Routes_Btn_View.backgroundColor=[UIColor blackColor];
    self.my_Routes_btn_View.backgroundColor=[UIColor whiteColor];
    [self.main_Scrole_View setContentOffset:CGPointMake((SCREEN_WIDTH-40), self.main_Scrole_View.contentOffset.y)];
    self.my_Routes_Label.textColor=[UIColor whiteColor];
    self.public_Routes_Title_lavel.textColor=APP_YELLOW_COLOR;
    
    if ([my_Routes_Called isEqualToString:@"YES"]) {
        routesIsFilterd=NO;
        [_routes_searchBar resignFirstResponder];
        _routes_searchBar.text=@"";
        _routes_searchBar.showsCancelButton=NO;
        [_my_Routes_Table reloadData];
        
    }
    else{
        routesIsFilterd=NO;
        [_routes_searchBar resignFirstResponder];
        _routes_searchBar.text=@"";
        _routes_searchBar.showsCancelButton=NO;
        [self get_Routes_Data];
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==_side_Menu_Table) {
         return menu_Items_Array.count;
    }

    if (tableView ==self.p_Routes_Table)
    {
        
        if (titles_For_Public_Routes.count>0)
        {
            if (routesIsFilterd) {
                return [_titles_Filter_Public_Routes count];
            }
            return titles_For_Public_Routes.count;
        }
        
        else{
            return 0;
        }
    
    }
//    else if ([view_Status isEqualToString:@"MY"])
    else if (tableView ==self.my_Routes_Table)
    {
        if (titles_For_My_Routes.count>0)
        {
            if (routesIsFilterd) {
                return [_titles_Filter_My_Routes count];
            }
            return titles_For_My_Routes.count;
        }
        else{
            return 0;
        }

    }
    else{
        return 0;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_side_Menu_Table) {
        return 45;
    }
    return 90;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView==_side_Menu_Table) {
        static NSString *simpleTableIdentifier = @"MenuTableViewCell";
        MenuTableViewCell *cell = (MenuTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        self.side_Menu_Table.separatorStyle=UITableViewCellSeparatorStyleNone;
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MenuTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.menu_Item_Label.text=[menu_Items_Array objectAtIndex:indexPath.row];
        cell.menu_Item_Label.textColor=[UIColor colorWithRed:255/255.0 green:222/255.0 blue:0/255.0 alpha:1.0];
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = [UIColor colorWithRed:255/255.0 green:222/255.0 blue:0/255.0 alpha:1.0];
        [cell setSelectedBackgroundView:bgColorView];
        cell.menu_Item_Label.highlightedTextColor=[UIColor blackColor];
        tableView.allowsMultipleSelection=NO;
        
        
        cell.menu_item_Count_Lab.hidden=YES;
        if (indexPath.row==0)
        {
            cell.menu_Item_iCon.image=[UIImage imageNamed:@"homeicon"];
            
        }
        else if (indexPath.row==1)
        {
            cell.menu_Item_iCon.image=[UIImage imageNamed:@"Dashboard_blue"];
        }
        else if (indexPath.row==2)
            
        {
            cell.menu_Item_iCon.image=[UIImage imageNamed:@"privacy"];
        }
        else if (indexPath.row==3)
            
        {
            cell.menu_Item_iCon.image=[UIImage imageNamed:@"terms"];
        }
        else if (indexPath.row==4)
            
        {
            cell.menu_Item_iCon.image=[UIImage imageNamed:@"Logout"];
        }
        
        return cell;
    }
   else  if ([self.is_FROM_VC_OR_Start_Ride isEqualToString:@"YES"] || [self.is_FROM_VC_OR_Start_Ride isEqualToString:@"CREATE RIDE"]||[self.is_FROM_VC_OR_Start_Ride isEqualToString:@"EDIT RIDE"]) {
        
//    if ([view_Status isEqualToString:@"PUBLIC"] || [view_Status isEqualToString:@""])
        if (tableView ==self.p_Routes_Table)
    {
        static NSString *simpleTableIdentifier = @"Public_Route_Cell";
        Public_Route_Cell *cell = (Public_Route_Cell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"Public_Route_Cell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        if (routesIsFilterd) {
            if (indexPath.row%2==0) {
                cell.back_Ground_view.backgroundColor=ROUTES_CELL_BG_COLOUR1;
            }
            else
                cell.back_Ground_view.backgroundColor=ROUTES_CELL_BG_COLOUR2;
            
            
            [cell.checkbox_btn_routes addTarget:self action:@selector(onClickCheckbox:) forControlEvents:UIControlEventTouchUpInside];
            cell.checkbox_btn_routes.tag=indexPath.row;
            
            
            cell.title_Label.text=[[_titles_Filter_Public_Routes objectAtIndex:indexPath.row] valueForKey:@"route_title"];
            
            cell.description_label.text=[[_titles_Filter_Public_Routes objectAtIndex:indexPath.row] valueForKey:@"route_description"];
            if ([cell.description_label.text isEqualToString:@""]) {
                cell.des_icon.hidden=YES;
            }
            else{
                cell.des_icon.hidden=NO;
            }
            cell.comments_Label.text=[NSString stringWithFormat:@"Comments( %@ )",[[_titles_Filter_Public_Routes objectAtIndex:indexPath.row] valueForKey:@"route_comments_count"]];
            NSMutableAttributedString *text =
            [[NSMutableAttributedString alloc]
             initWithAttributedString: cell.comments_Label.attributedText];
            
            [text addAttribute:NSForegroundColorAttributeName
                         value:APP_BLUE_COLOR
                         range:NSMakeRange(9,2)];
            [text addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(9,2)];
            [cell.comments_Label setAttributedText: text];
            
            [cell.rating_Bar_View initRateBar];
            [cell.rating_Bar_View  setUserInteractionEnabled:NO];
            
            NSString *rate_value=[NSString stringWithFormat:@"%@",[[_titles_Filter_Public_Routes objectAtIndex:indexPath.row] valueForKey:@"route_ratings_count"]];
            RBRatings rate;
            if ([rate_value isEqualToString:@"<null>"]) {
                rate =0;
            }
            else{
                rate =[[[_titles_Filter_Public_Routes objectAtIndex:indexPath.row] valueForKey:@"route_ratings_count"] floatValue]*2;
            }
            [cell.rating_Bar_View setRatings:rate];
            [cell.rating_Bar_View initRateBar];
            [cell.rating_Bar_View setUserInteractionEnabled:NO];
            
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            
            
            NSString*public_routeID=[NSString stringWithFormat:@"%@",[[_titles_Filter_Public_Routes objectAtIndex:indexPath.row] objectForKey:@"route_id"]];
            
            //        if ([checkboxSelecteString isEqualToString:[NSString stringWithFormat:@"%ld",(long)indexPath.row]])
            //        if ([checkboxSelecteString containsString:public_routeID])
            if ([_checkboxSelecteString isEqualToString:public_routeID])
            {
                [cell.checkbox_btn_routes setBackgroundImage:[UIImage imageNamed:@"checkbox_fill"] forState:UIControlStateNormal];
            }
            else{
                [cell.checkbox_btn_routes setBackgroundImage:[UIImage imageNamed:@"checkbox_empty"] forState:UIControlStateNormal];
            }

        }
        else
        {
            // Without Search
            if (indexPath.row%2==0) {
                cell.back_Ground_view.backgroundColor=ROUTES_CELL_BG_COLOUR1;
            }
            else
                cell.back_Ground_view.backgroundColor=ROUTES_CELL_BG_COLOUR2;
            
            
            [cell.checkbox_btn_routes addTarget:self action:@selector(onClickCheckbox:) forControlEvents:UIControlEventTouchUpInside];
            cell.checkbox_btn_routes.tag=indexPath.row;
            
            
            cell.title_Label.text=[[titles_For_Public_Routes objectAtIndex:indexPath.row] valueForKey:@"route_title"];
            
            cell.description_label.text=[[titles_For_Public_Routes objectAtIndex:indexPath.row] valueForKey:@"route_description"];
            if ([cell.description_label.text isEqualToString:@""]) {
                cell.des_icon.hidden=YES;
            }
            else{
                cell.des_icon.hidden=NO;
            }
            cell.comments_Label.text=[NSString stringWithFormat:@"Comments( %@ )",[[titles_For_Public_Routes objectAtIndex:indexPath.row] valueForKey:@"route_comments_count"]];
            NSMutableAttributedString *text =
            [[NSMutableAttributedString alloc]
             initWithAttributedString: cell.comments_Label.attributedText];
            
            [text addAttribute:NSForegroundColorAttributeName
                         value:APP_BLUE_COLOR
                         range:NSMakeRange(9,2)];
            [text addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(9,2)];
            [cell.comments_Label setAttributedText: text];
            
            [cell.rating_Bar_View initRateBar];
            [cell.rating_Bar_View  setUserInteractionEnabled:NO];
            
            NSString *rate_value=[NSString stringWithFormat:@"%@",[[titles_For_Public_Routes objectAtIndex:indexPath.row] valueForKey:@"route_ratings_count"]];
            RBRatings rate;
            if ([rate_value isEqualToString:@"<null>"]) {
                rate =0;
            }
            else{
                rate =[[[titles_For_Public_Routes objectAtIndex:indexPath.row] valueForKey:@"route_ratings_count"] floatValue]*2;
            }
            [cell.rating_Bar_View setRatings:rate];
            [cell.rating_Bar_View initRateBar];
            [cell.rating_Bar_View setUserInteractionEnabled:NO];
            
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            
            
            NSString*public_routeID=[NSString stringWithFormat:@"%@",[[titles_For_Public_Routes objectAtIndex:indexPath.row] objectForKey:@"route_id"]];
            
            //        if ([checkboxSelecteString isEqualToString:[NSString stringWithFormat:@"%ld",(long)indexPath.row]])
            //        if ([checkboxSelecteString containsString:public_routeID])
            if ([_checkboxSelecteString isEqualToString:public_routeID])
            {
                [cell.checkbox_btn_routes setBackgroundImage:[UIImage imageNamed:@"checkbox_fill"] forState:UIControlStateNormal];
            }
            else{
                [cell.checkbox_btn_routes setBackgroundImage:[UIImage imageNamed:@"checkbox_empty"] forState:UIControlStateNormal];
            }

        }
        
        return cell;
    }
    else
    {
        static NSString *simpleTableIdentifier = @"Public_Route_Cell";
        Public_Route_Cell *cell = (Public_Route_Cell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"Public_Route_Cell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        if (routesIsFilterd) {
            if (indexPath.row%2==0) {
                cell.back_Ground_view.backgroundColor=ROUTES_CELL_BG_COLOUR1;
            }
            else
                cell.back_Ground_view.backgroundColor=ROUTES_CELL_BG_COLOUR2;
            
            cell.title_Label.text=[[_titles_Filter_My_Routes objectAtIndex:indexPath.row] valueForKey:@"route_title"];
            cell.description_label.text=[[_titles_Filter_My_Routes objectAtIndex:indexPath.row] valueForKey:@"route_description"];
            if ([cell.description_label.text isEqualToString:@""]) {
                cell.des_icon.hidden=YES;
            }
            else{
                cell.des_icon.hidden=NO;
            }
            [cell.checkbox_btn_routes addTarget:self action:@selector(onClickCheckbox:) forControlEvents:UIControlEventTouchUpInside];
            cell.checkbox_btn_routes.tag=indexPath.row;
            
            cell.comments_Label.text=[NSString stringWithFormat:@"Comments( %@ )",[[_titles_Filter_My_Routes objectAtIndex:indexPath.row] valueForKey:@"route_comments_count"]];
            NSMutableAttributedString *text =
            [[NSMutableAttributedString alloc]
             initWithAttributedString: cell.comments_Label.attributedText];
            
            [text addAttribute:NSForegroundColorAttributeName
                         value:APP_BLUE_COLOR
                         range:NSMakeRange(9,2)];
            [text addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(9,2)];
            [cell.comments_Label setAttributedText: text];
            
            
            [cell.rating_Bar_View initRateBar];
            [cell.rating_Bar_View  setUserInteractionEnabled:NO];
            
            
            NSString *rate_value=[NSString stringWithFormat:@"%@",[[_titles_Filter_My_Routes objectAtIndex:indexPath.row] valueForKey:@"route_ratings_count"]];
            RBRatings rate;
            if ([rate_value isEqualToString:@"<null>"]) {
                rate =0;
            }
            else{
                rate =[[[_titles_Filter_My_Routes objectAtIndex:indexPath.row] valueForKey:@"route_ratings_count"] floatValue]*2;
            }
            
            [cell.rating_Bar_View setRatings:rate];
            [cell.rating_Bar_View initRateBar];
            [cell.rating_Bar_View setUserInteractionEnabled:NO];
            
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            
            NSString*myRoutes_routeID=[NSString stringWithFormat:@"%@",[[_titles_Filter_My_Routes objectAtIndex:indexPath.row] objectForKey:@"route_id"]];
            
            
            //        if ([checkboxSelecteString isEqualToString:[NSString stringWithFormat:@"%ld",(long)indexPath.row]])
            //        if ([checkboxSelecteString containsString:myRoutes_routeID])
            if ([_checkboxSelecteString containsString:myRoutes_routeID])
                
            {
                [cell.checkbox_btn_routes setBackgroundImage:[UIImage imageNamed:@"checkbox_fill"] forState:UIControlStateNormal];
            }
            else{
                [cell.checkbox_btn_routes setBackgroundImage:[UIImage imageNamed:@"checkbox_empty"] forState:UIControlStateNormal];
            }
         
        }
        else
        {
            // Without Search
            if (indexPath.row%2==0) {
                cell.back_Ground_view.backgroundColor=ROUTES_CELL_BG_COLOUR1;
            }
            else
                cell.back_Ground_view.backgroundColor=ROUTES_CELL_BG_COLOUR2;
            
            cell.title_Label.text=[[titles_For_My_Routes objectAtIndex:indexPath.row] valueForKey:@"route_title"];
            cell.description_label.text=[[titles_For_My_Routes objectAtIndex:indexPath.row] valueForKey:@"route_description"];
            if ([cell.description_label.text isEqualToString:@""]) {
                cell.des_icon.hidden=YES;
            }
            else{
                cell.des_icon.hidden=NO;
            }
            [cell.checkbox_btn_routes addTarget:self action:@selector(onClickCheckbox:) forControlEvents:UIControlEventTouchUpInside];
            cell.checkbox_btn_routes.tag=indexPath.row;
            
            cell.comments_Label.text=[NSString stringWithFormat:@"Comments( %@ )",[[titles_For_My_Routes objectAtIndex:indexPath.row] valueForKey:@"route_comments_count"]];
            NSMutableAttributedString *text =
            [[NSMutableAttributedString alloc]
             initWithAttributedString: cell.comments_Label.attributedText];
            
            [text addAttribute:NSForegroundColorAttributeName
                         value:APP_BLUE_COLOR
                         range:NSMakeRange(9,2)];
            [text addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(9,2)];
            [cell.comments_Label setAttributedText: text];
            
            
            [cell.rating_Bar_View initRateBar];
            [cell.rating_Bar_View  setUserInteractionEnabled:NO];
            
            
            NSString *rate_value=[NSString stringWithFormat:@"%@",[[titles_For_My_Routes objectAtIndex:indexPath.row] valueForKey:@"route_ratings_count"]];
            RBRatings rate;
            if ([rate_value isEqualToString:@"<null>"]) {
                rate =0;
            }
            else{
                rate =[[[titles_For_My_Routes objectAtIndex:indexPath.row] valueForKey:@"route_ratings_count"] floatValue]*2;
            }
            
            [cell.rating_Bar_View setRatings:rate];
            [cell.rating_Bar_View initRateBar];
            [cell.rating_Bar_View setUserInteractionEnabled:NO];
            
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            
            NSString*myRoutes_routeID=[NSString stringWithFormat:@"%@",[[titles_For_My_Routes objectAtIndex:indexPath.row] objectForKey:@"route_id"]];
            
            
            //        if ([checkboxSelecteString isEqualToString:[NSString stringWithFormat:@"%ld",(long)indexPath.row]])
            //        if ([checkboxSelecteString containsString:myRoutes_routeID])
            if ([_checkboxSelecteString containsString:myRoutes_routeID])
                
            {
                [cell.checkbox_btn_routes setBackgroundImage:[UIImage imageNamed:@"checkbox_fill"] forState:UIControlStateNormal];
            }
            else{
                [cell.checkbox_btn_routes setBackgroundImage:[UIImage imageNamed:@"checkbox_empty"] forState:UIControlStateNormal];
            }
        }
   
        return cell;
      }
    }
    else{
//        From Dashboard od sidemenu Routes
        
        if ([view_Status isEqualToString:@"PUBLIC"] || [view_Status isEqualToString:@""]) {
            static NSString *simpleTableIdentifier = @"Public_Route_Cell";
            Public_Route_Cell *cell = (Public_Route_Cell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"Public_Route_Cell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            if (routesIsFilterd) {
                if (indexPath.row%2==0) {
                    cell.back_Ground_view.backgroundColor=ROUTES_CELL_BG_COLOUR1;
                }
                else
                    cell.back_Ground_view.backgroundColor=ROUTES_CELL_BG_COLOUR2;
                
                cell.checkbox_btn_routes.hidden=YES;
                [cell.checkbox_btn_routes addTarget:self action:@selector(onClickCheckbox:) forControlEvents:UIControlEventTouchUpInside];
                cell.checkbox_btn_routes.tag=indexPath.row;
                cell.leadingConstraint_title_routes.constant=10;
                [cell.title_Label updateConstraints];
                
                cell.title_Label.text=[[_titles_Filter_Public_Routes objectAtIndex:indexPath.row] valueForKey:@"route_title"];
                
                cell.description_label.text=[[_titles_Filter_Public_Routes objectAtIndex:indexPath.row] valueForKey:@"route_description"];
                if ([cell.description_label.text isEqualToString:@""]) {
                    cell.des_icon.hidden=YES;
                }
                else{
                    cell.des_icon.hidden=NO;
                }
                cell.comments_Label.text=[NSString stringWithFormat:@"Comments( %@ )",[[_titles_Filter_Public_Routes objectAtIndex:indexPath.row] valueForKey:@"route_comments_count"]];
                NSMutableAttributedString *text =
                [[NSMutableAttributedString alloc]
                 initWithAttributedString: cell.comments_Label.attributedText];
                
                [text addAttribute:NSForegroundColorAttributeName
                             value:APP_BLUE_COLOR
                             range:NSMakeRange(9,2)];
                [text addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(9,2)];
                [cell.comments_Label setAttributedText: text];
                
                [cell.rating_Bar_View initRateBar];
                [cell.rating_Bar_View  setUserInteractionEnabled:NO];
                
                NSString *rate_value=[NSString stringWithFormat:@"%@",[[_titles_Filter_Public_Routes objectAtIndex:indexPath.row] valueForKey:@"route_ratings_count"]];
                RBRatings rate;
                if ([rate_value isEqualToString:@"<null>"]) {
                    rate =0;
                }
                else{
                    rate =[[[_titles_Filter_Public_Routes objectAtIndex:indexPath.row] valueForKey:@"route_ratings_count"] floatValue]*2;
                }
                [cell.rating_Bar_View setRatings:rate];
                [cell.rating_Bar_View initRateBar];
                [cell.rating_Bar_View setUserInteractionEnabled:NO];
                
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
            else
            {
                if (indexPath.row%2==0) {
                    cell.back_Ground_view.backgroundColor=ROUTES_CELL_BG_COLOUR1;
                }
                else
                    cell.back_Ground_view.backgroundColor=ROUTES_CELL_BG_COLOUR2;
                
                cell.checkbox_btn_routes.hidden=YES;
                [cell.checkbox_btn_routes addTarget:self action:@selector(onClickCheckbox:) forControlEvents:UIControlEventTouchUpInside];
                cell.checkbox_btn_routes.tag=indexPath.row;
                cell.leadingConstraint_title_routes.constant=10;
                [cell.title_Label updateConstraints];
                
                cell.title_Label.text=[[titles_For_Public_Routes objectAtIndex:indexPath.row] valueForKey:@"route_title"];
                
                cell.description_label.text=[[titles_For_Public_Routes objectAtIndex:indexPath.row] valueForKey:@"route_description"];
                if ([cell.description_label.text isEqualToString:@""]) {
                    cell.des_icon.hidden=YES;
                }
                else{
                    cell.des_icon.hidden=NO;
                }
                cell.comments_Label.text=[NSString stringWithFormat:@"Comments( %@ )",[[titles_For_Public_Routes objectAtIndex:indexPath.row] valueForKey:@"route_comments_count"]];
                NSMutableAttributedString *text =
                [[NSMutableAttributedString alloc]
                 initWithAttributedString: cell.comments_Label.attributedText];
                
                [text addAttribute:NSForegroundColorAttributeName
                             value:APP_BLUE_COLOR
                             range:NSMakeRange(9,2)];
                [text addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(9,2)];
                [cell.comments_Label setAttributedText: text];
                
                [cell.rating_Bar_View initRateBar];
                [cell.rating_Bar_View  setUserInteractionEnabled:NO];
                
                NSString *rate_value=[NSString stringWithFormat:@"%@",[[titles_For_Public_Routes objectAtIndex:indexPath.row] valueForKey:@"route_ratings_count"]];
                RBRatings rate;
                if ([rate_value isEqualToString:@"<null>"]) {
                    rate =0;
                }
                else{
                    rate =[[[titles_For_Public_Routes objectAtIndex:indexPath.row] valueForKey:@"route_ratings_count"] floatValue]*2;
                }
                [cell.rating_Bar_View setRatings:rate];
                [cell.rating_Bar_View initRateBar];
                [cell.rating_Bar_View setUserInteractionEnabled:NO];
                
                cell.selectionStyle=UITableViewCellSelectionStyleNone;

            }
            
            return cell;
        }
        else
        {
            static NSString *simpleTableIdentifier = @"Public_Route_Cell";
            Public_Route_Cell *cell = (Public_Route_Cell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"Public_Route_Cell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            if (routesIsFilterd) {
                if (indexPath.row%2==0) {
                    cell.back_Ground_view.backgroundColor=ROUTES_CELL_BG_COLOUR1;
                }
                else
                    cell.back_Ground_view.backgroundColor=ROUTES_CELL_BG_COLOUR2;
                
                cell.title_Label.text=[[_titles_Filter_My_Routes objectAtIndex:indexPath.row] valueForKey:@"route_title"];
                
                
                cell.checkbox_btn_routes.hidden=YES;
                [cell.checkbox_btn_routes addTarget:self action:@selector(onClickCheckbox:) forControlEvents:UIControlEventTouchUpInside];
                cell.checkbox_btn_routes.tag=indexPath.row;
                cell.leadingConstraint_title_routes.constant=10;
                [cell.title_Label updateConstraints];
                
                cell.description_label.text=[[_titles_Filter_My_Routes objectAtIndex:indexPath.row] valueForKey:@"route_description"];
                if ([cell.description_label.text isEqualToString:@""]) {
                    cell.des_icon.hidden=YES;
                }
                else{
                    cell.des_icon.hidden=NO;
                }
                cell.comments_Label.text=[NSString stringWithFormat:@"Comments( %@ )",[[titles_For_My_Routes objectAtIndex:indexPath.row] valueForKey:@"route_comments_count"]];
                NSMutableAttributedString *text =
                [[NSMutableAttributedString alloc]
                 initWithAttributedString: cell.comments_Label.attributedText];
                
                [text addAttribute:NSForegroundColorAttributeName
                             value:APP_BLUE_COLOR
                             range:NSMakeRange(9,2)];
                [text addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(9,2)];
                [cell.comments_Label setAttributedText: text];
                
                
                [cell.rating_Bar_View initRateBar];
                [cell.rating_Bar_View  setUserInteractionEnabled:NO];
                
                
                NSString *rate_value=[NSString stringWithFormat:@"%@",[[_titles_Filter_My_Routes objectAtIndex:indexPath.row] valueForKey:@"route_ratings_count"]];
                RBRatings rate;
                if ([rate_value isEqualToString:@"<null>"]) {
                    rate =0;
                }
                else{
                    rate =[[[_titles_Filter_My_Routes objectAtIndex:indexPath.row] valueForKey:@"route_ratings_count"] floatValue]*2;
                }
                
                [cell.rating_Bar_View setRatings:rate];
                [cell.rating_Bar_View initRateBar];
                [cell.rating_Bar_View setUserInteractionEnabled:NO];
                
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
            else
            {
                if (indexPath.row%2==0) {
                    cell.back_Ground_view.backgroundColor=ROUTES_CELL_BG_COLOUR1;
                }
                else
                    cell.back_Ground_view.backgroundColor=ROUTES_CELL_BG_COLOUR2;
                
                cell.title_Label.text=[[titles_For_My_Routes objectAtIndex:indexPath.row] valueForKey:@"route_title"];
                
                
                cell.checkbox_btn_routes.hidden=YES;
                [cell.checkbox_btn_routes addTarget:self action:@selector(onClickCheckbox:) forControlEvents:UIControlEventTouchUpInside];
                cell.checkbox_btn_routes.tag=indexPath.row;
                cell.leadingConstraint_title_routes.constant=10;
                [cell.title_Label updateConstraints];
                
                cell.description_label.text=[[titles_For_My_Routes objectAtIndex:indexPath.row] valueForKey:@"route_description"];
                if ([cell.description_label.text isEqualToString:@""]) {
                    cell.des_icon.hidden=YES;
                }
                else{
                    cell.des_icon.hidden=NO;
                }
                cell.comments_Label.text=[NSString stringWithFormat:@"Comments( %@ )",[[titles_For_My_Routes objectAtIndex:indexPath.row] valueForKey:@"route_comments_count"]];
                NSMutableAttributedString *text =
                [[NSMutableAttributedString alloc]
                 initWithAttributedString: cell.comments_Label.attributedText];
                
                [text addAttribute:NSForegroundColorAttributeName
                             value:APP_BLUE_COLOR
                             range:NSMakeRange(9,2)];
                [text addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(9,2)];
                [cell.comments_Label setAttributedText: text];
                
                
                [cell.rating_Bar_View initRateBar];
                [cell.rating_Bar_View  setUserInteractionEnabled:NO];
                
                
                NSString *rate_value=[NSString stringWithFormat:@"%@",[[titles_For_My_Routes objectAtIndex:indexPath.row] valueForKey:@"route_ratings_count"]];
                RBRatings rate;
                if ([rate_value isEqualToString:@"<null>"]) {
                    rate =0;
                }
                else{
                    rate =[[[titles_For_My_Routes objectAtIndex:indexPath.row] valueForKey:@"route_ratings_count"] floatValue]*2;
                }
                
                [cell.rating_Bar_View setRatings:rate];
                [cell.rating_Bar_View initRateBar];
                [cell.rating_Bar_View setUserInteractionEnabled:NO];
                
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
            
            
            return cell;
        }

    }
    return nil;
    
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_side_Menu_Table) {
//        if (indexPath.row==2) {
            [cell setSelected:NO animated:NO];
//        }
//        else{
//            [cell setSelected:NO animated:NO];
//        }
    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_side_Menu_Table) {
        if (indexPath.row==0)
        {
            HomeDashboardView*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"HomeDashboardView"];
            [self.navigationController pushViewController:controller animated:YES];
        }
        
        if (indexPath.row == 1)
        {
            DashboardViewController*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"DashboardViewController"];
            [self.navigationController pushViewController:controller animated:YES];
        }
        else if (indexPath.row == 2)
        {
            PrivacyPolicy*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"PrivacyPolicy"];
            [self.navigationController pushViewController:controller animated:YES];
        }
        else if (indexPath.row == 3)
        {
            TermsAndConditions *cntrl = [self.storyboard instantiateViewControllerWithIdentifier:@"TermsAndConditions"];
            [self.navigationController pushViewController:cntrl animated:YES];
        }
        else  if (indexPath.row == 4) {
            
            if (appDelegate.ridedashboard_home) {
                [self logoutRideGoingAlert:@"You cannot logout while the ride is going on."];
            }
            
            else
            {
                [self showpopup:Terminate];
            }
            
            [self.side_Menu_Table reloadData];
        }
        [self menuleft];
    }
    else{
    
//    if ([self.is_FROM_VC_OR_Start_Ride isEqualToString:@"CREATE RIDE"])
//    {
//         if ([view_Status isEqualToString:@"PUBLIC"]) {
//              appDelegate.select_Route_Id_For_Create=[[titles_For_Public_Routes objectAtIndex:indexPath.row] valueForKey:@"route_id"];
//         }
//         else{
//             appDelegate.select_Route_Id_For_Create=[[titles_For_My_Routes objectAtIndex:indexPath.row] valueForKey:@"route_id"];
//         }
//        [self.navigationController popViewControllerAnimated:YES];
//    }
    if ([self.is_FROM_VC_OR_Start_Ride isEqualToString:@"YES"] || [self.is_FROM_VC_OR_Start_Ride isEqualToString:@"CREATE RIDE"]) {
        
//        SelectFriendsAndGroups*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"SelectFriendsAndGroups"];
//        appDelegate.isFrom_RidersVC_Or_FreeRideRoutes=@"FreerideRoutesToFrndsAndGroups";
//        [self.navigationController pushViewController:controller animated:YES];
    }
    
    
    if (tableView ==self.p_Routes_Table)
    {
        if (routesIsFilterd) {
            route_id_str = [NSString stringWithFormat:@"%@",[[_titles_Filter_Public_Routes objectAtIndex:indexPath.row] valueForKey:@"route_id"]];
        }
        else
        {
             route_id_str = [NSString stringWithFormat:@"%@",[[titles_For_Public_Routes objectAtIndex:indexPath.row] valueForKey:@"route_id"]];
        }
        
       
       
    }
    else{
        
        if (routesIsFilterd) {
            route_id_str = [NSString stringWithFormat:@"%@",[[_titles_Filter_My_Routes objectAtIndex:indexPath.row] valueForKey:@"route_id"]];
        }
        else
        {
        route_id_str = [NSString stringWithFormat:@"%@",[[titles_For_My_Routes objectAtIndex:indexPath.row] valueForKey:@"route_id"]];
        }
        
    }
    
    RouteDetailViewController *cntrlr = [self.storyboard instantiateViewControllerWithIdentifier:@"RouteDetailViewController"];
  
    cntrlr.route_id = route_id_str;
    [self.navigationController pushViewController:cntrlr animated:YES];
    }
}
-(void)onClickCheckbox:(UIButton *)sender
{
//    checkboxSelecteString=[NSString stringWithFormat:@"%ld",(long)
//                           sender.tag];
     changed_route_status=YES;
      if ([view_Status isEqualToString:@"PUBLIC"] || [view_Status isEqualToString:@""]) {
          NSDictionary *dict;
          if (routesIsFilterd) {
              dict=[_titles_Filter_Public_Routes objectAtIndex:sender.tag];
          }
          else
          {
               dict=[titles_For_Public_Routes objectAtIndex:sender.tag];
              
          }
          
          NSString *route_str=[NSString stringWithFormat:@"%@",
                               [dict objectForKey:@"route_id"]];
          NSString*route_start_address=[NSString stringWithFormat:@"%@",
                                        [dict objectForKey:@"route_start_address"]];
          NSString*route_start_address_latitude=[NSString stringWithFormat:@"%@",
                                                 [dict objectForKey:@"route_start_latitude"]];
          NSString*route_start_address_longtitude=[NSString stringWithFormat:@"%@",
                                                   [dict objectForKey:@"route_start_longitude"]];
          
          NSString*route_title=[NSString stringWithFormat:@"%@",
                                                   [dict objectForKey:@"route_title"]];
          
          if ([_checkboxSelecteString isEqualToString:route_str]) {
              _checkboxSelecteString=@"";
              selectedRouteIDString_checkbox=@"";
              selectedRouteStartAddressString=@"";
              selectedRouteStartAddressString_checkbox=@"";
              selectedRouteAddressLatitude=@"";
              selectedRouteAddressLongtitude=@"";
              _checkboxSelectedAddressString=@"";
              
          }
          else{
              _checkboxSelecteString=route_str;
              selectedRouteIDString_checkbox=route_str;
              selectedRouteStartAddressString=route_start_address;
              selectedRouteStartAddressString_checkbox=route_start_address;
              selectedRouteAddressLatitude=route_start_address_latitude;
              selectedRouteAddressLongtitude=route_start_address_longtitude;
              NSLog(@"Selected route Id From My Route checkbox %@",_checkboxSelecteString);
              _checkboxSelectedAddressString=route_title;
              
          }
          
          
          
          
          [self.p_Routes_Table reloadData];
          [self.my_Routes_Table reloadData];
    }
    else
    {
         NSDictionary *dict;
        if (routesIsFilterd) {
            dict=[_titles_Filter_My_Routes objectAtIndex:sender.tag];
        }
        else
        {
            dict=[titles_For_My_Routes objectAtIndex:sender.tag];
        }
        NSString *route_str=[NSString stringWithFormat:@"%@",
                             [dict objectForKey:@"route_id"]];
        NSString *route_Addrss_str=[NSString stringWithFormat:@"%@",
                                    [dict objectForKey:@"route_start_address"]];
        NSString*route_start_address_latitude=[NSString stringWithFormat:@"%@",
                                               [dict objectForKey:@"route_start_latitude"]];
        NSString*route_start_address_longtitude=[NSString stringWithFormat:@"%@",
                                                 [dict objectForKey:@"route_start_longitude"]];
        NSString*route_title=[NSString stringWithFormat:@"%@",
                              [dict objectForKey:@"route_title"]];
        if ([_checkboxSelecteString isEqualToString:route_str]) {
            _checkboxSelecteString=@"";
            selectedRouteIDString_checkbox=@"";
            selectedRouteStartAddressString=@"";
            selectedRouteStartAddressString_checkbox=@"";
            selectedRouteAddressLatitude=@"";
            selectedRouteAddressLongtitude=@"";
            _checkboxSelectedAddressString=@"";
        }
        else{
            _checkboxSelecteString=route_str;
            selectedRouteIDString_checkbox=route_str;
            selectedRouteStartAddressString=route_Addrss_str;
            selectedRouteStartAddressString_checkbox=route_Addrss_str;
            NSLog(@"Selected route Id From My Route checkbox %@",_checkboxSelecteString);
            selectedRouteAddressLatitude=route_start_address_latitude;
            selectedRouteAddressLongtitude=route_start_address_longtitude;
            _checkboxSelectedAddressString=route_title;
        }
        [self.my_Routes_Table reloadData];
        [self.p_Routes_Table reloadData];
        
    }
    
    if ([self.is_FROM_VC_OR_Start_Ride isEqualToString:@"YES"])
    {
        
        
    }
    else
    {
        if ([selectedRouteIDString_checkbox isEqualToString:@""]) {
            [skip_btn_is setTitle:@"Skip"];
        }
        else
            [skip_btn_is setTitle:@"Next"];
    }
// appDelegate.select_Route_Id_For_CreateAndFreeRide=selectedRouteIDString_checkbox;
// appDelegate.select_Route_Address_For_CreateAndFreeRide=selectedRouteStartAddressString_checkbox;
    
}
-(void)service_fail_OR_EmptyData{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
//        [self.navigationController popViewControllerAnimated:YES];
    });
}

-(void)menuleft
{
   
    menu_Status=@"LEFT";
    [UIView animateWithDuration:0.5 delay:0.3 options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.menu_Leading_Constraint.constant=-300;
                         [self.view layoutIfNeeded];
                     } completion:^(BOOL finished) {
                         self.routes_Bg_View.alpha=1;
                         
                         self.routes_Bg_View.userInteractionEnabled=YES;
                     }];
}

-(void)menuright
{
   
    menu_Status=@"RIGHT";
    _side_Menu_view.hidden=NO;
    [UIView animateWithDuration:0.5 delay:0.3 options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.menu_Leading_Constraint.constant=0;
                         [self.view layoutIfNeeded];
                     } completion:^(BOOL finished)
     {
         
         self.routes_Bg_View.alpha=0.8;
         self.routes_Bg_View.userInteractionEnabled=NO;
     }];
}


- (IBAction)Menu_Action:(id)sender {
    _side_Menu_view.hidden=NO;
    if ([menu_Status isEqualToString:@"LEFT"]) {
        [self menuright];
        
    }
    else{
        [self menuleft];
    }
}
- (IBAction)Profile_Action:(id)sender {
    
    ProfileView*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileView"];
    [self.navigationController pushViewController:controller animated:YES];
    [self menuleft];
}
-(void)logoutRideGoingAlert:(NSString *)alert_msg
{
    UIAlertController *alertview = [UIAlertController alertControllerWithTitle:@"Can't Logout" message:alert_msg preferredStyle:UIAlertControllerStyleAlert];
    
    [alertview addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        
    }]];
    [self presentViewController:alertview animated:YES completion:nil];
}
-(void)showpopup:(NSString *)message
{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"LOGOUT"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                             NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                             NSData *uData = [NSKeyedArchiver archivedDataWithRootObject:@""];
                             [defaults setObject:uData forKey:User_ID];
                             NSString *user_data;
                             user_data = [defaults objectForKey:User_ID];
                             [defaults removeObjectForKey:User_ID];
                             [defaults removeObjectForKey:@"UserName"];
                             [[FBSDKLoginManager new] logOut];
                             [[GIDSignIn sharedInstance] signOut];
                             [defaults synchronize];
                             
                             LoginViewController *login = [self.storyboard  instantiateViewControllerWithIdentifier:@"LoginViewController"];
                             [self.navigationController pushViewController:login animated:YES];
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"CANCEL"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
   
    if (scrollView==_my_Routes_Table||scrollView==_p_Routes_Table) {
        self.p_Routes_Btn_View.userInteractionEnabled=NO;
        self.my_Routes_btn_View.userInteractionEnabled=NO;
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    self.p_Routes_Btn_View.userInteractionEnabled=YES;
    self.my_Routes_btn_View.userInteractionEnabled=YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Search methods
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)aSearchBar {
    self.routes_searchBar.placeholder = @"search";
    self.routes_searchBar.showsCancelButton = YES;
    
    return YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)SearchBar
{
    if([view_Status isEqualToString:@""]||[view_Status isEqualToString:@"PUBLIC"])
    {
        _routes_searchBar.text = nil;
        self.routes_searchBar.placeholder = @"search";
        self.routes_searchBar.showsCancelButton = NO;
        [_routes_searchBar resignFirstResponder];
        routesIsFilterd = NO;
        [_p_Routes_Table setContentOffset:CGPointZero animated:NO];
        [_p_Routes_Table reloadData];
    }
    else{
        _routes_searchBar.text = nil;
        self.routes_searchBar.placeholder = @"search";
        self.routes_searchBar.showsCancelButton = NO;
        [_routes_searchBar resignFirstResponder];
        routesIsFilterd = NO;
        [_my_Routes_Table setContentOffset:CGPointZero animated:NO];
        [_my_Routes_Table reloadData];
    }
    
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.routes_searchBar resignFirstResponder];
    for (UIView *view in searchBar.subviews)
    {
        for (id subview in view.subviews)
        {
            if ( [subview isKindOfClass:[UIButton class]] )
            {
                [subview setEnabled:YES];
                
                NSLog(@"enableCancelButton");
                return;
            }
        }
    }
    
    [searchBar setShowsCancelButton:YES animated:YES];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
    if(searchText.length == 0)
    {
        routesIsFilterd = NO;
    }
    else
    {
        if([view_Status isEqualToString:@""]||[view_Status isEqualToString:@"PUBLIC"])
        {
            routesIsFilterd = YES;
           // _titles_Filter_Public_Routes = [[NSMutableArray alloc]init];
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(route_title contains[c] %@)||(route_start_address contains[c] %@)",searchText,searchText];
            _titles_Filter_Public_Routes = [NSMutableArray arrayWithArray:[titles_For_Public_Routes filteredArrayUsingPredicate:predicate]];
               [_p_Routes_Table reloadData];
        }
        else
        {
            routesIsFilterd = YES;
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(route_title contains[c] %@)||(route_start_address contains[c] %@)",searchText,searchText];
            _titles_Filter_My_Routes = [NSMutableArray arrayWithArray:[titles_For_My_Routes filteredArrayUsingPredicate:predicate]];            
            [_my_Routes_Table reloadData];
        }
    }
}

- (IBAction)onClick_routes_rideGoingBtn:(id)sender {
    
    self.routes_rideGoingBtn.hidden=YES;
    self.routes_scrollLabel.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }

}
@end
