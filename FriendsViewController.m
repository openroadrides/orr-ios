//
//  FriendsViewController.m
//  openroadrides
//
//  Created by apple on 26/06/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "FriendsViewController.h"

@interface FriendsViewController ()<FBSDKAppInviteDialogDelegate>
{
    UIBarButtonItem *item0;

}

@end

@implementation FriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 120, 30)];
//    label.textAlignment = NSTextAlignmentCenter;
//    [label setFont:[UIFont fontWithName:@"Antonio-Bold" size:20]];
//    [label setBackgroundColor:[UIColor clearColor]];
//    [label setTextColor:[UIColor blackColor]];
//    [label setText:@"FRIENDS"];
//    [self.navigationController.navigationBar.topItem setTitleView:label];
    self.title = @"FRIENDS";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    
    self.frnds_scrollLabel.hidden=YES;
    self.frnds_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.frnds_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.frnds_scrollLabel.textColor = [UIColor blackColor];
    self.frnds_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.frnds_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.frnds_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.frnds_scrollLabel.scrollSpeed = 60; // pixels per second
    self.frnds_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.frnds_scrollLabel.fadeLength = 0.f;
    self.frnds_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.frnds_scrollLabel observeApplicationNotifications];
    
    self.frnds_rideGoingBtn.hidden=YES;
    self.bottomConstraint_inviteFrndsBtn.constant=-32;
    if (appDelegate.ridedashboard_home) {
         self.frnds_scrollLabel.hidden=NO;
        self.frnds_rideGoingBtn.hidden=NO;
        self.bottomConstraint_inviteFrndsBtn.constant=2;
    }

    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [leftBtn addTarget:self action:@selector(leftBtn:) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.frame = CGRectMake(0, 0, 25, 25);
    //    [leftBtn setBackgroundImage:[UIImage imageNamed:@"Finalback-arrow.png"] forState:UIControlStateNormal];
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"back_Image"] forState:UIControlStateNormal];
    UIBarButtonItem *leftBackButton=[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    item0=leftBackButton;
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:item0, nil];
    
    CGFloat content_View_Width=SCREEN_WIDTH-20;
    self.widthConstraint_friendsview.constant=content_View_Width*2;
    [self.view layoutIfNeeded];
    
    
    [self.public_friends_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.my_friends_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    indicaterview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    activeIndicatore.backgroundColor=[UIColor clearColor];
    activeIndicatore = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.frame = CGRectMake(0.0, 0.0, 80, 80);
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    [indicaterview addSubview:activeIndicatore];
    [self.view addSubview:indicaterview];
    [indicaterview bringSubviewToFront:self.view];
    [activeIndicatore startAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    indicaterview.hidden=YES;
    
    view_Status=@"";
    
    
    self.public_friends_tableView.dataSource=self;
    self.public_friends_tableView.delegate=self;
    self.my_friends_tableView.dataSource=self;
    self.my_friends_tableView.delegate=self;
    
    
    self.refreshControl = [[UIRefreshControl alloc]init];
    self.refreshControl.tintColor=APP_YELLOW_COLOR;
   
    self.refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
    [self.refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
     [self.public_friends_tableView addSubview:self.refreshControl];
    
    self.refreshControl2 = [[UIRefreshControl alloc]init];
    self.refreshControl2.tintColor=APP_YELLOW_COLOR;
    
    self.refreshControl2.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
    [self.refreshControl2 addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
   
    [self.my_friends_tableView addSubview:self.refreshControl2];
    [self get_Friends_Data];
    
    _searchBar.hidden=NO;
    _searchBar.delegate=self;
    self.searchBar.tintColor = [UIColor whiteColor];
//    self.searchBar.barTintColor=[UIColor blackColor];
    self.searchBar.barTintColor= ROUTES_CELL_BG_COLOUR1;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)leftBtn:(UIButton *)sender
{
    
   [self.navigationController popViewControllerAnimated:YES];
    
}
-(void)viewWillAppear:(BOOL)animated{
    if ([appDelegate.un_frind_in_Profile isEqualToString:@"YES"]) {
        appDelegate.un_frind_in_Profile=@"";
         if ([view_Status isEqualToString:@""]||[view_Status isEqualToString:@"PUBLIC"]) {
             [arrayOfPublicFriends removeAllObjects];
             [_public_friends_tableView reloadData];
             my_Friends_Called=@"";
             
             [self get_Friends_Data];
         }
         else{
             [arrayOfMyFriends removeAllObjects];
             [_my_friends_tableView reloadData];
             Public_Friends_Called=@"";
              [self get_Friends_Data];
         }
    }
}
- (void)refreshTable {
     [self.refreshControl endRefreshing];
    //TODO: refresh your data
    if([view_Status isEqualToString:@""]||[view_Status isEqualToString:@"PUBLIC"])
    {
        Public_Friends_Called=@"YES";
        [self.refreshControl endRefreshing];
//        [self.public_friends_tableView reloadData];
        
        arrayOfPublicFriends=nil;
        arrayOfPublicFriends=[[NSMutableArray alloc]init];
        filteredArray=nil;
        filteredArray=[[NSMutableArray alloc]init];
        [self.public_friends_tableView reloadData];
        [self get_Friends_Data];
        [self.searchBar setText:@""];
    }
    else{
        my_Friends_Called=@"YES";
        
         arrayOfMyFriends=nil;
        arrayOfMyFriends=[[NSMutableArray alloc]init];
        filteredArray2=nil;
        filteredArray2=[[NSMutableArray alloc]init];
        [self.my_friends_tableView reloadData];
        [self.refreshControl2 endRefreshing];
        [self get_Friends_Data];
        [self.searchBar setText:@""];
    }
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)onClick_publicfnds_btn:(id)sender {
    if (arrayOfPublicFriends.count>0) {
        
    }
    view_Status=@"PUBLIC";
    self.public_segement_view.backgroundColor=[UIColor whiteColor];
    self.myfriends_segment_view.backgroundColor=[UIColor blackColor];
    [self.mainScrollView setContentOffset:CGPointMake((0), _mainScrollView.contentOffset.y)];
    self.public_title_label.textColor=[UIColor whiteColor];
    self.myfriends_title_label.textColor=APP_YELLOW_COLOR;
    
   
    
    if ([Public_Friends_Called isEqualToString:@"YES"]) {
        isFiltered=NO;
        [_searchBar resignFirstResponder];
        _searchBar.text=@"";
        _searchBar.showsCancelButton=NO;
        [_public_friends_tableView reloadData];
    }
    else{
        isFiltered=NO;
        [_searchBar resignFirstResponder];
        _searchBar.text=@"";
        _searchBar.showsCancelButton=NO;
        
        [self get_Friends_Data];
    }

}
- (IBAction)onClick_myFrnds_btn:(id)sender {
    if (arrayOfMyFriends.count>0) {
         self.no_Data_Label_MY.hidden=YES;
    }
   
    
    view_Status=@"MY";
    self.public_segement_view.backgroundColor=[UIColor blackColor];
    self.myfriends_segment_view.backgroundColor=[UIColor whiteColor];
    [self.mainScrollView setContentOffset:CGPointMake((SCREEN_WIDTH-20), self.mainScrollView.contentOffset.y)];
    self.myfriends_title_label.textColor=[UIColor whiteColor];
    self.public_title_label.textColor=APP_YELLOW_COLOR;
    
   
   
    if ([my_Friends_Called isEqualToString:@"YES"]) {
        isFiltered=NO;
        [_searchBar resignFirstResponder];
        _searchBar.text=@"";
        _searchBar.showsCancelButton=NO;
        [_my_friends_tableView reloadData];

    }
    else{
        isFiltered=NO;
        [_searchBar resignFirstResponder];
        _searchBar.text=@"";
        _searchBar.showsCancelButton=NO;

        [self get_Friends_Data];
    }

}
#pragma mark - ScrollView
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    self.public_segement_view.userInteractionEnabled=YES;
    self.myfriends_segment_view.userInteractionEnabled=YES;
    
    if (scrollView.tag==5) {
        if (scrollView.contentOffset.x==0) {
            [self onClick_publicfnds_btn:0];
        }
        if (scrollView.contentOffset.x==SCREEN_WIDTH-20) {
            [self onClick_myFrnds_btn:0];
        }
    }
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    if (scrollView==_public_friends_tableView||scrollView==_my_friends_tableView) {
        self.public_segement_view.userInteractionEnabled=NO;
        self.public_segement_view.userInteractionEnabled=NO;
    }
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    self.public_segement_view.userInteractionEnabled=YES;
    self.public_segement_view.userInteractionEnabled=YES;
}
-(void)get_Friends_Data
{
    [_searchBar resignFirstResponder];
    [_searchBar setText:@""];
    self.searchBar.showsCancelButton = NO;
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [SHARED_HELPER showAlert:Nonetwork];
       indicaterview.hidden=YES;
        return;
    }
    indicaterview.hidden=NO;
    _mainScrollView.userInteractionEnabled=NO;
    
    NSString *user_id=[Defaults valueForKey:User_ID];
//    NSString *user_id=@"1";
    NSString *service_is;
    if ([view_Status isEqualToString:@""]||[view_Status isEqualToString:@"PUBLIC"]) {
        service_is=WS_PUBLICFRIENDS_REQUEST;
        arrayOfPublicFriends=[[NSMutableArray alloc]init];
        filteredArray=[[NSMutableArray alloc]init];
      
    }
    else{
        service_is=WS_MYFRIENDS_REQUEST;
        arrayOfMyFriends=[[NSMutableArray alloc]init];
        filteredArray2=[[NSMutableArray alloc]init];
        
    }
    [SHARED_API publicFriendsRequest:user_id public_myfriends:service_is withSuccess:^(NSDictionary *response) {
        NSLog(@"%@",response);
        [self Friends_Service_Sucsess:response];
        indicaterview.hidden=YES;
    } onfailure:^(NSError *theError) {
        [SHARED_HELPER showAlert:ServiceFail];
        indicaterview.hidden=YES;
         _mainScrollView.userInteractionEnabled=YES;
    }];
    
}
-(void)Friends_Service_Sucsess:(NSDictionary *)respnse{
    
     _mainScrollView.userInteractionEnabled=YES;
    if ([[respnse valueForKey:STATUS] isEqualToString:SUCCESS]) {
        NSArray *friends_data=[respnse valueForKey:@"friends"];
        if (friends_data.count>0) {
//            for (int i=0; i<friends_data.count; i++) {
//                NSDictionary *data_Dict_is=[friends_data objectAtIndex:i];
//                NSString *route_Privacy=[data_Dict_is valueForKey:@"friends"];
             self.mainScrollView.hidden=NO;
                if ([view_Status isEqualToString:@""]||[view_Status isEqualToString:@"PUBLIC"]) {
                     arrayOfPublicFriends=[[NSMutableArray alloc]init];
                    [arrayOfPublicFriends addObjectsFromArray:friends_data];
                    [filteredArray addObjectsFromArray:friends_data];
                  
                    
                    view_Status=@"PUBLIC";
                    Public_Friends_Called=@"YES";
                    [self.public_friends_tableView reloadData];
                }
                else{
                    arrayOfMyFriends=[[NSMutableArray alloc]init];
                    [arrayOfMyFriends addObjectsFromArray:friends_data];
                    [filteredArray2 addObjectsFromArray:friends_data];
                 
                    
                    [self.my_friends_tableView reloadData];
                    self.no_Data_Label_MY.hidden=YES;
                    my_Friends_Called=@"YES";
                }
//            }
            
        }
        else{
            self.mainScrollView.hidden=NO;
            self.no_Data_Label_MY.hidden=NO;
            [SHARED_HELPER showAlert:NO_Friends];
        }
    }
    else{
        [SHARED_HELPER showAlert:ServiceFail];
        [self service_fail_OR_EmptyData];
    }
}
-(void)service_fail_OR_EmptyData{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{

    });
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([view_Status isEqualToString:@"PUBLIC"]) {
        
//        if (arrayOfPublicFriends.count>0)
//        {
//
//        }
//        else{
////            NO_DATA      = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
//            NO_DATA.numberOfLines=2;
//            NO_DATA.textColor        = APP_YELLOW_COLOR;
//            NO_DATA.textAlignment    = NSTextAlignmentCenter;
//            NO_DATA.text  = @"No Friends found";
//            tableView.backgroundView = NO_DATA;
//            tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//            return 0;
//        }
        
        if ([arrayOfPublicFriends count]>0)
            {
                if(isFiltered)
                {
                    return [filteredArray count];
                }
                
                return arrayOfPublicFriends.count;
        }
        
    }
    else if ([view_Status isEqualToString:@"MY"])
    {
//        if (arrayOfMyFriends.count>0)
//        {
//
//        }
//        else{
//            NO_DATA      = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
//            NO_DATA.numberOfLines=2;
//            NO_DATA.textColor        = APP_YELLOW_COLOR;
//            NO_DATA.textAlignment    = NSTextAlignmentCenter;
//            NO_DATA.text  = @"No Friends found";
//            tableView.backgroundView = NO_DATA;
//            tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
////            return 0;
//        }
        
        
        
        if ([arrayOfMyFriends count]>0)
        {
            if(isFiltered)
            {
                return [filteredArray2 count];
            }
            
            return arrayOfMyFriends.count;
        }
        

    }
    else{
        return 0;
    }
    return 0;
}

#pragma mark - TableView
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([view_Status isEqualToString:@"PUBLIC"]) {
        
      
         PublicFriendsCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
        
        if (cell == nil)
        {
            cell = [[PublicFriendsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        }
        if (isFiltered) {
        
        if (indexPath.row%2==0) {
            cell.contentView_publicfriends.backgroundColor=ROUTES_CELL_BG_COLOUR1;
        }
        else
            cell.contentView_publicfriends.backgroundColor=ROUTES_CELL_BG_COLOUR2;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.username_publicFriend.text=[[filteredArray objectAtIndex:indexPath.row] objectForKey:@"name"];
        
            NSString * areaName=[NSString stringWithFormat:@"%@",[[filteredArray objectAtIndex:indexPath.row] objectForKey:@"address"]];
            NSString * cityName=[NSString stringWithFormat:@"%@",[[filteredArray objectAtIndex:indexPath.row] objectForKey:@"city"]];
            NSString * stateName=[NSString stringWithFormat:@"%@",[[filteredArray objectAtIndex:indexPath.row] objectForKey:@"state"]];
            NSString *zipCode=[NSString stringWithFormat:@"%@",[[filteredArray objectAtIndex:indexPath.row] objectForKey:@"zipcode"]];
            
            NSMutableArray *empaty_array_search_public=[[NSMutableArray alloc]init];
            [empaty_array_search_public addObject:@"<null>"];
            [empaty_array_search_public addObject:@"null"];
            [empaty_array_search_public addObject:@""];
            [empaty_array_search_public addObject:@"(null)"];
            [empaty_array_search_public addObject:@"0"];
            
            NSMutableArray *Data_array_search_public=[[NSMutableArray alloc]init];
            
            
            if ([empaty_array_search_public containsObject:areaName]) {
                
            }
            else{
                
                [Data_array_search_public addObject:areaName];
            }
            if ([empaty_array_search_public containsObject:cityName]) {
                
            }
            else{
                [Data_array_search_public addObject:cityName];
            }
            
            if ([empaty_array_search_public containsObject:stateName]) {
                
            }
            else{
                [Data_array_search_public addObject:stateName];
            }
            
            if ([empaty_array_search_public containsObject:zipCode]) {
                
            }
            else{
                [Data_array_search_public addObject:zipCode];
            }
            
            NSMutableString *add_data_search_public=[[NSMutableString alloc]init];
            if (Data_array_search_public.count>0) {
                cell.location_imageview.image=[UIImage imageNamed:@"check_in_placeholder"];
                
                for (int i=0; i<Data_array_search_public.count; i++) {
                    
                    NSString *adress=[Data_array_search_public objectAtIndex:i];
                    if (i==0) {
                        [add_data_search_public appendFormat:@"%@", [NSString stringWithFormat:@"%@",adress]];
                    }
                    else
                    {
                    int zip=[[Data_array_search_public objectAtIndex:i] intValue];
                        if (zip>0) {
                        [add_data_search_public appendFormat:@"%@", [NSString stringWithFormat:@" %@",adress]];
                        }
                        else
                    [add_data_search_public appendFormat:@"%@", [NSString stringWithFormat:@", %@",adress]];
                    }
                  
                }
                cell.address_publicfriend.text=add_data_search_public;
                cell.topConstraint_view_addafriend.priority=750;
                cell.View_To_Username_Constraint.priority=250;
            }
            else{
                cell.location_imageview.image=[UIImage imageNamed:@""];
                cell.address_publicfriend.text=@"";
                
                cell.topConstraint_view_addafriend.priority=250;
                cell.View_To_Username_Constraint.priority=750;
            }
        
        NSString *profile_image_string1 = [NSString stringWithFormat:@"%@", [[filteredArray objectAtIndex:indexPath.row] objectForKey:@"profile_image"]];
         NSURL*url=[NSURL URLWithString:profile_image_string1];
        [cell.profile_imageView sd_setImageWithURL:url
                     placeholderImage:[UIImage imageNamed:@"user_Placeholder"]
                              options:SDWebImageRefreshCached];

        
        
        cell.statusbtn_publicFriend.layer.borderWidth=1.0f;
        cell.statusbtn_publicFriend.layer.borderColor=[[UIColor whiteColor] CGColor];
        cell.username_publicFriend.textColor=APP_YELLOW_COLOR;
        cell.statusbtn_publicFriend.tag=indexPath.row;

        NSString*statusString=[NSString stringWithFormat:@"%@",[[filteredArray objectAtIndex:indexPath.row] objectForKey:@"status"]];
        if ([statusString isEqualToString:@"<null>"] || [statusString isEqualToString:@""] || [statusString isEqualToString:@"null"] || statusString == nil) {
            
            
        }
        
        else{
            
            if ([statusString isEqualToString:@"notfriend"]) {
                
                cell.statusLabel.text=@"Add friend";
                cell.imageView_statusBtn.image=[UIImage imageNamed:@"Addafriend"];
                cell.statusbtn_publicFriend.userInteractionEnabled=YES;
                cell.leadingConstraintForstatusImageView.constant=12;
                [cell.imageView_statusBtn updateConstraints];
                
            }
            else if ([statusString isEqualToString:@"pending"])
            {
                cell.statusLabel.text=@"pending";
                cell.imageView_statusBtn.image=[UIImage imageNamed:@"unfriend"];
                cell.statusbtn_publicFriend.userInteractionEnabled=NO;
                cell.leadingConstraintForstatusImageView.constant=30;
                [cell.imageView_statusBtn updateConstraints];
            }
            else
            {
                
                cell.statusLabel.text=@"friends";
                cell.imageView_statusBtn.image=[UIImage imageNamed:@"unfriend"];
                cell.statusbtn_publicFriend.userInteractionEnabled=NO;
                cell.leadingConstraintForstatusImageView.constant=30;
                [cell.imageView_statusBtn updateConstraints];
                
            }
            
        }
        }
        else{
//            without Search
            if (indexPath.row%2==0) {
                cell.contentView_publicfriends.backgroundColor=ROUTES_CELL_BG_COLOUR1;
            }
            else
                cell.contentView_publicfriends.backgroundColor=ROUTES_CELL_BG_COLOUR2;
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.username_publicFriend.text=[[arrayOfPublicFriends objectAtIndex:indexPath.row] objectForKey:@"name"];
            
            NSString * areaName=[NSString stringWithFormat:@"%@",[[arrayOfPublicFriends objectAtIndex:indexPath.row] objectForKey:@"address"]];
            NSString * cityName=[NSString stringWithFormat:@"%@",[[arrayOfPublicFriends objectAtIndex:indexPath.row] objectForKey:@"city"]];
            NSString * stateName=[NSString stringWithFormat:@"%@",[[arrayOfPublicFriends objectAtIndex:indexPath.row] objectForKey:@"state"]];
            NSString *zipCode=[NSString stringWithFormat:@"%@",[[arrayOfPublicFriends objectAtIndex:indexPath.row] objectForKey:@"zipcode"]];
           
            NSMutableArray *empaty_array=[[NSMutableArray alloc]init];
            [empaty_array addObject:@"<null>"];
            [empaty_array addObject:@"null"];
            [empaty_array addObject:@""];
            [empaty_array addObject:@"(null)"];
            [empaty_array addObject:@"0"];
            
            NSMutableArray *Data_array=[[NSMutableArray alloc]init];
           
            
            if ([empaty_array containsObject:areaName]) {
                
            }
            else{
                
                [Data_array addObject:areaName];
            }
            if ([empaty_array containsObject:cityName]) {
                
            }
            else{
                 [Data_array addObject:cityName];
            }
            
            if ([empaty_array containsObject:stateName]) {
               
            }
            else{
                [Data_array addObject:stateName];
            }
            
            if ([empaty_array containsObject:zipCode]) {
               
            }
            else{
                [Data_array addObject:zipCode];
            }
            
            NSMutableString *add_data=[[NSMutableString alloc]init];
            if (Data_array.count>0) {
                cell.location_imageview.image=[UIImage imageNamed:@"check_in_placeholder"];
                
                for (int i=0; i<Data_array.count; i++) {
                    
                    NSString *adress=[Data_array objectAtIndex:i];
                    if (i==0) {
                       [add_data appendFormat:@"%@", [NSString stringWithFormat:@"%@",adress]];
                    }
                    else
                    {
                        int zip=[[Data_array objectAtIndex:i] intValue];
                        if (zip>0) {
                            [add_data appendFormat:@"%@", [NSString stringWithFormat:@" %@",adress]];
                        }
                        else
                            [add_data appendFormat:@"%@", [NSString stringWithFormat:@", %@",adress]];
                    }
                    
                }
                cell.address_publicfriend.text=add_data;
                cell.topConstraint_view_addafriend.priority=750;
                cell.View_To_Username_Constraint.priority=250;
            }
            else{
                cell.location_imageview.image=[UIImage imageNamed:@""];
                cell.address_publicfriend.text=@"";
                
                cell.topConstraint_view_addafriend.priority=250;
                cell.View_To_Username_Constraint.priority=750;
            }
            
            
            NSString *profile_image_string1 = [NSString stringWithFormat:@"%@", [[arrayOfPublicFriends objectAtIndex:indexPath.row] objectForKey:@"profile_image"]];
            NSURL*url=[NSURL URLWithString:profile_image_string1];
            [cell.profile_imageView sd_setImageWithURL:url
                                      placeholderImage:[UIImage imageNamed:@"user_Placeholder"]
                                               options:SDWebImageRefreshCached];
            
            
            
            cell.statusbtn_publicFriend.layer.borderWidth=1.0f;
            cell.statusbtn_publicFriend.layer.borderColor=[[UIColor whiteColor] CGColor];
            cell.username_publicFriend.textColor=APP_YELLOW_COLOR;
            cell.statusbtn_publicFriend.tag=indexPath.row;
            
            
            NSString*statusString=[NSString stringWithFormat:@"%@",[[arrayOfPublicFriends objectAtIndex:indexPath.row] objectForKey:@"status"]];
            if ([statusString isEqualToString:@"<null>"] || [statusString isEqualToString:@""] || [statusString isEqualToString:@"null"] || statusString == nil) {
                
                cell.statusbtn_publicFriend.hidden=YES;
            }
            
            else{
                cell.statusbtn_publicFriend.hidden=NO;
                if ([statusString isEqualToString:@"notfriend"]) {
                    
                    cell.statusLabel.text=@"Add friend";
                   cell.imageView_statusBtn.image=[UIImage imageNamed:@"Addafriend"];
                    cell.statusbtn_publicFriend.userInteractionEnabled=YES;
                    cell.leadingConstraintForstatusImageView.constant=12;
                    [cell.imageView_statusBtn updateConstraints];
                    
                }
                else if ([statusString isEqualToString:@"pending"])
                {
                    cell.statusLabel.text=@"pending";
                    cell.imageView_statusBtn.image=[UIImage imageNamed:@"unfriend"];
                    cell.statusbtn_publicFriend.userInteractionEnabled=NO;
                    cell.leadingConstraintForstatusImageView.constant=30;
                    [cell.imageView_statusBtn updateConstraints];
                }
                else if ([statusString isEqualToString:@"invitation"])
                {
                    cell.statusbtn_publicFriend.layer.borderWidth=0;
                    cell.statusLabel.text=@"";
                    cell.imageView_statusBtn.image=[UIImage imageNamed:@""];
                    cell.leadingConstraintForstatusImageView.constant=30;
                    cell.statusbtn_publicFriend.userInteractionEnabled=NO;
                    [cell.imageView_statusBtn updateConstraints];
                }
                else
                {
                    cell.statusbtn_publicFriend.layer.borderWidth=0;
                    cell.statusLabel.text=@"friends";
                    cell.imageView_statusBtn.image=[UIImage imageNamed:@"unfriend"];
                    cell.leadingConstraintForstatusImageView.constant=0;
                    cell.statusbtn_publicFriend.userInteractionEnabled=NO;
                    [cell.imageView_statusBtn updateConstraints];
                }
                
            }

            
        }
              return cell;
    }
    
    else
    {
        
        MyFriendsCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell1"];
        
        if (cell == nil)
        {
            cell = [[MyFriendsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        }
        if (isFiltered) {
        
        if (indexPath.row%2==0) {
            cell.contentview_myFriends.backgroundColor=ROUTES_CELL_BG_COLOUR1;
        }
        else
            cell.contentview_myFriends.backgroundColor=ROUTES_CELL_BG_COLOUR2;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.userName_myFriends.text=[[filteredArray2 objectAtIndex:indexPath.row] objectForKey:@"name"];
        cell.userName_myFriends.textColor=APP_YELLOW_COLOR;
            
            NSString * areaName=[NSString stringWithFormat:@"%@",[[filteredArray2 objectAtIndex:indexPath.row] objectForKey:@"address"]];
            NSString * cityName=[NSString stringWithFormat:@"%@",[[filteredArray2 objectAtIndex:indexPath.row] objectForKey:@"city"]];
            NSString * stateName=[NSString stringWithFormat:@"%@",[[filteredArray2 objectAtIndex:indexPath.row] objectForKey:@"state"]];
            NSString *zipCode=[NSString stringWithFormat:@"%@",[[filteredArray2 objectAtIndex:indexPath.row] objectForKey:@"zipcode"]];
            NSMutableArray *empaty_array2=[[NSMutableArray alloc]init];
            [empaty_array2 addObject:@"<null>"];
            [empaty_array2 addObject:@"null"];
            [empaty_array2 addObject:@""];
            [empaty_array2 addObject:@"(null)"];
            [empaty_array2 addObject:@"0"];
            
            NSMutableArray *Data_array_location=[[NSMutableArray alloc]init];
            
            
            if ([empaty_array2 containsObject:areaName]) {
                
            }
            else{
                
                [Data_array_location addObject:areaName];
            }
            if ([empaty_array2 containsObject:cityName]) {
                
            }
            else{
                [Data_array_location addObject:cityName];
            }
            
            if ([empaty_array2 containsObject:stateName]) {
                
            }
            else{
                [Data_array_location addObject:stateName];
            }
            
            if ([empaty_array2 containsObject:zipCode]) {
                
            }
            else{
                [Data_array_location addObject:zipCode];
            }
            
            NSMutableString *add_data2=[[NSMutableString alloc]init];
            if (Data_array_location.count>0) {
                cell.locationPlaceholde_imageView.image=[UIImage imageNamed:@"check_in_placeholder"];
                
                for (int i=0; i<Data_array_location.count; i++) {
                    
                    NSString *adress=[Data_array_location objectAtIndex:i];
                    if (i==0) {
                        [add_data2 appendFormat:@"%@", [NSString stringWithFormat:@"%@",adress]];
                    }
                    else
                        [add_data2 appendFormat:@"%@", [NSString stringWithFormat:@",%@",adress]];
                }
                cell.address_myFriends.text=add_data2;
                cell.topConstraint_ViewForRequests.priority=750;
                cell.viewto_useranme_constraint.priority=250;
            }
            else{
                cell.locationPlaceholde_imageView.image=[UIImage imageNamed:@""];
                cell.address_myFriends.text=@"";
                
                cell.topConstraint_ViewForRequests.priority=250;
                cell.viewto_useranme_constraint.priority=750;
            }
            
  
            
//        NSString * areaName=[NSString stringWithFormat:@"%@",[[filteredArray2 objectAtIndex:indexPath.row] objectForKey:@"address"]];
//        NSString * cityName=[NSString stringWithFormat:@"%@",[[filteredArray2 objectAtIndex:indexPath.row] objectForKey:@"city"]];
//        NSString * stateName=[NSString stringWithFormat:@"%@",[[filteredArray2 objectAtIndex:indexPath.row] objectForKey:@"state"]];
//        NSString *status_null;
//        
//        if ([areaName isEqualToString:@"<null>"] || [areaName isEqualToString:@""] || [areaName isEqualToString:@"null"] || areaName == nil) {
//            status_null=@"YES";
//            
//        }
//        else{
//            //        cell.location_imageview.image=[UIImage imageNamed:@"check_in_placeholder"];
//            cell.address_myFriends.text=areaName;
//        }
//        
//        
//        
//        if ([status_null isEqualToString:@"YES"]) {
//            if ([cityName isEqualToString:@"<null>"] || [cityName isEqualToString:@""] || [cityName isEqualToString:@"null"] || cityName == nil) {
//                status_null=@"YESSS";
//                
//            }
//            else{
//                cell.address_myFriends.text=cityName;
//            }
//            
//        }
//        else{
//            if ([cityName isEqualToString:@"<null>"]|| [cityName isEqualToString:@""] || [cityName isEqualToString:@"null"] || cityName == nil) {
//                status_null=@"YESS";
//                cell.locationPlaceholde_imageView.image=[UIImage imageNamed:@"check_in_placeholder"];
//                
//            }
//            else
//                cell.locationPlaceholde_imageView.image=[UIImage imageNamed:@"check_in_placeholder"];
//            
//            cell.address_myFriends.text=[NSString stringWithFormat:@"%@,%@",areaName,cityName];
//        }
//        
//        
//        
//        
//        if ([status_null isEqualToString:@"YESS"]) {
//            
//            if ([stateName isEqualToString:@"<null>"]|| [stateName isEqualToString:@""] || [stateName isEqualToString:@"null"] || stateName == nil) {
//                
//                
//            }
//            else
//                cell.locationPlaceholde_imageView.image=[UIImage imageNamed:@"check_in_placeholder"];
//            cell.address_myFriends.text=[NSString stringWithFormat:@"%@,%@",areaName,stateName];
//            
//        }
//        else if ([status_null isEqualToString:@"YESSS"]) {
//            
//            if ([stateName isEqualToString:@"<null>"]|| [stateName isEqualToString:@""] || [stateName isEqualToString:@"null"] || stateName == nil) {
//                
//                cell.address_myFriends.text=@"";
//                cell.locationPlaceholde_imageView.image=[UIImage imageNamed:@""];
//            }
//            else
//                cell.address_myFriends.text=stateName;
//            
//        }
//        else{
//            if ([stateName isEqualToString:@"<null>"]|| [stateName isEqualToString:@""] || [stateName isEqualToString:@"null"] || stateName == nil) {
//                
//                cell.address_myFriends.text=@"";
//                cell.locationPlaceholde_imageView.image=[UIImage imageNamed:@""];
//                
//            }
//            else
//                cell.address_myFriends.text=[NSString stringWithFormat:@"%@,%@,%@",areaName,cityName,stateName];
//        }
       
       
        NSString *profile_image_string = [NSString stringWithFormat:@"%@", [[filteredArray2 objectAtIndex:indexPath.row] objectForKey:@"profile_image"]];
         NSURL*url1=[NSURL URLWithString:profile_image_string];
        [cell.profileUser_myFriends sd_setImageWithURL:url1
                                  placeholderImage:[UIImage imageNamed:@"user_Placeholder"]
                                           options:SDWebImageRefreshCached];
        
        cell.viewForAccept_myFriends.hidden=YES;
        cell.viewForReject_myFriends.hidden=YES;
        cell.viewForFriends_myFriends.hidden=YES;
        cell.acceptBtn_myFriends.tag=indexPath.row;
        cell.rejectBtn_myFriends.tag=indexPath.row;
        NSString*statusString=[NSString stringWithFormat:@"%@",[[filteredArray2 objectAtIndex:indexPath.row] objectForKey:@"status"]];
        
        if ([statusString isEqualToString:@"<null>"] || [statusString isEqualToString:@""] || [statusString isEqualToString:@"null"] || statusString == nil) {
            
            
        }
        
        else{
            
            if ([statusString isEqualToString:@"friend"]) {
                
                cell.viewForFriends_myFriends.hidden=NO;
                
            }
            else if ([statusString isEqualToString:@"pending"])
            {
                cell.viewForFriends_myFriends.hidden=NO;
                cell.labe_friend_pending_myFriends.text=@"pending";
            }
            else
            {
                
                cell.viewForAccept_myFriends.hidden=NO;
                cell.viewForReject_myFriends.hidden=NO;
                cell.acceptBtn_myFriends.layer.borderWidth=1.0f;
                cell.acceptBtn_myFriends.layer.borderColor=[[UIColor whiteColor] CGColor];
                
                cell.rejectBtn_myFriends.layer.borderWidth=1.0f;
                cell.rejectBtn_myFriends.layer.borderColor=[[UIColor whiteColor] CGColor];
                
                
            }
            
        }
    }
        else{
         
//            without Search
            if (indexPath.row%2==0) {
                cell.contentview_myFriends.backgroundColor=ROUTES_CELL_BG_COLOUR1;
            }
            else
                cell.contentview_myFriends.backgroundColor=ROUTES_CELL_BG_COLOUR2;
            
           
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.userName_myFriends.text=[[arrayOfMyFriends objectAtIndex:indexPath.row] objectForKey:@"name"];
            cell.userName_myFriends.textColor=APP_YELLOW_COLOR;
          
            
            NSString * areaName=[NSString stringWithFormat:@"%@",[[arrayOfMyFriends objectAtIndex:indexPath.row] objectForKey:@"address"]];
            NSString * cityName=[NSString stringWithFormat:@"%@",[[arrayOfMyFriends objectAtIndex:indexPath.row] objectForKey:@"city"]];
            NSString * stateName=[NSString stringWithFormat:@"%@",[[arrayOfMyFriends objectAtIndex:indexPath.row] objectForKey:@"state"]];
            NSString *zipCode=[NSString stringWithFormat:@"%@",[[arrayOfMyFriends objectAtIndex:indexPath.row] objectForKey:@"zipcode"]];
//            NSString *status_null;
            
            
            NSMutableArray *empaty_array1=[[NSMutableArray alloc]init];
            [empaty_array1 addObject:@"<null>"];
            [empaty_array1 addObject:@"null"];
            [empaty_array1 addObject:@""];
            [empaty_array1 addObject:@"(null)"];
            [empaty_array1 addObject:@"0"];
            
            NSMutableArray *Data_array=[[NSMutableArray alloc]init];
            
            
            if ([empaty_array1 containsObject:areaName]) {
                
            }
            else{
                
                [Data_array addObject:areaName];
            }
            if ([empaty_array1 containsObject:cityName]) {
                
            }
            else{
                [Data_array addObject:cityName];
            }
            
            if ([empaty_array1 containsObject:stateName]) {
                
            }
            else{
                [Data_array addObject:stateName];
            }
            
            if ([empaty_array1 containsObject:zipCode]) {
                
            }
            else{
                [Data_array addObject:zipCode];
            }
            
            NSMutableString *add_data1=[[NSMutableString alloc]init];
            if (Data_array.count>0) {
                cell.locationPlaceholde_imageView.image=[UIImage imageNamed:@"check_in_placeholder"];
                
                for (int i=0; i<Data_array.count; i++) {
                    
                    NSString *adress=[Data_array objectAtIndex:i];
                    if (i==0) {
                        [add_data1 appendFormat:@"%@", [NSString stringWithFormat:@"%@",adress]];
                    }
                    else
                    {
                        int zip=[[Data_array objectAtIndex:i] intValue];
                        if (zip>0) {
                            [add_data1 appendFormat:@"%@", [NSString stringWithFormat:@" %@",adress]];
                        }
                        else
                            [add_data1 appendFormat:@"%@", [NSString stringWithFormat:@", %@",adress]];
                    }
                }
                cell.address_myFriends.text=add_data1;
                cell.topConstraint_ViewForRequests.priority=750;
                cell.viewto_useranme_constraint.priority=250;
            }
            else{
                cell.locationPlaceholde_imageView.image=[UIImage imageNamed:@""];
                cell.address_myFriends.text=@"";
                
                cell.topConstraint_ViewForRequests.priority=250;
                cell.viewto_useranme_constraint.priority=750;
            }
            
            
            
            
            NSString *profile_image_string = [NSString stringWithFormat:@"%@", [[arrayOfMyFriends objectAtIndex:indexPath.row] objectForKey:@"profile_image"]];
            NSURL*url1=[NSURL URLWithString:profile_image_string];
            [cell.profileUser_myFriends sd_setImageWithURL:url1
                                          placeholderImage:[UIImage imageNamed:@"user_Placeholder"]
                                                   options:SDWebImageRefreshCached];
            
            cell.viewForAccept_myFriends.hidden=YES;
            cell.viewForReject_myFriends.hidden=YES;
            cell.viewForFriends_myFriends.hidden=YES;
            cell.acceptBtn_myFriends.tag=indexPath.row;
            cell.rejectBtn_myFriends.tag=indexPath.row;
            NSString*statusString=[NSString stringWithFormat:@"%@",[[arrayOfMyFriends objectAtIndex:indexPath.row] objectForKey:@"status"]];
            
            if ([statusString isEqualToString:@"<null>"] || [statusString isEqualToString:@""] || [statusString isEqualToString:@"null"] || statusString == nil) {
                
                
            }
            
            else{
                
                if ([statusString isEqualToString:@"friend"]) {
                    
                    cell.viewForFriends_myFriends.hidden=NO;
                    cell.labe_friend_pending_myFriends.text=@"friends";
                    
                }
                
                else if ([statusString isEqualToString:@"pending"])
                {
                    cell.viewForFriends_myFriends.hidden=NO;
                    cell.labe_friend_pending_myFriends.text=@"pending";
                }
                else
                {
                    
                    cell.viewForAccept_myFriends.hidden=NO;
                    cell.viewForReject_myFriends.hidden=NO;
                    cell.acceptBtn_myFriends.layer.borderWidth=1.0f;
                    cell.acceptBtn_myFriends.layer.borderColor=[[UIColor whiteColor] CGColor];
                    
                    cell.rejectBtn_myFriends.layer.borderWidth=1.0f;
                    cell.rejectBtn_myFriends.layer.borderColor=[[UIColor whiteColor] CGColor];
                    
                    
                }
                
            }
        }
        return cell;
   
    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  
    ProfileView*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileView"];
     if ([view_Status isEqualToString:@""]||[view_Status isEqualToString:@"PUBLIC"]) {
         if (isFiltered) {
             NSString *friend_id=[[filteredArray objectAtIndex:indexPath.row] objectForKey:@"friend_id"];
             controller.id_user=friend_id;
             controller.friend_status=[[filteredArray objectAtIndex:indexPath.row] objectForKey:@"status"];
         }
         else
         {
             NSString *friend_id=[[arrayOfPublicFriends objectAtIndex:indexPath.row] objectForKey:@"friend_id"];
             controller.id_user=friend_id;
             controller.friend_status=[[arrayOfPublicFriends objectAtIndex:indexPath.row] objectForKey:@"status"];
         }
        
     }
     else{
         
         if (isFiltered) {
             NSString *friend_id=[[filteredArray2 objectAtIndex:indexPath.row] objectForKey:@"friend_id"];
             controller.id_user=friend_id;
             controller.friend_status=[[filteredArray2 objectAtIndex:indexPath.row] objectForKey:@"status"];
         }
         else
         {
             NSString *friend_id=[[arrayOfMyFriends objectAtIndex:indexPath.row] objectForKey:@"friend_id"];
             controller.id_user=friend_id;
             controller.friend_status=[[arrayOfMyFriends objectAtIndex:indexPath.row] objectForKey:@"status"];
         }
        
     }
    
  
     controller.is_From=@"Friends";
    [self.navigationController pushViewController:controller animated:YES];

    
    
}
#pragma mark - Search methods
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)aSearchBar {
    self.searchBar.placeholder = @"search";
    self.searchBar.showsCancelButton = YES;
    
    return YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)SearchBar
{
    if([view_Status isEqualToString:@""]||[view_Status isEqualToString:@"PUBLIC"])
    {
    self.searchBar.text = nil;
    self.searchBar.placeholder = @"search";
    self.searchBar.showsCancelButton = NO;
    [self.searchBar resignFirstResponder];
    isFiltered = NO;
    [_public_friends_tableView setContentOffset:CGPointZero animated:NO];
    [_public_friends_tableView reloadData];
    }
    else{
        self.searchBar.text = nil;
        self.searchBar.placeholder = @"search";
        self.searchBar.showsCancelButton = NO;
        [self.searchBar resignFirstResponder];
        isFiltered = NO;
         [_my_friends_tableView setContentOffset:CGPointZero animated:NO];
         [_my_friends_tableView reloadData];
    }
   
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar resignFirstResponder];
    for (UIView *view in searchBar.subviews)
    {
        for (id subview in view.subviews)
        {
            if ( [subview isKindOfClass:[UIButton class]] )
            {
                [subview setEnabled:YES];
                
                NSLog(@"enableCancelButton");
                return;
            }
        }
    }
    
    [searchBar setShowsCancelButton:YES animated:YES];
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
    if(searchText.length == 0)
    {
        isFiltered = NO;
    }
    else
    {
      if([view_Status isEqualToString:@""]||[view_Status isEqualToString:@"PUBLIC"])
        {
        
        
        
        isFiltered = YES;
        filteredArray = [[NSMutableArray alloc]init];
        
        for(NSDictionary *frnds_Dict in arrayOfPublicFriends)
        {
            NSString *friend_Name=[frnds_Dict valueForKey:@"name"];
            
            NSRange stringRange = [friend_Name rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if(stringRange.location != NSNotFound)
            {
                [filteredArray addObject:frnds_Dict];
                
            }
        }
    
            [_public_friends_tableView reloadData];
        }
        else
        {
            isFiltered = YES;
            filteredArray2 = [[NSMutableArray alloc]init];
            
            for(NSDictionary *frnds_Dict1 in arrayOfMyFriends)
            {
                NSString *friend_Name1=[frnds_Dict1 valueForKey:@"name"];
                
                NSRange stringRange = [friend_Name1 rangeOfString:searchText options:NSCaseInsensitiveSearch];
                
                if(stringRange.location != NSNotFound)
                {
                    [filteredArray2 addObject:frnds_Dict1];
                    
                }
            }
            
            [_my_friends_tableView reloadData];
        }
 }
    
    
}
- (IBAction)onClick_frnds_rideGoingBtn:(id)sender {
    
    self.frnds_scrollLabel.hidden=YES;
    self.frnds_rideGoingBtn.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }

}

- (IBAction)onClick_statusBtn:(UIButton *)sender
{
      indicaterview.hidden=NO;
    if ([view_Status isEqualToString:@""]||[view_Status isEqualToString:@"PUBLIC"]) {
        if (isFiltered) {
            friendID=[NSString stringWithFormat:@"%@",[[filteredArray objectAtIndex:sender.tag] objectForKey:@"friend_id"]];
            NSLog(@"Add a Friend Id %@",friendID);
            NSMutableDictionary *frnd_dict = [NSMutableDictionary new];
            [frnd_dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
            [frnd_dict setObject:friendID forKey:@"user_friend_id"];
            NSLog(@"Friend Dict %@",frnd_dict);
            [SHARED_API add_a_friend_Request1:frnd_dict withSuccess:^(NSDictionary *response)
             {
                indicaterview.hidden=YES;
                 NSLog(@"Add Frnd In Response %@",response);
                 //         [self.public_friends_tableView reloadData];
                 [self get_Friends_Data];
                 
                 
             } onfailure:^(NSError *theError)
             {
                   indicaterview.hidden=YES;
                 NSLog(@"Add Frnd In Error %@",theError);
                 
             }];
            
        }
        else{
            friendID=[NSString stringWithFormat:@"%@",[[arrayOfPublicFriends objectAtIndex:sender.tag] objectForKey:@"friend_id"]];
            NSLog(@"Add a Friend Id %@",friendID);
            NSMutableDictionary *frnd_dict = [NSMutableDictionary new];
            [frnd_dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
            [frnd_dict setObject:friendID forKey:@"user_friend_id"];
            NSLog(@"Friend Dict %@",frnd_dict);
            [SHARED_API add_a_friend_Request1:frnd_dict withSuccess:^(NSDictionary *response)
             {
                   indicaterview.hidden=YES;
                 NSLog(@"Add Frnd In Response %@",response);
                 [self get_Friends_Data];
                 
                 
             } onfailure:^(NSError *theError)
             {
                indicaterview.hidden=YES;
                 NSLog(@"Add Frnd In Error %@",theError);
                 
             }];
        }
        
    }
    else{
          indicaterview.hidden=YES;
    }
    
    }

- (IBAction)onClick_inviteFrnds_btn:(id)sender {
    
//    NSLog( @"### running FB sdk version: %@", [FBSDKSettings sdkVersion] );
    FBSDKAppInviteContent *content =[[FBSDKAppInviteContent alloc] init];
    
//    last app link 
//    content.appLinkURL = [NSURL URLWithString:@"https://fb.me/854002744765943"];
//    before current app link
//    content.appLinkURL=[NSURL URLWithString:@"https://fb.me/854008464765371"];
//    cureent app link 
      content.appLinkURL=[NSURL URLWithString:@"https://fb.me/854106071422277"];
//    content.appLinkURL=[NSURL URLWithString:@""];
    
    //optionally set previewImageURL
    content.appInvitePreviewImageURL = [NSURL URLWithString:@"http://openroadrides.thinkwithebiz.com/images/logo.png"];
    
    // Present the dialog. Assumes self is a view controller
    // which implements the protocol `FBSDKAppInviteDialogDelegate`.
    [FBSDKAppInviteDialog showFromViewController:self
                                     withContent:content
                                        delegate:self];

}
- (void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog
 didCompleteWithResults:(NSDictionary *)results
    {
        NSLog(@"Facebook Results %@",results);
    }
-(void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialogdidFailWithError
    {
        NSLog(@"Facebook Error %@",appInviteDialogdidFailWithError);
    }

-(void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didFailWithError:(NSError *)error
    {
    
        NSLog(@"Facebook Error %@",error);
    }

- (IBAction)onClick_acceptBtn:(UIButton *)sender {
    indicaterview.hidden=NO;
     if ([view_Status isEqualToString:@""]||[view_Status isEqualToString:@"PUBLIC"]) {
          indicaterview.hidden=YES;
     }
    else
    {
    if (isFiltered) {
        myFrnd_FriendID=[NSString stringWithFormat:@"%@",[[filteredArray2 objectAtIndex:sender.tag] objectForKey:@"friend_id"]];
        accept_reject_string=@"accept";
        NSMutableDictionary *myFrnd_dict = [NSMutableDictionary new];
        [myFrnd_dict setObject:@"1" forKey:@"user_id"];
        [myFrnd_dict setObject:[NSString stringWithFormat:@"%@",myFrnd_FriendID] forKey:@"user_friend_id"];
        [myFrnd_dict setValue:[NSString stringWithFormat:@"%@",accept_reject_string] forKey:@"accept_or_reject_status"];
        NSLog(@"Friend Dict %@",myFrnd_dict);
        [SHARED_API accept_reject_frnd_request:myFrnd_dict withSuccess:^(NSDictionary *response)
         {
             indicaterview.hidden=YES;
             NSLog(@"Accept Frnd In Response %@",response);
             [self get_Friends_Data];
             
             
         } onfailure:^(NSError *theError)
         {
             NSLog(@"Accept Frnd In Error %@",theError);
            indicaterview.hidden=YES;
         }];
        
    }
    else
    {

        myFrnd_FriendID=[NSString stringWithFormat:@"%@",[[arrayOfMyFriends objectAtIndex:sender.tag] objectForKey:@"friend_id"]];
        accept_reject_string=@"accept";
        NSMutableDictionary *myFrnd_dict = [NSMutableDictionary new];
        [myFrnd_dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
         [myFrnd_dict setObject:[NSString stringWithFormat:@"%@",myFrnd_FriendID] forKey:@"user_friend_id"];
//        [myFrnd_dict setObject:[NSString stringWithFormat:@"%@",myFrnd_FriendID] forKey:@"user_friend_id"];
        [myFrnd_dict setValue:[NSString stringWithFormat:@"%@",accept_reject_string] forKey:@"accept_or_reject_status"];
        NSLog(@"Friend Dict %@",myFrnd_dict);
        [SHARED_API accept_reject_frnd_request:myFrnd_dict withSuccess:^(NSDictionary *response)
         {
             
             NSLog(@"Accept Frnd In Response %@",response);
             if([[response objectForKey:STATUS] isEqualToString:SUCCESS])
             {
                 //                [indicator stopAnimating];
            indicaterview.hidden=YES;
             [self get_Friends_Data];
             }
             else if([[response objectForKey:STATUS] isEqualToString:FAIL])
             {
                 [SHARED_HELPER showAlert:ServiceFail2];
                  indicaterview.hidden=YES;
             }
             
             
         } onfailure:^(NSError *theError)
         {
             NSLog(@"Accept Frnd In Error %@",theError);
            indicaterview.hidden=YES;
         }];

        
    }
}

}

- (IBAction)onClick_rejectBtn:(UIButton *)sender {
      indicaterview.hidden=NO;
    if ([view_Status isEqualToString:@""]||[view_Status isEqualToString:@"PUBLIC"])
    {
          indicaterview.hidden=YES;
    }
    else
    {
        
        
        if (isFiltered) {
            myFrnd_FriendID=[NSString stringWithFormat:@"%@",[[filteredArray2 objectAtIndex:sender.tag] objectForKey:@"friend_id"]];
            accept_reject_string=@"reject";
            NSMutableDictionary *myFrnd_dict = [NSMutableDictionary new];
            [myFrnd_dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
            [myFrnd_dict setObject:myFrnd_FriendID forKey:@"user_friend_id"];
            [myFrnd_dict setValue:[NSString stringWithFormat:@"%@",accept_reject_string] forKey:@"accept_or_reject_status"];
            NSLog(@"Friend Dict %@",myFrnd_dict);
            [SHARED_API accept_reject_frnd_request:myFrnd_dict withSuccess:^(NSDictionary *response)
             {
                 
                 NSLog(@"Reject Frnd In Response %@",response);
                 
                indicaterview.hidden=YES;
                 
                 [self get_Friends_Data];
                 
                 
             } onfailure:^(NSError *theError)
             {
                 NSLog(@"Reject Frnd In Error %@",theError);
                   indicaterview.hidden=YES;
             }];
                     
        }
        else
        {
            myFrnd_FriendID=[NSString stringWithFormat:@"%@",[[arrayOfMyFriends objectAtIndex:sender.tag] objectForKey:@"friend_id"]];
            accept_reject_string=@"reject";
            NSMutableDictionary *myFrnd_dict = [NSMutableDictionary new];
            [myFrnd_dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
            [myFrnd_dict setObject:myFrnd_FriendID forKey:@"user_friend_id"];
            [myFrnd_dict setValue:[NSString stringWithFormat:@"%@",accept_reject_string] forKey:@"accept_or_reject_status"];
            NSLog(@"Friend Dict %@",myFrnd_dict);
            [SHARED_API accept_reject_frnd_request:myFrnd_dict withSuccess:^(NSDictionary *response)
             {
                   indicaterview.hidden=YES;
                 NSLog(@"Accept Frnd In Response %@",response);
                 [self get_Friends_Data];
                 
                 
             } onfailure:^(NSError *theError)
             {
                 NSLog(@"Accept Frnd In Error %@",theError);
                  indicaterview.hidden=YES;
             }];
        }
    }

}


@end
