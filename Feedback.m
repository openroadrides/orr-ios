//
//  Feedback.m
//  openroadrides
//
//  Created by apple on 15/09/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "Feedback.h"

@interface Feedback ()

@end

@implementation Feedback

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
//   self.navigationController.navigationBar.hidden=NO;
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
     [self.navigationController.navigationBar setTranslucent:NO];
    
    self.title = @"FEEDBACK";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    
    
    self.feedback_scrollLabel.hidden=YES;
    self.feedback_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.feedback_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.feedback_scrollLabel.textColor = [UIColor blackColor];
    self.feedback_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.feedback_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.feedback_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.feedback_scrollLabel.scrollSpeed = 60; // pixels per second
    self.feedback_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.feedback_scrollLabel.fadeLength = 0.f;
    self.feedback_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.feedback_scrollLabel observeApplicationNotifications];
    
    
    self.feedback_rideGoingBtn.hidden=YES;
    if (appDelegate.ridedashboard_home) {
        
        self.feedback_scrollLabel.hidden=NO;
        self.feedback_rideGoingBtn.hidden=NO;
    }
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [leftBtn addTarget:self action:@selector(back_Action) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.frame = CGRectMake(0, 0, 25, 25);
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"back_Image"] forState:UIControlStateNormal];
    UIBarButtonItem *leftBackButton=[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:leftBackButton, nil];
    
    subRatingView = [[RatingBar alloc]initWithSize:CGSizeMake (260, 50) AndPosition:CGPointMake(0, 5)];
    RBRatings rating=[subRatingView getcurrentRatings];
    Rate=rating/2.0;
    subRatingView.backgroundColor=[UIColor clearColor];
    [self.Feedback_ratingView addSubview:subRatingView];
    [self.view layoutIfNeeded];
    
    
//    [self.Feedback_ratingView initRateBar];
//    RBRatings rating=[_Feedback_ratingView getcurrentRatings];
//    Rate=rating/2.0;;
    
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    _feedback_commentTextview.inputAccessoryView = numberToolbar;

}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden=NO;
    [[self navigationController] setNavigationBarHidden:NO animated:NO];

}
-(void)back_Action{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)doneWithNumberPad{
    [_feedback_commentTextview resignFirstResponder];
}
-(void)cancelNumberPad{
    
    [_feedback_commentTextview resignFirstResponder];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onClick_feedback_submitBtn:(id)sender {
     _feedback_commentTextview.text=[_feedback_commentTextview.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (![SHARED_HELPER checkIfisInternetAvailable]) {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
    [self.view endEditing:YES];
    if(_feedback_commentTextview.text.length == 0)
    {
        [SHARED_HELPER showAlert:CommentText];
    }
    else
    {
     [self feedback_send];
        
    }
}

-(void)feedback_send{
    RBRatings rating=[subRatingView getcurrentRatings];
    Rate=rating/2.0;
//    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//    activeIndicatore.center = self.view.center;
//    activeIndicatore.color = APP_YELLOW_COLOR;
//    activeIndicatore.hidesWhenStopped = TRUE;
//    [activeIndicatore startAnimating];
//    [self.view addSubview:activeIndicatore];
    
    indicaterview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    activeIndicatore.backgroundColor=[UIColor clearColor];
    activeIndicatore = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.frame = CGRectMake(0.0, 0.0, 80, 80);
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    [indicaterview addSubview:activeIndicatore];
    [self.view addSubview:indicaterview];
    [indicaterview bringSubviewToFront:self.view];
    [activeIndicatore startAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    indicaterview.hidden=NO;
//    "{
//    ""user_id"":"""",
//    ""username"":"""",
//    ""email"":"""",
//    ""comment"":"""",
//    ""rating"":""""
//}"
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = malloc(size);
   sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithCString:machine encoding:NSUTF8StringEncoding];
    NSString *name=[[UIDevice currentDevice] name];
    NSString *model=[SHARED_HELPER platformType:platform];
    NSString *version=[[UIDevice currentDevice] systemVersion];
    NSString *our_app= [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];

//    NSString*bundle_version=[[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey];
    
    NSString*detail=[NSString stringWithFormat:@"Device Name: %@\nDevice Model: %@\nOS Version: %@\nApplication Version: %@\nApplication Bundle Id: %@",name,model,version,our_app,bundleIdentifier];
    
    NSMutableDictionary *feedback_dict = [NSMutableDictionary new];
    
    [feedback_dict setObject:_feedback_commentTextview.text forKey:@"comment"];
    [feedback_dict setObject:[NSString stringWithFormat:@"%.1f",Rate] forKey:@"rating"];
    [feedback_dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
    [feedback_dict setObject:[Defaults valueForKey:@"UserName"] forKey:@"username"];
    [feedback_dict setObject:[Defaults valueForKey:@"EMAIL"] forKey:@"email"];
    [feedback_dict setObject:detail forKey:@"detail"];
    
     NSLog(@"Feedback dict %@",feedback_dict);
    [SHARED_API feedbackRequestsWithParams:feedback_dict withSuccess:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           NSLog(@"Feedback Response%@",response);
                        if([[response objectForKey:STATUS] isEqualToString:SUCCESS])
                           {
//                               
//                               if ([activeIndicatore isAnimating]) {
//                                   [activeIndicatore stopAnimating];
//                                   [activeIndicatore removeFromSuperview];
//                               }
                                indicaterview.hidden=YES;
                               _feedback_commentTextview.text=@"";
                             
                               RBRatings rating=[subRatingView getcurrentRatings];
                               Rate=rating/2.0;
                                 Rate=0;
                               [self.navigationController popViewControllerAnimated:YES];
                               [SHARED_HELPER showAlert:feedbackSuccessText];
                               
                               
                           }
                           else{
//                               if ([activeIndicatore isAnimating]) {
//                                   [activeIndicatore stopAnimating];
//                                   [activeIndicatore removeFromSuperview];
//                               }
                                indicaterview.hidden=YES;
                               [SHARED_HELPER showAlert:Forgotfail];
                           }
                           
                       });
        
    } onfailure:^(NSError *theError) {
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           
//                           if ([activeIndicatore isAnimating]) {
//                               [activeIndicatore stopAnimating];
//                               [activeIndicatore removeFromSuperview];
//                           }
                            indicaterview.hidden=YES;
                           [SHARED_HELPER showAlert:ServiceFail];
                           
                           
                       });
    }];
    
    
}


- (IBAction)onClick_feedback_rideGoingBtn:(id)sender {
    
    self.feedback_scrollLabel.hidden=YES;
    self.feedback_rideGoingBtn.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }
}
@end
