//
//  PublicFriendsCell.h
//  openroadrides
//
//  Created by apple on 26/06/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PublicFriendsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *contentView_publicfriends;
@property (weak, nonatomic) IBOutlet UIButton *checkbox_btn;
@property (weak, nonatomic) IBOutlet UIImageView *profile_imageView;
@property (weak, nonatomic) IBOutlet UILabel *username_publicFriend;
@property (weak, nonatomic) IBOutlet UILabel *address_publicfriend;
@property (weak, nonatomic) IBOutlet UIButton *statusbtn_publicFriend;
@property (weak, nonatomic) IBOutlet UIImageView *imageView_statusBtn;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingConstraintForstatusImageView;
@property (weak, nonatomic) IBOutlet UIImageView *location_imageview;
@property (weak, nonatomic) IBOutlet UIView *view_addafriendbtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint_view_addafriend;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *View_To_Username_Constraint;
@end
