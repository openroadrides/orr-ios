//
//  RideDashboard.h
//  openroadrides
//
//  Created by apple on 03/05/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <CoreLocation/CoreLocation.h>
#import <AVFoundation/AVFoundation.h>
#import "AFNetworking.h"
#import <objc/runtime.h>
#import <GoogleMaps/GoogleMaps.h>
#import "Constants.h"
#import <AWSS3/AWSS3.h>
#import <AWSCore/AWSCore.h>
#import "BIZPopupViewController.h"
#import "AddAPlace.h"
#import "SaveARoute.h"
#import "APIHelper.h"
#import "AppHelperClass.h"
#import "TCMXMLWriter.h"
#import "RideDetailViewController.h"
#import <GPXParser.h>
#import "DashboardViewController.h"
#import "Promotions.h"
#import "Rides+CoreDataClass.h"
#import "Routes+CoreDataClass.h"
#import "Model+CoreDataModel.h"

@import GoogleMaps;

@interface RideDashboard : UIViewController<CLLocationManagerDelegate,GMSMapViewDelegate>
{
    NSString *ride_Finished,*map_Clicked,*manual_Paused;
    GMSMapView *mapView;
    NSMutableArray *trackpoints,*way_Points_Array,*addPlacesDataArray;
    UILabel *distance_lab;
    int dis_int_for_dflt_marker,original_Marker_Val,fore_ground_count,default_marker_forService;
    NSInteger marker_count,wheather_call,loc_Update_count;
    UIView *setDefaultMarker_view;
   
    NSString * locations_data;
   
    BOOL isPanned,isTapped;
    UIActivityIndicatorView  *activeIndicatore,*activeIndicatore2;
    NSString *back_Clicked,*first_Ride_Complete;
    
    NSInteger ii,places_s3;
    NSInteger select_route_places_count;
    
    double add_place_lat,add_Place_Long;
    
    NSMutableArray *add_places,*tracks_array,*way_array;

     NSString *start_lat_long,*end_lat_long;
     double x,z;
    
    NSString *selected_route_title,*select_Route_Des,*created_ride_description,*selected_route_owner_id,*ride_owner_id;
    NSString *which_Ride_this_is;
    
    NSString *route_strat_adress,*route_end_Adress,*ride_strat_adress,*ride_end_Adress,*ride_Start_Lat,*ride_End_Lat,*ride_Start_Long,*ride_End_Long,*ride_Title,*selected_Route_GPX;
    
    UIView *indicaterview;
    
    //addplaces add coredata
    NSMutableArray *add_places_imag_urls;
    
 //Core Data
    int64_t local_Ride_ID;
    NSString *s3_inProgress;
    Rides *rides_object;
    NSManagedObject *routes_object;
    
    NSString *encoding_path;
}


@property (strong, nonatomic)NSString *is_From,*ride_id_From_Notifications;
@property (strong, nonatomic) IBOutlet UILabel *ride_Name_label;
@property (strong, nonatomic) IBOutlet UILabel *route_Name_Label;
@property BOOL updateRideStatus;
@property (strong, nonatomic) IBOutlet UIButton *Promotions_Btn_Outlet;

- (IBAction)Promotions_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *rideDashboard_Promotions_View;

@property (weak, nonatomic) IBOutlet UIButton *ridedashboard_homeBtn;
- (IBAction)onclick_ridedashboard_homeButton:(id)sender;


@property (weak, nonatomic) IBOutlet UIView *view_SpeedDetails;
@property (weak, nonatomic) IBOutlet UIImageView *imageView_speedDetails_bg;
@property (weak, nonatomic) IBOutlet UIButton *checkIn_btn;
- (IBAction)onClick_checkin_btn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *defaultMarker_btn;

- (IBAction)onClick_defaultMarker_btn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *custommarkers_btn;

@property (strong, nonatomic) IBOutlet UIButton *custom_Markers_btn2;


- (IBAction)onClick_customMarkers_btn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *mapToggle_btn;
- (IBAction)onClick_mapToggle_btn:(id)sender;


@property (weak, nonatomic) IBOutlet UIView *view_rideDashboard;
@property (weak, nonatomic) IBOutlet UIView *view_map_rideDashboard;
@property  BOOL boolvalue_rideToggle;


@property (weak, nonatomic) IBOutlet UILabel *label_wheather_placeName;
@property (weak, nonatomic) IBOutlet UILabel *label_temprature;
@property (weak, nonatomic) IBOutlet UILabel *label_weather_condition;

@property (weak, nonatomic) IBOutlet UILabel *label_totalDistance_rideDashboard;
@property (weak, nonatomic) IBOutlet UILabel *label_averageSpeed_rideDashboard;
@property (weak, nonatomic) IBOutlet UILabel *label_maxSpeed_rideDashboard;
@property (weak, nonatomic) IBOutlet UILabel *label_rideTime_rideDashboard;
@property (weak, nonatomic) IBOutlet UILabel *label_totalTime_rideDashboard;
@property (weak, nonatomic) IBOutlet UILabel *label_elevation_rideDashboard;
@property (weak, nonatomic) IBOutlet UILabel *labe_totalDistance_mapRideDashboard;
@property (weak, nonatomic) IBOutlet UILabel *label_totalTime_mapRideDashboard;
@property (weak, nonatomic) IBOutlet UILabel *label_averageSpeed_mapRideDashboard;
@property (weak, nonatomic) IBOutlet UIButton *startAndPause_btn;
- (IBAction)onClick_startAndPause_btn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *finish_btn;
- (IBAction)onClick_finish_btn:(id)sender;
@property NSTimer *rideTimeStart,*totalTimeStart;
- (IBAction)zoomToUserLocations:(id)sender;
- (IBAction)Back_Action:(id)sender;




@property (weak, nonatomic) IBOutlet UIView *content_map_showing_view;
@property (strong, nonatomic) IBOutlet UIView *checkin_View;
@property (strong, nonatomic) IBOutlet UIView *default_Marker_view;
@property (strong, nonatomic) IBOutlet UIView *add_Place_View;



@property (strong, nonatomic) IBOutlet UIView *bottom_ride_start_stop_view;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottom_ride_start_stop_view_height;
@property (strong, nonatomic) IBOutlet UIView *markers_View2;
@property (strong, nonatomic) IBOutlet UIView *check_in_View2;
@property (strong, nonatomic) IBOutlet UIView *default_marker_View2;
@property (strong, nonatomic) IBOutlet UIView *add_Places_View2;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *check_in2_to_Add_Palces_constarint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *ride_view_cintent_View_eight_constraint;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *check_in2_To_Default_marker_View_Constraint;
@property (strong, nonatomic) IBOutlet UIButton *check_in_btn_2;
@property (strong, nonatomic) IBOutlet UIButton *default_markers_btn_2;
@property (strong, nonatomic) IBOutlet UIImageView *bottom_ride_start_view_imageview;

// s3
@property (nonatomic, strong) AWSS3TransferManagerUploadRequest *uploadRequest;
@property (nonatomic) uint64_t filesize;
@property (nonatomic) uint64_t amountUploaded;
@property (nonatomic, strong) UILabel *progressLabel;

//CoreData
@property NSEntityDescription * dataEntity;
- (IBAction)Get_Riders_Count:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *riders_Count;

@property (weak, nonatomic) IBOutlet UIImageView *start_ride_image_View;
@property (weak, nonatomic) IBOutlet UIImageView *finish_Ride_Imageview;


@end
