//
//  StartARide.h
//  openroadrides
//
//  Created by apple on 03/07/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "scheduleRideCollectionViewCell.h"
#import "Constants.h"
#import "APIHelper.h"
#import "RideDashboard.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "RoutesVC.h"
#import "AppDelegate.h"
#import "Schedule_Rides_TableCell.h"
@interface StartARide : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *arrayOfScheduleRides;
    UIActivityIndicatorView  *activeIndicatore;
    UILabel *NO_DATA;
    
}
@property (weak, nonatomic) IBOutlet UIButton *freeRide_btn;
- (IBAction)onClick_freeRide_btn:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *scheduleRide_TableView;
@property (weak, nonatomic) IBOutlet UIView *subView_scheduleRide;
@property  NSMutableArray *scheduledFrnds_array;
@property  NSMutableArray *scheduledGroups_array;
- (IBAction)onClick_Scdule_Ride_btn:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *noData_label;


@property (weak, nonatomic) IBOutlet UIImageView *choose_Route_ImgView;
@property (weak, nonatomic) IBOutlet UIImageView *choose_Friend_ImgView;
@property (weak, nonatomic) IBOutlet UIImageView *choose_Group_ImgView;

@property (weak, nonatomic) IBOutlet UIButton *sch_Ride_Button;
@property (weak, nonatomic) IBOutlet UIView *choose_Route_View;
@property (weak, nonatomic) IBOutlet UIView *choose_Friends_View;
@property (weak, nonatomic) IBOutlet UIView *choose_Group_View;
@property (weak, nonatomic) IBOutlet UILabel *choose_Route_Label;
@property (weak, nonatomic) IBOutlet UILabel *choose_Friend_Label;
@property (weak, nonatomic) IBOutlet UILabel *choose_Group_Label;
@property (weak, nonatomic) IBOutlet UILabel *Selected_Friends_Count_Label;
@property (weak, nonatomic) IBOutlet UILabel *Selected_Groups_Count_Label;




- (IBAction)onClick_Choose_Route_btn:(id)sender;
- (IBAction)onClick_Choose_Friend_btn:(id)sender;
- (IBAction)onClick_Choose_Group_btn:(id)sender;



@end
