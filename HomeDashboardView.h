//
//  HomeDashboardView.h
//  openroadrides
//
//  Created by apple on 22/09/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeDashboardCollectionCell.h"
#import "Feedback.h"
#import "RidesController.h"
#import "RoutesVC.h"
#import "Promotions.h"
#import "EventsViewController.h"
#import "NewsViewController.h"
#import "MarketPlace.h"
#import "BrowseRidersViewController.h"
#import "Constants.h"
#import <mach/mach.h>
#import "Model+CoreDataModel.h"
#import "CBAutoScrollLabel.h"

@interface HomeDashboardView : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>
{
    UIActivityIndicatorView  *activeIndicatore;
    UIView *indicaterview;
    NSMutableArray *links_array;
    UIView *links_Main_view,*links_sub_view;
    int64_t pending_Ride_id;
    Rides *Rides_object;
    double user_current_latitude,user_current_longtitude;
}
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (weak, nonatomic) IBOutlet UICollectionView *homeDashboard_collectionView;
@property NSMutableArray *arrayOfHomeDashboardItems;
@property (weak, nonatomic) IBOutlet UIButton *rideGoingBtn;
- (IBAction)onClick_rideGoingBtn:(id)sender;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *homeDashboard_scrollLabel;

- (IBAction)clicked_items_Here:(UIButton *)sender;



@end
