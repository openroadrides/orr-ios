//
//  StartARide.m
//  openroadrides
//
//  Created by apple on 03/07/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "StartARide.h"

@interface StartARide ()
{
    UIBarButtonItem *item0;
    NSString *ride_ID;
    NSArray *itemsfrdid,*itemsgroupsid;
    AppDelegate *app_delegate;
}
@end
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) // iPhone and       iPod touch style UI

#define IS_IPHONE_5 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0f)
#define IS_IPHONE_6 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 667.0f)
#define IS_IPHONE_6P (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 736.0f)
#define IS_IPHONE_4 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height < 568.0f)

@implementation StartARide
#pragma mark - viewDidLoad
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Start Your Ride";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [leftBtn addTarget:self action:@selector(leftBtn:) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.frame = CGRectMake(0, 0, 25, 25);
    //    [leftBtn setBackgroundImage:[UIImage imageNamed:@"Finalback-arrow.png"] forState:UIControlStateNormal];
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"close_icon"] forState:UIControlStateNormal];
    UIBarButtonItem *leftBackButton=[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    item0=leftBackButton;
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:item0, nil];
    
    self.freeRide_btn.layer.cornerRadius=5.0f;
    self.freeRide_btn.layer.masksToBounds=YES;
    
    self.scheduleRide_TableView.dataSource=self;
    self.scheduleRide_TableView.delegate=self;
    
    _scheduledFrnds_array=[[NSMutableArray alloc]init];
    _scheduledGroups_array=[[NSMutableArray alloc]init];
    
    
    self.noData_label.hidden=YES;
    [self schedule_Ride_list];

    
    
    appDelegate.select_Route_Id_For_CreateAndFreeRide=@"";
    appDelegate.select_Route_Address_For_CreateAndFreeRide=@"";
    appDelegate.arrayOfSelectedFrndsAndRiders=[[NSMutableArray alloc]init];
    appDelegate.arrayOfSelectedGroupsAndRiders=[[NSMutableArray alloc]init];
    //
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ride_Scheduled_Refresh) name:@"Start_Ride_From_Ride_Scheduled" object:nil];
    self.scheduleRide_TableView.backgroundColor=[UIColor clearColor];
    self.scheduleRide_TableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    
    self.choose_Friends_View.layer.borderWidth=1.0;
    self.choose_Group_View.layer.borderWidth=1.0;
    self.choose_Route_View.layer.borderWidth=1.0;
    self.choose_Friends_View.layer.borderColor=[[UIColor whiteColor] CGColor];
    self.choose_Group_View.layer.borderColor=[[UIColor whiteColor] CGColor];
    self.choose_Route_View.layer.borderColor=[[UIColor whiteColor] CGColor];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(Route_Select_Deselect) name:@"Start_Ride_From_Route_Select_Deselect" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(Friend_Select_Deselect) name:@"Start_Ride_From_Friends_Select_Deselect" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(Group_Select_Deselect) name:@"Start_Ride_From_Group_Select_Deselect" object:nil];
    
    self.choose_Route_ImgView.image=[UIImage imageNamed:@"unselect_route"];
    self.choose_Friend_ImgView.image=[UIImage imageNamed:@"unselect_friend"];
    self.choose_Group_ImgView.image=[UIImage imageNamed:@"unselect_group"];
    
    
   
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    //appDelegate.select_Route_Id_For_CreateAndFreeRide=@"";
    appDelegate.ride_ID_Api=@"";
    appDelegate.Ride_Name_From_Schedule=@"";
}
#pragma mark - Tableview

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
     return [arrayOfScheduleRides count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"cell";
    Schedule_Rides_TableCell *cell = (Schedule_Rides_TableCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"Schedule_Rides_TableCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    if (indexPath.row%2==0) {
        cell.bg_View.backgroundColor=ROUTES_CELL_BG_COLOUR1;
    }
    else
        cell.bg_View.backgroundColor=ROUTES_CELL_BG_COLOUR2;
    
    
    NSDictionary *dict=[arrayOfScheduleRides objectAtIndex:indexPath.row];
    cell.ride_Name.text=[dict objectForKey:@"ride_title"];
    
    NSString *profile_image_string1 = [NSString stringWithFormat:@"%@", [dict objectForKey:@"ride_cover_image"]];
    NSURL*url=[NSURL URLWithString:profile_image_string1];
    [cell.image_view sd_setImageWithURL:url
                              placeholderImage:[UIImage imageNamed:@"scheduledride_placeholder"]
                                       options:SDWebImageRefreshCached];
    
    NSString *ride_start_address = [NSString stringWithFormat:@"%@", [dict objectForKey:@"ride_start_address"]];
    NSString *ride_start_date = [NSString stringWithFormat:@"%@", [dict objectForKey:@"ride_start_date"]];
    NSString *ride_start_time = [NSString stringWithFormat:@"%@", [dict objectForKey:@"ride_start_time"]];
    
    cell.start_Place.text=ride_start_address;
    if ([ride_start_date isEqualToString:@"<null>"] || [ride_start_date isEqualToString:@""] || [ride_start_date isEqualToString:@"null"] || ride_start_date == nil) {
        cell.start_Date.text=@"";
        
    }
    else
    {
        
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"yyyy-MM-dd"];
        NSDate *date1 = [dateFormatter1 dateFromString: ride_start_date];
        dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"MMM dd,yyyy"];// here set format
        NSString *convertedString1 = [dateFormatter1 stringFromDate:date1];
        NSLog(@"Converted String : %@",convertedString1);
        cell.start_Date.text=[NSString stringWithFormat:@"%@",convertedString1];
    }
    
    if ([ride_start_time isEqualToString:@"<null>"] || [ride_start_time isEqualToString:@""] || [ride_start_time isEqualToString:@"null"] || ride_start_time == nil) {
        cell.time_Start.text=@"";
        
    }
    else
    {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setLocale:[NSLocale currentLocale]];
        [formatter setDateStyle:NSDateFormatterNoStyle];
        [formatter setTimeZone:[NSTimeZone localTimeZone]];
        [formatter setTimeStyle:NSDateFormatterLongStyle];
        NSString *dateString = [formatter stringFromDate:[NSDate date]];
        NSRange amRange = [dateString rangeOfString:[formatter AMSymbol]];
        NSRange pmRange = [dateString rangeOfString:[formatter PMSymbol]];
        BOOL is24h = (amRange.location == NSNotFound && pmRange.location == NSNotFound);
       
        if (is24h==YES) {
            NSDateFormatter *dateFormatter4 = [[NSDateFormatter alloc] init] ;
            [dateFormatter4 setDateFormat:@"HH:mm:ss"];
            NSDate *start_time = [dateFormatter4 dateFromString:ride_start_time];
            // [dateFormatter4 setDateFormat:@"HH:mm"];
            NSString *starttime = [dateFormatter4 stringFromDate:start_time];
            cell.time_Start.text=[NSString stringWithFormat:@"%@",starttime];
            
        }
        else if(is24h==NO)
        {
            NSDateFormatter *dateFormatter4 = [[NSDateFormatter alloc] init] ;
            [dateFormatter4 setDateFormat:@"HH:mm:ss"];
            NSDate *start_time = [dateFormatter4 dateFromString:ride_start_time];
            [dateFormatter4 setDateFormat:@"hh:mm a"];
            NSString *starttime = [dateFormatter4 stringFromDate:start_time];
            cell.time_Start.text=[NSString stringWithFormat:@"%@",starttime];
        }
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *created_user_id=[NSString stringWithFormat:@"%@",[[arrayOfScheduleRides objectAtIndex:indexPath.row] valueForKey:@"created_user_id"]];
    NSString *user_id_curent=[NSString stringWithFormat:@"%@",[Defaults valueForKey:User_ID]];
    
    appDelegate.Ride_Name_From_Schedule=[NSString stringWithFormat:@"%@",[[arrayOfScheduleRides objectAtIndex:indexPath.row] valueForKey:@"ride_title"]];
    appDelegate.ride_ID_Api=[NSString stringWithFormat:@"%@",[[arrayOfScheduleRides objectAtIndex:indexPath.row] valueForKey:@"ride_id"]];
    NSLog( @"ride details %@",[arrayOfScheduleRides objectAtIndex:indexPath.row]);
    NSLog(@"Ride_Api_ID : %@",appDelegate.ride_ID_Api);
    appDelegate.ride_status_start=[NSString stringWithFormat:@"%@",[[arrayOfScheduleRides objectAtIndex:indexPath.row] valueForKey:@"ride_status"]];
    NSLog(@"Ride_status : %@",appDelegate.ride_status_start);
    ride_ID=[NSString stringWithFormat:@"%@",[[arrayOfScheduleRides objectAtIndex:indexPath.row] valueForKey:@"ride_id"]];
    NSLog(@"Schedule Ride ID : %@",ride_ID);
    NSString*routeID=[NSString stringWithFormat:@"%@",[[arrayOfScheduleRides objectAtIndex:indexPath.row] valueForKey:@"route_id"]];
    NSString*frndsID=[NSString stringWithFormat:@"%@",[[arrayOfScheduleRides objectAtIndex:indexPath.row] valueForKey:@"friends"]];
    
    if ([frndsID isEqualToString:@"<null>"] || [frndsID isEqualToString:@""] || [frndsID isEqualToString:@"null"] || frndsID == nil) {
        itemsfrdid=[[NSArray alloc]init];
        
    }
    else{
        itemsfrdid = [frndsID componentsSeparatedByString:@","];
        
    }
    NSString*groupsID=[NSString stringWithFormat:@"%@",[[arrayOfScheduleRides objectAtIndex:indexPath.row] valueForKey:@"groups"]];
    if ([groupsID isEqualToString:@"<null>"] || [groupsID isEqualToString:@""] || [groupsID isEqualToString:@"null"] || groupsID == nil) {
        itemsgroupsid=[[NSArray alloc]init];
        
    }
    else
    {
        itemsgroupsid = [groupsID componentsSeparatedByString:@","];
    }
    
    
    [[AppHelperClass appsharedInstance] setRideType:@"setup_ride"];
    if ([appDelegate.ride_status_start isEqualToString:@"started"]&&![created_user_id isEqualToString:user_id_curent]) {
        RideDashboard*dashboard=[self.storyboard instantiateViewControllerWithIdentifier:@"RideDashboardStoryboard"];
        dashboard.is_From=@"NOTIFICATIONS";
        dashboard.ride_id_From_Notifications=appDelegate.ride_ID_Api;
        
        [self.navigationController pushViewController:dashboard animated:YES];
    }
    else if([appDelegate.ride_status_start isEqualToString:@"yet_to_start"]|| [created_user_id isEqualToString:user_id_curent])
    {
        /*
         RoutesVC *cntlr = [self.storyboard instantiateViewControllerWithIdentifier:@"RoutesVC"];
         cntlr.is_FROM_VC_OR_Start_Ride=@"YES";
         
         cntlr.checkboxSelecteString=[NSString stringWithFormat:@"%@",routeID];
         cntlr.arrayOfFrndsInRoutes =[itemsfrdid mutableCopy];
         cntlr.arrayOfgroupsInRoutes =[itemsgroupsid mutableCopy];
         NSLog(@"Scheduled Ride Type %@",[[AppHelperClass appsharedInstance] rideType]);
         [self.navigationController pushViewController:cntlr animated:YES];
         */
        RideDashboard*dashboard=[self.storyboard instantiateViewControllerWithIdentifier:@"RideDashboardStoryboard"];
        dashboard.ride_id_From_Notifications=appDelegate.ride_ID_Api;
        [self.navigationController pushViewController:dashboard animated:YES];
    }
}

-(void)leftBtn:(UIButton *)sender
{
    appDelegate.arrayOfSelectedFrndsAndRiders=[[NSMutableArray alloc]init];
    appDelegate.arrayOfSelectedGroupsAndRiders=[[NSMutableArray alloc]init];
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - schedule_Ride_list API
-(void)ride_Scheduled_Refresh
{
    [self schedule_Ride_list];
}
-(void)schedule_Ride_list{
    
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
    
    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.frame = CGRectMake(0, 0, SCREEN_WIDTH, (SCREEN_HEIGHT/4)+60);
//    activeIndicatore.frame = CGRectMake(self.subView_scheduleRide.frame.origin.x/2, self.subView_scheduleRide.frame.origin.y/2, self.subView_scheduleRide.frame.size.width, self.subView_scheduleRide.frame.size.height);

//    activeIndicatore.center = self.subView_scheduleRide.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    activeIndicatore.hidesWhenStopped = TRUE;
    [activeIndicatore startAnimating];
    [self.subView_scheduleRide addSubview:activeIndicatore];
    
    [SHARED_API scheduleRideRequestsWithParams:[Defaults valueForKey:User_ID] withSuccess:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           NSLog(@"responce is %@",response);
                           
                           
    arrayOfScheduleRides=[[NSMutableArray alloc]init];
    NSArray*rides=[response valueForKey:@"rides"];
        if (rides.count>0) {
            for (int i=0; i<rides.count; i++) {
                 NSDictionary *data_Dict_is=[rides objectAtIndex:i];
                 NSString *ride_status=[NSString stringWithFormat:@"%@",[data_Dict_is valueForKey:@"ride_status"] ];
                NSString *rider_users_id=[NSString stringWithFormat:@"%@",[data_Dict_is valueForKey:@"created_user_id"] ];
                NSString*myUser_id=[NSString stringWithFormat:@"%@",[Defaults valueForKey:User_ID]];
                if (([rider_users_id isEqualToString:myUser_id]&&[ride_status isEqualToString:@"yet_to_start"]) || [ride_status isEqualToString:@"started"]) {
                    [arrayOfScheduleRides addObject:data_Dict_is];
                    }
                }
        }
                           
     
          if (arrayOfScheduleRides.count>0) {
                               
            [self.scheduleRide_TableView reloadData];
              self.noData_label.hidden=YES;
                if ([activeIndicatore isAnimating]) {
                        [activeIndicatore stopAnimating];
                        _scheduleRide_TableView.hidden = NO;
                        [activeIndicatore removeFromSuperview];
                    }
                           }
                           else{
                               [self.scheduleRide_TableView reloadData];
                               self.noData_label.hidden=NO;
                               if ([activeIndicatore isAnimating]) {
                                   [activeIndicatore stopAnimating];
                                   _scheduleRide_TableView.hidden = NO;
                                   [activeIndicatore removeFromSuperview];
                               }
                               // [SHARED_HELPER showAlert:GroupsEmpty];
                           }
                           
                           
                           
                           
                       });
        
    } onfailure:^(NSError *theError) {
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           
                           [SHARED_HELPER showAlert:ServiceFail];
                           _scheduleRide_TableView.hidden = YES;
                           if ([activeIndicatore isAnimating]) {
                               [activeIndicatore stopAnimating];
                               [activeIndicatore removeFromSuperview];
                           }
                           
                           
                           
                       });
    }];
}
#pragma mark - On Click Actions
- (IBAction)onClick_freeRide_btn:(id)sender
{
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [SHARED_HELPER showAlert:NonetworkError];
        return;
    }
    [[AppHelperClass appsharedInstance] setRideType:@"free_ride"];
    RideDashboard *rideDashboard=[self.storyboard instantiateViewControllerWithIdentifier:@"RideDashboardStoryboard"];
    rideDashboard.updateRideStatus=NO;
    [self.navigationController pushViewController:rideDashboard animated:YES];

}
- (IBAction)onClick_Scdule_Ride_btn:(id)sender
{
    //Clear choosed Data
    [self remove_Selected_Data];
    
    CreaterideViewController *createride = [self.storyboard instantiateViewControllerWithIdentifier:@"CreaterideViewController"];
    //    [[AppHelperClass appsharedInstance] setRideType:@"Set_Up_Ride"];
    [[AppHelperClass appsharedInstance] setRideType:@"setup_ride"];
    createride.is_From=@"Start_Ride";
    [self.navigationController pushViewController:createride animated:YES];
    
}
- (IBAction)onClick_Choose_Route_btn:(id)sender
{
    RoutesVC *cntlr = [self.storyboard instantiateViewControllerWithIdentifier:@"RoutesVC"];
    cntlr.is_FROM_VC_OR_Start_Ride=@"YES";
    cntlr.is_Free_Ride=@"YES";
    [self.navigationController pushViewController:cntlr animated:YES];
}
- (IBAction)onClick_Choose_Friend_btn:(id)sender
{
    SelectFriendsAndGroups*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"SelectFriendsAndGroups"];
    
    appDelegate.isFrom_RidersVC_Or_FreeRideRoutes=@"FreerideRoutesToFrndsAndGroups";
    
    controller.change_status_routeID=YES;
    controller.free_route_Frnds_groups_id_string=@"YES";
    controller.is_For_Only=@"Friends";
    [self.navigationController pushViewController:controller animated:YES];
}
- (IBAction)onClick_Choose_Group_btn:(id)sender
{
    SelectFriendsAndGroups*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"SelectFriendsAndGroups"];
    
    appDelegate.isFrom_RidersVC_Or_FreeRideRoutes=@"FreerideRoutesToFrndsAndGroups";
    
    controller.change_status_routeID=YES;
    controller.free_route_Frnds_groups_id_string=@"YES";
    controller.is_For_Only=@"Groups";
    [self.navigationController pushViewController:controller animated:YES];
}
#pragma mark - Choosing For Ride
-(void)Route_Select_Deselect
{//
    if ([appDelegate.select_Route_Id_For_CreateAndFreeRide isEqualToString:@""]) {
        self.choose_Route_View.backgroundColor=[UIColor clearColor];
        self.choose_Route_Label.text=@"CHOOSE A ROUTE";
        self.choose_Route_View.layer.borderWidth=1.0;
        self.choose_Route_View.layer.borderColor=[[UIColor whiteColor] CGColor];
        self.choose_Route_Label.textColor=[UIColor whiteColor];
        
       
        self.choose_Route_ImgView.image=[UIImage imageNamed:@"unselect_route"];
        
    }
    else
    {
        self.choose_Route_View.backgroundColor=[UIColor colorWithRed:2/255.0 green:222/255.0 blue:255/255.0 alpha:1.0];
        self.choose_Route_Label.text=[NSString stringWithFormat:@"%@ route",appDelegate.select_Route_Address_For_CreateAndFreeRide];
        self.choose_Route_Label.textColor=[UIColor blackColor];
        self.choose_Route_View.layer.borderWidth=0.0;
        
       self.choose_Route_ImgView.image=[UIImage imageNamed:@"selected_route"];
        
    }
}
-(void)Friend_Select_Deselect
{
    if (appDelegate.arrayOfSelectedFrndsAndRiders.count==0) {
        self.choose_Friends_View.backgroundColor=[UIColor clearColor];
        self.choose_Friend_Label.text=@"CHOOSE FRIENDS";
        self.choose_Friends_View.layer.borderWidth=1.0;
        self.choose_Friends_View.layer.borderColor=[[UIColor whiteColor] CGColor];
        self.Selected_Friends_Count_Label.text=@"";
        self.choose_Friend_Label.textColor=[UIColor whiteColor];
       
       
        self.choose_Friend_ImgView.image=[UIImage imageNamed:@"unselect_friend"];
        
        
        
    }
    else
    {
        self.choose_Friends_View.backgroundColor=[UIColor colorWithRed:2/255.0 green:222/255.0 blue:255/255.0 alpha:1.0];
         if (appDelegate.arrayOfSelectedFrndsAndRiders.count==1)
         {
             self.Selected_Friends_Count_Label.text=[NSString stringWithFormat:@"%lu",(unsigned long)appDelegate.arrayOfSelectedFrndsAndRiders.count];
             self.choose_Friend_Label.text=@"Friend Selected";
         }
        else
        {
            self.Selected_Friends_Count_Label.text=[NSString stringWithFormat:@"%lu",(unsigned long)appDelegate.arrayOfSelectedFrndsAndRiders.count];
            self.choose_Friend_Label.text=@"Friends Selected";
        }
       
       
        
        self.choose_Friend_ImgView.image=[UIImage imageNamed:@"selected_friend"];
       
        
        
        self.choose_Friend_Label.textColor=[UIColor blackColor];
        self.choose_Friends_View.layer.borderWidth=0.0;
    }
    
    
}
-(void)Group_Select_Deselect
{
    if (appDelegate.arrayOfSelectedGroupsAndRiders.count==0) {
        self.choose_Group_View.backgroundColor=[UIColor clearColor];
        self.choose_Group_Label.text=@"CHOOSE GROUPS";
        self.choose_Group_View.layer.borderWidth=1.0;
        self.choose_Group_View.layer.borderColor=[[UIColor whiteColor] CGColor];
        self.Selected_Groups_Count_Label.text=@"";
        self.choose_Group_Label.textColor=[UIColor whiteColor];
        
        
        self.choose_Group_ImgView.image=[UIImage imageNamed:@"unselect_group"];
    }
    else
    {
        self.choose_Group_View.backgroundColor=[UIColor colorWithRed:2/255.0 green:222/255.0 blue:255/255.0 alpha:1.0];
        if (appDelegate.arrayOfSelectedGroupsAndRiders.count==1)
        {
            self.Selected_Groups_Count_Label.text=[NSString stringWithFormat:@"%lu",(unsigned long)appDelegate.arrayOfSelectedGroupsAndRiders.count];
            self.choose_Group_Label.text=@"Group Selected";
        }
        else
        {
            self.Selected_Groups_Count_Label.text=[NSString stringWithFormat:@"%lu",(unsigned long)appDelegate.arrayOfSelectedGroupsAndRiders.count];
            self.choose_Group_Label.text=@"Groups Selected";
        }
       
       
        self.choose_Group_ImgView.image=[UIImage imageNamed:@"selected_group"];
        
        self.choose_Group_Label.textColor=[UIColor blackColor];
        self.choose_Group_View.layer.borderWidth=0.0;
    }
}
-(void)remove_Selected_Data
{
    appDelegate.select_Route_Id_For_CreateAndFreeRide=@"";
    appDelegate.select_Route_Address_For_CreateAndFreeRide=@"";
    appDelegate.arrayOfSelectedFrndsAndRiders=[[NSMutableArray alloc]init];
    appDelegate.arrayOfSelectedGroupsAndRiders=[[NSMutableArray alloc]init];
    
    self.choose_Friends_View.layer.borderWidth=1.0;
    self.choose_Group_View.layer.borderWidth=1.0;
    self.choose_Route_View.layer.borderWidth=1.0;
    self.choose_Friends_View.layer.borderColor=[[UIColor whiteColor] CGColor];
    self.choose_Group_View.layer.borderColor=[[UIColor whiteColor] CGColor];
    self.choose_Route_View.layer.borderColor=[[UIColor whiteColor] CGColor];
    
    self.choose_Route_Label.text=@"CHOOSE A ROUTE";
    self.choose_Friend_Label.text=@"CHOOSE FRIENDS";
    self.Selected_Friends_Count_Label.text=@"";
    self.choose_Group_Label.text=@"CHOOSE GROUPS";
    self.Selected_Groups_Count_Label.text=@"";
    
    self.choose_Route_Label.textColor=[UIColor whiteColor];
    
    self.choose_Friend_Label.textColor=[UIColor whiteColor];
   
    self.choose_Group_Label.textColor=[UIColor whiteColor];
    
  
    
    
    
    self.choose_Friends_View.backgroundColor=[UIColor clearColor];
    self.choose_Group_View.backgroundColor=[UIColor clearColor];
    self.choose_Route_View.backgroundColor=[UIColor clearColor];
    
    
    self.choose_Route_ImgView.image=[UIImage imageNamed:@"unselect_route"];
    self.choose_Friend_ImgView.image=[UIImage imageNamed:@"unselect_friend"];
    self.choose_Group_ImgView.image=[UIImage imageNamed:@"unselect_group"];
    
    
    
}



@end
