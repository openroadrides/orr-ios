//
//  DeleteMemberCell.h
//  openroadrides
//
//  Created by apple on 18/12/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeleteMemberCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *deleteMemberBGView;
@property (weak, nonatomic) IBOutlet UIImageView *deleteMemberImageView;
@property (weak, nonatomic) IBOutlet UILabel *deleteMemberNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *deleteMemberuserLabel;
@property (weak, nonatomic) IBOutlet UIButton *deleteMemberBtn;

@end
