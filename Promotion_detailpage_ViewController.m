//
//  Promotion_detailpage_ViewController.m
//  openroadrides
//
//  Created by SrkIosEbiz on 27/09/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "Promotion_detailpage_ViewController.h"

@interface Promotion_detailpage_ViewController ()

@end

@implementation Promotion_detailpage_ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"PROMOTION";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    
    
    [self.navigationController.navigationBar setTranslucent:NO];
    
    self.promotion_detailPage_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride. Ride is ongoing. Touch to open the Ride.";
    [self.promotion_detailPage_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.promotion_detailPage_scrollLabel.textColor = [UIColor blackColor];
    self.promotion_detailPage_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.promotion_detailPage_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.promotion_detailPage_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.promotion_detailPage_scrollLabel.scrollSpeed = 60; // pixels per second
    self.promotion_detailPage_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.promotion_detailPage_scrollLabel.fadeLength = 0.f;
    self.promotion_detailPage_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.promotion_detailPage_scrollLabel observeApplicationNotifications];
    if (_promotion_detailPage_onGoing) {
        
        self.promotion_detailPage_scrollLabel.hidden=YES;
        

        self.promotion_detailPage_rideGoingBtn.hidden=YES;
    }
    else
    {
        
        if (appDelegate.ridedashboard_home) {
            self.promotion_detailPage_scrollLabel.hidden=NO;
            self.promotion_detailPage_rideGoingBtn.hidden=NO;
        }
        else
        {
            self.promotion_detailPage_scrollLabel.hidden=YES;
            self.promotion_detailPage_rideGoingBtn.hidden=YES;
        }
      
    }
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [leftBtn addTarget:self action:@selector(back_Action) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.frame = CGRectMake(0, 0, 25, 25);
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"back_Image"] forState:UIControlStateNormal];
    UIBarButtonItem *leftBackButton=[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:leftBackButton, nil];
    
    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.frame=CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
    activeIndicatore.color = APP_YELLOW_COLOR;
    activeIndicatore.hidesWhenStopped = TRUE;
    [activeIndicatore startAnimating];
    [self.view addSubview:activeIndicatore];
    
    
    [self get_promotions_Details];
}
-(void)back_Action{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)get_promotions_Details
{
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
    
    [SHARED_API get_promotion_detail_page:_promotion_id withSuccess:^(NSDictionary *response) {
        if ([[response valueForKey:STATUS] isEqualToString:SUCCESS]) {
            [activeIndicatore stopAnimating];
            NSLog(@"%@", response);
            NSArray *data_array=[response valueForKey:@"promotion_details"];
            if (data_array.count>0) {
                NSDictionary *data=[data_array objectAtIndex:0];
                
                NSDateFormatter *  YMD_formatter = [[NSDateFormatter alloc] init];
                YMD_formatter.dateFormat = @"yyyy-MM-dd";
                NSDateFormatter *  MDY_formatter = [[NSDateFormatter alloc] init];
                MDY_formatter.dateFormat = @"dd";
                [MDY_formatter setTimeZone:[NSTimeZone localTimeZone]];
                
                
                
                NSString *img_str=[data valueForKey:@"business_image"];
                NSString *title=[data valueForKey:@"promotion_name"];
                NSString *business_name=[data valueForKey:@"business_name"];
                business_name=[NSString stringWithFormat:@"By %@",business_name];
                
                
                
                NSString *end_date=[data valueForKey:@"schedule_enddate"];
                NSDate *TZ_date = [YMD_formatter dateFromString:end_date];
                end_date = [MDY_formatter stringFromDate:TZ_date];
                if ([end_date isEqualToString:@"01"]||[end_date isEqualToString:@"21"]||[end_date isEqualToString:@"31"])
                {
                    MDY_formatter.dateFormat = @"d'st' MMM,yyyy";
                }
                else if ([end_date isEqualToString:@"02"]||[end_date isEqualToString:@"22"])
                {
                    MDY_formatter.dateFormat = @"d'nd' MMM,yyyy";
                }
                else if ([end_date isEqualToString:@"03"]||[end_date isEqualToString:@"23"])
                {
                    MDY_formatter.dateFormat = @"d'rd' MMM,yyyy";
                }
                else
                {
                    MDY_formatter.dateFormat = @"d'th' MMM,yyyy";
                }
                end_date = [MDY_formatter stringFromDate:TZ_date];
                
                
                NSString *date=[NSString stringWithFormat:@"End Date: %@",end_date];
                NSString *des=[data valueForKey:@"description"];
                
                NSString *promotion_content_or_img_or_url=[data valueForKey:@"promotion_content"];
                NSString *promotion_content_type=[data valueForKey:@"promotion_content_type"];
                
                
                
                _promotion_des.text=des;
                _promotion_end_date.text=date;
                _offer_model_name.text=business_name;
                _promotion_title.text=title;
                NSURL*url=[NSURL URLWithString:img_str];
                [_promotion_image sd_setImageWithURL:url
                                    placeholderImage:[UIImage imageNamed:@"add_places_placeholder"]
                                             options:SDWebImageRefreshCached];
                
                
                if ([promotion_content_type isEqualToString:@"Notification"]) {
                    _promotion_Notification_description.hidden=NO;
                    _promotion_Notification_description.text=promotion_content_or_img_or_url;
                    
                }
                if ([promotion_content_type isEqualToString:@"Image"]) {
                    _image_view.hidden=NO;
                    NSURL*url_is=[NSURL URLWithString:promotion_content_or_img_or_url];
                    [_notification_image sd_setImageWithURL:url_is
                                        placeholderImage:[UIImage imageNamed:@"add_places_placeholder"]
                                                 options:SDWebImageRefreshCached];
                    _notification_content_view_height.constant=120;
                }
                if ([promotion_content_type isEqualToString:@"URL"]) {
                    _url_view.hidden=NO;
                     _website_label.text=promotion_content_or_img_or_url;
                   
                    if ([promotion_content_or_img_or_url hasPrefix:@"http://"] || [promotion_content_or_img_or_url hasPrefix:@"https://"])
                    {
                        webURL = [NSURL URLWithString:promotion_content_or_img_or_url];
                    }
                    else
                    {
                        webURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@", promotion_content_or_img_or_url]];
                    }
               
                }
                
            }
            
        }
        else
        {
            [activeIndicatore stopAnimating];
            [SHARED_HELPER showAlert:ServiceFail];
            int duration = 1; // duration in seconds
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [self back_Action];
            });
        }
        
    } onfailure:^(NSError *theError) {
        
        [activeIndicatore stopAnimating];
        [self network_Alert:@""];
    }];
    
}
-(void)network_Alert:(NSString *)call_from{
    UIAlertController *alertview = [UIAlertController alertControllerWithTitle:@"Network Error" message:Nonetwork preferredStyle:UIAlertControllerStyleAlert];
    
    [alertview addAction:[UIAlertAction actionWithTitle:@"retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self get_promotions_Details];
    }]];
    
    [self presentViewController:alertview animated:YES completion:nil];
}


- (IBAction)website_action:(id)sender {
    if (![_website_label.text isEqualToString:@""]) {
           
            [[UIApplication sharedApplication] openURL:webURL];
    }
    
}
- (IBAction)onClick_promotion_detailPage_rideGoingBtn:(id)sender {
    
    self.promotion_detailPage_scrollLabel.hidden=YES;
    self.promotion_detailPage_rideGoingBtn.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }

}
@end
