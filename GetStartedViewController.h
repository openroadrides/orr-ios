//
//  GetStartedViewController.h
//  REbeacon
//
//  Created by MobileTeam Ebiz on 23/06/17.
//  Copyright © 2017 ebiz solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "GetStartedAlert.h"
#import "BIZPopupViewController.h"

@interface GetStartedViewController : UIViewController<UIScrollViewDelegate, UINavigationControllerDelegate>
{
    CGRect screenRect;
    CGFloat screenWidth;
    CGFloat screenHeight;
    
    NSMutableArray *getStartedImages_Array;
    
    NSInteger pageControllerIndex;
    UIView *welcome_View;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIScrollView *getStarted_scrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

@end
