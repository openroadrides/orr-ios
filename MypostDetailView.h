//
//  MypostDetailView.h
//  openroadrides
//
//  Created by apple on 04/10/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InterestedUserCell.h"
#import "APIHelper.h"
#import "AppHelperClass.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "EditPostView.h"
#import "CBAutoScrollLabel.h"
@interface MypostDetailView : UIViewController<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>
{
    UIActivityIndicatorView  *activeIndicatore;
    NSMutableDictionary *mypostDetailDict;
    NSArray *imagearayy,*interestedUserArray;
    NSMutableArray *myPostImagesArray;
    NSString *image_str;
    UIPageControl *pageControl;
}
@property (weak, nonatomic) IBOutlet UIScrollView *postScrollView;
@property (weak, nonatomic) IBOutlet UIView *postContentView;
@property (weak, nonatomic) IBOutlet UIScrollView *multipleImages_scrollView;
@property (weak, nonatomic) IBOutlet UILabel *postUserNamelabel;
@property (weak, nonatomic) IBOutlet UILabel *postPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *postedDateLabel;
@property (weak, nonatomic) IBOutlet UIView *postTitle_view;
@property (weak, nonatomic) IBOutlet UILabel *postTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *addressTopView;
@property (weak, nonatomic) IBOutlet UIImageView *postAddressImageView;
@property (weak, nonatomic) IBOutlet UILabel *postAddressLabel;
@property (weak, nonatomic) IBOutlet UIImageView *postPhoneNumberImageView;
@property (weak, nonatomic) IBOutlet UILabel *postPhoneNumberLabel;
@property (weak, nonatomic) IBOutlet UIImageView *postEmailImageView;
@property (weak, nonatomic) IBOutlet UILabel *postEmailLabel;
@property (weak, nonatomic) IBOutlet UIView *descriptionTopView;
@property (weak, nonatomic) IBOutlet UILabel *postDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIView *interestedUserTopView;
@property (weak, nonatomic) IBOutlet UILabel *interestedUsersLabel;
@property (weak, nonatomic) IBOutlet UITableView *interestedUsersTableview;
@property (weak, nonatomic) IBOutlet UIButton *closePostButton;
- (IBAction)onClick_closePost_btn:(id)sender;
@property (strong, nonatomic) NSString *myPostIDString;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *interestedUserTableView_topConstraint;
@property (weak, nonatomic) IBOutlet UILabel *postCategoryTypeLabel;
@property (weak, nonatomic) IBOutlet UIButton *myPostDetail_rideGoingBtn;
- (IBAction)onClick_myPostDeatil_rideGoingBtn:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint_closePostBtn;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *myPostDetail_scrollLabel;

@end
