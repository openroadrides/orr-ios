//
//  FullFilledRidesCell.h
//  openroadrides
//
//  Created by apple on 06/07/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FullFilledRidesCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *fullFilledRidesCell_bgView;
@property (weak, nonatomic) IBOutlet UILabel *fullFilled_ride_title;
@property (weak, nonatomic) IBOutlet UILabel *fullFilled_ride_address;
@property (weak, nonatomic) IBOutlet UILabel *fullfilled_ride_date;
@property (weak, nonatomic) IBOutlet UILabel *fullfilled_ride_time;
@property (weak, nonatomic) IBOutlet UILabel *fullfilled_ride_description;
@property (weak, nonatomic) IBOutlet UIView *fullFilled_addressView;
@property (weak, nonatomic) IBOutlet UIView *fullFilled_dateAndTimeView;
@property (weak, nonatomic) IBOutlet UIView *fullFilled_descriptionView;
@property (weak, nonatomic) IBOutlet UIImageView *fullFilledDateAndTimeLineImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fullFilledDateAndTimeViewTopConstraint;

@end
