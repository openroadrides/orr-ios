//
//  NewsTableViewCell.h
//  openroadrides
//
//  Created by apple on 29/05/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *News_content_view;
@property (weak, nonatomic) IBOutlet UIImageView *News_image_view;
@property (weak, nonatomic) IBOutlet UILabel *News_title_lbl;
@property (weak, nonatomic) IBOutlet UILabel *News_date_lbl;
@property (weak, nonatomic) IBOutlet UILabel *News_description_lbl;

@end
