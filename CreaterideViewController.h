//
//  CreaterideViewController.h
//  openroadrides
//
//  Created by apple on 22/05/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "AppHelperClass.h"
#import "APIHelper.h"
#import "RoutesVC.h"
#import <GooglePlacePicker/GooglePlacePicker.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "SelectFriendsAndGroups.h"
#import "AppDelegate.h"
#import "CBAutoScrollLabel.h"
@interface CreaterideViewController : UIViewController<GMSPlacePickerViewControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    NSString * random_number_string;
    UIDatePicker *date,*date1,*meeting_date,*start_time;
    NSString *user_token_id;
     UIActivityIndicatorView  *activeIndicatore;
    double latitude,longitude,meeting_latitude,meeting_longitude;
    
    UIImage *cameraImage;
    NSString *location_str,*user_id,*selected_route,*rideStartTimeString,*rideMeetingTimeString;
    NSString *base64String;
    NSData *profileImgData;
    UIBarButtonItem *item0;
}

@property (strong, nonatomic)  NSString *is_From;

@property (weak, nonatomic) IBOutlet UITextField *Ridename_tf;
@property (weak, nonatomic) IBOutlet UITextField *Placetostart_tf;
@property (weak, nonatomic) IBOutlet UITextField *Ridedate_tf;
@property (weak, nonatomic) IBOutlet UITextField *Meetingtime_tf;
@property (weak, nonatomic) IBOutlet UITextField *Description_tf;

- (IBAction)Select_Route_Action:(id)sender;
- (IBAction)pickPlace:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *startingLocation_btn;


- (IBAction)upload_img_btn:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *uploadimg_lbl;
@property (weak, nonatomic) IBOutlet UITextField *upload_img_tf;
@property (weak, nonatomic) IBOutlet UIButton *invite_riders_btn;
- (IBAction)onClick_invite_riders_btn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *rideimage_btn;
- (IBAction)onClick_rideImage_btn:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *meetingAddress_TF;
@property (weak, nonatomic) IBOutlet UITextField *meetingDate_TF;
@property (weak, nonatomic) IBOutlet UITextField *startTime_TF;
@property (weak, nonatomic) IBOutlet UIButton *meetingAddress_btn;
- (IBAction)onClick_meetingAddress_btn:(id)sender;
@property (weak, nonatomic) NSString *location_str_ride,*AddressTextString;
@property (weak, nonatomic) IBOutlet UIImageView *ride_coverImage;
@property (weak, nonatomic) IBOutlet UIButton *createRide_rideGoingBtn;
- (IBAction)onClick_createRide_rideGoinBtn:(id)sender;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *createRide_scrollLabel;
@end
