//
//  ESWebService.h
//  Emmanuel Sanders
//
//  Created by MacBook Pro on 27/01/16.
//  Copyright © 2016 Emmanuel Sanders. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef void (^WebServiceSuccess) (NSData *theData);
typedef void (^WebServiceFailure) (NSError *theError);


@interface WebService : NSObject


// ****************
// Service Calls
// ****************

@property (nonatomic, strong) NSString *baseUrlString;

-(void)getWithPath:(NSString*)path andParams:(NSDictionary*)params
    withCompletion:(WebServiceSuccess)completion
           failure:(WebServiceFailure)failure;

-(void)postWithPath:(NSString*)path andParams:(NSDictionary*)params
     withCompletion:(WebServiceSuccess)completion
            failure:(WebServiceFailure)failure;


@end
