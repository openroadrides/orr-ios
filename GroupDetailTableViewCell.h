//
//  GroupDetailTableViewCell.h
//  openroadrides
//
//  Created by apple on 01/06/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupDetailTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *content_view;
@property (weak, nonatomic) IBOutlet UILabel *ridername_lbl;
@property (weak, nonatomic) IBOutlet UILabel *location_lbl;
@property (weak, nonatomic) IBOutlet UIImageView *groupdetail_img;
@property (weak, nonatomic) IBOutlet UIView *memberAddressView;
@property (weak, nonatomic) IBOutlet UILabel *memberAddressLabel;
@property (weak, nonatomic) IBOutlet UIView *memberStatusView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *memberStatusViewTopConstraint;

@end
