//
//  DashboardViewController.h
//  openroadrides
//
//  Created by apple on 08/05/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "FriendsViewController.h"
#import "StartARide.h"
#import "RidesController.h"
#import "ProfileView.h"
#import "LoginViewController.h"
#import "JSBadgeView.h"
#import "PrivacyPolicy.h"
#import "TermsAndConditions.h"
#import "BrowseRidersViewController.h"
#import "BIZPopupViewController.h"
#import "Promotions.h"
#import "Feedback.h"
#import "HomeDashboardView.h"
#import "CBAutoScrollLabel.h"
@interface DashboardViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate>
{
    CLLocationManager *appDasboard_locationManager;
    double user_current_latitude,user_current_longtitude;
      UIActivityIndicatorView *activeIndicatore;
    UIView *indicaterview;
    NSString*friend_count,*group_count;
    NSString*notification_count;
    JSBadgeView *badgeView;
    
    UIBarButtonItem *menuButton;
}


@property (strong, nonatomic) UIButton*notification_btn;
- (IBAction)Promotions_Action:(id)sender;

- (IBAction)Ride_action:(id)sender;
- (IBAction)Route_action:(id)sender;
- (IBAction)Event_action:(id)sender;
- (IBAction)Friends_action:(id)sender;
- (IBAction)Groups_action:(id)sender;
- (IBAction)Setuparide_action:(id)sender;
- (IBAction)Startaride_action:(id)sender;
- (IBAction)MyRides_action:(id)sender;


@property (strong, nonatomic) IBOutlet UIImageView *User_image;
@property (strong, nonatomic) IBOutlet UILabel *User_Name;
@property (strong, nonatomic) IBOutlet UITableView *side_Menu_Table;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *menu_Leading_Constraint;
- (IBAction)Menu_Action:(id)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *main_Scriole_View;
- (IBAction)Profile_Action:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *side_menu_view;

@property (strong, nonatomic) IBOutlet UIView *start_Ride_View;
@property (strong, nonatomic) IBOutlet UIButton *my_Rides_Btn;
@property (strong, nonatomic) IBOutlet UIButton *Sch_Rides_Btn;


@property (weak, nonatomic) IBOutlet UILabel *startRide_Label;
@property (weak, nonatomic) IBOutlet UILabel *events_count;
@property (weak, nonatomic) IBOutlet UILabel *fav_routes;
@property (weak, nonatomic) IBOutlet UILabel *groups_count;
@property (weak, nonatomic) IBOutlet UILabel *friends_count;
@property (weak, nonatomic) IBOutlet UILabel *fav_rides;
@property (weak, nonatomic) IBOutlet UILabel *routes_count;
@property (weak, nonatomic) IBOutlet UILabel *rides_count;
@property (weak, nonatomic) IBOutlet UIButton *dashboard_rideGoingBtn;
- (IBAction)onClick_dashboard_rideGoingBtn:(id)sender;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *dashboard_scrollLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint_dashboardScrollView;
@end
