//
//  CreateGroupViewController.h
//  openroadrides
//
//  Created by apple on 05/06/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RideInviteFriends.h"
#import "CBAutoScrollLabel.h"

@interface CreateGroupViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    UIImage *cameraImage;
    NSData *profileImgData;
    NSString *base64String;
    NSUserDefaults *defaults;
    NSString *user_token_id;
    UIBarButtonItem *item0;
}
@property (weak, nonatomic) IBOutlet UIImageView *logo_img;
- (IBAction)logo_action:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *groupname_tf;
@property (weak, nonatomic) IBOutlet UITextField *description_tf;
@property (weak, nonatomic) IBOutlet UIScrollView *scrl_vw;
@property (weak, nonatomic) IBOutlet UIView *content_view;
@property (weak, nonatomic) IBOutlet UIButton *createFrnds_inviteFrnds_btn;
- (IBAction)onClick_createGroup_inviteFrnds_btn:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *select_members_name_label;
@property (weak, nonatomic) IBOutlet UIButton *createGroup_rideGoingBtn;
- (IBAction)onClick_createGroup_rideGoingBtn:(id)sender;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *createGroup_scrollLabel;

@end
