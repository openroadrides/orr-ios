//
//  GroupDetailViewController.m
//  openroadrides
//
//  Created by apple on 01/06/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "GroupDetailViewController.h"
#import "GroupDetailTableViewCell.h"
#import "Constants.h"
#import "APIHelper.h"
#import "AppHelperClass.h"
#import "ProfileView.h"
@interface GroupDetailViewController (){
    
    NSDictionary *groupdetails_dict;
    NSArray *groupmembers_ary;
    CGFloat view_height;
    double height;
}

@end

@implementation GroupDetailViewController

- (void)viewDidLoad {
    
    appDelegate.edit_Group=@"NO";
   
    [super viewDidLoad];
    self.title = @"GROUPS";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    _main_content_view.hidden = YES;
    view_height=self.view.frame.size.height;
    
//    self.leaveGroup_btn.layer.borderWidth=1;
//    self.leaveGroup_btn.layer.borderColor=[[UIColor redColor]CGColor];
    self.leaveGroup_View.layer.borderWidth = 1.0;
    self.leaveGroup_View.layer.borderColor = [UIColor colorWithRed:255.0/255.0 green:2.0/255.0 blue:7.0/255.0 alpha:1.0].CGColor;
    self.leaveGroup_View.clipsToBounds=YES;
    self.leaveGroup_View.backgroundColor=[UIColor clearColor];

 
#pragma mark - Bar buttons on nav bar.....
    
    
    
//    UIBarButtonItem *search_btn=[[UIBarButtonItem alloc]initWithImage:
//                              [[UIImage imageNamed:@"SEARCH"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
//                                                             style:UIBarButtonItemStylePlain target:self action:@selector(searchclicked)];
//    self.navigationItem.rightBarButtonItem=search_btn;
    
    UIBarButtonItem *back=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:back_Image] style:UIBarButtonItemStylePlain target:self action:@selector(backclicked)];
    self.navigationItem.leftBarButtonItem = back;
//     self.leaveGroup_btn.hidden=YES;
    
    
    self.groupDetail_scrollLabel.hidden=YES;
    self.groupDetail_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.groupDetail_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.groupDetail_scrollLabel.textColor = [UIColor blackColor];
    self.groupDetail_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.groupDetail_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.groupDetail_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.groupDetail_scrollLabel.scrollSpeed = 60; // pixels per second
    self.groupDetail_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.groupDetail_scrollLabel.fadeLength = 0.f;
    self.groupDetail_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.groupDetail_scrollLabel observeApplicationNotifications];

    self.groupDetails_rideGoingBtn.hidden=YES;
    self.bottomConstraint_leaveGroupBtnVIew.constant=-30;
    if (appDelegate.ridedashboard_home) {
        self.groupDetail_scrollLabel.hidden=NO;
        self.groupDetails_rideGoingBtn.hidden=NO;
        self.bottomConstraint_leaveGroupBtnVIew.constant=0;
    }

    NSString*current_userID=[NSString stringWithFormat:@"%@",[Defaults valueForKey:User_ID]];
    if ([_group_Owner_Id isEqualToString:[NSString stringWithFormat:@"%@",current_userID]]) {
         self.leaveGroup_memeber_btn.hidden=YES;
        self.leaveGroup_View.hidden=YES;
    }
    else{
        self.leaveGroup_memeber_btn.hidden=NO;
        self.leaveGroup_View.hidden=NO;
    }
    if ([_is_from isEqualToString:@"Notification_groupDetails"]) {
        NSLog(@"From notification group");
        
    }
    
    self.addMembers_imageView.hidden=YES;
    self.AddMembers_btn.hidden=YES;
    
    
      [self get_groupsdetail_list];
    
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    if ([appDelegate.un_frind_in_Profile isEqualToString:@"YES"]) {
        appDelegate.un_frind_in_Profile=@"";
        [self get_groupsdetail_list];
    }
    else  if ([appDelegate.edit_Group isEqualToString:@"YES"]) {
        [self get_groupsdetail_list];
        appDelegate.edit_Group=@"NO";
        appDelegate.edit_List_Groups=@"YES";
    }
    else  if ([appDelegate.edit_Group isEqualToString:@"INVITE"]) {
        [self edit_Group_dict];
        appDelegate.edit_Group=@"NO";
        appDelegate.edit_List_Groups=@"YES";
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


#pragma mark - Tableview methods......
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return groupmembers_ary.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *simpleTableIdentifier = @"GroupsdetailCell";
    GroupDetailTableViewCell *cell = (GroupDetailTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
     self.Groupdetail_table_view.separatorStyle=UITableViewCellSeparatorStyleNone;
   
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"GroupDetailTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    if (indexPath.row%2==0) {
        
        cell.content_view.backgroundColor = [UIColor colorWithRed:41/255.0 green:41/255.0 blue:41/255.0 alpha:1.0];
    }
    else{
        
        cell.content_view.backgroundColor = [UIColor colorWithRed:26/255.0 green:26/255.0 blue:26/255.0 alpha:1.0];
        
        
        
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSString *str1 = [NSString stringWithFormat:@"%@", [[groupmembers_ary objectAtIndex:indexPath.row] objectForKey:@"name"]];
    
    if ([str1 isEqualToString:@"<null>"] || [str1 isEqualToString:@""] || [str1 isEqualToString:@"null"] || str1 == nil) {
        
        cell.ridername_lbl.text = [[groupmembers_ary objectAtIndex:indexPath.row] valueForKey:@""];
        
    }
    else
    {
        cell.ridername_lbl.text = [[groupmembers_ary objectAtIndex:indexPath.row] valueForKey:@"name"];
    }
    
    NSString *str2 = [NSString stringWithFormat:@"%@", [[groupmembers_ary objectAtIndex:indexPath.row] objectForKey:@"address"]];
    
    if ([str2 isEqualToString:@"<null>"] || [str2 isEqualToString:@""] || [str2 isEqualToString:@"null"] || str2 == nil) {
        
        cell.location_lbl.text = [[groupmembers_ary objectAtIndex:indexPath.row] valueForKey:@""];
        
    }
    else
    {
        cell.location_lbl.text = [[groupmembers_ary objectAtIndex:indexPath.row] valueForKey:@"address"];
    }
    
    
    
    
    
    NSString * areaName=[NSString stringWithFormat:@"%@",[[groupmembers_ary objectAtIndex:indexPath.row] objectForKey:@"address"]];
    NSString * cityName=[NSString stringWithFormat:@"%@",[[groupmembers_ary objectAtIndex:indexPath.row] objectForKey:@"city"]];
    NSString * stateName=[NSString stringWithFormat:@"%@",[[groupmembers_ary objectAtIndex:indexPath.row] objectForKey:@"state"]];
    NSString *zipCode=[NSString stringWithFormat:@"%@",[[groupmembers_ary objectAtIndex:indexPath.row] objectForKey:@"zipcode"]];
    
    NSMutableArray *empaty_array_search_public=[[NSMutableArray alloc]init];
    [empaty_array_search_public addObject:@"<null>"];
    [empaty_array_search_public addObject:@"null"];
    [empaty_array_search_public addObject:@""];
    [empaty_array_search_public addObject:@"(null)"];
    [empaty_array_search_public addObject:@"0"];
    
    NSMutableArray *Data_array_search_public=[[NSMutableArray alloc]init];
    
    
    if ([empaty_array_search_public containsObject:areaName]) {
        
    }
    else{
        
        [Data_array_search_public addObject:areaName];
    }
    if ([empaty_array_search_public containsObject:cityName]) {
        
    }
    else{
        [Data_array_search_public addObject:cityName];
    }
    
    if ([empaty_array_search_public containsObject:stateName]) {
        
    }
    else{
        [Data_array_search_public addObject:stateName];
    }
    
    if ([empaty_array_search_public containsObject:zipCode]) {
        
    }
    else{
        [Data_array_search_public addObject:zipCode];
    }
    
    NSMutableString *add_data_search_public=[[NSMutableString alloc]init];
    if (Data_array_search_public.count>0) {
//        cell.location_imageview.image=[UIImage imageNamed:@"check_in_placeholder"];
                for (int i=0; i<Data_array_search_public.count; i++) {
                        NSString *adress=[Data_array_search_public objectAtIndex:i];
            if (i==0) {
                [add_data_search_public appendFormat:@"%@", [NSString stringWithFormat:@"%@",adress]];
            }
            else
            {
                int zip=[[Data_array_search_public objectAtIndex:i] intValue];
                if (zip>0) {
                    [add_data_search_public appendFormat:@"%@", [NSString stringWithFormat:@" %@",adress]];
                }
                else
                    [add_data_search_public appendFormat:@"%@", [NSString stringWithFormat:@", %@",adress]];
            }
            
        }
        cell.memberAddressLabel.text=add_data_search_public;
         cell.memberAddressView.hidden=NO;
        cell.memberStatusViewTopConstraint.constant=4;
    }
    else{
        
        cell.memberAddressLabel.text=@"";
        cell.memberAddressView.hidden=YES;
        cell.memberStatusViewTopConstraint.constant=-44;
    }

    
    
    NSString *str = [NSString stringWithFormat:@"%@", [[groupmembers_ary objectAtIndex:indexPath.row] objectForKey:@"profile_image"]];
    NSURL*url=[NSURL URLWithString:str];
    [cell.groupdetail_img sd_setImageWithURL:url
                       placeholderImage:[UIImage imageNamed:@"groups-icon"]
                                options:SDWebImageRefreshCached];
    
    
//    if ([str isEqualToString:@"<null>"] || [str isEqualToString:@""] || [str isEqualToString:@"null"] || str == nil) {
//
//        [self photoForCell:cell user:[[groupmembers_ary objectAtIndex:indexPath.row] valueForKey:@""] withIndexPath:indexPath];
//    }
//    
//    else{
//        
//        [self photoForCell:cell user:[[groupmembers_ary objectAtIndex:indexPath.row] valueForKey:@"profile_image"] withIndexPath:indexPath];
//        
//    }
    NSString *user_status=[[groupmembers_ary objectAtIndex:indexPath.row] valueForKey:@"user_type"];
    if ([user_status isEqualToString:@"<null>"] || [user_status isEqualToString:@""] || [user_status isEqualToString:@"null"] || user_status == nil) {
        
        cell.location_lbl.text =@"";
        
    }
    else{
        if ([user_status isEqualToString:@"member"]) {
            
            
            NSString*accept_reject_status_string=[NSString stringWithFormat:@"%@",[[groupmembers_ary objectAtIndex:indexPath.row] valueForKey:@"accept_or_reject_status"]];
            if ([accept_reject_status_string isEqualToString:@"accept"]) {
                 cell.location_lbl.text =@"Member";
            }
            else  if ([accept_reject_status_string isEqualToString:@""])
            {
                 cell.location_lbl.text =@"Pending";
            }
           
        }
        else if ([user_status isEqualToString:@"owner"])
        {
            cell.location_lbl.text =@"Owner";
        }
//        else if ([user_status isEqualToString:@"pending"])
//        {
//            cell.location_lbl.text =@"Owner";
//        }
    }
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    ProfileView*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileView"];
//    NSString *friend_id=[[groupmembers_ary objectAtIndex:indexPath.row] objectForKey:@"user_id"];
//    controller.id_user=friend_id;
//    controller.friend_status=[[groupmembers_ary objectAtIndex:indexPath.row] objectForKey:@"status"];
//    controller.is_From=@"Groups";
//    [self.navigationController pushViewController:controller animated:YES];
}
-(void)get_groupsdetail_list{
    
    
    
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
    
    
    
    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    activeIndicatore.hidesWhenStopped = TRUE;
    [activeIndicatore startAnimating];
    [self.view addSubview:activeIndicatore];
    
    appDelegate.createGroup_frndInvitations=[[NSMutableArray alloc]init];
    group_members_array=[[NSMutableArray alloc]init];
    
     [SHARED_API groupsdetailRequestsWithParams:_group_id user_ID:[Defaults valueForKey:User_ID] withSuccess:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^
            {
                           
               
                
                NSLog(@"responce is %@",response);
                
                groupdetails_dict = [response valueForKey:@"groupDetails"];
                NSString *group_Owwner_id=[NSString stringWithFormat:@"%@",[groupdetails_dict valueForKey:@"group_owner_id"]];
                NSString *user_id=[NSString stringWithFormat:@"%@",[Defaults valueForKey:User_ID]];
               
                if ([group_Owwner_id isEqualToString:user_id]) {
                    
                    //Edit Button Display
                    UIButton *edit_btn = [UIButton buttonWithType:UIButtonTypeCustom];
                    [edit_btn addTarget:self action:@selector(Edit_Clicked) forControlEvents:UIControlEventTouchUpInside];
                    edit_btn.frame = CGRectMake(0, 0, 30, 30);
                    [edit_btn setBackgroundImage:[UIImage imageNamed:@"setup-ride"] forState:UIControlStateNormal];
                    UIBarButtonItem * edit_Group= [[UIBarButtonItem alloc] initWithCustomView:edit_btn];
                    self.navigationItem.rightBarButtonItems=[NSArray arrayWithObjects:edit_Group, nil];
                    
                    
                    self.addMembers_imageView.hidden=NO;
                    self.AddMembers_btn.hidden=NO;

                }
               
                
                
                _image_str = [NSString stringWithFormat:@"%@",[groupdetails_dict valueForKey:@"group_image"]];
                if ([_image_str isEqualToString:@""]||[_image_str isEqualToString:@"null"] || _image_str == nil||[_image_str isEqualToString:@"<null>"]) {
//                    self.Groupdetail_img.image=[UIImage imageNamed:@"groups-icon"];
                    self.Groupdetail_img.image=[UIImage imageNamed:@"groupdetailPlaceholder"];
                    
                    _image_str=@"";
                    
                }
                else
                {
                    NSURL*url=[NSURL URLWithString:_image_str];
                    [_Groupdetail_img sd_setImageWithURL:url
                                        placeholderImage:[UIImage imageNamed:@"groupdetailPlaceholder"]
                                                 options:SDWebImageRefreshCached];
                    [self.Groupdetail_img setBackgroundColor:[UIColor clearColor]];

                }
                
               //                add_places_placeholder
                
                
//                if ([image_str isEqualToString:@"<null>"] || [image_str isEqualToString:@""] || [image_str isEqualToString:@"null"] || image_str == nil)
//                {
//                    [self configureCoverPicForUser:[groupdetails_dict valueForKey:@"group_image"]];
//                    
//                }
//                
//                else
//                {
//                    
//                    [self configureCoverPicForUser:[groupdetails_dict valueForKey:@"group_image"]];
//                }
                
                self.Groupdetail_img.contentMode=UIViewContentModeScaleAspectFit;
                self.Groupdetail_table_view.hidden=YES;
                groupmembers_ary = [groupdetails_dict valueForKey:@"group_members"];
                [self.Groupdetail_table_view reloadData];
                               
                if (groupmembers_ary.count>0) {// for group invite
                     self.Groupdetail_table_view.hidden=NO;
                    for (NSDictionary *member_dict in groupmembers_ary) {
                        NSString *member_id=[NSString stringWithFormat:@"%@",[member_dict valueForKey:@"user_id"]];
                        NSString *member_name=[NSString stringWithFormat:@"%@",[member_dict valueForKey:@"name"]];
                        NSString *id_with_Name=[NSString stringWithFormat:@"%@,%@",member_id,member_name];
                        [appDelegate.createGroup_frndInvitations addObject:id_with_Name];
                        [group_members_array addObject:id_with_Name];
                        self.groupDetaitsMembersViewHeightConstraint.constant=group_members_array.count*100;
                      
                    }
                }
                
                              
                
                _title_lbl.text = [groupdetails_dict valueForKey:@"group_name"];
               _description_labl.text = [groupdetails_dict valueForKey:@"group_description"];
                
                NSString *groupname_strng = [_title_lbl.text uppercaseString];
                if ([groupname_strng isEqualToString:@""]||[groupname_strng isEqualToString:@"null"] || groupname_strng == nil||[groupname_strng isEqualToString:@"<null>"]) {
                    
                    self.title = @"GROUP";
                }
                else
                {
//                    self.title = groupname_strng;
                    self.title = @"GROUP";
                }
                CGFloat ht = _main_content_view.frame.size.height;
                if (ht>view_height) {
                    height = [self getLabelHeight:_description_labl];
                    
                }
                _heightConstraint_mainContentView.constant = view_height+height;
                if ([activeIndicatore isAnimating])
                {
                    [activeIndicatore stopAnimating];
                    _main_content_view.hidden = NO;
                    [activeIndicatore removeFromSuperview];
                }
                
             
                
                
                       });
        
    } onfailure:^(NSError *theError) {
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           
                           [SHARED_HELPER showAlert:ServiceFail];
                           
                           if ([activeIndicatore isAnimating]) {
                               [activeIndicatore stopAnimating];
                               [activeIndicatore removeFromSuperview];
                           }
                           
                           
                           
                       });
    }];
}
//-(void)searchclicked{
//    
//    
//}
-(void)Edit_Clicked{
    
    EditGroupViewController *cntlr = [self.storyboard instantiateViewControllerWithIdentifier:@"EditGroupViewController"];
    cntlr.Group_Details=groupdetails_dict;
    [self.navigationController pushViewController:cntlr animated:YES];
}

-(void)edit_Group_dict
{
    
    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    activeIndicatore.hidesWhenStopped = TRUE;
    [activeIndicatore startAnimating];
    [self.view addSubview:activeIndicatore];
    NSString *type=@"0";
    
    NSMutableArray *new_Members_Arry=[[NSMutableArray alloc]init];
    NSMutableArray *delet_Members_Arry=[[NSMutableArray alloc]init];
    
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:[_title_lbl.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"group_name"];
    [dict setObject:[_description_labl.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"group_description"];
    
    NSMutableDictionary *image_dict=[[NSMutableDictionary alloc]init];
    
    [image_dict setObject:type forKey:@"type"];
    [image_dict setObject:_image_str forKey:@"image"];
    
    [dict setObject:image_dict forKey:@"group_image"];
    
    for (NSString *member_String in appDelegate.createGroup_frndInvitations) {
        
       
        if ([group_members_array containsObject:member_String]);
        else
        {
            NSArray *member_array=[member_String componentsSeparatedByString:@","];
            [new_Members_Arry addObject:[member_array objectAtIndex:0]];
        }
    }
    
    [dict setObject:new_Members_Arry forKey:@"group_members"];
    [dict setObject:delet_Members_Arry forKey:@"delete_group_members"];
    [dict setObject:_group_id forKey:@"group_id"];
    [dict setObject:[NSString stringWithFormat:@"%@",[Defaults valueForKey:User_ID]] forKey:@"user_id"];
    NSLog(@"%@",dict);
    
    [SHARED_API UpdateGroupRequestsWithParams:dict withSuccess:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSLog(@"responce is %@",response);
            
            if ([[response valueForKey:STATUS] isEqualToString:SUCCESS]) {
                if ([activeIndicatore isAnimating])
                {
                    [activeIndicatore stopAnimating];
                    _main_content_view.hidden = NO;
                    [activeIndicatore removeFromSuperview];
                    [appDelegate.createGroup_frndInvitations removeAllObjects];
                    [group_members_array removeAllObjects];
//                    [SHARED_HELPER showAlert:groupupdated];
                    [SHARED_HELPER showAlert:groupMemberInvited];
                    
                    int duration = 1; // duration in seconds
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                         appDelegate.edit_List_Groups=@"YES";
                         [self get_groupsdetail_list];
                    });
                }
            }
            else
            {
                if ([activeIndicatore isAnimating])
                {
                    [activeIndicatore stopAnimating];
                    _main_content_view.hidden = NO;
                    [activeIndicatore removeFromSuperview];
                }
                [SHARED_HELPER showAlert:ServiceFail];
            }
            
        });
        
    } onfailure:^(NSError *theError) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([activeIndicatore isAnimating])
            {
                [activeIndicatore stopAnimating];
                _main_content_view.hidden = NO;
                [activeIndicatore removeFromSuperview];
            }
            [SHARED_HELPER showAlert:ServerConnection];
        });
        
    }];
}

-(void)backclicked{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)configureCoverPicForUser:(NSString *)user
{
    NSURL *url = [NSURL URLWithString:user];
    [self downloadImageWithURL:url completionBlock:^(BOOL succeeded, UIImage *image) {
        if (succeeded) {
            
            if (image==nil) {
                //cell.userImageView.image=[UIImage imageNamed:@"userLogo.png"];
            }
            else{
                self.Groupdetail_img.image=image;
                
            }
            
        }
    }];
    
}

//- (void)photoForview:(NSString *)user
//{
//    NSURL *url = [NSURL URLWithString:user];
//    [self downloadImageWithURL:url completionBlock:^(BOOL succeeded, UIImage *image) {
//        if (succeeded) {
//            
//            if (image==nil) {
//                
////                 _Groupdetail_img.contentMode = UIViewContentModeScaleAspectFill;
//            }
//            else{
//                _Groupdetail_img.image=image;
////                 _Groupdetail_img.contentMode = UIViewContentModeScaleAspectFill;
//            }
//            
//        }
//    }];
//    
//}
- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES,image);
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
}

- (void)photoForCell:(GroupDetailTableViewCell *)cell user:(NSString *)user withIndexPath:(NSIndexPath *)indexPath
{
    NSURL *url = [NSURL URLWithString:user];
    [self downloadImageWithURL:url completionBlock:^(BOOL succeeded, UIImage *image) {
        if (succeeded) {
            
            if (image==nil) {
                cell.groupdetail_img.image=[UIImage imageNamed:@"user_Placeholder"];
            }
            else{
                cell.groupdetail_img.image=image;
                
            }
            
        }
    }];
    
}
- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}

- (IBAction)onClick_leaveGroup_btn:(id)sender {
    
    
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Leave Group"
                                      message:@"Are you sure you want leave group?"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Yes"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [self Laeve_Group_Service];
                                 
                             }];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
        
        [alert addAction:ok];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
-(void)Laeve_Group_Service
{
    
if (![SHARED_HELPER checkIfisInternetAvailable])
{
    [SHARED_HELPER showAlert:Nonetwork];
    return;
}
activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
activeIndicatore.center = self.view.center;
activeIndicatore.color = APP_YELLOW_COLOR;
activeIndicatore.hidesWhenStopped = TRUE;
[activeIndicatore startAnimating];
[self.view addSubview:activeIndicatore];
NSMutableDictionary *dict = [NSMutableDictionary new];
[dict setObject:_group_id forKey:@"group_id"];
[dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
NSLog(@"%@",dict);
[SHARED_API leaveGroupRequestsWithParams:dict withSuccess:^(NSDictionary *response) {
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSLog(@"Leave Group responce is %@",response);
        
        if ([[response valueForKey:STATUS] isEqualToString:SUCCESS]) {
            [activeIndicatore stopAnimating];
            appDelegate.leave_OR_Delete_Group_YES=@"YES";
            [self.navigationController popViewControllerAnimated:YES];
            
        }
    });
    
} onfailure:^(NSError *theError) {
    dispatch_async(dispatch_get_main_queue(), ^{
        [activeIndicatore stopAnimating];
        [SHARED_HELPER showAlert:ServerConnection];
    });
}];
}



- (IBAction)onClick_AddMembers_Btn:(id)sender {
   
    RideInviteFriends*rideInviteFrnds=[self.storyboard instantiateViewControllerWithIdentifier:@"RideInviteFriends"];
    rideInviteFrnds.isFrom=@"edit";
    rideInviteFrnds.group_id=_group_id;
    [self.navigationController pushViewController:rideInviteFrnds animated:YES];
}
- (IBAction)onClick_groupDetail_rideGoingBtn:(id)sender {
    
    self.groupDetail_scrollLabel.hidden=YES;
    self.groupDetails_rideGoingBtn.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }

}
@end
