//
//  AppHelper.m
//  Thredz
//
//  Created by Jyothsna on 7/11/16.
//  Copyright © 2016 ebiz. All rights reserved.
//

#import "AppHelperClass.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "SignupViewController.h"

@implementation AppHelperClass


#pragma - AppHelperClass sharedInstance

+(AppHelperClass *)appsharedInstance {
    static AppHelperClass *_sharedInstance = nil;
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{ _sharedInstance = [[self alloc] init]; });
    return _sharedInstance;
}

//save user details

-(UserDetails *)getStoredUser
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:User_ID];
    UserDetails *user  = (UserDetails*)[NSKeyedUnarchiver unarchiveObjectWithData:data];
    return user;
}




//Checking email validations using email regex

-(BOOL)checkEmailValidations:(NSString *)email
{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
    
}
-(void)setShadowToView:(UIView *)view withColor:(UIColor *)shadowColor
{
    [view.layer setShadowColor:shadowColor.CGColor];
    [view.layer setShadowOpacity:0.5];
    [view.layer setShadowRadius:1.0];
    [view.layer setShadowOffset:CGSizeMake(1, 1)];
    [view.layer setMasksToBounds:NO];
    
}

-( NSString*) mimeTypeFromFileExt:(NSString* )fileName
{
    
    // NSArray *components = [fileName componentsSeparatedByString:@"."];
    // if([components count]<2)return @"dir";
    NSString *ext = [fileName lowercaseString];// [[components lastObject] lowercaseString];
    //Documents
    if([ext isEqualToString:@"pdf"])return @"application/pdf";
    else if([ext isEqualToString:@"rtf"])return @"text/rtf";
    else if([ext isEqualToString:@"txt"])return @"text/plain";
    
    //Microsoft Office
    else if([ext isEqualToString:@"doc"])return @"application/msword";
    else if([ext isEqualToString:@"docx"])return @"application/vnd.openxmlformats-officedocument.wordprocessingml.document";
    else if([ext isEqualToString:@"ppt"])return @"application/vnd.ms-powerpoint";
    else if([ext isEqualToString:@"pptx"])return @"application/vnd.openxmlformats-officedocument.presentationml.presentation";
    else if([ext isEqualToString:@"xls"])return @"application/vnd.ms-excel";
    else if([ext isEqualToString:@"xlsx"]||[ext isEqualToString:@"xlsm"]||[ext isEqualToString:@"xlsb"])return @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    
    //Open Office
    else if([ext isEqualToString:@"odt"])return @"application/vnd.oasis.opendocument.text";
    else if([ext isEqualToString:@"odp"])return @"application/vnd.oasis.opendocument.presentation";
    else if([ext isEqualToString:@"ods"])return @"application/vnd.oasis.opendocument.spreadsheet";
    else if([ext isEqualToString:@"odg"])return @"application/vnd.oasis.opendocument.graphics";
    
    //iWork (Unofficial, Technically application/zip)
    else if([ext isEqualToString:@"pages"])return @"application/x-iwork-pages-sffpages";
    else if([ext isEqualToString:@"keynote"])return @"application/x-iwork-keynote-sffkeynote";
    else if([ext isEqualToString:@"numbers"])return @"application/x-iwork-numbers-sffnumbers";
    
    //Web Markup
    else if([ext isEqualToString:@"htm"]||[ext isEqualToString:@"html"]||[ext isEqualToString:@"shtml"])return @"text/html";
    else if([ext isEqualToString:@"css"])return @"text/css";
    else if([ext isEqualToString:@"js"])return @"application/javascript";
    else if([ext isEqualToString:@"php"])return @"application/x-php"; //There is some dispute to this
    else if([ext isEqualToString:@"xml"]||[ext isEqualToString:@"rss"])return @"text/xml";
    else if([ext isEqualToString:@"swf"])return @"application/x-shockwave-flash";
    
    //Images
    else if([ext isEqualToString:@"png"])return @"image/png";
    else if([ext isEqualToString:@"bmp"])return @"image/bmp";
    else if([ext isEqualToString:@"gif"])return @"image/gif";
    else if([ext isEqualToString:@"tif"]||[ext isEqualToString:@"tiff"])return @"image/tiff";
    else if([ext isEqualToString:@"ico"])return @"image/vnd.microsoft.icon";
    else if([ext isEqualToString:@"svg"])return @"image/svg+xml";
    else if([ext isEqualToString:@"jpg"]||[ext isEqualToString:@"jpeg"])return @"image/jpeg";
    
    //Audio
    else if([ext isEqualToString:@"mp3"])return @"audio/mpeg";
    else if([ext isEqualToString:@"m4a"])return @"audio/mp4";
    else if([ext isEqualToString:@"ogg"]||[ext isEqualToString:@"oga"]||[ext isEqualToString:@"spx"])return @"audio/vorbis";
    else if([ext isEqualToString:@"flac"])return @"audio/x-flac";
    else if([ext isEqualToString:@"wma"])return @"audio/x-ms-wma";
    else if([ext isEqualToString:@"wav"])return @"audio/vnd.wave";
    else if([ext isEqualToString:@"aif"]||[ext isEqualToString:@"aifc"]||[ext isEqualToString:@"aiff"])return @"audio/x-aiff";
    else if([ext isEqualToString:@"ra"]||[ext isEqualToString:@"ram"])return @"audio/x-pn-realaudio";
    
    //Movies
    else if([ext isEqualToString:@"mp4"]||[ext isEqualToString:@"m4v"])return @"video/mp4";
    else if([ext isEqualToString:@"webm"])return @"video/webm";
    else if([ext isEqualToString:@"ogv"])return @"video/ogg";
    else if([ext isEqualToString:@"mov"]||[ext isEqualToString:@"qt"])return @"video/quicktime";
    else if([ext isEqualToString:@"mp2"]||[ext isEqualToString:@"mpg"]||[ext isEqualToString:@"mpeg"])return @"video/mpeg";
    else if([ext isEqualToString:@"wmv"])return @"video/x-ms-wmv";
    else if([ext isEqualToString:@"avi"])return @"video/x-msvideo";
    else if([ext isEqualToString:@"divx"])return @"video/x-divx";
    
    //Compression
    else if([ext isEqualToString:@"zip"])return @"application/zip";
    else if([ext isEqualToString:@"gz"])return @"application/x-gzip";
    else if([ext isEqualToString:@"tar"])return @"application/x-tar";
    else if([ext isEqualToString:@"sit"]||[ext isEqualToString:@"sitx"]||[ext isEqualToString:@"sif"])return @"application/x-stuffit";
    else if([ext isEqualToString:@"rar"])return @"application/x-rar-compressed";
    else if([ext isEqualToString:@"dmg"])return @"application/x-apple-diskimage";
    
    return @"text/plain";
}
-(UIImage *)imageFromColor:(UIColor *)color

{    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}



#pragma - Internet Notifier

-(void)setInternetNotifier
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    
    //Change the host name here to change the server you want to monitor.
    self.internetReachability = [Reachability reachabilityForInternetConnection];
    [self.internetReachability startNotifier];
    NetworkStatus netStatus = [self.internetReachability currentReachabilityStatus];
    
    if (netStatus==NotReachable) {
        [SHARED_HELPER setInternetAvailable:NO];
        
    }else{
        [SHARED_HELPER setInternetAvailable:YES];
    }
}


-(BOOL)checkIfisInternetAvailable
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable)
    {
        [SHARED_HELPER setInternetAvailable:NO];
        
    }else
    {
        [SHARED_HELPER setInternetAvailable:YES];
    }
    return _internetAvailable;
    
}




-(void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message andShowinView:(UIViewController *)vc
{
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:NSLocalizedString(title,nil)
                                          message:NSLocalizedString(message, nil)
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", nil)
                               style:UIAlertActionStyleDestructive
                               handler:^(UIAlertAction *action)
                               {
                                   [alertController dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [alertController addAction:okAction];
    
    [vc presentViewController:alertController animated:YES completion:nil];
}

-(void)showAppUpdateAlertWithTitle:(NSString *)title andMessage:(NSString *)message andShowinView:(UIViewController *)vc
{
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:NSLocalizedString(title,nil)
                                          message:NSLocalizedString(message, nil)
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"Download Now", nil)
                               style:UIAlertActionStyleDestructive
                               handler:^(UIAlertAction *action)
                               {
                                   //[alertController dismissViewControllerAnimated:YES completion:nil];
                                   [self goToDownloadAppFromStore];
                               }];
    
    /* UIAlertAction *cancelAction = [UIAlertAction
     actionWithTitle:NSLocalizedString(@"Cancel", nil)
     style:UIAlertActionStyleDestructive
     handler:^(UIAlertAction *action)
     {
     [alertController dismissViewControllerAnimated:YES completion:nil];
     }];*/
    
    [alertController addAction:okAction];
    // [alertController addAction:cancelAction];
    
    
    [vc presentViewController:alertController animated:YES completion:nil];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationWillResignActiveNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification* notification){
        [alertController dismissViewControllerAnimated:YES completion:nil];
        
    }];
}

-(void)goToDownloadAppFromStore
{
    NSString* updateURLString = @"https://itunes.apple.com/app/id1078942797";
    NSURL* updateURL = [NSURL URLWithString:updateURLString];
    [[UIApplication sharedApplication] openURL:updateURL];
    
    
}

- (void) reachabilityChanged:(NSNotification *)note
{
    Reachability* reachability = [note object];
    NSParameterAssert([reachability isKindOfClass:[Reachability class]]);
    
    NetworkStatus netStatus = [reachability currentReachabilityStatus];
    //BOOL connectionRequired = [reachability connectionRequired];
    //NSString* statusString  = @"";
    
    switch (netStatus)
    {
        case NotReachable:        {
            // connectionRequired = NO;
            [SHARED_HELPER setInternetAvailable:NO];
            break;
        }
            
        case ReachableViaWWAN:        {
            //statusString = NSLocalizedString(@"Reachable WWAN", @"");
            [SHARED_HELPER setInternetAvailable:YES];
            [self hideBanner];
            // [self callScreensToRefresh];
            break;
        }
        case ReachableViaWiFi:        {
            // statusString= NSLocalizedString(@"Reachable WiFi", @"");
            [SHARED_HELPER setInternetAvailable:YES];
            [self hideBanner];
            // [self callScreensToRefresh];
            break;
        }
    }
    
}



-(void)hideValidataionAlertBanner
{
    //[self hideBanner];
    
    [self performSelector:@selector(hideBanner) withObject:self afterDelay:2.0];
}


-(NSString *)getDeviceUUID
{
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"deviceUUID"])
    {
        
        // NSLog(@"the uuid is %@", [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceUUID"]);
        
        return [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceUUID"];
        
    }
    
    NSString *deviceUUID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    [[NSUserDefaults standardUserDefaults]setObject:deviceUUID forKey:@"deviceUUID"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    //  NSLog(@"the return uuid is %@", deviceUUID);
    return deviceUUID;
    
    /*@autoreleasepool {
     
     CFUUIDRef uuidReference = CFUUIDCreate(nil);
     CFStringRef stringReference = CFUUIDCreateString(nil, uuidReference);
     NSString *uuidString = (__bridge NSString *)(stringReference);
     [[NSUserDefaults standardUserDefaults] setObject:uuidString forKey:[[NSBundle mainBundle] bundleIdentifier]];
     [[NSUserDefaults standardUserDefaults] synchronize];
     CFRelease(uuidReference);
     CFRelease(stringReference);
     
     NSLog(@"the return uuid is %@", uuidString);
     
     return uuidString;
     }*/
    
}
-(NSString *)getAppBuildVersion
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleVersionKey];
}

- (NSString*)base64forData:(NSData*)theData
{
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}

-(void)showAlert:(NSString *)message
{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:message  preferredStyle:UIAlertControllerStyleActionSheet];
    UIView  *firstSubview = alert.view.subviews.firstObject;
    firstSubview.backgroundColor = APP_YELLOW_COLOR;
    UIView *alertContentView = firstSubview.subviews.firstObject;
    alertContentView.backgroundColor = APP_YELLOW_COLOR;
    alertContentView.tintColor = [UIColor blueColor];
    for (UIView *subSubView in alertContentView.subviews) {
        
        subSubView.backgroundColor = APP_YELLOW_COLOR;
    }
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alert animated:YES completion:nil];
    
    int duration = 2; // duration in seconds
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        
        [alert dismissViewControllerAnimated:YES completion:nil];
        
    });
    
}

- (NSString *) platformType:(NSString *)platform
{
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone3,3"])    return @"Verizon iPhone 4";
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5 (GSM)";
    if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone 5 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone5,3"])    return @"iPhone 5c (GSM)";
    if ([platform isEqualToString:@"iPhone5,4"])    return @"iPhone 5c (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone6,1"])    return @"iPhone 5s (GSM)";
    if ([platform isEqualToString:@"iPhone6,2"])    return @"iPhone 5s (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone7,2"])    return @"iPhone 6";
    if ([platform isEqualToString:@"iPhone7,1"])    return @"iPhone 6 Plus";
    if ([platform isEqualToString:@"iPhone8,1"])    return @"iPhone 6s";
    if ([platform isEqualToString:@"iPhone8,2"])    return @"iPhone 6s Plus";
    if ([platform isEqualToString:@"iPhone8,4"])    return @"iPhone SE";
    if ([platform isEqualToString:@"iPhone9,1"])    return @"iPhone 7";
    if ([platform isEqualToString:@"iPhone9,2"])    return @"iPhone 7 Plus";
    if ([platform isEqualToString:@"iPhone9,3"])    return @"iPhone 7";
    if ([platform isEqualToString:@"iPhone9,4"])    return @"iPhone 7 Plus";
    if ([platform isEqualToString:@"iPhone10,1"])    return @"iPhone 8";
    if ([platform isEqualToString:@"iPhone10,2"])    return @"iPhone 8 Plus";
    if ([platform isEqualToString:@"iPhone10,3"])    return @"iPhone X";
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([platform isEqualToString:@"iPod5,1"])      return @"iPod Touch 5G";
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    if ([platform isEqualToString:@"iPad2,4"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,5"])      return @"iPad Mini (WiFi)";
    if ([platform isEqualToString:@"iPad2,6"])      return @"iPad Mini (GSM)";
    if ([platform isEqualToString:@"iPad2,7"])      return @"iPad Mini (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,1"])      return @"iPad 3 (WiFi)";
    if ([platform isEqualToString:@"iPad3,2"])      return @"iPad 3 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,3"])      return @"iPad 3 (GSM)";
    if ([platform isEqualToString:@"iPad3,4"])      return @"iPad 4 (WiFi)";
    if ([platform isEqualToString:@"iPad3,5"])      return @"iPad 4 (GSM)";
    if ([platform isEqualToString:@"iPad3,6"])      return @"iPad 4 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad4,1"])      return @"iPad Air (WiFi)";
    if ([platform isEqualToString:@"iPad4,2"])      return @"iPad Air (Cellular)";
    if ([platform isEqualToString:@"iPad4,3"])      return @"iPad Air";
    if ([platform isEqualToString:@"iPad4,4"])      return @"iPad Mini 2G (WiFi)";
    if ([platform isEqualToString:@"iPad4,5"])      return @"iPad Mini 2G (Cellular)";
    if ([platform isEqualToString:@"iPad4,6"])      return @"iPad Mini 2G";
    if ([platform isEqualToString:@"iPad4,7"])      return @"iPad Mini 3 (WiFi)";
    if ([platform isEqualToString:@"iPad4,8"])      return @"iPad Mini 3 (Cellular)";
    if ([platform isEqualToString:@"iPad4,9"])      return @"iPad Mini 3 (China)";
    if ([platform isEqualToString:@"iPad5,3"])      return @"iPad Air 2 (WiFi)";
    if ([platform isEqualToString:@"iPad5,4"])      return @"iPad Air 2 (Cellular)";
    if ([platform isEqualToString:@"AppleTV2,1"])   return @"Apple TV 2G";
    if ([platform isEqualToString:@"AppleTV3,1"])   return @"Apple TV 3";
    if ([platform isEqualToString:@"AppleTV3,2"])   return @"Apple TV 3 (2013)";
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    if ([platform isEqualToString:@"x86_64"])       return @"Simulator";
    return platform;
}

@end
