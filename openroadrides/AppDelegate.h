//
//  AppDelegate.h
//  openroadrides
//
//  Created by apple on 27/04/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/SignIn.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "LoginViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>
#import <AWSS3/AWSS3.h>
#import <AWSCore/AWSCore.h>
#import "Constants.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "HomeDashboardView.h"
#import "BIZPopupViewController.h"
#import "Reachability.h"
#import "AddPlace+CoreDataClass.h"
#import "RideDashboard.h"
#import "GetStartedViewController.h"

@import GoogleMaps;
@class LoginViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate, GIDSignInDelegate,CLLocationManagerDelegate>
//com.ebiz.openroadriders
//com.ebiz.wastemanagement
{
    
    //BG Syncing
    NSInteger places_index,s3places_index,ride_id_local_DB;
    
    
}
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString  *app_Status;
@property (strong, nonatomic) LoginViewController *loginview;

@property (nonatomic,retain) Reachability *internetReachability,*hostName;

@property (nonatomic,strong) NSString *loginwithstrg;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) NSMutableArray *addAPlace_imagesArray,*addAPlace_data_Array;
@property (strong, nonatomic) NSMutableArray *Save_Route_Data_Array,*createGroup_frndInvitations,*arrayOfSelectedFrndsAndRiders,*arrayOfSelectedGroupsAndRiders,*arrayOFRemoveFrndsID;
@property (nonatomic,strong) NSString *pop_UP_out_Side_Click_Hide,*isFrom_RidersVC_Or_FreeRideRoutes,*isFrom_ride_scheduled_Or_fullFilled,*editRideString,*user_Profile_edited;
@property (nonatomic,strong) NSString *un_frind_in_Profile;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (strong, nonatomic) NSString *check_in_Lat,*check_in_Long,*check_in_Clicked,*ride_Random_Number;
@property (strong, nonatomic) NSString *add_Place_marker_title,*is_Done_Add_Place;
@property (strong, nonatomic) NSString *checkin_marker_title,*FreeRide_Name,*select_Route_Id_For_CreateAndFreeRide,*select_Ride_ID_RideList,*ride_ID_Api,*ride_status_start,*Ride_Name_From_Schedule,*select_Route_Address_For_CreateAndFreeRide;

//for ride detail
@property (strong, nonatomic) NSString *Ride_Detail_Edited;

//for groups
@property (strong, nonatomic) NSString *leave_OR_Delete_Group_YES,*edit_Group,*edit_List_Groups;

//For Events
@property (strong, nonatomic) NSString *update_event,*delete_event,*create_event_or_edit_event;

//Friends Controller
@property NSInteger valueForIndex;


//S3 For AddPlaces & Background_Syncing
@property (nonatomic, strong) AWSS3TransferManagerUploadRequest *uploadRequest;
@property (nonatomic) uint64_t filesize;
@property (nonatomic) uint64_t amountUploaded;
@property (nonatomic, strong) UILabel *progressLabel;
@property BOOL internet_Available,ridedashboard_home;
@property (nonatomic, strong) NSString *is_Running_Background_Sync;
@property (nonatomic, strong) NSArray *completed_Rides_Array;
@property (nonatomic, strong) NSDictionary *completed_Ride_route_dict;

//User_Current_Coordinates
@property (nonatomic)double app_current_lattitude,app_current_longitude;


//MarketPlace
@property(nonatomic,strong)NSString *edit_posted,*my_post_Changed,*start_address_latitude,*start_address_longtitude;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
@end

