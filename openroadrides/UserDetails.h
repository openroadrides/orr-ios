//
//  UserDetails.h
//  Thredz
//
//  Created by Jyothsna on 7/8/16.
//  Copyright © 2016 ebiz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@interface UserDetails : JSONModel

@property (nonatomic,retain) NSString <Optional> *user_id,*user_name,*user_email;

@end
