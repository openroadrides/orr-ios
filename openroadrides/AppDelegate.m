//
//  AppDelegate.m
//  openroadrides
//
//  Created by apple on 27/04/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "AppDelegate.h"
#import "UserDetails.h"
#import "DashboardViewController.h"
#import "Constants.h"
#import "Notifications.h"
@interface AppDelegate () 

@end

@implementation AppDelegate
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    _is_Running_Background_Sync=@"NO";
    _ridedashboard_home=NO;
    [Fabric with:@[[Crashlytics class]]];
    
    UITextField *lagFreeField = [[UITextField alloc] init];
    [self.window addSubview:lagFreeField];
    [lagFreeField becomeFirstResponder];
    [lagFreeField resignFirstResponder];
    [lagFreeField removeFromSuperview];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(Check_Internet_Status) name:kReachabilityChangedNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(Check_Internet_Status) name:@"SaveRide_Service" object:nil];
    
    [Defaults setObject:@"RIDE_ENDED" forKey:@"RIDE_STATUS"];
    
    //Change the host name here to change the server you want to monitor.
    self.internetReachability = [Reachability reachabilityForInternetConnection];
    [self.internetReachability startNotifier];
   
   
   
    
    
    AWSStaticCredentialsProvider *credentialsProvider = [[AWSStaticCredentialsProvider alloc]initWithAccessKey:ACCESS_KEY secretKey:SECRET_KEY];
    AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionUSWest2 credentialsProvider:credentialsProvider];
    [AWSServiceManager defaultServiceManager].defaultServiceConfiguration = configuration;

    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }

    
    // Override point for customization after application launch.
    [GMSServices provideAPIKey:@"AIzaSyD84k1kRny-hKgT_XpuzY3InAQjS12-SgE"];
    [GMSPlacesClient provideAPIKey:@"AIzaSyCYinhpForKMGAdsftxaLLepsD-zKSK0Kc"];
//    AIzaSyD84k1kRny-hKgT_XpuzY3InAQjS12-SgE
    
    
    NSString *device_id = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSUserDefaults *device_id_defaults = [NSUserDefaults standardUserDefaults];
    [device_id_defaults setObject:device_id forKey:UDID];
    

    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *user_data = [NSString stringWithFormat:@"%@",[defaults objectForKey:User_ID]];
  
    
    
    [[UINavigationBar appearance] setTintColor:[UIColor blackColor]];
    NSError* configureError;
    [[GGLContext sharedInstance] configureWithError: &configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    [GIDSignIn sharedInstance].delegate = self;
    
    NSString *getStarted = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"getStartedName"];
    
    if ([getStarted isEqualToString:@"YES"])
    {
        if([user_data isEqualToString:@"" ]|| user_data == nil || [user_data isEqualToString:@"(null)"] || [user_data isEqualToString:@"<null>"] || [user_data isEqualToString:@"null"]){
            LoginViewController *login = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"LoginViewController"];
            [(UINavigationController *)self.window.rootViewController pushViewController:login animated:NO];
        }
        else
        {
            if (launchOptions == nil)//When Notifications Time
            {
                HomeDashboardView *homedashboard = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"HomeDashboardView"];
                [(UINavigationController *)self.window.rootViewController pushViewController:homedashboard animated:NO];
                
                NSLog(@"USer ID %@",[defaults valueForKey:@"User_ID"]);
            }
            else
            {
                NSDictionary* userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
                if (userInfo!=nil) {
                    NSLog(@"USer data %@",userInfo);
                    NSString *alert_str=[NSString stringWithFormat:@"%@",[[userInfo valueForKey:@"aps"] valueForKey:@"alert"]];
                    
                    NSDictionary *type_dict=[[userInfo valueForKey:@"custom"] valueForKey:@"a"];
                    NSString *notification_type=[type_dict valueForKey:@"userType"];
                    
                    if ([notification_type isEqualToString:@"admin_notifications"]) {
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Notification"
                                                                            message:alert_str  delegate:self
                                                                  cancelButtonTitle:@"OK"
                                                                  otherButtonTitles:nil, nil];
                        [alertView show];
                        
                        HomeDashboardView *homedashboard = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"HomeDashboardView"];
                        [(UINavigationController *)self.window.rootViewController pushViewController:homedashboard animated:NO];
                        
                        
                    }
                    else
                    {
                        Notifications *cntlr = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"Notifications"];
                        cntlr.is_from=@"Launching";
                        [(UINavigationController *)self.window.rootViewController pushViewController:cntlr animated:NO];
                    }
                }
                else
                {
                    HomeDashboardView *homedashboard = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"HomeDashboardView"];
                    [(UINavigationController *)self.window.rootViewController pushViewController:homedashboard animated:NO];
                }
            }
        }
    }
    else
    {
        GetStartedViewController *homedashboard = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"GetStartedViewController"];
        [(UINavigationController *)self.window.rootViewController pushViewController:homedashboard animated:NO];
    }
    
    
     self.locationManager = [[CLLocationManager alloc] init];
     self.locationManager.delegate = self;
    _app_current_lattitude=self.locationManager.location.coordinate.latitude;
    _app_current_longitude=self.locationManager.location.coordinate.longitude;
     [self checkLocationAuthorize];
     [self Check_Internet_Status];
    
    //Stop Display Annotations
    [self stop_Annotations];
    
    
    NSLog( @"### running FB sdk version: %@", [FBSDKSettings sdkVersion] );
    return YES;
}
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
     if (application.applicationState == UIApplicationStateActive ) {
         
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:notification.alertBody preferredStyle:UIAlertControllerStyleAlert];
         
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
         
         
        }]];
         
[self.window.rootViewController presentViewController:actionSheet animated:YES completion:nil];
         
     }
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    // NSLog(@"user info %@",userInfo);
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    if (userInfo!=nil) {
        
        if (application.applicationState == UIApplicationStateActive ) {
            
            NSString *alert_str=[NSString stringWithFormat:@"%@",[[userInfo valueForKey:@"aps"] valueForKey:@"alert"]];
            
            NSDictionary *type_dict=[[userInfo valueForKey:@"custom"] valueForKey:@"a"];
            NSString *notification_type=[type_dict valueForKey:@"userType"];
            
            if ([notification_type isEqualToString:@"admin_notifications"]) {
                
            UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Notification" message:alert_str preferredStyle:UIAlertControllerStyleAlert];

            [actionSheet addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {

            }]];

       [self.window.rootViewController presentViewController:actionSheet animated:YES completion:nil];
            }

        }
        else{
           
            NSLog(@"the dictionary is %@",userInfo);
            NSString *alert_str=[NSString stringWithFormat:@"%@",[[userInfo valueForKey:@"aps"] valueForKey:@"alert"]];
            
            NSDictionary *type_dict=[[userInfo valueForKey:@"custom"] valueForKey:@"a"];
            NSString *notification_type=[type_dict valueForKey:@"userType"];
            
            if ([notification_type isEqualToString:@"admin_notifications"]) {
                UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Notification" message:alert_str preferredStyle:UIAlertControllerStyleAlert];
                
                [actionSheet addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                    
                    
                }]];
                
                [self.window.rootViewController presentViewController:actionSheet animated:YES completion:nil];
            }
            else
            {
            if ([[Defaults valueForKey:@"RIDE_STATUS"]isEqualToString:@"RIDE_STARTED" ]) {
                
            }
            else
            {
                NSString *user_data = [NSString stringWithFormat:@"%@",[Defaults objectForKey:User_ID]];
                if([user_data isEqualToString:@"" ]|| user_data == nil || [user_data isEqualToString:@"(null)"] || [user_data isEqualToString:@"<null>"] || [user_data isEqualToString:@"null"]){
                    LoginViewController *login = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"LoginViewController"];
                    [(UINavigationController *)self.window.rootViewController pushViewController:login animated:NO];
                    
                }
                else{
                    Notifications *cntlr = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"Notifications"];
                    cntlr.is_from=@"Launching";
                    [(UINavigationController *)self.window.rootViewController pushViewController:cntlr animated:NO];
                }
            }
            }
        }
    }
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    //NSLog(@"didRegisterForRemoteNotificationsWithDeviceToken");
    
   NSString * _pushTokenString = [[[deviceToken description]
                         stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]]
                        stringByReplacingOccurrencesOfString:@" "
                        withString:@""];
    [Defaults setObject:_pushTokenString forKey:Device_ID];
    NSLog(@"pushTokenString %@",_pushTokenString);
}
-(void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err
{
    NSString *str = [NSString stringWithFormat: @"Error: %@", err];
    NSLog(@"Device Token %@",str);
}
- (BOOL)checkLocationAuthorize
{
    
    BOOL available = NO;
    if (self.locationManager == nil) {
        self.locationManager = [[CLLocationManager alloc] init];
        [self.locationManager setDelegate:self];
    }
    
    if ([CLLocationManager locationServicesEnabled]) {
        CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
        if (status == kCLAuthorizationStatusNotDetermined) {
            if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                [self.locationManager requestAlwaysAuthorization];
            }
            else {
                [self.locationManager startUpdatingLocation];
                [self.locationManager stopUpdatingLocation];
            }
            available = YES;
        }
        else if (status == kCLAuthorizationStatusDenied) {
            NSLog(@"Location authorization denied");
        }
        else if (status == kCLAuthorizationStatusRestricted) {
            NSLog(@"Location authorization restricted");
        }
        else {
            available = YES;
        }
    }
    else {
        NSLog(@"Use location disabled");
    }
    return available;
}
- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region
{
    
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    
    if(state == CLRegionStateInside)
    {
        //notification.alertBody = NSLocalizedString(@"You're inside the region", @"");
    }
    else if(state == CLRegionStateOutside)
    {
        //notification.alertBody = NSLocalizedString(@"You're outside the region", @"");
    }
    else
    {
        return;
    }
    
    
    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    
    
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
    NSLog(@"did enter region");
}

- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    
    // Perform any operations on signed in user here.
    NSString *userId = user.userID;                  // For client-side use only!
    NSString *idToken = user.authentication.idToken; // Safe to send to the server
    NSString *fullName = user.profile.name;
    NSString *givenName = user.profile.givenName;
    NSString *familyName = user.profile.familyName;
    NSString *email = user.profile.email;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:email forKey:@"gglsgInemail"];
    [defaults setObject:givenName forKey:@"gglsgInefstname"];
    [defaults setObject:familyName forKey:@"gglsgInelstname"];
    [defaults setObject:userId forKey:@"gglsgInId"];
   
    
    NSLog(@"UserId: %@, idToken: %@, fullName: %@, email: %@",userId,idToken,fullName,email);
    if (email==nil)
    {
        
    }
    else
    {
        [self.loginview googlesociallogin];
    }

    
    // ...
}

- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.
    // ...
}
- (void)applicationWillResignActive:(UIApplication *)application
{
     _app_Status=@"Background";
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application
{
    
    
    UIApplication *app = [UIApplication sharedApplication];
    UIBackgroundTaskIdentifier bgTask = 0;
    bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        [app endBackgroundTask:bgTask];
    }];
  

    [self.window endEditing:YES];
       _app_Status=@"Background";
    
 
   
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application
{
    
    _app_Status=@"ForeGround";
   
    
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application
{
    
     _app_Status=@"ForeGround";
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    NSString *url_str=[NSString stringWithFormat:@"%@",url];
    
    if ([url_str containsString:@"fb"])
    {
        return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                              openURL:url
                                                    sourceApplication:sourceApplication
                                                           annotation:annotation];
    }
    else
        return [[GIDSignIn sharedInstance] handleURL:url
                                   sourceApplication:sourceApplication
                                          annotation:annotation];
}
- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Model.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
        
    }
    
    return _persistentStoreCoordinator;
}
#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - Background Syncing Methodes-----Addplaces & RideAPI----->
-(void)Check_Internet_Status{
    NetworkStatus netStatus = [self.internetReachability currentReachabilityStatus];
    if (netStatus==NotReachable) {
        _internet_Available=NO;
    }
    else if (netStatus==ReachableViaWiFi||netStatus==ReachableViaWWAN)
    {
        _internet_Available=YES;
        if ([_is_Running_Background_Sync isEqualToString:@"NO"]) {
             _is_Running_Background_Sync=@"YES";
            [self Start_Syncing];
        }
        else if ([_is_Running_Background_Sync isEqualToString:@"YES"])
        {
            
        }
    }
    else{
       
    }
}

-(void)Start_Syncing{
    _completed_Rides_Array=[self get_Completed_Rides_From_DB];
   
    if (_completed_Rides_Array.count>0) {
        NSDictionary *ride_dict=[_completed_Rides_Array objectAtIndex:0];
        NSString *ride_status=[ride_dict valueForKey:ride_table_ride_Status];
        ride_id_local_DB=[[ride_dict valueForKey:all_table_local_rid] integerValue];
        _completed_Ride_route_dict=[self get_complete_ride_Route_From_DB:ride_id_local_DB];
        if ([ride_status isEqualToString:table_status_complete]) {
            places_index=0;
            [self MoveS3Places];
        }
        else if ([ride_status isEqualToString:table_status_save])
        {
          
            [self save_Ride_API:@""];
        }
        else if ([ride_status isEqualToString:table_status_quit])
        {
            //quit ride_api_hear
            
            [self save_Ride_API:@"Quit"];
        }
    }
    else
    {
        _is_Running_Background_Sync=@"NO";
    }
}

-(void)Xml_Creation{
    NSString *selected_route=[_completed_Ride_route_dict valueForKey:route_table_exist_gpx_url];
    NSString *curent_xml_str=[_completed_Ride_route_dict valueForKey:route_table_gpx_file];
    if (curent_xml_str.length>0) {
        //already xml_created but gpx failed
        [self createGPX:curent_xml_str existing_gpx:selected_route];
    }
    else
    {
    if ([selected_route isEqualToString:@""]) {
        //IF Selected Route
        NSArray *locations=[self get_locations_and_default_Markers_data_from_DB:Locations_Table];
        NSArray *way_Points=[self get_locations_and_default_Markers_data_from_DB:Waypoints_Table];
        NSArray *places_data=[self gettingAddPlacesFromCoreData:ride_id_local_DB];

        TCMXMLWriter *writer = [[TCMXMLWriter alloc] initWithOptions:TCMXMLWriterOptionPrettyPrinted];
        //    [writer instructXML];
        [writer instructXMLStandalone];
        //    <gpx xmlns="http://www.topografix.com/GPX/1/1" version="1.1" creator="OPEN ROAD RIDES">
        [writer tag:@"gpx" attributes:@{@"xmlns": @"http://www.topografix.com/GPX/1/1",@"version": @"1.1",@"creator":@"OPEN ROAD RIDES"} contentBlock:^{
            
            for (int i=0;i<[way_Points count];i++) {
                NSManagedObject *object=[way_Points objectAtIndex:i];
                NSString *lat=[object valueForKey:wayPoints_table_latitude];
                NSString *long_=[object valueForKey:wayPoints_table_longtitude];
                [writer tag:@"wpt" attributes:@{ @"lat":lat, @"lon":long_} contentBlock:^{
                    [writer tag:@"name" contentText:[NSString stringWithFormat:@"WAY POINT%d",i+1]];
                    [writer tag:@"desc" contentText:[NSString stringWithFormat:@"WAY POINT%d",i+1]];
                }];
            }
            
            
            //Tracks------------------------->XML Creation
            [writer tag:@"trk" contentBlock:^{
                [writer tag:@"trkseg" contentBlock:^{
                    for (int i=0;i<[locations count];i++) {
                        NSManagedObject *object=[locations objectAtIndex:i];
                        NSString *lat=[object valueForKey:locations_table_latitude];
                        NSString *long_=[object valueForKey:locations_table_longtitude];
                        NSString *elevation_gpx=[object valueForKey:locations_table_altitude];
                        [writer tag:@"trkpt" attributes:@{@"lat":lat, @"lon":long_} contentBlock:^{
                            [writer tag:@"ele" contentText:[NSString stringWithFormat:@"%@",elevation_gpx]];
                        }];
                    }
                }];
            }];
            
            
            
            //ADD_Places------------------------->XML Creation
            [writer tag:@"extensions" contentBlock:^{
                [writer tag:@"addplace" contentBlock:^{
                    
                    for (int i=0;i<[places_data count];i++) {
                        NSDictionary * addAPlaces_dict=[places_data objectAtIndex:i];
                        
                        [writer tag:@"place" contentBlock:^{
                            
                            [writer tag:@"title" contentText:[addAPlaces_dict valueForKey:@"AddAPlace_Title"]];
                            NSString *desr_is=[NSString stringWithFormat:@"%@",[addAPlaces_dict valueForKey:@"AddAPlace_desc"]];
                            if ([desr_is isEqualToString:@""]) {
                                [writer tag:@"descr" contentText:@" "];
                            } else {
                                [writer tag:@"descr" contentText:[addAPlaces_dict valueForKey:@"AddAPlace_desc"]];
                            }
                            [writer tag:@"time" contentText:[addAPlaces_dict valueForKey:@"AddAPlace_Time"]];
                            [writer tag:@"latitude" contentText:[addAPlaces_dict valueForKey:@"AddAPlace_lat"]];
                            [writer tag:@"longitude" contentText:[addAPlaces_dict valueForKey:@"AddAPlace_long"]];
                            
                            NSInteger count=[[addAPlaces_dict valueForKey:@
                                              "img_count"] integerValue];
                            NSMutableArray *sample_Add_images_array=[[NSMutableArray alloc]init];
                            NSArray *urls_array=[addAPlaces_dict valueForKey:@
                                                 "img_urls"];
                            [sample_Add_images_array addObjectsFromArray:urls_array];
                            if (sample_Add_images_array.count>=count) {
                                [writer tag:@"image" contentBlock:^{
                                    for (int j=0; j<count; j++)
                                    {
                                        [writer tag:@"url" contentText:[sample_Add_images_array objectAtIndex:j]];
                                    }
                                    
                                }];
                            }
                            else{
                                [writer tag:@"image" contentBlock:^{
                                }];
                            }
                        }];
                        
                    }
                    
                }];
            }];
            
        }];
        
        NSLog(@"result XML:n\n%@", writer.XMLString);
        NSString *xml_str = writer.XMLString;
        
        [self createGPX:xml_str existing_gpx:selected_route];
        
    }
    else
    {
        NSArray *places_data=[self gettingAddPlacesFromCoreData:ride_id_local_DB];
        NSURL *url = [NSURL URLWithString:selected_route];
        NSData*fileData = [NSData dataWithContentsOfURL:url];
        NSString *xml_str = [[NSString alloc] initWithData:fileData encoding:NSUTF8StringEncoding];
        
        if ([places_data count]>0) {
    TCMXMLWriter *writer = [[TCMXMLWriter alloc] initWithOptions:TCMXMLWriterOptionPrettyPrinted];
        for (int i=0;i<[places_data count];i++) {
            NSDictionary * addAPlaces_dict=[places_data objectAtIndex:i];
        [writer tag:@"place" contentBlock:^{
            [writer tag:@"title" contentText:[addAPlaces_dict valueForKey:@"AddAPlace_Title"]];
            NSString *desr_is=[NSString stringWithFormat:@"%@",[addAPlaces_dict valueForKey:@"AddAPlace_desc"]];
            if ([desr_is isEqualToString:@""]) {
                [writer tag:@"descr" contentText:@" "];
            } else {
                [writer tag:@"descr" contentText:[addAPlaces_dict valueForKey:@"AddAPlace_desc"]];
            }
            [writer tag:@"time" contentText:[addAPlaces_dict valueForKey:@"AddAPlace_Time"]];
            [writer tag:@"latitude" contentText:[addAPlaces_dict valueForKey:@"AddAPlace_lat"]];
            [writer tag:@"longitude" contentText:[addAPlaces_dict valueForKey:@"AddAPlace_long"]];
            
            NSInteger count=[[addAPlaces_dict valueForKey:@
                              "img_count"] integerValue];
            NSMutableArray *sample_Add_images_array=[[NSMutableArray alloc]init];
            NSArray *urls_array=[addAPlaces_dict valueForKey:@
                                 "img_urls"];
            [sample_Add_images_array addObjectsFromArray:urls_array];
            if (sample_Add_images_array.count>=count) {
                [writer tag:@"image" contentBlock:^{
                    for (int j=0; j<count; j++)
                    {
                        [writer tag:@"url" contentText:[sample_Add_images_array objectAtIndex:j]];
                    }
                }];
            }
            else{
                [writer tag:@"image" contentBlock:^{
                }];
            }
        }];
        }
            
            NSArray *xml_array=[xml_str componentsSeparatedByString:@"</addplace>"];
            xml_str=[NSString stringWithFormat:@"%@\n%@\n%@\n%@\n%@",[xml_array objectAtIndex:0],writer.XMLString,@"</addplace>",@"</extensions>",@"</gpx>"];
        }
        else
        {
            
        }
        
         [self createGPX:xml_str existing_gpx:selected_route];
    }
    }
}

-(void) createGPX:(NSString *)xml_Str  existing_gpx:(NSString *)selected_Route_GPX
{
    if ([SHARED_HELPER checkIfisInternetAvailable]) {
        
    }
    else{
        _is_Running_Background_Sync=@"NO";
         [self Routes_Table_Update_Method:@"" created_xml_str:xml_Str];
        
        return;
    }
    
    NSString *selected_Route_Key_OR_NEW_KEY=@"";
    NSString *gpxString=xml_Str;
    
    // write gpx to file
    NSError *error;
    NSString *filePath = [self gpxFilePath];
    if (![gpxString writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error]) {
        if (error) {
            NSLog(@"error, %@", error);
        }
    }
    
    NSLog(@"filePath%@",filePath);
    
    NSURL *url = [[NSURL alloc] initFileURLWithPath:filePath];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    _uploadRequest = [AWSS3TransferManagerUploadRequest new];
    _uploadRequest.bucket = AMAZON_BUCKET_NAME;
    _uploadRequest.ACL = AWSS3ObjectCannedACLPublicReadWrite;
    
    if ([selected_Route_GPX isEqualToString:@""]) { //Route_Not_Selected
        NSString *amazonS3Url=[NSString stringWithFormat:@"%@/GPX_files/%@",[defaults valueForKey:@"User_ID"],[filePath lastPathComponent]];
        NSLog(@"Amazon S3 Url %@",amazonS3Url);
        selected_Route_Key_OR_NEW_KEY=amazonS3Url;
    }
    else{ //Route_Selected
        selected_Route_Key_OR_NEW_KEY=[selected_Route_GPX stringByReplacingOccurrencesOfString:@"https://openroadrides.s3.amazonaws.com/" withString:@""];
    }
    
    _uploadRequest.key = selected_Route_Key_OR_NEW_KEY;
    _uploadRequest.contentType = @"text/xml";
    _uploadRequest.body = url;
    
    __weak AppDelegate *weakSelf = self;
    
    _uploadRequest.uploadProgress =^(int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend){
        dispatch_sync(dispatch_get_main_queue(), ^{
            weakSelf.amountUploaded = totalBytesSent;
            weakSelf.filesize = totalBytesExpectedToSend;
            
            
        });
    };
    // now the upload request is set up we can creat the transfermanger, the credentials are already set up in the app delegate
    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];
    //    // start the upload
    [[transferManager upload:_uploadRequest] continueWithExecutor:[AWSExecutor mainThreadExecutor] withBlock:^id(AWSTask *task) {
        
        // once the uploadmanager finishes check if there were any errors
        if (task.error) {
            NSLog(@" Gpx Uploading Error%@", task.error);
            [self Routes_Table_Update_Method:@"" created_xml_str:xml_Str];
            _is_Running_Background_Sync=@"NO";
            [self Check_Internet_Status];
        }
        else
        {// if there aren't any then the image is uploaded!
            
            NSString * curent_GPX=[NSString stringWithFormat:@"%@%@",Amazon_url_prefix,selected_Route_Key_OR_NEW_KEY];
            NSLog(@"URl String %@",curent_GPX);
            //Update_Routes_Table
            [self Routes_Table_Update_Method:curent_GPX created_xml_str:xml_Str];
            
            //Call Save API
            [self save_Ride_API:@""];
        }
        return nil;
    }];
    
}
- (void) update{
    _progressLabel.text = [NSString stringWithFormat:@"Uploading:%.0f%%", ((float)self.amountUploaded/ (float)self.filesize) * 100];
}
- (NSString *)gpxFilePath
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setTimeStyle:NSDateFormatterFullStyle];
    [formatter setDateFormat:@"yyyyMMMdd__HHmmss"];
    NSString *dateString = [formatter stringFromDate:[NSDate date]];
    NSString *fileName;
    if ([[SHARED_HELPER rideType]isEqualToString:@"free_ride"]) {
        fileName = [NSString stringWithFormat:@"Free_Ride_%@.gpx", dateString];
    }
    else{
        fileName = [NSString stringWithFormat:@"setup_Ride_%@.gpx", dateString];
    }
    return [NSTemporaryDirectory() stringByAppendingPathComponent:fileName];
}

-(void)MoveS3Places{
    NSArray *places_data=[self gettingAddPlacesFromCoreData:ride_id_local_DB];
    
    if (places_data.count>places_index) {
    NSString *status=[[places_data objectAtIndex:places_index]valueForKey:@"status"];
    if ([status isEqualToString:table_status_complete]) {
        places_index++;
        [self MoveS3Places];
    }
    else
    {
        s3places_index=0;
        [self send_to_s3:places_data];
    }
    }
    else
    {
        //this ride addplaces uploading completed
        [self Xml_Creation];
    }
}
-(void)send_to_s3:(NSArray *)places_data{
    
    NSArray *image_paths=[[places_data objectAtIndex:places_index]valueForKey:@"img_paths"];

    NSInteger img_count=[[[places_data objectAtIndex:places_index]valueForKey:@"img_count"] integerValue];
    NSInteger places_id=[[[places_data objectAtIndex:places_index]valueForKey:@"places_id"] integerValue];
    
    if (img_count>0) {
    if (s3places_index==0) {
        NSArray *image_urls=[[places_data objectAtIndex:places_index]valueForKey:@"img_urls"];
        s3places_index=image_urls.count;
    }
        
    if (image_paths.count>s3places_index) {
        
    NSString *image_filePath=[NSString stringWithFormat:@"%@",[image_paths objectAtIndex:s3places_index] ];
        NSLog(@"Image_filePath===%@",image_filePath);
        NSDateFormatter *formatter1 = [NSDateFormatter new];
        [formatter1 setTimeStyle:NSDateFormatterFullStyle];
        [formatter1 setDateFormat:@"yyyyMMMdd__HHmmss"];
        NSString *dateString = [formatter1 stringFromDate:[NSDate date]];
        NSURL *url = [[NSURL alloc] initFileURLWithPath:image_filePath];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        _uploadRequest = [AWSS3TransferManagerUploadRequest new];
        _uploadRequest.bucket = AMAZON_BUCKET_NAME;
        _uploadRequest.ACL = AWSS3ObjectCannedACLPublicReadWrite;
        NSString *foldercreation_string=[NSString stringWithFormat:@"%@/add_a_place/%@/%@",[defaults valueForKey:@"User_ID"],dateString,[image_filePath lastPathComponent]];
        NSLog(@"Image To s3====%@",foldercreation_string);
        _uploadRequest.key = foldercreation_string;
        _uploadRequest.contentType = @"image/png";
        _uploadRequest.body = url;
        __weak AppDelegate *weakSelf = self;
        _uploadRequest.uploadProgress =^(int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend){
    
            dispatch_sync(dispatch_get_main_queue(), ^{
                weakSelf.amountUploaded = totalBytesSent;
                weakSelf.filesize = totalBytesExpectedToSend;
                [weakSelf update];
            });
    
        };
    
        // now the upload request is set up we can creat the transfermanger, the credentials are already set up in the app delegate
    
        AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];
        [[transferManager upload:_uploadRequest] continueWithExecutor:[AWSExecutor mainThreadExecutor] withBlock:^id(AWSTask *task) {
            // once the uploadmanager finishes check if there were any errors
            if (task.error) {
                _is_Running_Background_Sync=@"NO";
               // [self Check_Internet_Status];
            }
            else
            {
                // NSLog(@"https://s3.amazonaws.com/s3-demo-objectivec/foldername/image.png");
            NSString * AmazonImageUrlString=[NSString stringWithFormat:@"%@%@",Amazon_url_prefix,foldercreation_string];
            NSLog(@"s3Url is %@",AmazonImageUrlString);
    
                [self Update_AddPlace_Table:places_id amazon_url:AmazonImageUrlString];
                s3places_index++;
                [self send_to_s3:places_data];
            }
            return nil;
            
        }];
    }
    else
    {
        places_index++;
        [self MoveS3Places];
    }
    }
    
}
-(void)save_Ride_API :(NSString *)call_For{
    
    NSMutableDictionary *endride_dict = [NSMutableDictionary new];
    NSMutableDictionary *ride_dict = [NSMutableDictionary new];
    NSMutableDictionary *route_dict = [NSMutableDictionary new];
    
    
    NSDictionary *Save_Ride_Dict=[_completed_Rides_Array objectAtIndex:0];
    _completed_Ride_route_dict=[self get_complete_ride_Route_From_DB:ride_id_local_DB];
    
    NSInteger loc_rid=[[Save_Ride_Dict valueForKey:all_table_local_rid] integerValue];
    
    [endride_dict setObject:[Save_Ride_Dict valueForKey:ride_table_userID] forKey:@"user_id"];
    
    [route_dict setObject:[_completed_Ride_route_dict valueForKey:route_table_id] forKey:@"existing_route_id"];
    if ([[_completed_Ride_route_dict valueForKey:route_table_name] isEqualToString:@""]) {
         [route_dict setObject:[NSString stringWithFormat:@"%@ %@",@"Quit Route",[Save_Ride_Dict valueForKey:ride_table_id]] forKey:@"route_title"];
    }
    else
    {
         [route_dict setObject:[_completed_Ride_route_dict valueForKey:route_table_name] forKey:@"route_title"];
    }
   
    [route_dict setObject:[_completed_Ride_route_dict valueForKey:route_table_description] forKey:@"route_description"];
    if ([[Save_Ride_Dict valueForKey:ride_table_start_adress] isEqualToString:@""]) {
        [route_dict setObject:[NSString stringWithFormat:@"%@,%@",[Save_Ride_Dict valueForKey:ride_table_start_lat],[Save_Ride_Dict valueForKey:ride_table_start_long]] forKey:@"route_start_address"];
    }
    else
    {
         [route_dict setObject:[Save_Ride_Dict valueForKey:ride_table_start_adress] forKey:@"route_start_address"];
    }
    
    [route_dict setObject:[_completed_Ride_route_dict valueForKey:route_table_privacy] forKey:@"route_privacy"];
   
    [route_dict setObject:[Save_Ride_Dict valueForKey:ride_table_end_adress] forKey:@"route_end_address"];
    
    [route_dict setObject:[_completed_Ride_route_dict valueForKey:route_table_gpx_url] forKey:@"route_gpx_file"];
    [route_dict setObject:[_completed_Ride_route_dict valueForKey:route_table_comments] forKey:@"comment"];
    [route_dict setObject:[_completed_Ride_route_dict valueForKey:route_table_ratings] forKey:@"rating"];
    [route_dict setObject:[_completed_Ride_route_dict valueForKey:route_table_maxElevation]
                   forKey:@"route_max_elevation"];
    
    [route_dict setObject:[_completed_Ride_route_dict valueForKey:route_table_minElevation] forKey:@"route_min_elevation"];
    
    [route_dict setObject:[Save_Ride_Dict valueForKey:ride_table_start_lat] forKey:@"route_start_latitude"];
    [route_dict setObject:[Save_Ride_Dict valueForKey:ride_table_start_long] forKey:@"route_start_longitude"];
    [route_dict setObject:[Save_Ride_Dict valueForKey:ride_table_end_lat] forKey:@"route_end_latitude"];
    [route_dict setObject:[Save_Ride_Dict valueForKey:ride_table_end_long]
                   forKey:@"route_end_longitude"];
    
    [ride_dict setObject:[Save_Ride_Dict valueForKey:ride_table_id] forKey:@"existing_ride_id"];
    [ride_dict setObject:[Save_Ride_Dict valueForKey:ride_table_name] forKey:@"ride_title"];
    [ride_dict setObject:[Save_Ride_Dict valueForKey:ride_table_description]  forKey:@"ride_description"];
    
    [ride_dict setObject:[Save_Ride_Dict valueForKey:ride_table_start_adress] forKey:@"ride_start_address"];
    [ride_dict setObject:[Save_Ride_Dict valueForKey:ride_table_end_adress]  forKey:@"ride_end_address"];
    [ride_dict setObject:[Save_Ride_Dict valueForKey:ride_table_start_lat] forKey:@"ride_start_latitude"];
    [ride_dict setObject:[Save_Ride_Dict valueForKey:ride_table_start_long]  forKey:@"ride_start_longitude"];
    [ride_dict setObject:[Save_Ride_Dict valueForKey:ride_table_end_lat] forKey:@"ride_end_latitude"];
    [ride_dict setObject:[Save_Ride_Dict valueForKey:ride_table_end_long]  forKey:@"ride_end_longitude"];
    [ride_dict setObject:[Save_Ride_Dict valueForKey:ride_table_type] forKey:@"ride_type"];
    [ride_dict setObject:[Save_Ride_Dict valueForKey:ride_table_time] forKey:@"ride_time"];
    [ride_dict setObject:[Save_Ride_Dict valueForKey:ride_table_totlaTime] forKey:@"ride_total_time"];
    [ride_dict setObject:[Save_Ride_Dict valueForKey:ride_table_dflt_marker_dist] forKey:@"ride_default_marker_distance"];
    [ride_dict setObject:[Save_Ride_Dict valueForKey:ride_table_avgSpeed] forKey:@"avg_speed"];
    [ride_dict setObject:[Save_Ride_Dict valueForKey:ride_table_totalDist] forKey:@"total_distance"];
    [ride_dict setObject:[Save_Ride_Dict valueForKey:ride_table_start_date] forKey:@"ride_start_date"];
    [ride_dict setObject:[Save_Ride_Dict valueForKey:ride_table_start_time] forKey:@"ride_start_time"];
    [ride_dict setObject:[Save_Ride_Dict valueForKey:ride_table_end_date] forKey:@"ride_end_date"];
    [ride_dict setObject:[Save_Ride_Dict valueForKey:ride_table_end_time] forKey:@"ride_end_time"];
    [ride_dict setObject:[Save_Ride_Dict valueForKey:ride_table_maxSpeed] forKey:@"max_speed"];
    [ride_dict setObject:[Save_Ride_Dict valueForKey:ride_table_randomNumber] forKey:@"random_number"];
    if ([call_For isEqualToString:@""])
    {
        [ride_dict setObject:@"end_ride" forKey:@"ride_status"];
    }
    else
    {
        [ride_dict setObject:@"quit_ride" forKey:@"ride_status"];
    }
    
    NSArray *image_array=[Save_Ride_Dict valueForKey:ride_table_rideImags];
    NSMutableArray *ride_images_arry=[[NSMutableArray alloc]init];
    if (image_array.count>0) {
        for (NSString *filepath in image_array) {
            NSURL *url = [[NSURL alloc] initFileURLWithPath:filepath];
            NSString *path = [url path];
            NSData *data = [[NSFileManager defaultManager] contentsAtPath:path];
            NSString *  base64String = [data base64EncodedStringWithOptions:0];
            [ride_images_arry addObject:base64String];
        }
        [ride_dict setObject:ride_images_arry forKey:@"ride_images"];
    }
    else
    {
        [ride_dict setObject:ride_images_arry forKey:@"ride_images"];
    }
    
    [endride_dict setObject:route_dict forKey:@"route"];
    [endride_dict setObject:ride_dict forKey:@"ride"];
    
    // NSLog(@"End ride api %@",endride_dict);
    NSString*ride_Name_str=[Save_Ride_Dict valueForKey:ride_table_name];
    [SHARED_API endRideRequestsWithParams:endride_dict withSuccess:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           if ([call_For isEqualToString:@""]) {
                               [self api_Sucsess:response ride_id_local:loc_rid :ride_Name_str];
                           }
                           else//for Quit Rides
                           {
                               if (![[response valueForKey:STATUS]isEqualToString:SUCCESS]) {
                                   _is_Running_Background_Sync=@"NO";
                                   return ;
                               }
                               if ([self Deleted_Saved_Ride_Details_in_AllTables:Rides_Table loc_ride_id:loc_rid]) {
                                   [self Deleted_Saved_Ride_Details_in_AllTables:Routes_Table loc_ride_id:loc_rid];
                                   [self Deleted_Saved_Ride_Details_in_AllTables:Locations_Table loc_ride_id:loc_rid];
                                   [self Deleted_Saved_Ride_Details_in_AllTables:Waypoints_Table loc_ride_id:loc_rid];
                                   [self Deleted_Saved_Ride_Details_in_AllTables:Add_places_Table loc_ride_id:loc_rid];
                               }
                               [self Start_Syncing];
                           }
                       });
        
    } onfailure:^(NSError *theError) {
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           NSLog(@"error %@",theError);
                           if ([call_For isEqualToString:@""]) {
                               //Update RideStatus As "Saved" it means gpxurl getting but api failed
                               [self Rides_Table_status_Update_Method];
                           }
                           _is_Running_Background_Sync=@"NO";
                       });
    }];
    
}


-(void)api_Sucsess :(NSDictionary *)response  ride_id_local:(NSInteger)ride_id :(NSString *)rideName{
    
    if ([[response valueForKey:STATUS] isEqualToString:SUCCESS]) {
        
        if ([self Deleted_Saved_Ride_Details_in_AllTables:Rides_Table loc_ride_id:ride_id]) {
            [self Deleted_Saved_Ride_Details_in_AllTables:Routes_Table loc_ride_id:ride_id];
            [self Deleted_Saved_Ride_Details_in_AllTables:Locations_Table loc_ride_id:ride_id];
            [self Deleted_Saved_Ride_Details_in_AllTables:Waypoints_Table loc_ride_id:ride_id];
            [self Deleted_Saved_Ride_Details_in_AllTables:Add_places_Table loc_ride_id:ride_id];
        }
        
//        [SHARED_HELPER showAlert:[NSString stringWithFormat:@"Your Ride(%@) was sucsessfully saved on background.",[response valueForKey:@"ride_id"]]];
        
        UILocalNotification *notification = [[UILocalNotification alloc] init];
        notification.fireDate = [NSDate dateWithTimeIntervalSinceNow:0];
        NSString*rideNameStr=[NSString stringWithFormat:@"Your ride \"%@\" is successfully saved to cloud",rideName];
        notification.alertBody = rideNameStr;
        notification.timeZone = [NSTimeZone defaultTimeZone];
        notification.soundName = UILocalNotificationDefaultSoundName;
        notification.repeatInterval=0;
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
        

       
        [self Start_Syncing];
    }
    else
    {
        _is_Running_Background_Sync=@"NO";
        //Update RideStatus As "Saved" it means gpxurl getting but api failed
        [self Rides_Table_status_Update_Method];
    }
}


#pragma mark - retriving and updating and deleting  From CoreData

-(NSArray *)get_locations_and_default_Markers_data_from_DB :(NSString *)entity_name
{
    NSManagedObjectContext *myContext = [self managedObjectContext];
    NSFetchRequest *req=[NSFetchRequest fetchRequestWithEntityName:entity_name];
    NSNumber *stdUserNumber = [NSNumber numberWithInteger:ride_id_local_DB];
    req.predicate=[NSPredicate predicateWithFormat:@"local_rid == %@",stdUserNumber];
    NSArray *result=[myContext executeFetchRequest:req error:nil];
    return result;
}



-(NSMutableArray *)gettingAddPlacesFromCoreData:(NSInteger )ride_loc
{
    NSError *error = nil;
    NSManagedObjectContext *myContext = [self managedObjectContext];
    NSFetchRequest *fetchLLObjects = [[NSFetchRequest alloc] init];
    [fetchLLObjects setEntity:[NSEntityDescription entityForName:Add_places_Table inManagedObjectContext:myContext]];
    NSNumber *stdUserNumber = [NSNumber numberWithInteger:ride_loc];
    fetchLLObjects.predicate=[NSPredicate predicateWithFormat:@"local_rid ==%@",stdUserNumber];
    NSMutableArray * result = [[myContext executeFetchRequest:fetchLLObjects error:&error] mutableCopy];
    NSMutableArray *results_array=[[NSMutableArray alloc]init];
    for (unsigned short int i=0;i<result.count;i++)
    {
        NSManagedObject *fetchResult=[result  objectAtIndex:i];
        NSMutableDictionary *coreDataDict=[[NSMutableDictionary alloc] init];
        [coreDataDict setValue:[NSString stringWithFormat:@"%@",[fetchResult valueForKey:@"lat"]] forKey:@"AddAPlace_lat"];
        [coreDataDict setValue:[NSString stringWithFormat:@"%@",[fetchResult valueForKey:@"longitude"]] forKey:@"AddAPlace_long"];
        [coreDataDict setValue:[fetchResult valueForKey:@"des"] forKey:@"AddAPlace_desc"];
        [coreDataDict setValue:[fetchResult valueForKey:@"title"] forKey:@"AddAPlace_Title"];
        [coreDataDict setValue:[NSString stringWithFormat:@"%@",[fetchResult valueForKey:@"img_count"]] forKey:@"img_count"];
        [coreDataDict setValue:[fetchResult valueForKey:@"time"] forKey:@"AddAPlace_Time"];
        NSString *paths=[fetchResult valueForKey:@"img_paths"];
        if (paths.length>0) {
            NSArray *img_urls=[paths componentsSeparatedByString:@","];
            [coreDataDict setValue:img_urls forKey:@"img_paths"];
        }
        else
        {
            NSMutableArray *empty=[[NSMutableArray alloc]init];
            [coreDataDict setValue:empty forKey:@"img_paths"];
        }
        
        NSString *urls=[fetchResult valueForKey:@"img_urls"];
        if (urls.length>0) {
            NSArray *img_urls=[urls componentsSeparatedByString:@","];
            [coreDataDict setValue:img_urls forKey:@"img_urls"];
        }
        else
        {
            NSMutableArray *empty=[[NSMutableArray alloc]init];
            [coreDataDict setValue:empty forKey:@"img_urls"];
        }
        
        [coreDataDict setValue:[fetchResult valueForKey:@"status"] forKey:@"status"];
        [coreDataDict setValue:[fetchResult valueForKey:@"places_id"] forKey:@"places_id"];
        [coreDataDict setValue:[fetchResult valueForKey:@"local_rid"] forKey:@"local_rid"];
        
        [results_array addObject:coreDataDict];
    }
    return results_array;
}

-(void)Update_AddPlace_Table  :(NSInteger)id_is amazon_url:(NSString *)image_url{
    NSManagedObjectContext *myContext = [self managedObjectContext];
    NSFetchRequest *req=[NSFetchRequest fetchRequestWithEntityName:Add_places_Table];
    NSNumber *stdUserNumber = [NSNumber numberWithInteger:id_is];
    req.predicate=[NSPredicate predicateWithFormat:@"places_id == %@",stdUserNumber];
    NSArray *places=[myContext executeFetchRequest:req error:nil];
    
    if (places.count>0) {
    AddPlace *place=[places objectAtIndex:0];
        
    NSString *urls=place.img_urls;
    NSString *paths=place.img_paths;
    int16_t img_count=place.img_count;
    
    NSArray *img_urls_array=[urls componentsSeparatedByString:@","];
    NSArray *img_paths_array=[paths componentsSeparatedByString:@","];
    NSMutableArray *urls_mut_array=[[NSMutableArray alloc]init];
    if (img_count>0) {
        [urls_mut_array addObjectsFromArray:img_urls_array];
        NSString *url=[urls_mut_array objectAtIndex:0];
        if (url.length>0) {
            
        }
        else
        {
            [urls_mut_array removeObjectAtIndex:0];
        }
        [urls_mut_array addObject:image_url];
        urls=[urls_mut_array componentsJoinedByString:@","];
        place.img_urls=urls;
    }
    else{
        place.img_urls=image_url;
    }
        
    if (img_paths_array.count<=urls_mut_array.count) {
        place.status=table_status_complete;
    }
    else
    {
        
    }
    [place.managedObjectContext save:nil];
    }
}
-(void)Rides_Table_status_Update_Method
{
    NSError *error;
    NSFetchRequest * fr = [[NSFetchRequest alloc]initWithEntityName:Rides_Table];
    NSNumber *stdUserNumber = [NSNumber numberWithInteger:ride_id_local_DB];
    fr.predicate=[NSPredicate predicateWithFormat:@"local_rid ==%@",stdUserNumber];
    NSMutableArray * result = [[self.managedObjectContext executeFetchRequest:fr error:&error] mutableCopy];
    if (result.count>0) {
        Rides *rides=[result objectAtIndex:0];
        rides.ride_status=table_status_save;
        [rides.managedObjectContext save:nil];
    }
}
-(void)Routes_Table_Update_Method :(NSString *)gpx_url  created_xml_str:(NSString *)xml_String
{
    NSError *error;
    NSFetchRequest * fr = [[NSFetchRequest alloc]initWithEntityName:Routes_Table];
    NSNumber *stdUserNumber = [NSNumber numberWithInteger:ride_id_local_DB];
    fr.predicate=[NSPredicate predicateWithFormat:@"local_rid ==%@",stdUserNumber];
    NSMutableArray * result = [[self.managedObjectContext executeFetchRequest:fr error:&error] mutableCopy];
    if (result.count>0) {
        Routes *routes=[result objectAtIndex:0];
        routes.gpx_url=gpx_url;
        routes.gpx_file=xml_String;
        [routes.managedObjectContext save:nil];
    }
}

-(NSMutableDictionary *)get_complete_ride_Route_From_DB :(NSInteger)ride_id{
    
    NSManagedObjectContext *myContext = [self managedObjectContext];
    NSFetchRequest *req=[NSFetchRequest fetchRequestWithEntityName:Routes_Table];
    NSNumber *stdUserNumber = [NSNumber numberWithInteger:ride_id];
    req.predicate=[NSPredicate predicateWithFormat:@"local_rid == %@",stdUserNumber];
    NSArray *route=[myContext executeFetchRequest:req error:nil];
    NSMutableDictionary *route_dict=[[NSMutableDictionary alloc]init];
    if (route.count>0) {
        Routes *object=[route objectAtIndex:0];
        [route_dict setValue:[NSString stringWithFormat:@"%lld",object.local_rid] forKey:all_table_local_rid];
         [route_dict setValue:object.route_id forKey:route_table_id];
         [route_dict setValue:object.route_name forKey:route_table_name];
         [route_dict setValue:object.route_desc forKey:route_table_description];
         [route_dict setValue:object.route_privacy forKey:route_table_privacy];
         [route_dict setValue:object.route_comments forKey:route_table_comments];
         [route_dict setValue:object.route_ratings forKey:route_table_ratings];
         [route_dict setValue:object.gpx_url forKey:route_table_gpx_url];
         [route_dict setValue:object.existing_gpx_url forKey:route_table_exist_gpx_url];
         [route_dict setValue:object.gpx_file forKey:route_table_gpx_file];
         [route_dict setValue:object.maxElevation forKey:route_table_maxElevation];
         [route_dict setValue:object.minElevation forKey:route_table_minElevation];
         [route_dict setValue:object.route_owner_user_id forKey:route_table_route_owner_id];
    }
    return route_dict;
}

-(NSMutableArray *)get_Completed_Rides_From_DB{
   
    NSManagedObjectContext *myContext = [self managedObjectContext];
    NSFetchRequest *req=[NSFetchRequest fetchRequestWithEntityName:Rides_Table];
    NSArray *ride=[myContext executeFetchRequest:req error:nil];
    NSMutableArray *rides_results_array=[[NSMutableArray alloc]init];
    for (Rides *object in ride) {
        if ([object.ride_status isEqualToString:table_status_save]||[object.ride_status isEqualToString:table_status_complete]||[object.ride_status isEqualToString:table_status_quit]) {
            NSMutableDictionary *ride_Detail_dict=[[NSMutableDictionary alloc]init];
            [ride_Detail_dict setValue:[NSString stringWithFormat:@"%lld",object.local_rid] forKey:all_table_local_rid];
            [ride_Detail_dict setValue:object.user_id forKey:ride_table_user_id];
            [ride_Detail_dict setValue:object.ride_owner_user_id forKey:ride_table_ride_owner_id];
            [ride_Detail_dict setValue:object.ride_id forKey:ride_table_id];
            [ride_Detail_dict setValue:object.ride_status forKey:ride_table_ride_Status];
            [ride_Detail_dict setValue:object.ride_name forKey:ride_table_name];
            [ride_Detail_dict setValue:object.ride_type forKey:ride_table_type];
            [ride_Detail_dict setValue:object.ride_descr forKey:ride_table_description];
            [ride_Detail_dict setValue:object.ride_time forKey:ride_table_time];
            [ride_Detail_dict setValue:object.ride_start_date forKey:ride_table_start_date];
            [ride_Detail_dict setValue:object.ride_start_time forKey:ride_table_start_time];
            [ride_Detail_dict setValue:object.ride_end_date forKey:ride_table_end_date];
            [ride_Detail_dict setValue:object.ride_end_time forKey:ride_table_end_time];
            [ride_Detail_dict setValue:object.rides_count forKey:ride_table_riders_count];
            [ride_Detail_dict setValue:object.ride_start_adress forKey:ride_table_start_adress];
            [ride_Detail_dict setValue:object.ride_end_adress forKey:ride_table_end_adress];
            NSString *ride_imgs=object.ride_images;
            if (ride_imgs.length>0) {
                NSArray *imgs=[ride_imgs componentsSeparatedByString:@","];
                [ride_Detail_dict setValue:imgs forKey:ride_table_rideImags];
            }
            else
            {
                NSMutableArray *empty=[[NSMutableArray alloc]init];
            [ride_Detail_dict setValue:empty forKey:ride_table_rideImags];
            }
            [ride_Detail_dict setValue:object.ride_total_time forKey:ride_table_totlaTime];
            [ride_Detail_dict setValue:object.ride_time forKey:ride_table_time];
            [ride_Detail_dict setValue:object.totalDistance forKey:ride_table_totalDist];
            [ride_Detail_dict setValue:object.avgSpeed forKey:ride_table_avgSpeed];
            [ride_Detail_dict setValue:object.maxSpeed forKey:ride_table_maxSpeed];
            [ride_Detail_dict setValue:object.ride_waypoints forKey:ride_table_dflt_marker_dist];
            [ride_Detail_dict setValue:object.randonNumber forKey:ride_table_randomNumber];
            [ride_Detail_dict setValue:[NSString stringWithFormat:@"%f",object.ride_start_lat] forKey:ride_table_start_lat];
            [ride_Detail_dict setValue:[NSString stringWithFormat:@"%f",object.ride_start_long] forKey:ride_table_start_long];
            [ride_Detail_dict setValue:[NSString stringWithFormat:@"%f",object.ride_end_lat] forKey:ride_table_end_lat];
            [ride_Detail_dict setValue:[NSString stringWithFormat:@"%f",object.ride_end_long] forKey:ride_table_end_long];
            
            [rides_results_array addObject:ride_Detail_dict];
        }
    }
    return rides_results_array;
}

- (BOOL)Deleted_Saved_Ride_Details_in_AllTables:(NSString *)entity loc_ride_id:(NSInteger )rid
{
    NSError *error = nil;
    NSManagedObjectContext *myContext = [self managedObjectContext];
    NSFetchRequest *fetchLLObjects = [[NSFetchRequest alloc] init];
    
    [fetchLLObjects setEntity:[NSEntityDescription entityForName:entity inManagedObjectContext:myContext]];
    NSNumber *stdUserNumber = [NSNumber numberWithLongLong:rid];
    fetchLLObjects.predicate=[NSPredicate predicateWithFormat:@"local_rid ==%@",stdUserNumber];
    [fetchLLObjects setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSArray *allObjects = [myContext executeFetchRequest:fetchLLObjects error:&error];
    for (NSManagedObject *object in allObjects) {
        [myContext deleteObject:object];
    }
    
    if ([myContext save:&error]) {
        NSLog(@"Empty saved  ");
        return YES;
    }
    else
    {
        NSLog(@"Empty not saved  ");
        return NO;
    }
}
#pragma mark - Annotations Stop
-(void)stop_Annotations
{
    [Defaults setBool:YES forKey:@"MPCoachMarksShown_CreatePost"];
    [Defaults setBool:YES forKey:@"MPCoachMarksShown_MarketPlace"];
    [Defaults setBool:YES forKey:@"MPCoachMarksShown_SocialShare"];
    [Defaults setBool:YES forKey:@"MPCoachMarksShown_Dashboard"];
    [Defaults setBool:YES forKey:@"MPCoachMarksShown_RideDashboard"];
    [Defaults setBool:YES forKey:@"MPCoachMarksShown_RideToDashboard"];
    [Defaults setBool:YES forKey:@"MPCoachMarksShown_SaveYourRoute"];
    [Defaults setBool:YES forKey:@"MPCoachMarksShown_AlmostDone"];
    [Defaults setBool:YES forKey:@"MPCoachMarksShown_MyRides"];
    [Defaults setBool:YES forKey:@"MPCoachMarksShown_CreateRide"];
    [Defaults setBool:YES forKey:@"MPCoachMarksShown_StartRide"];
    [Defaults setBool:YES forKey:@"MPCoachMarksShown_Routes"];
    [Defaults setBool:YES forKey:@"MPCoachMarksShown_CreateGroup"];
    [Defaults setBool:YES forKey:@"MPCoachMarksShown_GroupsView"];
    [Defaults setBool:YES forKey:@"MPCoachMarksShown_FriendsAndGroups"];
    [Defaults setBool:YES forKey:@"MPCoachMarksShown_CreateEvent"];
    [Defaults setBool:YES forKey:@"MPCoachMarksShown_MyEvent"];
    [Defaults setBool:YES forKey:@"MPCoachMarksShown_Event"];
    [Defaults setBool:YES forKey:@"MPCoachMarksShown_Events"];
    [Defaults setBool:YES forKey:@"MPCoachMarksShown_FindRiders"];
    [Defaults setBool:YES forKey:@"MPCoachMarksShown_BrowseRiders"];
    
    [Defaults synchronize];
}

#pragma mark - App Short Cuts
- (void)configDynamicShortcutItems {
    
    // config image shortcut items
    // if you want to use custom image in app bundles, use iconWithTemplateImageName method
    UIApplicationShortcutIcon *shortcutAddIcon = [UIApplicationShortcutIcon iconWithType:UIApplicationShortcutIconTypeAdd];
    //    UIApplicationShortcutIcon *shortcutFavoriteIcon = [UIApplicationShortcutIcon iconWithType:UIApplicationShortcutIconTypeShare];
    
    UIApplicationShortcutIcon *icon1 = [UIApplicationShortcutIcon iconWithType:UIApplicationShortcutIconTypeCaptureVideo];
    UIApplicationShortcutIcon *icon2 = [UIApplicationShortcutIcon iconWithType:UIApplicationShortcutIconTypeDate];
    
    UIApplicationShortcutItem *shortcutSearch = [[UIApplicationShortcutItem alloc]
                                                 initWithType:@"com.eis.openroadrides.Facebook"
                                                 localizedTitle:@"Facebook"
                                                 localizedSubtitle:nil
                                                 icon:icon1
                                                 userInfo:nil];
    
    UIApplicationShortcutItem *shortcutFavorite = [[UIApplicationShortcutItem alloc]
                                                   initWithType:@"com.eis.openroadrides.Google"
                                                   localizedTitle:@"Google"
                                                   localizedSubtitle:nil
                                                   icon:icon2
                                                   userInfo:nil];
    
    UIApplicationShortcutItem *shortcutAdd = [[UIApplicationShortcutItem alloc]
                                              initWithType:@"com.eis.openroadrides.Create new user"
                                              localizedTitle:@"Create new user"
                                              localizedSubtitle:nil
                                              icon:shortcutAddIcon
                                              userInfo:nil];
    
    
    // add all items to an array
    NSArray *items = @[shortcutSearch, shortcutFavorite,shortcutAdd];
    
    // add the array to our app
    [UIApplication sharedApplication].shortcutItems = items;
}



@end
