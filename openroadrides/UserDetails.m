//
//  UserDetails.m
//  Thredz
//
//  Created by Jyothsna on 7/8/16.
//  Copyright © 2016 ebiz. All rights reserved.
//

#import "UserDetails.h"

@implementation UserDetails


-(id)initWithDictionary:(NSDictionary*)d error:(NSError *__autoreleasing *)err
{
    self = [super initWithDictionary:d error:err];
    if (self) {
        
    }
    return self;
}
-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    if (self) {
        
        self.user_id    = [aDecoder decodeObjectForKey:@"user_id"];
        self.user_name   = [aDecoder decodeObjectForKey:@"first_name"];
        self.user_email      = [aDecoder decodeObjectForKey:@"email"];
       
    }
    return self;
    
}

-(void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject: self.user_id        forKey:@"user_id"];
    [aCoder encodeObject: self.user_name      forKey:@"first_name"];
    [aCoder encodeObject: self.user_email     forKey:@"email"];
}

@end
