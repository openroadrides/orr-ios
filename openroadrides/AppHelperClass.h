//
//  AppHelper.h
//  Thredz
//
//  Created by Jyothsna on 7/11/16.
//  Copyright © 2016 ebiz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"
#import "UserDetails.h"
@interface AppHelperClass : NSObject
+(AppHelperClass *)appsharedInstance;

@property (nonatomic, readwrite) BOOL internetAvailable,isLoggedin;
@property (nonatomic,retain) Reachability *internetReachability;
@property (nonatomic, retain) UserDetails *userDetails;


-(void)showValidationsAlertBanner:(NSString *)message withTitle:(NSString *)title;
-(void)showNetworkAlertBanner;
-(BOOL)checkEmailValidations:(NSString *)email;
-( NSString*) mimeTypeFromFileExt:(NSString* )fileName;
-(BOOL)checkIfisInternetAvailable;
-(void)showInternetAlertBanner;
-(void)hideBanner;
-(void)hideValidataionAlertBanner;
-(void)showNetworkAlertBannerWithErrorMessage:(NSString *)errorMessage;
-(NSString *)getDeviceUUID;
-(NSString *)getAppBuildVersion;
-(UserDetails *)getStoredUser;
-(void)showAlert:(NSString *)message;

//ride type
@property (strong, nonatomic) NSString *rideType,*sidemenu_frnds_count,*sidemenu_groups_count;
- (NSString *) platformType:(NSString *)platform;


@end
