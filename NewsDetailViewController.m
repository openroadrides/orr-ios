//
//  NewsDetailViewController.m
//  openroadrides
//
//  Created by apple on 30/05/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "NewsDetailViewController.h"
#import "Constants.h"
#import "APIHelper.h"
#import "AppHelperClass.h"
@interface NewsDetailViewController (){
    NSDictionary *news_details_dict;
    UIBarButtonItem *item0;
}

@end

@implementation NewsDetailViewController

- (void)viewDidLoad {
    
    
    
    
    self.title = @"NEWS";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    
    self.newsDetail_scrollLabel.hidden=YES;
    self.newsDetail_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.newsDetail_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.newsDetail_scrollLabel.textColor = [UIColor blackColor];
    self.newsDetail_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.newsDetail_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.newsDetail_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.newsDetail_scrollLabel.scrollSpeed = 60; // pixels per second
    self.newsDetail_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.newsDetail_scrollLabel.fadeLength = 0.f;
    self.newsDetail_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.newsDetail_scrollLabel observeApplicationNotifications];
    
    
    self.newsDetail_rideGoingBtn.hidden=YES;
    if (appDelegate.ridedashboard_home) {
        
        self.newsDetail_scrollLabel.hidden=NO;
        self.newsDetail_rideGoingBtn.hidden=NO;
    }
    _content_view.hidden = YES;
    
    
#pragma mark - Bar buttons on nav bar.....
    
    
    
    UIBarButtonItem *Share_btn=[[UIBarButtonItem alloc]initWithImage:
                              [[UIImage imageNamed:@"share"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
                                                             style:UIBarButtonItemStylePlain target:self action:@selector(shareclicked)];
    self.navigationItem.rightBarButtonItem=Share_btn;
    
    
//    UIBarButtonItem *back_btn=[[UIBarButtonItem alloc]initWithImage:
//                                 [[UIImage imageNamed:@"sidemenuicon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
//                                                                style:UIBarButtonItemStylePlain target:self action:@selector(back_Action)];
//    UIBarButtonItem *back_btn=[[UIBarButtonItem alloc]initWithTitle:@"<" style:UIBarButtonItemStylePlain target:self action:@selector(back_Action)];
//    [back_btn setTitleTextAttributes:@{
//                                       NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:26.0],
//                                       NSForegroundColorAttributeName: [UIColor blackColor]
//                                       } forState:UIControlStateNormal];
//    
//    self.navigationItem.leftBarButtonItem = back_btn;
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [leftBtn addTarget:self action:@selector(back_Action:) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.frame = CGRectMake(0, 0, 25, 25);
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"back_Image"] forState:UIControlStateNormal];
    UIBarButtonItem *leftBackButton=[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    item0=leftBackButton;
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:item0, nil];
    

    [self  get_newsdetail_list];
    
    [super viewDidLoad];
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(void)shareclicked{
    
        NSString *Description = _description_lbl.text;
        NSString *Title =_title_lbl.text ;
        NSURL *Image_url = [NSURL URLWithString:_news_image_share_string];
        UIImage *aImage;
        aImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:Image_url]];
        NSArray *objectsToShare = @[_news_detail_img.image, Title, Description];
        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
        NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                       UIActivityTypePrint,
                                       UIActivityTypeAssignToContact,
                                       UIActivityTypeSaveToCameraRoll,
                                       UIActivityTypeAddToReadingList,
                                       UIActivityTypePostToFlickr,
                                       UIActivityTypePostToVimeo];
    
        activityVC.excludedActivityTypes = excludeActivities;
    
        [self presentViewController:activityVC animated:YES completion:nil];
//
    
}
-(void)back_Action:(UIButton *)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)photoForview:(NSString *)user
{
    NSURL *url = [NSURL URLWithString:user];
    [self downloadImageWithURL:url completionBlock:^(BOOL succeeded, UIImage *image) {
        if (succeeded) {
            
            if (image==nil) {
                //cell.userImageView.image=[UIImage imageNamed:@"userLogo.png"];
            }
            else{
                _news_detail_img.image=image;
                
            }
            
        }
    }];
    
}
- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES,image);
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
}



-(void)get_newsdetail_list{
    
    
    
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }

    
    
    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    activeIndicatore.hidesWhenStopped = TRUE;
    [activeIndicatore startAnimating];
    [self.view addSubview:activeIndicatore];

    [SHARED_API newsdetailRequestsWithParams:_news_id withSuccess:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^
            {
                
               
                  NSLog(@"responce is %@",response);
                
               
                 
                   news_details_dict = [response valueForKey:@"news_details"];
                NSString *date_str = [NSString stringWithFormat:@"%@",[news_details_dict valueForKey:@"news_date"]]; /// here this is your date with format yyyy-MM-dd
                
                NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
                [dateFormatter1 setDateFormat:@"yyyy-MM-dd"]; //// here set format of date which is in your output date (means above str with format)
                
                NSDate *date = [dateFormatter1 dateFromString: date_str]; // here you can fetch date from string with define format
                
                dateFormatter1 = [[NSDateFormatter alloc] init];
                [dateFormatter1 setDateFormat:@"MMMM yyyy"];// here set format which you want...
                NSString *convertedString = [dateFormatter1 stringFromDate:date]; //here convert date in NSString
                NSLog(@"Converted String : %@",convertedString);
                
                 NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                   [dateFormatter setDateFormat:@"dd"];
                 NSString *convertedString1 = [dateFormatter stringFromDate:date];
                
                
                _title_lbl.text = [news_details_dict valueForKey:@"news_title"];
                _description_lbl.text = [news_details_dict valueForKey:@"news_description"];
                _day_lbl.text = [NSString stringWithFormat:@"%@",convertedString1];
                _mounth_lbl.text = [NSString stringWithFormat:@"%@",convertedString];
                NSString *news_cover_image_string=[NSString stringWithFormat:@"%@",[news_details_dict valueForKey:@"news_image_2_1"]];
                if ([news_cover_image_string isEqualToString:@"<null>"] || [news_cover_image_string isEqualToString:@""] || [news_cover_image_string isEqualToString:@"null"] || news_cover_image_string == nil) {
                    _news_image_share_string=[NSString stringWithFormat:@""];
                }
                else{
                     _news_image_share_string=[NSString stringWithFormat:@"%@",[news_details_dict valueForKey:@"news_image_2_1"]];
                }

                
                
                
                [self photoForview:[news_details_dict valueForKey:@"news_image_2_1"]];
                
                if ([activeIndicatore isAnimating]) {
                    [activeIndicatore stopAnimating];
                     _content_view.hidden = NO;
                    [activeIndicatore removeFromSuperview];
                }
                
                
                
                
                
        });
        
    } onfailure:^(NSError *theError) {
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           
                           [SHARED_HELPER showAlert:ServiceFail];
                           
                           if ([activeIndicatore isAnimating]) {
                               [activeIndicatore stopAnimating];
                               [activeIndicatore removeFromSuperview];
                           }
                           

                           
                       });
    }];
}


- (IBAction)onClick_newsDetail_rideGoingBtn:(id)sender {
    
    self.newsDetail_scrollLabel.hidden=YES;
    self.newsDetail_rideGoingBtn.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }
}
@end
