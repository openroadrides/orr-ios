//
//  CreatePostView.m
//  openroadrides
//
//  Created by apple on 05/10/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "CreatePostView.h"
#import "MPCoachMarks.h"

@interface CreatePostView ()
{
    int scrollWidth ;
    UIImageView *img;
    int x_axis;
    UIView *baseView_placesImages;
    NSMutableArray *img_array;
    int index,imgIndex,x;
    NSString *location_str;
    double start_latitude,start_longitude;
}
@end

@implementation CreatePostView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"CREATE POST";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    UIButton *saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [saveBtn addTarget:self action:@selector(save_clicked) forControlEvents:UIControlEventTouchUpInside];
    saveBtn.frame = CGRectMake(0, 0, 30, 30);
    [saveBtn setBackgroundImage:[UIImage imageNamed:@"SAVE"] forState:UIControlStateNormal];
    UIBarButtonItem * create_Ride= [[UIBarButtonItem alloc] initWithCustomView:saveBtn];
    self.navigationItem.rightBarButtonItems=[NSArray arrayWithObjects:create_Ride, nil];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [leftBtn addTarget:self action:@selector(cancle_clicked:) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.frame = CGRectMake(0, 0, 25, 25);
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"close_icon"] forState:UIControlStateNormal];
    UIBarButtonItem *leftBackButton=[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    item0=leftBackButton;
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:item0, nil];
    
    
    self.createPostView_scrollLabel.hidden=YES;
    self.createPostView_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.createPostView_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.createPostView_scrollLabel.textColor = [UIColor blackColor];
    self.createPostView_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.createPostView_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.createPostView_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.createPostView_scrollLabel.scrollSpeed = 60; // pixels per second
    self.createPostView_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.createPostView_scrollLabel.fadeLength = 0.f;
    self.createPostView_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.createPostView_scrollLabel observeApplicationNotifications];

    
    self.createPostView_rideGoingBtn.hidden=YES;
    self.bottomContraint_createPostImagesView.constant=-30;
    if (appDelegate.ridedashboard_home) {
        self.createPostView_scrollLabel.hidden=NO;
        self.bottomContraint_createPostImagesView.constant=0;
        self.createPostView_rideGoingBtn.hidden=NO;
    }

    [self.createPostTitleTF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.createPostTypeTF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.createPostPriceTF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.createPostAddressTF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.createPostPhoneNumberTF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.createPostEmailTF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.createPostDescriptionTextView setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    
    
    [self MaeketPlace_Category_Service];
    
    index = 1;
    imgIndex = 1;
    x=0;
    img_array = [[NSMutableArray alloc]init];
    self.multipleImages_scrollview.showsHorizontalScrollIndicator = NO;
    add_btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 15, 90,90)];
    [add_btn addTarget:self action:@selector(add_click:) forControlEvents:UIControlEventTouchUpInside];
    //    add_btn.backgroundColor = [UIColor greenColor];
    [add_btn setBackgroundImage:[UIImage imageNamed:plus_icon] forState:UIControlStateNormal];
    [self.multipleImages_scrollview addSubview:add_btn];
    
    self.createPostTitleTF.delegate=self;
    self.createPostTypeTF.delegate=self;
    self.createPostPriceTF.delegate=self;
    self.createPostAddressTF.delegate=self;
    self.createPostPhoneNumberTF.delegate=self;
    self.createPostEmailTF.delegate=self;
    self.createPostDescriptionTextView.delegate=self;
    
//    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
//    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
//    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
//                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
//                            [[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
//    [numberToolbar sizeToFit];
//    _createPostPriceTF.inputAccessoryView = numberToolbar;
    
    
    
    UIToolbar* numberToolbar1 = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar1.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar1.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad1)],
                             [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                             [[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad1)]];
    [numberToolbar1 sizeToFit];
    _createPostPhoneNumberTF.inputAccessoryView = numberToolbar1;
    
    
    UIToolbar* numberToolbar2 = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar2.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar2.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad2)],
                             [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                             [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad2)]];
    [numberToolbar2 sizeToFit];
    _createPostDescriptionTextView.inputAccessoryView = numberToolbar2;
    
    
    
    
    typer_picker = [[UIPickerView alloc]init];
    typer_picker.dataSource = self;
    typer_picker.delegate = self;
    typer_picker.showsSelectionIndicator = YES;
   
    UIToolbar *toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    [toolbar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *done = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(type_Picker_Done)];
    done.tintColor=[UIColor blackColor];
    UIBarButtonItem *space = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolbar setItems:[NSArray arrayWithObjects:space,done,nil]];
    [_createPostTypeTF setInputAccessoryView:toolbar];
    _createPostTypeTF.inputView = typer_picker;
    
    self.createPostAddressTF.userInteractionEnabled=NO;
   
    //Display Annotation
    // Show coach marks
    BOOL coachMarksShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"MPCoachMarksShown_CreatePost"];
    if (coachMarksShown == NO) {
        // Don't show again
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MPCoachMarksShown_CreatePost"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        // Show coach marks
        [self showAnnotation];
    }
}

#pragma mark - Annotations
-(void)showAnnotation
{
    [self.view layoutIfNeeded];
    
    
    // Setup coach marks
    CGRect coachmark1 = CGRectMake (6,20, self.navigationController.navigationBar.frame.size.height,self.navigationController.navigationBar.frame.size.height);
    CGRect coachmark2 = CGRectMake( ([UIScreen mainScreen].bounds.size.width - 53), 20, self.navigationController.navigationBar.frame.size.height, self.navigationController.navigationBar.frame.size.height);
    CGRect coachmark3 = CGRectMake( _createPost_Images_view.frame.origin.x, _createPost_Images_view.frame.origin.y+23, add_btn.frame.size.width, add_btn.frame.size.height);
    
    
    // Setup coach marks
    NSArray *coachMarks = @[
                            @{
                                @"rect": [NSValue valueWithCGRect:coachmark1],
                                @"caption": @"Tap to close the post",
                                @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                                @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                                @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_LEFT]
                                //@"showArrow":[NSNumber numberWithBool:YES]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:coachmark2],
                                @"caption": @"Tap to save the post",
                                @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                                @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                                @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_RIGHT],
                                //@"showArrow":[NSNumber numberWithBool:YES]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:coachmark3],
                                @"caption": @"Add the post image here",
                                @"shape": [NSNumber numberWithInteger:SHAPE_SQUARE],
                                @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM]
                                
                                },
                            ];
    
    
    MPCoachMarks *coachMarksView = [[MPCoachMarks alloc] initWithFrame:self.navigationController.view.bounds coachMarks:coachMarks];
    [self.navigationController.view addSubview:coachMarksView];
    //[[UIApplication sharedApplication].keyWindow addSubview:coachMarksView];
    [coachMarksView start];
    
}

-(void)cancelNumberPad{
    
   [self.view endEditing:YES];
    
}
-(void)type_Picker_Done{
   
    if ([_createPostTypeTF.text isEqualToString:@""]) {
        _createPostTypeTF.text=[[_arrayOFCreatePostCategories objectAtIndex:0] valueForKey:@"category_name"];
        postCategoryID=[[_arrayOFCreatePostCategories objectAtIndex:0] valueForKey:@"id"];

             self.createPostPriceTF.userInteractionEnabled=YES;
             [_createPostPriceTF becomeFirstResponder];
        self.topConstraint_priceBottomView.constant=0;
        self.createPost_priceImage.hidden=NO;
        self.createPostPriceTF.hidden=NO;

        
    }
    else
    {
        if ([self.createPostTypeTF.text isEqualToString:@"Personal"]) {
            self.createPostPriceTF.userInteractionEnabled=NO;
            [_createPostEmailTF becomeFirstResponder];
            self.createPostPriceTF.text=@"";
            self.topConstraint_priceBottomView.constant=-41;
            self.createPost_priceImage.hidden=YES;
            self.createPostPriceTF.hidden=YES;

        }
        else
        {
            self.createPostPriceTF.userInteractionEnabled=YES;
            [_createPostPriceTF becomeFirstResponder];
            self.topConstraint_priceBottomView.constant=0;
            self.createPost_priceImage.hidden=NO;
            self.createPostPriceTF.hidden=NO;

        }
    }
//    [_createPostPriceTF becomeFirstResponder];
}
-(void)doneWithNumberPad{
    [_createPostEmailTF becomeFirstResponder];
//     [self.view endEditing:YES];
}

-(void)cancelNumberPad1{
    
    [self.view endEditing:YES];
    
}

-(void)doneWithNumberPad1{
    
    [_createPostEmailTF becomeFirstResponder];
}
-(void)cancelNumberPad2
{

    [self.view endEditing:YES];
}
-(void)doneWithNumberPad2{
    

    [self.view endEditing:YES];
}
#pragma mark - Picker View Data source
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component{
    return [_arrayOFCreatePostCategories count];
}

#pragma mark- Picker View Delegate

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:
(NSInteger)row inComponent:(NSInteger)component{
    [_createPostTypeTF setText:[[_arrayOFCreatePostCategories objectAtIndex:row] objectForKey:@"category_name"]];
    postCategoryID=[[_arrayOFCreatePostCategories objectAtIndex:row] objectForKey:@"id"];
    
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:
(NSInteger)row forComponent:(NSInteger)component{
    return [[_arrayOFCreatePostCategories objectAtIndex:row] objectForKey:@"category_name"];
}
#pragma - mark TextField Delegate Methods
-(void)textstartedting
{
   
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -150., self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];
}
-(void)textendedting
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +150., self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];
}
//-(void)textFieldDidEndEditing:(UITextField *)textField
//{
//    [self textendedting];
//}
//- (void)textFieldDidBeginEditing:(UITextField *)textField {
//    
//    if (textField==_createPostEmailTF) {
//         [self textstartedting];
//    }
//    
////    currentTextField = textField;
//    
//   
//}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
     [self textstartedting];
    _descriptionPlaceholder_Label.hidden=YES;
}
-(void)textViewDidEndEditing:(UITextView *)textView
{
     [self textendedting];
    if (_createPostDescriptionTextView.text.length == 0) {
        _descriptionPlaceholder_Label.hidden=NO;
    }
    else
    {
        _descriptionPlaceholder_Label.hidden=YES;
    }
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if (textField== _createPostTitleTF)
    {
        [_createPostTypeTF becomeFirstResponder];
        return NO;
    }
    else if (textField== _createPostTypeTF)
    {
        [_createPostPriceTF becomeFirstResponder];
        return NO;
    }
    else if (textField== _createPostPriceTF)
    {
        [_createPostEmailTF becomeFirstResponder];
        return NO;
    }
    else if (textField== _createPostEmailTF)
    {
//        [_createPostAddressTF becomeFirstResponder];
//        return YES;
        [textField resignFirstResponder];
    }

    else if (textField== _createPostAddressTF)
    {
        [_createPostPhoneNumberTF becomeFirstResponder];
        return NO;
    }
    else if (textField== _createPostPhoneNumberTF)
    {
        [_createPostDescriptionTextView becomeFirstResponder];
        return NO;
    }
    
    [textField resignFirstResponder];
    return YES;
}
-(BOOL)NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == _createPostPhoneNumberTF)
    {
        NSCharacterSet *numSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789-"];
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSInteger charCount = [newString length];
        
        
        //new
        NSString *stringWithoutSpaces = [newString
                                         stringByReplacingOccurrencesOfString:@"-" withString:@""];
        NSInteger newcharCount = [stringWithoutSpaces length];
        if (newcharCount>10) {
            stringWithoutSpaces = [stringWithoutSpaces substringToIndex:[stringWithoutSpaces length]-1];
            NSMutableString *mu = [NSMutableString stringWithString:stringWithoutSpaces];
            [mu insertString:@"-" atIndex:3];
            [mu insertString:@"-" atIndex:7];
            textField.text = mu;
            return NO;
            
        }
        else if (newcharCount==10)
        {
            NSMutableString *mu = [NSMutableString stringWithString:stringWithoutSpaces];
            [mu insertString:@"-" atIndex:3];
            [mu insertString:@"-" atIndex:7];
            textField.text = mu;
            return NO;
        }
            
        if (charCount == 3 || charCount == 7) {
            if ([string isEqualToString:@""]){
                return YES;
            }else{
                newString = [newString stringByAppendingString:@"-"];
            }
        }
        
        if (charCount == 4 || charCount == 8) {
            if (![string isEqualToString:@"-"]){
                newString = [newString substringToIndex:[newString length]-1];
                newString = [newString stringByAppendingString:@"-"];
            }
        }
        
        if ([newString rangeOfCharacterFromSet:[numSet invertedSet]].location != NSNotFound
            || [string rangeOfString:@"-"].location != NSNotFound
            || charCount > 12) {
            return NO;
        }
        
        textField.text = newString;
        return NO;
    }
    
    return YES;
}
//-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
//{
//    if (textView ==_createPostDescriptionTextView) {
//        NSString *newString = [_createPostDescriptionTextView.text stringByReplacingCharactersInRange:range withString:text];
//        return !([newString length] >= 510);
//        return NO;
//        
//    }
//    return YES;
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Category_Type_Service
-(void)MaeketPlace_Category_Service{
    
    if (![SHARED_HELPER checkIfisInternetAvailable]) {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
   
    
    [SHARED_API get_marketPlace_Category_Type:@"" withSuccess:^(NSDictionary *response) {
        
        if ([[response valueForKey:STATUS]isEqualToString:SUCCESS]) {
           self.createPostTypeTF.text= _DisplayCatString;
            postCategoryID=_displatCatID;
            NSLog(@"Displaying CatID %@",postCategoryID);
            if ([postCategoryID isEqualToString:@"2"] || [self.createPostTypeTF.text isEqualToString:@"Personal"]) {
                self.createPostPriceTF.userInteractionEnabled=NO;
                self.createPostPriceTF.text=@"";
                self.topConstraint_priceBottomView.constant=-41;
                self.createPost_priceImage.hidden=YES;
                self.createPostPriceTF.hidden=YES;
            }
            else
            {
               self.createPostPriceTF.userInteractionEnabled=YES;
                self.topConstraint_priceBottomView.constant=0;
                self.createPost_priceImage.hidden=NO;
                self.createPostPriceTF.hidden=NO;

            }
            
           
            self.arrayOFCreatePostCategories=[[NSMutableArray alloc]init];
            _arrayOFCreatePostCategories=[response valueForKey:@"categories"];
            
           
        }
        else{
            
            [SHARED_HELPER showAlert:ServiceFail];
           
    }
 }
     
    onfailure:^(NSError *theError) {
            [SHARED_HELPER showAlert:ServerConnection];
        
    }];
    
}

-(void)cancle_clicked:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)add_click:(UIButton *)sender
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped do nothing.
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

- (void) imagePickerController:(UIImagePickerController *)picker
         didFinishPickingImage:(UIImage *)image
                   editingInfo:(NSDictionary *)editingInfo
{
    //    img.image = [UIImage imageNamed:[array objectAtIndex:index]];
    UIImage *photoTaken = image;
    if (photoTaken)
    {
        
        if (index <=5)
        {
            if (index == 1) {
                x = 0;
            }
            baseView_placesImages = [[UIView alloc]initWithFrame:CGRectMake(x, 0, 150, 120)];
            baseView_placesImages.tag = 100+index;
            baseView_placesImages.backgroundColor = [UIColor clearColor];
            [self.multipleImages_scrollview addSubview:baseView_placesImages];
            
            img = [[UIImageView alloc] initWithFrame:CGRectMake(0,15,90, 90)];
            img.image=photoTaken;
            img.contentMode=UIViewContentModeScaleAspectFit;
            
            //image path from gallery
//            CGFloat compression = 0.9f;
            //CGFloat maxCompression = 0.1f;
           
//            int maxFileSize = 2000000;
//            NSData *imgData = UIImageJPEGRepresentation(photoTaken, 1);
//            while ([imgData length] > maxFileSize)
//            {
//                compression -= 0.1;
//                imgData = UIImageJPEGRepresentation(img.image,compression);
//            }
//            NSString *  base64String = [imgData base64EncodedStringWithOptions:0];
//            [img_array addObject:base64String];
            
            
            
//            CGFloat maxCompression = 0.1f;
//            int maxFileSize = 250*1024;
//            NSData *imageData1 = UIImageJPEGRepresentation(img.image,compression);
            
//            while ([imageData1 length] > maxFileSize && compression > maxCompression)
//            {
//                compression -= 0.1;
//                imageData1 = UIImageJPEGRepresentation(img.image,compression);
//            }
            NSData *imageData1=[self compressImage:photoTaken];
           NSString * base64String = [imageData1 base64EncodedStringWithOptions:0];
            [img_array addObject:base64String];

            
            imgIndex++;
            
            x = x+110;
            
            
            [baseView_placesImages addSubview:img];
            
            subtract_btn = [[UIButton alloc]initWithFrame:CGRectMake(img.frame.size.width-12.5, 0, 25, 25)];
            //            [subtract_btn setTitle:@"X" forState:UIControlStateNormal];
            //            [subtract_btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
            [subtract_btn setBackgroundImage:[UIImage imageNamed:@"wrong_icon.png"] forState:UIControlStateNormal];
            [subtract_btn.titleLabel setFont:[UIFont systemFontOfSize:20]];
            [subtract_btn addTarget:self action:@selector(sub_click:) forControlEvents:UIControlEventTouchUpInside];
            subtract_btn.tag=index;
            [baseView_placesImages addSubview:subtract_btn];
            
            
            add_btn.frame = CGRectMake(baseView_placesImages.frame.origin.x+baseView_placesImages.frame.size.width-40, img.frame.origin.y, 90, 90);
            self.multipleImages_scrollview.contentSize = CGSizeMake(add_btn.frame.origin.x+add_btn.frame.size.width, 120);
            
            if (index == 5) {
                self.multipleImages_scrollview.contentSize = CGSizeMake(add_btn.frame.origin.x-5, 120);
                add_btn.hidden = YES;
            }
            
            index++;
            
        }
        
        
    }
    else
    {
        NSLog(@"Selected photo is NULL");
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}
-(NSData *)compressImage:(UIImage *)image{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    //    float maxHeight = 1130.0f;
    float maxHeight = 816.0f;
    float maxWidth = 640.0f;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.9;//50 percent compression
    NSData *imageData = UIImageJPEGRepresentation(image, compressionQuality);
    while (actualHeight > maxHeight || actualWidth > maxWidth){
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
        CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
        UIGraphicsBeginImageContext(rect.size);
        [image drawInRect:rect];
        UIImage *img_is = UIGraphicsGetImageFromCurrentImageContext();
        imageData = UIImageJPEGRepresentation(img_is, compressionQuality);
        UIGraphicsEndImageContext();
        
    }
    //        else{
    //        actualHeight = maxHeight;
    //        actualWidth = maxWidth;
    //        compressionQuality = 1;
    //    }
    
    
    return imageData;
}

-(IBAction)sub_click:(UIButton *)sender
{
    
    UIView *remove_baseView_placesImages = (UIView *)[self.multipleImages_scrollview viewWithTag:sender.tag+100];
    [remove_baseView_placesImages removeFromSuperview];
    
    int xCoordinate = remove_baseView_placesImages.frame.origin.x;
    
    if (sender.tag < index-1)
    {
        for (int i = (int)sender.tag+1; i<= index-1 ; i++) {
            
            UIView *moveForward_placesImages = (UIView *)[self.multipleImages_scrollview viewWithTag:i+100];
            moveForward_placesImages.frame = CGRectMake(xCoordinate, 0, 150, self.multipleImages_scrollview.frame.size.height);
            UIButton *buttonTag = (UIButton *)[moveForward_placesImages viewWithTag:(int)moveForward_placesImages.tag-100];
            buttonTag.tag = buttonTag.tag-1;
            moveForward_placesImages.tag = 100+i-1;
            xCoordinate = xCoordinate+110;
            
        }
        
        index --;
        
        x = xCoordinate;
        
        add_btn.hidden = NO;
        add_btn.frame = CGRectMake(xCoordinate, img.frame.origin.y, 90, 90);
        self.multipleImages_scrollview.contentSize = CGSizeMake(add_btn.frame.origin.x + add_btn.frame.size.width, 120);
        
    }
    else
    {
        x = x-110;
        index = (int)sender.tag;
        
        add_btn.frame = CGRectMake(remove_baseView_placesImages.frame.origin.x, img.frame.origin.y, 90, 90);
        self.multipleImages_scrollview.contentSize = CGSizeMake(add_btn.frame.origin.x + add_btn.frame.size.width, 120);
        
    }
    
    [img_array removeObjectAtIndex:(int)sender.tag-1];
    
    if (sender.tag == 5) {
        
        add_btn.hidden = NO;
        add_btn.frame = CGRectMake(remove_baseView_placesImages.frame.origin.x, img.frame.origin.y, 90, 90);
        self.multipleImages_scrollview.contentSize = CGSizeMake(add_btn.frame.origin.x + add_btn.frame.size.width, 120);
    }
    
}
-(void)save_clicked
{
     _createPostTitleTF.text=[_createPostTitleTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
     _createPostPriceTF.text=[_createPostPriceTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
     _createPostAddressTF.text=[_createPostAddressTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (![SHARED_HELPER checkIfisInternetAvailable]) {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
    if(_createPostTitleTF.text.length == 0 )
    {
        [SHARED_HELPER showAlert:@"Title should not be empty"];
    }
    else if(_createPostTypeTF.text.length ==0)
    {
        [SHARED_HELPER showAlert:@"Please select the category"];
    }
    else if(_createPostEmailTF.text.length ==0)
    {
        [SHARED_HELPER showAlert:EmailText];
    }
    else if ([self NSStringIsValidEmail:_createPostEmailTF.text]== NO)
    {
        [SHARED_HELPER showAlert:ValidEmailText];
        
    }
    else if (_createPostPhoneNumberTF.text.length !=12 && _createPostPhoneNumberTF.text.length !=0)
    {
        [SHARED_HELPER showAlert:MobileText];
    }

//    else if(_createPostPriceTF.text.length ==0)
//    {
//        [SHARED_HELPER showAlert:@"Please enter the price"];
//    }
//    else if(_createPostAddressTF.text.length ==0)
//    {
//        [SHARED_HELPER showAlert:@"Please select the address"];
//    }
   else if (!([_createPostDescriptionTextView.text length]>0)) {
        [SHARED_HELPER showAlert:event_des];
        return;
    }
    //    else if ([img_array count]<2)
//    {
//        [SHARED_HELPER showAlert:ImageText1];
//    }
    
    
   
    else if (_createPostPhoneNumberTF.text.length !=12 && _createPostPhoneNumberTF.text.length !=0)
    {
        [SHARED_HELPER showAlert:MobileText];
    }
    else
    {
        [self createpostMethod];
    }

}
-(void)createpostMethod
{
//    http://openroadridesapi.thinkwithebiz.com/rides/create_post
    if (![SHARED_HELPER checkIfisInternetAvailable]) {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
//    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//    activeIndicatore.center = self.view.center;
//    activeIndicatore.color = APP_YELLOW_COLOR;
//    activeIndicatore.hidesWhenStopped = TRUE;
//    [self.view addSubview:activeIndicatore];
//    [activeIndicatore startAnimating];
    
    
    indicaterview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    activeIndicatore.backgroundColor=[UIColor clearColor];
    activeIndicatore = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.frame = CGRectMake(0.0, 0.0, 80, 80);
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    [indicaterview addSubview:activeIndicatore];
    [self.view addSubview:indicaterview];
    [indicaterview bringSubviewToFront:self.view];
    [activeIndicatore startAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    indicaterview.hidden=NO;

//    "{
//    ""user_id"": """",
//    ""post_title"": """",
//    ""post_price"": """",
//    ""post_address"": """",
//    ""post_latitute"": """",
//    ""post_longitude"": """",
//    ""phone_number"": """",
//    ""email"": """",
//    ""description"": """",
//    ""status"":""Running"",
//    ""post_image_names"":[""""]
//}"
    NSMutableDictionary *createpost_dict = [NSMutableDictionary new];
    
    [createpost_dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
   
    if (start_latitude==0) {
       
        [createpost_dict setObject:@""forKey:@"post_latitute"];
    }
    else
    {
         [createpost_dict setObject:[NSNumber numberWithDouble:start_latitude] forKey:@"post_latitute"];
    }
    if (start_longitude==0) {
        [createpost_dict setObject:@""forKey:@"post_longitude"];
    }
    else
    {
        [createpost_dict setObject:[NSNumber numberWithDouble:start_longitude] forKey:@"post_longitude"];
    }
    
    [createpost_dict setObject:_createPostTitleTF.text forKey:@"post_title"];
    [createpost_dict setObject:_createPostPriceTF.text forKey:@"post_price"];
    [createpost_dict setObject:_createPostAddressTF.text forKey:@"post_address"];
    [createpost_dict setObject:_createPostPhoneNumberTF.text forKey:@"phone_number"];
    [createpost_dict setObject:_createPostEmailTF.text forKey:@"email"];
    [createpost_dict setObject:_createPostDescriptionTextView.text forKey:@"description"];
    [createpost_dict setObject:@"Running" forKey:@"status"];
    [createpost_dict setObject:postCategoryID forKey:@"categories_id"];
    [createpost_dict setObject:img_array forKey:@"post_image_names"];
    
    
    NSLog(@"Create post Dict %@",createpost_dict);
    
    [SHARED_API createMyPostRequestWithParams:createpost_dict withSuccess:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           NSLog(@"Create Post Response %@",response);
                           if([[response objectForKey:STATUS] isEqualToString:SUCCESS])
                           {
                               [self.navigationController popViewControllerAnimated:YES];
//                                [activeIndicatore stopAnimating];
                               indicaterview.hidden=YES;
                               [[NSNotificationCenter defaultCenter] postNotificationName:@"CreatePostReload" object:self];
                               
                               [SHARED_HELPER showAlert:@"Your post is created successfully"];
                            }
                           else if ([[response objectForKey:STATUS] isEqualToString:FAIL])
                           {
                               
//                            [activeIndicatore stopAnimating];
                               indicaterview.hidden=YES;
                               
                               
                            [SHARED_HELPER showAlert:Forgotfail];
                           }
//                           [activeIndicatore stopAnimating];
                           indicaterview.hidden=YES;
                       });
        
    } onfailure:^(NSError *theError) {
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           NSLog(@"Create Post Response %@",theError);
//                           if ([activeIndicatore isAnimating]) {
//                               [activeIndicatore stopAnimating];
//                               [activeIndicatore removeFromSuperview];
//                           }
                           indicaterview.hidden=YES;
                           [SHARED_HELPER showAlert:ServiceFail];
                           
                           
                       });
    }];
    

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)OnClick_createPostAddressLocationBtn:(id)sender {
    location_str = @"start_location_btn";
    GMSPlacePickerConfig *config = [[GMSPlacePickerConfig alloc] initWithViewport:nil];
    GMSPlacePickerViewController *placePicker =
    [[GMSPlacePickerViewController alloc] initWithConfig:config];
    placePicker.delegate = self;
    [self presentViewController:placePicker animated:YES completion:nil];
}
- (void)placePicker:(GMSPlacePickerViewController *)viewController didPickPlace:(GMSPlace *)place {
    // Dismiss the place picker, as it cannot dismiss itself.
    [viewController dismissViewControllerAnimated:YES completion:nil];
    
    
    if ([location_str isEqualToString:@"start_location_btn"]) {
//        _createPostAddressTF.text = [NSString stringWithFormat:@"%@ %@",place.name,place.formattedAddress];
        
        
        NSString*loc_str_name=[NSString stringWithFormat:@"%@",place.name];
        if ([loc_str_name isEqualToString:@"(null)"] || [loc_str_name isEqualToString:@"<null>"] || [loc_str_name isEqualToString:@""] || [loc_str_name isEqualToString:@"null"] || loc_str_name == nil) {
            loc_str_name=@"";
            
        }
        NSString*loc_str_address=[NSString stringWithFormat:@"%@",place.formattedAddress];
        if ([loc_str_address isEqualToString:@"(null)"] || [loc_str_address isEqualToString:@"<null>"] || [loc_str_address isEqualToString:@""] || [loc_str_address isEqualToString:@"null"] || loc_str_address == nil) {
            loc_str_address=@"";
            
        }
        NSString*loc_name_address_str=[NSString stringWithFormat:@"%@ %@",loc_str_name,loc_str_address];
        
        if ([loc_name_address_str isEqualToString:@"(null)"] || [loc_name_address_str isEqualToString:@"<null>"] || [loc_name_address_str isEqualToString:@""] || [loc_name_address_str isEqualToString:@"null"] || loc_name_address_str == nil) {
            loc_name_address_str=@"";
            
        }
        else
        {
            _createPostAddressTF.text=loc_name_address_str;
           
        }
        
        start_latitude=place.coordinate.latitude;
        start_longitude=place.coordinate.longitude;
        
        
        NSLog(@"Place name %@", place.name);
        NSLog(@"Place address %@", place.formattedAddress);
        NSLog(@"Place attributions %@", place.attributions.string);
    }
}

- (void)placePickerDidCancel:(GMSPlacePickerViewController *)viewController {
    // Dismiss the place picker, as it cannot dismiss itself.
    [viewController dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"No place selected");
}


- (IBAction)onClick_createPostView_rideGoingBtn:(id)sender {
    
    self.createPostView_scrollLabel.hidden=YES;
    self.createPostView_rideGoingBtn.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }
}
@end
