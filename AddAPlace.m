//
//  AddAPlace.m
//  openroadrides
//
//  Created by apple on 25/05/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "AddAPlace.h"
#import "AppDelegate.h"
@interface AddAPlace ()
{
    NSMutableArray *img_array;
    NSArray *array;
    int scrollWidth ;
    UIImageView *img;
    int x_axis;
    UIButton *add_btn;
    UIButton *subtract_btn;
    int index,imgIndex;
    int x;
    BIZPopupViewController *popupViewController;
    AppDelegate *app_Deleagte;
}
@end

@implementation AddAPlace

- (void)viewDidLoad {
    [super viewDidLoad];
    index = 1;
    imgIndex = 1;
    clickedSave=@"NO";
    
    addplace_count_in_ride=[self gettingAddPlacesFromCoreData];
    
    //    array = @[@"download",@"images",@"download",@"images",@"download"];
    img_array = [[NSMutableArray alloc]init];
    _base_view.backgroundColor = [UIColor clearColor];
    _scrl_view.showsHorizontalScrollIndicator = NO;
    add_btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 12, 90,90)];
    [add_btn addTarget:self action:@selector(add_click:) forControlEvents:UIControlEventTouchUpInside];
    //    add_btn.backgroundColor = [UIColor greenColor];
    [add_btn setBackgroundImage:[UIImage imageNamed:plus_icon] forState:UIControlStateNormal];
    [_scrl_view addSubview:add_btn];
    x = 0;
    [self.Title_tf setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.Description_tf setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    
    app_Deleagte=(AppDelegate *)[UIApplication sharedApplication].delegate;
    
//    if (appDelegate.addAPlace_imagesArray.count>0) {
//        NSLog(@"Add_Place");
//    }
//    else
//    {
//        app_Deleagte.addAPlace_imagesArray=[[NSMutableArray alloc]init];
        
//    }
    
//    if (app_Deleagte.addAPlace_data_Array.count>0) {
//        NSLog(@"Add_Place");
//    }
//    else
//    {
//        app_Deleagte.addAPlace_data_Array=[[NSMutableArray alloc]init];
//    }
   appDelegate.pop_UP_out_Side_Click_Hide=@"NO";
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
//    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
//    [dateFormatter setTimeZone:timeZone];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss+HH:mm"];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZ"];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss z"];
    NSLog(@"ride End Date %@",[dateFormatter stringFromDate:[NSDate date]]);
    _add_A_Place_timeString=[NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:[NSDate date]]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)add_click:(UIButton *)sender
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped do nothing.
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = NO;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = NO;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }]];
[self presentViewController:actionSheet animated:YES completion:nil];
    
}

- (void) imagePickerController:(UIImagePickerController *)picker
         didFinishPickingImage:(UIImage *)image
                   editingInfo:(NSDictionary *)editingInfo
{
    //    img.image = [UIImage imageNamed:[array objectAtIndex:index]];
    UIImage *photoTaken = image;
    if (photoTaken)
    {
        
        if (index <=5)
        {
            if (index == 1) {
                x = 0;
            }
            
            
            baseView_placesImages = [[UIView alloc]initWithFrame:CGRectMake(x, 0, 150, self.scrl_view.frame.size.height)];
            baseView_placesImages.tag = 100+index;
            baseView_placesImages.backgroundColor = [UIColor clearColor];
            [_scrl_view addSubview:baseView_placesImages];
            
            img = [[UIImageView alloc] initWithFrame:CGRectMake(0,12,90, 90)];
//            img.backgroundColor=[UIColor clearColor];
            img.image=photoTaken;
            img.contentMode=UIViewContentModeScaleAspectFit;
            
            CGFloat compression = 1.0f;
            CGFloat maxCompression = 0.1f;
            int maxFileSize = 1048576;
            //image path from gallery
            NSData *webData;
           
            webData =UIImageJPEGRepresentation(img.image, compression);
            if ([webData length] > maxFileSize && compression > maxCompression) {
                compression -= 0.1;
                webData =UIImageJPEGRepresentation(img.image, compression);
            }
//            NSLog(@"lenth of nsdata is %lu",(unsigned long)webData.length);
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"MM-dd-HH:mm:ss"];
            NSString *img_pick_time=[dateFormatter stringFromDate:[NSDate date]];
            NSString *imgFormat = [NSString stringWithFormat:@"Place_%@.png",img_pick_time];
            
            NSString *localFilePath = [documentsDirectory stringByAppendingPathComponent:imgFormat];
            [webData writeToFile:localFilePath atomically:YES];
            [img_array addObject:localFilePath];
            NSLog(@"images array is %@",img_array);
            
            imgIndex++;
            
            x = x+110;
            
            
            [baseView_placesImages addSubview:img];
            
            subtract_btn = [[UIButton alloc]initWithFrame:CGRectMake(img.frame.size.width-12.5, 0, 25, 25)];
//            [subtract_btn setTitle:@"X" forState:UIControlStateNormal];
//            [subtract_btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
            [subtract_btn setBackgroundImage:[UIImage imageNamed:@"wrong_icon.png"] forState:UIControlStateNormal];
            [subtract_btn.titleLabel setFont:[UIFont systemFontOfSize:20]];
            [subtract_btn addTarget:self action:@selector(sub_click:) forControlEvents:UIControlEventTouchUpInside];
            subtract_btn.tag=index;
            [baseView_placesImages addSubview:subtract_btn];
            
            
            add_btn.frame = CGRectMake(baseView_placesImages.frame.origin.x+baseView_placesImages.frame.size.width-40, img.frame.origin.y, 90, 90);
            _scrl_view.contentSize = CGSizeMake(add_btn.frame.origin.x+add_btn.frame.size.width, self.base_view.frame.size.height);
            
            if (index == 5) {
                _scrl_view.contentSize = CGSizeMake(add_btn.frame.origin.x-5, self.base_view.frame.size.height);
                add_btn.hidden = YES;
            }
            
            index++;
            
        }
        
        
    }
    else
    {
        NSLog(@"Selected photo is NULL");
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)sub_click:(UIButton *)sender
{
    
    UIView *remove_baseView_placesImages = (UIView *)[_scrl_view viewWithTag:sender.tag+100];
    [remove_baseView_placesImages removeFromSuperview];
    
    int xCoordinate = remove_baseView_placesImages.frame.origin.x;
    
    if (sender.tag < index-1)
    {
        for (int i = (int)sender.tag+1; i<= index-1 ; i++) {
            
            UIView *moveForward_placesImages = (UIView *)[_scrl_view viewWithTag:i+100];
            moveForward_placesImages.frame = CGRectMake(xCoordinate, 0, 150, self.scrl_view.frame.size.height);
            UIButton *buttonTag = (UIButton *)[moveForward_placesImages viewWithTag:(int)moveForward_placesImages.tag-100];
            buttonTag.tag = buttonTag.tag-1;
            moveForward_placesImages.tag = 100+i-1;
            xCoordinate = xCoordinate+110;
            
        }
        
        index --;
        
        x = xCoordinate;
        
        add_btn.hidden = NO;
        add_btn.frame = CGRectMake(xCoordinate, img.frame.origin.y, 90, 90);
        _scrl_view.contentSize = CGSizeMake(add_btn.frame.origin.x + add_btn.frame.size.width, self.base_view.frame.size.height);
        
    }
    else
    {
        x = x-110;
        index = (int)sender.tag;
        
        add_btn.frame = CGRectMake(remove_baseView_placesImages.frame.origin.x, img.frame.origin.y, 90, 90);
        _scrl_view.contentSize = CGSizeMake(add_btn.frame.origin.x + add_btn.frame.size.width, self.base_view.frame.size.height);
        
    }
    
    [img_array removeObjectAtIndex:(int)sender.tag-1];
    
    if (sender.tag == 5) {
        
        add_btn.hidden = NO;
        add_btn.frame = CGRectMake(remove_baseView_placesImages.frame.origin.x, img.frame.origin.y, 90, 90);
        _scrl_view.contentSize = CGSizeMake(add_btn.frame.origin.x + add_btn.frame.size.width, self.base_view.frame.size.height);
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onClick_close_btn:(id)sender {
    popupViewController = [[BIZPopupViewController alloc]init];
    popupViewController.delegate=self;
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)onClick_save_btn:(id)sender
{
    [self.view endEditing:YES];
//    if (![SHARED_HELPER checkIfisInternetAvailable]) {
//        [self showAlert:Nonetwork];
//        return;
//    }
    if ([_Title_tf.text isEqualToString:@""]) {
        [self showAlert:TitleAlert];
        return;
    }
    if ([clickedSave isEqualToString:@"YES"]) {
        return;
    }
    clickedSave=@"YES";
    appDelegate.add_Place_marker_title=self.Title_tf.text;
    popupViewController = [[BIZPopupViewController alloc]init];
    popupViewController.delegate=self;
   
    
    NSManagedObjectContext *context=[self managedObjectContext];
    AddPlace *place=[[AddPlace alloc]initWithContext:context];
    place.lat=[[NSString stringWithFormat:@"%@",_add_A_place_lat] doubleValue];
    place.longitude=[[NSString stringWithFormat:@"%@",_add_A_place_long] doubleValue];
    place.des=_Description_tf.text;
    place.title=self.Title_tf.text;
    place.time=_add_A_Place_timeString;
    place.img_urls=@"";
    place.img_count=img_array.count;
    if (img_array.count>0) {
        NSString *str=[img_array componentsJoinedByString:@","];
        place.img_paths=str;
        place.status=table_status_pending;
    }
    else
    {
        place.img_paths=@"";
        place.status=table_status_complete;
    }
    place.places_id=addplace_count_in_ride;
    place.local_rid=[_local_rid intValue];
    if ([place.managedObjectContext save:nil]) {
        NSLog(@"This Place saved in DataBase");
    }
   

    [self dismissViewControllerAnimated:YES completion:nil];
    appDelegate.is_Done_Add_Place=@"YES";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"selectedData_addAPlace" object:self];
    
    
  /*  [appDelegate.addAPlace_imagesArray addObjectsFromArray:img_array];
    NSMutableDictionary *addaAPlace_dict=[[NSMutableDictionary alloc]init];
  
    [addaAPlace_dict setObject:[NSString stringWithFormat:@"%@",self.Title_tf.text] forKey:@"AddAPlace_Title"];
    [addaAPlace_dict setObject:[NSString stringWithFormat:@"%@",_add_A_place_lat] forKey:@"AddAPlace_lat"];
    [addaAPlace_dict setObject:[NSString stringWithFormat:@"%@",_add_A_place_long] forKey:@"AddAPlace_long"];
    [addaAPlace_dict setObject:[NSString stringWithFormat:@"%@",_Description_tf.text] forKey:@"AddAPlace_desc"];
    [addaAPlace_dict setObject:[NSString stringWithFormat:@"%@",_add_A_Place_timeString] forKey:@"AddAPlace_Time"];
    [addaAPlace_dict setObject:[NSString stringWithFormat:@"%lu",(unsigned long)img_array.count] forKey:@"ThisPlaceimagesCount"];
    
    [appDelegate.addAPlace_data_Array addObject:addaAPlace_dict];
    NSLog(@"Add A Place When we save Data %@",appDelegate.addAPlace_data_Array);
    
Save Add_Place Data on Local DataBase
NSError *error=nil;
NSManagedObjectContext *context=[self managedObjectContext];
 NSManagedObject *newDevice=[NSEntityDescription insertNewObjectForEntityForName:@"AddPlace" inManagedObjectContext:context];
   [newDevice setValue:[NSNumber numberWithDouble:[[NSString stringWithFormat:@"%@",_add_A_place_lat] doubleValue]] forKey:@"lat"];
    [newDevice setValue:[NSNumber numberWithDouble:[[NSString stringWithFormat:@"%@",_add_A_place_long] doubleValue]] forKey:@"long"];
    [newDevice setValue:[NSString stringWithFormat:@"%@",_Description_tf.text] forKey:@"des"];
    [newDevice setValue:[NSString stringWithFormat:@"%@",self.Title_tf.text] forKey:@"title"];
    [newDevice setValue:[NSNumber numberWithInt:[[NSString stringWithFormat:@"%lu",(unsigned long)img_array.count] intValue]] forKey:@"img_count"];
    [newDevice setValue:[NSString stringWithFormat:@"%@",_add_A_Place_timeString] forKey:@"time"];
    if (img_array.count>0) {
        NSString *str=[img_array componentsJoinedByString:@","];
        [newDevice setValue:str forKey:@"img_urls"];
    }
    else
    {
        [newDevice setValue:@"" forKey:@"img_urls"];
    }
    
    [newDevice.managedObjectContext save:&error];
    if ([newDevice.managedObjectContext save:&error])
    {
        NSLog(@"Saved latitude and longitude in coredata for default markers======>>");
    }
    */
}
- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}
-(NSUInteger)gettingAddPlacesFromCoreData
{
    NSError *error;
    _dataEntity = [NSEntityDescription entityForName:@"AddPlace" inManagedObjectContext:self.managedObjectContext];
    NSFetchRequest * fr = [[NSFetchRequest alloc]init];
    [fr setEntity:_dataEntity];
    NSMutableArray * result = [[self.managedObjectContext executeFetchRequest:fr error:&error] mutableCopy];
    int64_t place_id=1;
    if (result.count>0) {
        AddPlace *place=[result  objectAtIndex:result.count-1];
        place_id=place.places_id;
        place_id++;
    }
    return place_id;
}
-(void)showAlert:(NSString *)message
{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:message  preferredStyle:UIAlertControllerStyleActionSheet];
    UIView  *firstSubview = alert.view.subviews.firstObject;
    firstSubview.backgroundColor = APP_YELLOW_COLOR;
    UIView *alertContentView = firstSubview.subviews.firstObject;
    alertContentView.backgroundColor = APP_YELLOW_COLOR;
    alertContentView.tintColor = [UIColor blueColor];
    for (UIView *subSubView in alertContentView.subviews) {
        
        subSubView.backgroundColor = APP_YELLOW_COLOR;
    }
   // [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alert animated:YES completion:nil];
   [self presentViewController:alert animated:YES completion:nil];
    
    int duration = 2; // duration in seconds
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        
        [alert dismissViewControllerAnimated:YES completion:nil];
        
    });
}

@end
