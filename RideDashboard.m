
//
//  RideDashboard.m
//  openroadrides
//
//  Created by apple on 03/05/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "RideDashboard.h"
#import "GPX.h"
#import <MessageUI/MFMailComposeViewController.h>
#import <MapKit/MapKit.h>
#import "CheckInPlaces.h"
#import "MPCoachMarks.h"

#define ridecreated @"Your schuduled ride is updated successfully."
@interface RideDashboard ()<MFMailComposeViewControllerDelegate>
{
    int rideTime_coutDown,totalTime_countDown,last_Ride_Time,locationSpeedValue,maxspeed,mapZoom;
    CLLocationManager *locationManager,*map_locationManager;
    float miles,weatherLatitude,weatherLongitutde,altitude,elevation,maxElevation,minElevation,avgSpeed;
    //    double altitude,elevation,maxElevation;
    NSString *ride_status,*ride_Start_date_String,*ride_Start_time_String,*ride_end_date_String,*ride_end_time_String,*ride_totalDistance;
    CLLocationDistance ride_Dist,dist_pr_to_Cur_LOC;
    CLLocation *previous_location,*Updated_Location;
    CLLocationCoordinate2D map_currentlocation_coordinate;
    
    GMSAddress* addressObj;
    GMSMarker *currentLocatonPin;
    GMSMutablePath *path,*path2;
    GMSPolyline *rectangle;
    GMSPolyline *polyline,*polyline_2;
    
    AppDelegate *app_Delegate;
    NSString *AmazonGpxUrlString,*AmazonImageUrlString,*xml_Create_string;
    
    NSArray *ride_Images;
    
    NSDictionary *add_dict;
}
@end

@implementation RideDashboard

#pragma mark -ViewDidLoad-Methodes
- (void)viewDidLoad {
    [super viewDidLoad];
    
    app_Delegate=(AppDelegate *)[UIApplication sharedApplication].delegate;
    self.navigationController.navigationBarHidden = YES;
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
    
    if (SCREEN_HEIGHT>480) {
        if (SCREEN_WIDTH==320) {
            self.ride_view_cintent_View_eight_constraint.constant=self.ride_view_cintent_View_eight_constraint.constant+50;
        }
    }
    else{
        self.ride_view_cintent_View_eight_constraint.constant=self.ride_view_cintent_View_eight_constraint.constant+80;
    }
    
    
    self.ridedashboard_homeBtn.hidden=YES;
    
    first_Ride_Complete=@"NO";
    ride_Finished=@"YES";
    original_Marker_Val=1;
    dis_int_for_dflt_marker=1;
    appDelegate.app_Status=@"Ride_page";
    AmazonGpxUrlString=@"";
    isPanned = false;
    isTapped=false;
    
    _boolvalue_rideToggle=YES;
    
    
    
    // Do any additional setup after loading the view.
    self.view_SpeedDetails.layer.borderColor=[[UIColor whiteColor] CGColor];
    self.view_SpeedDetails.layer.borderWidth=1.0;
    //  self.view_SpeedDetails.alpha=0.2;
    
    //Labels
    self.route_Name_Label.text=@"";
    
    /*self.checkIn_btn.layer.borderColor=[[UIColor colorWithRed:(211/255.0) green:(184/255.0) blue:(9/255.0) alpha:1.0]CGColor];
    self.checkIn_btn.layer.borderWidth=1.0;
    
    
    
    self.defaultMarker_btn.layer.borderColor=[[UIColor colorWithRed:(16/255.0) green:(204/255.0) blue:(20/255.0) alpha:1.0]CGColor];
    self.defaultMarker_btn.layer.borderWidth=1.0;
    
    self.custommarkers_btn.layer.borderColor=[[UIColor colorWithRed:(10/255.0) green:(165/255.0) blue:(204/255.0) alpha:1.0]CGColor];
    self.custommarkers_btn.layer.borderWidth=1.0;
    
    self.check_in_btn_2.layer.borderColor=[[UIColor colorWithRed:(211/255.0) green:(184/255.0) blue:(9/255.0) alpha:1.0]CGColor];
    self.check_in_btn_2.layer.borderWidth=1.0;
    
    self.default_markers_btn_2.layer.borderColor=[[UIColor colorWithRed:(16/255.0) green:(204/255.0) blue:(20/255.0) alpha:1.0]CGColor];
    self.default_markers_btn_2.layer.borderWidth=1.0;
    
    self.custom_Markers_btn2.layer.borderColor=[[UIColor colorWithRed:(10/255.0) green:(165/255.0) blue:(204/255.0) alpha:1.0]CGColor];
    self.custom_Markers_btn2.layer.borderWidth=1.0;
    */
    
    self.checkIn_btn.backgroundColor=APP_YELLOW_COLOR;
    self.defaultMarker_btn.backgroundColor=APP_YELLOW_COLOR;
    self.custommarkers_btn.backgroundColor=[UIColor colorWithRed:(10/255.0) green:(165/255.0) blue:(204/255.0) alpha:1.0];
    self.self.check_in_btn_2.backgroundColor=APP_YELLOW_COLOR;
    self.self.default_markers_btn_2.backgroundColor=APP_YELLOW_COLOR;
    self.self.custom_Markers_btn2.backgroundColor=[UIColor colorWithRed:(10/255.0) green:(165/255.0) blue:(204/255.0) alpha:1.0];
    
    self.checkIn_btn.layer.cornerRadius=5;
    self.defaultMarker_btn.layer.cornerRadius=5;
    self.add_Place_View.layer.cornerRadius=5;
    self.check_in_View2.layer.cornerRadius=5;
    self.default_marker_View2.layer.cornerRadius=5;
    self.add_Places_View2.layer.cornerRadius=5;
    
    self.checkIn_btn.clipsToBounds=YES;
    self.defaultMarker_btn.clipsToBounds=YES;
    self.add_Place_View.clipsToBounds=YES;
    self.check_in_View2.clipsToBounds=YES;
    self.default_marker_View2.clipsToBounds=YES;
    self.add_Places_View2.clipsToBounds=YES;
    
    self.finish_btn.hidden=YES;
    self.finish_Ride_Imageview.hidden=YES;
    _rideDashboard_Promotions_View.hidden=YES;
    
    
    
    
    
    self.bottom_ride_start_view_imageview.alpha=1.0;
    self.bottom_ride_start_view_imageview.backgroundColor=[UIColor clearColor];
    
    self.view_map_rideDashboard.hidden=YES;
    
    self.checkin_View.hidden=YES;
    self.add_Place_View.hidden=YES;
    self.markers_View2.hidden=YES;
    self.check_in_View2.hidden=YES;
    self.add_Places_View2.hidden=YES;
    //  [self.mapToggle_btn setImage:[UIImage imageNamed:@"map_toggle"] forState:UIControlStateNormal];
    //Genarate random number for ride at one time only
    NSUInteger len =8;
    NSString *letterd =@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (NSUInteger i = 0; i < len; i++) {
        u_int32_t r = arc4random() % [letterd length];
        unichar c = [letterd characterAtIndex:r];
        [randomString appendFormat:@"%C", c];
    }
    NSString *random=[NSString stringWithFormat:@"%@",randomString];
    appDelegate.ride_Random_Number=random;
    
    
    [self map_loadingMethod];
    [self weatherMethod:@""];
    
    
    //notification when user complete below functionalities
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectedData_addAPlace) name:@"selectedData_addAPlace" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SAVE_Route_Action_And_END_RIDE_SERVICE) name:@"SaveARoute" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(Only_Save_Route_Action) name:@"Route_Sucsess" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(check_Location_On_Or_Off) name:UIApplicationWillEnterForegroundNotification object:nil];
    
    
    
    
    //Indication
    indicaterview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    activeIndicatore.backgroundColor=[UIColor clearColor];
    activeIndicatore = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.frame = CGRectMake(0.0, 0.0, 80, 80);
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    [indicaterview addSubview:activeIndicatore];
    [self.view addSubview:indicaterview];
    [indicaterview bringSubviewToFront:self.view];
    [activeIndicatore startAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    indicaterview.hidden=YES;
    
    
    
    //For AddaPlaces
    appDelegate.check_in_Clicked=@"NO";
    appDelegate.is_Done_Add_Place=@"NO";
    addPlacesDataArray=[[NSMutableArray alloc]init];
    
    //abour rides
    route_strat_adress=@"";
    route_end_Adress=@"";
    ride_Start_Lat=@"";
    ride_Start_Long=@"";
    ride_End_Lat=@"";
    ride_End_Long=@"";
    ride_strat_adress=@"";
    ride_end_Adress=@"";
    selected_route_title=@"";
    ride_Title=@"";
    select_Route_Des=@"";
    selected_Route_GPX=@"";
    ride_totalDistance=@"0";
    ride_Dist=0.0;
    select_route_places_count=0;
    s3_inProgress=@"NO";
    created_ride_description=@"";
    ride_Start_date_String=@"";
    ride_Start_time_String=@"";
    ride_end_date_String=@"";
    ride_end_time_String=@"";
    
    ride_owner_id=[NSString stringWithFormat:@"%@",[Defaults valueForKey:User_ID]];
    selected_route_owner_id=[NSString stringWithFormat:@"%@",[Defaults valueForKey:User_ID]];
    
    
    local_Ride_ID=[self get_ride_Table_count];
    
    //we do functionality based on which page to come
    
    if ([_is_From isEqualToString:@"HOME"]) {
        [self is_From_Home];
    }
    else if ([_is_From isEqualToString:@"NOTIFICATIONS"]) {
        //Members invited by owner
        appDelegate.ride_ID_Api=_ride_id_From_Notifications;
        [self get_Ride_Details:_ride_id_From_Notifications];
        which_Ride_this_is=MEMBER_SCHEDULE_OR_FREE_RIDE;
    }
    else{
        //Owners only free or schedule rides
        if ([appDelegate.arrayOfSelectedFrndsAndRiders count]>0 || [appDelegate.arrayOfSelectedGroupsAndRiders count]>0) {
            NSLog(@"Ride Id is %@",appDelegate.ride_ID_Api);
            if ([appDelegate.ride_ID_Api isEqualToString:@""]) {
                [self start_immediate_ride_request];
                // this condition is for --->Free Ride With Friends or Groups----->before
                // this condition is for --->Free Ride changed now no routes and no friends------>present
            }
            else{
                if (_updateRideStatus==YES) {
                    //when owner changed his schedule ride details
                    NSLog(@"Changed route ID From Scheduled Ride %@", appDelegate.select_Route_Id_For_CreateAndFreeRide);
                    NSLog(@"Changed Frnds ID From Scheduled Ride %@",appDelegate.arrayOfSelectedFrndsAndRiders);
                    NSLog(@"Changed Groups ID From Scheduled Ride %@",appDelegate.arrayOfSelectedGroupsAndRiders);
                    NSLog(@"Remove frnd Id from Scheduled ride %@",appDelegate.arrayOFRemoveFrndsID);
                    [self update_ride_request_rideDashboard:appDelegate.ride_ID_Api];
                }
                else{
                    [self get_Ride_Details:appDelegate.ride_ID_Api];   //when owner  his schedule ride
                }
            }
            which_Ride_this_is=GROUP_RIDE;
        }
        else{
            if ([appDelegate.ride_ID_Api isEqualToString:@""]) {
                //Free Ride With Out Friends With Route or with out Route----->before
                //Free Ride changed now no routes and no friends------>present
                NSLog(@"Ride Id is %@",appDelegate.ride_ID_Api);
                appDelegate.ride_ID_Api=@"";
                appDelegate.select_Route_Id_For_CreateAndFreeRide;
                NSLog(@"solo ride with selected Route Id : %@",appDelegate.select_Route_Id_For_CreateAndFreeRide);
                if ([appDelegate.select_Route_Id_For_CreateAndFreeRide isEqualToString:@""]) {
                    //Free Ride Owner only
                    
                }
                else{
                    NSLog(@"route id%@",appDelegate.select_Route_Id_For_CreateAndFreeRide);
                    path2= [GMSMutablePath path];
                    dispatch_async(dispatch_get_main_queue(), ^
                                   {
                                       [self Route_Detail_Service:appDelegate.select_Route_Id_For_CreateAndFreeRide];
                                   });
                    //Free Ride with route this will not used now
                }
                which_Ride_this_is=SOLO_RIDE;
            }
            else
            {
                //Schedule Ride With Out Friends With group or with out Route
                if (_updateRideStatus==YES) {
                    NSLog(@"Changed route ID From Scheduled Ride %@", appDelegate.select_Route_Id_For_CreateAndFreeRide);
                    NSLog(@"Changed Frnds ID From Scheduled Ride %@",appDelegate.arrayOfSelectedFrndsAndRiders);
                    NSLog(@"Changed Groups ID From Scheduled Ride %@",appDelegate.arrayOfSelectedGroupsAndRiders);
                    NSLog(@"Remove frnd Id from Scheduled ride %@",appDelegate.arrayOFRemoveFrndsID);
                    [self update_ride_request_rideDashboard:appDelegate.ride_ID_Api];
                }
                else{
                    [self get_Ride_Details:appDelegate.ride_ID_Api];  //Schedule Ride With Friends or Groups owener clicks
                }
                which_Ride_this_is=GROUP_RIDE;
            }
        }
    }
    
    //Display Annotation
    // Show coach marks
    BOOL coachMarksShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"MPCoachMarksShown_RideDashboard"];
    if (coachMarksShown == NO) {
        // Don't show again
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MPCoachMarksShown_RideDashboard"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self showAnnotation];
    }
}

#pragma mark - Annotations
-(void)showAnnotation
{
    [self.view layoutIfNeeded];
    
    // Setup coach marks
    CGRect coachmark1 = CGRectMake (_mapToggle_btn.frame.origin.x, ([UIScreen mainScreen].bounds.size.height - 60), _mapToggle_btn.frame.size.width, _mapToggle_btn.frame.size.height);
    CGRect coachmark2 = CGRectMake (_startAndPause_btn.frame.origin.x, ([UIScreen mainScreen].bounds.size.height - 80), _startAndPause_btn.frame.size.width, _startAndPause_btn.frame.size.height);
    CGRect coachmark3 = CGRectMake(_finish_btn.frame.origin.x, ([UIScreen mainScreen].bounds.size.height - 70), _finish_btn.frame.size.width, _finish_btn.frame.size.height);
    
    NSArray *coachMarks;
    
    if (_boolvalue_rideToggle == YES) {
        // Setup coach marks
        coachMarks = @[
                       @{
                           @"rect": [NSValue valueWithCGRect:coachmark1],
                           @"caption": @"Tap here to swap to the map view",
                           @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                           @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
                           @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_LEFT]
                           //@"showArrow":[NSNumber numberWithBool:YES]
                           },
                       @{
                           @"rect": [NSValue valueWithCGRect:coachmark2],
                           @"caption": @"Tap here to start the ride",
                           @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                           @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                           @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_CENTER]
                           //@"showArrow":[NSNumber numberWithBool:YES]
                           },
                       @{
                           @"rect": [NSValue valueWithCGRect:coachmark3],
                           @"caption": @"Tap here to stop the ride",
                           @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                           @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
                           @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_RIGHT],
                           //@"showArrow":[NSNumber numberWithBool:YES]
                           },

                       ];
    }
    else
    {
        // Setup coach marks
        coachMarks = @[
                       @{
                           @"rect": [NSValue valueWithCGRect:coachmark1],
                           @"caption": @"Tap here to swap to the dashboard",
                           @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                           @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
                           @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_LEFT]
                           //@"showArrow":[NSNumber numberWithBool:YES]
                           },
//                       @{
//                           @"rect": [NSValue valueWithCGRect:coachmark2],
//                           @"caption": @"Tap here to start the ride",
//                           @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
//                           @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
//                           @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_CENTER]
//                           //@"showArrow":[NSNumber numberWithBool:YES]
//                           },
//                       @{
//                           @"rect": [NSValue valueWithCGRect:coachmark3],
//                           @"caption": @"Tap here to stop the ride",
//                           @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
//                           @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
//                           @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_RIGHT],
//                           //@"showArrow":[NSNumber numberWithBool:YES]
//                           },
                       ];

    }
    
    
    
    MPCoachMarks *coachMarksView = [[MPCoachMarks alloc] initWithFrame:self.navigationController.view.bounds coachMarks:coachMarks];
    //[self.navigationController.view addSubview:coachMarksView];
    [[UIApplication sharedApplication].keyWindow addSubview:coachMarksView];
    [coachMarksView start];
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = YES;
    if ([appDelegate.check_in_Clicked isEqualToString:@"YES"]) {
        appDelegate.check_in_Clicked=@"NO";
        
        double check_in_lat,check_in_Long;
        check_in_lat=[appDelegate.check_in_Lat doubleValue];
        check_in_Long=[appDelegate.check_in_Long doubleValue];
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake(check_in_lat,check_in_Long);
        marker.appearAnimation = kGMSMarkerAnimationPop;
        marker.icon = [UIImage imageNamed:@"checkin_marker_bg.png"];
        marker.title=appDelegate.checkin_marker_title;
        marker.map = mapView;
    }
    else{
        [self check_Location_On_Or_Off];
    }
}
-(void)viewWillDisappear:(BOOL)animated{
    if ([back_Clicked isEqualToString:@"Rides"]) {
        self.navigationController.navigationBarHidden = NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL)check_Location_On_Or_Off{
    if ([CLLocationManager locationServicesEnabled]){
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
        {
            UIAlertController *alertview = [UIAlertController alertControllerWithTitle:@"LOCATION" message:@"Turn on your App Location" preferredStyle:UIAlertControllerStyleAlert];
            
            [alertview addAction:[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root=Privacy:Location_Services"]];
            }]];
            [self presentViewController:alertview animated:YES completion:nil];
            return NO;
        }
        else
            return YES;
    }
    else{
        
        UIAlertController *alertview = [UIAlertController alertControllerWithTitle:@"LOCATION" message:@"Turn ON Your Location" preferredStyle:UIAlertControllerStyleAlert];
        
        [alertview addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
            // Cancel button tappped.
            [self dismissViewControllerAnimated:YES completion:^{
            }];
            [self check_Location_On_Or_Off];
        }]];
        [alertview addAction:[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
            
            // Distructive button tapped.
            //                [self dismissViewControllerAnimated:YES completion:^{
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root=Privacy:Location_Services"]];
            //                }];
        }]];
        
        [self presentViewController:alertview animated:YES completion:nil];
        return NO;
    }
    
    
}
-(void)getAdrressFromLatLong :(NSString *)call_for location_twod:(CLLocationCoordinate2D)adreess_Coordinates
{
    
    if (adreess_Coordinates.latitude==0.0 && adreess_Coordinates.longitude ==0.0) {
        return;
    }
    NSString *urlString = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&amp;sensor=false",adreess_Coordinates.latitude,adreess_Coordinates.longitude];
    

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager POST:urlString parameters:nil progress:nil
          success:^(NSURLSessionDataTask *task, id responseObject)
     
     {
         NSDictionary* resultDict = nil;
         NSError* error = nil;
         resultDict = [NSJSONSerialization JSONObjectWithData:responseObject
                                                      options:kNilOptions error:&error];
         
         NSLog(@"adress%@",resultDict);
         if (resultDict.count>0) {
             NSArray *resultsarray=[resultDict valueForKey:@"results"];
             if (resultsarray.count>0) {
                 NSDictionary *address_components=[resultsarray objectAtIndex:0];
                 NSString *formatted_address=[address_components valueForKey:@"formatted_address"];
                 NSArray *items = [formatted_address componentsSeparatedByString:@","];
                 if (items.count>0) {
                     NSMutableArray *item=[items mutableCopy];
                     [item removeObjectAtIndex:items.count-1];
                     if ([call_for isEqualToString:@"start"]) {
                         ride_strat_adress = [NSString stringWithFormat:@"%@",[item componentsJoinedByString:@","]];
                     }
                     else
                         ride_end_Adress=[NSString stringWithFormat:@"%@",[item componentsJoinedByString:@","]];
                 }
             }
         }
     }
      failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         if ([call_for isEqualToString:@"start"]) {
             ride_strat_adress=@"";
         }
         else
             ride_end_Adress=@"";
     }];
}
#pragma mark Weather
-(void)weatherMethod:(NSString *)call_From
{
    
    weatherLatitude =  map_currentlocation_coordinate.latitude;
    weatherLongitutde = map_currentlocation_coordinate.longitude;
    
    
    //    NSString *requestURL=[NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/weather?lat=%@&lon=%@&appid=45f4570d4bc107b936522ebb9cd3568e",lat,log];
    
    //    NSString *requestURL = @"http://api.openweathermap.org/data/2.5/weather?lat=17.057571&lon=79.262029&appid=45f4570d4bc107b936522ebb9cd3568e";
    if (weatherLatitude==0.0 && weatherLongitutde ==0.0) {
        return;
    }
    NSString *requestURL=[NSString stringWithFormat:@"http://api.wunderground.com/api/956eca8468cf6350/conditions/forecast/alert/q/%f,%f.json",weatherLatitude,weatherLongitutde ];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager POST:requestURL parameters:nil progress:nil
          success:^(NSURLSessionDataTask *task, id responseObject)
     
     {
         
         NSDictionary* resultDict = nil;
         
         NSError* error = nil;
         
         resultDict = [NSJSONSerialization JSONObjectWithData:responseObject
                       
                                                      options:kNilOptions error:&error];
         
         //         NSLog(@"%@",resultDict);
         
         NSString *tempature=[[resultDict objectForKey:@"current_observation"]  objectForKey:@"temp_f"];
         float temp=[[NSString stringWithFormat:@"%@",tempature]floatValue ];
         _label_temprature.text=[NSString stringWithFormat:@"%.2f",temp];
         NSString *icon=[[resultDict objectForKey:@"current_observation"]  objectForKey:@"icon"];
         NSString *  weather=[[resultDict objectForKey:@"current_observation"]  objectForKey:@"weather"];
         _label_weather_condition.text=[NSString stringWithFormat:@"%@",weather];
         NSString *city=[[[resultDict objectForKey:@"current_observation"] objectForKey:@"display_location"]  objectForKey:@"city"];
         _label_wheather_placeName.text=[NSString stringWithFormat:@"%@",city];
         
         
         NSLog(@"%@",tempature);
         NSLog(@"%@",icon);
         NSLog(@"%@",city);
         NSDate *currentTime = [NSDate date];
         NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
         [dateFormatter setDateFormat:@"hh:mm a"];
         NSString *strCurrentTime = [dateFormatter stringFromDate:currentTime];
         NSLog(@"str Current Time %@",strCurrentTime);
         NSString *dayTime = @"";
         if ([strCurrentTime floatValue]>= 0 && [strCurrentTime floatValue]< 6)
         {
             dayTime = @"Night";
             
         }
         else if ([strCurrentTime floatValue]>= 6 && [strCurrentTime floatValue]< 12)
         {
             dayTime = @"Morning";
         }
         else if ([strCurrentTime floatValue]>= 12 && [strCurrentTime floatValue]< 16)
         {
             dayTime = @"Afternoon";
         }
         else if ([strCurrentTime floatValue]>= 16 && [strCurrentTime floatValue]< 21)
         {
             dayTime = @"Evening";
         }
         else if ([strCurrentTime floatValue]>= 21 && [strCurrentTime floatValue]< 24)
         {
             dayTime =@ "Night";
         }
         if ([call_From isEqualToString:@"locations"]) {
             
         }
         else
         {
             if ([appDelegate.ride_ID_Api isEqualToString:@""] && ![_is_From isEqualToString:@"HOME"]) {
                 
                 self.ride_Name_label.text=[NSString stringWithFormat:@"%@ %@ at %@", _label_wheather_placeName.text,dayTime,strCurrentTime];
                 appDelegate.FreeRide_Name=self.ride_Name_label.text;
                 ride_Title=self.ride_Name_label.text;
             }
             else{
                 self.ride_Name_label.text=ride_Title;
                 appDelegate.FreeRide_Name=self.ride_Name_label.text;
             }
         }
         NSLog(@"Route Name %@",self.route_Name_Label.text);
         if ([strCurrentTime floatValue] >= 18.00 || [strCurrentTime floatValue]  <= 6.00)
             
         {
             NSLog(@"It's night time");
         }
         
         else
             
         {
             
             NSLog(@"It's day time");
         }
         
         if ([icon isEqualToString:@"chanceflurries"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"chancerain"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"chancesleet"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"chancesnow"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"chancetstorms"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"clear"])
             
         {
             
             NSLog(@"It's day clear");
             
         }
         
         else if ([icon isEqualToString:@"cloudy"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"flurries"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"fog"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"hail"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"hazy"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"light_rain"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"mostlycloudy"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"mostlysunny"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"na"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"nt_chanceflurries"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"nt_chancerain"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"nt_chancesleet"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"nt_chancesnow"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"nt_chancetstorms"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"nt_clear"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"nt_cloudy"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"nt_flurries"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"nt_fog"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"nt_hazy"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"nt_mostlycloudy"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"nt_mostlysunny"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"nt_partlycloudy"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"nt_rain"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"nt_sleet"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"nt_snow"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"nt_sunny"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"nt_tstorms"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"nt_unknown"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"partlycloudy"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"partlysunny"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"rain"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"shower2"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"sleet"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"snow"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"sunny"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"tstorm1"])
             
         {
             
             
             
         }
         
         else if ([icon isEqualToString:@"tstorms"])
             
         {
             
             
             
         }
         
         else
             
         {
             
             
             
         }
         
     }
     
          failure:^(NSURLSessionDataTask *task, NSError *error)
     
     {
         
         if ([call_From isEqualToString:@"locations"]) {
             
         }
         else
         {
             if (![_is_From isEqualToString:@"HOME"]) {
                 ride_Title=@"";
                 self.ride_Name_label.text=ride_Title;
                 appDelegate.FreeRide_Name=self.ride_Name_label.text;
             }
        }
     }
     
     ];
}

#pragma mark ---About Ride (get set_up ride details or notify join rides for invited users or stat service for owners for setup ride)
-(void)update_ride_request_rideDashboard  :(NSString *)ride_ID_is

{
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [self network_Alert:@"update_ride"];
        return;
    }
    self.view.userInteractionEnabled=NO;
    indicaterview.hidden=NO;
    //    {
    //        "user_id":"",
    //        "ride_id":"",
    //        "type":"ride/edit"
    //        "route_id":"",
    //        "group_invite_ids":[],
    //        "friend_invite_ids":[],
    //        "remove_friend_invite_ids":[]
    //    }
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:@"ride" forKey:@"request_type"];
    [dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
    [dict setObject:appDelegate.select_Route_Id_For_CreateAndFreeRide forKey:@"route_id"];
    [dict setObject:appDelegate.Ride_Name_From_Schedule forKey:@"ride_title"];
    [dict setObject:ride_ID_is forKey:@"ride_id"];
    
    if ([appDelegate.arrayOfSelectedFrndsAndRiders count]>0)
        
    {
        [dict setObject:appDelegate.arrayOfSelectedFrndsAndRiders forKey:@"friend_invite_ids"];
        [dict setObject:appDelegate.arrayOfSelectedFrndsAndRiders forKey:@"friend_invite_ids"];
    }
    
    else
    {
        appDelegate.arrayOfSelectedFrndsAndRiders=[[NSMutableArray alloc]init];
        [dict setObject:appDelegate.arrayOfSelectedFrndsAndRiders forKey:@"friend_invite_ids"];
    }
    
    if ([appDelegate.arrayOfSelectedGroupsAndRiders count]>0) {
        [dict setObject:appDelegate.arrayOfSelectedGroupsAndRiders forKey:@"group_invite_ids"];
    }
    else{
        appDelegate.arrayOfSelectedGroupsAndRiders=[[NSMutableArray alloc]init];
        [dict setObject:appDelegate.arrayOfSelectedGroupsAndRiders forKey:@"group_invite_ids"];
    }
    
    if ([appDelegate.arrayOFRemoveFrndsID count]>0) {
        [dict setObject:appDelegate.arrayOFRemoveFrndsID forKey:@"remove_friend_invite_ids"];
    }
    else{
        appDelegate.arrayOFRemoveFrndsID=[[NSMutableArray alloc]init];
        [dict setObject:appDelegate.arrayOFRemoveFrndsID forKey:@"remove_friend_invite_ids"];
    }
    
    NSLog(@"%@",dict);
    
    [SHARED_API editRideRequestsWithParams:dict withSuccess:^(NSDictionary *response) {
        
        NSLog(@"responce is %@",response);
        
        if ([[response valueForKey:STATUS] isEqualToString:SUCCESS]) {
            [SHARED_HELPER showAlert:ridecreated];
            int duration = 2;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [self get_Ride_Details:ride_ID_is];
            });
            appDelegate.Ride_Name_From_Schedule=@"";
        }
        else{
            [SHARED_HELPER showAlert:@"Failed to update ride."];
            int duration = 2;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [self Back_Action:0];
            });
        }
        self.view.userInteractionEnabled=YES;
        indicaterview.hidden=YES;
    } onfailure:^(NSError *theError) {
        
        indicaterview.hidden=YES;
        self.view.userInteractionEnabled=YES;
        [self network_Alert:@"update_ride"];
    }];
}


-(void)get_Ride_Details:(NSString *)ride_id_Is{
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [self network_Alert:@"get_ride"];
        return;
    }
    indicaterview.hidden=NO;
    self.view.userInteractionEnabled=NO;
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
    [dict setObject:ride_id_Is forKey:@"ride_id"];
    [dict setObject:@"0" forKey:@"type"];
    
    [SHARED_API rideDetailRequestsWithParams:dict withSuccess:^(NSDictionary *response) {
        NSLog(@" Ride_Detail responce is : %@",response);
        if ([[response valueForKey:STATUS]isEqualToString:SUCCESS]) {
            
            NSDictionary *ride_Data = [response valueForKey:@"ride_data"];
            NSString *start_adress=[ride_Data valueForKey:@"ride_start_address"];
            //NSString *start_latitude=[ride_Data valueForKey:@"ride_start_latitude"];
            //NSString *start_longitude=[ride_Data valueForKey:@"ride_start_longitude"];
            NSString *ride_title_is_=[ride_Data valueForKey:@"ride_title"];
            NSString *ride_type=[ride_Data valueForKey:@"ride_type"];
            NSString *route_id=[NSString stringWithFormat:@"%@",[ride_Data valueForKey:@"route_id"]];
            NSString *is_route_selected=[NSString stringWithFormat:@"%@",[ride_Data valueForKey:@"is_route_selected"]];
            created_ride_description=[NSString stringWithFormat:@"%@",[ride_Data valueForKey:@"ride_description"]];
            ride_owner_id=[NSString stringWithFormat:@"%@",[ride_Data valueForKey:@"created_user_id"]];
            
            
            route_end_Adress=@"";
            ride_Start_Lat=@"";
            ride_Start_Long=@"";
            ride_End_Lat=@"";
            ride_End_Long=@"";
            ride_end_Adress=@"";
            ride_strat_adress=@"";
            
            if ([ride_type isEqualToString:@"free_ride"]) {
                [[AppHelperClass appsharedInstance] setRideType:@"free_ride"];
            }
            else{
                [[AppHelperClass appsharedInstance] setRideType:@"setup_ride"];
            }
            
            if ([ride_title_is_ isEqual:[NSNull null]] || [ride_title_is_ isEqualToString:@""] || [ride_title_is_ isEqualToString:@"null"] || ride_title_is_ == nil) {
                ride_Title=@"";
            }
            else
            {
                ride_Title=ride_title_is_;
            }
            self.ride_Name_label.text=ride_Title;
            
            if ([start_adress isEqual:[NSNull null]] || [start_adress isEqualToString:@""] || [start_adress isEqualToString:@"null"] || start_adress == nil) {
                route_strat_adress=@"";
                ride_strat_adress=@"";
            }
            else
            {
                route_strat_adress=start_adress;
                ride_strat_adress=start_adress;
            }
            
            if ([is_route_selected isEqualToString:@"no"]) {
                appDelegate.select_Route_Id_For_CreateAndFreeRide=@"";
                if([_is_From isEqualToString:@"HOME"])
                {
                    _startAndPause_btn.tag=0;
                    [self onClick_startAndPause_btn:0];
                }
                    
                
            }
            else{
                if ([route_id isEqualToString:@"0"]) {
                    appDelegate.select_Route_Id_For_CreateAndFreeRide=@"";
                    
                }
                else{
                    appDelegate.select_Route_Id_For_CreateAndFreeRide=route_id;
                    path2= [GMSMutablePath path];
                    dispatch_async(dispatch_get_main_queue(), ^
                                   {
                                       [self Route_Detail_Service:appDelegate.select_Route_Id_For_CreateAndFreeRide];
                                   });
                }
            }
            
            indicaterview.hidden=YES;
            self.view.userInteractionEnabled=YES;
        }
        else{
            indicaterview.hidden=YES;
            self.view.userInteractionEnabled=YES;
            
            [SHARED_HELPER showAlert:@"Unable to get ride details."];
            
            int duration = 2;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [self Back_Action:0];
            });
        }
    } onfailure:^(NSError *theError) {
        
        indicaterview.hidden=YES;
        self.view.userInteractionEnabled=YES;
        [self network_Alert:@"get_ride"];
    }];
}


-(void)start_immediate_ride_request{
    self.view.userInteractionEnabled=NO;
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [SHARED_HELPER showAlert:NonetworkError];
        return;
    }
    indicaterview.hidden=NO;
    
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSString *strCurrentTime = [dateFormatter stringFromDate:currentTime];
    
    if ([[[AppHelperClass appsharedInstance] rideType] isEqualToString:@"free_ride"]) {
        ride_Title=[NSString stringWithFormat:@"%@ %@",[Defaults valueForKey:@"UserName"],strCurrentTime];
        [dict setObject:ride_Title forKey:@"ride_title"];
    }
    else{
        ride_Title=[NSString stringWithFormat:@"(%@)'s-%@",[Defaults valueForKey:@"UserName"],strCurrentTime];
        [dict setObject:ride_Title forKey:@"ride_title"];
    }
    self.ride_Name_label.text=ride_Title;
    appDelegate.ride_ID_Api=@"0";
    NSDateFormatter *dateFormatter1=[[NSDateFormatter alloc] init];
    //            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatter1 setDateFormat:@"MM/dd/yyyy"];
    NSLog(@"ride Start Date %@",[dateFormatter1 stringFromDate:[NSDate date]]);
    NSString *immidiate_ride_start=[dateFormatter1 stringFromDate:[NSDate date]];
    
    NSDateFormatter *dateFormatter2=[[NSDateFormatter alloc] init];
    [dateFormatter2 setDateFormat:@"HH:mm"];
    NSString *immidiate_ride_start_time=[dateFormatter2 stringFromDate:[NSDate date]];
    
    
    
    
    [dict setObject:@"" forKey:@"ride_description"];
    [dict setObject:@"" forKey:@"ride_start_point_address"];
    [dict setObject:immidiate_ride_start forKey:@"ride_start_date"];
    [dict setObject:immidiate_ride_start_time forKey:@"ride_start_time"];
    [dict setObject:@"" forKey:@"meeting_point_address"];
    [dict setObject:@"" forKey:@"meeting_time"];
    [dict setObject:@"" forKey:@"meeting_date"];
    [dict setObject:@"" forKey:@"meeting_point_latitude"];
    [dict setObject:@""  forKey:@"meeting_point_longitude"];
    [dict setObject:@"" forKey:@"ride_cover_image"];
    
    [dict setObject:appDelegate.ride_Random_Number forKey:@"random_number"];
    [dict setObject:[[AppHelperClass appsharedInstance] rideType] forKey:@"ride_type"];
    [dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
    [dict setObject:appDelegate.select_Route_Id_For_CreateAndFreeRide forKey:@"route_id"];
    
    if ([appDelegate.arrayOfSelectedFrndsAndRiders count]>0) {
        [dict setObject:appDelegate.arrayOfSelectedFrndsAndRiders forKey:@"friend_invite_ids"];
        
    }
    else{
        appDelegate.arrayOfSelectedFrndsAndRiders=[[NSMutableArray alloc]init];
        [dict setObject:appDelegate.arrayOfSelectedFrndsAndRiders forKey:@"friend_invite_ids"];
    }
    if ([appDelegate.arrayOfSelectedGroupsAndRiders count]>0) {
        [dict setObject:appDelegate.arrayOfSelectedGroupsAndRiders forKey:@"group_invite_ids"];
        
    }
    else{
        appDelegate.arrayOfSelectedGroupsAndRiders=[[NSMutableArray alloc]init];
        [dict setObject:appDelegate.arrayOfSelectedGroupsAndRiders forKey:@"group_invite_ids"];
    }
    
    NSLog(@"%@",dict);
    [SHARED_API startimediateRideRequestsWithParams:dict withSuccess:^(NSDictionary *response) {
        NSLog(@"responce is %@",response);
        if ([[response valueForKey:STATUS] isEqualToString:SUCCESS]) {
            [self create_Ride_Sucsess];
            appDelegate.ride_ID_Api=[NSString stringWithFormat:@"%@",[response valueForKey:@"ride_id"]];
            self.view.userInteractionEnabled=YES;
            
            if ([appDelegate.select_Route_Id_For_CreateAndFreeRide isEqualToString:@""]) {
                
            }
            else{
                NSLog(@"route id%@",appDelegate.select_Route_Id_For_CreateAndFreeRide);
                path2= [GMSMutablePath path];
                dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   [self Route_Detail_Service:appDelegate.select_Route_Id_For_CreateAndFreeRide];
                               });
            }
        }
        else
        {
            
        }
    } onfailure:^(NSError *theError) {
        appDelegate.ride_ID_Api=@"";
        ride_Title=@"";
        indicaterview.hidden=YES;
        self.view.userInteractionEnabled=YES;
        [SHARED_HELPER showAlert:ServerConnection];
    }];
}

-(void)create_Ride_Sucsess{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        appDelegate.arrayOfSelectedFrndsAndRiders=[[NSMutableArray alloc]init];
        appDelegate.arrayOfSelectedGroupsAndRiders=[[NSMutableArray alloc]init];
        indicaterview.hidden=YES;
    });
}
//"{
//""ride_id"":"""",
//""created_user_id"":""""
//}"

-(void)notify_users_request{
    
    indicaterview.hidden=NO;
    NSMutableDictionary *dict = [NSMutableDictionary new];
    
    [dict setObject:appDelegate.ride_ID_Api forKey:@"ride_id"];
    [dict setObject:[Defaults valueForKey:User_ID] forKey:@"created_user_id"];
    
    NSLog(@"notifu Users %@",dict);
    [SHARED_API notify_users_RequestsWithParams:dict withSuccess:^(NSDictionary *response) {
        NSLog(@"responce is %@",response);
        if ([[response valueForKey:STATUS] isEqualToString:SUCCESS]) {
            indicaterview.hidden=YES;
            [self Start_Ride_OR_Pause_Ride:0];
        }
        else if ([[response valueForKey:STATUS] isEqualToString:FAIL])
        {
            self.startAndPause_btn.tag=0;
            [SHARED_HELPER showAlert:NOTIFYRIDERSFAIL];
            indicaterview.hidden=YES;
        }
        //        Notify sucess then only ride will be started
    } onfailure:^(NSError *theError) {
        indicaterview.hidden=YES;
        //        retry Popup
        //        Service Fail - With no Internet
        //        Can\'t connect. Something went wrong, please try again
        //
        //        Service Fail - other response code other than 200 and 0
        //        Can\'t connect. Check your network and try again.
        self.startAndPause_btn.tag=0;
        [SHARED_HELPER showAlert:ServerConnection];
        
    }];
}
-(void)join_Riders_Api{
    indicaterview.hidden=NO;
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    
    [dict setObject:appDelegate.ride_ID_Api forKey:@"ride_id"];
    [dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
    
    NSLog(@"notifu Users %@",dict);
    [SHARED_API Join_RIdes :dict withSuccess:^(NSDictionary *response) {
        NSLog(@"responce is %@",response);
        if ([[response valueForKey:STATUS] isEqualToString:SUCCESS]) {
            
            _riders_Count.text=[NSString stringWithFormat:@"%@",[response valueForKey:@"riders_count"]];
            [self Start_Ride_OR_Pause_Ride:0];
        }
        else if ([[response valueForKey:STATUS] isEqualToString:@"Ride_Completed"])
        {
            
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@"Oops! Ride was Completed!"
                                          message:@"The ride which you want to join, it's already completed.\nUnable to join this ride."
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"ok"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
//                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     [self Back_Action:0];
                                 }];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else{
            self.startAndPause_btn.tag=0;
            [SHARED_HELPER showAlert:@"Can't join the ride. Please try again."];
        }
        indicaterview.hidden=YES;
        //    after joining then only ride will be started
    } onfailure:^(NSError *theError) {
        self.startAndPause_btn.tag=0;
        indicaterview.hidden=YES;
        [SHARED_HELPER showAlert:ServerConnection];
        
    }];
}
-(void)user_update_location:(double)current_latitude :(double)current_longtitude is_show:(NSString *) is_show
{
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
    [dict setObject:[NSNumber numberWithDouble:current_latitude] forKey:@"current_latitude"];
    [dict setObject:[NSNumber numberWithDouble:current_longtitude] forKey:@"current_longitude"];
    [dict setObject:is_show forKey:@"is_show"];
    NSLog(@"Update user location : %@",dict);
    
    [SHARED_API updateUserlocationRequestsWithParams:dict withSuccess:^(NSDictionary *response) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if([[response objectForKey:STATUS] isEqualToString:SUCCESS])
            {
                NSLog(@"Update user response : %@",response);
                
            }
            else
            {
                if ([[response objectForKey:STATUS] isEqualToString:FAIL])
                {
                    NSLog(@"Update user response : %@",response);
                }
            }
        });
        
    } onfailure:^(NSError *theError) {
        
    }];
    
}
-(void)is_From_Home{
    
    NSArray *pending_ride=[self get_pending_Rides_From_DB];
    if (pending_ride.count>0) {
        rides_object=[pending_ride objectAtIndex:0];
        local_Ride_ID=[[rides_object valueForKey:all_table_local_rid] intValue];
        routes_object=[self get_pending_Route_From_DB];
        
        
        
        ride_strat_adress=[rides_object valueForKey:ride_table_start_adress];
        ride_end_Adress=@"";
        ride_Title=[rides_object valueForKey:ride_table_name];
        appDelegate.FreeRide_Name=ride_Title;
        self.ride_Name_label.text=ride_Title;
        [self weatherMethod:@""];
        appDelegate.ride_Random_Number=[rides_object valueForKey:ride_table_randomNumber];
        
        NSString *ride_total_time=[rides_object valueForKey:ride_table_totlaTime];
        NSArray *ride_t_time=[ride_total_time componentsSeparatedByString:@":"];
        totalTime_countDown=[self secondsFromArray:ride_t_time];
        
        NSString *ride_time=[rides_object valueForKey:ride_table_time];
        ride_t_time=[ride_time componentsSeparatedByString:@":"];
        
        
        rideTime_coutDown=[self secondsFromArray:ride_t_time];
        miles=[[rides_object valueForKey:ride_table_totalDist] floatValue];
        ride_Dist=miles *1609.344;
        marker_count=miles;
        loc_Update_count=miles;
        maxElevation=[[routes_object valueForKey:route_table_maxElevation] floatValue];//its from route
        minElevation=[[routes_object valueForKey:route_table_minElevation] floatValue];
        maxspeed=[[rides_object valueForKey:ride_table_maxSpeed]floatValue];
        avgSpeed=[[rides_object valueForKey:ride_table_avgSpeed]floatValue];
        original_Marker_Val=[[rides_object valueForKey:ride_table_dflt_marker_dist]intValue];
        
        
        _label_totalTime_rideDashboard.text=ride_total_time;
        _label_totalTime_mapRideDashboard.text=ride_total_time;
        
        _label_rideTime_rideDashboard.text =ride_time;
        
        
        
        _label_totalDistance_rideDashboard.text = [NSString stringWithFormat:@"%.2f",miles];
        _labe_totalDistance_mapRideDashboard.text= [NSString stringWithFormat:@"%.2f",miles];
        _label_elevation_rideDashboard.text = [NSString stringWithFormat:@"%.0f", maxElevation];
        _label_maxSpeed_rideDashboard.text = [NSString stringWithFormat:@"%2d",maxspeed];
        _label_averageSpeed_rideDashboard.text = [NSString stringWithFormat: @"%0.2f",avgSpeed];
        _label_averageSpeed_mapRideDashboard.text=[NSString stringWithFormat: @"%0.2f",avgSpeed];
        
        
        ride_Start_Lat=[rides_object valueForKey:ride_table_start_lat];
        ride_Start_Long=[rides_object valueForKey:ride_table_start_long];
        
        path = [GMSMutablePath path];
        [path addCoordinate:CLLocationCoordinate2DMake([ride_Start_Lat doubleValue], [ride_Start_Long doubleValue])];
        polyline = [GMSPolyline polylineWithPath:path];
        polyline.strokeColor = [UIColor yellowColor];
        polyline.strokeWidth = 5.0f;
        polyline.geodesic = YES;
        polyline.map = mapView;
        
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake([ride_Start_Lat floatValue],[ride_Start_Long floatValue]);
        marker.appearAnimation = kGMSMarkerAnimationPop;
        marker.title=Start_Point_Title;
        marker.icon = [UIImage imageNamed:@"red_marker.png"];
        marker.map = mapView;
        
        ride_Start_date_String=[rides_object valueForKey:ride_table_start_date];
        ride_Start_time_String=[rides_object valueForKey:ride_table_start_time];
        [[AppHelperClass appsharedInstance] setRideType:[rides_object valueForKey:ride_table_type]];
        
        [self Show_path_for_unexpected_stoped_rides:Locations_Table];
        
        if ([rides_object.ride_id isEqualToString:@""]) {
            appDelegate.ride_ID_Api=@"";
            
            if ([[routes_object valueForKey:route_table_id] isEqualToString:@""]) {
                appDelegate.select_Route_Id_For_CreateAndFreeRide=@""; //free_ride with no route and friends
                 _startAndPause_btn.tag=0;
                [self onClick_startAndPause_btn:0];
            }
            else
            {
                appDelegate.select_Route_Id_For_CreateAndFreeRide=[routes_object valueForKey:route_table_id];
                [self Route_Detail_Service:appDelegate.select_Route_Id_For_CreateAndFreeRide];
            }
        }
        else
        {
            appDelegate.ride_ID_Api=rides_object.ride_id;
            [self get_Ride_Details:appDelegate.ride_ID_Api];
        }
    }
    else
    {
        [SHARED_HELPER showAlert:ServiceFail];
        
        int duration = 2; // duration in seconds
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            
            [self Back_Action:0];
            
        });
        
    }
}


#pragma mark Route_Detail_Service for ride with route set_up rides
-(void)Route_Detail_Service:(NSString *)route_id{
    
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [self network_Alert:@"get_route"];
        return;
    }
    indicaterview.hidden=NO;
    [SHARED_API routedetailRequestsWithParams:route_id withSuccess:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           NSLog(@"responce is %@",response);
                           
                           if ([[response valueForKey:STATUS] isEqualToString:SUCCESS])
                           {
                               NSDictionary *route_data = [response valueForKey:@"route_data"];
                               [self route_Detail_Sucsess:route_data];
                           }
                           else
                           {
                               selected_Route_GPX=@"";
                               [self network_Alert:@"get_route"];
                               indicaterview.hidden=YES;
                           }
                      });
        
    } onfailure:^(NSError *theError) {
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           selected_Route_GPX=@"";
                           [self network_Alert:@"get_route"];
                           indicaterview.hidden=YES;
                       });
    }];
}

-(void)route_Detail_Sucsess:(NSDictionary *)route_data{
    
    selected_route_title=[route_data valueForKey:@"route_title"];
    self.route_Name_Label.text=selected_route_title;
    original_Marker_Val=[[route_data valueForKey:@"route_default_marker_distance"] intValue];
    selected_route_owner_id=[NSString stringWithFormat:@"%@",[route_data valueForKey:@"created_by"]];
    
    NSString *des_is=[NSString stringWithFormat:@"%@",[route_data valueForKey:@"route_description"]];
    if ([des_is isEqual:[NSNull null]] || [des_is isEqualToString:@""] || [des_is isEqualToString:@"null"] || des_is == nil) {
        select_Route_Des=@"";
    }
    else{
        select_Route_Des=des_is;
    }
    
    NSString * gpx_str = [NSString stringWithFormat:@"%@",[route_data valueForKey:@"route_gpx_file"]];
    selected_Route_GPX=gpx_str;
    
    NSURL *url = [NSURL URLWithString:gpx_str];
    NSData*fileData = [NSData dataWithContentsOfURL:url];
    
    add_places =[[NSMutableArray alloc]init];
    tracks_array=[[NSMutableArray alloc]init];
    way_array=[[NSMutableArray alloc]init];
    NSError *error = nil;
    NSDictionary *dict = [XMLReader dictionaryForXMLData:fileData
                                                 options:XMLReaderOptionsProcessNamespaces
                                                   error:&error];
    
    
    //    NSString *add_place_str = [[[[dict valueForKey:@"gpx"] valueForKey:@"extensions"] valueForKey:@"addplace"] valueForKey:@"place"];
    NSString *traks_str=[[[[dict valueForKey:@"gpx"] valueForKey:@"trk"] valueForKey:@"trkseg"] valueForKey:@"trkpt"];
    NSString*way_str=[[dict valueForKey:@"gpx"] valueForKey:@"wpt"];
    
    
    //    if ([add_place_str isKindOfClass:[NSArray class]])
    //    {
    //        NSArray *places_is = [[[[dict valueForKey:@"gpx"] valueForKey:@"extensions"] valueForKey:@"addplace"] valueForKey:@"place"];
    //        [add_places addObjectsFromArray:places_is];
    //
    //    }
    //    else if ([add_place_str isKindOfClass:[NSDictionary class]]){
    //
    //        NSDictionary *data = [[[[dict valueForKey:@"gpx"] valueForKey:@"extensions"] valueForKey:@"addplace"] valueForKey:@"place"];
    //
    //        [add_places addObject:data];
    //    }
    
    if ([traks_str isKindOfClass:[NSArray class]])
    {
        NSArray *places_is = [[[[dict valueForKey:@"gpx"] valueForKey:@"trk"] valueForKey:@"trkseg"] valueForKey:@"trkpt"];
        [tracks_array addObjectsFromArray:places_is];
        
    }
    else if ([traks_str isKindOfClass:[NSDictionary class]]){
        
        NSDictionary *data = [[[[dict valueForKey:@"gpx"] valueForKey:@"trk"] valueForKey:@"trkseg"] valueForKey:@"trkpt"];
        
        [tracks_array addObject:data];
    }
    
    
    if ([way_str isKindOfClass:[NSArray class]])
    {
        NSArray *places_is = [[dict valueForKey:@"gpx"] valueForKey:@"wpt"];
        [way_array addObjectsFromArray:places_is];
        
    }
    else if ([way_str isKindOfClass:[NSDictionary class]]){
        
        NSDictionary *data = [[dict valueForKey:@"gpx"] valueForKey:@"wpt"];
        
        [way_array addObject:data];
    }
    
    
    //    Waypoints
    
    //NSMutableArray *waypoints_lat_lon_name_array=[[NSMutableArray alloc]init];
    for (int q=0; q<way_array.count; q++) {
        NSString *waypoint_lat_str = [NSString stringWithFormat:@"%@",[[way_array objectAtIndex:q] valueForKey:@"lat"]];
        NSString *watpoint_lon_str = [NSString stringWithFormat:@"%@",[[way_array objectAtIndex:q] valueForKey:@"lon"]];
        NSString *watpoint_title_str = [NSString stringWithFormat:@"%@",[[[way_array objectAtIndex:q] valueForKey:@"name"] valueForKey:@"text"]];
        
        GMSMarker *start_marker = [[GMSMarker alloc] init];
        start_marker.position = CLLocationCoordinate2DMake([waypoint_lat_str doubleValue],[watpoint_lon_str doubleValue]);
        start_marker.appearAnimation = kGMSMarkerAnimationPop;
        start_marker.title=watpoint_title_str;
        start_marker.icon = [UIImage imageNamed:@"default_marker"];
        start_marker.map = mapView;
        
        // NSString*wayPoints_lat_lon_str=[NSString stringWithFormat:@"%@,%@,%@",watpoint_title_str,waypoint_lat_str,watpoint_lon_str];
    }
    
    
    //Add A Places
    // select_route_places_count=add_places.count;
    for (int i=0; i<add_places.count; i++)
    {
        NSDictionary *add_place_di=[add_places objectAtIndex:i];
        
        NSString *lat_str = [NSString stringWithFormat:@"%@",[[add_place_di valueForKey:@"latitude"] valueForKey:@"text"]];
        NSString *long_str = [NSString stringWithFormat:@"%@",[[add_place_di valueForKey:@"longitude"] valueForKey:@"text"]];
        NSString *add_pace_title = [NSString stringWithFormat:@"%@",[[add_place_di valueForKey:@"title"] valueForKey:@"text"]];
        
        
        GMSMarker *add_place_marker = [[GMSMarker alloc] init];
        add_place_marker.position = CLLocationCoordinate2DMake([lat_str floatValue],[long_str floatValue]);
        add_place_marker.appearAnimation = kGMSMarkerAnimationPop;
        add_place_marker.title=add_pace_title;
        add_place_marker.icon = [UIImage imageNamed:@"addplace_marker_bg.png"];
        add_place_marker.map = mapView;
        
        
        /*
         NSString *desr_str=[[add_place_di valueForKey:@"descr"] valueForKey:@"text"];
         NSString *add_pace_time = [NSString stringWithFormat:@"%@",[[[add_places objectAtIndex:i] valueForKey:@"time"] valueForKey:@"text"]];
         
         NSString *des_is;
         
         if ([desr_str isKindOfClass:[NSString class]]) {
         des_is=desr_str;
         }
         else{
         des_is=@"";
         }
         NSString * place_count_is=@"0";
         NSMutableArray *add_places_array=[[NSMutableArray alloc]init];
         NSDictionary *imgs_dict = [add_place_di  valueForKey:@"image"];
         NSString *  url_str_for_images = [[imgs_dict valueForKey:@"url"] valueForKey:@"text"];
         if (url_str_for_images==nil||[url_str_for_images isEqual:[NSNull null]])
         {
         
         }
         else if([url_str_for_images isKindOfClass:[NSArray class]])
         {
         NSArray *url_arry=[[imgs_dict valueForKey:@"url"] valueForKey:@"text"];
         place_count_is=[NSString stringWithFormat:@"%lu",(unsigned long)url_arry.count];
         [add_places_array addObjectsFromArray:url_arry];
         }
         else
         {
         place_count_is=@"1";
         [add_places_array addObject:url_str_for_images];
         }
         
         
         
         //save on coredata
         if (![_is_From isEqualToString:@"HOME"]) { //is comes from home means when ride time suddenly app terminated afer open app it will redirected to ride page
         NSError *error=nil;
         NSManagedObjectContext *context=[self managedObjectContext];
         NSManagedObject *newDevice=[NSEntityDescription insertNewObjectForEntityForName:@"AddPlace" inManagedObjectContext:context];
         
         [newDevice setValue:[NSNumber numberWithDouble:[lat_str doubleValue]] forKey:@"lat"];
         [newDevice setValue:[NSNumber numberWithDouble:[long_str doubleValue]] forKey:@"longitude"];
         [newDevice setValue:des_is forKey:@"des"];
         [newDevice setValue:add_pace_title forKey:@"title"];
         [newDevice setValue:[NSNumber numberWithInt:[place_count_is intValue]] forKey:@"img_count"];
         [newDevice setValue:add_pace_time forKey:@"time"];
         if (add_places_array.count>0) {
         NSString *str=[add_places_array componentsJoinedByString:@","];
         [newDevice setValue:str forKey:@"img_urls"];
         }
         else
         {
         [newDevice setValue:@"" forKey:@"img_urls"];
         }
         
         [newDevice setValue:@"" forKey:@"img_paths"];
         [newDevice setValue:@"completed" forKey:@"status"];
         [newDevice setValue:[NSNumber numberWithInt:i+1] forKey:@"places_id"];
         [newDevice setValue:[NSNumber numberWithInt:1] forKey:@"local_rid_uniq"];
         
         [newDevice.managedObjectContext save:&error];
         }*/
    }
    
    //    Tracks
    
    for (int p=0; p<tracks_array.count; p++) {
        
        NSString *lat_str = [NSString stringWithFormat:@"%@",[[tracks_array objectAtIndex:p] valueForKey:@"lat"]];
        NSString *lon_str = [NSString stringWithFormat:@"%@",[[tracks_array objectAtIndex:p] valueForKey:@"lon"]];
        //NSDictionary *ele_dict=[[tracks_array objectAtIndex:p] valueForKey:@"ele"];
        //NSString *elevation_is=[NSString stringWithFormat:@"%@",[ele_dict valueForKey:@"text"]];
        
        x = [lat_str doubleValue];
        z = [lon_str doubleValue];
        if (p==0) {
            GMSMarker *start_marker = [[GMSMarker alloc] init];
            start_marker.position = CLLocationCoordinate2DMake(x,z);
            start_marker.appearAnimation = kGMSMarkerAnimationPop;
            start_marker.title=@"Route Start Point";
            start_marker.icon = [UIImage imageNamed:@"red_marker.png"];
            start_marker.map = mapView;
            
        }
        else if (p==tracks_array.count-1)
        {
            GMSMarker *end_marker = [[GMSMarker alloc] init];
            end_marker.position =  CLLocationCoordinate2DMake(x,z);
            end_marker.appearAnimation = kGMSMarkerAnimationPop;
            end_marker.title=@"Route End Point";
            end_marker.icon = [UIImage imageNamed:@"finish_marker.png"];
            end_marker.map = mapView;
            
            GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[[[tracks_array objectAtIndex:p] valueForKey:@"lat"]doubleValue] longitude:[[[tracks_array objectAtIndex:p] valueForKey:@"lon"]doubleValue] zoom:15];
            [mapView animateToCameraPosition:camera];
        }
        [path2 addCoordinate:CLLocationCoordinate2DMake(x, z)];
        // NSString*lat_lon_str=[NSString stringWithFormat:@"%@,%@,%@",lat_str,lon_str,elevation_is];
    }
    polyline_2 = [GMSPolyline polylineWithPath:path2];
    [path2 removeAllCoordinates];
    polyline_2.strokeColor = [UIColor blueColor];
    polyline_2.strokeWidth = 5.0f;
    polyline_2.geodesic = YES;
    polyline_2.map = mapView;
    

    
    
    [add_places removeAllObjects];
    [tracks_array removeAllObjects];
    [way_array removeAllObjects];
    indicaterview.hidden=YES;
    
    
 //When get From Notification Add A place Button will Getting For Blur.
    _defaultMarker_btn.userInteractionEnabled=NO;
    _defaultMarker_btn.alpha=0.5;
    _default_markers_btn_2.userInteractionEnabled=NO;
    _default_markers_btn_2.alpha=0.5;
    if ([_is_From isEqualToString:@"NOTIFICATIONS"]) {
        _custommarkers_btn.userInteractionEnabled=NO;
        _custommarkers_btn.alpha=0.5;
        _custom_Markers_btn2.userInteractionEnabled=NO;
        _custom_Markers_btn2.alpha=0.5;
       
        
    }
    else if([_is_From isEqualToString:@"HOME"])
    {
        if (![appDelegate.ride_ID_Api isEqualToString:@""])
        {
            NSString *U_id=[NSString stringWithFormat:@"%@",[Defaults valueForKey:User_ID]];
            if ([[rides_object valueForKey:ride_table_ride_owner_id] isEqualToString:U_id]) {
                NSLog(@"i am the owner");
            }
            else
            {
                _custommarkers_btn.userInteractionEnabled=NO;
                _custommarkers_btn.alpha=0.5;
            }
            
        }
         _startAndPause_btn.tag=0;
          [self onClick_startAndPause_btn:0];
    }
    
    
    
    
}


#pragma mark StoryBoard Button Actions
- (IBAction)onClick_checkin_btn:(id)sender {
    
    if ([self check_Location_On_Or_Off]) {
        
    }
    else{
        return;
    }
    if ([SHARED_HELPER checkIfisInternetAvailable]) {
        CheckInPlaces *CheckInPlaces = [self.storyboard instantiateViewControllerWithIdentifier:@"CheckInPlaces"];
        CheckInPlaces.ride_id_is=appDelegate.ride_ID_Api;
        [self.navigationController pushViewController:CheckInPlaces animated:YES];
    }
    else
        [SHARED_HELPER showAlert:Nonetwork];
}
- (IBAction)onClick_defaultMarker_btn:(id)sender {
    if ([_is_From isEqualToString:@"HOME"]) {
        return;
    }
    if ([appDelegate.select_Route_Id_For_CreateAndFreeRide isEqualToString:@""]) {
        
        if (![ride_status isEqualToString:@"Started"]) {
            setDefaultMarker_view = [[UIView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/2-140, SCREEN_HEIGHT/2-85, 280, 170)];
            setDefaultMarker_view.backgroundColor=[UIColor yellowColor];
            UIButton *close_but=[[UIButton alloc]initWithFrame:CGRectMake(5, 5, 30, 30)];
            [close_but setImage:[UIImage imageNamed:@"close_Small"] forState:UIControlStateNormal];
            [close_but addTarget:self action:@selector(close_marker_view) forControlEvents:UIControlEventTouchUpInside];
            [close_but setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
            UIButton *sucsess_but=[[UIButton alloc]initWithFrame:CGRectMake(setDefaultMarker_view.frame.size.width-40, 5, 30, 30)];
            [sucsess_but setImage:[UIImage imageNamed:@"right_click_small"] forState:UIControlStateNormal];
            
            [sucsess_but addTarget:self action:@selector(Tick_marker_view) forControlEvents:UIControlEventTouchUpInside];
            [sucsess_but setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
            
            UILabel *dist_title=[[UILabel alloc]initWithFrame:CGRectMake(setDefaultMarker_view.frame.size.width/2-50, 15, 100, 30)];
            dist_title.text=@"DISTANCE";
            dist_title.textAlignment=NSTextAlignmentCenter;
            dist_title.textColor=[UIColor blackColor];
            [dist_title setFont:[UIFont fontWithName:Heading_Font size:20]];
            distance_lab=[[UILabel alloc]initWithFrame:CGRectMake(setDefaultMarker_view.frame.size.width/2-75, 50, 150, 50)];
            distance_lab.text=@"";
            distance_lab.textColor=[UIColor blackColor];
            [distance_lab setFont:[UIFont fontWithName:Heading_Font_Reg size:17]];
            distance_lab.textAlignment=NSTextAlignmentCenter;
            
            UISlider *range_Slider=[[UISlider alloc]initWithFrame:CGRectMake(10, 95, setDefaultMarker_view.frame.size.width-20, 30)];
            range_Slider.value=1;
            
            range_Slider.minimumValue=0;
            range_Slider.maximumValue=10;
            [range_Slider addTarget:self action:@selector(sliderRang:) forControlEvents:UIControlEventTouchUpInside];
            range_Slider.thumbTintColor=[UIColor whiteColor];
            range_Slider.tintColor=APP_BLUE_COLOR;
            
            distance_lab.text=[NSString stringWithFormat:@"EVERY %d MILES",original_Marker_Val];
            NSMutableAttributedString *text =
            [[NSMutableAttributedString alloc]
             initWithAttributedString: distance_lab.attributedText];
            
            [text addAttribute:NSForegroundColorAttributeName
                         value:[UIColor blackColor]
                         range:NSMakeRange(6,2)];
            [text addAttribute:NSFontAttributeName value:[UIFont fontWithName:Heading_Font size:25] range:NSMakeRange(6,2)];
            [distance_lab setAttributedText: text];
            
            if (original_Marker_Val>=1) {
                range_Slider.value=original_Marker_Val;
            }
            else{
                range_Slider.value=0;
            }
            
            UIView *zero_View=[[UIView alloc]initWithFrame:CGRectMake(20, 130, 2, 10)];
            zero_View.backgroundColor=[UIColor blackColor];
            UILabel *zero_Label=[[UILabel alloc]initWithFrame:CGRectMake(15, 142, 10, 15)];
            zero_Label.text=@"0";
            zero_Label.textAlignment=NSTextAlignmentCenter;
            zero_Label.textColor=[UIColor blackColor];
            UIView *ten_View=[[UIView alloc]initWithFrame:CGRectMake(setDefaultMarker_view.frame.size.width-15, 130, 2, 10)];
            ten_View.backgroundColor=[UIColor blackColor];
            UILabel *ten_Label=[[UILabel alloc]initWithFrame:CGRectMake(setDefaultMarker_view.frame.size.width-25, 142, 20, 15)];
            ten_Label.text=@"10";
            ten_Label.textAlignment=NSTextAlignmentCenter;
            ten_Label.textColor=[UIColor blackColor];
            
            [setDefaultMarker_view addSubview:sucsess_but];
            [setDefaultMarker_view addSubview:close_but];
            [setDefaultMarker_view addSubview:distance_lab];
            [setDefaultMarker_view addSubview:dist_title];
            [setDefaultMarker_view addSubview:range_Slider];
            [setDefaultMarker_view addSubview:zero_View];
            [setDefaultMarker_view addSubview:ten_View];
            [setDefaultMarker_view addSubview:zero_Label];
            [setDefaultMarker_view addSubview:ten_Label];
            
            setDefaultMarker_view.alpha=0;
            [UIView animateWithDuration:.50 animations:^{
                setDefaultMarker_view.alpha = 1;
                setDefaultMarker_view.transform = CGAffineTransformMakeScale(1, 1);
            }];
            [self.view addSubview:setDefaultMarker_view];
        }
    }
    else{
        
    }
}
-(void)Tick_marker_view{
    
    [UIView animateWithDuration:.50 animations:^{
        setDefaultMarker_view.transform = CGAffineTransformMakeScale(1.3, 1.3);
        setDefaultMarker_view.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            [setDefaultMarker_view removeFromSuperview];
        }
    }];
    if (dis_int_for_dflt_marker>0) {
        original_Marker_Val=dis_int_for_dflt_marker;
    }
    else
        original_Marker_Val=0;
    
}
-(void)close_marker_view{
    [UIView animateWithDuration:.50 animations:^{
        setDefaultMarker_view.transform = CGAffineTransformMakeScale(1.3, 1.3);
        setDefaultMarker_view.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            [setDefaultMarker_view removeFromSuperview];
        }
    }];
}
-(void)sliderRang:(id)sender{
    UISlider *slider = (UISlider*)sender;
    int value = slider.value;
    if (value>0) {
        dis_int_for_dflt_marker=value;
    }
    else{
        dis_int_for_dflt_marker=0;
    }
    distance_lab.text=[NSString stringWithFormat:@"EVERY %d MILES",dis_int_for_dflt_marker];
    NSMutableAttributedString *text =
    [[NSMutableAttributedString alloc]
     initWithAttributedString: distance_lab.attributedText];
    
    [text addAttribute:NSForegroundColorAttributeName
                 value:[UIColor blackColor]
                 range:NSMakeRange(6,2)];
    [text addAttribute:NSFontAttributeName value:[UIFont fontWithName:Heading_Font size:25] range:NSMakeRange(6,2)];
    [distance_lab setAttributedText: text];
}

- (IBAction)onClick_customMarkers_btn:(id)sender { //AddPlaces Action
    if ([_is_From isEqualToString:@"NOTIFICATIONS"]) {
        if ([appDelegate.select_Route_Id_For_CreateAndFreeRide isEqualToString:@""]) {
            
        }
        else
            return;
    }
    else if([_is_From isEqualToString:@"HOME"])
    {
        if (![appDelegate.ride_ID_Api isEqualToString:@""])
        {
            if ([appDelegate.select_Route_Id_For_CreateAndFreeRide isEqualToString:@""]) {
                
            }
            else
            {
                NSString *U_id=[NSString stringWithFormat:@"%@",[Defaults valueForKey:User_ID]];
                if ([[rides_object valueForKey:ride_table_ride_owner_id] isEqualToString:U_id]) {
                    NSLog(@"i am the owner");
                }
                else
                {
                    return;
                }
            }
        }
    }
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddAPlace*addAPlace = [storyboard instantiateViewControllerWithIdentifier:@"AddAPlace"];
    BIZPopupViewController *addAPlacePopupView = [[BIZPopupViewController alloc] initWithContentViewController:addAPlace contentSize:CGSizeMake(self.view.frame.size.width-40,320)];
    add_place_lat=map_currentlocation_coordinate.latitude;
    add_Place_Long=map_currentlocation_coordinate.longitude;
    
    addAPlace.add_A_place_lat=[NSString stringWithFormat:@"%f",add_place_lat];
    addAPlace.add_A_place_long=[NSString stringWithFormat:@"%f",add_Place_Long];
    addAPlace.local_rid=[NSString stringWithFormat:@"%ld",(long)local_Ride_ID];
    
    [self presentViewController:addAPlacePopupView animated:NO completion:nil];
    
}
- (IBAction)onClick_mapToggle_btn:(id)sender {
    if (_boolvalue_rideToggle==YES) {
        
       
        
        //self.bottom_ride_start_stop_view_height.constant=80;
        self.markers_View2.hidden=NO;
        self.bottom_ride_start_stop_view.backgroundColor=[UIColor clearColor];
        self.bottom_ride_start_view_imageview.alpha=0.2;
        self.bottom_ride_start_view_imageview.backgroundColor=[UIColor blackColor];
        
        self.view_map_rideDashboard.hidden=NO;
        self.view_rideDashboard.hidden=YES;
        //        self.mapToRideDashboard_toggle_btn.hidden=NO;
        [self.mapToggle_btn setImage:[UIImage imageNamed:@"mapToRideDashboard_toggle"] forState:UIControlStateNormal];
        _boolvalue_rideToggle=NO;
        map_Clicked=@"YES";
        appDelegate.app_Status=@"ForeGround";
        //Display Annotation
        // Show coach marks
        BOOL coachMarksShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"MPCoachMarksShown_RideToDashboard"];
        if (coachMarksShown == NO) {
            // Don't show again
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MPCoachMarksShown_RideToDashboard"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self showAnnotation];
        }

    }
    else
    {
        //self.bottom_ride_start_stop_view_height.constant=80;
        self.markers_View2.hidden=YES;
        self.bottom_ride_start_stop_view.backgroundColor=[UIColor clearColor];
        self.bottom_ride_start_view_imageview.alpha=1.0;
        self.bottom_ride_start_view_imageview.backgroundColor=[UIColor clearColor];
        
        
        
        self.view_map_rideDashboard.hidden=YES;
        self.view_rideDashboard.hidden=NO;
        //        self.mapToRideDashboard_toggle_btn.hidden=NO;
        [self.mapToggle_btn setImage:[UIImage imageNamed:@"map_toggle"] forState:UIControlStateNormal];
        _boolvalue_rideToggle=YES;
        
        appDelegate.app_Status=@"Ride_Page";
    }
}


- (IBAction)onClick_startAndPause_btn:(UIButton*)sender {
    
    if ([ride_status isEqualToString:@"Started"]||[ride_status isEqualToString:@"Paused"]) {
        
    }
    else
    {
        if ([SHARED_HELPER checkIfisInternetAvailable]) {
            
        }
        else{
            [SHARED_HELPER showAlert:Nonetwork];
            return;
        }
    }
    if([_is_From isEqualToString:@"HOME"])
    {
        
    }
    else
    {
        if ([self check_Location_On_Or_Off]) {
            
        }
        else{
            return;
        }
    }
        
    if ([first_Ride_Complete isEqualToString:@"YES"]) {
        return;
    }
    
    if (_startAndPause_btn.tag==0) {
        
       _startAndPause_btn.tag=1;
        
        if ([ride_Finished isEqualToString:@"YES"]) {
            
            if ([_is_From isEqualToString:@"NOTIFICATIONS"]) { //  Join_Ride NOT Owner
                [self join_Riders_Api];
            }
            else if (![appDelegate.ride_ID_Api isEqualToString:@""]) { // Owner  notify Users
                self.riders_Count.text=@"1";
                if ([_is_From isEqualToString:@"HOME"]) {
                    [self Start_Ride_OR_Pause_Ride:0];
                }
                else
                    [self notify_users_request];
            }
            else{
                //solo ride withroute or without route
                self.riders_Count.text=@"1";
                [self Start_Ride_OR_Pause_Ride:0];
            }
        }
        else{
            [self Start_Ride_OR_Pause_Ride:0];
        }
        self.ridedashboard_homeBtn.hidden=NO;
    }
    else
    {
        _startAndPause_btn.tag=0;
        [self Start_Ride_OR_Pause_Ride:1];
        
    }
    
}
-(void)Start_Ride_OR_Pause_Ride :(NSInteger )sender
{
    
    if (sender==0)
    {
        isPanned=false;
        self.checkin_View.hidden=NO;
        self.add_Place_View.hidden=NO;
        
       
        self.default_Marker_view.hidden=YES;
        
        
        self.check_in_View2.hidden=NO;
        self.add_Places_View2.hidden=NO;
        
        self.default_marker_View2.hidden=YES;
        
        self.bottom_ride_start_stop_view_height.constant=100;
        self.finish_btn.hidden=NO;
        self.finish_Ride_Imageview.hidden=NO;
        [self.view layoutIfNeeded];
        
        
        
        //[Defaults setObject:@"ride_started" forKey:@"ride_status"];
        ride_status=@"Started";
        //[self.startAndPause_btn setImage:[UIImage imageNamed:@"Pause_Ride"] forState:UIControlStateNormal];
        self.start_ride_image_View.image=[UIImage imageNamed:@"Pause_Ride"];
        
        
        if ([ride_Finished isEqualToString:@"YES"]) {
            
            if ([_is_From isEqualToString:@"HOME"]) {
                ride_Finished=@"NO";
                
                _totalTimeStart = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(ride_total_time_methode) userInfo:nil repeats:YES];
                //_rideTimeStart = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector   (ride_start_time_methode) userInfo:nil repeats:YES];
                
                locationManager = [[CLLocationManager alloc] init];
                locationManager.delegate = self;
                
                previous_location=locationManager.location;
                map_currentlocation_coordinate=locationManager.location.coordinate;
                if([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
                {
                    
                    [locationManager requestWhenInUseAuthorization];
                }
                else
                {
                    [locationManager startUpdatingLocation];
                }
                
                altitude=locationManager.location.altitude;
                elevation=altitude*3.28084;
                if (elevation!=0) {
                _label_elevation_rideDashboard.text = [NSString stringWithFormat:@"%.0f", elevation];
                }
                ride_Title=[rides_object valueForKey:ride_table_name];
                appDelegate.FreeRide_Name=ride_Title;
                [self weatherMethod:@""];
                
                
                [Defaults setObject:@"RIDE_STARTED" forKey:@"RIDE_STATUS"];
            }
            else
            {
                
                rideTime_coutDown=0;//this code priviously in finish methode
                totalTime_countDown=0;
                miles=0;
                maxElevation=0;
                minElevation=0;
                maxspeed=0;
                avgSpeed=0;
                marker_count=0;
                loc_Update_count=0;
                NSString *txt = [self stringFromTimeInterval:rideTime_coutDown];
                _label_rideTime_rideDashboard.text=txt;
                
               
                _label_totalTime_rideDashboard.text=txt;
                _label_totalTime_mapRideDashboard.text=txt;
                _label_totalDistance_rideDashboard.text = [NSString stringWithFormat:@"%.2f",miles];
                _labe_totalDistance_mapRideDashboard.text= [NSString stringWithFormat:@"%.2f",miles];
                
                _label_maxSpeed_rideDashboard.text = [NSString stringWithFormat:@"%2d",maxspeed];
                _label_averageSpeed_rideDashboard.text = [NSString stringWithFormat: @"0"];
                //above code
                
                ride_Finished=@"NO";
                locationManager = [[CLLocationManager alloc] init];
                locationManager.delegate = self;
                
                if([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
                {
                    
                    [locationManager requestWhenInUseAuthorization];
                }
                
                else
                {
                    [locationManager startUpdatingLocation];
                }
                
                previous_location=locationManager.location;
                map_currentlocation_coordinate=locationManager.location.coordinate;
                
                
                appDelegate.FreeRide_Name=ride_Title;
                [self weatherMethod:@""];
                [self getAdrressFromLatLong:@"start"location_twod:map_currentlocation_coordinate];
                
                trackpoints =[[NSMutableArray alloc] init];
                way_Points_Array=[[NSMutableArray alloc] init];
                
                
                path = [GMSMutablePath path];
                [path addCoordinate:CLLocationCoordinate2DMake(locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude)];
                polyline = [GMSPolyline polylineWithPath:path];
                polyline.strokeColor = [UIColor yellowColor];
                polyline.strokeWidth = 5.0f;
                polyline.geodesic = YES;
                polyline.map=mapView;
                
                
                
                maxspeed = 0;
                maxElevation=0;
                altitude=locationManager.location.altitude;
                elevation=altitude*3.28084;
                _label_elevation_rideDashboard.text = [NSString stringWithFormat:@"%.0f", elevation];
                
                ride_Start_Lat=[NSString stringWithFormat:@"%f",locationManager.location.coordinate.latitude];
                ride_Start_Long=[NSString stringWithFormat:@"%f",locationManager.location.coordinate.longitude];
                
                GMSMarker *marker = [[GMSMarker alloc] init];
                marker.position = CLLocationCoordinate2DMake(locationManager.location.coordinate.latitude,locationManager.location.coordinate.longitude);
                marker.appearAnimation = kGMSMarkerAnimationPop;
                marker.title=Start_Point_Title;
                marker.icon = [UIImage imageNamed:@"red_marker.png"];
                marker.map = mapView;
                
                //    start ride Date and Time
                NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
                //         [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                [dateFormatter setDateFormat:@"MM/dd/yyyy"];
                NSLog(@"ride Start Date %@",[dateFormatter stringFromDate:[NSDate date]]);
                ride_Start_date_String=[NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:[NSDate date]]];
                
                NSDateFormatter *dateFormatter1=[[NSDateFormatter alloc] init];
                [dateFormatter1 setDateFormat:@"HH:mm:ss"];
                NSLog(@"ride Start time %@",[dateFormatter1 stringFromDate:[NSDate date]]);
                ride_Start_time_String=[NSString stringWithFormat:@"%@",[dateFormatter1 stringFromDate:[NSDate date]]];
                
                //Create Ride  Route Row In CoreData
                [self Rides_Routes_TableCreate_Method];
                
                
                _totalTimeStart = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(ride_total_time_methode) userInfo:nil repeats:YES];
                //_rideTimeStart = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(ride_start_time_methode) userInfo:nil repeats:YES];
                
                [Defaults setObject:@"RIDE_STARTED" forKey:@"RIDE_STATUS"];
                
            }
            
            [self user_update_location:locationManager.location.coordinate.latitude :locationManager.location.coordinate.longitude is_show :@"Yes"]; //Service For Send Our Location To Backend
        }
        else
        {
            //start timer after pause start button clicked
            //_rideTimeStart = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(ride_start_time_methode) userInfo:nil repeats:YES];
        }
    }
    else
    {
        ride_status=@"Paused";
      
        //[self.startAndPause_btn setImage:[UIImage imageNamed:@"Resume_Ride"] forState:UIControlStateNormal];
        self.start_ride_image_View.image=[UIImage imageNamed:@"Resume_Ride"];
        ride_Finished=@"NO";
       // [_rideTimeStart  invalidate];
    }
}






- (IBAction)Get_Riders_Count:(id)sender {
    if ([ride_status isEqualToString:@"Started"] || [ride_status isEqualToString:@"Paused"])
    {
    }
    else{
        return;
    }
    if ([appDelegate.ride_ID_Api isEqualToString:@""]) {
        return;
    }
    else{
        [SHARED_API get_Riders_List:appDelegate.ride_ID_Api withSuccess:^(NSDictionary *response) {
            if ([[response valueForKey:STATUS]isEqualToString:SUCCESS]) {
                NSArray *data=[response valueForKey:@"riders"];
                if (data.count>0) {
                    _riders_Count.text=[NSString stringWithFormat:@"%lu",(unsigned long)data.count];
                }
            }
            else if ([[response valueForKey:STATUS]isEqualToString:FAIL])
            {
                [SHARED_HELPER showAlert:GETRIDERSCOUNTFAIL];
            }
        } onfailure:^(NSError *theError) {
            
        }];
    }
}
- (IBAction)Promotions_Action:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    Promotions*promotions = [storyboard instantiateViewControllerWithIdentifier:@"Promotions"];
    promotions.is_from_rideDashboard=@"RideDashboard";
    [self.navigationController pushViewController:promotions animated:NO];
}
#pragma mark Finish_ride Action And Service Methodes
- (IBAction)onClick_finish_btn:(id)sender {
    if ([ride_status isEqualToString:@"Started"] || [ride_status isEqualToString:@"Paused"])
    {
        
//        if (path.count>100) {
       
             [self save_Route_Pop_UP];
//        }
//        else
//        {
//            [SHARED_HELPER showAlert:@"Oops! Ride was too short to save."];
//        }
    }
}
-(void)Only_Save_Route_Action{
    [self Finish_Ride];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Route_Sucsess" object:nil];
    //    [self Create_addAPlace_images];
}

-(void)Finish_Ride
{
    if ([ride_status isEqualToString:@"Started"] || [ride_status isEqualToString:@"Paused"]) {
        
        //        self.checkin_View.hidden=YES;
        //        self.add_Place_View.hidden=YES;
        //
      
        //        self.default_Marker_view.hidden=NO;
        //
        //
        //        self.check_in_View2.hidden=YES;
        //        self.add_Places_View2.hidden=YES;
        //
        //        self.default_marker_View2.hidden=NO;
        
        
        default_marker_forService=original_Marker_Val;
       
        ride_Finished=@"YES";
        ride_status=@"Finished";
       // [_rideTimeStart invalidate];
        [_totalTimeStart invalidate];
        //_rideTimeStart=nil;
        
        //[self.startAndPause_btn setImage:[UIImage imageNamed:@"start_Ride"] forState:UIControlStateNormal];
         self.start_ride_image_View.image=[UIImage imageNamed:@"start_Ride"];
        
        _startAndPause_btn.tag=0;
        
        ride_End_Lat=[NSString stringWithFormat:@"%f",map_currentlocation_coordinate.latitude];
        ride_End_Long=[NSString stringWithFormat:@"%f",map_currentlocation_coordinate.longitude];
        
        //Finish Marker
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake(map_currentlocation_coordinate.latitude,map_currentlocation_coordinate.longitude);
        marker.appearAnimation = kGMSMarkerAnimationPop;
        marker.title=End_Point_title;
        marker.icon = [UIImage imageNamed:@"finish_marker.png"];
        marker.map = mapView;
        
        [path addLatitude:map_currentlocation_coordinate.latitude longitude:map_currentlocation_coordinate.longitude];
        polyline.path=path;
        GMSCoordinateBounds *bounds2 = [[GMSCoordinateBounds alloc] initWithPath:path];
        [mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds2]];
        
        self.ridedashboard_homeBtn.hidden=YES;
        
        //encoding_path=[path encodedPath];
        
        
        //End ride Date and Time
        
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        //            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        NSLog(@"ride End Date %@",[dateFormatter stringFromDate:[NSDate date]]);
        ride_end_date_String=[NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:[NSDate date]]];
        
        NSDateFormatter *dateFormatter1=[[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"HH:mm:ss"];
        NSLog(@"ride End time %@",[dateFormatter1 stringFromDate:[NSDate date]]);
        ride_end_time_String=[NSString stringWithFormat:@"%@",[dateFormatter1 stringFromDate:[NSDate date]]];
       
        if ([SHARED_HELPER checkIfisInternetAvailable]) {
            [self user_update_location:map_currentlocation_coordinate.latitude :map_currentlocation_coordinate.longitude is_show:@"No"];
            [self getAdrressFromLatLong:@"end"location_twod:map_currentlocation_coordinate];
        }
    }
}

-(void)save_Route_Pop_UP{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SaveARoute*SaveARoute = [storyboard instantiateViewControllerWithIdentifier:@"SaveARoute"];
    SaveARoute.seletced_route_title=selected_route_title;
    if ([_is_From isEqualToString:@"NOTIFICATIONS"]) {
        SaveARoute.ride_Owned_BY_ME=@"NO";
    }
    else if ([_is_From isEqualToString:@"HOME"])
    {
        NSString *U_id=[NSString stringWithFormat:@"%@",[Defaults valueForKey:User_ID]];
        if ([[rides_object valueForKey:ride_table_ride_owner_id] isEqualToString:U_id]) {
            SaveARoute.ride_Owned_BY_ME=@"YES";
        }
        else
            SaveARoute.ride_Owned_BY_ME=@"NO";
    }
    else{
        SaveARoute.ride_Owned_BY_ME=@"YES";
    }
    SaveARoute.route_des=select_Route_Des;
    SaveARoute.ride_description=created_ride_description;
    
    if ([selected_route_title isEqualToString:@""]) {
        BIZPopupViewController *addAPlacePopupView = [[BIZPopupViewController alloc] initWithContentViewController:SaveARoute contentSize:CGSizeMake(self.view.frame.size.width-40,440)];
        [self presentViewController:addAPlacePopupView animated:NO completion:nil];
    }
    else
    {
        BIZPopupViewController *addAPlacePopupView = [[BIZPopupViewController alloc] initWithContentViewController:SaveARoute contentSize:CGSizeMake(self.view.frame.size.width-40,380)];
        [self presentViewController:addAPlacePopupView animated:NO completion:nil];
    }
   
}
-(void)SAVE_Route_Action_And_END_RIDE_SERVICE{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SaveARoute" object:nil];
    s3_inProgress=@"STOP";
    if ([self Rides_Table_Update_Method:@"END_Ride"]) {
        [self Routes_Table_Update_Method:@"END_Ride"];
    }
    else
    {
        
    }
    
    self.markers_View2.hidden=YES;
    _startAndPause_btn.hidden=YES;
    _finish_btn.hidden=YES;
    self.checkin_View.hidden=YES;
    self.default_Marker_view.hidden=YES;
    self.add_Place_View.hidden=YES;
    self.start_ride_image_View.hidden=YES;
    self.finish_Ride_Imageview.hidden=YES;
    self.check_in_View2.hidden=YES;
    self.default_marker_View2.hidden=YES;
    self.add_Places_View2.hidden=YES;
    
    
    
    
    
    /*_startAndPause_btn.userInteractionEnabled=NO;
    _finish_btn.userInteractionEnabled=NO;
    
    _startAndPause_btn.alpha=0.5;
    _finish_btn.alpha=0.5;
    
    _checkIn_btn.userInteractionEnabled=NO;
    _check_in_btn_2.userInteractionEnabled=NO;
    _custommarkers_btn.userInteractionEnabled=NO;
    _custom_Markers_btn2.userInteractionEnabled=NO;
    _Promotions_Btn_Outlet.userInteractionEnabled=NO;
    _rideDashboard_Promotions_View.alpha=0.5;
    _checkIn_btn.alpha=0.5;
    _check_in_btn_2.alpha=0.5;
    _custommarkers_btn.alpha=0.5;
    _custom_Markers_btn2.alpha=0.5;
    _Promotions_Btn_Outlet.alpha=0.5;
    */
    
    [self RideEndedWithAlert:@"Your ride was saved, we will sync in the background. We will notify you when it's done."];
    
}
-(void)RideEndedWithAlert :(NSString *)message
{
    
    UIAlertController * alert_vc=   [UIAlertController
                                     alertControllerWithTitle:@"Congratulations!"
                                     message:message
                                     preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"ok"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             appDelegate.ridedashboard_home=NO;
                             if ([SHARED_HELPER internetAvailable]) {
                                 if ([appDelegate.is_Running_Background_Sync isEqualToString:@"NO"]) {
                                     [[NSNotificationCenter defaultCenter] postNotificationName:@"SaveRide_Service" object:self]; //in appdelegate
                                 }
                             }
                             [Defaults setObject:@"RIDE_ENDED" forKey:@"RIDE_STATUS"];
                             map_locationManager=nil;
                             locationManager=nil;
                         }];
    [alert_vc addAction:ok];
    [self presentViewController:alert_vc animated:YES completion:nil];
}
/*
 -(void)SAVE_Route_Action_And_END_RIDE_SERVICE{
 [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SaveARoute" object:nil];
 indicaterview.hidden=NO;
 //    addPlacesDataArray=[[NSMutableArray alloc]init];
 //    [self gettingDataFromCoreData:@"Locations"];
 //    [self gettingDataFromCoreData:@"Defaultmarkers"];
 //    addPlacesDataArray=[self gettingAddPlacesFromCoreData:@"AddPlace"];
 //    [self xmlCreation];
 //    ii=0; //First addplace move s3 next create xmlstring
 //    [self Create_addAPlace_images];
 
 if (appDelegate.internet_Available) {
 // appDelegate.stop_Processing=@"YES";
 [[NSNotificationCenter defaultCenter] postNotificationName:@"Addplaces_Service" object:self]; //in appdelegate
 }
 }
 
 -(void)SAVE_Route_Action_And_END_RIDE_SERVICE_AFTER_GPX{
 
 if ([SHARED_HELPER checkIfisInternetAvailable]) {
 
 }
 else{
 [self network_Alert:@"end_ride"];
 return;
 }
 
 NSMutableDictionary *endride_dict = [NSMutableDictionary new];
 NSMutableDictionary *ride_dict = [NSMutableDictionary new];
 NSMutableDictionary *route_dict = [NSMutableDictionary new];
 
 
 
 NSDictionary *Save_Route_Dict=[appDelegate.Save_Route_Data_Array objectAtIndex:0];
 NSDictionary *Save_Ride_Dict=[appDelegate.Save_Route_Data_Array objectAtIndex:1];
 ride_Images=[appDelegate.Save_Route_Data_Array objectAtIndex:2];
 
 //    ud3NrluYz2om503s4O8Y/TedfzU=
 
 [endride_dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
 
 [route_dict setObject:appDelegate.select_Route_Id_For_CreateAndFreeRide forKey:@"existing_route_id"];
 [route_dict setObject:[Save_Route_Dict valueForKey:@"Title"] forKey:@"route_title"];
 [route_dict setObject:[Save_Route_Dict valueForKey:@"Des"] forKey:@"route_description"];
 [route_dict setObject:route_strat_adress forKey:@"route_start_address"];
 [route_dict setObject:[Save_Route_Dict valueForKey:@"RouteStatus"] forKey:@"route_privacy"];
 [route_dict setObject:route_end_Adress forKey:@"route_end_address"];
 
 [route_dict setObject:[NSString stringWithFormat:@"%@",AmazonGpxUrlString] forKey:@"route_gpx_file"];
 [route_dict setObject:[Save_Route_Dict valueForKey:@"Comments"] forKey:@"comment"];
 [route_dict setObject:[Save_Route_Dict valueForKey:@"Rating"] forKey:@"rating"];
 [route_dict setObject:[NSString stringWithFormat:@"%@",_label_elevation_rideDashboard.text]
 forKey:@"route_max_elevation"];
 
 
 [ride_dict setObject:appDelegate.ride_ID_Api forKey:@"existing_ride_id"];
 [ride_dict setObject:[Save_Ride_Dict valueForKey:@"RideTitle"] forKey:@"ride_title"];
 [ride_dict setObject:[Save_Ride_Dict valueForKey:@"RideDescriptionDes"]  forKey:@"ride_description"];
 
 [ride_dict setObject:ride_strat_adress forKey:@"ride_start_address"];
 [ride_dict setObject:ride_end_Adress  forKey:@"ride_end_address"];
 [ride_dict setObject:ride_Start_Lat forKey:@"ride_start_latitude"];
 [ride_dict setObject:ride_Start_Long  forKey:@"ride_start_longitude"];
 [ride_dict setObject:ride_End_Lat forKey:@"ride_end_latitude"];
 [ride_dict setObject:ride_End_Long  forKey:@"ride_end_longitude"];
 
 
 [ride_dict setObject:[SHARED_HELPER rideType] forKey:@"ride_type"];
 
 
 [ride_dict setObject:self.label_rideTime_rideDashboard.text forKey:@"ride_time"];
 [ride_dict setObject:self.label_totalTime_rideDashboard.text forKey:@"ride_total_time"];
 [ride_dict setObject:[NSNumber numberWithInt:default_marker_forService] forKey:@"ride_default_marker_distance"];
 [ride_dict setObject:self.label_averageSpeed_rideDashboard.text forKey:@"avg_speed"];
 [ride_dict setObject:[NSString stringWithFormat:@"%@",ride_totalDistance] forKey:@"total_distance"];
 [ride_dict setObject:[NSString stringWithFormat:@"%@",ride_Start_date_String] forKey:@"ride_start_date"];
 [ride_dict setObject:[NSString stringWithFormat:@"%@",ride_Start_time_String] forKey:@"ride_start_time"];
 [ride_dict setObject:[NSString stringWithFormat:@"%@",ride_end_date_String] forKey:@"ride_end_date"];
 [ride_dict setObject:[NSString stringWithFormat:@"%@",ride_end_time_String] forKey:@"ride_end_time"];
 [ride_dict setObject:self.label_maxSpeed_rideDashboard.text forKey:@"max_speed"];
 [ride_dict setObject:appDelegate.ride_Random_Number forKey:@"random_number"];
 [ride_dict setObject:ride_Images forKey:@"ride_images"];
 
 [endride_dict setObject:route_dict forKey:@"route"];
 [endride_dict setObject:ride_dict forKey:@"ride"];
 
 NSLog(@"End ride api %@",endride_dict);
 
 [SHARED_API endRideRequestsWithParams:endride_dict withSuccess:^(NSDictionary *response) {
 dispatch_async(dispatch_get_main_queue(), ^
 {
 [self END_SERVICE_Sucsess:response];
 });
 
 } onfailure:^(NSError *theError) {
 
 dispatch_async(dispatch_get_main_queue(), ^
 {
 NSLog(@"server error %@",theError);
 // NSString *error=[theError localizedDescription];
 [self network_Alert:@"end_ride"];
 indicaterview.hidden=YES;
 });
 }];
 }
 
 -(void)END_SERVICE_Sucsess:(NSDictionary *)response{
 if ([[response valueForKey:STATUS] isEqualToString:SUCCESS]) {
 indicaterview.hidden=YES;
 [SHARED_HELPER showAlert:Ride_Sucsess];
 int duration = 2; // duration in seconds
 back_Clicked=@"Rides";
 first_Ride_Complete=@"YES";
 appDelegate.stop_Processing=@"NO";
 
 [Defaults setObject:@"ride_finished" forKey:@"ride_status"];
 [Defaults removeObjectForKey:@"ride_details"];
 [Defaults removeObjectForKey:@"route_details"];
 [Defaults removeObjectForKey:@"ride_is"];
 [Defaults removeObjectForKey:@"ride_id_is"];
 
 [self user_update_location:locationManager.location.coordinate.latitude :locationManager.location.coordinate.longitude ]; //Service For Send Our Location To Backend
 
 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
 RideDetailViewController*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"RideDetailViewController"];
 controller.is_From=@"RIDE_DASHBOARD";
 controller.ride_owner_id=[NSString stringWithFormat:@"%@",[Defaults valueForKey:User_ID]];
 controller.scheduled_fullFilled_rideID_string=[NSString stringWithFormat:@"%@",[response valueForKey:@"ride_id"]];
 controller.scheduled_fullFilled_user_ride_data_string=@"FUllFILLED";
 [self.navigationController pushViewController:controller animated:YES];
 });
 }
 else{
 [self network_Alert:@"end_ride"];
 }
 }
 
 */

#pragma mark - network _retry Alert
-(void)network_Alert:(NSString *)call_from{
    UIAlertController *alertview = [UIAlertController alertControllerWithTitle:@"Network Error" message:Nonetwork preferredStyle:UIAlertControllerStyleAlert];
    
    [alertview addAction:[UIAlertAction actionWithTitle:@"retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        if ([call_from isEqualToString:@"gpx"]) {
            //[self createGPX];
        }
        else if([call_from isEqualToString:@"end_ride"]){
            // [self SAVE_Route_Action_And_END_RIDE_SERVICE_AFTER_GPX];
        }
        else if ([call_from isEqualToString:@"get_ride"])
        {
            [self get_Ride_Details:appDelegate.ride_ID_Api];
        }
        else if ([call_from isEqualToString:@"get_route"])
        {
            [self Route_Detail_Service:appDelegate.select_Route_Id_For_CreateAndFreeRide];
        }
        else if ([call_from isEqualToString:@"update_ride"]){
            [self update_ride_request_rideDashboard:appDelegate.ride_ID_Api];
        }
    }]];
    
    [self presentViewController:alertview animated:YES completion:nil];
}
#pragma mark - GPX Uploading to S3
/*
 -(void)xmlCreation
 {
 //    NSString *create_xml_=@"";
 //    if (![appDelegate.select_Route_Id_For_CreateAndFreeRide isEqualToString:@""]&& ![appDelegate.is_Done_Add_Place isEqualToString:@"NO"]) {//with route id and with new add places
 //        create_xml_=@"YES";
 //    }
 //    else if (![appDelegate.select_Route_Id_For_CreateAndFreeRide isEqualToString:@""]) {//with route no add places
 //        //        create_xml_=@"NO";
 //        create_xml_=@"YES";
 //    }
 if (![appDelegate.select_Route_Id_For_CreateAndFreeRide isEqualToString:@""])//with route id
 {
 NSURL *url = [NSURL URLWithString:selected_Route_GPX];
 NSData*fileData = [NSData dataWithContentsOfURL:url];
 NSString *xml_str = [[NSString alloc] initWithData:fileData encoding:NSUTF8StringEncoding];
 
 TCMXMLWriter *writer = [[TCMXMLWriter alloc] initWithOptions:TCMXMLWriterOptionPrettyPrinted];
 [writer tag:@"extensions" contentBlock:^{
 [writer tag:@"addplace" contentBlock:^{
 
 for (int i=0;i<[addPlacesDataArray count];i++) {
 NSDictionary * addAPlaces_dict=[addPlacesDataArray objectAtIndex:i];
 
 [writer tag:@"place" contentBlock:^{
 [writer tag:@"title" contentText:[addAPlaces_dict valueForKey:@"AddAPlace_Title"]];
 NSString *desr_is=[NSString stringWithFormat:@"%@",[addAPlaces_dict valueForKey:@"AddAPlace_desc"]];
 if ([desr_is isEqualToString:@""]) {
 [writer tag:@"descr" contentText:@" "];
 } else {
 [writer tag:@"descr" contentText:[addAPlaces_dict valueForKey:@"AddAPlace_desc"]];
 }
 [writer tag:@"time" contentText:[addAPlaces_dict valueForKey:@"AddAPlace_Time"]];
 [writer tag:@"latitude" contentText:[addAPlaces_dict valueForKey:@"AddAPlace_lat"]];
 [writer tag:@"longitude" contentText:[addAPlaces_dict valueForKey:@"AddAPlace_long"]];
 
 NSInteger count=[[addAPlaces_dict valueForKey:@
 "img_count"] integerValue];
 NSMutableArray *sample_Add_images_array=[[NSMutableArray alloc]init];
 NSArray *urls_array=[addAPlaces_dict valueForKey:@
 "img_urls"];
 [sample_Add_images_array addObjectsFromArray:urls_array];
 if (sample_Add_images_array.count>=count) {
 [writer tag:@"image" contentBlock:^{
 for (int j=0; j<count; j++)
 {
 [writer tag:@"url" contentText:[sample_Add_images_array objectAtIndex:j]];
 }
 }];
 }
 else{
 [writer tag:@"image" contentBlock:^{
 }];
 }
 }];
 
 }
 }];
 }];
 NSArray *array=[xml_str componentsSeparatedByString:@"<extensions>"];
 xml_Create_string=[NSString stringWithFormat:@"%@%@%@",[array objectAtIndex:0],writer.XMLString,@"</gpx>"];
 [self createGPX];
 }
 else{
 TCMXMLWriter *writer = [[TCMXMLWriter alloc] initWithOptions:TCMXMLWriterOptionPrettyPrinted];
 //    [writer instructXML];
 [writer instructXMLStandalone];
 //    <gpx xmlns="http://www.topografix.com/GPX/1/1" version="1.1" creator="OPEN ROAD RIDES">
 [writer tag:@"gpx" attributes:@{@"xmlns": @"http://www.topografix.com/GPX/1/1",@"version": @"1.1",@"creator":@"OPEN ROAD RIDES"} contentBlock:^{
 
 
 //Ways------------------------->XML Creation
 for (int i=0;i<[way_Points_Array count];i++) {
 NSString *lat=[NSString stringWithFormat:@"%@",[[way_Points_Array objectAtIndex:i]objectForKey:@"lat"]];
 NSString *long_=[NSString stringWithFormat:@"%@",[[way_Points_Array objectAtIndex:i]objectForKey:@"long"]];
 [writer tag:@"wpt" attributes:@{ @"lat":lat, @"lon":long_} contentBlock:^{
 [writer tag:@"name" contentText:[NSString stringWithFormat:@"WAY POINT%d",i+1]];
 [writer tag:@"desc" contentText:[NSString stringWithFormat:@"WAY POINT%d",i+1]];
 }];
 }
 
 
 
 //Tracks------------------------->XML Creation
 [writer tag:@"trk" contentBlock:^{
 [writer tag:@"trkseg" contentBlock:^{
 
 for (int i=0;i<[trackpoints count];i++) {
 NSString *lat=[NSString stringWithFormat:@"%@",[[trackpoints objectAtIndex:i]objectForKey:@"lat"]];
 NSString *long_=[NSString stringWithFormat:@"%@",[[trackpoints objectAtIndex:i]objectForKey:@"long"]];
 NSString *elevation_gpx=[NSString stringWithFormat:@"%@",[[trackpoints objectAtIndex:i] objectForKey:@"ele"]];
 [writer tag:@"trkpt" attributes:@{@"lat":lat, @"lon":long_} contentBlock:^{
 //                [writer tag:@"time" contentText:[[trackpoints objectAtIndex:i] objectForKey:@"time"]];
 [writer tag:@"ele" contentText:[NSString stringWithFormat:@"%@",elevation_gpx]];
 }];
 }
 }];
 }];
 
 
 
 //ADD_Places------------------------->XML Creation
 [writer tag:@"extensions" contentBlock:^{
 [writer tag:@"addplace" contentBlock:^{
 
 for (int i=0;i<[addPlacesDataArray count];i++) {
 NSDictionary * addAPlaces_dict=[addPlacesDataArray objectAtIndex:i];
 
 [writer tag:@"place" contentBlock:^{
 
 [writer tag:@"title" contentText:[addAPlaces_dict valueForKey:@"AddAPlace_Title"]];
 NSString *desr_is=[NSString stringWithFormat:@"%@",[addAPlaces_dict valueForKey:@"AddAPlace_desc"]];
 if ([desr_is isEqualToString:@""]) {
 [writer tag:@"descr" contentText:@" "];
 } else {
 [writer tag:@"descr" contentText:[addAPlaces_dict valueForKey:@"AddAPlace_desc"]];
 }
 [writer tag:@"time" contentText:[addAPlaces_dict valueForKey:@"AddAPlace_Time"]];
 [writer tag:@"latitude" contentText:[addAPlaces_dict valueForKey:@"AddAPlace_lat"]];
 [writer tag:@"longitude" contentText:[addAPlaces_dict valueForKey:@"AddAPlace_long"]];
 
 NSInteger count=[[addAPlaces_dict valueForKey:@
 "img_count"] integerValue];
 NSMutableArray *sample_Add_images_array=[[NSMutableArray alloc]init];
 NSArray *urls_array=[addAPlaces_dict valueForKey:@
 "img_urls"];
 [sample_Add_images_array addObjectsFromArray:urls_array];
 if (sample_Add_images_array.count>=count) {
 [writer tag:@"image" contentBlock:^{
 for (int j=0; j<count; j++)
 {
 [writer tag:@"url" contentText:[sample_Add_images_array objectAtIndex:j]];
 }
 
 }];
 
 //                    [writer tag:@"title" contentBlock:^{
 //                      [writer text:[addAPlaces_dict valueForKey:@"AddAPlace_Title"]];
 //
 //                    }];
 
 //                    [writer tag:@"descr" contentBlock:^{
 //                      [writer text:[addAPlaces_dict valueForKey:@"AddAPlace_desc"]];
 //
 ////                    }];
 //                    [writer tag:@"time" contentBlock:^{
 //                      [writer text:[addAPlaces_dict valueForKey:@"AddAPlace_Time"]];
 ////                        AddAPlace_Time
 //                    }];
 //                    [writer tag:@"latitude" contentBlock:^{
 //                      [writer text:[addAPlaces_dict valueForKey:@"AddAPlace_lat"]];
 //
 //                    }];
 //                    [writer tag:@"longitude" contentBlock:^{
 //                      [writer text:[addAPlaces_dict valueForKey:@"AddAPlace_long"]];
 //
 //                    }];
 
 
 
 }
 else{
 [writer tag:@"image" contentBlock:^{
 }];
 }
 
 }];
 
 }
 
 }];
 }];
 
 }];
 
 NSLog(@"result XML:n\n%@", writer.XMLString);
 xml_Create_string=[NSString stringWithFormat:@"%@",writer.XMLString];
 [self createGPX];
 }
 }
 
 
 -(void) createGPX
 {
 if ([SHARED_HELPER checkIfisInternetAvailable]) {
 
 }
 else{
 [self network_Alert:@"gpx"];
 return;
 }
 
 
 //    GPXRoot *root = [GPXRoot rootWithCreator:@"OPEN ROAD RIDES"];
 //    GPXTrack *track = [root newTrack];
 //
 //
 //    track.name = @"My New Track";
 //    for (int i=0;i<[trackpoints count];i++) {
 //
 //     GPXWaypoint *waypoint = [root newWaypointWithLatitude:[[[trackpoints objectAtIndex:i]objectForKey:@"lat"] doubleValue] longitude:[[[trackpoints objectAtIndex:i]objectForKey:@"long"] doubleValue]];
 //        waypoint.name = @"Waypoint";
 //
 //    [track newTrackpointWithLatitude:[[[trackpoints objectAtIndex:i]objectForKey:@"lat"] doubleValue] longitude:[[[trackpoints objectAtIndex:i]objectForKey:@"long"] doubleValue]];
 //    }
 //    NSLog(@"track Points ");
 
 //    for (int i=0;i<[way_Points_Array count];i++) {
 //        GPXWaypoint *waypoint = [root newWaypointWithLatitude:[[[way_Points_Array objectAtIndex:i]objectForKey:@"lat"] doubleValue] longitude:[[[way_Points_Array objectAtIndex:i]objectForKey:@"long"] doubleValue]];
 //        waypoint.name = @"Waypoint";
 //    }
 
 //    NSLog(@"root%@",root);
 //    NSString *gpxString = root.gpx;
 
 
 NSString *selected_Route_Key_OR_NEW_KEY=@"";
 
 NSString *gpxString=xml_Create_string;
 
 // write gpx to file
 NSError *error;
 NSString *filePath = [self gpxFilePath];
 if (![gpxString writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error]) {
 if (error) {
 NSLog(@"error, %@", error);
 }
 }
 
 NSLog(@"filePath%@",filePath);
 
 NSURL *url = [[NSURL alloc] initFileURLWithPath:filePath];
 NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
 _uploadRequest = [AWSS3TransferManagerUploadRequest new];
 _uploadRequest.bucket = AMAZON_BUCKET_NAME;
 _uploadRequest.ACL = AWSS3ObjectCannedACLPublicReadWrite;
 
 if ([selected_Route_GPX isEqualToString:@""]) { //Route_Not_Selected
 NSString *amazonS3Url=[NSString stringWithFormat:@"%@/GPX_files/%@",[defaults valueForKey:@"User_ID"],[filePath lastPathComponent]];
 NSLog(@"Amazon S3 Url %@",amazonS3Url);
 selected_Route_Key_OR_NEW_KEY=amazonS3Url;
 }
 else{ //Route_Selected
 selected_Route_Key_OR_NEW_KEY=[selected_Route_GPX stringByReplacingOccurrencesOfString:@"https://openroadrides.s3.amazonaws.com/" withString:@""];
 }
 
 _uploadRequest.key = selected_Route_Key_OR_NEW_KEY;
 _uploadRequest.contentType = @"text/xml";
 _uploadRequest.body = url;
 
 __weak RideDashboard *weakSelf = self;
 
 _uploadRequest.uploadProgress =^(int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend){
 dispatch_sync(dispatch_get_main_queue(), ^{
 weakSelf.amountUploaded = totalBytesSent;
 weakSelf.filesize = totalBytesExpectedToSend;
 [weakSelf update];
 
 });
 };
 // now the upload request is set up we can creat the transfermanger, the credentials are already set up in the app delegate
 AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];
 //    // start the upload
 [[transferManager upload:_uploadRequest] continueWithExecutor:[AWSExecutor mainThreadExecutor] withBlock:^id(AWSTask *task) {
 
 // once the uploadmanager finishes check if there were any errors
 if (task.error) {
 NSLog(@" Gpx Uploading Error%@", task.error);
 [self network_Alert:@"gpx"];
 }
 else
 {// if there aren't any then the image is uploaded!
 // this is the url of the image we just uploaded
 // NSLog(@"https://s3.amazonaws.com/s3-demo-objectivec/foldername/image.png");
 
 AmazonGpxUrlString=[NSString stringWithFormat:@"%@%@",Amazon_url_prefix,selected_Route_Key_OR_NEW_KEY];
 NSLog(@"URl String %@",AmazonGpxUrlString);
 
 //             MFMailComposeViewController *controller = [MFMailComposeViewController new];
 //             controller.mailComposeDelegate = self;
 //
 //             NSData *data = [NSData dataWithContentsOfFile:filePath];
 //             [controller addAttachmentData:data mimeType:@"application/gpx+xml" fileName:[filePath lastPathComponent]];
 //             [controller setMessageBody:[NSString stringWithFormat:@"GPX : %@", AmazonGpxUrlString] isHTML:YES];
 //             if ([MFMailComposeViewController canSendMail]&&controller) {
 //             [self presentViewController:controller animated:YES completion:NULL];
 //             }
 
 [self SAVE_Route_Action_And_END_RIDE_SERVICE_AFTER_GPX];
 [trackpoints removeAllObjects];
 }
 return nil;
 }];
 
 
 //For Clear database Of this ride
 if ([self clearEntity:@"Locations"]) {
 NSLog(@"Ride Data Cleared on Your Stack");
 }
 else{
 NSLog(@"Ride Data not Cleared on Your Stack");
 }
 if ([self clearEntity:@"Defaultmarkers"]) {
 NSLog(@"Ride Data Cleared on Your Stack");
 }
 else{
 NSLog(@"Ride Data not Cleared on Your Stack");
 }
 if ([self clearEntity:@"AddPlace"]) {
 NSLog(@"Ride Data Cleared on Your Stack");
 }
 else{
 NSLog(@"Ride Data not Cleared on Your Stack");
 }
 }
 - (void) update{
 _progressLabel.text = [NSString stringWithFormat:@"Uploading:%.0f%%", ((float)self.amountUploaded/ (float)self.filesize) * 100];
 }
 - (NSString *)gpxFilePath
 {
 NSDateFormatter *formatter = [NSDateFormatter new];
 [formatter setTimeStyle:NSDateFormatterFullStyle];
 [formatter setDateFormat:@"yyyyMMMdd__HHmmss"];
 NSString *dateString = [formatter stringFromDate:[NSDate date]];
 NSString *fileName;
 if ([[SHARED_HELPER rideType]isEqualToString:@"free_ride"]) {
 fileName = [NSString stringWithFormat:@"Free_Ride_%@.gpx", dateString];
 }
 else{
 fileName = [NSString stringWithFormat:@"setup_Ride_%@.gpx", dateString];
 }
 return [NSTemporaryDirectory() stringByAppendingPathComponent:fileName];
 }
 
 */

#pragma mark - image Uploading to S3- add A Plcae
-(void)selectedData_addAPlace
{
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(add_place_lat,add_Place_Long);
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.icon = [UIImage imageNamed:@"addplace_marker_bg.png"];
    marker.title=appDelegate.add_Place_marker_title;
    marker.map = mapView;
    //    NSLog(@"App Delegate Images array===%@",app_Delegate.addAPlace_imagesArray);
    //    ii=0;
    //    add_places_imag_urls=[[NSMutableArray alloc]init];
    //    [self Create_addAPlace_images];
    
    if ([s3_inProgress isEqualToString:@"NO"]) {
        ii=0; //First addplace move s3 next create xmlstring
        [self Create_addAPlace_images];
    }
    else
    {
        return;
    }
    
}


-(void)Create_addAPlace_images
{
    if (![SHARED_HELPER internetAvailable]) {
        s3_inProgress=@"NO";
        return;
    }
    if ([s3_inProgress isEqualToString:@"STOP"]) {
        return;
    }
    s3_inProgress=@"YES";
    NSArray *add_placesarray=[self gettingAddPlacesFromCoreData:Add_places_Table];
    if (add_placesarray.count>ii) {
        add_dict=[add_placesarray objectAtIndex:ii];
        places_s3=0;
        NSString *status=[add_dict valueForKey:addPlaces_table_status];
        if ([status isEqualToString:table_status_pending]) {
            [self movetoS3_addplaces];
        }
        else
        {
            ii++;
            [self Create_addAPlace_images];
        }
    }
    else
    {
        // [self xmlCreation];
        s3_inProgress=@"NO";
    }
}
-(void)movetoS3_addplaces{
    
    NSArray *img_paths=[add_dict valueForKey:@"img_paths"];
    if (places_s3==0) { // if ex: 3 images uploaded and 2 is not uploaded after calling this place it will take from 4 th image
        NSArray *img_urls=[add_dict valueForKey:addPlaces_table_imgUrls];
        places_s3=img_urls.count;
    }
    
    if (img_paths.count>places_s3) {
        
        NSInteger place_id=[[add_dict valueForKey:addPlaces_table_id] integerValue];
        NSString *image_filePath;
        image_filePath=[NSString stringWithFormat:@"%@",[img_paths objectAtIndex:places_s3]];
        NSLog(@"Image_filePath===%@",image_filePath);
        NSDateFormatter *formatter1 = [NSDateFormatter new];
        [formatter1 setTimeStyle:NSDateFormatterFullStyle];
        [formatter1 setDateFormat:@"yyyyMMMdd__HHmmss"];
        NSString *dateString = [formatter1 stringFromDate:[NSDate date]];
        NSURL *url = [[NSURL alloc] initFileURLWithPath:image_filePath];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        _uploadRequest = [AWSS3TransferManagerUploadRequest new];
        _uploadRequest.bucket = AMAZON_BUCKET_NAME;
        _uploadRequest.ACL = AWSS3ObjectCannedACLPublicReadWrite;
        NSString *foldercreation_string=[NSString stringWithFormat:@"%@/add_a_place/%@/%@",[defaults valueForKey:@"User_ID"],dateString,[image_filePath lastPathComponent]];
        NSLog(@"Image To s3====%@",foldercreation_string);
        _uploadRequest.key = foldercreation_string;
        _uploadRequest.contentType = @"image/png";
        _uploadRequest.body = url;
        __weak RideDashboard *weakSelf = self;
        _uploadRequest.uploadProgress =^(int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend){
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                weakSelf.amountUploaded = totalBytesSent;
                weakSelf.filesize = totalBytesExpectedToSend;
                [weakSelf update_image];
            });
            
        };
        
        // now the upload request is set up we can creat the transfermanger, the credentials are already set up in the app delegate
        
        AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];
        [[transferManager upload:_uploadRequest] continueWithExecutor:[AWSExecutor mainThreadExecutor] withBlock:^id(AWSTask *task) {
            // once the uploadmanager finishes check if there were any errors
            if (task.error) {
                NSLog(@" Image Uploading Error%@", task.error);
                s3_inProgress=@"NO";
            }
            else
            {
                // NSLog(@"https://s3.amazonaws.com/s3-demo-objectivec/foldername/image.png");
                AmazonImageUrlString=[NSString stringWithFormat:@"%@%@",Amazon_url_prefix,foldercreation_string];
                NSLog(@"Image URl String %@",AmazonImageUrlString);
                
                [self Update_AddPlace_Table:place_id amazon_url:AmazonImageUrlString];
                places_s3++;
                [self movetoS3_addplaces];
            }
            return nil;
            
        }];
    }
    else
    {
        ii++;
        [self Create_addAPlace_images];
    }
}


#pragma mark - image Uploading to S3- add A Plcae------->oldCode
//-(void)Create_addAPlace_images
//{
//       NSString *image_filePath;
//
//    if ([app_Delegate.addAPlace_imagesArray count]>0 &&[app_Delegate.addAPlace_imagesArray count]>ii) {
//
//        image_filePath=[NSString stringWithFormat:@"%@",[app_Delegate.addAPlace_imagesArray objectAtIndex:ii]];
//        NSLog(@"Image_filePath===%@",image_filePath);
//        NSDateFormatter *formatter1 = [NSDateFormatter new];
//        [formatter1 setTimeStyle:NSDateFormatterFullStyle];
//        [formatter1 setDateFormat:@"yyyyMMMdd__HHmmss"];
//        NSString *dateString = [formatter1 stringFromDate:[NSDate date]];
//        NSURL *url = [[NSURL alloc] initFileURLWithPath:image_filePath];
//        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//        _uploadRequest = [AWSS3TransferManagerUploadRequest new];
//        _uploadRequest.bucket = AMAZON_BUCKET_NAME;
//        _uploadRequest.ACL = AWSS3ObjectCannedACLPublicReadWrite;
//        NSString *foldercreation_string=[NSString stringWithFormat:@"%@/add_a_place/%@/%@",[defaults valueForKey:@"User_ID"],dateString,[image_filePath lastPathComponent]];
//        NSLog(@"Image To s3====%@",foldercreation_string);
//        _uploadRequest.key = foldercreation_string;
//        _uploadRequest.contentType = @"image/png";
//        _uploadRequest.body = url;
//        __weak RideDashboard *weakSelf = self;
//        _uploadRequest.uploadProgress =^(int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend){
//
//        dispatch_sync(dispatch_get_main_queue(), ^{
//            weakSelf.amountUploaded = totalBytesSent;
//            weakSelf.filesize = totalBytesExpectedToSend;
//            [weakSelf update_image];
//        });
//
//    };
//
//
//
//    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];
//
//    //    // start the upload
//
//    [[transferManager upload:_uploadRequest] continueWithExecutor:[AWSExecutor mainThreadExecutor] withBlock:^id(AWSTask *task) {
//        // once the uploadmanager finishes check if there were any errors
//
//        if (task.error) {
//            NSLog(@" Image Uploading Error%@", task.error);
//
//            [add_places_imag_urls addObject:@""];
//
//            ii++;
//            [self Create_addAPlace_images];
//            }
//
//        else
//
//        {// if there aren't any then the image is uploaded!
//
//            // this is the url of the image we just uploaded
//
//            //            NSLog(@"https://s3.amazonaws.com/s3-demo-objectivec/foldername/image.png");
//            AmazonImageUrlString=[NSString stringWithFormat:@"%@%@",Amazon_url_prefix,foldercreation_string];
//            NSLog(@"Image URl String %@",AmazonImageUrlString);
//
//            [add_places_imag_urls addObject:AmazonImageUrlString];
//            ii++;
//            [self Create_addAPlace_images];
//
//        }
//        return nil;
//
//    }];
//
//  }
//    else
//    {
//        //add addplace to core data
//        [self add_places_to_coredata];
//    }
//
////    if (ii==[appDelegate.addAPlace_imagesArray count]) {
////          [appDelegate.addAPlace_imagesArray removeAllObjects];
////    }
//
//}
//- (NSString *)addAPlace_images_filePath
//{
//    NSDateFormatter *formatter = [NSDateFormatter new];
//    [formatter setTimeStyle:NSDateFormatterFullStyle];
//    [formatter setDateFormat:@"yyyyMMdd_"];
//    NSString *dateString = [formatter stringFromDate:[NSDate date]];
//
//    NSString *fileName = [NSString stringWithFormat:@"%@", dateString];
//    return [NSTemporaryDirectory() stringByAppendingPathComponent:fileName];
//}
//-(void)add_places_to_coredata{
//
//    NSDictionary *add_place_dict=[addPlacesDataArray objectAtIndex:addPlacesDataArray.count-1];
//
//
//    NSError *error=nil;
//    NSManagedObjectContext *context=[self managedObjectContext];
//    NSManagedObject *newDevice=[NSEntityDescription insertNewObjectForEntityForName:@"AddPlace" inManagedObjectContext:context];
//
//    [newDevice setValue:[NSNumber numberWithDouble:[[add_place_dict valueForKey:@"AddAPlace_lat"] doubleValue]] forKey:@"lat"];
//    [newDevice setValue:[NSNumber numberWithDouble:[[add_place_dict valueForKey:@"AddAPlace_long"] doubleValue]] forKey:@"long"];
//    [newDevice setValue:[add_place_dict valueForKey:@"AddAPlace_desc"] forKey:@"des"];
//    [newDevice setValue:[add_place_dict valueForKey:@"AddAPlace_Title"] forKey:@"title"];
//    [newDevice setValue:[NSNumber numberWithInt:[[add_place_dict valueForKey:@"ThisPlaceimagesCount"] intValue]] forKey:@"img_count"];
//    [newDevice setValue:[add_place_dict valueForKey:@"AddAPlace_Time"] forKey:@"time"];
//    if (add_places_imag_urls.count>0) {
//    NSString *str=[add_places_imag_urls componentsJoinedByString:@","];
//    [newDevice setValue:str forKey:@"img_urls"];
//    }
//    else
//    {
//    [newDevice setValue:@"" forKey:@"img_urls"];
//    }
//
//    [newDevice.managedObjectContext save:&error];
//    if ([newDevice.managedObjectContext save:&error])
//    {
//        NSLog(@"Saved latitude and longitude in coredata for default markers======>>");
//    }
//}
//
//
- (void) update_image{
    _progressLabel.text = [NSString stringWithFormat:@"Uploading:%.0f%%", ((float)self.amountUploaded/ (float)self.filesize) * 100];
}

#pragma mark mapLoading Methode
-(void)map_loadingMethod
{
    map_locationManager = [[CLLocationManager alloc] init];
    map_locationManager.delegate=self;
    //    _locationManager.distanceFilter=kCLDistanceFilterNone;
    //[_locationManager requestWhenInUseAuthorization];
    [map_locationManager requestAlwaysAuthorization];
    [map_locationManager startUpdatingLocation];
    [map_locationManager startUpdatingHeading];
    map_locationManager.desiredAccuracy=kCLLocationAccuracyBestForNavigation;
    [map_locationManager startMonitoringSignificantLocationChanges];
    map_locationManager.allowsBackgroundLocationUpdates=YES;
    map_locationManager.pausesLocationUpdatesAutomatically=NO;
    mapZoom=18;
    map_currentlocation_coordinate=map_locationManager.location.coordinate;
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:map_currentlocation_coordinate.latitude
                                                            longitude:map_currentlocation_coordinate.longitude
                                                                 zoom:mapZoom];
    mapView = [GMSMapView mapWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) camera:camera];
    mapView.delegate=self;
    mapView.myLocationEnabled = true;
    //    mapView.mapType=kGMSTypeTerrain;
    //    mapView.trafficEnabled=YES;
    //    [mapView setMinZoom:10 maxZoom:20];
    //    [mapView animateToCameraPosition:camera];
    
    //    NSBundle *mainBundle = [NSBundle mainBundle];
    //    NSURL *styleUrl = [mainBundle URLForResource:@"map_style" withExtension:@"json"];
    //    NSError *error;
    //
    //    // Set the map style by passing the URL for style.json.
    //    GMSMapStyle *style = [GMSMapStyle styleWithContentsOfFileURL:styleUrl error:&error];
    //
    //    if (!style) {
    //        NSLog(@"The style definition could not be loaded: %@", error);
    //    }
    //
    //    mapView.mapStyle = style;
    [self.content_map_showing_view addSubview:mapView];
}





-(void)ride_start_time_methode
{
    /*
    rideTime_coutDown = rideTime_coutDown+1;
    _label_rideTime_rideDashboard.text = [[NSNumber numberWithInt:rideTime_coutDown] stringValue];
    NSString *txt = [self stringFromTimeInterval:rideTime_coutDown];
    _label_rideTime_rideDashboard.text=txt;
    
    //Call Location methode
    [self locationUpdate_Every_Second:Updated_Location];
    */
}
-(void)ride_total_time_methode
{
    totalTime_countDown = totalTime_countDown+1;
    NSString *txt1 = [self stringFromTimeInterval:totalTime_countDown];
    _label_totalTime_rideDashboard.text=txt1;
    _label_totalTime_mapRideDashboard.text=txt1;
    
    //Ride_Time
    if ([ride_status isEqualToString:@"Started"]) {
        rideTime_coutDown = rideTime_coutDown+1;
        _label_rideTime_rideDashboard.text = [[NSNumber numberWithInt:rideTime_coutDown] stringValue];
        NSString *txt = [self stringFromTimeInterval:rideTime_coutDown];
        _label_rideTime_rideDashboard.text=txt;
        
        
        if ([self isBetterLocation:Updated_Location current:previous_location]) {
            //Call Location methode
            [self locationUpdate_Every_Second:Updated_Location];
        }
    }
    
    //Update Rides And Routes
    [self Rides_Table_Update_Method:@"Locations"];
    
    //Call Wheather every 20 miles
    NSInteger dist=miles;
    if (dist>1)
    {
        if ((dist%20)==0 &&dist!=wheather_call)
        {
            wheather_call=dist;
            if ([SHARED_HELPER checkIfisInternetAvailable])
            {
                [self weatherMethod:@"locations"];
            }
        }
    }
}
-(BOOL)isBetterLocation :(CLLocation *)new_Location  current:(CLLocation *)old_Location{
    
        if (old_Location == nil) {
        // A new location is always better than no location
           return true;
        }
        
        // Check whether the new location fix is newer or older
        NSDate *old_date=old_Location.timestamp;
        NSDate *new_date=new_Location.timestamp;
        NSTimeInterval timeDelta = [new_date timeIntervalSinceDate:old_date];
        Boolean isSignificantlyNewer = timeDelta > 1;
        Boolean isSignificantlyOlder = timeDelta < -1;
        Boolean isNewer = timeDelta > 0;
    
        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }
        
        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (new_Location.horizontalAccuracy - old_Location.horizontalAccuracy);
        Boolean isLessAccurate = accuracyDelta > 0;
        Boolean isMoreAccurate = accuracyDelta < 0;
        Boolean isSignificantlyLessAccurate = accuracyDelta > 200;
        
        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate) {
            return true;
        }
        return false;
    }

- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}
-(int)secondsFromArray:(NSArray *)H_M_S_Array
{
    int ti=0;
    
    int hours = [[H_M_S_Array objectAtIndex:0] intValue];
    int minutes = [[H_M_S_Array objectAtIndex:1] intValue];
    int seconds = [[H_M_S_Array objectAtIndex:2] intValue];
    
    ti=hours *3600;
    ti=ti +minutes *60;
    ti=ti+seconds;
    return ti;
}


#pragma mark CllocationsMethodes And polyLineDelagates & saving tracks on coredata

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    if ([ride_status isEqualToString:@"Started"]) {
        CLLocation *currentLocation = newLocation;
        map_currentlocation_coordinate=manager.location.coordinate;
        Updated_Location=manager.location;
        if (currentLocation != nil)
        {
            if(newLocation.coordinate.latitude != 0.0 && newLocation.coordinate.longitude != 0.0)
            {
                //  [self locationUpdate:newLocation];
            }
        }
    }
    //     if ([ride_status isEqualToString:@"Started"]) {
    //         if (!isPanned ) {
    ////             GMSVisibleRegion region = mapView.projection.visibleRegion;
    ////             GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithRegion:region];
    ////             if (![bounds containsCoordinate:map_currentlocation])
    ////             {
    //                 if (!isPanned ) {
    //                     GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:current_lat longitude:current_long zoom:mapZoom];
    //                     [mapView animateToCameraPosition:camera];
    //
    //                }
    //            // }
    //         }
    //     }
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
        {
            // do some error handling
        }
            break;
        default:{
            [locationManager startUpdatingLocation];
        }
            break;
    }
}

- (void)locationUpdate_Every_Second:(CLLocation *)location {
    
    //Average Speed Getting
    avgSpeed =  ride_Dist / rideTime_coutDown;
    float avg_speed = avgSpeed * 2.2369356;
    //int avg=roundf(avgSpeed);
    if (avg_speed>0) {
        _label_averageSpeed_rideDashboard.text = [NSString stringWithFormat: @"%.02f", avg_speed];
        NSLog(@"Average speed %.02f", avgSpeed);
        _label_averageSpeed_mapRideDashboard.text = [NSString stringWithFormat: @"%.02f", avg_speed];
    }
    
    //Distance Getting
    if (previous_location.coordinate.latitude==location.coordinate.latitude &&previous_location.coordinate.longitude==location.coordinate.longitude)
    {
        NSLog(@"you Are not moved");
    }
    else
    {
        //   dist_pr_to_Cur_LOC = [previous_location distanceFromLocation:location];
        /*   int time=rideTime_coutDown-last_Ride_Time;
         bool is_valid=false;
         float d_spee=dist_pr_to_Cur_LOC/time;
         
         int speed_=location.speed;
         if (d_spee>2 ||speed_>2)
         {
         float temp = 20;
         if (avgSpeed > temp)
         {
         temp = avgSpeed;
         }
         if (avgSpeed > 0 && (d_spee < temp ||speed_<temp))
         {
         is_valid = true;
         } else if (avgSpeed<=0)
         {
         is_valid = true;
         }
         }*/
        
        bool is_valid=false;
        int speed_=location.speed;
        if (speed_>2)
        {
//            float temp = 45;
//            if (avgSpeed > temp)
//            {
//                temp = avgSpeed;
//            }
//            if (avgSpeed > 0 && speed_<temp)
//                is_valid = true;
//            else if (avgSpeed<=0)
//                is_valid = true;
            
            is_valid = true;
            
        }
        
        if (is_valid) {
            last_Ride_Time=rideTime_coutDown;
            dist_pr_to_Cur_LOC = [previous_location distanceFromLocation:location];
            ride_Dist = ride_Dist+dist_pr_to_Cur_LOC;
            NSLog(@"Distance is %f",ride_Dist);
            previous_location = location;
            
            
            // float km = distance/1000;
            miles = (ride_Dist / 1609.344);
            _label_totalDistance_rideDashboard.text = [NSString stringWithFormat:@"%.2f",miles];
            _labe_totalDistance_mapRideDashboard.text= [NSString stringWithFormat:@"%.2f",miles];
            ride_totalDistance=[NSString stringWithFormat:@"%@",_labe_totalDistance_mapRideDashboard.text];
            //Distance Ending
            
            
            //MaxSpeed Getting
            NSLog(@"didUpdateToLocation: %f", miles);
            locationSpeedValue=location.speed;
            int speedM = locationSpeedValue * 2.2369356;
            if (speedM > maxspeed) {
                maxspeed = speedM;
                _label_maxSpeed_rideDashboard.text = [NSString stringWithFormat:@"%2d",maxspeed];
            }
            
            
            // altitude to Feet
            altitude=location.altitude;
            if (altitude!=0)
            {
                elevation=altitude*3.28084;
                _label_elevation_rideDashboard.text = [NSString stringWithFormat:@"%.0f", elevation];
                minElevation=[[NSString stringWithFormat:@"%.0f", elevation] floatValue];
                if (elevation>=maxElevation) {
                    maxElevation=elevation;
                    [self Routes_Table_Update_Method:@"Locations"];
                }
               else if (elevation<minElevation) {
                    minElevation=elevation;
                    [self Routes_Table_Update_Method:@"Locations"];
                }
            }
            
            
            //For Default Marker setting
            NSString *ride_Distance=[NSString stringWithFormat:@"%.2f",miles];
            NSInteger distance_is=[ride_Distance integerValue];
            
            if (distance_is>0&&original_Marker_Val!=0)
            {
                if ((distance_is%original_Marker_Val)==0)
                {
                    if (distance_is!=marker_count)
                    {
                        GMSMarker *marker = [[GMSMarker alloc] init];
                        marker.position = CLLocationCoordinate2DMake(location.coordinate.latitude,location.coordinate.longitude);
                        marker.appearAnimation = kGMSMarkerAnimationPop;
                        marker.icon = [UIImage imageNamed:@"default_marker_is.png"];
                        marker.map = mapView;
                        
                        marker_count=distance_is;
                        
                        //Saving DefaultMarkers In Entity
                        NSError *error=nil;
                        NSManagedObjectContext *context=[self managedObjectContext];
                        Defaultmarkers *object=[[Defaultmarkers alloc]initWithContext:context];
                        
                        object.latitudemarker=location.coordinate.latitude;
                        object.longitudemarker=location.coordinate.longitude;
                        object.local_rid=local_Ride_ID;
                        object.ride_id_marker=appDelegate.ride_ID_Api;
                        object.descr_marker=[NSString stringWithFormat:@"WAY POINT%ld",(long)marker_count];
                        
                        if ([object.managedObjectContext save:&error]) {
                        }
                        
                        
                    }
                }
            }
            
            //Update UserLocation
            if (distance_is>=1) {
            if (distance_is!=loc_Update_count)
            {
                loc_Update_count=distance_is;
                if ([SHARED_HELPER checkIfisInternetAvailable]) {
                  [self user_update_location:location.coordinate.latitude :location.coordinate.longitude is_show:@"Yes" ];
                    
                    if ([ride_strat_adress isEqualToString:@""])
                    {
                        [self getAdrressFromLatLong:@"start"location_twod:CLLocationCoordinate2DMake([ride_Start_Lat doubleValue],[ride_Start_Long doubleValue])];
                    }
                }
            }
            }
            
            
            //Draw Route
            [self drawPolyline:location];
        }
    }
}

#pragma mark LocationUpdate_Old_Methode....
/*
 - (void)locationUpdate:(CLLocation *)location {
 //Distance Getting
 if (previous_location.coordinate.latitude==location.coordinate.latitude &&previous_location.coordinate.longitude==location.coordinate.longitude)
 {
 NSLog(@"you Are not moved");
 }
 else
 {
 distance2 = [previous_location distanceFromLocation:location];
 distance = distance+distance2;
 NSLog(@"Distance is %f",distance);
 previous_location = location;
 
 // float km = distance/1000;
 miles = (distance / 1609.344);
 _label_totalDistance_rideDashboard.text = [NSString stringWithFormat:@"%.2f",miles];
 _labe_totalDistance_mapRideDashboard.text= [NSString stringWithFormat:@"%.2f",miles];
 ride_totalDistance=[NSString stringWithFormat:@"%@",_labe_totalDistance_mapRideDashboard.text];
 //Distance Ending
 
 
 //MaxSpeed Getting
 NSLog(@"didUpdateToLocation: %f", miles);
 locationSpeedValue=locationManager.location.speed;
 int speedM = locationSpeedValue * 2.2369356;
 if (speedM > maxspeed) {
 maxspeed = speedM;
 _label_maxSpeed_rideDashboard.text = [NSString stringWithFormat:@"%2d",maxspeed];
 }
 
 //Average Speed Getting
 avgSpeed =  distance / rideTime_coutDown;
 float avg_speed = avgSpeed * 2.2369356;
 int avg=roundf(avgSpeed);
 if (avg>=0 &&distance>150) {
 _label_averageSpeed_rideDashboard.text = [NSString stringWithFormat: @"%.02f", avg_speed];
 NSLog(@"Average speed %.02f", avgSpeed);
 _label_averageSpeed_mapRideDashboard.text = [NSString stringWithFormat: @"%.02f", avg_speed];
 }
 //avg speed ending
 
 // altitude to Feet
 altitude=locationManager.location.altitude;
 if (altitude!=0)
 {
 elevation=altitude*3.28084;
 _label_elevation_rideDashboard.text = [NSString stringWithFormat:@"%.0f", elevation];
 if (elevation>=maxElevation) {
 maxElevation=elevation;
 [self Routes_Table_Update_Method:@"Locations"];
 }
 }
 
 //For Default Marker setting
 NSString *ride_Distance=[NSString stringWithFormat:@"%.2f",miles];
 NSInteger distance_is=[ride_Distance integerValue];
 
 if (distance_is>0&&original_Marker_Val!=0)
 {
 if ((distance_is%original_Marker_Val)==0)
 {
 if (distance_is!=marker_count)
 {
 GMSMarker *marker = [[GMSMarker alloc] init];
 marker.position = CLLocationCoordinate2DMake(location.coordinate.latitude,location.coordinate.longitude);
 marker.appearAnimation = kGMSMarkerAnimationPop;
 marker.icon = [UIImage imageNamed:@"default_marker_is.png"];
 marker.map = mapView;
 
 marker_count=distance_is;
 
 //Saving DefaultMarkers In Entity
 NSError *error=nil;
 NSManagedObjectContext *context=[self managedObjectContext];
 Defaultmarkers *object=[[Defaultmarkers alloc]initWithContext:context];
 
 object.latitudemarker=location.coordinate.latitude;
 object.longitudemarker=location.coordinate.longitude;
 object.local_rid=local_Ride_ID;
 object.ride_id_marker=appDelegate.ride_ID_Api;
 object.descr_marker=[NSString stringWithFormat:@"WAY POINT%ld",(long)marker_count];
 
 if ([object.managedObjectContext save:&error]) {
 }
 
 
 }
 }
 }
 
 [self drawPolyline:location];
 }
 }
 */
-(void)drawPolyline:(CLLocation *)location
{
    NSMutableDictionary *locations_dict=[[NSMutableDictionary alloc]init];
    [locations_dict setObject:[NSNumber numberWithDouble:location.coordinate.latitude]  forKey:@"latitude"];
    [locations_dict setObject:[NSNumber numberWithDouble:location.coordinate.longitude] forKey:@"longtitude"];
    
    
    [path addCoordinate:CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)];
    if ([appDelegate.app_Status isEqualToString:@"Riding"]) {
        polyline.path=path;
        NSLog(@"PolyLine Drawed");
    }
    
    
    //core data
    NSError *error=nil;
    NSManagedObjectContext *context=[self managedObjectContext];
    Locations *object=[[Locations alloc]initWithContext:context];
    object.latitude=location.coordinate.latitude;
    object.longitude=location.coordinate.longitude;
    object.elevation=location.altitude*3.28084;
    object.local_rid=local_Ride_ID;
    object.ride_id=[appDelegate.ride_ID_Api intValue];
    [object.managedObjectContext save:&error];
    
    
}
-(void)Show_path_for_unexpected_stoped_rides:(NSString *)entity
{
    NSError *error;
    _dataEntity = [NSEntityDescription entityForName:entity inManagedObjectContext:self.managedObjectContext];
    NSFetchRequest * fr = [[NSFetchRequest alloc]init];
    [fr setEntity:_dataEntity];
    NSNumber *stdUserNumber = [NSNumber numberWithLongLong:local_Ride_ID];
    fr.predicate=[NSPredicate predicateWithFormat:@"local_rid == %@",stdUserNumber];
    NSMutableArray * result = [[self.managedObjectContext executeFetchRequest:fr error:&error] mutableCopy];
    
    if ([entity isEqualToString:Locations_Table]) {
        for (NSManagedObject *fetchResult in result)
        {
            double lat=[[fetchResult valueForKey:@"latitude"] doubleValue];
            double long_is=[[fetchResult valueForKey:@"longitude"] doubleValue];
            [path addCoordinate:CLLocationCoordinate2DMake(lat, long_is)];
        }
        polyline.path=path;
        
        [result removeAllObjects];
    }
    else if([entity isEqualToString:Waypoints_Table])
    {
        for (unsigned short int i=0;i<result.count;i++)
        {
            NSManagedObject *fetchResult=[result  objectAtIndex:i];
            double lat=[[fetchResult valueForKey:@"latitudemarker"] doubleValue];
            double long_is=[[fetchResult valueForKey:@"longitudemarker"] doubleValue];
            
            GMSMarker *marker = [[GMSMarker alloc] init];
            marker.position = CLLocationCoordinate2DMake(lat,long_is);
            marker.appearAnimation = kGMSMarkerAnimationPop;
            marker.icon = [UIImage imageNamed:@"default_marker_is.png"];
            marker.map = mapView;
        }
        [result removeAllObjects];
    }
    else
    {
        for (unsigned short int i=0;i<result.count;i++)
        {
            AddPlace *Object=[result  objectAtIndex:i];
            double lat=Object.lat;
            double long_is=Object.longitude;
            
            GMSMarker *marker = [[GMSMarker alloc] init];
            marker.position = CLLocationCoordinate2DMake(lat,long_is);
            marker.appearAnimation = kGMSMarkerAnimationPop;
            marker.icon = [UIImage imageNamed:@"addplace_marker_bg.png"];
            marker.title=Object.title;
            marker.map = mapView;
        }
        [result removeAllObjects];
    }
    
    if ([entity isEqualToString:Locations_Table]) {
        [self Show_path_for_unexpected_stoped_rides:Waypoints_Table];
    }
    else if ([entity isEqualToString:Waypoints_Table])
    {
        [self Show_path_for_unexpected_stoped_rides:Add_places_Table];
    }
}


-(void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading
{
    
    if ([ride_status isEqualToString:@"Started"]) {
        
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:map_currentlocation_coordinate.latitude longitude:map_currentlocation_coordinate.longitude zoom:mapZoom bearing:newHeading.trueHeading viewingAngle:mapView.camera.viewingAngle];
        if ([appDelegate.app_Status isEqualToString:@"ForeGround"]) {
            [mapView animateToCameraPosition:camera];
            appDelegate.app_Status=@"Riding";
            polyline.path=path;
            isPanned=false;
        }
        else{
            if (isPanned) {
                
            }
            else
                [mapView animateToCameraPosition:camera];
            // [mapView animateToBearing:newHeading.trueHeading];
        }
        //        [mapView animateToViewingAngle:45];
    }
}
-(void)mapView:(GMSMapView *)mapView1 didChangeCameraPosition:(GMSCameraPosition*)position {
    if ([ride_status isEqualToString:@"Started"]) {
        mapZoom = mapView1.camera.zoom;
    }
    //    if ([ride_status isEqualToString:@"Started"]) {
    //        mapZoom = mapView1.camera.zoom;
    //        GMSVisibleRegion region = mapView.projection.visibleRegion;
    //        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithRegion:region];
    //        if (![bounds containsCoordinate:map_currentlocation]){
    //            if (isTapped) {
    //                 isPanned = true;
    //            }
    //            else
    //            isPanned = false;
    //        }
    //        else
    //        {
    //            isPanned = true;
    //
    //        }
    //        [mapView animateToViewingAngle:mapView1.camera.viewingAngle];
    //    }
}

- (void)mapView:(GMSMapView *)mapViewis willMove:(BOOL)gesture{
    
    if ([ride_status isEqualToString:@"Started"])
    {
        if (gesture)
        {
            isPanned=true;
        }
    }
}
- (IBAction)zoomToUserLocations:(id)sender { //go to curent position
    
    GMSCameraPosition *camera;
    CLHeading *heading;
    if ([ride_status isEqualToString:@"Started"]) {
        camera = [GMSCameraPosition cameraWithLatitude:map_currentlocation_coordinate.latitude
                                             longitude:map_currentlocation_coordinate.longitude
                                                  zoom:18  bearing:heading.trueHeading
                                          viewingAngle:50];
        isPanned=false;
        
    }
    else{
        camera = [GMSCameraPosition cameraWithLatitude:map_currentlocation_coordinate.latitude
                                             longitude:map_currentlocation_coordinate.longitude
                                                  zoom:mapZoom];
    }
    
    [mapView animateToCameraPosition:camera];
}

#pragma mark Saving & Updating  Ride_Route_Details In------> CoreData
-(void)Rides_Routes_TableCreate_Method
{
    NSManagedObjectContext *context=[self managedObjectContext];
    Rides *rides=[[Rides alloc]initWithContext:context];
    rides.user_id=[NSString stringWithFormat:@"%@",[Defaults valueForKey:User_ID]];
    rides.local_rid=local_Ride_ID;
    rides.ride_id=appDelegate.ride_ID_Api;
    rides.ride_owner_user_id=ride_owner_id;
    rides.ride_name=ride_Title;
    rides.ride_descr=created_ride_description;
    rides.ride_type=[SHARED_HELPER rideType];
    rides.ride_time=self.label_rideTime_rideDashboard.text;
    rides.ride_total_time=self.label_totalTime_rideDashboard.text;
    rides.ride_waypoints=[NSString stringWithFormat:@"%d",original_Marker_Val];
    rides.avgSpeed=self.label_averageSpeed_rideDashboard.text;
    rides.totalDistance=self.label_totalDistance_rideDashboard.text;
    rides.ride_start_date=ride_Start_date_String;
    rides.ride_start_time=ride_Start_time_String;
    rides.ride_end_date=ride_end_date_String;
    rides.ride_end_time=ride_end_time_String;
    rides.ride_start_adress=ride_strat_adress;
    rides.ride_end_adress=@"";
    rides.ride_start_lat=[ride_Start_Lat doubleValue];
    rides.ride_start_long=[ride_Start_Long doubleValue];
    rides.ride_end_lat=[ride_End_Lat doubleValue];
    rides.ride_end_long=[ride_End_Long doubleValue];
    int rides_count=[_riders_Count.text intValue];
    rides.rides_count=[NSString stringWithFormat:@"%d",rides_count];
    float maxSpeed_is=[self.label_maxSpeed_rideDashboard.text floatValue];
    rides.maxSpeed=[NSString stringWithFormat:@"%f",maxSpeed_is];
    rides.randonNumber=appDelegate.ride_Random_Number;
    rides.ride_status=table_status_pending;
    rides.ride_images=@"";
    [rides.managedObjectContext save:nil];
    
    NSManagedObjectContext *context2=[self managedObjectContext];
    Routes *routes=[[Routes alloc]initWithContext:context2];
    routes.route_id=appDelegate.select_Route_Id_For_CreateAndFreeRide;
    routes.local_rid=local_Ride_ID;
    routes.route_name=selected_route_title;
    routes.route_desc=select_Route_Des;
    routes.route_privacy=@"public";
    routes.gpx_url=@"";
    routes.existing_gpx_url=selected_Route_GPX;
    routes.route_owner_user_id=selected_route_owner_id;
    routes.gpx_file=@"";
    float maxElev=[_label_elevation_rideDashboard.text floatValue];
    routes.maxElevation=[NSString stringWithFormat:@"%f",maxElev];
    routes.minElevation=[NSString stringWithFormat:@"%f",minElevation];
    routes.route_comments=select_Route_Des;
    routes.route_ratings=@"";
    [routes.managedObjectContext save:nil];
}
-(BOOL)Rides_Table_Update_Method :(NSString *)call_from
{
    NSError *error;
    NSFetchRequest * fr = [[NSFetchRequest alloc]initWithEntityName:Rides_Table];
    NSNumber *stdUserNumber = [NSNumber numberWithLongLong:local_Ride_ID];
    fr.predicate=[NSPredicate predicateWithFormat:@"local_rid ==%@",stdUserNumber];
    NSMutableArray * result = [[self.managedObjectContext executeFetchRequest:fr error:&error] mutableCopy];
    if (result.count>0) {
        Rides *rides=[result objectAtIndex:0];
        if ([call_from isEqualToString:@"END_Ride"]) {
            
            NSDictionary *Save_Ride_Dict=[appDelegate.Save_Route_Data_Array objectAtIndex:1];
            NSArray * ride_Images_aray=[appDelegate.Save_Route_Data_Array objectAtIndex:2];
            
            rides.ride_name=[Save_Ride_Dict valueForKey:@"RideTitle"];
            rides.ride_descr=[Save_Ride_Dict valueForKey:@"RideDescriptionDes"];
            rides.ride_status=table_status_complete;
            NSString *ride_images_str=[ride_Images_aray componentsJoinedByString:@","];
            rides.ride_images=ride_images_str;
            rides.ride_end_date=ride_end_date_String;
            rides.ride_end_time=ride_end_time_String;
            rides.ride_end_lat=[ride_End_Lat doubleValue];
            rides.ride_end_long=[ride_End_Long doubleValue];
            rides.ride_end_adress=ride_end_Adress;
        }
        else if ([call_from isEqualToString:@"Quit_Ride"])
        {
            rides.ride_end_lat=[ride_End_Lat doubleValue];
            rides.ride_end_long=[ride_End_Long doubleValue];
            rides.ride_end_adress=ride_end_Adress;
            rides.ride_status=table_status_quit;
        }
        else{
            rides.ride_name=ride_Title;
        }
        rides.ride_time=self.label_rideTime_rideDashboard.text;
        rides.ride_total_time=self.label_totalTime_rideDashboard.text;
        rides.avgSpeed=self.label_averageSpeed_rideDashboard.text;
        rides.totalDistance=self.label_totalDistance_rideDashboard.text;
        rides.ride_start_adress=ride_strat_adress;
       
        int rides_count=[_riders_Count.text intValue];
        rides.rides_count=[NSString stringWithFormat:@"%d",rides_count];
        float maxSpeed_is=[self.label_maxSpeed_rideDashboard.text floatValue];
        rides.maxSpeed=[NSString stringWithFormat:@"%f",maxSpeed_is];
        
        if ([rides.managedObjectContext save:nil]) {
            return YES;
        }
        else
            return NO;
    }
    else
        return NO;
}
-(void)Routes_Table_Update_Method :(NSString *)call_from
{
    NSError *error;
    NSFetchRequest * fr = [[NSFetchRequest alloc]initWithEntityName:Routes_Table];
    NSNumber *stdUserNumber = [NSNumber numberWithLongLong:local_Ride_ID];
    fr.predicate=[NSPredicate predicateWithFormat:@"local_rid ==%@",stdUserNumber];
    NSMutableArray * result = [[self.managedObjectContext executeFetchRequest:fr error:&error] mutableCopy];
    if (result.count>0) {
        Routes *routes=[result objectAtIndex:0];
        
        if ([call_from isEqualToString:@"END_Ride"]) {
            NSDictionary *Save_Route_Dict=[appDelegate.Save_Route_Data_Array objectAtIndex:0];
            routes.route_name=[Save_Route_Dict valueForKey:@"Title"];
            routes.route_desc=[Save_Route_Dict valueForKey:@"Des"];
            routes.route_privacy=[Save_Route_Dict valueForKey:@"RouteStatus"];
            routes.route_comments=[Save_Route_Dict valueForKey:@"Comments"];
            routes.route_ratings=[Save_Route_Dict valueForKey:@"Rating"];
        }
        else
        {
            
        }
        routes.maxElevation=[NSString stringWithFormat:@"%.0f",maxElevation];
        routes.minElevation=[NSString stringWithFormat:@"%0f",minElevation];
        [routes.managedObjectContext save:nil];
    }
}
-(void)Update_AddPlace_Table  :(NSInteger)id_is amazon_url:(NSString *)image_url{
    NSManagedObjectContext *myContext = [self managedObjectContext];
    NSFetchRequest *req=[NSFetchRequest fetchRequestWithEntityName:@"AddPlace"];
    NSNumber *stdUserNumber = [NSNumber numberWithInteger:id_is];
    req.predicate=[NSPredicate predicateWithFormat:@"places_id == %@",stdUserNumber];
    NSArray *places=[myContext executeFetchRequest:req error:nil];
    
    if (places.count>0) {
        AddPlace *place=[places objectAtIndex:0];
        
        NSString *urls=place.img_urls;
        NSString *paths=place.img_paths;
        int16_t img_count=place.img_count;
        
        NSArray *img_urls_array=[urls componentsSeparatedByString:@","];
        NSArray *img_paths_array=[paths componentsSeparatedByString:@","];
        NSMutableArray *urls_mut_array=[[NSMutableArray alloc]init];
        if (img_count>0) {
            [urls_mut_array addObjectsFromArray:img_urls_array];
            NSString *url=[urls_mut_array objectAtIndex:0];
            if (url.length>0) {
                
            }
            else
            {
                [urls_mut_array removeObjectAtIndex:0];
            }
            [urls_mut_array addObject:image_url];
            urls=[urls_mut_array componentsJoinedByString:@","];
            place.img_urls=urls;
        }
        else{
            place.img_urls=image_url;
        }
        
        if (img_paths_array.count<=urls_mut_array.count) {
            place.status=table_status_complete;
        }
        else
        {
            
        }
        [place.managedObjectContext save:nil];
    }
}

#pragma mark fecthing and deleting tracks and defaultmarkers data from  ------>Core Data
- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}
-(NSMutableArray *)gettingAddPlacesFromCoreData:(NSString * )entity_name
{
    NSError *error;
    _dataEntity = [NSEntityDescription entityForName:entity_name inManagedObjectContext:self.managedObjectContext];
    NSFetchRequest * fr = [[NSFetchRequest alloc]init];
    [fr setEntity:_dataEntity];
    NSNumber *stdUserNumber = [NSNumber numberWithLongLong:local_Ride_ID];
    fr.predicate=[NSPredicate predicateWithFormat:@"local_rid ==%@",stdUserNumber];
    NSMutableArray * result = [[self.managedObjectContext executeFetchRequest:fr error:&error] mutableCopy];
    
    NSMutableArray *results_array=[[NSMutableArray alloc]init];
    for (unsigned short int i=0;i<result.count;i++)
    {
        AddPlace *fetchResult=[result  objectAtIndex:i];
        NSMutableDictionary *coreDataDict=[[NSMutableDictionary alloc] init];
        [coreDataDict setValue:[NSString stringWithFormat:@"%hd",fetchResult.img_count] forKey:@"img_count"];
        NSString *paths=fetchResult.img_paths;
        if (paths.length>0) {
            NSArray *img_paths=[paths componentsSeparatedByString:@","];
            [coreDataDict setValue:img_paths forKey:addPlaces_table_imgpaths];
        }
        else
        {
            NSMutableArray *empty=[[NSMutableArray alloc]init];
            [coreDataDict setValue:empty forKey:addPlaces_table_imgpaths];
        }
        
        NSString *urls=fetchResult.img_urls;
        if (urls.length>0) {
            NSArray *img_urls=[urls componentsSeparatedByString:@","];
            [coreDataDict setValue:img_urls forKey:addPlaces_table_imgUrls];
        }
        else
        {
            NSMutableArray *empty=[[NSMutableArray alloc]init];
            [coreDataDict setValue:empty forKey:addPlaces_table_imgUrls];
        }
        
        [coreDataDict setValue:fetchResult.status forKey:addPlaces_table_status];
        [coreDataDict setValue:[NSString stringWithFormat:@"%lld",fetchResult.places_id] forKey:addPlaces_table_id];
        [coreDataDict setValue:[NSString stringWithFormat:@"%lld",fetchResult.local_rid] forKey:all_table_local_rid];
        [results_array addObject:coreDataDict];
    }
    return results_array;
}

-(int64_t)get_ride_Table_count{
    NSError *error;
    NSFetchRequest * fr = [[NSFetchRequest alloc]initWithEntityName:Rides_Table];
    NSMutableArray * result = [[self.managedObjectContext executeFetchRequest:fr error:&error] mutableCopy];
    int64_t local_Rid=1;
    if (result.count>0) {
        Rides *object=[result objectAtIndex:result.count-1];
        local_Rid=object.local_rid+1;
    }
    return local_Rid;
}

-(NSArray *)get_pending_Rides_From_DB{
    NSManagedObjectContext *myContext = [self managedObjectContext];
    NSFetchRequest *req=[NSFetchRequest fetchRequestWithEntityName:Rides_Table];
    NSArray *ride=[myContext executeFetchRequest:req error:nil];
    NSMutableArray *rides_results_array=[[NSMutableArray alloc]init];
    for (Rides *object in ride) {
        if ([object.ride_status isEqualToString:table_status_pending]) {
            [rides_results_array addObject:object];
            break;
        }
    }
    return rides_results_array;
}

-(NSManagedObject *)get_pending_Route_From_DB{
    NSManagedObjectContext *myContext = [self managedObjectContext];
    NSFetchRequest *req=[NSFetchRequest fetchRequestWithEntityName:Routes_Table];
    NSNumber *stdUserNumber = [NSNumber numberWithLongLong:local_Ride_ID];
    req.predicate=[NSPredicate predicateWithFormat:@"local_rid == %@",stdUserNumber];
    NSArray *route=[myContext executeFetchRequest:req error:nil];
    NSManagedObject *object=[route objectAtIndex:0];
    return object;
}

- (BOOL)Deleted_Curent_Ride_Details_in_AllTables:(NSString *)entity
{
    NSError *error = nil;
    NSManagedObjectContext *myContext = [self managedObjectContext];
    NSFetchRequest *fetchLLObjects = [[NSFetchRequest alloc] init];
    
    [fetchLLObjects setEntity:[NSEntityDescription entityForName:entity inManagedObjectContext:myContext]];
    NSNumber *stdUserNumber = [NSNumber numberWithLongLong:local_Ride_ID];
    fetchLLObjects.predicate=[NSPredicate predicateWithFormat:@"local_rid ==%@",stdUserNumber];
    [fetchLLObjects setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSArray *allObjects = [myContext executeFetchRequest:fetchLLObjects error:&error];
    for (NSManagedObject *object in allObjects) {
        [myContext deleteObject:object];
    }
    
    if ([myContext save:&error]) {
        NSLog(@"Empty saved  ");
        return YES;
    }
    else
    {
        NSLog(@"Empty not saved  ");
        return NO;
    }
}

/*
 -(void)gettingDataFromCoreData:(NSString * )entity_name
 {
 NSError *error;
 _dataEntity = [NSEntityDescription entityForName:entity_name inManagedObjectContext:self.managedObjectContext];
 NSFetchRequest * fr = [[NSFetchRequest alloc]init];
 [fr setEntity:_dataEntity];
 NSMutableArray * result = [[self.managedObjectContext executeFetchRequest:fr error:&error] mutableCopy];
 for (unsigned short int i=0;i<result.count;i++)
 {
 NSManagedObject *fetchResult=[result  objectAtIndex:i];
 NSMutableDictionary *coreDataDict=[[NSMutableDictionary alloc] init];
 if ([entity_name  isEqualToString:@"Locations"]) {
 [coreDataDict setValue:[fetchResult valueForKey:@"latitude"] forKey:@"lat"];
 [coreDataDict setValue:[fetchResult valueForKey:@"longitude"] forKey:@"long"];
 [coreDataDict setValue:[fetchResult valueForKey:@"elevation"] forKey:@"ele"];
 //              [coreDataDict setValue:[fetchResult valueForKey:@"time_stamp"] forKey:@"time"];
 [trackpoints addObject:coreDataDict];
 }
 else{
 [coreDataDict setValue:[fetchResult valueForKey:@"latitudemarker"] forKey:@"lat"];
 [coreDataDict setValue:[fetchResult valueForKey:@"longitudemarker"] forKey:@"long"];
 [way_Points_Array addObject:coreDataDict];
 }
 
 }
 [result removeAllObjects];
 }
 */



#pragma mark -Back_Action
- (IBAction)Back_Action:(id)sender
{
    if ([ride_status isEqualToString:@"Started"] || [ride_status isEqualToString:@"Paused"])
    {
        
        //   [SHARED_HELPER showAlert:UR_IN_RIDE];
        [self showpopup:@"Are you sure you want to quit the ride? Make sure that your ride data will be lost."];
    }
    else{
        //Navigate to Dashboard/
        if ([_is_From isEqualToString:@"HOME"]) {
            HomeDashboardView *dashboard = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeDashboardView"];
            [self.navigationController pushViewController:dashboard animated:YES];
        }
        else
        {
            DashboardViewController *dashboard = [self.storyboard instantiateViewControllerWithIdentifier:@"DashboardViewController"];
            
            [self.navigationController pushViewController:dashboard animated:YES];
        }
        map_locationManager=nil;
        locationManager=nil;
    }
   
}
-(void)showpopup:(NSString *)message
{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Quit Ride"
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"No"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Yes"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [self Finish_Ride];
                                 //Deleted Curent Ride details from all tables
                                 indicaterview.hidden=NO;
                                 s3_inProgress=@"STOP";
                                 appDelegate.ridedashboard_home=NO;
                                 
                                 if ([appDelegate.ride_ID_Api isEqualToString:@""]) {
                                     if ([self Deleted_Curent_Ride_Details_in_AllTables:Rides_Table]) {
                                         [self Deleted_Curent_Ride_Details_in_AllTables:Routes_Table];
                                         [self Deleted_Curent_Ride_Details_in_AllTables:Locations_Table];
                                         [self Deleted_Curent_Ride_Details_in_AllTables:Waypoints_Table];
                                         [self Deleted_Curent_Ride_Details_in_AllTables:Add_places_Table];
                                     }
                                 }
                                 else
                                 {
                                     if ([self Rides_Table_Update_Method:@"Quit_Ride"]) {
                                         if ([SHARED_HELPER internetAvailable]) {
                                             if ([appDelegate.is_Running_Background_Sync isEqualToString:@"NO"]) {
                                                 [[NSNotificationCenter defaultCenter] postNotificationName:@"SaveRide_Service" object:self]; //in appdelegate
                                             }
                                         }
                                     }
                                 }
                                 
                                 
                                 
                                 
                                 [Defaults setObject:@"RIDE_ENDED" forKey:@"RIDE_STATUS"];
                                 int duration = 1;
                                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                     
                                     if ([_is_From isEqualToString:@"HOME"]) {
                                         HomeDashboardView *dashboard = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeDashboardView"];
                                         [self.navigationController pushViewController:dashboard animated:YES];
                                     }
                                     else
                                     {
                                         DashboardViewController *dashboard = [self.storyboard instantiateViewControllerWithIdentifier:@"DashboardViewController"];
                                         [self.navigationController pushViewController:dashboard animated:YES];
                                     }
                                     indicaterview.hidden=YES;
                                     
                                 });
                                 map_locationManager=nil;
                                 locationManager=nil;
                                 
                               
                               
                                 
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)onclick_ridedashboard_homeButton:(id)sender {
    if([[Defaults valueForKey:@"RIDE_STATUS"]isEqualToString:@"RIDE_STARTED"])
    {
        appDelegate.ridedashboard_home=YES;
        self.ridedashboard_homeBtn.hidden=NO;
        HomeDashboardView*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"HomeDashboardView"];
//          [KGStatusBar showWithStatus:@"Ride is ongoing.Touch to open the ride"];
        [self.navigationController pushViewController:controller animated:YES];
    }
    
    
}
@end



#pragma mark -MailDismis
@implementation RideDashboard (MFMailComposeViewControllerDelegate)

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    //    [self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
@end
