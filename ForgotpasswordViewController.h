//
//  ForgotpasswordViewController.h
//  
//
//  Created by apple on 01/05/17.
//
//

#import <UIKit/UIKit.h>

@interface ForgotpasswordViewController : UIViewController<UITextFieldDelegate>{
    UIActivityIndicatorView  *activeIndicatore;
}
@property (weak, nonatomic) IBOutlet UITextField *Email_tf;
- (IBAction)Send_mail_click:(id)sender;
- (IBAction)Login_click:(id)sender;

@end
