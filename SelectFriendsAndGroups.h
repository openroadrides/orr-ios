//
//  SelectFriendsAndGroups.h
//  openroadrides
//
//  Created by apple on 06/07/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectGroupsCell.h"
#import "SelectFriendsCell.h"
#import "Constants.h"
#import "APIHelper.h"
#import "AppHelperClass.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "RideDashboard.h"
#import "CBAutoScrollLabel.h"
@interface SelectFriendsAndGroups : UIViewController<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,UISearchBarDelegate>
{
        UIActivityIndicatorView  *activeIndicatore;
        UILabel *NO_DATA;
    NSString *view_Status_selectFrndsAndGroups,*select_Friends_Called,*select_Groups_Called,*select_friendID,*select_groupID;
    NSMutableArray *arrayOfSelectFrnds,*arratOfSelectGroups,*removeFrndsIDs;
   
    UIBarButtonItem *skip_btn_is;
    UIView *indicaterview;
    
      BOOL change_status_frnds,frndsAndGroups_isFiltered;
}
@property(strong,nonatomic)NSString *is_For_Only;
@property BOOL change_status_routeID;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *search_Bar_Top;

@property (weak, nonatomic) IBOutlet UIView *view_selectFrnds_groups_segment;
@property (weak, nonatomic) IBOutlet UIView *view_selectFrnds_segment;
@property (weak, nonatomic) IBOutlet UIView *view_selectGroups_segment;
@property (weak, nonatomic) IBOutlet UILabel *selectFrnds_label_segment;
@property (weak, nonatomic) IBOutlet UIButton *selctFrnds_btn_segment;
- (IBAction)onClick_selectFrnds_btn_segment:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *selectGroups_label_segment;
@property (weak, nonatomic) IBOutlet UIButton *selectGroups_btn_segment;
- (IBAction)onClick_selectGroups_btn_segment:(id)sender;
@property (weak, nonatomic) IBOutlet UISearchBar *selectFrndsAndGroups_searchBar;
@property (weak, nonatomic) IBOutlet UIScrollView *selectFrndsAndgroups_scrollView;
@property (weak, nonatomic) IBOutlet UIView *selectFrndsAndGroups_contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthConstraint_selectFrndsAndGroups_contentView;
@property (weak, nonatomic) IBOutlet UIView *selectFrnds_contentView;
@property (weak, nonatomic) IBOutlet UIView *selectGroups_contentView;
@property (weak, nonatomic) IBOutlet UITableView *selectFrnds_tableView;
@property (weak, nonatomic) IBOutlet UITableView *selectGroups_tableView;
- (IBAction)onClick_selectFrnds_checkbox_btn:(UIButton *)sender;
- (IBAction)onClick_selectGroups_checkbox_btn:(UIButton *)sender;
@property UIRefreshControl *selectFrnds_refreshControl,*selectGroups_refreshControl;
@property NSMutableArray *arrayOfSelectedFrndsAndGroupsID,*selectedFrndsFilterdArray;
@property NSMutableArray *arrayOfSelectedGroupsID,*selectedGroupsFilterdArray;

@property (weak, nonatomic)NSString *free_route_Frnds_groups_id_string;
@property (weak, nonatomic) IBOutlet UILabel *noData_label_frnds;
@property (weak, nonatomic) IBOutlet UILabel *noData_label_groups;
@property (weak, nonatomic) IBOutlet UIButton *frndsAndGroups_rideGoingBtn;
- (IBAction)onClick_frndsAndGroup_rideGoingBtn:(id)sender;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *frndsAndGroups_scrollLabel;

@end
