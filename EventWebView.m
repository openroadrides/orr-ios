//
//  EventWebView.m
//  openroadrides
//
//  Created by apple on 19/09/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "EventWebView.h"

@interface EventWebView ()

@end

@implementation EventWebView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [leftBtn addTarget:self action:@selector(back_Action) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.frame = CGRectMake(0, 0, 25, 25);
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"back_Image"] forState:UIControlStateNormal];
    UIBarButtonItem *leftBackButton=[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:leftBackButton, nil];
    
    
    NSURL *webURL;
    
    if ([_websiteString hasPrefix:@"http://"] || [_websiteString hasPrefix:@"https://"])
    {
        webURL = [NSURL URLWithString:_websiteString];
    }
    else
    {
        webURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@", _websiteString]];
    }

    
    
    //URL Requst Object
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:webURL];
    
    self.linkWebView .scalesPageToFit = YES;
    self.linkWebView .scrollView.scrollEnabled = TRUE;
    self.linkWebView .delegate=self;
    //Load the request in the UIWebView.
    [self.linkWebView loadRequest:requestObj];
    if ([ UIScreen mainScreen ].bounds.size.height == 480)
    {
        CGRect visitWebviewFrame=self.linkWebView.frame;
        visitWebviewFrame.size.height = 415;
        self.linkWebView.frame=visitWebviewFrame;
    }
    
    
    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = [UIColor blackColor];
    activeIndicatore.hidesWhenStopped = TRUE;
    [self.view addSubview:activeIndicatore];

}
-(void)back_Action{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark- webview delegate methods

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [activeIndicatore stopAnimating];
    
    NSString *string = [self.linkWebView stringByEvaluatingJavaScriptFromString:@"document.getElementsByTagName('body')[0].innerHTML"];
    BOOL isEmpty = string==nil || [string length]==0;
    
    if (isEmpty)
    {
        [self.linkWebView removeFromSuperview];
        
        UILabel *navigationTitle_label;
        navigationTitle_label = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 50, 44)];
        
        navigationTitle_label.backgroundColor = [UIColor clearColor];
        navigationTitle_label.numberOfLines = 2;
        navigationTitle_label.font = [UIFont boldSystemFontOfSize: 17.0f];
        navigationTitle_label.textAlignment = NSTextAlignmentCenter;
        navigationTitle_label.textColor = [UIColor whiteColor];
        navigationTitle_label.text = @"Webpage not available";
        
        self.navigationItem.titleView = navigationTitle_label;
        
        UILabel *noWebpageData_label;
        noWebpageData_label = [[UILabel alloc] initWithFrame:CGRectMake(5, self.view.bounds.size.height/2 - 50, self.view.bounds.size.width-20, 44)];
        
        noWebpageData_label.backgroundColor = [UIColor clearColor];
        noWebpageData_label.numberOfLines = 2;
        noWebpageData_label.font = [UIFont boldSystemFontOfSize: 17.0f];
        noWebpageData_label.textAlignment = NSTextAlignmentCenter;
        noWebpageData_label.textColor = [UIColor blackColor];
        noWebpageData_label.text = @"Requested Webpage not Found";
        
        [self.view addSubview:noWebpageData_label];
        
    }
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self webViewTitle];
    [activeIndicatore stopAnimating];
    
}
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [activeIndicatore startAnimating];
    
    
}

-(void)webViewTitle
{
    NSString *theTitle=[self.linkWebView stringByEvaluatingJavaScriptFromString:@"document.title"];
    
    UILabel *label;
    
    if (theTitle.length <=27)
        label = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 50, 44)];
    else
        label = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 300, 44)];
    
    label.backgroundColor = [UIColor clearColor];
    label.numberOfLines = 2;
    label.font = [UIFont boldSystemFontOfSize: 17.0f];
    label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor blackColor];
    
    label.text = theTitle;
    
    self.navigationItem.titleView = label;
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
