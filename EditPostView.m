//
//  EditPostView.m
//  openroadrides
//
//  Created by apple on 05/12/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "EditPostView.h"

@interface EditPostView ()

@end

@implementation EditPostView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    index = 1;
    imgIndex = 1;
    x=0;
    
   
    
    self.title = @"EDIT POST";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    
    self.editPostView_scrollLabel.hidden=YES;
    self.editPostView_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.editPostView_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.editPostView_scrollLabel.textColor = [UIColor blackColor];
    self.editPostView_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.editPostView_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.editPostView_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.editPostView_scrollLabel.scrollSpeed = 60; // pixels per second
    self.editPostView_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.editPostView_scrollLabel.fadeLength = 0.f;
    self.editPostView_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.editPostView_scrollLabel observeApplicationNotifications];
    
    self.editPost_rideGoingBtn.hidden=YES;
    self.bottomConstraint_editPostImagesView.constant=-30;
    if (appDelegate.ridedashboard_home) {
        
        self.editPostView_scrollLabel.hidden=NO;
        self.bottomConstraint_editPostImagesView.constant=0;
        self.editPost_rideGoingBtn.hidden=NO;
    }
    
    UIButton *saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [saveBtn addTarget:self action:@selector(Save_Clicked) forControlEvents:UIControlEventTouchUpInside];
    saveBtn.frame = CGRectMake(0, 0, 30, 30);
    [saveBtn setBackgroundImage:[UIImage imageNamed:@"SAVE"] forState:UIControlStateNormal];
    UIBarButtonItem * save_post= [[UIBarButtonItem alloc] initWithCustomView:saveBtn];
    self.navigationItem.rightBarButtonItems=[NSArray arrayWithObjects:save_post, nil];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [leftBtn addTarget:self action:@selector(cancle_clicked) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.frame = CGRectMake(0, 0, 25, 25);
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"close_icon"] forState:UIControlStateNormal];
    UIBarButtonItem *leftBackButton=[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    item0=leftBackButton;
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:item0, nil];
    
    
    
    self.editPost_titleTF.delegate=self;
    self.editPost_priceTF.delegate=self;
    self.editPost_addressTF.delegate=self;
    self.editPost_phoneNumberTF.delegate=self;
    self.editPost_emailTF.delegate=self;
    self.editPost_typeTF.delegate=self;
    self.editPost_descriptionTextView.delegate=self;
    
    
    [self.editPost_titleTF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.editPost_priceTF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.editPost_addressTF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.editPost_phoneNumberTF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.editPost_emailTF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.editPost_typeTF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.editPost_descriptionTextView setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    
//    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
//    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
//    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
//                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
//                            [[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
//    [numberToolbar sizeToFit];
//    _editPost_priceTF.inputAccessoryView = numberToolbar;
    
    
    
    UIToolbar* numberToolbar1 = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar1.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar1.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad1)],
                             [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                             [[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad1)]];
    [numberToolbar1 sizeToFit];
    _editPost_phoneNumberTF.inputAccessoryView = numberToolbar1;
    
    
    UIToolbar* numberToolbar2 = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar2.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar2.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad2)],
                             [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                             [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad2)]];
    [numberToolbar2 sizeToFit];
    _editPost_descriptionTextView.inputAccessoryView = numberToolbar2;
    
    self.editPost_addressTF.userInteractionEnabled=NO;
    
  
    if (_post_details.count>0) {
    
         self.editPost_titleTF.text=[_post_details valueForKey:@"post_title"];
         self.editPost_priceTF.text=[NSString stringWithFormat:@"%@",[_post_details valueForKey:@"post_price"]];
        if([self.editPost_priceTF.text isEqualToString:@"0"])
        {
            self.editPost_priceTF.text=@"";
        }
        
         self.editPost_addressTF.text=[_post_details valueForKey:@"post_address"];
         self.editPost_phoneNumberTF.text=[_post_details valueForKey:@"phone_number"];
         self.editPost_emailTF.text=[_post_details valueForKey:@"email"];
        
         self.editPost_descriptionTextView.text=[_post_details valueForKey:@"description"];
        if (_editPost_descriptionTextView.text.length == 0) {
            _editPost_descriptionLabel.hidden=NO;
        }
        else
        {
            _editPost_descriptionLabel.hidden=YES;
        }
        
         post_latitude=[[_post_details valueForKey:@"post_latitute"] doubleValue];
         post_longitude=[[_post_details valueForKey:@"post_longitude"] doubleValue];
         post_status=[_post_details valueForKey:@"status"];
         post_id=[NSString stringWithFormat:@"%@",[_post_details valueForKey:@"post_id"]];
         categere_id=[NSString stringWithFormat:@"%@",[_post_details valueForKey:@"categories_id"]];
        if ([categere_id isEqualToString:@"2"] || [_editPost_typeTF.text isEqualToString:@"Personal"]) {
            self.editPost_priceTF.userInteractionEnabled=NO;
             self.editPost_priceTF.text=@"";
            _editpost_priceImage.hidden=YES;
            _editPost_priceTF.hidden=YES;
            _topConstraint_editPost_priceBottomView.constant=-41;
        }
        
        
        
        
         typer_picker = [[UIPickerView alloc]init];
         typer_picker.dataSource = self;
         typer_picker.delegate = self;
         typer_picker.showsSelectionIndicator = YES;
         UIToolbar* numberToolbar3 = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
         numberToolbar3.barStyle = UIBarStyleBlackTranslucent;
         numberToolbar3.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                 [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(type_Picker_Done)]];
         [numberToolbar3 sizeToFit];
         _editPost_typeTF.inputAccessoryView = numberToolbar3;
         _editPost_typeTF.inputView = typer_picker;
       
    }
    
    img_array = [[NSMutableArray alloc]init];
    deleted_ids=[[NSMutableArray alloc]init];
    base_64_image_array= [[NSMutableArray alloc]init];
    [img_array addObjectsFromArray:[_post_details valueForKey:@"post_image_names"]];
    self.editPost_multipleImages_scrollview.showsHorizontalScrollIndicator = NO;
    add_btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 15, 90,90)];
    [add_btn addTarget:self action:@selector(add_click:) forControlEvents:UIControlEventTouchUpInside];
    //    add_btn.backgroundColor = [UIColor greenColor];
    [add_btn setBackgroundImage:[UIImage imageNamed:plus_icon] forState:UIControlStateNormal];
    [self.editPost_multipleImages_scrollview addSubview:add_btn];
    if (img_array.count>0) {
        
        for (int i=0; i<img_array.count; i++) {
            if (index == 1) {
                x = 0;
            }
            baseView_placesImages = [[UIView alloc]initWithFrame:CGRectMake(x, 0, 150, 120)];
            baseView_placesImages.tag = 100+index;
            baseView_placesImages.backgroundColor = [UIColor clearColor];
            [self.editPost_multipleImages_scrollview addSubview:baseView_placesImages];
            
            img = [[UIImageView alloc] initWithFrame:CGRectMake(0,15,90, 90)];
            [img setImageWithURL:[NSURL URLWithString:[[img_array objectAtIndex:i] valueForKey:@"post_image_names"]] placeholderImage:[UIImage imageNamed:@"add_places_placeholder"]];
            img.contentMode=UIViewContentModeScaleAspectFit;
            
            [baseView_placesImages addSubview:img];
            subtract_btn = [[UIButton alloc]initWithFrame:CGRectMake(img.frame.size.width-12.5, 0, 25, 25)];
            [subtract_btn setBackgroundImage:[UIImage imageNamed:@"wrong_icon.png"] forState:UIControlStateNormal];
            [subtract_btn.titleLabel setFont:[UIFont systemFontOfSize:20]];
            [subtract_btn addTarget:self action:@selector(sub_click:) forControlEvents:UIControlEventTouchUpInside];
            subtract_btn.tag=index;
            [baseView_placesImages addSubview:subtract_btn];
            
            
            add_btn.frame = CGRectMake(baseView_placesImages.frame.origin.x+baseView_placesImages.frame.size.width-40, img.frame.origin.y, 90, 90);
            self.editPost_multipleImages_scrollview.contentSize = CGSizeMake(add_btn.frame.origin.x+add_btn.frame.size.width, 120);
            imgIndex++;
            x = x+110;
            index++;
            
        }
        if (img_array.count>=5)
        {
            self.editPost_multipleImages_scrollview.contentSize = CGSizeMake(add_btn.frame.origin.x-5, 120);
            add_btn.hidden = YES;
        }
    }
    
    
    indicaterview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    activeIndicatore.backgroundColor=[UIColor clearColor];
    activeIndicatore = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    [indicaterview addSubview:activeIndicatore];
    [self.view addSubview:indicaterview];
    [indicaterview bringSubviewToFront:self.view];
    [activeIndicatore startAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    indicaterview.hidden=YES;
    
    [self get_Categeries_List];
}

-(void)type_Picker_Done{
    if (![_editPost_typeTF.text isEqualToString:@"Personal"]) {
       
        _editPost_priceTF.userInteractionEnabled=YES;
         [_editPost_priceTF becomeFirstResponder];
        _topConstraint_editPost_priceBottomView.constant=0;
        _editpost_priceImage.hidden=NO;
        _editPost_priceTF.hidden=NO;
        
    }
    else
    {
        _editPost_priceTF.userInteractionEnabled=NO;
        [_editPost_emailTF becomeFirstResponder];
        _topConstraint_editPost_priceBottomView.constant=-41;
         _editPost_priceTF.hidden=YES;
        _editpost_priceImage.hidden=YES;
        _editPost_priceTF.text=@"";
    }

}

-(void)cancle_clicked{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)get_Categeries_List{
    indicaterview.hidden=NO;
       
    [SHARED_API get_marketPlace_Category_Type:@"" withSuccess:^(NSDictionary *response) {
        if ([[response valueForKey:STATUS]isEqualToString:SUCCESS]) {
           
            
            _categeries_Names_ids=[response valueForKey:@"categories"];
            if (_categeries_Names_ids.count>0) {
               [typer_picker reloadComponent:0];
                
                for (NSDictionary *dict in _categeries_Names_ids) {
                    if ([[NSString stringWithFormat:@"%@",[dict valueForKey:@"id"]] isEqualToString:categere_id]) {
                        _editPost_typeTF.text=[dict valueForKey:@"category_name"];
                        break;
                    }
                }
            }
            indicaterview.hidden=YES;
        }
        else{
            
            [SHARED_HELPER showAlert:@"At present you can't edit a post."];
            indicaterview.hidden=YES;
            int duration = 1;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }

    } onfailure:^(NSError *theError) {
        
        indicaterview.hidden=YES;
        [SHARED_HELPER showAlert:@"At present you can't edit a post."];
        int duration = 1;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self.navigationController popViewControllerAnimated:YES];
        });

        
    }];
}


-(void)Save_Clicked
{
    _editPost_titleTF.text=[_editPost_titleTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    _editPost_priceTF.text=[_editPost_priceTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    _editPost_addressTF.text=[_editPost_addressTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (![SHARED_HELPER checkIfisInternetAvailable]) {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
    if(_editPost_titleTF.text.length == 0 )
    {
        [SHARED_HELPER showAlert:@"Title should not be empty"];
    }
    else if(_editPost_typeTF.text.length ==0)
    {
        [SHARED_HELPER showAlert:@"Please select the post category"];
    }
    else if(_editPost_emailTF.text.length ==0)
    {
        [SHARED_HELPER showAlert:EmailText];
    }
    else if ([self NSStringIsValidEmail:_editPost_emailTF.text]== NO)
    {
        [SHARED_HELPER showAlert:ValidEmailText];
        
    }
    else if (!([_editPost_descriptionTextView.text length]>0)) {
        [SHARED_HELPER showAlert:event_des];
        return;
    }
   
    else if (_editPost_phoneNumberTF.text.length !=12 && _editPost_phoneNumberTF.text.length !=0)
    {
        [SHARED_HELPER showAlert:MobileText];
    }
    else
    {
        [self Edit_Post_Methode];
    }
    
}


-(void)Edit_Post_Methode
{
    indicaterview.hidden=NO;
    
//    "{
//    ""post_id"":"""",
//    ""user_id"":"""",
//    ""categories_id"":"""",
//    ""post_title"":"""",
//    ""post_price"":"""",
//    ""post_address"":"""",
//    ""post_latitute"":"""",
//    ""post_longitude"":"""",
//    ""phone_number"":"""",
//    ""email"":"""",
//    ""description"":"""",
//    ""post_image"":"""",
//    ""previoues_post_image"":"""",
//    ""original_post_image"":"""",
//    ""post_image_names"":[""""],
//    ""remove_post_image_names"":[""""],
//    ""cover_image"":
//    {
//        ""type"":""0/1"",
//        ""image"":"""",
//        ""id"":""""
//    }
//}"


    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
    if (post_latitude==0.0 &&post_longitude==0.0 ) {
        [dict setObject:@"" forKey:@"post_latitute"];
        [dict setObject:@"" forKey:@"post_longitude"];
    }
    else
    {
        [dict setObject:[NSNumber numberWithDouble:post_latitude] forKey:@"post_latitute"];
        [dict setObject:[NSNumber numberWithDouble:post_longitude] forKey:@"post_longitude"];
    }
    
    [dict setObject:post_id forKey:@"post_id"];
    [dict setObject:_editPost_titleTF.text forKey:@"post_title"];
    [dict setObject:_editPost_priceTF.text forKey:@"post_price"];
    [dict setObject:_editPost_addressTF.text forKey:@"post_address"];
    [dict setObject:_editPost_phoneNumberTF.text forKey:@"phone_number"];
    [dict setObject:categere_id forKey:@"categories_id"];
    
    [dict setObject:_editPost_emailTF.text forKey:@"email"];
    [dict setObject:_editPost_descriptionTextView.text forKey:@"description"];
   
    
    [dict setObject:deleted_ids forKey:@"remove_post_image_names"];
    if (img_array.count>0) {
        NSMutableDictionary *cover_img_dict=[[NSMutableDictionary alloc]init];
        [cover_img_dict setObject:[[img_array objectAtIndex:0]valueForKey:@"post_image_names"] forKey:@"image"];
        [cover_img_dict setObject:[[img_array objectAtIndex:0]valueForKey:@"post_image_id"] forKey:@"id"];
        [cover_img_dict setObject:@"0" forKey:@"type"];
        [dict setObject:cover_img_dict forKey:@"cover_image"];
        [dict setObject:[[img_array objectAtIndex:0]valueForKey:@"post_image_names"] forKey:@"previoues_post_image"];
    }
   
    
    else
    {
        if (base_64_image_array.count>0) {
        NSMutableDictionary *cover_img_dict=[[NSMutableDictionary alloc]init];
        base64_del_image=[base_64_image_array objectAtIndex:0];
        [cover_img_dict setObject:[base_64_image_array objectAtIndex:0] forKey:@"image"];
        [cover_img_dict setObject:@"" forKey:@"id"];
        [cover_img_dict setObject:@"1" forKey:@"type"];
        [dict setObject:cover_img_dict forKey:@"cover_image"];
        [base_64_image_array removeObjectAtIndex:0];
        }
        else
        {
        [dict setObject:@"" forKey:@"post_image"];
            NSMutableDictionary *cover_img_dict=[[NSMutableDictionary alloc]init];
            [cover_img_dict setObject:@"" forKey:@"image"];
            [cover_img_dict setObject:@"0" forKey:@"id"];
            [cover_img_dict setObject:@"0" forKey:@"type"];
            [dict setObject:cover_img_dict forKey:@"cover_image"];
        }
    }
    if (base_64_image_array.count>0) {
        [dict setObject:base_64_image_array forKey:@"post_image_names"];
        
    }
    else
    {
        NSMutableArray *sample=[[NSMutableArray alloc]init];
        [dict setObject:sample forKey:@"post_image_names"];
    }
     NSLog(@"Edit Post dict %@",dict);
    [SHARED_API UpdateMyPostRequestWithParams:dict withSuccess:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           NSLog(@"Edit Post Response %@",response);
                           if([[response objectForKey:STATUS] isEqualToString:SUCCESS])
                           {
                               appDelegate.edit_posted=@"YES";
                                [SHARED_HELPER showAlert:@"Your post is updated successfully"];
                               int duration = 1;
                               dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                   [self.navigationController popViewControllerAnimated:YES];
                               });

                           }
                           else if ([[response objectForKey:STATUS] isEqualToString:FAIL])
                           {
                               [SHARED_HELPER showAlert:Forgotfail];
                           }
                           indicaterview.hidden=YES;
                       });
        
    } onfailure:^(NSError *theError) {
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           indicaterview.hidden=YES;
                           [SHARED_HELPER showAlert:ServiceFail];
                           NSLog(@"error %@",theError);
                       });
    }];
}


#pragma mark - Picker View Data source
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component{
    return [_categeries_Names_ids count];
}

#pragma mark- Picker View Delegate

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:
(NSInteger)row inComponent:(NSInteger)component{
    
  categere_id=[[_categeries_Names_ids objectAtIndex:row] valueForKey:@"id"];
  _editPost_typeTF.text=[[_categeries_Names_ids objectAtIndex:row] valueForKey:@"category_name"];
    
    
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:
(NSInteger)row forComponent:(NSInteger)component{
    return [[_categeries_Names_ids objectAtIndex:row] valueForKey:@"category_name"];
}
#pragma - mark TextField Delegate Methods
-(void)textstartedting
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -150., self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];
}
-(void)textendedting
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +150., self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [self textstartedting];
    _editPost_descriptionLabel.hidden=YES;
}
-(void)textViewDidEndEditing:(UITextView *)textView
{
    [self textendedting];
    if (_editPost_descriptionTextView.text.length == 0) {
        _editPost_descriptionLabel.hidden=NO;
    }
    else
    {
        _editPost_descriptionLabel.hidden=YES;
    }
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if (textField== _editPost_titleTF)
    {
        [_editPost_typeTF becomeFirstResponder];
        return NO;
    }
    else if (textField ==_editPost_typeTF)
    {
        [_editPost_priceTF becomeFirstResponder];
    }
    else if (textField== _editPost_priceTF)
    {
        [_editPost_emailTF becomeFirstResponder];
        return NO;
    }
    else if (textField== _editPost_emailTF)
    {
        [textField becomeFirstResponder];
//        return NO;
    }
    else if (textField== _editPost_addressTF)
    {
        [_editPost_phoneNumberTF becomeFirstResponder];
        return NO;
    }
    else if (textField== _editPost_phoneNumberTF)
    {
        [_editPost_descriptionTextView becomeFirstResponder];
        return NO;
    }
    
    
    [textField resignFirstResponder];
    return YES;
}
-(BOOL)NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == _editPost_phoneNumberTF)
    {
        NSCharacterSet *numSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789-"];
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSInteger charCount = [newString length];
        
        
        //new
        NSString *stringWithoutSpaces = [newString
                                         stringByReplacingOccurrencesOfString:@"-" withString:@""];
        NSInteger newcharCount = [stringWithoutSpaces length];
        if (newcharCount>10) {
            stringWithoutSpaces = [stringWithoutSpaces substringToIndex:[stringWithoutSpaces length]-1];
            NSMutableString *mu = [NSMutableString stringWithString:stringWithoutSpaces];
            [mu insertString:@"-" atIndex:3];
            [mu insertString:@"-" atIndex:7];
            textField.text = mu;
            return NO;
            
        }
        else if (newcharCount==10)
        {
            NSMutableString *mu = [NSMutableString stringWithString:stringWithoutSpaces];
            [mu insertString:@"-" atIndex:3];
            [mu insertString:@"-" atIndex:7];
            textField.text = mu;
            return NO;
        }
        
        if (charCount == 3 || charCount == 7) {
            if ([string isEqualToString:@""]){
                return YES;
            }else{
                newString = [newString stringByAppendingString:@"-"];
            }
        }
        
        if (charCount == 4 || charCount == 8) {
            if (![string isEqualToString:@"-"]){
                newString = [newString substringToIndex:[newString length]-1];
                newString = [newString stringByAppendingString:@"-"];
            }
        }
        
        if ([newString rangeOfCharacterFromSet:[numSet invertedSet]].location != NSNotFound
            || [string rangeOfString:@"-"].location != NSNotFound
            || charCount > 12) {
            return NO;
        }
        
        textField.text = newString;
        return NO;
    }
    return YES;
}



-(void)cancelNumberPad{
    
    [self.view endEditing:YES];
    
}

-(void)doneWithNumberPad{
    [_editPost_emailTF becomeFirstResponder];
}

-(void)cancelNumberPad1{
    
    [self.view endEditing:YES];
    
}

-(void)doneWithNumberPad1{
    
    [_editPost_emailTF becomeFirstResponder];
}
-(void)cancelNumberPad2
{
    
    [self.view endEditing:YES];
}
-(void)doneWithNumberPad2{
    
    
    [self.view endEditing:YES];
}



-(void)add_click:(UIButton *)sender
{
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped do nothing.
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}
- (void) imagePickerController:(UIImagePickerController *)picker
         didFinishPickingImage:(UIImage *)image
                   editingInfo:(NSDictionary *)editingInfo
{
    //    img.image = [UIImage imageNamed:[array objectAtIndex:index]];
    UIImage *photoTaken = image;
    if (photoTaken)
    {
        
        if (index <=5)
        {
            if (index == 1) {
                x = 0;
            }
            baseView_placesImages = [[UIView alloc]initWithFrame:CGRectMake(x, 0, 150, 120)];
            baseView_placesImages.tag = 100+index;
            baseView_placesImages.backgroundColor = [UIColor clearColor];
            [self.editPost_multipleImages_scrollview addSubview:baseView_placesImages];
            
            img = [[UIImageView alloc] initWithFrame:CGRectMake(0,15,90, 90)];
            img.image=photoTaken;
            img.contentMode=UIViewContentModeScaleAspectFit;
            
            NSData *imageData1=[self compressImage:photoTaken];
            base64String = [imageData1 base64EncodedStringWithOptions:0];
            [base_64_image_array addObject:base64String];
            
            imgIndex++;
            x = x+110;
            
            [baseView_placesImages addSubview:img];
            subtract_btn = [[UIButton alloc]initWithFrame:CGRectMake(img.frame.size.width-12.5, 0, 25, 25)];
            [subtract_btn setBackgroundImage:[UIImage imageNamed:@"wrong_icon.png"] forState:UIControlStateNormal];
            [subtract_btn.titleLabel setFont:[UIFont systemFontOfSize:20]];
            [subtract_btn addTarget:self action:@selector(sub_click:) forControlEvents:UIControlEventTouchUpInside];
            subtract_btn.tag=index;
            [baseView_placesImages addSubview:subtract_btn];
            
            
            add_btn.frame = CGRectMake(baseView_placesImages.frame.origin.x+baseView_placesImages.frame.size.width-40, img.frame.origin.y, 90, 90);
            self.editPost_multipleImages_scrollview.contentSize = CGSizeMake(add_btn.frame.origin.x+add_btn.frame.size.width, 120);
            
            if (index == 5) {
                self.editPost_multipleImages_scrollview.contentSize = CGSizeMake(add_btn.frame.origin.x-5, 120);
                add_btn.hidden = YES;
            }
            index++;
        }
    }
    else
    {
        NSLog(@"Selected photo is NULL");
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}
-(NSData *)compressImage:(UIImage *)image{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    //    float maxHeight = 1130.0f;
    float maxHeight = 816.0f;
    float maxWidth = 640.0f;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.9;//50 percent compression
    NSData *imageData = UIImageJPEGRepresentation(image, compressionQuality);
    while (actualHeight > maxHeight || actualWidth > maxWidth){
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
        CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
        UIGraphicsBeginImageContext(rect.size);
        [image drawInRect:rect];
        UIImage *img_is = UIGraphicsGetImageFromCurrentImageContext();
        imageData = UIImageJPEGRepresentation(img_is, compressionQuality);
        UIGraphicsEndImageContext();
        
    }
    return imageData;
}

-(IBAction)sub_click:(UIButton *)sender
{
    UIView *remove_baseView_placesImages = (UIView *)[self.editPost_multipleImages_scrollview viewWithTag:sender.tag+100];
    [remove_baseView_placesImages removeFromSuperview];
    
    int xCoordinate = remove_baseView_placesImages.frame.origin.x;
    
    if (sender.tag < index-1)
    {
        for (int i = (int)sender.tag+1; i<= index-1 ; i++) {
            
            UIView *moveForward_placesImages = (UIView *)[self.editPost_multipleImages_scrollview viewWithTag:i+100];
            moveForward_placesImages.frame = CGRectMake(xCoordinate, 0, 150, self.editPost_multipleImages_scrollview.frame.size.height);
            UIButton *buttonTag = (UIButton *)[moveForward_placesImages viewWithTag:(int)moveForward_placesImages.tag-100];
            buttonTag.tag = buttonTag.tag-1;
            moveForward_placesImages.tag = 100+i-1;
            xCoordinate = xCoordinate+110;
            
        }
        
        index --;
        
        x = xCoordinate;
        
        add_btn.hidden = NO;
        add_btn.frame = CGRectMake(xCoordinate, img.frame.origin.y, 90, 90);
        self.editPost_multipleImages_scrollview.contentSize = CGSizeMake(add_btn.frame.origin.x + add_btn.frame.size.width, 120);
        
    }
    else
    {
        x = x-110;
        index = (int)sender.tag;
        
        add_btn.frame = CGRectMake(remove_baseView_placesImages.frame.origin.x, img.frame.origin.y, 90, 90);
        self.editPost_multipleImages_scrollview.contentSize = CGSizeMake(add_btn.frame.origin.x + add_btn.frame.size.width, 120);
        
    }
    
    
    int inde=(int)sender.tag-1;
    NSString *status=@"NO";
    if (img_array.count>0) {
        int value=(int)sender.tag;
        if (img_array.count>=value) {
            NSString *deleted_id=[[img_array objectAtIndex:inde] valueForKey:@"post_image_id"];
            [deleted_ids addObject:deleted_id];
            [img_array removeObjectAtIndex:(int)sender.tag-1];
            NSLog(@"img_array is %ld",img_array.count);
            status=@"YES";
        }
    }
    
    if (![status isEqualToString:@"YES"]) {
        NSInteger count=img_array.count;
        NSInteger value=(int)sender.tag-1;
        count=value-count;
        [base_64_image_array removeObjectAtIndex:count];
        NSLog(@"sub tag is %ld",(long)sender.tag);
        NSLog(@"base_64_image_array count is %ld",base_64_image_array.count);
    }
    
    
    if (sender.tag == 5) {
        
        add_btn.hidden = NO;
        add_btn.frame = CGRectMake(remove_baseView_placesImages.frame.origin.x, img.frame.origin.y, 90, 90);
        self.editPost_multipleImages_scrollview.contentSize = CGSizeMake(add_btn.frame.origin.x + add_btn.frame.size.width, 120);
    }
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onClick_editPost_addressBtn:(id)sender {
    GMSPlacePickerConfig *config = [[GMSPlacePickerConfig alloc] initWithViewport:nil];
    GMSPlacePickerViewController *placePicker =
    [[GMSPlacePickerViewController alloc] initWithConfig:config];
    placePicker.delegate = self;
    [self presentViewController:placePicker animated:YES completion:nil];
   
}
- (void)placePicker:(GMSPlacePickerViewController *)viewController didPickPlace:(GMSPlace *)place {
    // Dismiss the place picker, as it cannot dismiss itself.
    [viewController dismissViewControllerAnimated:YES completion:nil];
    
        NSString*loc_str_name=[NSString stringWithFormat:@"%@",place.name];
        if ([loc_str_name isEqualToString:@"(null)"] || [loc_str_name isEqualToString:@"<null>"] || [loc_str_name isEqualToString:@""] || [loc_str_name isEqualToString:@"null"] || loc_str_name == nil) {
            loc_str_name=@"";
            
        }
        NSString*loc_str_address=[NSString stringWithFormat:@"%@",place.formattedAddress];
        if ([loc_str_address isEqualToString:@"(null)"] || [loc_str_address isEqualToString:@"<null>"] || [loc_str_address isEqualToString:@""] || [loc_str_address isEqualToString:@"null"] || loc_str_address == nil) {
            loc_str_address=@"";
            
        }
        NSString*loc_name_address_str=[NSString stringWithFormat:@"%@ %@",loc_str_name,loc_str_address];
        
        if ([loc_name_address_str isEqualToString:@"(null)"] || [loc_name_address_str isEqualToString:@"<null>"] || [loc_name_address_str isEqualToString:@""] || [loc_name_address_str isEqualToString:@"null"] || loc_name_address_str == nil) {
            loc_name_address_str=@"";
            
        }
        else
        {
            _editPost_addressTF.text=loc_name_address_str;
        }
        
        
        post_latitude=place.coordinate.latitude;
        post_longitude=place.coordinate.longitude;
        
        NSLog(@"Place name %@", place.name);
        NSLog(@"Place address %@", place.formattedAddress);
        NSLog(@"Place attributions %@", place.attributions.string);
    
}

- (void)placePickerDidCancel:(GMSPlacePickerViewController *)viewController {
    // Dismiss the place picker, as it cannot dismiss itself.
    [viewController dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"No place selected");
}



- (IBAction)onClick_editPost_rideGoingBtn:(id)sender {
    
    self.editPostView_scrollLabel.hidden=YES;
    self.editPost_rideGoingBtn.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }
}
@end
