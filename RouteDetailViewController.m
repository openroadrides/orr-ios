//
//  RouteDetailViewController.m
//  openroadrides
//
//  Created by apple on 28/06/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "RouteDetailViewController.h"
#import "Constants.h"
#import "AppHelperClass.h"
#import "APIHelper.h"
#import <GPXParser.h>
#import "MapViewController.h"

@interface RouteDetailViewController ()
{
    double height;
    UIImageView* imgView;
   
   
    CGPoint tempContentOffset;
    UIView *swipe_view;
    NSMutableArray *comments_ary;
    NSMutableArray *places_ary;
    NSMutableArray *add_places,*way_array;
    UIBarButtonItem *item0;
    UIImageView *images;
    NSString *gpx_str;
    int j;
    int l;
    NSMutableArray *places_pics_arry;
    NSArray *places;
    NSString *elementname;
    NSString *start_lat_long,*end_lat_long;
    GMSMutablePath *path2;
    GMSPolyline *polyline;
    double x,z;
    GMSMapView *mapView;
}
@property(strong,nonatomic)NSMutableArray *tracks_array;
@property (strong, nonatomic) MKPlacemark *destination;
@property (strong,nonatomic) MKPlacemark *source;
@property (strong,nonatomic) MKPolygon *polygon;
@end

@implementation RouteDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.title = @"ROUTE";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    [self.view layoutIfNeeded];
    
    self.routeDetail_scrollLabel.hidden=YES;
    self.routeDetail_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.routeDetail_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.routeDetail_scrollLabel.textColor = [UIColor blackColor];
    self.routeDetail_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.routeDetail_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.routeDetail_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.routeDetail_scrollLabel.scrollSpeed = 60; // pixels per second
    self.routeDetail_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.routeDetail_scrollLabel.fadeLength = 0.f;
    self.routeDetail_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.routeDetail_scrollLabel observeApplicationNotifications];
    
    self.routeDetail_rideGoingBtn.hidden=YES;
    if (appDelegate.ridedashboard_home) {
        
        self.routeDetail_scrollLabel.hidden=NO;
        self.routeDetail_rideGoingBtn.hidden=NO;
    }
    
    int mapZoom=0;
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:37.0902
                                                            longitude:95.7129
                                                                 zoom:mapZoom];
    mapView = [GMSMapView mapWithFrame:CGRectMake(self.view_map.frame.origin.x, self.view_map.frame.origin.y, self.view_map.frame.size.width, self.view_map.frame.size.height) camera:camera];
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:CLLocationCoordinate2DMake(48.857229, -49.898547) coordinate:CLLocationCoordinate2DMake(23.955779,-127.945417)];
    [mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds]];
    mapView.delegate=self;
   [self.view_map layoutIfNeeded];
    
    [self.view_map addSubview:mapView];
    path2 = [GMSMutablePath path];
    
    //    UIBarButtonItem *search_btn=[[UIBarButtonItem alloc]initWithImage:
    //                                 [[UIImage imageNamed:@"SEARCH"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
    //                                                                style:UIBarButtonItemStylePlain target:self action:@selector(edit_routeclicked)];
    //    self.navigationItem.rightBarButtonItem=search_btn;
    
    //    UIBarButtonItem *menuButton=[[UIBarButtonItem alloc]initWithImage:
    //                                 [[UIImage imageNamed:@"sidemenuicon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
    //                                                                style:UIBarButtonItemStylePlain target:self action:@selector(Menu_Action)];
    //
    //    self.navigationItem.leftBarButtonItem = menuButton;
    
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [leftBtn addTarget:self action:@selector(Menu_Action:) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.frame = CGRectMake(0, 0, 25, 25);
    //    [leftBtn setBackgroundImage:[UIImage imageNamed:@"Finalback-arrow.png"] forState:UIControlStateNormal];
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"back_Image"] forState:UIControlStateNormal];
    UIBarButtonItem *leftBackButton=[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    item0=leftBackButton;
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:item0, nil];
    
    _map.hidden = YES;
    
    _main_scrl_view.showsVerticalScrollIndicator = NO;
    [self.view layoutIfNeeded];
    
   
    [self get_Routedetail_list];
    
    
    
    comments_ary = [[NSMutableArray alloc]init];
    places_ary = [[NSMutableArray alloc]init];
    
  
    places_pics_arry=[[NSMutableArray alloc]init];
    [self.bottom_View layoutIfNeeded];
    //    _comments_view.backgroundColor = [UIColor clearColor];
    
    _comments_scrlview.delegate = self;
    
    [self.view layoutIfNeeded];
    _comments_scrlview = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, _comments_view.frame.size.width, _comments_view.frame.size.height)];
    
    
    
    
    
    _comments_scrlview.pagingEnabled = YES;
    [_comments_view addSubview:_comments_scrlview];
    
    
    _comments_scrlview.userInteractionEnabled = NO;
    [_comments_scrlview layoutIfNeeded];
    
    
    
    
    // Do any additional setup after loading the view, typically from a nib.
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    
    
    
    
    
    // Dispose of any resources that can be recreated.
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
    
    if(scrollView.tag == 0 || scrollView.tag == -2 || scrollView.tag == -5)
    {
        
    }
    else
    {
        UIView *pages_view=(UIView *)[self.bottom_View viewWithTag:(scrollView.tag-add_places.count)];
        UIPageControl *page=(UIPageControl *)[pages_view viewWithTag:scrollView.tag+add_places.count];
        page.currentPage=scrollView.contentOffset.x/self.bottom_View.frame.size.width;
        
        
    }
    
}

-(void)get_Routedetail_list{
    
    
    
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
    
    
    
//    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//    activeIndicatore.center = self.view.center;
//    activeIndicatore.color = APP_YELLOW_COLOR;
//    activeIndicatore.hidesWhenStopped = TRUE;
//    [activeIndicatore startAnimating];
//    [self.view addSubview:activeIndicatore];
    
    indicaterview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    activeIndicatore.backgroundColor=[UIColor clearColor];
    activeIndicatore = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.frame = CGRectMake(0.0, 0.0, 80, 80);
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    [indicaterview addSubview:activeIndicatore];
    [self.view addSubview:indicaterview];
    [indicaterview bringSubviewToFront:self.view];
    [activeIndicatore startAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    indicaterview.hidden=NO;
    
    [SHARED_API routedetailRequestsWithParams:_route_id withSuccess:^(NSDictionary *response) {
        
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           
                           
                           NSLog(@"responce is %@",response);
                           
                           if ([[response valueForKey:STATUS] isEqualToString:SUCCESS]) {
                               
                               NSDictionary *route_data = [response valueForKey:@"route_data"];
                               _route_title_lble.text = [route_data valueForKey:@"route_title"];
                               _route_desc_lbl.text = [route_data valueForKey:@"route_description"];
                               
                               
                               gpx_str = [NSString stringWithFormat:@"%@",[route_data valueForKey:@"route_gpx_file"]];
                               
                               [_rating_view initRateBar];
                               NSString *rate_value=[NSString stringWithFormat:@"%@",[route_data valueForKey:@"route_ratings_count"]];
                               
                               RBRatings rate;
                               if ([rate_value isEqualToString:@"<null>"]) {
                                   rate =0;
                               }
                               else{
                                   rate =[[route_data valueForKey:@"route_ratings_count"] floatValue]*2;
                               }
                               [_rating_view setRatings:rate];
                               [_rating_view setUserInteractionEnabled:NO];
                               
                               
                               comments_ary = [route_data valueForKey:@"comments"];
                               
                             [self gpx_parsing];
                               
                           }
                           
                           else
                           {
                               [SHARED_HELPER showAlert:NotificatiosEmpty];
//                               if ([activeIndicatore isAnimating]) {
//                                   [activeIndicatore stopAnimating];
                                indicaterview.hidden=YES;
                                   [self goback];
                                   _content_view.hidden = YES;
//                                   [activeIndicatore removeFromSuperview];
//                               }
                               
                               
                           }
                           
                       });
        
    } onfailure:^(NSError *theError) {
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           
                           [SHARED_HELPER showAlert:ServiceFail];
                           
//                           if ([activeIndicatore isAnimating]) {
//                               [activeIndicatore stopAnimating];
                                indicaterview.hidden=YES;
                               _content_view.hidden = YES;
//                               [activeIndicatore removeFromSuperview];
//                           }
                           
                           
                           
                       });
    }];
}


#pragma mark - GPX PARSING........
-(void)gpx_parsing
{
     mapView.userInteractionEnabled=NO;
//     [activeIndicatore startAnimating];
    indicaterview.hidden=NO;
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
    
    NSURL *url1 = [NSURL URLWithString:gpx_str];
    NSData*fileData2 = [NSData dataWithContentsOfURL:url1];
    
    add_places =[[NSMutableArray alloc]init];
    _tracks_array=[[NSMutableArray alloc]init];
    way_array=[[NSMutableArray alloc]init];
    NSError *error = nil;
    NSDictionary *dict = [XMLReader dictionaryForXMLData:fileData2
                                                 options:XMLReaderOptionsProcessNamespaces
                                                   error:&error];
    dispatch_async(dispatch_get_main_queue(), ^{
        
   /* NSString *add_place_str = [[[[dict valueForKey:@"gpx"] valueForKey:@"extensions"] valueForKey:@"addplace"] valueForKey:@"place"];
        // Add A Places
        
        if ([add_place_str isKindOfClass:[NSArray class]])
        {
            NSArray *places_is = [[[[dict valueForKey:@"gpx"] valueForKey:@"extensions"] valueForKey:@"addplace"] valueForKey:@"place"];
            [add_places addObjectsFromArray:places_is];
            
        }
        else if ([add_place_str isKindOfClass:[NSDictionary class]]){
            
            NSDictionary *data = [[[[dict valueForKey:@"gpx"] valueForKey:@"extensions"] valueForKey:@"addplace"] valueForKey:@"place"];
            
            [add_places addObject:data];
        }
        
       
        
        for (int i=0; i<add_places.count; i++)
        {
            
            NSString *lat_str = [NSString stringWithFormat:@"%@",[[[add_places objectAtIndex:i] valueForKey:@"latitude"] valueForKey:@"text"]];
            NSString *long_str = [NSString stringWithFormat:@"%@",[[[add_places objectAtIndex:i] valueForKey:@"longitude"] valueForKey:@"text"]];
            NSString *add_pace_title = [NSString stringWithFormat:@"%@",[[[add_places objectAtIndex:i] valueForKey:@"title"] valueForKey:@"text"]];
            
            GMSMarker *add_place_marker = [[GMSMarker alloc] init];
            add_place_marker.position = CLLocationCoordinate2DMake([lat_str floatValue],[long_str floatValue]);
            add_place_marker.appearAnimation = kGMSMarkerAnimationPop;
            add_place_marker.title=add_pace_title;
            add_place_marker.icon = [UIImage imageNamed:@"addplace_marker_bg.png"];
            add_place_marker.map = mapView;
        }*/
        
        if (add_places.count>0) {
            [self places_details];
        }
        else
        {
            [self places_details];
            self.places_label.hidden=YES;
            self.placesDetails_image.hidden=YES;
        }
        
        
        
        
    NSString *traks_str=[[[[dict valueForKey:@"gpx"] valueForKey:@"trk"] valueForKey:@"trkseg"] valueForKey:@"trkpt"];
    NSString*way_str=[[dict valueForKey:@"gpx"] valueForKey:@"wpt"];
    
    
    if ([way_str isKindOfClass:[NSArray class]])
    {
        NSArray *places_is = [[dict valueForKey:@"gpx"] valueForKey:@"wpt"];
        [way_array addObjectsFromArray:places_is];
        
    }
    else if ([way_str isKindOfClass:[NSDictionary class]]){
        
        NSDictionary *data = [[dict valueForKey:@"gpx"] valueForKey:@"wpt"];
        
        [way_array addObject:data];
    }
    //    Waypoints
    for (int q=0; q<way_array.count; q++) {
        NSString *waypoint_lat_str = [NSString stringWithFormat:@"%@",[[way_array objectAtIndex:q] valueForKey:@"lat"]];
        NSString *watpoint_lon_str = [NSString stringWithFormat:@"%@",[[way_array objectAtIndex:q] valueForKey:@"lon"]];
        NSString *watpoint_title_str = [NSString stringWithFormat:@"%@",[[[way_array objectAtIndex:q] valueForKey:@"name"] valueForKey:@"text"]];
        
        GMSMarker *start_marker = [[GMSMarker alloc] init];
        start_marker.position = CLLocationCoordinate2DMake([waypoint_lat_str doubleValue],[watpoint_lon_str doubleValue]);
        start_marker.appearAnimation = kGMSMarkerAnimationPop;
        start_marker.title=watpoint_title_str;
        start_marker.icon = [UIImage imageNamed:@"default_marker"];
        start_marker.map = mapView;
     
    }
  
      
    //    Tracks
   // NSArray *places_is;
    if ([traks_str isKindOfClass:[NSArray class]])
    {
        [_tracks_array addObjectsFromArray:[[[[dict valueForKey:@"gpx"] valueForKey:@"trk"] valueForKey:@"trkseg"] valueForKey:@"trkpt"]];
    }
    else if ([traks_str isKindOfClass:[NSDictionary class]]){
        
        NSDictionary *data = [[[[dict valueForKey:@"gpx"] valueForKey:@"trk"] valueForKey:@"trkseg"] valueForKey:@"trkpt"];
        
        [_tracks_array addObject:data];
    }
    NSUInteger count=_tracks_array.count;
    for (int p=0; p<count; p++) {
        NSString *lat_str = [NSString stringWithFormat:@"%@",[[_tracks_array objectAtIndex:0] valueForKey:@"lat"]];
        NSString *lon_str = [NSString stringWithFormat:@"%@",[[_tracks_array objectAtIndex:0] valueForKey:@"lon"]];
        
        x = [lat_str doubleValue];
        z = [lon_str doubleValue];
        [_tracks_array removeObjectAtIndex:0];
        
        
            if (p==0) {
                GMSMarker *start_marker = [[GMSMarker alloc] init];
                start_marker.position = CLLocationCoordinate2DMake(x,z);
                start_marker.appearAnimation = kGMSMarkerAnimationPop;
                start_marker.title=Start_Point_Title;
                start_marker.icon = [UIImage imageNamed:@"red_marker.png"];
                start_marker.map = mapView;
            }
            else if (p==count-1)
            {
                GMSMarker *end_marker = [[GMSMarker alloc] init];
                end_marker.position = CLLocationCoordinate2DMake(x,z);
                end_marker.appearAnimation = kGMSMarkerAnimationPop;
                end_marker.title=End_Point_title;
                end_marker.icon = [UIImage imageNamed:@"finish_marker.png"];
                end_marker.map = mapView;
            }
        
       [path2 addCoordinate:CLLocationCoordinate2DMake(x, z)];
    }
        
    encoded_path=[path2 encodedPath];
    polyline = [GMSPolyline polylineWithPath:path2];
    polyline.strokeColor = [UIColor yellowColor];
    polyline.strokeWidth = 5.0f;
    polyline.geodesic = YES;
    polyline.map = mapView;
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithPath:path2];
    [mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds]];
    [path2 removeAllCoordinates];
     mapView.userInteractionEnabled=YES;
    
   // [way_array removeAllObjects];
    [_tracks_array removeAllObjects];
//  [activeIndicatore stopAnimating];
    indicaterview.hidden=YES;
    });
    });
}

-(void)goback{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:YES];
//        [activeIndicatore stopAnimating];
        indicaterview.hidden=YES;
        
    });
}

-(void)edit_routeclicked{
    
    
}
-(void)Menu_Action:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
    
}


- (IBAction)right_click:(UIButton *)sender
{
    
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options: UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         
                         if((_comments_scrlview.contentOffset.x + _comments_scrlview.frame.size.width) < _comments_scrlview.contentSize.width )
                         {
                             tempContentOffset = _comments_scrlview.contentOffset;
                             tempContentOffset.x += _comments_view.frame.size.width;
                             [_comments_scrlview setContentOffset: tempContentOffset animated:NO];
                             
                             
                             
                         }
                         
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Done!");
                     }];
    
    
    
    
    
}

- (IBAction)left_click:(UIButton *)sender

{
    
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options: UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         
                         if((_comments_scrlview.contentOffset.x + _comments_scrlview.frame.size.width) > _comments_scrlview.frame.size.width)
                         {
                             
                             tempContentOffset.x -= _comments_view.frame.size.width;
                             [_comments_scrlview setContentOffset: tempContentOffset animated:NO];
                             
                         }
                         
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Done!");
                     }];
    
    
    
    
}
-(void)places_details
{
    for (l=0; l<add_places.count; l++)
    {
        
        NSDictionary *imgs_dict = [[add_places objectAtIndex:l]  valueForKey:@"image"];
        url_str_for_images = [[imgs_dict valueForKey:@"url"] valueForKey:@"text"];
        if (url_str_for_images==nil||[url_str_for_images isEqual:[NSNull null]]) {
            check_str = @"String";
            NSMutableArray *mut=[[NSMutableArray alloc]init];
            [mut addObject:check_str];
            [mut addObject:@""];
            [ places_pics_arry  addObject:mut];
        }
        else if([url_str_for_images isKindOfClass:[NSArray class]])
        {
            check_str = @"Array";
            NSMutableArray *mut=[[NSMutableArray alloc]init];
            [mut addObject:check_str];
            [mut addObject:url_str_for_images];
            [ places_pics_arry  addObject:mut];
        }
        else
        {
            check_str = @"String";
            NSMutableArray *mut=[[NSMutableArray alloc]init];
            [mut addObject:check_str];
            [mut addObject:url_str_for_images];
            [ places_pics_arry  addObject:mut];
        }
    }
    
    [self comments_places];
    
}
//old Final Code
//-(void)comments_places{
//
//
////    places_images and desc
//    for (int i=0; i<places_ary.count; i++)
//    {
//        scroles_back_view=[[UIView alloc]initWithFrame:CGRectMake(0, i*230, self.bottom_View.frame.size.width, 150)];
//        desc_view = [[UIView alloc]initWithFrame:CGRectMake(0, scroles_back_view.frame.size.height, scroles_back_view.frame.size.width, 70)];
//
//        place_name = [[UILabel alloc]initWithFrame:CGRectMake(10, 5, desc_view.frame.size.width-20, 20)];
//        place_name.text = [[places_ary objectAtIndex:i] valueForKey:@"title"];
//
//        [place_name setFont:[UIFont fontWithName:@"SohoStd" size:15]];
//        place_name.textColor = [UIColor blackColor];
//        place_desc = [[UILabel alloc]initWithFrame:CGRectMake(10, place_name.frame.size.height+10, desc_view.frame.size.width-20, 40)];
//        NSString *desr_str=[[places_ary objectAtIndex:i] valueForKey:@"descr"];
//
//        if ([desr_str isKindOfClass:[NSString class]]) {
//
//        NSString *newString = [[desr_str componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
//        NSString * newJsonString = [newString stringByReplacingOccurrencesOfString:@"\t" withString:@""];
//          place_desc.text = newJsonString;
//        }
//        else{
//            place_desc.text = @"";
//        }
//
//        [place_desc setFont:[UIFont fontWithName:@"SohoStd" size:11]];
//        place_desc.textColor = [UIColor blackColor];
//        place_name.numberOfLines = 2;
//        place_desc.numberOfLines = 4;
//        place_name.backgroundColor = [UIColor clearColor];
//        place_desc.backgroundColor = [UIColor clearColor];
//        desc_view.backgroundColor =  [UIColor colorWithRed:56/255.0 green:215/255.0 blue:255/255.0 alpha:1.0];
//        scroles_back_view.tag=i+1;
//
//
//        scrole_views=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, scroles_back_view.frame.size.width, scroles_back_view.frame.size.height)];
//        scrole_views.tag=i+places_ary.count+1;
//        scrole_views.delegate=self;
//        scrole_views.pagingEnabled=YES;
//        scrole_views.showsHorizontalScrollIndicator = NO;
//
//        CGFloat width = 0.0;
//        places=[places_pics_arry objectAtIndex:i];
//        NSString *araay_or_str=[NSString stringWithFormat:@"%@",[places objectAtIndex:0]];
//        NSUInteger count_paging=0;
//        if ([araay_or_str isEqualToString:@"Array"]) {
//            places=[places objectAtIndex:1];
//            count_paging=places.count;
//            for ( j = 0; j < [places count]; j++)
//            {
//                images=[[UIImageView alloc]initWithFrame:CGRectMake(j*scrole_views.frame.size.width, 0, scrole_views.frame.size.width, scrole_views.frame.size.height)];
//                NSString *url_string = [NSString stringWithFormat:@"%@",[places objectAtIndex:j]];
//                NSURL*url=[NSURL URLWithString:url_string];
//                [images sd_setImageWithURL:url
//                          placeholderImage:[UIImage imageNamed:@"background"]
//                                   options:SDWebImageRefreshCached];
//
//                width=images.frame.size.width+images.frame.origin.x;
//                [scrole_views addSubview:images];
//            }
//        }
//        else if([araay_or_str isEqualToString:@"String"])
//        {
//            url_str_for_images=[NSString stringWithFormat:@"%@",[places objectAtIndex:1]];
//            images=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, scrole_views.frame.size.width, scrole_views.frame.size.height)];
//            NSString *url_string = [NSString stringWithFormat:@"%@",url_str_for_images];
//            NSURL*url=[NSURL URLWithString:url_string];
//            [images sd_setImageWithURL:url
//                      placeholderImage:[UIImage imageNamed:@"background"]
//                               options:SDWebImageRefreshCached];
//
//            width=images.frame.size.width+images.frame.origin.x;
//            [scrole_views addSubview:images];
//        }
//        scrole_views.contentSize=CGSizeMake(width, scrole_views.frame.size.height);
//        pages_=[[UIPageControl alloc]initWithFrame:CGRectMake(scrole_views.frame.size.width/2-50, 120, 100, 20)];
//        pages_.numberOfPages=count_paging;
//        pages_.pageIndicatorTintColor=[UIColor yellowColor];
//        pages_.currentPageIndicatorTintColor=[UIColor blackColor];
//        pages_.currentPage=0;
//        pages_.tag=i+2*places_ary.count+1;
//
////        for (int i = 0; i < pages_.numberOfPages; i++) {
////            UIView* dot = [pages_.subviews objectAtIndex:i];
////            if (i == pages_.currentPage) {
////                dot.backgroundColor = [UIColor whiteColor];
////                dot.layer.cornerRadius = dot.frame.size.height / 2;
////            } else {
////                dot.backgroundColor = [UIColor clearColor];
////                dot.layer.cornerRadius = dot.frame.size.height / 2;
////                dot.layer.borderColor = [UIColor whiteColor].CGColor;
////                dot.layer.borderWidth = 1;
////            }
////        }
//
//        [scroles_back_view addSubview:scrole_views];
//        [scroles_back_view addSubview:pages_];
//        [scroles_back_view addSubview:desc_view];
//        [desc_view addSubview:place_name];
//        [desc_view addSubview:place_desc];
//        [self.bottom_View addSubview:scroles_back_view];
//
//
//    }
//    [self.bottom_View layoutIfNeeded];
//
//    if (_route_desc_lbl.text.length ==0)
//    {
//
//      self.content_View_Height_Constrint.constant=(places_ary.count*230)+377;
//
//    }
//    else
//    {
//      [self getLabelHeight:_route_desc_lbl];
//      self.content_View_Height_Constrint.constant=(places_ary.count*230)+_bottom_View.frame.origin.y+height-8;
//    }
//
//
//
//
////    comments images and desc
//    for (int k = 0; k < comments_ary.count; k++)
//    {
//        CGRect frame;
//        frame.origin.x = _comments_scrlview.frame.size.width * k;
//        frame.origin.y = 0;
//        frame.size = _comments_scrlview.frame.size;
//
//        swipe_view=[[UIView alloc]init];
//        swipe_view.frame = frame;
//        UIImageView *profile_pic = [[UIImageView alloc]initWithFrame:CGRectMake(5, swipe_view.frame.size.height/2-25, 50, 50)];
//        profile_pic.image = [UIImage imageNamed:@"2.jpeg"];
//        NSString *url_string = [NSString stringWithFormat:@"%@",[[comments_ary objectAtIndex:k] valueForKey:@"profile_image"]];
//        NSURL*url=[NSURL URLWithString:url_string];
//        [profile_pic sd_setImageWithURL:url
//                  placeholderImage:[UIImage imageNamed:@"unfriend"]
//                           options:SDWebImageRefreshCached];
//
//        UILabel *comment_title = [[UILabel alloc]initWithFrame:CGRectMake(profile_pic.frame.size.width+10, 5, _comments_scrlview.frame.size.width-65, 20)];
//        comment_title.tag = k+1;
//        comment_title.text = [[comments_ary objectAtIndex:k] valueForKey:@"name"];
//                comment_title.textColor = APP_YELLOW_COLOR;
//        [comment_title setFont:[UIFont fontWithName:@"SohoStd" size:15]];
//        UILabel *comment_desc = [[UILabel alloc]initWithFrame:CGRectMake(comment_title.frame.origin.x, comment_title.frame.size.height+10, comment_title.frame.size.width, 35)];
//        [comment_desc setFont:[UIFont fontWithName:@"SohoStd" size:11]];
//        comment_desc.text = [[comments_ary objectAtIndex:k] valueForKey:@"comment"];
//        comment_desc.textColor = [UIColor whiteColor];
//        comment_title.numberOfLines = 2;
//        comment_desc.numberOfLines = 4;
//        comment_title.backgroundColor = [UIColor clearColor];
//        comment_desc.backgroundColor = [UIColor clearColor];
//        swipe_view.backgroundColor = [UIColor clearColor];
//        swipe_view.tag = k+1;
//
//        [_comments_scrlview addSubview:swipe_view];
//        [swipe_view addSubview:profile_pic];
//        [swipe_view addSubview:comment_title];
//        [swipe_view addSubview:comment_desc];
//        _comments_scrlview.contentSize = CGSizeMake(swipe_view.frame.size.width+swipe_view.frame.origin.x,_comments_scrlview.frame.size.height);
//
//    }
//
//    [self gpx_parsing];
//
//}
///////////////////////////////////////////////////////////////
-(void)comments_places{
    
    //    places_images and desc
    for (int i=0; i<add_places.count; i++)
    {
        scroles_back_view=[[UIView alloc]initWithFrame:CGRectMake(0, i*230, self.bottom_View.frame.size.width, 150)];
        desc_view = [[UIView alloc]initWithFrame:CGRectMake(0, scroles_back_view.frame.size.height, scroles_back_view.frame.size.width, 70)];
        
        place_name = [[UILabel alloc]initWithFrame:CGRectMake(10, 5, desc_view.frame.size.width-20, 20)];
        place_name.text = [[[add_places objectAtIndex:i] valueForKey:@"title"] valueForKey:@"text"];
        
        [place_name setFont:[UIFont fontWithName:@"SohoStd" size:15]];
        place_name.textColor = [UIColor blackColor];
        place_desc = [[UILabel alloc]initWithFrame:CGRectMake(10, place_name.frame.size.height+10, desc_view.frame.size.width-20, 40)];
        NSString *desr_str=[[[add_places objectAtIndex:i] valueForKey:@"descr"] valueForKey:@"text"];
        
        if ([desr_str isKindOfClass:[NSString class]]) {
            
            
            place_desc.text = desr_str;
        }
        else{
            place_desc.text = @"";
        }
        
        [place_desc setFont:[UIFont fontWithName:@"SohoStd" size:11]];
        place_desc.textColor = [UIColor blackColor];
        place_name.numberOfLines = 2;
        place_desc.numberOfLines = 4;
        place_name.backgroundColor = [UIColor clearColor];
        place_desc.backgroundColor = [UIColor clearColor];
        desc_view.backgroundColor =  [UIColor colorWithRed:56/255.0 green:215/255.0 blue:255/255.0 alpha:1.0];
        scroles_back_view.tag=i+1;
        
        
        scrole_views=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, scroles_back_view.frame.size.width, scroles_back_view.frame.size.height)];
        scrole_views.tag=i+add_places.count+1;
        scrole_views.delegate=self;
        scrole_views.pagingEnabled=YES;
        scrole_views.showsHorizontalScrollIndicator = NO;
        
        CGFloat width = 0.0;
        places=[places_pics_arry objectAtIndex:i];
        NSString *araay_or_str=[NSString stringWithFormat:@"%@",[places objectAtIndex:0]];
        NSUInteger count_paging=0;
        if ([araay_or_str isEqualToString:@"Array"]) {
            places=[places objectAtIndex:1];
            count_paging=places.count;
            for ( j = 0; j < [places count]; j++)
            {
                images=[[UIImageView alloc]initWithFrame:CGRectMake(j*scrole_views.frame.size.width, 0, scrole_views.frame.size.width, scrole_views.frame.size.height)];
               images.contentMode=UIViewContentModeScaleAspectFill;
                images.clipsToBounds=YES;
                NSString *url_string = [NSString stringWithFormat:@"%@",[places objectAtIndex:j]];
                NSURL*url=[NSURL URLWithString:url_string];
                [images sd_setImageWithURL:url
                          placeholderImage:[UIImage imageNamed:@"add_places_placeholder"]
                                   options:SDWebImageRefreshCached];
                
                width=images.frame.size.width+images.frame.origin.x;
                [scrole_views addSubview:images];
            }
        }
        else if([araay_or_str isEqualToString:@"String"])
        {
            url_str_for_images=[NSString stringWithFormat:@"%@",[places objectAtIndex:1]];
            images=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, scrole_views.frame.size.width, scrole_views.frame.size.height)];
            images.contentMode=UIViewContentModeScaleAspectFill;
            images.clipsToBounds=YES;
            //images.image=[UIImage imageNamed:@"add_places_placeholder"];
            NSString *url_string = [NSString stringWithFormat:@"%@",url_str_for_images];
            NSURL*url=[NSURL URLWithString:url_string];
            [images sd_setImageWithURL:url
                      placeholderImage:[UIImage imageNamed:@"add_places_placeholder"]
                               options:SDWebImageRefreshCached];
            
            width=images.frame.size.width+images.frame.origin.x;
            [scrole_views addSubview:images];
        }
        scrole_views.contentSize=CGSizeMake(width, scrole_views.frame.size.height);
        pages_=[[UIPageControl alloc]initWithFrame:CGRectMake(scrole_views.frame.size.width/2-50, 120, 100, 20)];
        pages_.numberOfPages=count_paging;
        pages_.pageIndicatorTintColor=[UIColor yellowColor];
        pages_.currentPageIndicatorTintColor=[UIColor blackColor];
        pages_.currentPage=0;
        pages_.tag=i+2*add_places.count+1;
        
        //        for (int i = 0; i < pages_.numberOfPages; i++) {
        //            UIView* dot = [pages_.subviews objectAtIndex:i];
        //            if (i == pages_.currentPage) {
        //                dot.backgroundColor = [UIColor whiteColor];
        //                dot.layer.cornerRadius = dot.frame.size.height / 2;
        //            } else {
        //                dot.backgroundColor = [UIColor clearColor];
        //                dot.layer.cornerRadius = dot.frame.size.height / 2;
        //                dot.layer.borderColor = [UIColor whiteColor].CGColor;
        //                dot.layer.borderWidth = 1;
        //            }
        //        }
        
        [scroles_back_view addSubview:scrole_views];
        [scroles_back_view addSubview:pages_];
        [scroles_back_view addSubview:desc_view];
        [desc_view addSubview:place_name];
        [desc_view addSubview:place_desc];
        [self.bottom_View addSubview:scroles_back_view];
        
        
    }
    [self.bottom_View layoutIfNeeded];
    
    if (_route_desc_lbl.text.length ==0)
    {
         if (add_places.count>0) {
                self.content_View_Height_Constrint.constant=(add_places.count*230)+390;
         }
         else{
                self.content_View_Height_Constrint.constant=(add_places.count*230)+400;
         }
     
        
    }
    else
    {
        
        [self getLabelHeight:_route_desc_lbl];
        if (add_places.count>0) {
            self.content_View_Height_Constrint.constant=(add_places.count*230)+_bottom_View.frame.origin.y+height-8;
        }
        else
        {
            self.content_View_Height_Constrint.constant=_bottom_View.frame.origin.y+height+10;
        }
        
        
        
    }
    
    
    
    
    //    comments images and desc
    for (int k = 0; k < comments_ary.count; k++)
    {
        CGRect frame;
        frame.origin.x = _comments_scrlview.frame.size.width * k;
        frame.origin.y = 0;
        frame.size = _comments_scrlview.frame.size;
        
        swipe_view=[[UIView alloc]init];
        swipe_view.frame = frame;
        UIImageView *profile_pic = [[UIImageView alloc]initWithFrame:CGRectMake(5, swipe_view.frame.size.height/2-25, 50, 50)];
        profile_pic.image = [UIImage imageNamed:@"2.jpeg"];
        NSString *url_string = [NSString stringWithFormat:@"%@",[[comments_ary objectAtIndex:k] valueForKey:@"profile_image"]];
        NSURL*url=[NSURL URLWithString:url_string];
        [profile_pic sd_setImageWithURL:url
                       placeholderImage:[UIImage imageNamed:@"user_Placeholder"]
                                options:SDWebImageRefreshCached];
        
        UILabel *comment_title = [[UILabel alloc]initWithFrame:CGRectMake(profile_pic.frame.size.width+10, 5, _comments_scrlview.frame.size.width-65, 20)];
        comment_title.tag = k+1;
        comment_title.text = [[comments_ary objectAtIndex:k] valueForKey:@"name"];
        comment_title.textColor = APP_YELLOW_COLOR;
        [comment_title setFont:[UIFont fontWithName:@"SohoStd" size:15]];
        UILabel *comment_desc = [[UILabel alloc]initWithFrame:CGRectMake(comment_title.frame.origin.x, comment_title.frame.size.height+10, comment_title.frame.size.width, 35)];
        [comment_desc setFont:[UIFont fontWithName:@"SohoStd" size:11]];
        comment_desc.text = [[comments_ary objectAtIndex:k] valueForKey:@"comment"];
        comment_desc.textColor = [UIColor whiteColor];
        comment_title.numberOfLines = 2;
        comment_desc.numberOfLines = 4;
        comment_title.backgroundColor = [UIColor clearColor];
        comment_desc.backgroundColor = [UIColor clearColor];
        swipe_view.backgroundColor = [UIColor clearColor];
        swipe_view.tag = k+1;
        
        [_comments_scrlview addSubview:swipe_view];
        [swipe_view addSubview:profile_pic];
        [swipe_view addSubview:comment_title];
        [swipe_view addSubview:comment_desc];
        _comments_scrlview.contentSize = CGSizeMake(swipe_view.frame.size.width+swipe_view.frame.origin.x,_comments_scrlview.frame.size.height);
        
    }
    if (comments_ary.count>1) {
        _comments_Right_Btn.hidden=NO;
        _coments_Left_BTN.hidden=NO;
        
    }
    else{
        _comments_Right_Btn.hidden=YES;
        _coments_Left_BTN.hidden=YES;
        if (comments_ary.count==0) {
            
            _comment_image.hidden=YES;
            _comment_label.hidden=YES;
            _comment_line_view.hidden=YES;
        }
        
    }
}

- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    height = size.height;
    return size.height;
}


#pragma mark - GMSMAPVIEW DELEGATE METHODS.......
- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    MapViewController *map_cntrl = [self.storyboard instantiateViewControllerWithIdentifier:@"MapViewController"];
    map_cntrl.gpx_file = gpx_str;
    map_cntrl.encoded_str=encoded_path;
    map_cntrl.way_points_array=way_array;
    [self.navigationController pushViewController:map_cntrl animated:YES];
}

//- (MKOverlayRenderer*)mapView:(MKMapView*)mapView rendererForOverlay:(id <MKOverlay>)overlay {
//    
//    MKPolylineRenderer* lineView = [[MKPolylineRenderer alloc] initWithPolyline:overlay];
//    lineView.strokeColor = [UIColor yellowColor];
//    lineView.lineWidth = 7;
//    return lineView;
//}


- (IBAction)onClick_routeDetail_rideGoingBtn:(id)sender {
    self.routeDetail_scrollLabel.hidden=YES;
    self.routeDetail_rideGoingBtn.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }

}
@end
