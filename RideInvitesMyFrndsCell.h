//
//  RideInvitesMyFrndsCell.h
//  openroadrides
//
//  Created by apple on 03/07/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RideInvitesMyFrndsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *rideinvites_myFrndsCell_bgView;

@property (weak, nonatomic) IBOutlet UIButton *rideInvites_mrFrndsCell_checkbox_btn;
@property (weak, nonatomic) IBOutlet UIImageView *rideInvites_myFrndCell_ProfileImage;
@property (weak, nonatomic) IBOutlet UILabel *rideInvites_mrFrndsCell_userName_Label;
@property (weak, nonatomic) IBOutlet UIImageView *rideInvites_myFrndsCell_location_imageView;
@property (weak, nonatomic) IBOutlet UILabel *rideInvites_myFrndsCell_address_label;
@property (weak, nonatomic) IBOutlet UIView *rideInvites_myFrnds_statusView;
@property (weak, nonatomic) IBOutlet UILabel *rideInvites_myFrnds_statusLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rideInvites_myFrnds_viewTop_userName_constraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rideInvites_myFrnds_viewTop_address_constraint;
@end
