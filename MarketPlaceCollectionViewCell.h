//
//  MarketPlaceCollectionViewCell.h
//  openroadrides
//
//  Created by apple on 26/09/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MarketPlaceCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *marketPlaceCell_bg_view;
@property (weak, nonatomic) IBOutlet UIImageView *marketPlace_item_imageView;
@property (weak, nonatomic) IBOutlet UILabel *marketPlace_item_title_label;
@property (weak, nonatomic) IBOutlet UILabel *marketPlace_posted_label;
@property (weak, nonatomic) IBOutlet UILabel *marketPlace_price_label;

@end
