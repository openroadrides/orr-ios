//
//  AddAPlace.h
//  openroadrides
//
//  Created by apple on 25/05/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BIZPopupViewController.h"
#import "AssetsLibrary/AssetsLibrary.h"
#import "AddPlace+CoreDataClass.h"
#import "Model+CoreDataModel.h"
@interface AddAPlace : UIViewController<UIScrollViewDelegate,BIZPopupViewControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    UIView *baseView_placesImages;
    NSString*clickedSave;
    NSInteger addplace_count_in_ride;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrl_view;
@property (weak, nonatomic) IBOutlet UIView *base_view;
@property (weak, nonatomic) IBOutlet UIButton *close_btn;
- (IBAction)onClick_close_btn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *save_btn;
- (IBAction)onClick_save_btn:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *Description_tf;
@property (weak, nonatomic) IBOutlet UITextField *Title_tf;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *alert_view_top;
@property (strong, nonatomic) IBOutlet UILabel *alert_label;
@property (strong, nonatomic) NSString *add_A_place_lat,*add_A_place_long,*add_A_Place_timeString,*local_rid;
@property NSEntityDescription * dataEntity;

@end
