//
//  Promotion_detailpage_ViewController.h
//  openroadrides
//
//  Created by SrkIosEbiz on 27/09/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APIHelper.h"
#import "Constants.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AppHelperClass.h"
#import "AppDelegate.h"
#import "RideDashboard.h"
#import "CBAutoScrollLabel.h"

@interface Promotion_detailpage_ViewController : UIViewController
{
    UIActivityIndicatorView *activeIndicatore;
     NSURL *webURL;
}
@property (strong,nonatomic)NSString *promotion_id;
@property (weak, nonatomic) IBOutlet UIImageView *promotion_image;
@property (weak, nonatomic) IBOutlet UILabel *promotion_title;
@property (weak, nonatomic) IBOutlet UILabel *offer_model_name;
@property (weak, nonatomic) IBOutlet UILabel *promotion_end_date;
@property (weak, nonatomic) IBOutlet UILabel *promotion_des;
@property (weak, nonatomic) IBOutlet UILabel *website_label;
- (IBAction)website_action:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *promotion_Notification_description;
@property (weak, nonatomic) IBOutlet UIView *notification_content_view;
@property (weak, nonatomic) IBOutlet UIView *url_view;
@property (weak, nonatomic) IBOutlet UIView *image_view;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *notification_content_view_height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *notification_view_bottom_constraint;
@property (weak, nonatomic) IBOutlet UIImageView *notification_image;
@property (weak, nonatomic) IBOutlet UIButton *promotion_detailPage_rideGoingBtn;
- (IBAction)onClick_promotion_detailPage_rideGoingBtn:(id)sender;
@property BOOL promotion_detailPage_onGoing;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *promotion_detailPage_scrollLabel;



@end
