//
//  EditGroupViewController.m
//  openroadrides
//
//  Created by apple on 13/12/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "EditGroupViewController.h"

@interface EditGroupViewController ()

@end

@implementation EditGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"EDIT GROUP";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    
    
    self.editGroup_scrollLabel.hidden=YES;
    self.editGroup_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.editGroup_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.editGroup_scrollLabel.textColor = [UIColor blackColor];
    self.editGroup_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.editGroup_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.editGroup_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.editGroup_scrollLabel.scrollSpeed = 60; // pixels per second
    self.editGroup_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.editGroup_scrollLabel.fadeLength = 0.f;
    self.editGroup_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.editGroup_scrollLabel observeApplicationNotifications];
    
    self.editGroup_rideGoingBtn.hidden=YES;
    if (appDelegate.ridedashboard_home) {
        self.editGroup_scrollLabel.hidden=NO;
        self.editGroup_rideGoingBtn.hidden=NO;
    }

    
    UIButton *saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [saveBtn addTarget:self action:@selector(save_clicked) forControlEvents:UIControlEventTouchUpInside];
    saveBtn.frame = CGRectMake(0, 0, 30, 30);
    [saveBtn setBackgroundImage:[UIImage imageNamed:@"SAVE"] forState:UIControlStateNormal];
    UIBarButtonItem * create_Ride= [[UIBarButtonItem alloc] initWithCustomView:saveBtn];
    self.navigationItem.rightBarButtonItems=[NSArray arrayWithObjects:create_Ride, nil];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [leftBtn addTarget:self action:@selector(left_closed_btn:) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.frame = CGRectMake(0, 0, 25, 25);
    //    [leftBtn setBackgroundImage:[UIImage imageNamed:@"Finalback-arrow.png"] forState:UIControlStateNormal];
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"close_icon"] forState:UIControlStateNormal];
    UIBarButtonItem *leftBackButton=[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    item0=leftBackButton;
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:item0, nil];
    
    [self.editGroup_groupNameTF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.editGroup_groupDescriptionTF setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    
    
    self.editGroup_selectedMembersLabel.hidden=YES;
    
    existing_Group_Members_Array=[[NSMutableArray alloc]init];
    if (_Group_Details.count>0) {
        NSLog(@"Group_details %@",_Group_Details);
        group_iD= [NSString stringWithFormat:@"%@",[_Group_Details valueForKey:@"group_id"]];
        existing_Image = [NSString stringWithFormat:@"%@",[_Group_Details valueForKey:@"group_image"]];
        if (![existing_Image isEqualToString:@""]) {
            NSURL*url=[NSURL URLWithString:existing_Image];
            [_logo_img sd_setImageWithURL:url
                         placeholderImage:[UIImage imageNamed:@"add_places_placeholder"]
                                  options:SDWebImageRefreshCached];
            
            self.logo_img.layer.cornerRadius = self.logo_img.frame.size.width / 2;
            self.logo_img.clipsToBounds = YES;
        }
       _editGroup_groupNameTF.text = [_Group_Details valueForKey:@"group_name"];
        _editGroup_groupDescriptionTF.text = [_Group_Details valueForKey:@"group_description"];
        
      NSArray * groupmembers_ary = [_Group_Details valueForKey:@"group_members"];
        _deleteGroupView.backgroundColor=[UIColor clearColor];
        if (groupmembers_ary.count>0) {
         
            [existing_Group_Members_Array addObjectsFromArray:groupmembers_ary];
            
            NSUInteger indexvalue = [[existing_Group_Members_Array valueForKey:@"user_type"] indexOfObject:@"owner"];
            [existing_Group_Members_Array removeObjectAtIndex:indexvalue];
           self.members_View_height.constant=existing_Group_Members_Array.count*72;
        }
        
    }
    
    indicaterview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    activeIndicatore.backgroundColor=[UIColor clearColor];
    activeIndicatore = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    [indicaterview addSubview:activeIndicatore];
    [self.view addSubview:indicaterview];
    [indicaterview bringSubviewToFront:self.view];
    [activeIndicatore startAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    indicaterview.hidden=YES;
    
    
}
-(void)viewWillAppear:(BOOL)animated{
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)left_closed_btn:(UIButton *)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)save_clicked{
    
    [self.view endEditing:YES];
    _editGroup_groupNameTF.text=[_editGroup_groupNameTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
    
    if (_editGroup_groupNameTF.text.length == 0) {
        [SHARED_HELPER showAlert:groupnamefail];
        return;
    }
    else if (_editGroup_groupDescriptionTF.text.length >257)
    {
        [SHARED_HELPER showAlert:@"Max Character limit is 256"];
        return;
    }
    
    [self edit_Group_dict];
    
}
-(void)edit_Group_dict
{
//    "{
//    ""group_id"":"""",
//    ""user_id"":"""",
//    ""group_name"":"""",
//    ""group_description"":"""",
//    ""group_members"":[],
//    ""delete_group_members"":[],
//    ""group_image"":
//    {
//        ""type"":""0/1"",
//        ""image"":""""
//    }
//}"
    indicaterview.hidden=NO;
    NSString *type=@"0";
    if([existing_Image isEqualToString:@""])
    {
        if (!base64String) {
            base64String=@"";
        }
        else
        {
             existing_Image=base64String;
            type=@"1";
        }
        
    }
    NSMutableArray *new_Members_Arry=[[NSMutableArray alloc]init];
    NSMutableArray *delet_Members_Arry=[[NSMutableArray alloc]init];
    
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:[_editGroup_groupNameTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"group_name"];
    [dict setObject:[_editGroup_groupDescriptionTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"group_description"];
    
    NSMutableDictionary *image_dict=[[NSMutableDictionary alloc]init];
    [image_dict setObject:type forKey:@"type"];
    [image_dict setObject:existing_Image forKey:@"image"];
    
    [dict setObject:image_dict forKey:@"group_image"];
    
    [dict setObject:new_Members_Arry forKey:@"group_members"];
    [dict setObject:delet_Members_Arry forKey:@"delete_group_members"];
    [dict setObject:group_iD forKey:@"group_id"];
    [dict setObject:[NSString stringWithFormat:@"%@",[Defaults valueForKey:User_ID]] forKey:@"user_id"];
    NSLog(@"%@",dict);
    
    [SHARED_API UpdateGroupRequestsWithParams:dict withSuccess:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSLog(@"responce is %@",response);
            
            if ([[response valueForKey:STATUS] isEqualToString:SUCCESS]) {
                 indicaterview.hidden=YES;
                
                [SHARED_HELPER showAlert:groupupdated];
                int duration = 1; // duration in seconds
                [appDelegate.createGroup_frndInvitations removeAllObjects];
                self.editGroup_selectedMembersLabel.hidden=YES;
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    appDelegate.edit_Group=@"YES";
                     [self.navigationController popViewControllerAnimated:YES];
                    
                });
                
            }
            else
            {
                 indicaterview.hidden=YES;
                 [SHARED_HELPER showAlert:ServiceFail];
            }
            
        });
        
    } onfailure:^(NSError *theError) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            indicaterview.hidden=YES;
            [SHARED_HELPER showAlert:ServerConnection];
        });
        
    }];
}

- (IBAction)onClick_editGroup_profileImageBtn:(id)sender {
    [self.view endEditing:YES];
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped do nothing.
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *cameraPicker = [[UIImagePickerController alloc] init];
        cameraPicker.delegate = self;
        
        [cameraPicker setAllowsEditing:NO];
        cameraPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:cameraPicker animated:YES completion:NULL];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UIImagePickerController *galleryPicker = [[UIImagePickerController alloc] init];
        galleryPicker.delegate = self;
        galleryPicker.allowsEditing = NO;
        
        galleryPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:galleryPicker animated:YES completion:NULL];
    }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    existing_Image=@"";
    self.logo_img.layer.cornerRadius = self.logo_img.frame.size.width / 2;
    self.logo_img.clipsToBounds = YES;
    
    
    cameraImage = info[UIImagePickerControllerOriginalImage];
    _logo_img.image=cameraImage;
    [picker dismissViewControllerAnimated:YES completion:NULL];
    CGFloat compression = 0.9f;
    CGFloat maxCompression = 0.1f;
    int maxFileSize = 250*1024;
    profileImgData = UIImageJPEGRepresentation(cameraImage,compression);
    while ([profileImgData length] > maxFileSize && compression > maxCompression)
    {
        compression -= 0.1;
        profileImgData = UIImageJPEGRepresentation(cameraImage,compression);
    }
    base64String = [profileImgData base64EncodedStringWithOptions:0];
    
}
- (IBAction)onClick_editGroup_inviteMembersBtn:(id)sender {
   
}
#pragma mark -Textfileds_deligate_methods............
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == _editGroup_groupDescriptionTF) {
        NSString *newString = [_editGroup_groupDescriptionTF.text stringByReplacingCharactersInRange:range withString:string];
        return !([newString length] >= 256);
        return NO;
    }
    return YES;
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -100., self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +100., self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    if (textField == _editGroup_groupNameTF)
    {
        [_editGroup_groupDescriptionTF becomeFirstResponder];
        return NO;
    }
    
    [textField resignFirstResponder];
    return YES;
}


#pragma mark - Tableview methods......
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 72;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return existing_Group_Members_Array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *simpleTableIdentifier = @"cell";
    DeleteMemberCell *cell = (DeleteMemberCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    self.deleteMember_tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"cell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    if (indexPath.row%2==0) {
        
        cell.deleteMemberBGView.backgroundColor = [UIColor colorWithRed:41/255.0 green:41/255.0 blue:41/255.0 alpha:1.0];
    }
    else{
        
        cell.deleteMemberBGView.backgroundColor = [UIColor colorWithRed:26/255.0 green:26/255.0 blue:26/255.0 alpha:1.0];
        
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSString *str1 = [NSString stringWithFormat:@"%@", [[existing_Group_Members_Array objectAtIndex:indexPath.row] objectForKey:@"name"]];
    
    if ([str1 isEqualToString:@"<null>"] || [str1 isEqualToString:@""] || [str1 isEqualToString:@"null"] || str1 == nil) {
        
        cell.deleteMemberNameLabel.text = [[existing_Group_Members_Array objectAtIndex:indexPath.row] valueForKey:@""];
        
    }
    else
    {
        cell.deleteMemberNameLabel.text = [[existing_Group_Members_Array objectAtIndex:indexPath.row] valueForKey:@"name"];
    }
    
    NSString *str = [NSString stringWithFormat:@"%@", [[existing_Group_Members_Array objectAtIndex:indexPath.row] objectForKey:@"profile_image"]];
    NSURL*url=[NSURL URLWithString:str];
    [cell.deleteMemberImageView sd_setImageWithURL:url
                            placeholderImage:[UIImage imageNamed:@"groups-icon"]
                                     options:SDWebImageRefreshCached];
    
    
    NSString *user_status=[[existing_Group_Members_Array objectAtIndex:indexPath.row] valueForKey:@"user_type"];
    if ([user_status isEqualToString:@"<null>"] || [user_status isEqualToString:@""] || [user_status isEqualToString:@"null"] || user_status == nil) {
        
        cell.deleteMemberuserLabel.text =@"";
        
    }
    else{
       
        if ([user_status isEqualToString:@"member"]) {
            
             cell.deleteMemberBtn.hidden=NO;
            NSString*accept_reject_status_string=[NSString stringWithFormat:@"%@",[[existing_Group_Members_Array objectAtIndex:indexPath.row] valueForKey:@"accept_or_reject_status"]];
            if ([accept_reject_status_string isEqualToString:@"accept"]) {
                cell.deleteMemberuserLabel.text =@"Member";
            }
            else  if ([accept_reject_status_string isEqualToString:@""])
            {
                cell.deleteMemberuserLabel.text =@"Pending";
            }
            
        }
        else if ([user_status isEqualToString:@"owner"])
        {
            cell.deleteMemberuserLabel.text =@"Owner";
             cell.deleteMemberBtn.hidden=YES;
        }
      
    }
    
    return cell;
}

- (IBAction)onClick_deleteMember:(UIButton *)sender {
    
    delete_Member_index=sender.tag;
    //service
    
        
        
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@""
                                      message:@"Are you sure you want to remove this user?"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"YES"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [self Laeve_Group_Service];
                                 
                             }];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"NO"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
        
        [alert addAction:ok];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)Laeve_Group_Service
{
    
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
    indicaterview.hidden=NO;
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:group_iD forKey:@"group_id"];
    [dict setObject:[NSString stringWithFormat:@"%@",[[existing_Group_Members_Array objectAtIndex:delete_Member_index] valueForKey:@"user_id"]] forKey:@"user_id"];
    NSLog(@"%@",dict);
    [SHARED_API leaveGroupRequestsWithParams:dict withSuccess:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSLog(@"Leave Group responce is %@",response);
            
            if ([[response valueForKey:STATUS] isEqualToString:SUCCESS]) {
               appDelegate.edit_Group=@"YES";
                [existing_Group_Members_Array removeObjectAtIndex:delete_Member_index];
                _members_View_height.constant=existing_Group_Members_Array.count*72;
                [_deleteMember_tableView reloadData];
            }
            indicaterview.hidden=YES;
        });
        
    } onfailure:^(NSError *theError) {
        dispatch_async(dispatch_get_main_queue(), ^{
             indicaterview.hidden=YES;
            [SHARED_HELPER showAlert:@"Unable to remove from group"];
        });
    }];
}


- (IBAction)onClick_editGroup_rideGoingBtn:(id)sender {
    
    self.editGroup_scrollLabel.hidden=YES;
    self.editGroup_rideGoingBtn.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }

}
@end
