//
//  EventDetailViewController.m
//  openroadrides
//
//  Created by apple on 07/06/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "EventDetailViewController.h"
#import "Constants.h"
#import "APIHelper.h"
#import "AppHelperClass.h"
#import "MPCoachMarks.h"

@interface EventDetailViewController ()

@end

@implementation EventDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    appDelegate.update_event=@"NO";
    appDelegate.delete_event=@"NO";
   appDelegate.create_event_or_edit_event=@"NO";
    self.title = @"EVENT";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    
     _main_view.hidden = YES;
    
    
#pragma mark - Bar buttons on nav bar.....
    
    
    
    
    
    //    UIBarButtonItem *back_btn=[[UIBarButtonItem alloc]initWithImage:
    //                                 [[UIImage imageNamed:@"sidemenuicon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
    //                                                                style:UIBarButtonItemStylePlain target:self action:@selector(back_Action)];
//    UIBarButtonItem *back_btn=[[UIBarButtonItem alloc]initWithTitle:@"<" style:UIBarButtonItemStylePlain target:self action:@selector(back_Action)];
//    [back_btn setTitleTextAttributes:@{
//                                       NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:26.0],
//                                       NSForegroundColorAttributeName: [UIColor blackColor]
//                                       } forState:UIControlStateNormal];
//    
//    self.navigationItem.leftBarButtonItem = back_btn;
    
    self.eventDetail_scrollLabel.hidden=YES;
    self.eventDetail_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.eventDetail_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.eventDetail_scrollLabel.textColor = [UIColor blackColor];
    self.eventDetail_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.eventDetail_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.eventDetail_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.eventDetail_scrollLabel.scrollSpeed = 60; // pixels per second
    self.eventDetail_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.eventDetail_scrollLabel.fadeLength = 0.f;
    self.eventDetail_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.eventDetail_scrollLabel observeApplicationNotifications];
    
    self.eventDetail_rideGoingBtn.hidden=YES;
    if (appDelegate.ridedashboard_home) {
        self.eventDetail_scrollLabel.hidden=NO;
        self.eventDetail_rideGoingBtn.hidden=NO;
    }
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [leftBtn addTarget:self action:@selector(event_back_Action:) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.frame = CGRectMake(0, 0, 25, 25);
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"back_Image"] forState:UIControlStateNormal];
    UIBarButtonItem *leftBackButton=[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    item0=leftBackButton;
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:item0, nil];
    
    
    
    

    
    self.view.backgroundColor = [UIColor colorWithRed:26/255.0 green:26/255.0 blue:26/255.0 alpha:1.0];
    self.main_view.backgroundColor = [UIColor colorWithRed:26/255.0 green:26/255.0 blue:26/255.0 alpha:1.0];
    
    
    [self get_Eventdetail_list];
    
    //Display Annotation
    // Show coach marks
//    BOOL coachMarksShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"MPCoachMarksShown_Event"];
//    if (coachMarksShown == NO) {
//        // Don't show again
//        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MPCoachMarksShown_Event"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//        
//        [self showAnnotation];
//    }
    
    // Do any additional setup after loading the view.
}

#pragma mark - Annotations
-(void)showAnnotation
{
    [self.view layoutIfNeeded];
    
    // Setup coach marks
    NSString *event_owner_id=[NSString stringWithFormat:@"%@",[event_details_dict valueForKey:@"user_id"]];
    if ([event_owner_id isEqualToString:[NSString stringWithFormat:@"%@",[Defaults valueForKey:User_ID]]]) {
        
        CGRect coachmark1 = CGRectMake( ([UIScreen mainScreen].bounds.size.width-95), 20, self.navigationController.navigationBar.frame.size.height, self.navigationController.navigationBar.frame.size.height);
        
         CGRect coachmark2 = CGRectMake( ([UIScreen mainScreen].bounds.size.width - 51), 20, self.navigationController.navigationBar.frame.size.height, self.navigationController.navigationBar.frame.size.height);
        
        
        // Setup coach marks
        NSArray *coachMarks = @[
                                
                                @{
                                    @"rect": [NSValue valueWithCGRect:coachmark1],
                                    @"caption": @"Edit your event",
                                    @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                                    @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                                    @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_RIGHT],
                                    //@"showArrow":[NSNumber numberWithBool:YES]
                                    },
                                @{
                                    @"rect": [NSValue valueWithCGRect:coachmark2],
                                    @"caption": @"Delete your event",
                                    @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                                    @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                                    @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_RIGHT],
                                    //@"showArrow":[NSNumber numberWithBool:YES]
                                    },

                                ];
        
        
        MPCoachMarks *coachMarksView = [[MPCoachMarks alloc] initWithFrame:self.navigationController.view.bounds coachMarks:coachMarks];
        //[self.navigationController.view addSubview:coachMarksView];
        [[UIApplication sharedApplication].keyWindow addSubview:coachMarksView];
        [coachMarksView start];

    }
    else
    {
        CGRect coachmark1 = CGRectMake( ([UIScreen mainScreen].bounds.size.width - 51), 20, self.navigationController.navigationBar.frame.size.height, self.navigationController.navigationBar.frame.size.height);
        
        
        // Setup coach marks
        NSArray *coachMarks = @[
                                
                                @{
                                    @"rect": [NSValue valueWithCGRect:coachmark1],
                                    @"caption": @"Share your event",
                                    @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                                    @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                                    @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_RIGHT],
                                    //@"showArrow":[NSNumber numberWithBool:YES]
                                    },
                                ];
        
        
        MPCoachMarks *coachMarksView = [[MPCoachMarks alloc] initWithFrame:self.navigationController.view.bounds coachMarks:coachMarks];
        //[self.navigationController.view addSubview:coachMarksView];
        [[UIApplication sharedApplication].keyWindow addSubview:coachMarksView];
        [coachMarksView start];
    }
    
   
}

-(void)viewWillAppear:(BOOL)animated{
    
    if ([appDelegate.update_event isEqualToString:@"YES"]) {
        appDelegate.update_event=@"NO";
       appDelegate.create_event_or_edit_event=@"YES";
        [self get_Eventdetail_list];
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)edit_clicked{
    if (imagearayy.count>1) {
         [self.multipeImagesScrollView setContentOffset:CGPointMake(0,0)];
        pageControl.currentPage=0;
    }
    
    EditEventViewController *cntlr = [self.storyboard instantiateViewControllerWithIdentifier:@"EditEventViewController"];
 cntlr.event_details=@{@"event_name":_main_title_labl.text,@"event_adress":_start_location_lbl.text,@"start_date":_start_date_lbl.text,@"start_time":_start_time_lbl.text,@"end_date":_end_date_lbl.text,@"end_time":_end_time_lbl.text,@"assosiated_ride":_associatedDescriptionTextLabel.text,@"description":_description_lbl.text,@"web_link":link_str_is,@"event_images":imagearayy,@"event_type":event_type_is,@"event_id":event_id,@"event_lat":event_lat,@"event_long":event_long};
    
    [self.navigationController pushViewController:cntlr animated:YES];
}

-(void)get_Eventdetail_list{
    
    
    
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
    
    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    activeIndicatore.hidesWhenStopped = TRUE;
    [activeIndicatore startAnimating];
    [self.view addSubview:activeIndicatore];

   [SHARED_API eventsDetailRequestsWithParams:_Event_id withSuccess:^(NSDictionary *response) {
dispatch_async(dispatch_get_main_queue(), ^
           {
               NSLog(@"responce is %@",response);
               if ([[response valueForKey:STATUS] isEqualToString:SUCCESS]) {
                   event_details_dict = [response valueForKey:@"event_details"];
                   _main_title_labl.text = [event_details_dict valueForKey:@"event_name"];
                   event_type_is=[event_details_dict valueForKey:@"event_type"];
                   event_id=[event_details_dict valueForKey:@"event_id"];
                   event_lat=[event_details_dict valueForKey:@"event_starting_location_latitude"];
                   event_long=[event_details_dict valueForKey:@"event_starting_location_longitude"];
                   
                   NSString *event_owner_id=[NSString stringWithFormat:@"%@",[event_details_dict valueForKey:@"user_id"]];
                   
                   
                self.navigationItem.rightBarButtonItems=nil;
                   if ([event_owner_id isEqualToString:[NSString stringWithFormat:@"%@",[Defaults valueForKey:User_ID]]]) {
                       UIBarButtonItem *Share_btn=[[UIBarButtonItem alloc]initWithImage:
                                                   [[UIImage imageNamed:@"event_delete"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
                                                                                  style:UIBarButtonItemStylePlain target:self action:@selector(Delete_Event)];
                      

                       
                       
                       UIButton *editBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                       [editBtn addTarget:self action:@selector(edit_clicked) forControlEvents:UIControlEventTouchUpInside];
                       editBtn.frame = CGRectMake(0, 0, 30, 30);
                       [editBtn setBackgroundImage:[UIImage imageNamed:@"event_edit"] forState:UIControlStateNormal];
                       UIBarButtonItem * edit_Ride= [[UIBarButtonItem alloc] initWithCustomView:editBtn];
                       self.navigationItem.rightBarButtonItems=[NSArray arrayWithObjects:Share_btn,edit_Ride, nil];
                       
                       BOOL coachMarksShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"MPCoachMarksShown_MyEvent"];
                       if (coachMarksShown == NO) {
                           // Don't show again
                           [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MPCoachMarksShown_MyEvent"];
                           [[NSUserDefaults standardUserDefaults] synchronize];
                           
                          [self showAnnotation];
                       }

                       
                       
                   }
                   else{
                       UIBarButtonItem *Share_btn=[[UIBarButtonItem alloc]initWithImage:
                                                   [[UIImage imageNamed:@"share"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
                                                                                  style:UIBarButtonItemStylePlain target:self action:@selector(shareclicked)];
                       self.navigationItem.rightBarButtonItem=Share_btn;
                       
                       BOOL coachMarksShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"MPCoachMarksShown_Event"];
                       if (coachMarksShown == NO) {
                           // Don't show again
                           [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MPCoachMarksShown_Event"];
                           [[NSUserDefaults standardUserDefaults] synchronize];
                           
                           [self showAnnotation];
                       }
                        

                   }

                   
                   NSString*start_loc_add_str=[NSString stringWithFormat:@"%@",[event_details_dict valueForKey:@"event_starting_location_address"]];
                   if ([start_loc_add_str isEqualToString:@"<null>"] || [start_loc_add_str isEqualToString:@""] || [start_loc_add_str isEqualToString:@"null"] || start_loc_add_str == nil)
                   {
                       start_loc_add_str=@"";
//
                       
                       
                       _start_date_noLoc_topConstraint.priority=750;
                       _start_date_topConstraint.priority=250;
                       _locationStartImage.hidden=YES;
                       _locationTitleLabel.hidden=YES;
                       _start_location_lbl.hidden=YES;
                   }
                   else
                   {
                      _start_date_noLoc_topConstraint.priority=250;
                      _start_date_topConstraint.priority=750;
                       _locationStartImage.hidden=NO;
                       _locationTitleLabel.hidden=NO;
                       _start_location_lbl.hidden=NO;
                       _start_location_lbl .text=[NSString stringWithFormat:@"%@",start_loc_add_str];
                   }
                   

                   
                   //Setting Starting Date:
                   NSString *date_str = [NSString stringWithFormat:@"%@",[event_details_dict valueForKey:@"event_start_date"]]; /// here this is your date with format yyyy-MM-dd
                   if ([date_str isEqualToString:@"1970-01-01"]||[date_str isEqualToString:@"0000-00-00"]||[date_str isEqualToString:@"<null>"]) {
                        _start_date_lbl .text =@"N/A";
                   }
                   else if([date_str isEqualToString:@"<null>"] || [date_str isEqualToString:@""] || [date_str isEqualToString:@"null"] || date_str == nil)
                   {
                       _start_date_lbl .text = @"N/A";
                   }
                   else{
                   NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
                   [dateFormatter setDateFormat:@"yyyy-MM-dd"]; //// here set format of date which is in your output date (means above str with format)
                   
                   NSDate *date = [dateFormatter dateFromString: date_str]; // here you can fetch date from string with define format
                   
                   dateFormatter = [[NSDateFormatter alloc] init];
                   [dateFormatter setDateFormat:@"MMMM dd,yyyy"];// here set format which you want...
                   NSString *convertedString = [dateFormatter stringFromDate:date]; //here convert date in NSString
                   NSLog(@"Converted String : %@",convertedString);
                   
                   _start_date_lbl .text = [NSString stringWithFormat:@"%@", convertedString];
                   }
                   
                   //Setting Ending Date:
                   NSString *date_str1 = [NSString stringWithFormat:@"%@",[event_details_dict valueForKey:@"event_end_date"]]; /// here this is your date with format yyyy-MM-dd
                   
                    if ([date_str1 isEqualToString:@"1970-01-01"]||[date_str1 isEqualToString:@"0000-00-00"]||[date_str1 isEqualToString:@"<null>"]) {
                          _end_date_lbl.text =@"N/A";
                    }
                    else if([date_str1 isEqualToString:@"<null>"] || [date_str1 isEqualToString:@""] || [date_str1 isEqualToString:@"null"] || date_str1 == nil)
                    {
                        _end_date_lbl.text =@"N/A";
                    }
                    else{
                   NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
                   [dateFormatter1 setDateFormat:@"yyyy-MM-dd"];
                   NSDate *date1 = [dateFormatter1 dateFromString: date_str1];
                   dateFormatter1 = [[NSDateFormatter alloc] init];
                   [dateFormatter1 setDateFormat:@"MMMM dd,yyyy"];
                   NSString *convertedString1 = [dateFormatter1 stringFromDate:date1]; //
                   
                    _end_date_lbl .text = [NSString stringWithFormat:@"%@", convertedString1];
                    }
                   
                   //Setting Time Format To HH:mm:(Start_Time)
                   NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                   [formatter setLocale:[NSLocale currentLocale]];
                   [formatter setDateStyle:NSDateFormatterNoStyle];
                   [formatter setTimeZone:[NSTimeZone localTimeZone]];
                   [formatter setTimeStyle:NSDateFormatterLongStyle];
                   NSString *dateString = [formatter stringFromDate:[NSDate date]];
                   NSRange amRange = [dateString rangeOfString:[formatter AMSymbol]];
                   NSRange pmRange = [dateString rangeOfString:[formatter PMSymbol]];
                   is24h = (amRange.location == NSNotFound && pmRange.location == NSNotFound);
                   NSLog(@"%@\n",(is24h ? @"YES" : @"NO"));
                   
                    NSString *start_time_str = [NSString stringWithFormat:@"%@",[event_details_dict valueForKey:@"event_start_time"]];
                   if ([start_time_str isEqualToString:@"00:00:00"]) {
                        _start_time_lbl .text = @"N/A";
                   }
                   else if([start_time_str isEqualToString:@"<null>"] || [start_time_str isEqualToString:@""] || [start_time_str isEqualToString:@"null"] || start_time_str == nil || [start_time_str isEqualToString:@"(null)"] || [start_time_str isEqualToString:@"00:00:00"])
                   {
                        _start_time_lbl .text = @"N/A";
                   }
                   else
                   {
                       
                       if (is24h==YES) {
                           NSDateFormatter *dateFormatter5 = [[NSDateFormatter alloc] init] ;
                           [dateFormatter5 setDateFormat:@"HH:mm:ss"];
                           NSDate *time_str = [dateFormatter5 dateFromString:start_time_str];
                            [dateFormatter5 setDateFormat:@"HH:mm"];
//                           [dateFormatter5 setDateFormat:@"hh:mm a"];
                           NSString *start_time = [dateFormatter5 stringFromDate:time_str];
                           NSLog(@"start_time : %@",start_time);
                           _start_time_lbl .text = start_time;
                       }
                       else if (is24h==NO)
                       {
                           NSDateFormatter *dateFormatter5 = [[NSDateFormatter alloc] init] ;
                           [dateFormatter5 setDateFormat:@"HH:mm:ss"];
                           NSDate *time_str = [dateFormatter5 dateFromString:start_time_str];
                           //                    [dateFormatter5 setDateFormat:@"HH:mm"];
                           [dateFormatter5 setDateFormat:@"hh:mm a"];
                           NSString *start_time = [dateFormatter5 stringFromDate:time_str];
                           NSLog(@"start_time : %@",start_time);
                           _start_time_lbl .text = start_time;
                       }
//                   NSDateFormatter *dateFormatter5 = [[NSDateFormatter alloc] init] ;
//                   [dateFormatter5 setDateFormat:@"HH:mm:ss"];
//                    NSDate *time_str = [dateFormatter5 dateFromString:start_time_str];
////                    [dateFormatter5 setDateFormat:@"HH:mm"];
//                       [dateFormatter5 setDateFormat:@"hh:mm a"];
//                    NSString *start_time = [dateFormatter5 stringFromDate:time_str];
//                   NSLog(@"start_time : %@",start_time);
//                   _start_time_lbl .text = start_time;
                   }
                   
                   //Setting Time Format To HH:mm:(End_Time)
                   NSString *end_time_str = [NSString stringWithFormat:@"%@",[event_details_dict valueForKey:@"event_end_time"]];
                   if ([end_time_str isEqualToString:@"00:00:00"]) {
                       _end_time_lbl.text = @"N/A";
                   }
                   else if([end_time_str isEqualToString:@"<null>"] || [end_time_str isEqualToString:@""] || [end_time_str isEqualToString:@"null"] || end_time_str == nil || [end_time_str isEqualToString:@"(null)"] || [end_time_str isEqualToString:@"00:00:00"])
                   {
                       _end_time_lbl.text = @"N/A";
                   }
                   else{
                       
                       if (is24h==YES) {
                           NSDateFormatter *dateFormatter4 = [[NSDateFormatter alloc] init] ;
                           [dateFormatter4 setDateFormat:@"HH:mm:ss"];
                           NSDate *ending_time = [dateFormatter4 dateFromString:end_time_str];
                           [dateFormatter4 setDateFormat:@"HH:mm"];
//                           [dateFormatter4 setDateFormat:@"hh:mm a"];
                           NSString *end_time = [dateFormatter4 stringFromDate:ending_time];
                           NSLog(@"end_Time : %@", end_time);
                           _end_time_lbl.text = end_time;
                       }
                       else if (is24h==NO)
                       {
                           NSDateFormatter *dateFormatter4 = [[NSDateFormatter alloc] init] ;
                           [dateFormatter4 setDateFormat:@"HH:mm:ss"];
                           NSDate *ending_time = [dateFormatter4 dateFromString:end_time_str];
                           // [dateFormatter4 setDateFormat:@"HH:mm"];
                           [dateFormatter4 setDateFormat:@"hh:mm a"];
                           NSString *end_time = [dateFormatter4 stringFromDate:ending_time];
                           NSLog(@"end_Time : %@", end_time);
                           _end_time_lbl.text = end_time;
                       }
//                   NSDateFormatter *dateFormatter4 = [[NSDateFormatter alloc] init] ;
//                   [dateFormatter4 setDateFormat:@"HH:mm:ss"];
//                   NSDate *ending_time = [dateFormatter4 dateFromString:end_time_str];
////                    [dateFormatter4 setDateFormat:@"HH:mm"];
//                       [dateFormatter4 setDateFormat:@"hh:mm a"];
//                   NSString *end_time = [dateFormatter4 stringFromDate:ending_time];
//                   NSLog(@"end_Time : %@", end_time);
//                   _end_time_lbl.text = end_time;
                   }
                   
                   _description_lbl .text = [event_details_dict valueForKey:@"event_description"];
                   
                    NSString *asscociated_str_is = [NSString stringWithFormat:@"%@",[event_details_dict valueForKey:@"associated_ride"]];

                   if ([asscociated_str_is isEqualToString:@"<null>"] || [asscociated_str_is isEqualToString:@""] || [asscociated_str_is isEqualToString:@"null"] || asscociated_str_is == nil)
                   {
                       
                       asscociated_str_is=@"";
                       
                       _desctitle_topConstraint.priority=250;
                       _desctitle_noText_topConstraint.priority=750;
                       _associatedLabel.hidden=YES;
                       _associatedDescriptionTextLabel.hidden=YES;
                       _descriptionBorderLineView.hidden=YES;
                       
                      
                      
                   }
                   else
                   {
                       _desctitle_topConstraint.priority=750;
                       _desctitle_noText_topConstraint.priority=250;
                        _descriptionBorderLineView.hidden=NO;
                       _associatedLabel.hidden=NO;
                       _associatedDescriptionTextLabel.hidden=NO;
                      
                       self.associatedDescriptionTextLabel.text=[NSString stringWithFormat:@"%@",asscociated_str_is];
                   }
                   
                   
                   link_str_is=[NSString stringWithFormat:@"%@",[event_details_dict valueForKey:@"link"]];
                   if ([link_str_is isEqualToString:@"<null>"] || [link_str_is isEqualToString:@""] || [link_str_is isEqualToString:@"null"] || link_str_is == nil)
                   {
                       
                       link_str_is=@"";
                       self.clcikHereBtn.hidden=YES;
                   }
                   else
                   {
                       eventLinkString=[NSString stringWithFormat:@"%@",link_str_is];
                       self.clcikHereBtn.hidden=NO;
                   }

                   
                   
                   _multipeImagesScrollView.pagingEnabled = YES;
                  
                 
                imagearayy= [event_details_dict valueForKey:@"images"];
                   for (UIView *subview in self.multipeImagesScrollView.subviews) {
                       [subview removeFromSuperview];
                   }
                  
                   
                   if ((imagearayy.count!=0))
                   {
                       for (int i=0; i<imagearayy.count; i++)
                       {
                           UIImageView *imagviews=[[UIImageView alloc] initWithFrame:CGRectMake(i*self.multipeImagesScrollView.frame.size.width, 0, self.multipeImagesScrollView.frame.size.width, self.multipeImagesScrollView.frame.size.height)];
                           NSString *image;
                           
                           image=[[imagearayy objectAtIndex:i] valueForKey:@"event_image_name"];
                           if (i==0) {
                               image_str=[[imagearayy objectAtIndex:i] valueForKey:@"event_image_name"];
                           }
                           [imagviews sd_setImageWithURL:[NSURL URLWithString:image] placeholderImage:[UIImage imageNamed:@"eventBg_Placeholder"]];
                           imagviews.contentMode = UIViewContentModeScaleAspectFit;
                           [self.multipeImagesScrollView addSubview:imagviews];
                           
                           UIView  * transparentview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, imagviews.frame.size.width, imagviews.frame.size.height)];
                           transparentview.backgroundColor = [UIColor blackColor];
                           transparentview.alpha = 0.1;
                           [imagviews addSubview:transparentview];
                           
                       }
                       self.multipeImagesScrollView.contentSize=CGSizeMake(imagearayy.count*self.multipeImagesScrollView.frame.size.width, self.multipeImagesScrollView.frame.size.height);
                       self.multipeImagesScrollView.showsHorizontalScrollIndicator = NO;
                       self.multipeImagesScrollView.delegate=self;
                        [pageControl removeFromSuperview];
                       if ([imagearayy count] > 1)
                       {
                           pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(10, self.multipeImagesScrollView.frame.size.height-12, self.multipeImagesScrollView.frame.size.width, 10)];
                          
                           pageControl.currentPageIndicatorTintColor = APP_YELLOW_COLOR;
                           pageControl.pageIndicatorTintColor =[UIColor whiteColor] ;
                           pageControl.numberOfPages = imagearayy.count;
                           pageControl.currentPage = 0;
                           [_content_view addSubview:pageControl];
                            self.multipeImagesScrollView.userInteractionEnabled=YES;
                       }
                    
                   }
                   else
                   {
                       image_str=@"";
                       UIImageView *imagviews=[[UIImageView alloc] initWithFrame:CGRectMake(0*self.multipeImagesScrollView.frame.size.width, 0, self.multipeImagesScrollView.frame.size.width, self.multipeImagesScrollView.frame.size.height)];
                       imagviews.image=[UIImage imageNamed:@"eventBg_Placeholder"];
                       imagviews.contentMode=UIViewContentModeScaleAspectFit;
                       [self.multipeImagesScrollView addSubview:imagviews];
                       [pageControl removeFromSuperview];
                       self.multipeImagesScrollView.userInteractionEnabled=NO;
//                       UIImageView *imagviews=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.multipeImagesScrollView.frame.size.width, self.multipeImagesScrollView.frame.size.height)];
//                       
//                
//                       
//                       [imagviews  sd_setImageWithURL:[NSURL URLWithString:[event_details_dict valueForKey:@"event_image"]] placeholderImage:[UIImage imageNamed:@"add_places_placeholder"]];
//                       
//                       imagviews.contentMode = UIViewContentModeScaleAspectFill;
//                       [self.multipeImagesScrollView addSubview:imagviews];
//                       UIView  * transparentview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, imagviews.frame.size.width, imagviews.frame.size.height)];
//                       transparentview.backgroundColor = [UIColor blackColor];
//                       transparentview.alpha = 0.1;
//                       [imagviews addSubview:transparentview];
                       
                   }

                   _main_view.hidden = NO;
                   [activeIndicatore stopAnimating];
                   
                   
               }
               else
               {
                   
                   [SHARED_HELPER showAlert:ServiceFail2];
                   [activeIndicatore stopAnimating];
                   _main_view.hidden = YES;

               }
           });

} onfailure:^(NSError *theError) {
    
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       [SHARED_HELPER showAlert:ServiceFail];
                       [activeIndicatore stopAnimating];
                       _main_view.hidden = YES;
                       self.navigationItem.rightBarButtonItem=nil;
                       
                   });
}];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)sender
{
    if (sender==_multipeImagesScrollView)
    {
        CGFloat pageWidth = _multipeImagesScrollView.frame.size.width;
        NSInteger offsetLooping = 1;
        int ppage = floor((_multipeImagesScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + offsetLooping;
        pageControl.currentPage = ppage %imagearayy.count;
        //        pageControl.currentPage = ppage %4;
        
    }
}




- (void)photoForview:(NSString *)user
{
    NSURL *url = [NSURL URLWithString:user];
    [self downloadImageWithURL:url completionBlock:^(BOOL succeeded, UIImage *image) {
        if (succeeded) {
            
            if (image==nil) {
                //cell.userImageView.image=[UIImage imageNamed:@"userLogo.png"];
            }
            else
            {
                 _event_detail_bg_img_view.image=image;
                _event_background_img_view.hidden = YES;
                
            }
            
        }
    }];
    
}
- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES,image);
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
}













/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)shareclicked{
    if ([image_str isEqualToString:@""]) {
        return;
    }
    NSString *Description = _description_lbl.text;
    NSString *Title =_main_title_labl.text ;
    NSURL *Image_url = [NSURL URLWithString:image_str];
    UIImage *aImage;
    aImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:Image_url]];
    NSArray *objectsToShare = @[aImage, Title, Description];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
    
}
-(void)Delete_Event{
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Delete Event" message:@"Are you sure you want to delete event?" preferredStyle:UIAlertControllerStyleAlert];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped do nothing.
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self event_delete];
        
    }]];
  
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}
-(void)event_delete{
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
    [activeIndicatore startAnimating];
    
    [SHARED_API get_Delete_event_page:event_id withSuccess:^(NSDictionary *response) {
        
        if ([[response valueForKey:STATUS]isEqualToString:SUCCESS]) {
            [SHARED_HELPER showAlert:event_deleted];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                appDelegate.delete_event=@"YES";
                [activeIndicatore stopAnimating];
                [self.navigationController popViewControllerAnimated:YES];
            });
        }
        else
        {
            [SHARED_HELPER showAlert:ServiceFail2];
            [activeIndicatore stopAnimating];
        }
        
    } onfailure:^(NSError *theError) {
        [SHARED_HELPER showAlert:ServiceFail];
        [activeIndicatore stopAnimating];
        
    }];
    

}
-(void)event_back_Action:(UIButton *)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (IBAction)onClick_clickHere_btn:(id)sender {
    NSString *webURLString = [NSString stringWithFormat:@"%@",link_str_is];
    EventWebView*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"EventWebView"];
    controller.websiteString=webURLString;
    [self.navigationController pushViewController:controller animated:YES];
    
    
}
- (IBAction)onClick_eventDetail_rideGoingBtn:(id)sender {
    self.eventDetail_scrollLabel.hidden=YES;
    self.eventDetail_rideGoingBtn.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }
}
@end
