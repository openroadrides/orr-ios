//
//  PrivacyPolicy.m
//  openroadrides
//
//  Created by apple on 27/07/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "PrivacyPolicy.h"

@interface PrivacyPolicy ()
{
    UIBarButtonItem *item0;

}
@end

@implementation PrivacyPolicy

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"PRIVACY POLICY";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    
    self.privacy_scrollLabel.hidden=YES;
    self.privacy_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.privacy_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.privacy_scrollLabel.textColor = [UIColor blackColor];
    self.privacy_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.privacy_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.privacy_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.privacy_scrollLabel.scrollSpeed = 60; // pixels per second
    self.privacy_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.privacy_scrollLabel.fadeLength = 0.f;
    self.privacy_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.privacy_scrollLabel observeApplicationNotifications];

    
    self.privacy_rideGoingBtn.hidden=YES;
    if (appDelegate.ridedashboard_home) {
        self.privacy_scrollLabel.hidden=NO;
        self.privacy_rideGoingBtn.hidden=NO;
    }
     self.privactPolicy_webview.backgroundColor=[UIColor blackColor];
    self.privactPolicy_webview.delegate=self;
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [leftBtn addTarget:self action:@selector(leftBtn:) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.frame = CGRectMake(0, 0, 25, 25);
    //    [leftBtn setBackgroundImage:[UIImage imageNamed:@"Finalback-arrow.png"] forState:UIControlStateNormal];
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"close_icon"] forState:UIControlStateNormal];
    UIBarButtonItem *leftBackButton=[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    item0=leftBackButton;
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:item0, nil];

    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    activeIndicatore.center = self.view.center;
    
    activeIndicatore.color = APP_YELLOW_COLOR;
    
    activeIndicatore.hidesWhenStopped = TRUE;
    
    [activeIndicatore startAnimating];
    
    [self.view addSubview:activeIndicatore];
    
    
   
    [self PrivacyPolicy];
}
-(void)leftBtn:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (BOOL)webView:(UIWebView *)wv shouldStartLoadWithRequest:(NSURLRequest *)rq
{
  
    return YES;
}

- (void)webView:(UIWebView *)wv didFailLoadWithError:(NSError *)error
{
    
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
     [activeIndicatore stopAnimating];
    
}

-(void)PrivacyPolicy
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURL *url;
        NSData *postData;
        
        NSString *postString  =[[NSString alloc] initWithFormat:@""];
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
        http://openroadridesapi.thinkwithebiz.com/users/registration
//        url=[NSURL URLWithString:[NSString stringWithFormat:@"http://54.70.46.133/admin/privacypolicy"]];
        url=[NSURL URLWithString:[NSString stringWithFormat:@"http://adminbackend.openroadrides.com/privacypolicy"]];
       
        postData = [postString dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        
        NSError *error = [[NSError alloc] init];
        NSHTTPURLResponse *response = nil;
        NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        NSLog(@"Response code: %ld", (long)[response statusCode]);
        if (urlData) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
                NSLog(@"UrlRequest=======%@",urlRequest);
                [self.privactPolicy_webview loadRequest:urlRequest];
                NSLog(@"UrlRequest111111=======%@",urlRequest);
                
               
                [self.privactPolicy_webview reload];
            });
        }
        else{
             [activeIndicatore stopAnimating];
        }
        
    });
}

- (IBAction)onClick_privacy_rideGoingBtn:(id)sender {
    
    self.privacy_scrollLabel.hidden=YES;
    self.privacy_rideGoingBtn.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }

}
@end
