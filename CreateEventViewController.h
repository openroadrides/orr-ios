//
//  CreateEventViewController.h
//  openroadrides
//
//  Created by apple on 16/06/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GooglePlacePicker/GooglePlacePicker.h>
#import "AppDelegate.h"
#import "CBAutoScrollLabel.h"
@interface CreateEventViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,GMSPlacePickerViewControllerDelegate,UITextFieldDelegate>{
    
    UIActivityIndicatorView  *activeIndicatore;
    UIDatePicker *date,*end_date,*start_time,*end_Time;
    UIImage *cameraImage;
     NSString *location_str;
    double start_latitude,start_longitude,end_latitude,end_longitude;
    NSString *base64String,*eventStartTimeString,*eventEndTimeString;
    NSData *profileImgData;
    
    NSMutableArray *img_array;
    UIButton *add_btn,*subtract_btn;;
    UIView *baseView_placesImages;
    int index,imgIndex,x;
     UIImageView *img;
    UIBarButtonItem *item0;
    
}
@property (weak, nonatomic) IBOutlet UITextField *event_name_tf;
@property (weak, nonatomic) IBOutlet UITextField *start_location_tf;
@property (weak, nonatomic) IBOutlet UITextField *end_location_tf;
- (IBAction)start_location_btn:(id)sender;
- (IBAction)end_location_btn:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *description_tf;
@property (weak, nonatomic) IBOutlet UITextField *upload_img_tf;
- (IBAction)onClick_startDate:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *upload_img_lbl;
- (IBAction)upload_img_btn:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *start_date_tf;
@property (weak, nonatomic) IBOutlet UITextField *end_date_tf;
@property (weak, nonatomic) IBOutlet UITextField *start_time_tf;
@property (weak, nonatomic) IBOutlet UITextField *end_time_tf;
- (IBAction)onClick_startTime:(id)sender;
- (IBAction)onClick_endDate:(id)sender;
- (IBAction)onClick_endTime:(id)sender;

@property (strong, nonatomic) IBOutlet UIScrollView *imges_Scrole_view;
@property (weak, nonatomic) IBOutlet UITextField *assosiated_ride_TF;
@property (weak, nonatomic) IBOutlet UITextField *website_link;
@property (weak, nonatomic) IBOutlet UIView *event_type_view;
- (IBAction)event_type_action_view:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *images_Baseview;
@property (weak, nonatomic) IBOutlet UIButton *createEvent_rideGoingBtn;
- (IBAction)onClick_createEvent_rideGoingBtn:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint_createEvent_image_baseView;

@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *createEvent_scrollLabel;


@end
