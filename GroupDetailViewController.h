//
//  GroupDetailViewController.h
//  openroadrides
//
//  Created by apple on 01/06/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditGroupViewController.h"
#import "CBAutoScrollLabel.h"
@interface GroupDetailViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    
    UIActivityIndicatorView  *activeIndicatore;
    NSMutableArray * group_members_array;
}

@property (weak, nonatomic) IBOutlet UIImageView *Groupdetail_img;
@property (weak, nonatomic) IBOutlet UILabel *title_lbl;
@property (weak, nonatomic) IBOutlet UILabel *description_labl;
@property (weak, nonatomic) IBOutlet UITableView *Groupdetail_table_view;
@property (weak, nonatomic) IBOutlet UIView *main_content_view;
@property (nonatomic, strong) NSString *group_id,*group_Owner_Id,*is_from;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraint_mainContentView;
@property (weak, nonatomic) IBOutlet UIButton *leaveGroup_btn;
- (IBAction)onClick_leaveGroup_btn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *leaveGroup_memeber_btn;
@property (weak, nonatomic) IBOutlet UIButton *AddMembers_btn;
- (IBAction)onClick_AddMembers_Btn:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *addMembers_imageView;
@property  (strong, nonatomic )NSString *image_str;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *groupDetaitsMembersViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIButton *groupDetails_rideGoingBtn;
- (IBAction)onClick_groupDetail_rideGoingBtn:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint_leaveGroupBtnVIew;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *groupDetail_scrollLabel;
@property (weak, nonatomic) IBOutlet UIView *leaveGroup_View;
@end
