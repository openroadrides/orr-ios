//
//  MyPostsCollectionViewCell.h
//  openroadrides
//
//  Created by apple on 26/09/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyPostsCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *myPostCollectionCell_bg_View;
@property (weak, nonatomic) IBOutlet UIImageView *myPosts_item_imageView;
@property (weak, nonatomic) IBOutlet UILabel *myPosts_title_label;
@property (weak, nonatomic) IBOutlet UILabel *myPosts_posted_label;
@property (weak, nonatomic) IBOutlet UILabel *myPosts_price_label;

@end
