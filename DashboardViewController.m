//
//  DashboardViewController.m
//  openroadrides
//
//  Created by apple on 08/05/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "DashboardViewController.h"
#import "RideDashboard.h"
#import "CreaterideViewController.h"
#import "MenuTableViewCell.h"
#import "NewsViewController.h"
#import "RoutesVC.h"
#import "GroupsViewController.h"
#import "Notifications.h"
#import "EventsViewController.h"
#import "MPCoachMarks.h"


@interface DashboardViewController (){
    UISwipeGestureRecognizer *swipe_right;
    UISwipeGestureRecognizer *swipe_left;
    NSArray *menu_Items_Array;
    NSString *menu_Status;
    NSMutableArray *check_strings_array,*arrayOfDashboard;
}

@end

@implementation DashboardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    menu_Status=@"LEFT";
    
    NSLog(@"dashboard changes done");
    _notification_btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 28)];
    
    [_notification_btn setBackgroundImage:[UIImage imageNamed:@"notificationicon"] forState:UIControlStateNormal];
    
    [_notification_btn addTarget:self action:@selector(notificationClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithCustomView:_notification_btn];
    
    badgeView = [[JSBadgeView alloc] initWithParentView:_notification_btn alignment:JSBadgeViewAlignmentTopRight];
    badgeView.badgeBackgroundColor=[UIColor clearColor];
    badgeView.badgeText=[NSString stringWithFormat:@""];
    self.navigationItem.rightBarButtonItems=[NSArray arrayWithObjects:button, nil];
    
    
    
    menuButton = [[UIBarButtonItem alloc]initWithImage:
                              [[UIImage imageNamed:@"sidemenuicon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
                                                                style:UIBarButtonItemStylePlain target:self action:@selector(Menu_Action:)];
    
    self.navigationItem.leftBarButtonItem = menuButton;
    [self.navigationController.navigationBar setBarTintColor:APP_YELLOW_COLOR];
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController.navigationBar setTranslucent:NO];
    self.navigationController.navigationBar.backItem.title=@"";
    [self.view layoutIfNeeded];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 120, 30)];
    label.textAlignment = NSTextAlignmentCenter;
    [label setFont:[UIFont fontWithName:@"Antonio-Bold" size:20]];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:[UIColor blackColor]];
    [label setText:@"DASHBOARD"];
    [self.navigationController.navigationBar.topItem setTitleView:label];
    
    swipe_right=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(menuright)];
    swipe_right.direction=UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipe_right];
    
    
    swipe_left=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(menuleft)];
    swipe_left.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipe_left];
    
    self.dashboard_scrollLabel.hidden=YES;
    self.dashboard_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.dashboard_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.dashboard_scrollLabel.textColor = [UIColor blackColor];
    self.dashboard_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.dashboard_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.dashboard_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.dashboard_scrollLabel.scrollSpeed = 60; // pixels per second
    self.dashboard_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.dashboard_scrollLabel.fadeLength = 0.f;
    self.dashboard_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.dashboard_scrollLabel observeApplicationNotifications];
    
    self.dashboard_rideGoingBtn.hidden=YES;
    if (appDelegate.ridedashboard_home) {
        self.dashboard_scrollLabel.hidden=NO;
         self.dashboard_rideGoingBtn.hidden=NO;
        self.bottomConstraint_dashboardScrollView.constant=35;
        _startRide_Label.text=@"Open Ride";
    }

    
    self.User_image.layer.cornerRadius=self.User_image.frame.size.width/2;
    self.User_image.clipsToBounds=YES;
    self.User_image.layer.borderColor = [UIColor whiteColor].CGColor;
    self.User_image.layer.borderWidth = 4.0;
    
    self.start_Ride_View.layer.cornerRadius=2.5;
    self.my_Rides_Btn.layer.cornerRadius=2.5;
    self.Sch_Rides_Btn.layer.cornerRadius=2.5;
    self.start_Ride_View.clipsToBounds=YES;
    self.my_Rides_Btn.clipsToBounds=YES;
    self.Sch_Rides_Btn.clipsToBounds=YES;
    
    
    
//    menu_Items_Array=@[@"Dashboard",@"Rides",@"Routes",@"Events",@"News",@"Friends",@"Groups",@"Find Riders",@"Privacy Policy",@"Terms & Conditions",@"Feedback",@"HomeDashboard",@"Logout"];
    
     menu_Items_Array=@[@"Home",@"Dashboard",@"Privacy Policy",@"Terms & Conditions",@"Logout"];
    
    
    friend_count=@"";
    
    group_count=@"";
    [_side_Menu_Table reloadData];
    NSUserDefaults*defaults=[NSUserDefaults standardUserDefaults];
    self.User_Name.text=[NSString stringWithFormat:@"%@",[defaults valueForKey:@"UserName"]];
    
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    
    appDasboard_locationManager = [[CLLocationManager alloc] init];
    appDasboard_locationManager.delegate=self;
    user_current_latitude = appDasboard_locationManager.location.coordinate.latitude;
    user_current_longtitude = appDasboard_locationManager.location.coordinate.longitude;
    if (user_current_latitude==0.0 && user_current_longtitude ==0.0) {
        
    }
    else
    {
        appDelegate.app_current_lattitude=user_current_latitude;
        appDelegate.app_current_longitude=user_current_longtitude;
    }
    check_strings_array=[[NSMutableArray alloc]init];
    [check_strings_array addObject:@"<null>"];
    [check_strings_array addObject:@"null"];
    [check_strings_array addObject:@""];
    [check_strings_array addObject:@"(null)"];
   
      arrayOfDashboard=[[NSMutableArray alloc]init];
    _events_count.text = @"0";
    _rides_count.text = @"0";
    _routes_count.text = @"0";
    _fav_rides.text = @"";
    _fav_routes.text = @"";
    _groups_count.text=@"0";
    _friends_count.text=@"0";
    
//    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//    activeIndicatore.center = self.view.center;
//    activeIndicatore.color = APP_YELLOW_COLOR;
//    activeIndicatore.hidesWhenStopped = TRUE;
//    [self.view addSubview:activeIndicatore];
    
    
    indicaterview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    activeIndicatore.backgroundColor=[UIColor clearColor];
    activeIndicatore = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.frame = CGRectMake(0.0, 0.0, 80, 80);
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    [indicaterview addSubview:activeIndicatore];
    [self.view addSubview:indicaterview];
    [indicaterview bringSubviewToFront:self.view];
    [activeIndicatore startAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    indicaterview.hidden=YES;

    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(check_Location_On_Or_Off) name:UIApplicationWillEnterForegroundNotification object:nil];
    
    
//   When ride was not inPogress(After Completed,Quite Ride,befor started the ride)
    if (![[Defaults valueForKey:@"RIDE_STATUS"]isEqualToString:@"RIDE_STARTED" ]) {
        
        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
        
        for(UIViewController *tempVC in navigationArray)
        {
            if([tempVC isKindOfClass:[RideDashboard class]])
            {
                [navigationArray removeObject:tempVC];
                break;
            }
        }
        self.navigationController.viewControllers=navigationArray;
    }
   
    //Display Annotation
    // Show coach marks
    BOOL coachMarksShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"MPCoachMarksShown_Dashboard"];
    if (coachMarksShown == NO) {
        // Don't show again
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MPCoachMarksShown_Dashboard"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        // Show coach marks
        [self showAnnotation];
    }
    
}


#pragma mark - Annotations
-(void)showAnnotation
{
    [self.view layoutIfNeeded];
    
    
    CGRect coachmark1, coachmark2, coachmark3, coachmark4, coachmark5, coachmark6;
    
    coachmark1 = CGRectMake (5,20, self.navigationController.navigationBar.frame.size.height,self.navigationController.navigationBar.frame.size.height);
    coachmark2 = CGRectMake( ([UIScreen mainScreen].bounds.size.width - 53), 20, self.navigationController.navigationBar.frame.size.height, self.navigationController.navigationBar.frame.size.height);
    coachmark3 = CGRectMake( 10, 415, ([UIScreen mainScreen].bounds.size.width), 100);
    coachmark4 = CGRectMake( 10, 555, ([UIScreen mainScreen].bounds.size.width)/2-10, 60);
    coachmark5 = CGRectMake( ([UIScreen mainScreen].bounds.size.width)/2, 555, ([UIScreen mainScreen].bounds.size.width)/2-10, 60);
    coachmark6 = CGRectMake( ([UIScreen mainScreen].bounds.size.width)/2-50, 640, 100, 80);
    NSArray *coachMarks;
    
    if([UIScreen mainScreen].bounds.size.width >320) {
        // Setup coach marks
        
        
        // Setup coach marks
        coachMarks = @[
                       @{
                           @"rect": [NSValue valueWithCGRect:coachmark1],
                           @"caption": @"Menu",
                           @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                           @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                           @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_LEFT]
                           //@"showArrow":[NSNumber numberWithBool:YES]
                           },
                       @{
                           @"rect": [NSValue valueWithCGRect:coachmark2],
                           @"caption": @"Notifications",
                           @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                           @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                           @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_RIGHT],
                           //@"showArrow":[NSNumber numberWithBool:YES]
                           },
                       @{
                           @"rect": [NSValue valueWithCGRect:coachmark3],
                           @"caption": @"Tap here to check rides, routes, events, friends and groups",
                           @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP]
                           
                           },
                       @{
                           @"rect": [NSValue valueWithCGRect:coachmark4],
                           @"caption": @"Tap here to schedule new ride",
                           @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
                           @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_LEFT],
                           //                                @"showArrow":[NSNumber numberWithBool:YES]
                           },
                       @{
                           @"rect": [NSValue valueWithCGRect:coachmark5],
                           @"caption": @"Tap here to start ride immediately",
                           @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
                           @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_RIGHT],
                           //                                @"showArrow":[NSNumber numberWithBool:YES]
                           },
                       @{
                           @"rect": [NSValue valueWithCGRect:coachmark6],
                           @"caption": @"Checkout promotions nearby",
                           @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                           @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_LEFT]
                           },
                       
                       ];

    }
    else
    {
        
        // Setup coach marks
        coachMarks = @[
                       @{
                           @"rect": [NSValue valueWithCGRect:coachmark1],
                           @"caption": @"Menu",
                           @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                           @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                           @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_LEFT]
                           //@"showArrow":[NSNumber numberWithBool:YES]
                           },
                       @{
                           @"rect": [NSValue valueWithCGRect:coachmark2],
                           @"caption": @"Notifications",
                           @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                           @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                           @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_RIGHT],
                           //@"showArrow":[NSNumber numberWithBool:YES]
                           },
                       @{
                           @"rect": [NSValue valueWithCGRect:coachmark3],
                           @"caption": @"Tap here to check rides, routes, events, friends and groups",
                           @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP]
                           
                           },
                       @{
                           @"rect": [NSValue valueWithCGRect:coachmark4],
                           @"caption": @"Tap here to schedule new ride",
                           @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
                           @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_LEFT],
                           //                                @"showArrow":[NSNumber numberWithBool:YES]
                           },
                       @{
                           @"rect": [NSValue valueWithCGRect:coachmark5],
                           @"caption": @"Tap here to start ride immediately",
                           @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
                           @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_RIGHT],
                           //                                @"showArrow":[NSNumber numberWithBool:YES]
                           },
                       ];
        
    }
    
    
    
    MPCoachMarks *coachMarksView = [[MPCoachMarks alloc] initWithFrame:self.navigationController.view.bounds coachMarks:coachMarks];
    [[UIApplication sharedApplication].keyWindow addSubview:coachMarksView];
    [coachMarksView start];

}


-(BOOL)check_Location_On_Or_Off{
    if ([CLLocationManager locationServicesEnabled]){
        NSLog(@"Location Services Enabled");
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
        {
            UIAlertController *alertview = [UIAlertController alertControllerWithTitle:@"LOCATION" message:@"Turn on your App Location" preferredStyle:UIAlertControllerStyleAlert];
            
            [alertview addAction:[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root=Privacy:Location_Services"]];
            }]];
            [self presentViewController:alertview animated:YES completion:nil];
            return NO;
        }
        else{
            
             NSLog(@"Location Services Enabled");
            if (appDelegate.ridedashboard_home) {
                
            }
            else
            {
                [self user_update_location];
            }
            
            return YES;
        }
    }
    else{
        UIAlertController *alertview = [UIAlertController alertControllerWithTitle:@"LOCATION" message:@"Turn ON Your Location" preferredStyle:UIAlertControllerStyleAlert];
        
        [alertview addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
            // Cancel button tappped.
            [self dismissViewControllerAnimated:YES completion:^{
            }];
            
        }]];
        [alertview addAction:[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
            
            // Distructive button tapped.
            //                [self dismissViewControllerAnimated:YES completion:^{
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root=Privacy:Location_Services"]];
            //                }];
        }]];
        
        [self presentViewController:alertview animated:YES completion:nil];
        
        return NO;
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [self check_Location_On_Or_Off];
    
    _side_menu_view.hidden=YES;
    [self.side_Menu_Table reloadData];
    
//    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 120, 30)];
//    label.textAlignment = NSTextAlignmentCenter;
//    [label setFont:[UIFont fontWithName:@"Antonio-Bold" size:20]];
//    [label setBackgroundColor:[UIColor clearColor]];
//    [label setTextColor:[UIColor blackColor]];
//    [label setText:@"DASHBOARD"];
//    [self.navigationController.navigationBar.topItem setTitleView:label];
    self.title=@"DASHBOARD";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    self.navigationController.navigationBar.hidden=NO;
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    
    
    [self get_dashboard_data];
    
}

//view_user_profile _api is Commnent
-(void)display_user_Profile{
    
    NSString *user_id=[Defaults valueForKey:User_ID];
    [SHARED_API DisplayUserProfile:user_id withSuccess:^(NSDictionary *response) {
        
        [self user_Profile_Data_Sucsess:response];
        
    } onfailure:^(NSError *theError) {
        
    }];
    
}
-(void)user_Profile_Data_Sucsess:(NSDictionary *)Profile_data{
    if ([[Profile_data valueForKey:STATUS] isEqualToString:SUCCESS]) {
        NSDictionary *user_data=[Profile_data valueForKey:@"data"];
        self.User_Name.text=[user_data valueForKey:@"name"];
        [Defaults setObject:self.User_Name.text forKey:@"UserName"];
        
        NSString *profile_image_str=[NSString stringWithFormat:@"%@",[user_data valueForKey:@"profile_image"]];
        
        if (![check_strings_array containsObject:profile_image_str] && ![profile_image_str isEqualToString:@"<null>"])
        {
            NSURL*url=[NSURL URLWithString:profile_image_str];
            [self.User_image sd_setImageWithURL:url
                               placeholderImage:[UIImage imageNamed:@"user_Placeholder"]
                                        options:SDWebImageRefreshCached];
        }
    }
}


-(void)viewWillDisappear:(BOOL)animated{
    [self menuleft];
}

-(void)menuleft
{
   
    menu_Status=@"LEFT";
    [UIView animateWithDuration:0.5 delay:0.3 options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.menu_Leading_Constraint.constant=-300;
                         [self.view layoutIfNeeded];
                     } completion:^(BOOL finished) {
                         self.main_Scriole_View.alpha=1;
                         
                         self.main_Scriole_View.userInteractionEnabled=YES;
                     }];
}

-(void)menuright
{
    
    menu_Status=@"RIGHT";
    [UIView animateWithDuration:0.5 delay:0.3 options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.menu_Leading_Constraint.constant=0;
                         [self.view layoutIfNeeded];
                     } completion:^(BOOL finished)
                    {
                       
                        self.main_Scriole_View.alpha=0.8;
                        self.main_Scriole_View.userInteractionEnabled=NO;
                    }];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  menu_Items_Array.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"MenuTableViewCell";
    MenuTableViewCell *cell = (MenuTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    self.side_Menu_Table.separatorStyle=UITableViewCellSeparatorStyleNone;
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MenuTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.menu_Item_Label.text=[menu_Items_Array objectAtIndex:indexPath.row];
    cell.menu_Item_Label.textColor=[UIColor colorWithRed:255/255.0 green:222/255.0 blue:0/255.0 alpha:1.0];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:255/255.0 green:222/255.0 blue:0/255.0 alpha:1.0];
    [cell setSelectedBackgroundView:bgColorView];
    cell.menu_Item_Label.highlightedTextColor=[UIColor blackColor];
    tableView.allowsMultipleSelection=NO;
    
    cell.menu_item_Count_Lab.hidden=YES;
    if (indexPath.row==0)
    {
        cell.menu_Item_iCon.image=[UIImage imageNamed:@"homeicon"];
        
    }
    else if (indexPath.row==1)
    {
        cell.menu_Item_iCon.image=[UIImage imageNamed:@"Dashboard_black"];
    }
    else if (indexPath.row==2)
        
    {
        cell.menu_Item_iCon.image=[UIImage imageNamed:@"privacy"];
    }
    else if (indexPath.row==3)
        
    {
        cell.menu_Item_iCon.image=[UIImage imageNamed:@"terms"];
    }
    else if (indexPath.row==4)
        
    {
        cell.menu_Item_iCon.image=[UIImage imageNamed:@"Logout"];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==1) {
        [cell setSelected:YES animated:NO];
    }
    else{
         [cell setSelected:NO animated:NO];
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row!=1) {
        NSIndexPath* path = [NSIndexPath indexPathForRow:0 inSection:0];
        UITableViewCell* cell = [tableView cellForRowAtIndexPath:path];
        [cell setSelected:NO animated:NO];
    }
    
     if ([SHARED_HELPER checkIfisInternetAvailable])
     {
         
         if (indexPath.row==0)
         {
             HomeDashboardView*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"HomeDashboardView"];
             [self.navigationController pushViewController:controller animated:YES];
         }
         
         if (indexPath.row == 1)
         {
         }
         else if (indexPath.row == 2)
         {
             PrivacyPolicy*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"PrivacyPolicy"];
             [self.navigationController pushViewController:controller animated:YES];
         }
         else if (indexPath.row == 3)
         {
             
             TermsAndConditions *cntrl = [self.storyboard instantiateViewControllerWithIdentifier:@"TermsAndConditions"];
             [self.navigationController pushViewController:cntrl animated:YES];
         }
         else  if (indexPath.row == 4) {
             if (appDelegate.ridedashboard_home) {
                 [self logoutRideGoingAlert:@"You cannot logout while the ride is going on."];
             }
             else
             {
                 [self showpopup:Terminate];
             }
             
             
             [self.side_Menu_Table reloadData];
         }
    }
     else{
         [SHARED_HELPER showAlert:Nonetwork];
     }
     [self menuleft];

}
-(void)notificationClicked:(UIButton *)sender
{
    if ([SHARED_HELPER checkIfisInternetAvailable]) {
        Notifications *cntlr = [self.storyboard instantiateViewControllerWithIdentifier:@"Notifications"];
        [self.navigationController pushViewController:cntlr animated:YES];
    }
    else{
        [SHARED_HELPER showAlert:Nonetwork];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)Promotions_Action:(id)sender {
    if ([SHARED_HELPER checkIfisInternetAvailable]) {
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        Promotions*promotions = [storyboard instantiateViewControllerWithIdentifier:@"Promotions"];
//        BIZPopupViewController *promotionsPopupView = [[BIZPopupViewController alloc] initWithContentViewController:promotions contentSize:CGSizeMake(SCREEN_WIDTH,SCREEN_HEIGHT-40)];
//        [self presentViewController:promotionsPopupView animated:NO completion:nil];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        Promotions*promotions = [storyboard instantiateViewControllerWithIdentifier:@"Promotions"];
        [self.navigationController pushViewController:promotions animated:NO];
    }
    else{
        [SHARED_HELPER showAlert:Nonetwork];
    }
}

- (IBAction)Ride_action:(id)sender 
{
     if ([SHARED_HELPER checkIfisInternetAvailable]) {
    RidesController*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"RidesController"];
    [self.navigationController pushViewController:controller animated:YES];
     }
     else{
          [SHARED_HELPER showAlert:Nonetwork];
     }
}
-(void)logoutRideGoingAlert:(NSString *)alert_msg
{
    UIAlertController *alertview = [UIAlertController alertControllerWithTitle:@"Can't Logout" message:alert_msg preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    [alertview addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [alertview dismissViewControllerAnimated:YES completion:nil];
        
    }]];
    [self presentViewController:alertview animated:YES completion:nil];
}
-(void)showpopup:(NSString *)message
{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"LOGOUT"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                             NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                            NSData *uData = [NSKeyedArchiver archivedDataWithRootObject:@""];
                             [defaults setObject:uData forKey:User_ID];
                             NSString *user_data;
                             user_data = [defaults objectForKey:User_ID];
                             [defaults removeObjectForKey:User_ID];
                             [defaults removeObjectForKey:@"UserName"];
                             [[FBSDKLoginManager new] logOut];
                             [[GIDSignIn sharedInstance] signOut];
                             [defaults synchronize];
                             
                             LoginViewController *login = [self.storyboard  instantiateViewControllerWithIdentifier:@"LoginViewController"];
                             [self.navigationController pushViewController:login animated:YES];
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"CANCEL"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}


- (IBAction)Route_action:(id)sender
{
    RoutesVC *cntlr = [self.storyboard instantiateViewControllerWithIdentifier:@"RoutesVC"];
    [self.navigationController pushViewController:cntlr animated:YES];
}

- (IBAction)Event_action:(id)sender
{
    EventsViewController *cntlr  = [self.storyboard instantiateViewControllerWithIdentifier:@"EventsViewController"];
    [self.navigationController pushViewController:cntlr animated:YES];
}

- (IBAction)Friends_action:(id)sender
{
    FriendsViewController *cntlr  = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendsViewController"];
    [self.navigationController pushViewController:cntlr animated:YES];
}

- (IBAction)Groups_action:(id)sender
{
    GroupsViewController *cntrl = [self.storyboard instantiateViewControllerWithIdentifier:@"GroupsViewController"];
    [self.navigationController pushViewController:cntrl animated:YES];
}
-(void)user_update_location

{
 
    if (![SHARED_HELPER checkIfisInternetAvailable]) {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
    [dict setObject:[NSNumber numberWithDouble:user_current_latitude] forKey:@"current_latitude"];
    [dict setObject:[NSNumber numberWithDouble:user_current_longtitude] forKey:@"current_longitude"];
     [dict setObject:@"No" forKey:@"is_show"];
    
   
    
    NSLog(@"Update user location : %@",dict);
    
    [SHARED_API updateUserlocationRequestsWithParams:dict withSuccess:^(NSDictionary *response) {
        
  
        dispatch_async(dispatch_get_main_queue(), ^{
            if([[response objectForKey:STATUS] isEqualToString:SUCCESS])
            {
                NSLog(@"Update user response : %@",response);
                
            }
            else
            {
                if ([[response objectForKey:STATUS] isEqualToString:FAIL])
                {
                    NSLog(@"Update user response : %@",response);
                }
            }
        });
        
    } onfailure:^(NSError *theError) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
           
            [SHARED_HELPER showAlert:UpdateLocation_fail];
        });
        
    }];

}
- (IBAction)Setuparide_action:(id)sender
{
   
    if ([SHARED_HELPER checkIfisInternetAvailable]) {
        RidesController*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"RidesController"];
        controller.is_For_Only=@"Scheduled_Rides";
        [self.navigationController pushViewController:controller animated:YES];
    }
    else{
        [SHARED_HELPER showAlert:Nonetwork];
    }
    
}

- (IBAction)Startaride_action:(id)sender
{
     if (appDelegate.ridedashboard_home) {
         self.dashboard_rideGoingBtn.hidden=YES;
         self.dashboard_scrollLabel.hidden=YES;
         NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
         for(UIViewController *tempVC in navigationArray)
         {
             if([tempVC isKindOfClass:[RideDashboard class]])
             {
                 [self.navigationController popToViewController:tempVC animated:YES];
             }
         }

     }
    else
    {
        StartARide*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"StartARide"];
        [[AppHelperClass appsharedInstance] setRideType:@"setup_ride"];
        [self.navigationController pushViewController:controller animated:YES];
    }
//    StartARide*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"StartARide"];
//    [[AppHelperClass appsharedInstance] setRideType:@"setup_ride"];
//    [self.navigationController pushViewController:controller animated:YES];
    
//    RoutesVC *cntlr = [self.storyboard instantiateViewControllerWithIdentifier:@"RoutesVC"];
//    cntlr.is_FROM_VC_OR_Start_Ride=@"YES";
////    [[AppHelperClass appsharedInstance] setRideType:@"Free Ride"];
//    [[AppHelperClass appsharedInstance] setRideType:@"free_ride"];
//     [self.navigationController pushViewController:cntlr animated:YES];
    
//    RideDashboard *rideDashboard=[self.storyboard instantiateViewControllerWithIdentifier:@"RideDashboardStoryboard"];
//    
//    [self.navigationController pushViewController:rideDashboard animated:YES];
    
}
- (IBAction)MyRides_action:(id)sender
{
    if ([SHARED_HELPER checkIfisInternetAvailable]) {
        RidesController*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"RidesController"];
        controller.is_For_Only=@"My_Rides";
        [self.navigationController pushViewController:controller animated:YES];
    }
    else{
        [SHARED_HELPER showAlert:Nonetwork];
    }
    
}
- (IBAction)Menu_Action:(id)sender {
    _side_menu_view.hidden=NO;
    if ([menu_Status isEqualToString:@"LEFT"]) {
        [self menuright];
    }
    else{
        [self menuleft];
    }
}
- (IBAction)Profile_Action:(id)sender {
    
    ProfileView*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileView"];
    
    [self.navigationController pushViewController:controller animated:YES];
    
}
-(void)get_dashboard_data

{
    
    if (![SHARED_HELPER checkIfisInternetAvailable])
        
    {
        
        [SHARED_HELPER showAlert:Nonetwork];
        
        return;
        
    }
         indicaterview.hidden=NO;
//       [activeIndicatore startAnimating];
    
    
    [SHARED_API dashboardRequestsWithParams:[Defaults valueForKey:User_ID] withSuccess:^(NSDictionary *response) {
        
        
        dispatch_async(dispatch_get_main_queue(), ^
                       
                       {
                           
                           NSLog(@" Dashboard responce is : %@",response);
                         
                           NSDictionary *data = [response valueForKey:@"data"];
                           
                           if ([[response valueForKey:STATUS] isEqualToString:SUCCESS])
                               
                           {
                              
                               self.User_Name.text=[data valueForKey:@"name"];
                               [Defaults setObject:self.User_Name.text forKey:@"UserName"];
                               
                               NSString *profile_image_str=[NSString stringWithFormat:@"%@",[data valueForKey:@"profile_image"]];
                               
                               if (![check_strings_array containsObject:profile_image_str] && ![profile_image_str isEqualToString:@"<null>"])
                               {
                                   NSURL*url=[NSURL URLWithString:profile_image_str];
                                   [self.User_image sd_setImageWithURL:url
                                                      placeholderImage:[UIImage imageNamed:@"user_Placeholder"]
                                                               options:SDWebImageRefreshCached];
                               }
                               friend_count=[NSString stringWithFormat:@"%@",[data valueForKey:@"total_friends"]];
                               group_count=[NSString stringWithFormat:@"%@",[data valueForKey:@"total_groups"]];
                               
                               [_side_Menu_Table reloadData];
                               
                               _events_count.text = [NSString stringWithFormat:@"%@",[data valueForKey:@"total_events"]];
                               _friends_count.text =[NSString stringWithFormat:@"%@",[data valueForKey:@"total_friends"]];
                               _groups_count.text=[NSString stringWithFormat:@"%@",[data valueForKey:@"total_groups"]];
                               _rides_count.text = [NSString stringWithFormat:@"%@",[data valueForKey:@"total_completed_rides"]];
                               
                               _routes_count.text = [NSString stringWithFormat:@"%@",[data valueForKey:@"total_routes"]];
                               
                               _fav_rides.text = [NSString stringWithFormat:@"%@",@"-"];
                               
                               _fav_routes.text = [NSString stringWithFormat:@"%@",@"-"];
                               _fav_rides.hidden=YES;
                               _fav_routes.hidden=YES;
                               int notification_coun=[[NSString stringWithFormat:@"%@",[data valueForKey:@"total_notifications"]] intValue];
                               if (notification_coun!=0) {
                                   UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithCustomView:_notification_btn];
                                   badgeView.badgeBackgroundColor=[UIColor blackColor];
                                   if (notification_coun>100) {
                                    badgeView.badgeText=[NSString stringWithFormat:@"100+"];
                                   }
                                   else
                                   badgeView.badgeText=[NSString stringWithFormat:@"%d",notification_coun];
                                   
                                   self.navigationItem.rightBarButtonItems=[NSArray arrayWithObjects:button, nil];
                               }
                           }
                           
//                           if ([activeIndicatore isAnimating])
//                               
//                           {
//                               
//                               [activeIndicatore stopAnimating];
//                               
//                               [activeIndicatore removeFromSuperview];
//                               
//                           }
                           indicaterview.hidden=YES;
                           
                           
                       });
        
    } onfailure:^(NSError *theError) {
        
        NSLog(@"error is %@",theError);
        dispatch_async(dispatch_get_main_queue(), ^
                       
                       {
                        NSString *error_code=[theError localizedDescription];
                        [SHARED_HELPER showAlert:error_code];
//                          [activeIndicatore stopAnimating];
                           indicaterview.hidden=YES;
                       });
        
    }];
}

- (IBAction)onClick_dashboard_rideGoingBtn:(id)sender {
    
   // self.side_menu_view.hidden=YES;
     self.dashboard_rideGoingBtn.hidden=YES;
    self.dashboard_scrollLabel.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
     for(UIViewController *tempVC in navigationArray)
   {
       if([tempVC isKindOfClass:[RideDashboard class]])
       {
           [self.navigationController popToViewController:tempVC animated:NO];
       }
   }

}
@end
