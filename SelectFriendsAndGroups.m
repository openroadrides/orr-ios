//
//  SelectFriendsAndGroups.m
//  openroadrides
//
//  Created by apple on 06/07/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "SelectFriendsAndGroups.h"
#import "MPCoachMarks.h"
#import "AppDelegate.h"

@interface SelectFriendsAndGroups ()
{
    UIBarButtonItem *item0;
    AppDelegate *app_delegate;
    UIButton *saveBtn;
}
@end

@implementation SelectFriendsAndGroups

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"SELECT FRIENDS & GROUPS";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    
    self.frndsAndGroups_scrollLabel.hidden=YES;
    self.frndsAndGroups_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.frndsAndGroups_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.frndsAndGroups_scrollLabel.textColor = [UIColor blackColor];
    self.frndsAndGroups_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.frndsAndGroups_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.frndsAndGroups_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.frndsAndGroups_scrollLabel.scrollSpeed = 60; // pixels per second
    self.frndsAndGroups_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.frndsAndGroups_scrollLabel.fadeLength = 0.f;
    self.frndsAndGroups_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.frndsAndGroups_scrollLabel observeApplicationNotifications];
    
    self.frndsAndGroups_rideGoingBtn.hidden=YES;
    if (appDelegate.ridedashboard_home) {
         self.frndsAndGroups_scrollLabel.hidden=NO;
        self.frndsAndGroups_rideGoingBtn.hidden=NO;
    }
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [leftBtn addTarget:self action:@selector(left_back_btn:) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.frame = CGRectMake(0, 0, 25, 25);
    //    [leftBtn setBackgroundImage:[UIImage imageNamed:@"Finalback-arrow.png"] forState:UIControlStateNormal];
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"back_Image"] forState:UIControlStateNormal];
    UIBarButtonItem *leftBackButton=[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    item0=leftBackButton;
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:item0, nil];
    
    
    
    change_status_frnds=_change_status_routeID;
    removeFrndsIDs=[[NSMutableArray alloc]init];
    
    
    CGFloat content_View_Width=SCREEN_WIDTH-20;
    self.widthConstraint_selectFrndsAndGroups_contentView.constant=content_View_Width*2;
    [self.view layoutIfNeeded];
    
    
    [self.selectFrnds_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.selectGroups_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
//    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//    activeIndicatore.center = self.view.center;
//    activeIndicatore.color = APP_YELLOW_COLOR;
//    activeIndicatore.hidesWhenStopped = TRUE;
//    [self.view addSubview:activeIndicatore];
    
    indicaterview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    activeIndicatore.backgroundColor=[UIColor clearColor];
    activeIndicatore = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.frame = CGRectMake(0.0, 0.0, 80, 80);
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    [indicaterview addSubview:activeIndicatore];
    [self.view addSubview:indicaterview];
    [indicaterview bringSubviewToFront:self.view];
    [activeIndicatore startAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    indicaterview.hidden=YES;

    
    
    
    
    view_Status_selectFrndsAndGroups=@"";
    
    self.selectFrnds_tableView.dataSource=self;
    self.selectFrnds_tableView.delegate=self;
    self.selectGroups_tableView.dataSource=self;
    self.selectGroups_tableView.delegate=self;
    self.noData_label_frnds.hidden=YES;
    self.noData_label_groups.hidden=YES;
    
    
    self.selectFrnds_refreshControl = [[UIRefreshControl alloc]init];
    self.selectFrnds_refreshControl.tintColor=APP_YELLOW_COLOR;
    
    self.selectFrnds_refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
    [self.selectFrnds_refreshControl addTarget:self action:@selector(frndsAndGroupsrefreshTable) forControlEvents:UIControlEventValueChanged];
    [self.selectFrnds_tableView addSubview:self.selectFrnds_refreshControl];
    
    self.selectGroups_refreshControl = [[UIRefreshControl alloc]init];
    self.selectGroups_refreshControl.tintColor=APP_YELLOW_COLOR;
    
    
    self.selectGroups_refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
    [self.selectGroups_refreshControl addTarget:self action:@selector(frndsAndGroupsrefreshTable) forControlEvents:UIControlEventValueChanged];
    
    [self.selectGroups_tableView addSubview:self.selectGroups_refreshControl];
    
    
    _selectFrndsAndGroups_searchBar.hidden=NO;
    _selectFrndsAndGroups_searchBar.delegate=self;
    self.selectFrndsAndGroups_searchBar.tintColor = [UIColor whiteColor];
//    self.selectFrndsAndGroups_searchBar.barTintColor=[UIColor blackColor];
    self.selectFrndsAndGroups_searchBar.barTintColor= ROUTES_CELL_BG_COLOUR1;
   
//    if (appDelegate.arrayOfSelectedFrndsAndRiders.count>0 || appDelegate.arrayOfSelectedGroupsAndRiders.count>0) {
//        [_arrayOfSelectedFrndsAndGroupsID addObjectsFromArray:appDelegate.arrayOfSelectedFrndsAndRiders];
//        [_arrayOfSelectedGroupsID addObjectsFromArray:appDelegate.arrayOfSelectedGroupsAndRiders];
//    }
    if ([_free_route_Frnds_groups_id_string isEqualToString:@"YES"])
    {
        _arrayOfSelectedFrndsAndGroupsID=[[NSMutableArray alloc ]init];
        _arrayOfSelectedGroupsID=[[NSMutableArray alloc]init];
        if (appDelegate.arrayOfSelectedFrndsAndRiders.count>0 || appDelegate.arrayOfSelectedGroupsAndRiders.count>0) {
            [_arrayOfSelectedFrndsAndGroupsID addObjectsFromArray:appDelegate.arrayOfSelectedFrndsAndRiders];
            [_arrayOfSelectedGroupsID addObjectsFromArray:appDelegate.arrayOfSelectedGroupsAndRiders];
        }
      removeFrndsIDs=[[NSMutableArray alloc]init];
    }
    else if ([_free_route_Frnds_groups_id_string isEqualToString:@"EDIT RIDE FRIENDS AND GROUP"])
    {
        _arrayOfSelectedFrndsAndGroupsID=[[NSMutableArray alloc ]init];
        _arrayOfSelectedGroupsID=[[NSMutableArray alloc]init];
        
        if (appDelegate.arrayOfSelectedFrndsAndRiders.count>0 || appDelegate.arrayOfSelectedGroupsAndRiders.count>0)
        {
            [_arrayOfSelectedFrndsAndGroupsID addObjectsFromArray:appDelegate.arrayOfSelectedFrndsAndRiders];
            
            [_arrayOfSelectedGroupsID addObjectsFromArray:appDelegate.arrayOfSelectedGroupsAndRiders];
        }
        removeFrndsIDs=[[NSMutableArray alloc]init];
    }
    
    
    //Chnaging barbutton Title
    if (_arrayOfSelectedFrndsAndGroupsID.count>0||_arrayOfSelectedGroupsID.count>0) {
    }
    else{
    }
 
    if ([appDelegate.isFrom_RidersVC_Or_FreeRideRoutes isEqualToString:@"InviteRiders"])
    {
        skip_btn_is=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(save_clicked)];
        [skip_btn_is setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"Antonio-Bold" size:17.0],
                                              NSForegroundColorAttributeName: [UIColor blackColor]
                                              } forState:UIControlStateNormal];
        
        self.navigationItem.rightBarButtonItem = skip_btn_is;
        
        [self get_selectFrndsAndGroups_Data];
    }
    else if ([appDelegate.isFrom_RidersVC_Or_FreeRideRoutes isEqualToString:@"FreerideRoutesToFrndsAndGroups"])
    {
        UIButton *right_Done_Button=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, 30)];
        right_Done_Button.backgroundColor=[UIColor blackColor];
        right_Done_Button.layer.cornerRadius=5;
        right_Done_Button.clipsToBounds=YES;
        [right_Done_Button setTitle:@"Done" forState:UIControlStateNormal];
        [right_Done_Button addTarget:self action:@selector(save_clicked) forControlEvents:UIControlEventTouchUpInside];
        [right_Done_Button setTintColor:[UIColor whiteColor]];
        [right_Done_Button.titleLabel setFont:[UIFont fontWithName:@"Antonio-Regular" size:17.0]];
        skip_btn_is =[[UIBarButtonItem alloc] initWithCustomView:right_Done_Button];
        
        self.navigationItem.rightBarButtonItem = skip_btn_is;
        
        self.selectFrndsAndGroups_searchBar.layer.borderWidth=1.0;
        self.selectFrndsAndGroups_searchBar.layer.borderColor=[[UIColor whiteColor] CGColor];
        self.selectFrndsAndGroups_searchBar.tintColor=[UIColor whiteColor];
        if ([_is_For_Only isEqualToString:@"Friends"]) {
            self.title = @"SELECT FRIENDS";
            self.selectFrndsAndgroups_scrollView.scrollEnabled=NO;
            self.view_selectFrnds_groups_segment.hidden=YES;
            self.search_Bar_Top.constant=-49;
            [self.view layoutIfNeeded];
            [self onClick_selectFrnds_btn_segment:0];
        }
        else if([_is_For_Only isEqualToString:@"Groups"])
        {
            self.title = @"SELECT GROUPS";
            self.view_selectFrnds_groups_segment.hidden=YES;
            self.selectFrndsAndgroups_scrollView.scrollEnabled=NO;
            self.search_Bar_Top.constant=-49;
            [self.view layoutIfNeeded];
            [self onClick_selectGroups_btn_segment:0];
        }
        else
        {
            [self get_selectFrndsAndGroups_Data];
        }
    }

    
//    _arrayOfSelectedFrndsAndGroupsID=[[NSMutableArray alloc ]init];
//    _arrayOfSelectedGroupsID=[[NSMutableArray alloc]init];

}
-(void)callAnnationsMethod
{
    if ([appDelegate.isFrom_RidersVC_Or_FreeRideRoutes isEqualToString:@"InviteRiders"] || [appDelegate.isFrom_RidersVC_Or_FreeRideRoutes isEqualToString:@"FreerideRoutesToFrndsAndGroups"])
    {
            //Display Annotation
        // Show coach marks
        BOOL coachMarksShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"MPCoachMarksShown_FriendsAndGroups"];
        if (coachMarksShown == NO) {
            // Don't show again
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MPCoachMarksShown_FriendsAndGroups"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self showAnnotation];
        }

    }
//       else if ([appDelegate.isFrom_RidersVC_Or_FreeRideRoutes isEqualToString:@"FreerideRoutesToFrndsAndGroups"]) {
//           //Display Annotation
//           // Show coach marks
//           BOOL coachMarksShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"MPCoachMarksShown_Routes_FriendsAndGroups"];
//           if (coachMarksShown == NO) {
//               // Don't show again
//               [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MPCoachMarksShown_Routes_FriendsAndGroups"];
//               [[NSUserDefaults standardUserDefaults] synchronize];
//               
//               [self showAnnotation];
//           }
//
//           
//       }
}
#pragma mark - Annotations
-(void)showAnnotation
{
    [self.view layoutIfNeeded];
    
    // Setup coach marks
    CGRect coachmark1 = CGRectMake ( ([UIScreen mainScreen].bounds.size.width - 45), 19, self.navigationController.navigationBar.frame.size.height, self.navigationController.navigationBar.frame.size.height);
    CGRect coachmark2 = CGRectMake (_selectFrndsAndGroups_searchBar.frame.origin.x,_selectFrndsAndGroups_searchBar.frame.origin.y+65, _selectFrndsAndGroups_searchBar.frame.size.width,_selectFrndsAndGroups_searchBar.frame.size.height);
    CGRect coachmark3 = CGRectMake( _selectFrndsAndGroups_searchBar.frame.origin.x+10, _selectFrndsAndGroups_searchBar.frame.origin.y+_selectFrndsAndGroups_searchBar.frame.size.height+105, 38, 38);
    
    NSArray *coachMarks;
    
   
    if (arrayOfSelectFrnds.count == 0) {
         if ([appDelegate.isFrom_RidersVC_Or_FreeRideRoutes isEqualToString:@"FreerideRoutesToFrndsAndGroups"])
         {
             coachMarks = @[
                            @{
                                @"rect": [NSValue valueWithCGRect:coachmark2],
                                @"caption": @"Search friends",
                                @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                                @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_CENTER]
                                //@"showArrow":[NSNumber numberWithBool:YES]
                                },

                            @{
                                @"rect": [NSValue valueWithCGRect:coachmark1],
                                @"caption": @"Tap here to skip",
                                @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                                @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                                @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_RIGHT]
                                //@"showArrow":[NSNumber numberWithBool:YES]
                                },
                        ];

         }
        else
        // Setup coach marks
        coachMarks = @[
                       @{
                           @"rect": [NSValue valueWithCGRect:coachmark2],
                           @"caption": @"Search friends",
                           @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                           @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_CENTER]
                           //@"showArrow":[NSNumber numberWithBool:YES]
                           },

                       @{
                           @"rect": [NSValue valueWithCGRect:coachmark1],
                           @"caption": @"Tap here to complete",
                           @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                           @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                           @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_RIGHT]
                           //@"showArrow":[NSNumber numberWithBool:YES]
                           },
                    ];
    }
    else
    {
        if ([appDelegate.isFrom_RidersVC_Or_FreeRideRoutes isEqualToString:@"FreerideRoutesToFrndsAndGroups"])
        {
            coachMarks = @[
                           
                           @{
                               @"rect": [NSValue valueWithCGRect:coachmark2],
                               @"caption": @"Search friends",
                               @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                               @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_CENTER]
                               //@"showArrow":[NSNumber numberWithBool:YES]
                               },
                           @{
                               @"rect": [NSValue valueWithCGRect:coachmark3],
                               @"caption": @"Select friends",
                               @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                               @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                               @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_LEFT],
                               //@"showArrow":[NSNumber numberWithBool:YES]
                               },
                           @{
                               @"rect": [NSValue valueWithCGRect:coachmark1],
                               @"caption": @"Tap here to skip",
                               @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                               @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                               @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_RIGHT]
                               //@"showArrow":[NSNumber numberWithBool:YES]
                               },
                           ];
        }
        else
        // Setup coach marks
        coachMarks = @[
                       
                       @{
                           @"rect": [NSValue valueWithCGRect:coachmark2],
                           @"caption": @"Search friends",
                           @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                           @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_CENTER]
                           //@"showArrow":[NSNumber numberWithBool:YES]
                           },
                       @{
                           @"rect": [NSValue valueWithCGRect:coachmark3],
                           @"caption": @"Select friends",
                           @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                           @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                           @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_LEFT],
                           //@"showArrow":[NSNumber numberWithBool:YES]
                           },
                       @{
                           @"rect": [NSValue valueWithCGRect:coachmark1],
                           @"caption": @"Tap here to complete",
                           @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                           @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                           @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_RIGHT]
                           //@"showArrow":[NSNumber numberWithBool:YES]
                           },
                       ];
        
    }
    
        
    MPCoachMarks *coachMarksView = [[MPCoachMarks alloc] initWithFrame:self.navigationController.view.bounds coachMarks:coachMarks];
    //[self.navigationController.view addSubview:coachMarksView];
    [[UIApplication sharedApplication].keyWindow addSubview:coachMarksView];
    [coachMarksView start];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)left_back_btn:(UIButton *)sender
{
    
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)save_clicked
{
//    if (arrayOfSelectedFrndsAndGroupsID.count>0) {
//    
    
    
    
//    }
//    else{
//        [SHARED_HELPER showAlert:CREATEGROUP];
//    }
   
    if ([appDelegate.isFrom_RidersVC_Or_FreeRideRoutes isEqualToString:@"InviteRiders"]) {
        NSLog(@"Selected Frnds Ids %@",_arrayOfSelectedFrndsAndGroupsID);
        NSLog(@"Selected Group Ids %@",_arrayOfSelectedGroupsID);
        appDelegate.arrayOfSelectedFrndsAndRiders=[[NSMutableArray alloc]init];
        appDelegate.arrayOfSelectedFrndsAndRiders=_arrayOfSelectedFrndsAndGroupsID;
        appDelegate.arrayOfSelectedGroupsAndRiders=[[NSMutableArray alloc]init];
        appDelegate.arrayOfSelectedGroupsAndRiders=_arrayOfSelectedGroupsID;
        
        appDelegate.arrayOFRemoveFrndsID=[[NSMutableArray alloc]init];
        appDelegate.arrayOFRemoveFrndsID=removeFrndsIDs;
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if ([appDelegate.isFrom_RidersVC_Or_FreeRideRoutes isEqualToString:@"FreerideRoutesToFrndsAndGroups"])
    {
         if ([_is_For_Only isEqualToString:@"Friends"])
         {
             NSLog(@"Selected Frnds Ids %@",_arrayOfSelectedFrndsAndGroupsID);
            
             appDelegate.arrayOfSelectedFrndsAndRiders=[[NSMutableArray alloc]init];
             appDelegate.arrayOfSelectedFrndsAndRiders=_arrayOfSelectedFrndsAndGroupsID;
             [[NSNotificationCenter defaultCenter]postNotificationName:@"Start_Ride_From_Friends_Select_Deselect" object:self];
         }
        else
        {
           
            NSLog(@"Selected Group Ids %@",_arrayOfSelectedGroupsID);
            appDelegate.arrayOfSelectedGroupsAndRiders=[[NSMutableArray alloc]init];
            appDelegate.arrayOfSelectedGroupsAndRiders=_arrayOfSelectedGroupsID;
            
            [[NSNotificationCenter defaultCenter]postNotificationName:@"Start_Ride_From_Group_Select_Deselect" object:self];
        }
        [self.navigationController popViewControllerAnimated:YES];
        
    }
//
}

- (void)frndsAndGroupsrefreshTable {
    //TODO: refresh your data
    if([view_Status_selectFrndsAndGroups isEqualToString:@""]||[view_Status_selectFrndsAndGroups isEqualToString:@"PUBLIC"])
    {
        select_Friends_Called=@"YES";
        [self.selectFrnds_refreshControl endRefreshing];
        //        [self.public_friends_tableView reloadData];
        arrayOfSelectFrnds=nil;
        arrayOfSelectFrnds=[[NSMutableArray alloc]init];
        _selectedFrndsFilterdArray=nil;
        _selectedFrndsFilterdArray=[[NSMutableArray alloc]init];
        [self.selectFrnds_tableView reloadData];
        [self get_selectFrndsAndGroups_Data];
        [self.selectFrndsAndGroups_searchBar setText:@""];
    }
    else{
        select_Groups_Called=@"YES";
        arratOfSelectGroups=nil;
        arratOfSelectGroups=[[NSMutableArray alloc]init];
        _selectedGroupsFilterdArray=nil;
        _selectedGroupsFilterdArray=[[NSMutableArray alloc]init];
        [self.selectGroups_tableView reloadData];
        [self.selectGroups_refreshControl endRefreshing];
        [self get_selectFrndsAndGroups_Data];
        [self.selectFrndsAndGroups_searchBar setText:@""];
        
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onClick_selectFrnds_btn_segment:(id)sender {
    
    if (arrayOfSelectFrnds.count>0) {
        self.noData_label_frnds.hidden=YES;
    }
    view_Status_selectFrndsAndGroups=@"PUBLIC";
    self.view_selectFrnds_segment.backgroundColor=[UIColor whiteColor];
    self.view_selectGroups_segment.backgroundColor=[UIColor blackColor];
    [self.selectFrndsAndgroups_scrollView setContentOffset:CGPointMake((0), _selectFrndsAndgroups_scrollView.contentOffset.y)];
    self.selectFrnds_label_segment.textColor=[UIColor whiteColor];
    self.selectGroups_label_segment.textColor=APP_YELLOW_COLOR;
    if ([select_Friends_Called isEqualToString:@"YES"]) {
        frndsAndGroups_isFiltered=NO;
        [_selectFrndsAndGroups_searchBar resignFirstResponder];
        _selectFrndsAndGroups_searchBar.text=@"";
        _selectFrndsAndGroups_searchBar.showsCancelButton=NO;
        [_selectFrnds_tableView reloadData];
    }
    else{
        frndsAndGroups_isFiltered=NO;
        [_selectFrndsAndGroups_searchBar resignFirstResponder];
        _selectFrndsAndGroups_searchBar.text=@"";
        _selectFrndsAndGroups_searchBar.showsCancelButton=NO;
        [self get_selectFrndsAndGroups_Data];
    }
}
- (IBAction)onClick_selectGroups_btn_segment:(id)sender {
    if (arratOfSelectGroups.count>0) {
        self.noData_label_frnds.hidden=YES;
    }
    
    view_Status_selectFrndsAndGroups=@"MY";
    self.view_selectFrnds_segment.backgroundColor=[UIColor blackColor];
    self.view_selectGroups_segment.backgroundColor=[UIColor whiteColor];
    [self.selectFrndsAndgroups_scrollView setContentOffset:CGPointMake((SCREEN_WIDTH-20), self.selectFrndsAndgroups_scrollView.contentOffset.y)];
    self.selectGroups_label_segment.textColor=[UIColor whiteColor];
    self.selectFrnds_label_segment.textColor=APP_YELLOW_COLOR;
    
    if ([select_Groups_Called isEqualToString:@"YES"]) {
        frndsAndGroups_isFiltered=NO;
        [_selectFrndsAndGroups_searchBar resignFirstResponder];
        _selectFrndsAndGroups_searchBar.text=@"";
        _selectFrndsAndGroups_searchBar.showsCancelButton=NO;
        [_selectGroups_tableView reloadData];

    }
    else{
        
        //  [self.my_Routes_Table reloadData];
        frndsAndGroups_isFiltered=NO;
        [_selectFrndsAndGroups_searchBar resignFirstResponder];
        _selectFrndsAndGroups_searchBar.text=@"";
        _selectFrndsAndGroups_searchBar.showsCancelButton=NO;
        [self get_selectFrndsAndGroups_Data];
    }
    
}
#pragma mark - ScrollView
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    self.selctFrnds_btn_segment.userInteractionEnabled=YES;
    self.selectGroups_btn_segment.userInteractionEnabled=YES;
    if (scrollView.tag==5) {
        if (scrollView.contentOffset.x==0) {
            [self onClick_selectFrnds_btn_segment:0];
        }
        if (scrollView.contentOffset.x==SCREEN_WIDTH-20) {
            [self onClick_selectGroups_btn_segment:0];
        }
    }
}
-(void)get_selectFrndsAndGroups_Data
{
//    [activeIndicatore startAnimating];
    indicaterview.hidden=NO;
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [SHARED_HELPER showAlert:Nonetwork];
//        [activeIndicatore stopAnimating];
        indicaterview.hidden=YES;
        return;
    }
    NSString *user_id=[Defaults valueForKey:User_ID];
    //    NSString *user_id=@"1";
    NSString *selectFrndsAndGroups_service_is;
    if ([view_Status_selectFrndsAndGroups isEqualToString:@""]||[view_Status_selectFrndsAndGroups isEqualToString:@"PUBLIC"]) {
        selectFrndsAndGroups_service_is=WS_PUBLICFRIENDS_REQUEST;
        arrayOfSelectFrnds=[[NSMutableArray alloc]init];
        _selectedFrndsFilterdArray=[[NSMutableArray alloc]init];
       
        
    }
    else{
        selectFrndsAndGroups_service_is=WS_GROUPS_REQUEST;
        arratOfSelectGroups=[[NSMutableArray alloc]init];
        _selectedGroupsFilterdArray=[[NSMutableArray alloc]init];
    }
    
   [SHARED_API selectedFriendsAndGroupsRequest:user_id public_myfriends:selectFrndsAndGroups_service_is withSuccess:^(NSDictionary *response) {
       NSLog(@"%@",response);
       [self rideInvite_Friends_Service_Sucsess:response];
//       [activeIndicatore stopAnimating];
       indicaterview.hidden=YES;

       
   } onfailure:^(NSError *theError) {
       [SHARED_HELPER showAlert:ServiceFail];
//       [activeIndicatore stopAnimating];
       indicaterview.hidden=YES;
       
   }];
    
    
    
//    
//    [SHARED_API publicFriendsRequest:user_id public_myfriends:selectFrndsAndGroups_service_is withSuccess:^(NSDictionary *response) {
//        NSLog(@"%@",response);
//        [self rideInvite_Friends_Service_Sucsess:response];
//        [activeIndicatore stopAnimating];
//    } onfailure:^(NSError *theError) {
//        [SHARED_HELPER showAlert:ServiceFail];
//        [activeIndicatore stopAnimating];
//    }];
    
}
-(void)rideInvite_Friends_Service_Sucsess:(NSDictionary *)respnse{
    
    
    if ([[respnse valueForKey:STATUS] isEqualToString:SUCCESS]) {
        if ([view_Status_selectFrndsAndGroups isEqualToString:@""]||[view_Status_selectFrndsAndGroups isEqualToString:@"PUBLIC"]) {
            NSArray *friends_data=[respnse valueForKey:@"friends"];
            if (friends_data.count>0) {
                for (int i=0; i<friends_data.count; i++) {
                    NSDictionary *data_Dict_is=[friends_data objectAtIndex:i];
                    [arrayOfSelectFrnds addObject:data_Dict_is];
                    [_selectedFrndsFilterdArray addObject:data_Dict_is];
                    
                }
                self.selectFrndsAndgroups_scrollView.hidden=NO;

                    view_Status_selectFrndsAndGroups=@"PUBLIC";
                    select_Friends_Called=@"YES";
                    //                _searchBar.hidden=NO;
                    [self.selectFrnds_tableView reloadData];
//                }
//                else{
//                    [self.selectGroups_tableView reloadData];
//                    select_Groups_Called=@"YES";
//                }
            }
            else{
                [self service_fail_OR_EmptyData];
            }
           

        }
        else{
                    NSArray *groups_data=[respnse valueForKey:@"groups"];
                    if (groups_data.count>0) {
                        for (int i=0; i<groups_data.count; i++) {
                            NSDictionary *data_Dict_is=[groups_data objectAtIndex:i];
                            
                            [arratOfSelectGroups addObject:data_Dict_is];
                            [_selectedGroupsFilterdArray addObject:data_Dict_is];
            
                        }
                        self.selectFrndsAndgroups_scrollView.hidden=NO;
                        
                            [self.selectGroups_tableView reloadData];
                            select_Groups_Called=@"YES";
//                        view_Status_selectFrndsAndGroups=@"PUBLIC";
                    }
                    else{
                            [self service_fail_OR_EmptyData];
                    }
        }
        [self callAnnationsMethod];
        
    }
    else{
        [SHARED_HELPER showAlert:ServiceFail];
        [self service_fail_OR_EmptyData];
    }
}
-(void)service_fail_OR_EmptyData{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        //        [self.navigationController popViewControllerAnimated:YES];
         if ([view_Status_selectFrndsAndGroups isEqualToString:@""]||[view_Status_selectFrndsAndGroups isEqualToString:@"PUBLIC"]) {
             view_Status_selectFrndsAndGroups=@"PUBLIC";
             select_Friends_Called=@"YES";
             self.noData_label_frnds.hidden=NO;
            
         }
         else{
              select_Groups_Called=@"YES";
             self.noData_label_groups.hidden=NO;
         }
         [self callAnnationsMethod];
    });
}
-(SelectFriendsCell *) containingCellForView:(UIView *)view
{
    if (!view.superview)
        return nil;
    
    if ([view.superview isKindOfClass:[SelectFriendsCell class]])
        return (SelectFriendsCell *)view.superview;
    
    return [self containingCellForView:view.superview];
}
- (IBAction)onClick_selectFrnds_checkbox_btn:(UIButton *)sender {
    
    
    if (frndsAndGroups_isFiltered) {
        SelectFriendsCell  *containingCell = [self containingCellForView:sender];
        NSIndexPath *indexPath = [_selectFrnds_tableView indexPathForCell:containingCell];
        
        NSString *select_Frnds_string=[NSString stringWithFormat:@"%@",[[_selectedFrndsFilterdArray objectAtIndex:indexPath.row] objectForKey:@"friend_id"]];
        
        change_status_frnds=YES;
        
        if ([_arrayOfSelectedFrndsAndGroupsID containsObject:select_Frnds_string])
            
        {
            [_arrayOfSelectedFrndsAndGroupsID removeObject:select_Frnds_string];
            
            if (![removeFrndsIDs containsObject:select_Frnds_string]) {
                [removeFrndsIDs addObject:select_Frnds_string];
                
            }
        }
        else
        {
            [_arrayOfSelectedFrndsAndGroupsID addObject:select_Frnds_string];
        }
        
        //Chnaging barbutton Title
        
        if ([appDelegate.isFrom_RidersVC_Or_FreeRideRoutes isEqualToString:@"InviteRiders"]) {
            if (_arrayOfSelectedFrndsAndGroupsID.count>0||_arrayOfSelectedGroupsID.count>0) {
                [skip_btn_is setTitle:@"Done"];
            }
            else{
                [skip_btn_is setTitle:@"Done"];
            }
        }
        else
        {
            
         
        }
        [self.selectFrnds_tableView reloadData];
    }
    else
    {
        SelectFriendsCell  *containingCell = [self containingCellForView:sender];
        NSIndexPath *indexPath = [_selectFrnds_tableView indexPathForCell:containingCell];
        
        NSString *select_Frnds_string=[NSString stringWithFormat:@"%@",[[arrayOfSelectFrnds objectAtIndex:indexPath.row] objectForKey:@"friend_id"]];
        
        change_status_frnds=YES;
        
        if ([_arrayOfSelectedFrndsAndGroupsID containsObject:select_Frnds_string])
            
        {
            [_arrayOfSelectedFrndsAndGroupsID removeObject:select_Frnds_string];
            
            if (![removeFrndsIDs containsObject:select_Frnds_string]) {
                [removeFrndsIDs addObject:select_Frnds_string];
                
            }
        }
        else
        {
            [_arrayOfSelectedFrndsAndGroupsID addObject:select_Frnds_string];
        }
        
        //Chnaging barbutton Title
        
        if ([appDelegate.isFrom_RidersVC_Or_FreeRideRoutes isEqualToString:@"InviteRiders"]) {
            if (_arrayOfSelectedFrndsAndGroupsID.count>0||_arrayOfSelectedGroupsID.count>0) {
                [skip_btn_is setTitle:@"Done"];
            }
            else{
                [skip_btn_is setTitle:@"Done"];
            }
        }
        else{
          
        }
        [self.selectFrnds_tableView reloadData];
    }
    
    
}
-(SelectGroupsCell *) containingCellForView1:(UIView *)view
{
    if (!view.superview)
        return nil;
    
    if ([view.superview isKindOfClass:[SelectGroupsCell class]])
        return (SelectGroupsCell *)view.superview;
    
    return [self containingCellForView1:view.superview];
}
- (IBAction)onClick_selectGroups_checkbox_btn:(UIButton *)sender {
    if (frndsAndGroups_isFiltered) {
        SelectGroupsCell  *containingCell = [self containingCellForView1:sender];
        NSIndexPath *indexPath = [_selectGroups_tableView indexPathForCell:containingCell];
        
        NSString *select_groups_string=[NSString stringWithFormat:@"%@",[[_selectedGroupsFilterdArray objectAtIndex:indexPath.row] objectForKey:@"group_id"]];
        
        change_status_frnds=YES;
        
        if ([_arrayOfSelectedGroupsID containsObject:select_groups_string])
            
        {
            [_arrayOfSelectedGroupsID removeObject:select_groups_string];
        }
        else
        {
            [_arrayOfSelectedGroupsID addObject:select_groups_string];
        }
        //Chnaging barbutton Title
        if ([appDelegate.isFrom_RidersVC_Or_FreeRideRoutes isEqualToString:@"InviteRiders"]) { //is comming from create or edit riders
            if (_arrayOfSelectedFrndsAndGroupsID.count>0||_arrayOfSelectedGroupsID.count>0) {
                [skip_btn_is setTitle:@"Done"];
            }
            else{
                [skip_btn_is setTitle:@"Skip"];
            }
        }
        else
        {
           
        }
        
        [self.selectGroups_tableView reloadData];
    }
    else
    {
        SelectGroupsCell  *containingCell = [self containingCellForView1:sender];
        NSIndexPath *indexPath = [_selectGroups_tableView indexPathForCell:containingCell];
        
        NSString *select_groups_string=[NSString stringWithFormat:@"%@",[[arratOfSelectGroups objectAtIndex:indexPath.row] objectForKey:@"group_id"]];
        
        change_status_frnds=YES;
        
        if ([_arrayOfSelectedGroupsID containsObject:select_groups_string])
            
        {
            [_arrayOfSelectedGroupsID removeObject:select_groups_string];
        }
        else
        {
            [_arrayOfSelectedGroupsID addObject:select_groups_string];
        }
        //Chnaging barbutton Title
        if ([appDelegate.isFrom_RidersVC_Or_FreeRideRoutes isEqualToString:@"InviteRiders"]) { //is comming from create or edit riders
            if (_arrayOfSelectedFrndsAndGroupsID.count>0||_arrayOfSelectedGroupsID.count>0) {
                [skip_btn_is setTitle:@"Done"];
            }
            else{
                [skip_btn_is setTitle:@"Skip"];
            }
        }
        else
        {
           
        }
        
        [self.selectGroups_tableView reloadData];
    }
    
}

#pragma mark - TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([view_Status_selectFrndsAndGroups isEqualToString:@"PUBLIC"]) {
        
        if ([arrayOfSelectFrnds count]>0)
        {
            if (frndsAndGroups_isFiltered) {
               
               return [_selectedFrndsFilterdArray count];
              
            }
            return arrayOfSelectFrnds.count;
        }
        
    }
    else if ([view_Status_selectFrndsAndGroups isEqualToString:@"MY"])
    {
        
        
        
        if ([arratOfSelectGroups count]>0)
        {
            if (frndsAndGroups_isFiltered) {
                
                return [_selectedGroupsFilterdArray count];
                
            }
            return arratOfSelectGroups.count;
        }
        
    }
    else{
        return 0;
    }
    return 0;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    

    if ([view_Status_selectFrndsAndGroups isEqualToString:@"PUBLIC"]) {
        
        
        
        SelectFriendsCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
        
        if (cell == nil)
        {
            cell = [[SelectFriendsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        }
        if (frndsAndGroups_isFiltered) {
            
            if (indexPath.row%2==0) {
                cell.selectFrndCell_bgView.backgroundColor=ROUTES_CELL_BG_COLOUR1;
            }
            else
                cell.selectFrndCell_bgView.backgroundColor=ROUTES_CELL_BG_COLOUR2;
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.selectFrnd_userName_label.text=[[_selectedFrndsFilterdArray objectAtIndex:indexPath.row] objectForKey:@"name"];
            
            NSString * areaName=[NSString stringWithFormat:@"%@",[[_selectedFrndsFilterdArray objectAtIndex:indexPath.row] objectForKey:@"address"]];
            NSString * cityName=[NSString stringWithFormat:@"%@",[[_selectedFrndsFilterdArray objectAtIndex:indexPath.row] objectForKey:@"city"]];
            NSString * stateName=[NSString stringWithFormat:@"%@",[[_selectedFrndsFilterdArray objectAtIndex:indexPath.row] objectForKey:@"state"]];
            NSString *zipCode=[NSString stringWithFormat:@"%@",[[_selectedFrndsFilterdArray objectAtIndex:indexPath.row] objectForKey:@"zipcode"]];
            
            NSMutableArray *empaty_array1=[[NSMutableArray alloc]init];
            [empaty_array1 addObject:@"<null>"];
            [empaty_array1 addObject:@"null"];
            [empaty_array1 addObject:@""];
            [empaty_array1 addObject:@"(null)"];
            [empaty_array1 addObject:@"0"];
            
            NSMutableArray *Data_array1=[[NSMutableArray alloc]init];
            
            
            if ([empaty_array1 containsObject:areaName]) {
                
            }
            else{
                
                [Data_array1 addObject:areaName];
            }
            if ([empaty_array1 containsObject:cityName]) {
                
            }
            else{
                [Data_array1 addObject:cityName];
            }
            
            if ([empaty_array1 containsObject:stateName]) {
                
            }
            else{
                [Data_array1 addObject:stateName];
            }
            
            if ([empaty_array1 containsObject:zipCode]) {
                
            }
            else{
                [Data_array1 addObject:zipCode];
            }
            
            NSMutableString *add_data1=[[NSMutableString alloc]init];
            if (Data_array1.count>0) {
                cell.selectFrnds_locationPlaceholder_imageView.image=[UIImage imageNamed:@"check_in_placeholder"];
                
                for (int i=0; i<Data_array1.count; i++) {
                    
                    NSString *adress=[Data_array1 objectAtIndex:i];
                    if (i==0) {
                        [add_data1 appendFormat:@"%@", [NSString stringWithFormat:@"%@",adress]];
                    }
                    else
                        [add_data1 appendFormat:@"%@", [NSString stringWithFormat:@",%@",adress]];
                }
                cell.selectFrnd_address_label.text=add_data1;
                cell.viewTop_address_constraint.priority=750;
                cell.viewtop_userName_constraint.priority=250;
            }
            else{
                cell.selectFrnds_locationPlaceholder_imageView.image=[UIImage imageNamed:@""];
                cell.selectFrnd_address_label.text=@"";
                
                cell.viewTop_address_constraint.priority=250;
                cell.viewtop_userName_constraint.priority=750;
            }
            
            
            NSString *profile_image_string1 = [NSString stringWithFormat:@"%@", [[_selectedFrndsFilterdArray objectAtIndex:indexPath.row] objectForKey:@"profile_image"]];
            NSURL*url=[NSURL URLWithString:profile_image_string1];
            [cell.selectFrnd_profileImage sd_setImageWithURL:url
                                            placeholderImage:[UIImage imageNamed:@"user_Placeholder"]
                                                     options:SDWebImageRefreshCached];
            
            cell.selectFrnd_checkbox_btn.tag=indexPath.row;
            
            
            NSString*publicFrndID=[NSString stringWithFormat:@"%@",[[_selectedFrndsFilterdArray objectAtIndex:indexPath.row] objectForKey:@"friend_id"]];
            
            
            
            if ([_arrayOfSelectedFrndsAndGroupsID containsObject:publicFrndID])
            {
                [cell.selectFrnd_checkbox_btn setBackgroundImage:[UIImage imageNamed:@"checkbox_fill"] forState:UIControlStateNormal];
            }
            else
            {
                [cell.selectFrnd_checkbox_btn setBackgroundImage:[UIImage imageNamed:@"checkbox_empty"] forState:UIControlStateNormal];
            }
            
            cell.selectFrnd_userName_label.textColor=APP_YELLOW_COLOR;
            NSString*statusString=[NSString stringWithFormat:@"%@",[[_selectedFrndsFilterdArray objectAtIndex:indexPath.row] objectForKey:@"status"]];
            if ([statusString isEqualToString:@"<null>"] || [statusString isEqualToString:@""] || [statusString isEqualToString:@"null"] || statusString == nil) {
                
            }
            else{
                if ([statusString isEqualToString:@"notfriend"]) {
                    cell.selectFrnd_statusLabel.text=@"";
                }
                else if ([statusString isEqualToString:@"pending"])
                {
                    cell.selectFrnd_statusLabel.text=@"";
                }
                else
                {
                    cell.selectFrnd_statusLabel.text=@"friends";
                }
            }
        }
        
        else
        {
            // Without Search
            if (indexPath.row%2==0) {
                cell.selectFrndCell_bgView.backgroundColor=ROUTES_CELL_BG_COLOUR1;
            }
            else
                cell.selectFrndCell_bgView.backgroundColor=ROUTES_CELL_BG_COLOUR2;
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.selectFrnd_userName_label.text=[[arrayOfSelectFrnds objectAtIndex:indexPath.row] objectForKey:@"name"];
            
            NSString * areaName=[NSString stringWithFormat:@"%@",[[arrayOfSelectFrnds objectAtIndex:indexPath.row] objectForKey:@"address"]];
            NSString * cityName=[NSString stringWithFormat:@"%@",[[arrayOfSelectFrnds objectAtIndex:indexPath.row] objectForKey:@"city"]];
            NSString * stateName=[NSString stringWithFormat:@"%@",[[arrayOfSelectFrnds objectAtIndex:indexPath.row] objectForKey:@"state"]];
            NSString *zipCode=[NSString stringWithFormat:@"%@",[[arrayOfSelectFrnds objectAtIndex:indexPath.row] objectForKey:@"zipcode"]];
            
            NSMutableArray *empaty_array=[[NSMutableArray alloc]init];
            [empaty_array addObject:@"<null>"];
            [empaty_array addObject:@"null"];
            [empaty_array addObject:@""];
            [empaty_array addObject:@"(null)"];
            [empaty_array addObject:@"0"];
            
            NSMutableArray *Data_array=[[NSMutableArray alloc]init];
            
            
            if ([empaty_array containsObject:areaName]) {
                
            }
            else{
                
                [Data_array addObject:areaName];
            }
            if ([empaty_array containsObject:cityName]) {
                
            }
            else{
                [Data_array addObject:cityName];
            }
            
            if ([empaty_array containsObject:stateName]) {
                
            }
            else{
                [Data_array addObject:stateName];
            }
            
            if ([empaty_array containsObject:zipCode]) {
                
            }
            else{
                [Data_array addObject:zipCode];
            }
            
            NSMutableString *add_data=[[NSMutableString alloc]init];
            if (Data_array.count>0) {
                cell.selectFrnds_locationPlaceholder_imageView.image=[UIImage imageNamed:@"check_in_placeholder"];
                
                for (int i=0; i<Data_array.count; i++) {
                    
                    NSString *adress=[Data_array objectAtIndex:i];
                    if (i==0) {
                        [add_data appendFormat:@"%@", [NSString stringWithFormat:@"%@",adress]];
                    }
                    else
                        [add_data appendFormat:@"%@", [NSString stringWithFormat:@",%@",adress]];
                }
                cell.selectFrnd_address_label.text=add_data;
                cell.viewTop_address_constraint.priority=750;
                cell.viewtop_userName_constraint.priority=250;
            }
            else{
                cell.selectFrnds_locationPlaceholder_imageView.image=[UIImage imageNamed:@""];
                cell.selectFrnd_address_label.text=@"";
                
                cell.viewTop_address_constraint.priority=250;
                cell.viewtop_userName_constraint.priority=750;
            }
            
            
            NSString *profile_image_string1 = [NSString stringWithFormat:@"%@", [[arrayOfSelectFrnds objectAtIndex:indexPath.row] objectForKey:@"profile_image"]];
            NSURL*url=[NSURL URLWithString:profile_image_string1];
            [cell.selectFrnd_profileImage sd_setImageWithURL:url
                                            placeholderImage:[UIImage imageNamed:@"user_Placeholder"]
                                                     options:SDWebImageRefreshCached];
            
            cell.selectFrnd_checkbox_btn.tag=indexPath.row;
            
            
            NSString*publicFrndID=[NSString stringWithFormat:@"%@",[[arrayOfSelectFrnds objectAtIndex:indexPath.row] objectForKey:@"friend_id"]];
            
            
            
            if ([_arrayOfSelectedFrndsAndGroupsID containsObject:publicFrndID])
            {
                [cell.selectFrnd_checkbox_btn setBackgroundImage:[UIImage imageNamed:@"checkbox_fill"] forState:UIControlStateNormal];
            }
            else
            {
                [cell.selectFrnd_checkbox_btn setBackgroundImage:[UIImage imageNamed:@"checkbox_empty"] forState:UIControlStateNormal];
            }
            
            cell.selectFrnd_userName_label.textColor=APP_YELLOW_COLOR;
            NSString*statusString=[NSString stringWithFormat:@"%@",[[arrayOfSelectFrnds objectAtIndex:indexPath.row] objectForKey:@"status"]];
            if ([statusString isEqualToString:@"<null>"] || [statusString isEqualToString:@""] || [statusString isEqualToString:@"null"] || statusString == nil) {
                
            }
            else{
                if ([statusString isEqualToString:@"notfriend"]) {
                    cell.selectFrnd_statusLabel.text=@"";
                }
                else if ([statusString isEqualToString:@"pending"])
                {
                    cell.selectFrnd_statusLabel.text=@"";
                }
                else
                {
                    cell.selectFrnd_statusLabel.text=@"friends";
                }
            }
        }
    
        return cell;
    }
    else
    {
        SelectGroupsCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
        
        if (cell == nil)
        {
            cell = [[SelectGroupsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        }
        if (frndsAndGroups_isFiltered) {
            //            without Search
            if (indexPath.row%2==0) {
                cell.selectgroupell_bgView.backgroundColor=ROUTES_CELL_BG_COLOUR1;
            }
            else
                cell.selectgroupell_bgView.backgroundColor=ROUTES_CELL_BG_COLOUR2;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.selectGroup_userName_label.text=[[_selectedGroupsFilterdArray objectAtIndex:indexPath.row] objectForKey:@"group_name"];
            cell.selectGroup_userName_label.textColor=APP_YELLOW_COLOR;
            cell.selectGroup_members_label.text=[NSString stringWithFormat:@"%@",[[_selectedGroupsFilterdArray objectAtIndex:indexPath.row] objectForKey:@"members_count"]];
            cell.selectGroup_createdBy_label.text=[NSString stringWithFormat:@"Created By %@",[[_selectedGroupsFilterdArray objectAtIndex:indexPath.row] objectForKey:@"group_owner"]];
            
            NSString *profile_image_string = [NSString stringWithFormat:@"%@", [[_selectedGroupsFilterdArray objectAtIndex:indexPath.row] objectForKey:@"group_image"]];
            NSURL*url1=[NSURL URLWithString:profile_image_string];
            [cell.selectGroup_groupImageView sd_setImageWithURL:url1
                                               placeholderImage:[UIImage imageNamed:@"groups-icon"]
                                                        options:SDWebImageRefreshCached];
            cell.selectGroup_checkbox_btn.tag=indexPath.row;
            
            
            NSString*myGroupID=[NSString stringWithFormat:@"%@",[[_selectedGroupsFilterdArray objectAtIndex:indexPath.row] objectForKey:@"group_id"]];
            
            
            
            if ([_arrayOfSelectedGroupsID containsObject:myGroupID])
            {
                [cell.selectGroup_checkbox_btn setBackgroundImage:[UIImage imageNamed:@"checkbox_fill"] forState:UIControlStateNormal];
            }
            else
            {
                [cell.selectGroup_checkbox_btn setBackgroundImage:[UIImage imageNamed:@"checkbox_empty"] forState:UIControlStateNormal];
            }
            

        }
        else
        {
            //            without Search
            if (indexPath.row%2==0) {
                cell.selectgroupell_bgView.backgroundColor=ROUTES_CELL_BG_COLOUR1;
            }
            else
                cell.selectgroupell_bgView.backgroundColor=ROUTES_CELL_BG_COLOUR2;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.selectGroup_userName_label.text=[[arratOfSelectGroups objectAtIndex:indexPath.row] objectForKey:@"group_name"];
            cell.selectGroup_userName_label.textColor=APP_YELLOW_COLOR;
            cell.selectGroup_members_label.text=[NSString stringWithFormat:@"%@",[[arratOfSelectGroups objectAtIndex:indexPath.row] objectForKey:@"members_count"]];
            cell.selectGroup_createdBy_label.text=[NSString stringWithFormat:@"Created By %@",[[arratOfSelectGroups objectAtIndex:indexPath.row] objectForKey:@"group_owner"]];
            
            NSString *profile_image_string = [NSString stringWithFormat:@"%@", [[arratOfSelectGroups objectAtIndex:indexPath.row] objectForKey:@"group_image"]];
            NSURL*url1=[NSURL URLWithString:profile_image_string];
            [cell.selectGroup_groupImageView sd_setImageWithURL:url1
                                               placeholderImage:[UIImage imageNamed:@"groups-icon"]
                                                        options:SDWebImageRefreshCached];
            cell.selectGroup_checkbox_btn.tag=indexPath.row;
            
            
            NSString*myGroupID=[NSString stringWithFormat:@"%@",[[arratOfSelectGroups objectAtIndex:indexPath.row] objectForKey:@"group_id"]];
            
            
            
            if ([_arrayOfSelectedGroupsID containsObject:myGroupID])
            {
                [cell.selectGroup_checkbox_btn setBackgroundImage:[UIImage imageNamed:@"checkbox_fill"] forState:UIControlStateNormal];
            }
            else
            {
                [cell.selectGroup_checkbox_btn setBackgroundImage:[UIImage imageNamed:@"checkbox_empty"] forState:UIControlStateNormal];
            }
        }
           return cell;
    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    if (scrollView==_selectFrnds_tableView||scrollView==_selectGroups_tableView) {
        self.selctFrnds_btn_segment.userInteractionEnabled=NO;
        self.selectGroups_btn_segment.userInteractionEnabled=NO;
    }
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    self.selctFrnds_btn_segment.userInteractionEnabled=YES;
    self.selectGroups_btn_segment.userInteractionEnabled=YES;
}
#pragma mark - Search methods
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)aSearchBar {
    self.selectFrndsAndGroups_searchBar.placeholder = @"search";
    self.selectFrndsAndGroups_searchBar.showsCancelButton = YES;
    return YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)SearchBar
{
    if([view_Status_selectFrndsAndGroups isEqualToString:@""]||[view_Status_selectFrndsAndGroups isEqualToString:@"PUBLIC"])
    {
        _selectFrndsAndGroups_searchBar.text = nil;
        self.selectFrndsAndGroups_searchBar.placeholder = @"search";
        self.selectFrndsAndGroups_searchBar.showsCancelButton = NO;
        [_selectFrndsAndGroups_searchBar resignFirstResponder];
        frndsAndGroups_isFiltered = NO;
         [_selectFrnds_tableView setContentOffset:CGPointZero animated:NO];
        [_selectFrnds_tableView reloadData];
    }
    else{
        _selectFrndsAndGroups_searchBar.text = nil;
        self.selectFrndsAndGroups_searchBar.placeholder = @"search";
        self.selectFrndsAndGroups_searchBar.showsCancelButton = NO;
        [_selectFrndsAndGroups_searchBar resignFirstResponder];
        frndsAndGroups_isFiltered = NO;
         [_selectGroups_tableView setContentOffset:CGPointZero animated:NO];
        [_selectGroups_tableView reloadData];
    }
    
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.selectFrndsAndGroups_searchBar resignFirstResponder];
    for (UIView *view in searchBar.subviews)
    {
        for (id subview in view.subviews)
        {
            if ( [subview isKindOfClass:[UIButton class]] )
            {
                [subview setEnabled:YES];
                
                NSLog(@"enableCancelButton");
                return;
            }
        }
    }
    
    [searchBar setShowsCancelButton:YES animated:YES];
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
    if(searchText.length == 0)
    {
        frndsAndGroups_isFiltered = NO;
    }
    else
    {
        if([view_Status_selectFrndsAndGroups isEqualToString:@""]||[view_Status_selectFrndsAndGroups isEqualToString:@"PUBLIC"])
        {
            
            
            
            frndsAndGroups_isFiltered = YES;
            _selectedFrndsFilterdArray = [[NSMutableArray alloc]init];
            
            for(NSDictionary *frnds_Dict in arrayOfSelectFrnds)
            {
                NSString *friend_Name=[frnds_Dict valueForKey:@"name"];
                
                NSRange stringRange = [friend_Name rangeOfString:searchText options:NSCaseInsensitiveSearch];
                
                if(stringRange.location != NSNotFound)
                {
                    [_selectedFrndsFilterdArray addObject:frnds_Dict];
                    
                }
            }
            
            [_selectFrnds_tableView reloadData];
        }
        else
        {
            frndsAndGroups_isFiltered = YES;
            _selectedGroupsFilterdArray = [[NSMutableArray alloc]init];
            
            for(NSDictionary *frnds_Dict1 in arratOfSelectGroups)
            {
                NSString *friend_Name1=[frnds_Dict1 valueForKey:@"group_name"];
                
                NSRange stringRange = [friend_Name1 rangeOfString:searchText options:NSCaseInsensitiveSearch];
                
                if(stringRange.location != NSNotFound)
                {
                    [_selectedGroupsFilterdArray addObject:frnds_Dict1];
                }
            }
            [_selectGroups_tableView reloadData];
        }
    }
}

- (IBAction)onClick_frndsAndGroup_rideGoingBtn:(id)sender {
    
    self.frndsAndGroups_scrollLabel.hidden=YES;
    self.frndsAndGroups_rideGoingBtn.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }

}
@end
