//
//  EditEventViewController.h
//  openroadrides
//
//  Created by SrkIosEbiz on 05/10/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "AppDelegate.h"
#import "APIHelper.h"
#import "AppHelperClass.h"
#import <GooglePlacePicker/GooglePlacePicker.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "CBAutoScrollLabel.h"
@interface EditEventViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,GMSPlacePickerViewControllerDelegate,UITextFieldDelegate>
{
    UIActivityIndicatorView  *activeIndicatore;
    
    UIDatePicker *date,*end_date,*start_time,*end_Time;
    UIImage *cameraImage;
    NSString *location_str;
    double start_latitude,start_longitude,end_latitude,end_longitude;
    NSString *base64String;
    NSData *profileImgData;
    
    NSMutableArray *img_array,*base_64_image_array,*deleted_ids;
    UIButton *add_btn,*subtract_btn;;
    UIView *baseView_placesImages;
    int index,imgIndex,x;
    UIImageView *img;
    NSString *event_id_is,*event_lat,*event_long,*eventStartTimeString,*eventEndTimeString;
    BOOL service_call_running;
    NSString *base64_del_image;
    UIBarButtonItem *item0;

}
@property(strong,nonatomic)NSDictionary *event_details;
@property (weak, nonatomic) IBOutlet UITextField *event_name_TF;
- (IBAction)Event_Adress_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *event_adress_TF;
- (IBAction)event_type_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *event_type_TF;
@property (weak, nonatomic) IBOutlet UIView *event_type_view;
- (IBAction)type_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *assosiated_ride_TF;
@property (weak, nonatomic) IBOutlet UITextField *web_link_TF;
@property (weak, nonatomic) IBOutlet UITextField *start_ride_TF;
@property (weak, nonatomic) IBOutlet UITextField *end_ride_TF;
@property (weak, nonatomic) IBOutlet UITextField *start_time_TF;
@property (weak, nonatomic) IBOutlet UITextField *end_time_TF;

@property (weak, nonatomic) IBOutlet UITextField *description_tf;
@property (weak, nonatomic) IBOutlet UIView *images_base_view;
@property (weak, nonatomic) IBOutlet UIScrollView *images_scroleview;
@property (weak, nonatomic) IBOutlet UIButton *editEvent_rideGoingBtn;
- (IBAction)onClick_editEvent_rideGoinBtn:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint_images_base_view;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *editEvent_scrollLabel;


@end
