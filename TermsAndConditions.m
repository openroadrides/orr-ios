//
//  TermsAndConditions.m
//  openroadrides
//
//  Created by apple on 27/07/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "TermsAndConditions.h"
#import "Constants.h"
#import "APIHelper.h"
@interface TermsAndConditions ()
{
    UIBarButtonItem *item0;
}
@end

@implementation TermsAndConditions

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"TERMS AND CONDITIONS";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    
    self.termsAndConditions_webView.backgroundColor=[UIColor blackColor];
    self.termsAndConditions_webView.delegate=self;
    
    self.terms_scrollLabel.hidden=YES;
    self.terms_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.terms_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.terms_scrollLabel.textColor = [UIColor blackColor];
    self.terms_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.terms_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.terms_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.terms_scrollLabel.scrollSpeed = 60; // pixels per second
    self.terms_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.terms_scrollLabel.fadeLength = 0.f;
    self.terms_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.terms_scrollLabel observeApplicationNotifications];
    
    self.terms_rideGoingBtn.hidden=YES;
    if (appDelegate.ridedashboard_home) {
        self.terms_scrollLabel.hidden=NO;
        self.terms_rideGoingBtn.hidden=NO;
    }
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [leftBtn addTarget:self action:@selector(leftBtn:) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.frame = CGRectMake(0, 0, 25, 25);
    //    [leftBtn setBackgroundImage:[UIImage imageNamed:@"Finalback-arrow.png"] forState:UIControlStateNormal];
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"close_icon"] forState:UIControlStateNormal];
    UIBarButtonItem *leftBackButton=[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    item0=leftBackButton;
    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:item0, nil];

    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    activeIndicatore.center = self.view.center;
    
    activeIndicatore.color = APP_YELLOW_COLOR;
    
    activeIndicatore.hidesWhenStopped = TRUE;
    
    [activeIndicatore startAnimating];
    
    [self.view addSubview:activeIndicatore];
    
    
    [self termsAndConditions];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)leftBtn:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (BOOL)webView:(UIWebView *)wv shouldStartLoadWithRequest:(NSURLRequest *)rq
{
    
    return YES;
}

- (void)webView:(UIWebView *)wv didFailLoadWithError:(NSError *)error
{
    
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
   [activeIndicatore stopAnimating];
}

-(void)termsAndConditions
{
    
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                NSURL *url;
                NSData *postData;
               
                NSString *postString  =[[NSString alloc] initWithFormat:@""];
                NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
//                url=[NSURL URLWithString:[NSString stringWithFormat:@"http://54.70.46.133/admin/termsandconditions"]];
                url=[NSURL URLWithString:[NSString stringWithFormat:@"http://adminbackend.openroadrides.com/termsandconditions"]];
                
                postData = [postString dataUsingEncoding:NSUTF8StringEncoding];
                NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
                [request setURL:url];
                [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
                [request setHTTPMethod:@"POST"];
                [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                [request setHTTPBody:postData];
                
                NSError *error = [[NSError alloc] init];
                NSHTTPURLResponse *response = nil;
                NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                
                NSLog(@"Response code: %ld", (long)[response statusCode]);
                if (urlData) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                            NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
                            NSLog(@"UrlRequest=======%@",urlRequest);
                            [self.termsAndConditions_webView loadRequest:urlRequest];
                            NSLog(@"UrlRequest111111=======%@",urlRequest);
                        
                            [self.termsAndConditions_webView reload];
                        });
                }
                else{
                     [activeIndicatore stopAnimating];
                }
                
            });
}
- (IBAction)onClick_terms_rideGoingBtn:(id)sender {
    
    self.terms_scrollLabel.hidden=YES;
    self.terms_rideGoingBtn.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }

}
@end
