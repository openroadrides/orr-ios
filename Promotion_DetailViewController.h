//
//  Promotion_DetailViewController.h
//  openroadrides
//
//  Created by SrkIosEbiz on 27/09/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APIHelper.h"
#import "Constants.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AppHelperClass.h"
#import "RideDashboard.h"
#import "AppDelegate.h"
#import "CBAutoScrollLabel.h"

@interface Promotion_DetailViewController : UIViewController
{
     UIActivityIndicatorView *activeIndicatore;
}
@property (strong,nonatomic)NSString *promotion_id;
@property (weak, nonatomic) IBOutlet UIImageView *promotion_image;
@property (weak, nonatomic) IBOutlet UILabel *promotion_title;
@property (weak, nonatomic) IBOutlet UILabel *offer_model_name;
@property (weak, nonatomic) IBOutlet UILabel *promotion_end_date;
@property (weak, nonatomic) IBOutlet UILabel *promotion_des;
@property (weak, nonatomic) IBOutlet UIButton *promotion_detail_rideGoingBtn;
@property BOOL promotion_detail_onGoingRide;
- (IBAction)onClick_promotion_detail_rideGoingBtn:(id)sender;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *promotion_detail_scrollLabel;

@end
