//
//  Constants.h
//  Thredz
//
//  Created by Jyothsna on 7/8/16.
//  Copyright © 2016 ebiz. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

//Base URL
//http://openroadridesapi.thinkwithebiz.com/
//#define WEB_SERVICE_URL @"http://54.70.46.133/api/users"
//#define WEB_SERVICE_CMS_URL @"http://54.70.46.133/api/cms"
//#define WEB_SERVICE_RIDES_URL @"http://54.70.46.133/api/rides"
//#define WEB_SERVICE_Routes_URL   @"http://54.70.46.133/api/routes"



//#define WEB_SERVICE_URL @"http://openroadridesapi.thinkwithebiz.com/users"
//#define WEB_SERVICE_CMS_URL @"http://openroadridesapi.thinkwithebiz.com/cms"
//#define WEB_SERVICE_RIDES_URL @"http://openroadridesapi.thinkwithebiz.com/rides"
//#define WEB_SERVICE_Routes_URL   @"http://openroadridesapi.thinkwithebiz.com/routes"


#define WEB_SERVICE_URL @"http://api.openroadrides.com/users"
#define WEB_SERVICE_CMS_URL @"http://api.openroadrides.com/cms"
#define WEB_SERVICE_RIDES_URL @"http://api.openroadrides.com/rides"
#define WEB_SERVICE_Routes_URL @"http://api.openroadrides.com/routes"

//Service Methods

#define WS_NORMAL_SIGNIN_REQUEST @"login"
#define WS_NORMAL_SIGNUP_REQUEST @"registration"
#define WS_SOCIALLOGIN_REQUEST @"social_login"
#define WS_SOCIAL_REGI_REQUEST @"social_registration"
#define WS_SOCIAL_UPDATEPROFILE_REQUEST @"update_user_registartion"
#define WS_NORMAL_UPDATEPROFILE_REQUEST @"update_user_registartion"
#define WS_FORGOTPWD_REQUEST @"forgot_password"
#define WS_NEWS_REQUEST @"get_news_list"
#define WS_NEWSDETAIL_REQUEST @"get_news_details/%@"
#define WS_GROUPS_REQUEST @"userGroups/%@"
#define WS_GROUPSDETAIL_REQUEST @"getGroupDetails/%@/%@"
#define WS_CREATEGROUP_REQUEST @"create_group"
#define WS_NOTIFICATIONS_REQUEST @"getAllNotifications/%@"
#define WS_store_ride_checkin @"store_ride_checkin"
#define WS_EVENTS_REQUEST @"get_events_list"
#define WS_ENDRIDE_REQUEST @"end_ride"
#define WS_ROUTES_List @"get_routes_list"
#define WS_CREATERIDE_REQUEST @"create_ride"
#define Ws_start_imediate_ride_request @"start_immediate_ride"
#define WS_EVENTS_DETAIL_REQUEST @"get_event_details/%@"
#define WS_CREATEEVENT_REQUEST @"create_event"
#define WS_PUBLICFRIENDS_REQUEST @"userFriends/%@/0"
#define WS_MYFRIENDS_REQUEST @"userFriends/%@/1"

#define WS_MYFRIENDS_Edit_REQUEST @"edit_Friends/%@/1"
#define WS_PUBLICFRIENDS_Edit_REQUEST @"edit_Friends/%@/0"

#define WS_ADDAFRIEND_REQUEST @"add_friend"
#define WS_ACCEPT_REJECT_REQUEST @"update_friend_accept_or_reject_status"
#define WS_ACCEPT_REJECT_GROUP_REQUEST @"update_group_accept_or_reject_status"
#define WS_ACCEPT_REJECT_RIDE_REQUEST @"reject_accepted_ride"
#define back_Image @"back_Image"
#define WS_SCHEDULERIDE_REQUEST @"user_scheduled_rides_list/%@"
#define WS_FULLFILLEDRIDE_REQUEST @"user_completed_rides_list/%@"
#define WS_RIDEDETAIL_REQUEST @"get_ride_details"
#define WS_DELETERIDE_REQUEST @"delete_ride/%@/%@"
#define WS_UPDATEUSERLOCATION_REQUEST @"update_user_current_location"
#define Ws_NOTIFYUSERS @"notify_riders_start_ride"
#define Ws_Accepted_Rejected_RIDE @"reject_accepted_ride"
#define WS_NOTIFICATION_READ_UNREAD_REQUEST @"updateNotificationStatus"
#define WS_GETBROWSERIDE_REQUEST @"get_near_by_users"

#define WS_viewUserProfile @"viewUserProfile/%@"
#define WS_Update_User_Profile @"updateUserProfile"

#define WS_ROUTEDETAIL_REQUEST @"get_route_details/%@"
#define WS_Un_friend @"unfriend"
#define WS_EDITRIDE_REQUEST @"update_ride"
#define WS_DASHBOARD_REQUEST @"get_dashboard_data/%@"
#define WS_LEAVE_GROUP_REQUEST @"leaveGroup"
#define WS_UPDATEGROUP_REQUEST @"update_group"
 #define WS_GET_Riders_List @"get_joined_riders/%@"
#define WS_NOTIFICATION_RIDE_ACCEPT_REJECT_REQUEST @"update_ride_invite_accept_or_reject_status"
#define WS_join_ride @"join_ride"
#define WS_PROMOTIONS_REQUEST @"get_near_by_promotions"
#define WS_CREATEMYPOST_REQUEST @"create_post"
#define WS_FEEDBACK_REQUEST @"user_feedback"
#define WS_MARKETPLACELIST_REQUEST @"get_near_by_marketplaces"
#define WS_MARKETPLACE_INTRESTED_REQUEST @"marketplace_interested"
#define WS_MYPOSTSLIST_REQUEST @"get_my_posts/%@/%@"
#define WS_MARKETPLACEDEATIL_REQUEST @"get_marketplace_details/%@/%@"
#define WS_MYPOSTDETAIL_REQUEST @"get_my_post_details/%@"
#define WS_CLOSEPOST_REQUEST @"close_post/%@/%@"
#define WS_get_feature_promotion_REQUEST @"get_feature_promotion_details/%@"
#define WS_get_promotion_details_REQUEST @"get_promotion_details/%@"
#define WS_Update_event_details_REQUEST @"update_event"
#define WS_Delete_event_details_REQUEST @"delete_event/%@"
#define WS_Social_link_REQUEST @"get_socialmedia_links"
#define WS_MARKETPLACE_CATEGORY_REQUEST @"get_all_categories"
#define WS_UPDATEMYPOST_REQUEST @"update_post"


//Appdeleagate
#define UIAPPDELEGATE \
((AppDelegate *)[UIApplication sharedApplication].delegate)
#define APP UIAPPDELEGATE

//Types
#define DEVICE_TYPE @"IOS"
#define REG_TYPE @"social"
#define REG_TYPENORMAL @"normal"
#define User_ID @"User_ID"
#define Device_ID @"Device_ID"
#define UDID @"UDID"
#define Entity_Name @"Locations"

//Responses
#define create_event_sucsess @"Event created successfully."
#define SUCCESS @"Success"
#define USER_EXIST @"User Exists"
#define USEREXISTS @"user exist"
#define FAIL @"Failed"
#define STATUS @"status"
#define REASON @"reason"
#define REGSUCCESS @"Success"
#define SOCIALLOGISUCCESS @"Success"
#define SOCILAREGSUCCESS  @"Success"
#define SOCIALFAIL @"Failed"
#define REGFAILED @"registration_fail"
#define Forgotfail @"Email is not registered yet. Please create an account."
#define DesAlert @"Please enter Description."
#define CREATEGROUP @"please select members"
#define CREATEGROUPMEMBERS @"please select group members"
#define RideDate @"Please select start date."
#define MeetingDate @"Please select meeting date."
#define EventDate @"Select Event Start Date"
#define Event_endDate @"Select Event End Date"
#define Eventtime @"Select Event Start Time"
#define Event_endtime @"Select Event End Time"
#define GROUP_RIDE @"GROUP_RIDE"
#define SOLO_RIDE @"SOLO_RIDE"
#define MEMBER_SCHEDULE_OR_FREE_RIDE @"MEMBER_SCHEDULE_OR_FREE_RIDE"
#define  GETRIDERSCOUNTFAIL @"Unable to get riders,please try again later."
#define NOTIFYRIDERSFAIL @"Unable to notify the other riders."
#define event_deleted @"Event deleted successfully"
//Shared API and Hepler Classes

#define SHARED_API [APIHelper sharedClient]  // shared API Client
#define SHARED_HELPER [AppHelperClass appsharedInstance]

#define Defaults  [NSUserDefaults standardUserDefaults]

#define appDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])
#define UIAPPDELEGATE \
((AppDelegate *)[UIApplication sharedApplication].delegate)

#define Date_Format @"MMM dd, yyyy"

//Screen Sizes

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPHONE_5_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH <= 568.0)


//Colors

#define APP_BLUE_COLOR [UIColor colorWithRed:0.0/255.0 green:189.0/255.0 blue:234.0/255.0 alpha:1.0]
#define APP_YELLOW_COLOR [UIColor colorWithRed:255/255.0 green:222/255.0 blue:0/255.0 alpha:1.0]

#define ROUTES_CELL_BG_COLOUR1    [UIColor colorWithRed:41/255.0 green:41/255.0 blue:41/255.0 alpha:1.0]
#define ROUTES_CELL_BG_COLOUR2    [UIColor colorWithRed:26/255.0 green:26/255.0 blue:26/255.0 alpha:1.0]
// Message Strings
#define FAILMESSAGE @"Invalid details, Please try again later."
#define NetworkError @"Network Not Found"
#define ServerConnection @"Server error occurred, Please try again."
#define UpdateLocation_Sucsess @"User update location success."
#define UpdateLocation_fail @"User update location failed."
// Validation Messagess-------------
#define NotFilled @"Please enter email."
#define TitleAlert @"Please enter title."


// Social Ids
#define kClientId  @"997291683224-9fjqtc7ffp51kkclgbg65k9a81g1cjsb.apps.googleusercontent.com"
#define RandomNumber @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

//Login-------

#define EmailText @"Email should not be empty."
#define CommentText @"Please give your comment."
#define ValidEmailText @"Email should be valid."
#define PasswordText @"Password should not be empty."
#define ValidPasswordText @"Password length should be greater than 5."
#define EmailNotExists @"Email not exist."
#define Loginfail @"You have not register with Open Road Rides, Please signup."
#define Logininvalid @"Please enter valid credentials."
#define Loginfailed @"Login Failed. Please try again."

#define EmptyZipcode @"Please enter Zipcode."
#define CHARACTER_LIMIT_ZIPCODE 5
#define ZipcodeText @"Zipcode should be 5 digits (#####)."
#define UNFRIEND_S @"You have done unfriend successfully."
#define Remove_From_Group @"You have removed successfully."


//Registration ----

#define UserNameText @"Name should not be empty."
#define MobileText @"Mobile number should be 10 digits (###-###-####)."
#define EmptyMobileNumber @"Please enter mobile number."
#define ConnectMobileNumber @"Phone number should not be empty."
#define normalupdate @"You have already registered with Open Road Rides. Do you want to connect with that account?"
#define socialupdate @"You have already registered with Open Road Rides. Do you want to connect with that account?"
#define SignUpfail @"Signup failed. Please try again."
#define groupnamefail @"Group name should not be empty."
#define groupcreated @"Your group is created successfully."
#define groupupdated @"Your group is updated successfully."
#define groupMemberInvited @"Your invite sent successfully."
#define forgotPassSucess @"Email sent successfully."
#define Nonetwork @"Can\'t connect. Check your network and try again."
#define NonetworkError @"Please check your network connection.."
#define ServiceFail @"Can\'t connect. Something went wrong, please try again."
#define NORIDES @"No Rides Available."
#define SEARCHINGRIDERS @"Searching for riders in this location"
#define NORIDERS_LOCATION @"Riders are not in this location"

#define ServiceFail2 @"Something went wrong, please try again."
#define NotificatiosEmpty @"No Data Found."
#define GroupsEmpty @"Please Create Your Group."
#define NO_Routes @"You Don't Have Any Routes"
#define NO_Friends @"No friends found"
#define DOB_EMPTY @"Please enter date of birth."
#define SignUpSuccess @"User registration is successful! Please check your mail to set Password."
#define Check_mail @"Please check your email to reset the password."
#define eventnameempty @"Please Enter Event Name."
#define feedbackSuccessText @"Thanks for your feedback."
#define NUMBERS_ONLY @"1234567890"
#define CHARACTER_LIMIT 10
#define CHARACTER_LIMIT_AGE 2

//Profile-----

#define ProfileUpdate @"Profile Updated Successfully."
#define SelectState @"First select the state."
#define CityText @"Please enter the city."
#define StateText @"Please enter the state."
#define ImageText @"Please take a photo or choose from your photo gallery."
#define ImageText1 @"Please Add minimum two images"
#define NewPasswordText @"Please enter a new password."
#define edit_event_sucsess @"Event updated successfully."
#define event_name @"Please enter event name."
#define event_des @"Please enter description."
#define event_img @"Please add minimum one image."
#define event_adress @"Please enter event address."
#define event_type @"Please enter event type."




#define FIRSTNAMEText @"First Name should not be empty."
#define LASTNAMEText @"Please enter the Last name."
#define EAMILText @"Please enter the Email address."
#define MOBILEText @"Please enter the Mobile Number."
#define ADDRES1Text @"Please enter the address."
#define ADDRES2Text @"Please enter the address2."
#define AddressTextError @"Invalid Address."
#define SelectKeyword @"First select Keyword."
#define GenderText @"Please select the gender."
//Verification

#define VerifycodeText @"Please enter verification code."
#define OTPSuccessText @"Verification sent successfully."
#define Entercorrectcode @"Please enter correct code."
#define Errorinsendingmail @"Error in sending mail."
#define Mailsendsuccess @"Email sent successfully with the reset password link."

//Images & iCONS
#define plus_icon @"plus_icon"
#define empty_star @"empty_save_star"
#define full_Star @"full_Start_save"
#define private_white_icon @"private_white_icon"
#define private_icon @"private_icon"
#define public_icon @"public_icon"
#define public_white_icon @"public_white_icon"
#define public_white_icon @"public_white_icon"
#define close_icon @"close_icon"
#define right_click_icon @"right_click_icon"
#define Ride_Sucsess @"Your ride was successfully completed."
#define UR_IN_RIDE @"Your are in ride, can't go back."
#define Heading_Font @"Antonio-Bold"
#define Heading_Font_Reg @"Antonio-Reguler"
#define Start_Point_Title @"Start Point"
#define End_Point_title @"End Point"
//app termination

#define Terminate @"Do you want to Logout?"

// Amazon s3
#define ACCESS_KEY @"AKIAIQMBOCEIMHNFDO2A"
#define SECRET_KEY @"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"
#define AMAZON_BUCKET_NAME @"openroadrides"
#define Amazon_url_prefix @"https://openroadrides.s3.amazonaws.com/"

// Coredata Entites

#define Rides_Table @"Rides"
#define Routes_Table @"Routes"
#define Add_places_Table @"AddPlace"
#define Waypoints_Table @"Defaultmarkers"
#define Locations_Table @"Locations"

#define all_table_local_rid @"local_rid"

//Ride Table Attributes
#define  ride_table_user_id @"user_id"
#define  ride_table_id @"ride_id"
#define ride_table_name @"ride_name"
#define ride_table_description @"ride_descr"
#define ride_table_type @"ride_type"
#define ride_table_time @"ride_time"
#define ride_table_totlaTime @"ride_total_time"
#define ride_table_dflt_marker_dist @"ride_waypoints"
#define ride_table_avgSpeed @"avgSpeed"
#define ride_table_totalDist @"totalDistance"
#define ride_table_start_date @"ride_start_date"
#define ride_table_start_time @"ride_start_time"
#define ride_table_start_lat @"ride_start_lat"
#define ride_table_start_long @"ride_start_long"
#define ride_table_end_date @"ride_end_date"
#define ride_table_end_time @"ride_end_time"
#define ride_table_end_lat @"ride_end_lat"
#define ride_table_end_long @"ride_end_long"
#define ride_table_riders_count @"rides_count"
#define ride_table_maxSpeed @"maxSpeed"
#define ride_table_randomNumber @"randonNumber"
#define ride_table_rideImags @"ride_images"
#define ride_table_ride_Status @"ride_status"
#define ride_table_userID @"user_id"
#define ride_table_start_adress @"ride_start_adress"
#define ride_table_end_adress @"ride_end_adress"
#define ride_table_ride_owner_id @"ride_owner_user_id"
//Routes Table Attribute

#define route_table_id @"route_id"
#define route_table_name @"route_name"
#define route_table_description @"route_desc"
#define route_table_privacy @"route_privacy"
#define route_table_gpx_url @"gpx_url"
#define route_table_exist_gpx_url @"existing_gpx_url"
#define route_table_gpx_file @"gpx_file"
#define route_table_maxElevation @"maxElevation"
#define route_table_minElevation @"minElevation"
#define route_table_comments @"route_comments"
#define route_table_ratings @"route_ratings"
#define route_table_route_owner_id @"route_owner_user_id"


// Watpoints Table Attributes

#define wayponts_table_existing_rid @"ride_id_marker"
#define wayPoints_table_latitude @"latitudemarker"
#define wayPoints_table_longtitude @"longitudemarker"
#define wayPoints_table_description @"descr_marker"

//Locations Table Atrributes

#define locations_table_existing_rid @"ride_id"
#define locations_table_latitude @"latitude"
#define locations_table_longtitude @"longitude"
#define locations_table_altitude @"elevation"

//AddPlaces Table Attributes


#define addPlaces_table_title @"title"
#define addPlaces_description @"des"
#define addPlaces_table_latitude @"lat"
#define addPlaces_table_longtitude @"longitude"
#define addPlaces_table_time @"time"
#define addPlaces_table_id @"places_id"
#define addPlaces_table_status @"status"
#define addPlaces_table_imgpaths @"img_paths"
#define addPlaces_table_imgCount @"img_count"
#define addPlaces_table_imgUrls @"img_urls"

//Tbale Ride And Addplaces Status
#define table_status_pending @"pending"
#define table_status_complete @"complete"
#define table_status_save @"save"
#define table_status_quit @"quit"

#endif /* Constants_h */
