//
//  APIHelper.m
//  Thredz
//
//  Created by Jyothsna on 7/8/16.
//  Copyright © 2016 ebiz. All rights reserved.
//

#import "APIHelper.h"
#import "Constants.h"

@implementation APIHelper

+ (APIHelper *)sharedClient {
    static APIHelper *sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedClient = [[self alloc] init];
    });
    return sharedClient;
}

-(void)signInRequestsWithParams:(NSDictionary *)params
                    withSuccess:(void (^)(NSDictionary *response))result
                      onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
     ws.baseUrlString = WEB_SERVICE_URL;
    [ws postWithPath:WS_NORMAL_SIGNIN_REQUEST andParams:params withCompletion:^(NSData *theData)
     {
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
         result(resultDict);
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}

-(void)signupRequestsWithParams:(NSDictionary *)params
                    withSuccess:(void (^)(NSDictionary *response))result
                      onfailure:(WebServiceFailure)failure
{
    
    WebService *ws = [[WebService alloc]init];
     ws.baseUrlString = WEB_SERVICE_URL;
    [ws postWithPath:WS_NORMAL_SIGNUP_REQUEST andParams:params withCompletion:^(NSData *theData)
     {
         NSDictionary* resultDict = nil;
         NSError* error = nil;
         resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                      options:kNilOptions error:&error];
         
         result(resultDict);
     }
    failure:^(NSError *theError)
    {
         failure(theError);
     }];
}
-(void)social_loginRequestsWithParams:(NSDictionary *)params
                    withSuccess:(void (^)(NSDictionary *response))result
                      onfailure:(WebServiceFailure)failure
{
    
    WebService *ws = [[WebService alloc]init];
     ws.baseUrlString = WEB_SERVICE_URL;
    [ws postWithPath:WS_SOCIALLOGIN_REQUEST andParams:params withCompletion:^(NSData *theData)
     {
         NSDictionary* resultDict = nil;
         NSError* error = nil;
         resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                      options:kNilOptions error:&error];
         
         result(resultDict);
     }
             failure:^(NSError *theError)
     {
         failure(theError);
     }];
}
-(void)social_registrationRequestsWithParams:(NSDictionary *)params
                          withSuccess:(void (^)(NSDictionary *response))result
                            onfailure:(WebServiceFailure)failure
{
    
    WebService *ws = [[WebService alloc]init];
     ws.baseUrlString = WEB_SERVICE_URL;
    [ws postWithPath:WS_SOCIAL_REGI_REQUEST andParams:params withCompletion:^(NSData *theData)
     {
         NSDictionary* resultDict = nil;
         NSError* error = nil;
         resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                      options:kNilOptions error:&error];
         
         result(resultDict);
     }
             failure:^(NSError *theError)
     {
         failure(theError);
     }];
}
-(void)socialUpdateRequestsWithParams:(NSDictionary *)params
                          withSuccess:(void (^)(NSDictionary *response))result
                            onfailure:(WebServiceFailure)failure
{
    
    WebService *ws = [[WebService alloc]init];
     ws.baseUrlString = WEB_SERVICE_URL;
    [ws postWithPath:WS_SOCIAL_UPDATEPROFILE_REQUEST andParams:params withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        NSLog(@"resultDict%@",resultDict);
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}
-(void)normalUpdateRequestsWithParams:(NSDictionary *)params
                          withSuccess:(void (^)(NSDictionary *response))result
                            onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
     ws.baseUrlString = WEB_SERVICE_URL;
    [ws postWithPath:WS_NORMAL_UPDATEPROFILE_REQUEST andParams:params withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        NSLog(@"resultDict%@",resultDict);
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}

-(void)forgotPwdRequestsWithParams:(NSDictionary *)params
                       withSuccess:(void (^)(NSDictionary *response))result
                         onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
     ws.baseUrlString = WEB_SERVICE_URL;
    [ws postWithPath:WS_FORGOTPWD_REQUEST andParams:params withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}
-(void)feedbackRequestsWithParams:(NSDictionary *)params
                       withSuccess:(void (^)(NSDictionary *response))result
                         onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    ws.baseUrlString = WEB_SERVICE_CMS_URL;
    [ws postWithPath:WS_FEEDBACK_REQUEST andParams:params withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}

-(void)newsRequestsWithParams:(NSDictionary *)params
                       withSuccess:(void (^)(NSDictionary *response))result
                         onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    
    ws.baseUrlString = WEB_SERVICE_CMS_URL;
    
    [ws getWithPath:WS_NEWS_REQUEST andParams:nil withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}

-(void)newsdetailRequestsWithParams:(NSString *)news_id
                  withSuccess:(void (^)(NSDictionary *response))result
                    onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    
    ws.baseUrlString = WEB_SERVICE_CMS_URL;
    
    [ws getWithPath:[NSString stringWithFormat:WS_NEWSDETAIL_REQUEST,news_id] andParams:nil withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
    
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}
-(void)groupsRequestsWithParams:(NSString *)user_token_id
                        withSuccess:(void (^)(NSDictionary *response))result
                          onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    
    [ws getWithPath:[NSString stringWithFormat:WS_GROUPS_REQUEST,user_token_id] andParams:nil withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}


-(void)groupsdetailRequestsWithParams:(NSString *)group_id user_ID:(NSString *)userid
                          withSuccess:(void (^)(NSDictionary *response))result
                            onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    
    [ws getWithPath:[NSString stringWithFormat:WS_GROUPSDETAIL_REQUEST,group_id,userid] andParams:nil withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}

-(void)createGroupRequestsWithParams:(NSDictionary *)params
                       withSuccess:(void (^)(NSDictionary *response))result
                         onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    [ws postWithPath:WS_CREATEGROUP_REQUEST andParams:params withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}


-(void)notificationsRequestsWithParams:(NSString *)user_id
                          withSuccess:(void (^)(NSDictionary *response))result
                            onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    
    ws.baseUrlString = WEB_SERVICE_URL;
    
    [ws getWithPath:[NSString stringWithFormat:WS_NOTIFICATIONS_REQUEST,user_id] andParams:nil withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}
-(void)store_ride_checkin:(NSDictionary *)params
              withSuccess:(void (^)(NSDictionary *response))result
                onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    [ws postWithPath:WS_store_ride_checkin andParams:params withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}

-(void)eventsRequestsWithParams:(NSDictionary *)params
                    withSuccess:(void (^)(NSDictionary *response))result
                      onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    
    ws.baseUrlString = WEB_SERVICE_CMS_URL;
    
    [ws getWithPath:WS_EVENTS_REQUEST andParams:nil withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}
-(void)Routes_Api:(NSDictionary *)params
      withSuccess:(void (^)(NSDictionary *response))result
        onfailure:(WebServiceFailure)failure{
    
    WebService *ws = [[WebService alloc]init];
    ws.baseUrlString = WEB_SERVICE_Routes_URL;
    [ws postWithPath:WS_ROUTES_List andParams:params withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}

//public friends And MyFriends api
-(void)publicFriendsRequest:(NSString *)userid public_myfriends:(NSString *)Public_OR_myfriends_Service
      withSuccess:(void (^)(NSDictionary *response))result
        onfailure:(WebServiceFailure)failure{

//    WebService *ws = [[WebService alloc]init];
//    
//    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
//    
//    [ws getWithPath:WS_PUBLICFRIENDS_REQUEST andParams:nil withCompletion:^(NSData *theData) {
//        
//        NSDictionary* resultDict = nil;
//        NSError* error = nil;
//        resultDict = [NSJSONSerialization JSONObjectWithData:theData
//                                                     options:kNilOptions error:&error];
//        
//        
//        result(resultDict);
//        
//    } failure:^(NSError *theError) {
//        failure(theError);
//    }];
    
    
    WebService *ws = [[WebService alloc]init];
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    [ws getWithPath:[NSString stringWithFormat:Public_OR_myfriends_Service,userid]  andParams:nil withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    

    
}
//-(void)add_a_friend_Request:(NSString *)userid friendsID:(NSString *)Public_OR_myfriends_Service 
//                withSuccess:(void (^)(NSDictionary *response))result
//                  onfailure:(WebServiceFailure)failure{
//    
//    
//    WebService *ws = [[WebService alloc]init];
//    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
//    [ws getWithPath:[NSString stringWithFormat:Public_OR_myfriends_Service,userid]  andParams:nil withCompletion:^(NSData *theData) {
//        
//        NSDictionary* resultDict = nil;
//        NSError* error = nil;
//        resultDict = [NSJSONSerialization JSONObjectWithData:theData
//                                                     options:kNilOptions error:&error];
//        
//        
//        
//        result(resultDict);
//        
//    } failure:^(NSError *theError) {
//        failure(theError);
//    }];
//    
//    
//    
//}


-(void)add_a_friend_Request1:(NSDictionary *)params
              withSuccess:(void (^)(NSDictionary *response))result
                onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    [ws postWithPath:WS_ADDAFRIEND_REQUEST andParams:params withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}
-(void)accept_reject_frnd_request:(NSDictionary *)params
                 withSuccess:(void (^)(NSDictionary *response))result
                   onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    [ws postWithPath:WS_ACCEPT_REJECT_REQUEST andParams:params withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}


-(void)endRideRequestsWithParams:(NSDictionary *)params
                     withSuccess:(void (^)(NSDictionary *response))result
                       onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    [ws postWithPath:WS_ENDRIDE_REQUEST andParams:params withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}
-(void)createRideRequestsWithParams:(NSDictionary *)params
                        withSuccess:(void (^)(NSDictionary *response))result
                          onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    [ws postWithPath:WS_CREATERIDE_REQUEST andParams:params withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}
//start_immediate_ride
-(void)startimediateRideRequestsWithParams:(NSDictionary *)params
                        withSuccess:(void (^)(NSDictionary *response))result
                          onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    [ws postWithPath:Ws_start_imediate_ride_request andParams:params withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}

-(void)eventsDetailRequestsWithParams:(NSString *)event_id
                          withSuccess:(void (^)(NSDictionary *response))result
                            onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    
    ws.baseUrlString = WEB_SERVICE_CMS_URL;
    
    [ws getWithPath:[NSString stringWithFormat: WS_EVENTS_DETAIL_REQUEST,event_id] andParams:nil withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}

-(void)createEventRequestsWithParams:(NSDictionary *)params
                         withSuccess:(void (^)(NSDictionary *response))result
                           onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    ws.baseUrlString = WEB_SERVICE_CMS_URL;
    [ws postWithPath:WS_CREATEEVENT_REQUEST andParams:params withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}
-(void)scheduleRideRequestsWithParams:(NSString *)user_token_id
                    withSuccess:(void (^)(NSDictionary *response))result
                      onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    
    [ws getWithPath:[NSString stringWithFormat:WS_SCHEDULERIDE_REQUEST,user_token_id] andParams:nil withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}

-(void)selectedFriendsAndGroupsRequest:(NSString *)userid public_myfriends:(NSString *)Friends_OR_groups_Service
                withSuccess:(void (^)(NSDictionary *response))result
                  onfailure:(WebServiceFailure)failure{
    
    WebService *ws = [[WebService alloc]init];
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    [ws getWithPath:[NSString stringWithFormat:Friends_OR_groups_Service,userid]  andParams:nil withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
    
    
}

-(void)myRidesRequest:(NSString *)userid scheduled_fullFilled:(NSString *)Scheduled_OR_Fullfilled_Service
                           withSuccess:(void (^)(NSDictionary *response))result
                             onfailure:(WebServiceFailure)failure{
    
    WebService *ws = [[WebService alloc]init];
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    [ws getWithPath:[NSString stringWithFormat:Scheduled_OR_Fullfilled_Service,userid]  andParams:nil withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
    
    
}
-(void)rideDetailRequestsWithParams:(NSDictionary *)params
                          withSuccess:(void (^)(NSDictionary *response))result
                            onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    
    [ws postWithPath:WS_RIDEDETAIL_REQUEST andParams:params withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}
//http://54.70.46.133/api/rides/delete_ride/id/id

-(void)deleteRideRequestsWithParams:(NSString *)ride_id user_ID:(NSString *)userid
                        withSuccess:(void (^)(NSDictionary *response))result
                          onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    
    [ws getWithPath:[NSString stringWithFormat:WS_DELETERIDE_REQUEST,ride_id,userid] andParams:nil withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}

-(void)updateUserlocationRequestsWithParams:(NSDictionary *)params
                    withSuccess:(void (^)(NSDictionary *response))result
                      onfailure:(WebServiceFailure)failure
{
    
    WebService *ws = [[WebService alloc]init];
    ws.baseUrlString = WEB_SERVICE_URL;
    [ws postWithPath:WS_UPDATEUSERLOCATION_REQUEST andParams:params withCompletion:^(NSData *theData)
     {
         NSDictionary* resultDict = nil;
         NSError* error = nil;
         resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                      options:kNilOptions error:&error];
         
         result(resultDict);
     }
             failure:^(NSError *theError)
     {
         failure(theError);
     }];
}
-(void)DisplayUserProfile:(NSString *)user_Id
              withSuccess:(void (^)(NSDictionary *response))result
                onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    
    ws.baseUrlString = WEB_SERVICE_URL;
    
    [ws getWithPath:[NSString stringWithFormat: WS_viewUserProfile,user_Id] andParams:nil withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}
-(void)Update_User_Profile:(NSDictionary *)params
               withSuccess:(void (^)(NSDictionary *response))result
                 onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    ws.baseUrlString = WEB_SERVICE_URL;
    [ws postWithPath:WS_Update_User_Profile andParams:params withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}
-(void)routedetailRequestsWithParams:(NSString *)Route_id
                         withSuccess:(void (^)(NSDictionary *response))result
                           onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    
    ws.baseUrlString = WEB_SERVICE_Routes_URL;
    
    [ws getWithPath:[NSString stringWithFormat:WS_ROUTEDETAIL_REQUEST,Route_id] andParams:nil withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}
-(void)Un_Frined_service:(NSDictionary *)params
               withSuccess:(void (^)(NSDictionary *response))result
                 onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    [ws postWithPath:WS_Un_friend andParams:params withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}
//http://54.70.46.133/api/rides/notify_riders_start_ride

-(void)notify_users_RequestsWithParams:(NSDictionary *)params
                    withSuccess:(void (^)(NSDictionary *response))result
                      onfailure:(WebServiceFailure)failure
{
    
    WebService *ws = [[WebService alloc]init];
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    [ws postWithPath:Ws_NOTIFYUSERS andParams:params withCompletion:^(NSData *theData)
     {
         NSDictionary* resultDict = nil;
         NSError* error = nil;
         resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                      options:kNilOptions error:&error];
         
         result(resultDict);
     }
             failure:^(NSError *theError)
     {
         failure(theError);
     }];
}
-(void)editRideRequestsWithParams:(NSDictionary *)params
                      withSuccess:(void (^)(NSDictionary *response))result
                        onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    [ws postWithPath:WS_EDITRIDE_REQUEST andParams:params withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}

-(void)leave_ride_RequestsWithParams:(NSDictionary *)params
                           withSuccess:(void (^)(NSDictionary *response))result
                             onfailure:(WebServiceFailure)failure
{
    
    WebService *ws = [[WebService alloc]init];
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    [ws postWithPath:Ws_Accepted_Rejected_RIDE andParams:params withCompletion:^(NSData *theData)
     {
         NSDictionary* resultDict = nil;
         NSError* error = nil;
         resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                      options:kNilOptions error:&error];
         
         result(resultDict);
     }
             failure:^(NSError *theError)
     {
         failure(theError);
     }];
}
-(void)dashboardRequestsWithParams:(NSString *)user_id

                       withSuccess:(void (^)(NSDictionary *response))result

                         onfailure:(WebServiceFailure)failure

{
    WebService *ws = [[WebService alloc]init];
        ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    
    [ws getWithPath:[NSString stringWithFormat: WS_DASHBOARD_REQUEST,user_id] andParams:nil withCompletion:^(NSData *theData) {
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                          options:kNilOptions error:&error];
         result(resultDict);
        
    } failure:^(NSError *theError) {
        
        failure(theError);
        
    }];
  
}
-(void)get_Riders_List:(NSString *)ride_id

                       withSuccess:(void (^)(NSDictionary *response))result

                         onfailure:(WebServiceFailure)failure

{
    WebService *ws = [[WebService alloc]init];
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    
    [ws getWithPath:[NSString stringWithFormat: WS_GET_Riders_List,ride_id] andParams:nil withCompletion:^(NSData *theData) {
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        result(resultDict);
        
    } failure:^(NSError *theError) {
        
        failure(theError);
        
    }];
    
}
-(void)UpdateGroupRequestsWithParams:(NSDictionary *)params
                         withSuccess:(void (^)(NSDictionary *response))result
                           onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    [ws postWithPath:WS_UPDATEGROUP_REQUEST andParams:params withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}

-(void)leaveGroupRequestsWithParams:(NSDictionary *)params
                        withSuccess:(void (^)(NSDictionary *response))result
                          onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    [ws postWithPath:WS_LEAVE_GROUP_REQUEST andParams:params withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        NSLog(@"resultDict%@",resultDict);
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}
-(void)accept_reject_group_request:(NSDictionary *)params

                       withSuccess:(void (^)(NSDictionary *response))result

                         onfailure:(WebServiceFailure)failure

{
    
    WebService *ws = [[WebService alloc]init];
    
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    
    [ws postWithPath:WS_ACCEPT_REJECT_GROUP_REQUEST andParams:params withCompletion:^(NSData *theData) {
            NSDictionary* resultDict = nil;
        
        NSError* error = nil;
        
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                      
                                                     options:kNilOptions error:&error];
              result(resultDict);
           } failure:^(NSError *theError) {
        failure(theError);
    }];
 
}

//http://54.70.46.133/api/rides/reject_accepted_ride

-(void)accept_reject_ride_request:(NSDictionary *)params

                      withSuccess:(void (^)(NSDictionary *response))result

                        onfailure:(WebServiceFailure)failure

{
    
    WebService *ws = [[WebService alloc]init];
    
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    
    [ws postWithPath:WS_ACCEPT_REJECT_RIDE_REQUEST andParams:params withCompletion:^(NSData *theData) {
        
             NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                      
                                                     options:kNilOptions error:&error];
             result(resultDict);
        
    } failure:^(NSError *theError) {
        
        failure(theError);
        
    }];
  
}
-(void)notification_read_unread_status_RequestsWithParams:(NSDictionary *)params

                                              withSuccess:(void (^)(NSDictionary *response))result

                                                onfailure:(WebServiceFailure)failure

{
    
    WebService *ws = [[WebService alloc]init];
    
    ws.baseUrlString = WEB_SERVICE_URL;
    
    [ws postWithPath:WS_NOTIFICATION_READ_UNREAD_REQUEST andParams:params withCompletion:^(NSData *theData) {
        
        
        
        NSDictionary* resultDict = nil;
        
        NSError* error = nil;
        
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                      
                                                     options:kNilOptions error:&error];
        
        result(resultDict);
        
        
        
    } failure:^(NSError *theError) {
        
        failure(theError);
        
    }];
    
}
-(void)accept_reject_notification_ride_request:(NSDictionary *)params

                                   withSuccess:(void (^)(NSDictionary *response))result

                                     onfailure:(WebServiceFailure)failure



{
    
    WebService *ws = [[WebService alloc]init];
    
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    
    [ws postWithPath:WS_NOTIFICATION_RIDE_ACCEPT_REJECT_REQUEST andParams:params withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        
        NSError* error = nil;
        
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                      
                      
                      
                                                     options:kNilOptions error:&error];
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        
        failure(theError);
        
    }];
    
}
-(void)Join_RIdes:(NSDictionary *)params

                                   withSuccess:(void (^)(NSDictionary *response))result

                                     onfailure:(WebServiceFailure)failure



{
    
    WebService *ws = [[WebService alloc]init];
    
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    
    [ws postWithPath:WS_join_ride andParams:params withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        
        NSError* error = nil;
        
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                      
                      
                      
                                                     options:kNilOptions error:&error];
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        
        failure(theError);
        
    }];
    
}

-(void)browseRidersWithParams:(NSDictionary *)params
                  withSuccess:(void (^)(NSDictionary *response))result
                    onfailure:(WebServiceFailure)failure
{
    
    WebService *ws = [[WebService alloc]init];
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    [ws postWithPath:WS_GETBROWSERIDE_REQUEST andParams:params withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}
-(void)promotionsWithParams:(NSDictionary *)params
                withSuccess:(void (^)(NSDictionary *response))result
                  onfailure:(WebServiceFailure)failure
{
    
    WebService *ws = [[WebService alloc]init];
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    [ws postWithPath:WS_PROMOTIONS_REQUEST andParams:params withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}
-(void)marketPlaceListRequestsWithParams:(NSDictionary *)params
                       withSuccess:(void (^)(NSDictionary *response))result
                         onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    [ws postWithPath:WS_MARKETPLACELIST_REQUEST andParams:params withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}
-(void)myPosts_Request_List:(NSString *)user_id  cat_ID:(NSString *)catID
           withSuccess:(void (^)(NSDictionary *response))result
             onfailure:(WebServiceFailure)failure

{
    WebService *ws = [[WebService alloc]init];
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    
    [ws getWithPath:[NSString stringWithFormat: WS_MYPOSTSLIST_REQUEST,user_id,catID] andParams:nil withCompletion:^(NSData *theData) {
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        result(resultDict);
        
    } failure:^(NSError *theError) {
        
        failure(theError);
        
    }];
    
}
-(void)getMarketPlace_Details_Request:(NSString *)post_id user_ID:(NSString *)userid

                withSuccess:(void (^)(NSDictionary *response))result

                  onfailure:(WebServiceFailure)failure

{
    WebService *ws = [[WebService alloc]init];
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    
    [ws getWithPath:[NSString stringWithFormat: WS_MARKETPLACEDEATIL_REQUEST,post_id,userid] andParams:nil withCompletion:^(NSData *theData) {
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        result(resultDict);
        
    } failure:^(NSError *theError) {
        
        failure(theError);
        
    }];
    
}
-(void)close_Post_Request:(NSString *)post_id user_ID:(NSString *)userid

                          withSuccess:(void (^)(NSDictionary *response))result

                            onfailure:(WebServiceFailure)failure

{
    WebService *ws = [[WebService alloc]init];
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    
    [ws getWithPath:[NSString stringWithFormat: WS_CLOSEPOST_REQUEST,post_id,userid] andParams:nil withCompletion:^(NSData *theData) {
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        result(resultDict);
        
    } failure:^(NSError *theError) {
        
        failure(theError);
        
    }];
    
}

-(void)getMyPost_Details_Request:(NSString *)post_id

                          withSuccess:(void (^)(NSDictionary *response))result

                            onfailure:(WebServiceFailure)failure

{
    WebService *ws = [[WebService alloc]init];
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    
    [ws getWithPath:[NSString stringWithFormat: WS_MYPOSTDETAIL_REQUEST,post_id] andParams:nil withCompletion:^(NSData *theData) {
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        result(resultDict);
        
    } failure:^(NSError *theError) {
        
        failure(theError);
        
    }];
    
}

-(void)marketPlace_intrested_RequestsWithParams:(NSDictionary *)params
                             withSuccess:(void (^)(NSDictionary *response))result
                               onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    [ws postWithPath:WS_MARKETPLACE_INTRESTED_REQUEST andParams:params withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}
-(void)createMyPostRequestWithParams:(NSDictionary *)params
                withSuccess:(void (^)(NSDictionary *response))result
                  onfailure:(WebServiceFailure)failure
{
    
    WebService *ws = [[WebService alloc]init];
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    [ws postWithPath:WS_CREATEMYPOST_REQUEST andParams:params withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}

-(void)get_feature_promotion:(NSString *)promotion_id
                 withSuccess:(void (^)(NSDictionary *response))result
                   onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    
    [ws getWithPath:[NSString stringWithFormat:WS_get_feature_promotion_REQUEST,promotion_id] andParams:nil withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}
-(void)get_promotion_detail_page:(NSString *)promotion_id
                     withSuccess:(void (^)(NSDictionary *response))result
                       onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    
    [ws getWithPath:[NSString stringWithFormat:WS_get_promotion_details_REQUEST,promotion_id] andParams:nil withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}
-(void)Edit_EventRequestsWithParams:(NSDictionary *)params
                        withSuccess:(void (^)(NSDictionary *response))result
                          onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    ws.baseUrlString = WEB_SERVICE_CMS_URL;
    [ws postWithPath:WS_Update_event_details_REQUEST andParams:params withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}
-(void)get_Delete_event_page:(NSString *)event_id
                     withSuccess:(void (^)(NSDictionary *response))result
                       onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    
    ws.baseUrlString = WEB_SERVICE_CMS_URL;
    
    [ws getWithPath:[NSString stringWithFormat:WS_Delete_event_details_REQUEST,event_id] andParams:nil withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}
-(void)get_Social_Links_page:(NSString *)not_requaired
                 withSuccess:(void (^)(NSDictionary *response))result
                   onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    
    ws.baseUrlString = WEB_SERVICE_CMS_URL;
    
    [ws getWithPath:WS_Social_link_REQUEST andParams:nil withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}
-(void)get_marketPlace_Category_Type:(NSString *)not_requaired
                 withSuccess:(void (^)(NSDictionary *response))result
                   onfailure:(WebServiceFailure)failure
{
    WebService *ws = [[WebService alloc]init];
    
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    
    [ws getWithPath:WS_MARKETPLACE_CATEGORY_REQUEST andParams:nil withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}
-(void)UpdateMyPostRequestWithParams:(NSDictionary *)params
                         withSuccess:(void (^)(NSDictionary *response))result
                           onfailure:(WebServiceFailure)failure
{
    
    WebService *ws = [[WebService alloc]init];
    ws.baseUrlString = WEB_SERVICE_RIDES_URL;
    [ws postWithPath:WS_UPDATEMYPOST_REQUEST andParams:params withCompletion:^(NSData *theData) {
        
        NSDictionary* resultDict = nil;
        NSError* error = nil;
        resultDict = [NSJSONSerialization JSONObjectWithData:theData
                                                     options:kNilOptions error:&error];
        
        
        
        result(resultDict);
        
    } failure:^(NSError *theError) {
        failure(theError);
    }];
    
}
@end
