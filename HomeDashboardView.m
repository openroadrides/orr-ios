//
//  HomeDashboardView.m
//  openroadrides
//
//  Created by apple on 22/09/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "HomeDashboardView.h"
#import "MPCoachMarks.h"

@interface HomeDashboardView ()
{
    CGRect coachmark1;
}

@end

@implementation HomeDashboardView

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.title = @"HOME";
//    [self.navigationController.navigationBar setTitleTextAttributes:
//     @{NSForegroundColorAttributeName:[UIColor blackColor],
//       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
//    
//    
//    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
//    [leftBtn addTarget:self action:@selector(back_Action) forControlEvents:UIControlEventTouchUpInside];
//    leftBtn.frame = CGRectMake(0, 0, 25, 25);
//    [leftBtn setBackgroundImage:[UIImage imageNamed:@"back_Image"] forState:UIControlStateNormal];
//    UIBarButtonItem *leftBackButton=[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
//    self.navigationItem.leftBarButtonItems=[NSArray arrayWithObjects:leftBackButton, nil];

//     [[self navigationController] setNavigationBarHidden:YES animated:NO];
    [self.navigationController.navigationBar setHidden:YES];
    [self.navigationController.navigationBar setBarTintColor:APP_YELLOW_COLOR];
    [self.navigationController.navigationBar setTranslucent:NO];
    self.homeDashboard_collectionView.dataSource=self;
    self.homeDashboard_collectionView.delegate=self;
    _arrayOfHomeDashboardItems=[[NSMutableArray alloc]initWithObjects:@"Rides",@"Route",@"Riders",@"Promotions",@"Events",@"MarketPlace",@"News/Laws",@"Feedback",@"Social", nil];
    
    indicaterview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    activeIndicatore.backgroundColor=[UIColor clearColor];
    activeIndicatore = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.frame = CGRectMake(0.0, 0.0, 80, 80);
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    [indicaterview addSubview:activeIndicatore];
    [self.view addSubview:indicaterview];
    [indicaterview bringSubviewToFront:self.view];
    [activeIndicatore startAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    indicaterview.hidden=YES;
    
    [self.view layoutIfNeeded];
    self.rideGoingBtn.hidden=YES;
    self.homeDashboard_scrollLabel.hidden=YES;
    self.homeDashboard_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.homeDashboard_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.homeDashboard_scrollLabel.textColor = [UIColor blackColor];
    self.homeDashboard_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.homeDashboard_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.homeDashboard_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.homeDashboard_scrollLabel.scrollSpeed = 60; // pixels per second
    self.homeDashboard_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.homeDashboard_scrollLabel.fadeLength = 0.f;
    self.homeDashboard_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.homeDashboard_scrollLabel observeApplicationNotifications];

    
    NSArray *rides_pending=[self get_pending_Rides_From_DB];
    if (rides_pending.count>0) {
        Rides_object=[rides_pending objectAtIndex:0];
        pending_Ride_id=Rides_object.local_rid;
        if ([Rides_object.ride_status isEqualToString:table_status_pending]) {
            if (!appDelegate.ridedashboard_home) {
                  [self Show_Ride_Alert];
            }
            else
            {
                    self.rideGoingBtn.hidden=NO;
                    self.homeDashboard_scrollLabel.hidden=NO;
               
            }
          
        }
    }
    else
    {
        if ([appDelegate.is_Running_Background_Sync isEqualToString:@"NO"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SaveRide_Service" object:self]; //in appdelegate
        }
    }
    
    
    //   When ride was not inPogress(After Completed,Quite Ride,befor started the ride)
    if (![[Defaults valueForKey:@"RIDE_STATUS"]isEqualToString:@"RIDE_STARTED" ]) {
        
        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
        
        for(UIViewController *tempVC in navigationArray)
        {
            if([tempVC isKindOfClass:[RideDashboard class]])
            {
                [navigationArray removeObject:tempVC];
                break;
            }
        }
        self.navigationController.viewControllers=navigationArray;
    }
    

   
}

-(BOOL)check_Location_On_Or_Off{
    if ([CLLocationManager locationServicesEnabled]){
        NSLog(@"Location Services Enabled");
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
        {
            UIAlertController *alertview = [UIAlertController alertControllerWithTitle:@"LOCATION" message:@"Turn on your App Location" preferredStyle:UIAlertControllerStyleAlert];
            
            [alertview addAction:[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root=Privacy:Location_Services"]];
            }]];
            [self presentViewController:alertview animated:YES completion:nil];
            return NO;
        }
        else{
            NSLog(@"Location Services Enabled");
//            [self user_update_location];
            return YES;
        }
    }
    else{
        UIAlertController *alertview = [UIAlertController alertControllerWithTitle:@"LOCATION" message:@"Turn ON Your Location" preferredStyle:UIAlertControllerStyleAlert];
        
        [alertview addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
            // Cancel button tappped.
            [self dismissViewControllerAnimated:YES completion:^{
            }];
            
        }]];
        [alertview addAction:[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
            
            // Distructive button tapped.
            //                [self dismissViewControllerAnimated:YES completion:^{
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root=Privacy:Location_Services"]];
            //                }];
        }]];
        
        [self presentViewController:alertview animated:YES completion:nil];
        
        return NO;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)back_Action{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
//    self.navigationController.navigationBar.hidden=YES;
    
    [self.navigationController.navigationBar setHidden:YES];
    if (appDelegate.app_current_lattitude==0.0 && appDelegate.app_current_longitude ==0.0) {
        
    }
    else
    {
        if (appDelegate.ridedashboard_home) {
            
        }
        else
        {
             [self user_update_location];
        }
    }
}
//-(void)viewDidDisappear:(BOOL)animated
//{
//    self.navigationController.navigationBar.hidden=NO;
//}
-(void)viewWillDisappear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden=NO;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - UserCurrentLocation
-(void)user_update_location

{
    
    if (![SHARED_HELPER checkIfisInternetAvailable]) {
        return;
    }
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:[Defaults valueForKey:User_ID] forKey:@"user_id"];
    [dict setObject:[NSNumber numberWithDouble:appDelegate.app_current_lattitude] forKey:@"current_latitude"];
    [dict setObject:[NSNumber numberWithDouble:appDelegate.app_current_longitude] forKey:@"current_longitude"];
    [dict setObject:@"No" forKey:@"is_show"];
    
    NSLog(@"Update user location : %@",dict);
    
    [SHARED_API updateUserlocationRequestsWithParams:dict withSuccess:^(NSDictionary *response) {
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if([[response objectForKey:STATUS] isEqualToString:SUCCESS])
            {
                NSLog(@"Update user response : %@",response);
                
            }
            else
            {
                if ([[response objectForKey:STATUS] isEqualToString:FAIL])
                {
                    NSLog(@"Update user response : %@",response);
                }
            }
        });
        
    } onfailure:^(NSError *theError) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
        
        });
        
    }];
    
}

#pragma mark - CollectionView
-(CGSize)collectionView:(UICollectionView *)collectionView
                 layout:(UICollectionViewLayout *)collectionViewLayout
 sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(SCREEN_WIDTH/3-10 , 100);
    
}
//

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    if (SCREEN_HEIGHT==480) {
        return CGSizeMake(SCREEN_WIDTH/3-10 , 100);
    }
    return CGSizeMake(0,0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_arrayOfHomeDashboardItems count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    HomeDashboardCollectionCell *cell = [self.homeDashboard_collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    if (indexPath.row==0) {
        cell.dashboardItem_imageview.image=[UIImage imageNamed:@"Home_ride"];
    }
    else if (indexPath.row ==1)
    {
       cell.dashboardItem_imageview.image=[UIImage imageNamed:@"home_route"];
    }
    else if (indexPath.row ==2)
    {
        cell.dashboardItem_imageview.image=[UIImage imageNamed:@"home_riders"];
    }
    else if (indexPath.row ==3)
    {
        cell.dashboardItem_imageview.image=[UIImage imageNamed:@"home_promotions"];
    }
    else if (indexPath.row ==4)
    {
       cell.dashboardItem_imageview.image=[UIImage imageNamed:@"home_events"];
    }
    else if (indexPath.row ==5)
    {
        cell.dashboardItem_imageview.image=[UIImage imageNamed:@"home_marketplace"];
    }
    else if (indexPath.row ==6)
    {
        cell.dashboardItem_imageview.image=[UIImage imageNamed:@"home_news"];
    }
    else if (indexPath.row ==7)
    {
       cell.dashboardItem_imageview.image=[UIImage imageNamed:@"home_feedback"];
    }else if (indexPath.row ==8)
    {
        cell.dashboardItem_imageview.image=[UIImage imageNamed:@"home_socail"];
        // Setup coach marks
        
        UICollectionViewLayoutAttributes *attributes = [_homeDashboard_collectionView layoutAttributesForItemAtIndexPath:indexPath];
        CGRect cellFrameInSuperview = [_homeDashboard_collectionView convertRect:attributes.frame toView:[_homeDashboard_collectionView superview]];
        
        
        coachmark1 = cellFrameInSuperview;
        
        //Display Annotation
        // Show coach marks
        BOOL coachMarksShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"MPCoachMarksShown_SocialShare"];
        if (coachMarksShown == NO) {
            // Don't show again
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MPCoachMarksShown_SocialShare"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            // Show coach marks
            [self showAnnotation];
        }
        
    }
    cell.dashboardHometitle_label.text=[NSString stringWithFormat:@"%@",[_arrayOfHomeDashboardItems objectAtIndex:indexPath.row]];
    
    
    return cell;
}

#pragma mark - Annotations
-(void)showAnnotation
{
    [self.view layoutIfNeeded];
    
    
    // Setup coach marks
    NSArray *coachMarks = @[
                            @{
                                @"rect": [NSValue valueWithCGRect:coachmark1],
                                @"caption": @"Share through different social apps",
                                @"shape": [NSNumber numberWithInteger:SHAPE_SQUARE],
                                @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
                                @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_CENTER]
                                //@"showArrow":[NSNumber numberWithBool:YES]
                                },
                            ];
    
    
    MPCoachMarks *coachMarksView = [[MPCoachMarks alloc] initWithFrame:self.navigationController.view.bounds coachMarks:coachMarks];
    [self.navigationController.view addSubview:coachMarksView];
    //[[UIApplication sharedApplication].keyWindow addSubview:coachMarksView];
    [coachMarksView start];
    
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
//     NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
//    
//    for(UIViewController *tempVC in navigationArray)
//    {
//        if([tempVC isKindOfClass:[DashboardViewController class]])
//        {
//             [navigationArray removeObject:tempVC];
//        }
//        else if([tempVC isKindOfClass:[RideDashboard class]])
//        {
//           
//        }
//    }
//     self.navigationController.viewControllers =navigationArray;
    
    
    if (indexPath.row==0) {
//        RidesController*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"RidesController"];
//        [self.navigationController pushViewController:controller animated:YES];
        if ([self check_Location_On_Or_Off])
        {
            DashboardViewController *dashboard = [self.storyboard instantiateViewControllerWithIdentifier:@"DashboardViewController"];
            [self.navigationController pushViewController:dashboard animated:YES];
        }
    }
    else if (indexPath.row ==1)
    {
        RoutesVC *cntlr = [self.storyboard instantiateViewControllerWithIdentifier:@"RoutesVC"];
        [self.navigationController pushViewController:cntlr animated:NO];
    }
    else if (indexPath.row ==2)
    {
        BrowseRidersViewController *cntrl = [self.storyboard instantiateViewControllerWithIdentifier:@"BrowseRidersViewController"];
        [self.navigationController pushViewController:cntrl animated:NO];
    }
    else if (indexPath.row ==3)
    {
        Promotions*promotions = [self.storyboard instantiateViewControllerWithIdentifier:@"Promotions"];
        [self.navigationController pushViewController:promotions animated:NO];
      
    }
    else if (indexPath.row ==4)
    {
        EventsViewController *cntlr  = [self.storyboard instantiateViewControllerWithIdentifier:@"EventsViewController"];
        [self.navigationController pushViewController:cntlr animated:NO];
        
    }
    else if (indexPath.row ==5)
    {
        if ([self check_Location_On_Or_Off])
        {
        MarketPlace*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"MarketPlace"];
        [self.navigationController pushViewController:controller animated:NO];
        }
    }
    else if (indexPath.row ==6)
    {
        NewsViewController *cntlr = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsViewController"];
        [self.navigationController pushViewController:cntlr animated:YES];
    }
    else if (indexPath.row ==7)
    {
        Feedback *cntrl = [self.storyboard instantiateViewControllerWithIdentifier:@"Feedback"];
        [self.navigationController pushViewController:cntrl animated:NO];
    }else if (indexPath.row ==8)
    {
        [self Social_Link_Service];
    }

}
#pragma mark - Social_Link_Service
-(void)Social_Link_Service{
    
    if ([SHARED_HELPER checkIfisInternetAvailable]) {
       
    }
    else{
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }
    
    indicaterview.hidden=NO;
    
    [SHARED_API get_Social_Links_page:@"" withSuccess:^(NSDictionary *response) {
        
        if ([[response valueForKey:STATUS]isEqualToString:SUCCESS]) {
            
            
             indicaterview.hidden=YES;
            
            NSDictionary *link_dict_is=[[response valueForKey:@"socialmedia_links"] objectAtIndex:0];
            
            NSString *link_is=[link_dict_is valueForKey:@"facebook_url"];
            links_array=[[NSMutableArray alloc]init];
           
            
            if ( [link_is isEqual:[NSNull null]]||[link_is isEqualToString:@""]) {
                
            }
            else{
                 NSDictionary *detail_dict=@{@"key":@"fb",@"link":link_is};
                [links_array addObject:detail_dict];
            }
            
            link_is=[link_dict_is valueForKey:@"twitter_link"];
            if ( [link_is isEqual:[NSNull null]]||[link_is isEqualToString:@""]) {
                
            }
            else{
                NSDictionary *detail_dict=@{@"key":@"twitter",@"link":link_is};
                [links_array addObject:detail_dict];
            }
            
            link_is=[link_dict_is valueForKey:@"pinterest_link"];
            if ( [link_is isEqual:[NSNull null]]||[link_is isEqualToString:@""]) {
                
            }
            else{
                NSDictionary *detail_dict=@{@"key":@"pinterest",@"link":link_is};
                [links_array addObject:detail_dict];
            }
            
            link_is=[link_dict_is valueForKey:@"instagram_link"];
          if ( [link_is isEqual:[NSNull null]]||[link_is isEqualToString:@""]) {
               
            }
           else{
               NSDictionary *detail_dict=@{@"key":@"instagram",@"link":link_is};
               [links_array addObject:detail_dict];
           }
          

            if (links_array.count>0) {
                
           links_Main_view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
                links_Main_view.backgroundColor=[UIColor colorWithRed:(17/255.0) green:(17/255.0) blue:(17/255.0) alpha:0.4];
        
           links_sub_view = [[UIView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/2-140, links_Main_view.frame.size.height/2-60, 280, 120)];
                links_sub_view.backgroundColor=[UIColor whiteColor];
                
            UIButton *close_but=[[UIButton alloc]initWithFrame:CGRectMake(5, 5, 35, 35)];
            
            [close_but setImage:[UIImage imageNamed:@"close_Small"] forState:UIControlStateNormal];
            [close_but addTarget:self action:@selector(Close_social) forControlEvents:UIControlEventTouchUpInside];
            [links_sub_view addSubview:close_but];
                
                
                for (int i=0; i<links_array.count; i++) {
                    
                    int x=0;
                    if (i==0) {
                    
                    }
                    else{
                        x=i*links_sub_view.frame.size.width/links_array.count;
                    }
                NSString *key=[[links_array objectAtIndex:i] valueForKey:@"key"];
                    if ([key isEqualToString:@"fb"]) {
                        key=@"fb_icon";
                    }
                    if ([key isEqualToString:@"twitter"]) {
                         key=@"twitter";
                    }
                    if ([key isEqualToString:@"instagram"]) {
                         key=@"instragram";
                    }
                    if ([key isEqualToString:@"pinterest"]) {
                         key=@"pinrest";
                    }
                UIView *button_view=[[UIView alloc]initWithFrame:CGRectMake(x, 50, links_sub_view.frame.size.width/links_array.count, 50)];
                    
                UIButton *link_but=[[UIButton alloc]initWithFrame:CGRectMake(button_view.frame.size.width/2-25, 0, 50, 50)];
                    
                link_but.backgroundColor=[UIColor clearColor];
               [link_but setImage:[UIImage imageNamed:key] forState:UIControlStateNormal];
                link_but.tag=i;
                    
                [link_but addTarget:self action:@selector(link_Action:) forControlEvents:UIControlEventTouchUpInside];
                [button_view addSubview:link_but];
                [links_sub_view addSubview:button_view];
                }
                [self.view addSubview:links_Main_view];
                [self.view addSubview:links_sub_view];
                
                [links_sub_view bringSubviewToFront:self.view];
                
            }
        }
        else if([[response valueForKey:STATUS]isEqualToString:FAIL])
        {
            links_array=[response valueForKey:@"socialmedia_links"];
            if (links_array.count>0) {
                
            }
             indicaterview.hidden=YES;
             [SHARED_HELPER showAlert:@"No Socail Pages Available"];
        }
        
    } onfailure:^(NSError *theError) {
         indicaterview.hidden=YES;
        [SHARED_HELPER showAlert:ServerConnection];
    }];
    
    
}
-(void)Close_social{
   
    [UIView animateWithDuration:.50 animations:^{
        links_sub_view.transform = CGAffineTransformMakeScale(1.3, 1.3);
        links_sub_view.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            [links_Main_view removeFromSuperview];
            [links_sub_view removeFromSuperview];
        }
    }];
    
}
-(void)link_Action:(UIButton *)sender{
    NSString *link_is=[[links_array objectAtIndex:sender.tag] valueForKey:@"link"];
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:link_is]];
    
}
-(void)Save_Ride_From_Home{
    [self Rides_Table_Update_Method:@"Save"];
    [self Routes_Table_Update_Method];
    
    if ([appDelegate.is_Running_Background_Sync isEqualToString:@"NO"]) {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SaveRide_Service" object:self]; //in appdelegate
    }
}
#pragma mark - CoreDataMethodes------>
- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}
-(NSArray *)get_pending_Rides_From_DB{
    
    NSManagedObjectContext *myContext = [self managedObjectContext];
    NSFetchRequest *req=[NSFetchRequest fetchRequestWithEntityName:Rides_Table];
    NSArray *ride=[myContext executeFetchRequest:req error:nil];
    NSMutableArray *rides_results_array=[[NSMutableArray alloc]init];
    for (Rides *object in ride) {
        if ([object.ride_status isEqualToString:table_status_pending]) {
            [rides_results_array addObject:object];
        }
    }
    return rides_results_array;
}
-(Routes *)get_pending_Route_From_DB{
    NSManagedObjectContext *myContext = [self managedObjectContext];
    NSFetchRequest *req=[NSFetchRequest fetchRequestWithEntityName:Routes_Table];
    NSNumber *stdUserNumber = [NSNumber numberWithLongLong:pending_Ride_id];
    req.predicate=[NSPredicate predicateWithFormat:@"local_rid == %@",stdUserNumber];
    NSArray *route=[myContext executeFetchRequest:req error:nil];
    Routes *object=[route objectAtIndex:0];
    return object;
}
-(void)Rides_Table_Update_Method :(NSString *)call_For
{
    NSError *error;
    NSFetchRequest * fr = [[NSFetchRequest alloc]initWithEntityName:Rides_Table];
    NSNumber *stdUserNumber = [NSNumber numberWithLongLong:pending_Ride_id];
    fr.predicate=[NSPredicate predicateWithFormat:@"local_rid ==%@",stdUserNumber];
    NSMutableArray * result = [[self.managedObjectContext executeFetchRequest:fr error:&error] mutableCopy];
    if (result.count>0) {
        Rides *rides=[result objectAtIndex:0];
        if ([call_For isEqualToString:@"Quit"]) {
            rides.ride_status=table_status_quit;
        }
        else
        {
            NSDictionary *Save_Ride_Dict=[appDelegate.Save_Route_Data_Array objectAtIndex:1];
            NSArray *ride_Images=[appDelegate.Save_Route_Data_Array objectAtIndex:2];
            rides.ride_name=[Save_Ride_Dict valueForKey:@"RideTitle"];
            rides.ride_descr=[Save_Ride_Dict valueForKey:@"RideDescriptionDes"];
            rides.ride_status=table_status_complete;
            NSString *ride_images_str=[ride_Images componentsJoinedByString:@","];
            rides.ride_images=ride_images_str;
        }
        
        [rides.managedObjectContext save:nil];
    }
}
-(void)Routes_Table_Update_Method
{
    NSError *error;
    NSFetchRequest * fr = [[NSFetchRequest alloc]initWithEntityName:Routes_Table];
    NSNumber *stdUserNumber = [NSNumber numberWithLongLong:pending_Ride_id];
    fr.predicate=[NSPredicate predicateWithFormat:@"local_rid ==%@",stdUserNumber];
    NSMutableArray * result = [[self.managedObjectContext executeFetchRequest:fr error:&error] mutableCopy];
    if (result.count>0) {
    Routes *routes=[result objectAtIndex:0];
    
    NSDictionary *Save_Route_Dict=[appDelegate.Save_Route_Data_Array objectAtIndex:0];
    routes.route_name=[Save_Route_Dict valueForKey:@"Title"];
    routes.route_desc=[Save_Route_Dict valueForKey:@"Des"];
    routes.route_privacy=[Save_Route_Dict valueForKey:@"RouteStatus"];
    routes.route_comments=[Save_Route_Dict valueForKey:@"Comments"];
    routes.route_ratings=[Save_Route_Dict valueForKey:@"Rating"];
    [routes.managedObjectContext save:nil];
    }
}

-(void)Show_Ride_Alert
{
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Unfinished Ride" message:@"You have unfinished ride. Do you want Continue the ride, or Save the ride or Quit?" preferredStyle:UIAlertControllerStyleAlert];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"SAVE" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(Save_Ride_From_Home) name:@"Save_Ride_From_Home" object:nil];
        
        Routes *object=[self get_pending_Route_From_DB];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SaveARoute*SaveARoute = [storyboard instantiateViewControllerWithIdentifier:@"SaveARoute"];
        SaveARoute.seletced_route_title=object.route_name;
        if ([Rides_object.user_id isEqualToString:Rides_object.ride_owner_user_id]) {
            SaveARoute.ride_Owned_BY_ME=@"YES";
        }
        else
        {
            SaveARoute.ride_Owned_BY_ME=@"NO";
        }
        SaveARoute.is_From=@"HOME";
        SaveARoute.route_des=object.route_desc;
        appDelegate.FreeRide_Name=Rides_object.ride_name;
        SaveARoute.ride_description=Rides_object.ride_descr;
        
        if ([object.route_name isEqualToString:@""]) {
            
            BIZPopupViewController *addAPlacePopupView = [[BIZPopupViewController alloc] initWithContentViewController:SaveARoute contentSize:CGSizeMake(self.view.frame.size.width-40,440)];
            [self presentViewController:addAPlacePopupView animated:NO completion:nil];
        }
        else
        {
            
            BIZPopupViewController *addAPlacePopupView = [[BIZPopupViewController alloc] initWithContentViewController:SaveARoute contentSize:CGSizeMake(self.view.frame.size.width-40,380)];
            [self presentViewController:addAPlacePopupView animated:NO completion:nil];
        }
           
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"CONTINUE" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        RideDashboard *cntlr = [self.storyboard instantiateViewControllerWithIdentifier:@"RideDashboardStoryboard"];
        cntlr.is_From=@"HOME";
        [self.navigationController pushViewController:cntlr animated:YES];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"QUIT" style:UIAlertActionStyleCancel  handler:^(UIAlertAction *action) {
        
        if ([Rides_object.ride_id isEqualToString:@""]) {
            if ([self Deleted_Saved_Ride_Details_in_AllTables:Rides_Table loc_ride_id:pending_Ride_id]) {
                [self Deleted_Saved_Ride_Details_in_AllTables:Routes_Table loc_ride_id:pending_Ride_id];
                [self Deleted_Saved_Ride_Details_in_AllTables:Locations_Table loc_ride_id:pending_Ride_id];
                [self Deleted_Saved_Ride_Details_in_AllTables:Waypoints_Table loc_ride_id:pending_Ride_id];
                [self Deleted_Saved_Ride_Details_in_AllTables:Add_places_Table loc_ride_id:pending_Ride_id];
            }
        }
        else
        {
            [self Rides_Table_Update_Method :@"Quit"];
            if ([appDelegate.is_Running_Background_Sync isEqualToString:@"NO"]) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"SaveRide_Service" object:self]; //in appdelegate
            }
        }
    }]];
    
    
    [self presentViewController:actionSheet animated:YES completion:nil];
}
- (BOOL)Deleted_Saved_Ride_Details_in_AllTables:(NSString *)entity loc_ride_id:(int64_t )rid
{
    NSError *error = nil;
    NSManagedObjectContext *myContext = [self managedObjectContext];
    NSFetchRequest *fetchLLObjects = [[NSFetchRequest alloc] init];
    
    [fetchLLObjects setEntity:[NSEntityDescription entityForName:entity inManagedObjectContext:myContext]];
    NSNumber *stdUserNumber = [NSNumber numberWithLongLong:rid];
    fetchLLObjects.predicate=[NSPredicate predicateWithFormat:@"local_rid ==%@",stdUserNumber];
    [fetchLLObjects setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSArray *allObjects = [myContext executeFetchRequest:fetchLLObjects error:&error];
    for (NSManagedObject *object in allObjects) {
        [myContext deleteObject:object];
    }
    
    if ([myContext save:&error]) {
        NSLog(@"Empty saved  ");
        return YES;
    }
    else
    {
        NSLog(@"Empty not saved  ");
        return NO;
    }
}


//void report_memory(void) {
//    struct task_basic_info info;
//    mach_msg_type_number_t size = sizeof(info);
//    kern_return_t kerr = task_info(mach_task_self(),
//                                   TASK_BASIC_INFO,
//                                   (task_info_t)&info,
//                                   &size);
//    if( kerr == KERN_SUCCESS ) {
//        NSLog(@"Memory in use (in bytes): %lu", info.resident_size);
//        NSLog(@"Memory in use (in MB): %f", ((CGFloat)info.resident_size / 1000000));
//    } else {
//        NSLog(@"Error with task_info(): %s", mach_error_string(kerr));
//    }
//}


- (IBAction)onClick_rideGoingBtn:(id)sender {
    self.rideGoingBtn.hidden=YES;
    self.homeDashboard_scrollLabel.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }

}
- (IBAction)clicked_items_Here:(UIButton *)sender
{
    
    if (sender.tag==0)
    {
       
        if ([self check_Location_On_Or_Off])
        {
            DashboardViewController *dashboard = [self.storyboard instantiateViewControllerWithIdentifier:@"DashboardViewController"];
            [self.navigationController pushViewController:dashboard animated:YES];
        }
    }
    else if (sender.tag ==1)
    {
        RoutesVC *cntlr = [self.storyboard instantiateViewControllerWithIdentifier:@"RoutesVC"];
        [self.navigationController pushViewController:cntlr animated:NO];
    }
    else if (sender.tag ==2)
    {
        BrowseRidersViewController *cntrl = [self.storyboard instantiateViewControllerWithIdentifier:@"BrowseRidersViewController"];
        [self.navigationController pushViewController:cntrl animated:NO];
    }
    else if (sender.tag ==3)
    {
        Promotions*promotions = [self.storyboard instantiateViewControllerWithIdentifier:@"Promotions"];
        [self.navigationController pushViewController:promotions animated:NO];
        
    }
    else if (sender.tag ==4)
    {
        EventsViewController *cntlr  = [self.storyboard instantiateViewControllerWithIdentifier:@"EventsViewController"];
        [self.navigationController pushViewController:cntlr animated:NO];
        
    }
    else if (sender.tag ==5)
    {
        if ([self check_Location_On_Or_Off])
        {
            MarketPlace*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"MarketPlace"];
            [self.navigationController pushViewController:controller animated:NO];
        }
    }
    else if (sender.tag ==6)
    {
        NewsViewController *cntlr = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsViewController"];
        [self.navigationController pushViewController:cntlr animated:YES];
    }
    else if (sender.tag ==7)
    {
        Feedback *cntrl = [self.storyboard instantiateViewControllerWithIdentifier:@"Feedback"];
        [self.navigationController pushViewController:cntrl animated:NO];
    }else if (sender.tag ==8)
    {
        [self Social_Link_Service];
    }
    
}
@end
