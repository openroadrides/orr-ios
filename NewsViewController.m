//
//  NewsViewController.m
//  openroadrides
//
//  Created by apple on 29/05/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "NewsViewController.h"
#import "NewsTableViewCell.h"
#import "AppHelperClass.h"
#import "APIHelper.h"
#import "NewsDetailViewController.h"

@interface NewsViewController (){
    
    NSArray *news_ary,*menu_Items_Array;
    UIActivityIndicatorView  *activeIndicatore;
    NSMutableArray *check_strings_array;
    NSString *menu_Status;
    
}

@end

@implementation NewsViewController

- (void)viewDidLoad {
      [super viewDidLoad];
    menu_Status=@"LEFT";
        self.title = @"NEWS";
        [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"Antonio-Bold" size:20]}];
    
    _News_tableview.hidden = YES;
    
    
    self.news_scrollLabel.hidden=YES;
    self.news_scrollLabel.text = @"   Ride is ongoing. Touch to open the Ride.             Ride is ongoing. Touch to open the Ride.";
    [self.news_scrollLabel setFont:[UIFont fontWithName:@"soho-std-regular" size:15.0]];
    self.news_scrollLabel.textColor = [UIColor blackColor];
    self.news_scrollLabel.backgroundColor=APP_YELLOW_COLOR;
    self.news_scrollLabel.labelSpacing = 30; // distance between start and end labels
    self.news_scrollLabel.pauseInterval = 0.0; // seconds of pause before scrolling starts again
    self.news_scrollLabel.scrollSpeed = 60; // pixels per second
    self.news_scrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.news_scrollLabel.fadeLength = 0.f;
    self.news_scrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.news_scrollLabel observeApplicationNotifications];

    self.news_rideGoingBtn.hidden=YES;
    if (appDelegate.ridedashboard_home) {
        
        self.news_scrollLabel.hidden=NO;
        self.news_rideGoingBtn.hidden=NO;
    }
    UIBarButtonItem *menuButton=[[UIBarButtonItem alloc]initWithImage:
                                 [[UIImage imageNamed:@"sidemenuicon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
                                                                style:UIBarButtonItemStylePlain target:self action:@selector(Menu_Action:)];
    
    self.navigationItem.leftBarButtonItem = menuButton;
    
    //Side Menu
    swipe_right=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(menuright)];
    swipe_right.direction=UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipe_right];
    
    swipe_left=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(menuleft)];
    swipe_left.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipe_left];
    
    self.User_image.layer.cornerRadius=self.User_image.frame.size.width/2;
    self.User_image.clipsToBounds=YES;
    self.User_image.layer.borderColor = [UIColor whiteColor].CGColor;
    self.User_image.layer.borderWidth = 4.0;
//   menu_Items_Array=@[@"Dashboard",@"Rides",@"Routes",@"Events",@"News",@"Friends",@"Groups",@"Find Riders",@"Privacy Policy",@"Terms & Conditions",@"Logout"];
    
    
    menu_Items_Array=@[@"Home",@"Dashboard",@"Privacy Policy",@"Terms & Conditions",@"Logout"];
    
    [_side_Menu_Table reloadData];
    self.User_Name.text=[NSString stringWithFormat:@"%@",[Defaults valueForKey:@"UserName"]];
    //
    
    check_strings_array=[[NSMutableArray alloc]init];
    [check_strings_array addObject:@"<null>"];
    [check_strings_array addObject:@"null"];
    [check_strings_array addObject:@""];
    [check_strings_array addObject:@"(null)"];
    
  
    // Do any additional setup after loading the view.
    
    [self get_news_list];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    _side_Menu_view.hidden=YES;
    self.navigationController.navigationBar.hidden=NO;
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    [self display_user_Profile];
}

-(void)display_user_Profile{
    
    NSString *user_id=[Defaults valueForKey:User_ID];
    [SHARED_API DisplayUserProfile:user_id withSuccess:^(NSDictionary *response) {
        
        [self user_Profile_Data_Sucsess:response];
        
    } onfailure:^(NSError *theError) {
        
    }];
}
-(void)user_Profile_Data_Sucsess:(NSDictionary *)Profile_data{
    if ([[Profile_data valueForKey:STATUS] isEqualToString:SUCCESS]) {
        NSDictionary *user_data=[Profile_data valueForKey:@"data"];
        self.User_Name.text=[user_data valueForKey:@"name"];
        [Defaults setObject:self.User_Name.text forKey:@"UserName"];
        
        NSString *profile_image_str=[NSString stringWithFormat:@"%@",[user_data valueForKey:@"profile_image"]];
        
        if (![check_strings_array containsObject:profile_image_str] && ![profile_image_str isEqualToString:@"<null>"])
        {
            NSURL*url=[NSURL URLWithString:profile_image_str];
            [self.User_image sd_setImageWithURL:url
                               placeholderImage:[UIImage imageNamed:@"user_Placeholder"]
                                        options:SDWebImageRefreshCached];
        }
        friends_count = [[NSString stringWithFormat:@"%@",[[Profile_data valueForKey:@"data"] valueForKey:@"total_friends"]] intValue];
        
        
        groups_count = [[NSString stringWithFormat:@"%@",[[Profile_data valueForKey:@"data"] valueForKey:@"total_groups"]] intValue];
        
        [_side_Menu_Table reloadData];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Tableview methods......

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==_side_Menu_Table) {
        return menu_Items_Array.count;
    }

    return news_ary.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView==_side_Menu_Table)
    {
        static NSString *simpleTableIdentifier = @"MenuTableViewCell";
        MenuTableViewCell *cell = (MenuTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        self.side_Menu_Table.separatorStyle=UITableViewCellSeparatorStyleNone;
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MenuTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.menu_Item_Label.text=[menu_Items_Array objectAtIndex:indexPath.row];
        cell.menu_Item_Label.textColor=[UIColor colorWithRed:255/255.0 green:222/255.0 blue:0/255.0 alpha:1.0];
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = [UIColor colorWithRed:255/255.0 green:222/255.0 blue:0/255.0 alpha:1.0];
        [cell setSelectedBackgroundView:bgColorView];
        cell.menu_Item_Label.highlightedTextColor=[UIColor blackColor];
        tableView.allowsMultipleSelection=NO;
        
        
        cell.menu_item_Count_Lab.hidden=YES;
        if (indexPath.row==0)
        {
            cell.menu_Item_iCon.image=[UIImage imageNamed:@"homeicon"];
            
        }
        else if (indexPath.row==1)
        {
            cell.menu_Item_iCon.image=[UIImage imageNamed:@"Dashboard_blue"];
        }
        else if (indexPath.row==2)
            
        {
            cell.menu_Item_iCon.image=[UIImage imageNamed:@"privacy"];
        }
        else if (indexPath.row==3)
            
        {
            cell.menu_Item_iCon.image=[UIImage imageNamed:@"terms"];
        }
        else if (indexPath.row==4)
            
        {
            cell.menu_Item_iCon.image=[UIImage imageNamed:@"Logout"];
        }
        
        return cell;
    }
    else{
    static NSString *simpleTableIdentifier = @"NewsCell";
    NewsTableViewCell *cell = (NewsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    self.News_tableview.separatorStyle=UITableViewCellSeparatorStyleNone;
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NewsTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    if (indexPath.row%2==0) {
        
        cell.News_content_view.backgroundColor = ROUTES_CELL_BG_COLOUR1;
    }
    else{
        
        cell.News_content_view.backgroundColor = ROUTES_CELL_BG_COLOUR2;
        
        
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
  
    
    //   #pragma mark - Set Date Format...........
    //
    NSString *date_str = [NSString stringWithFormat:@"%@",[[news_ary objectAtIndex:indexPath.row] valueForKey:@"news_date"]]; /// here this is your date with format yyyy-MM-dd
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
    [dateFormatter setDateFormat:@"yyyy-MM-dd"]; //// here set format of date which is in your output date (means above str with format)
    
    NSDate *date = [dateFormatter dateFromString: date_str]; // here you can fetch date from string with define format
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMMM dd,yyyy"];// here set format which you want...
    NSString *convertedString = [dateFormatter stringFromDate:date]; //here convert date in NSString
    NSLog(@"Converted String : %@",convertedString);
    news_id_str = [NSString stringWithFormat:@"%@", [[news_ary objectAtIndex:indexPath.row] valueForKey:@"news_id"]];
    cell.News_description_lbl.text = [[news_ary objectAtIndex:indexPath.row] valueForKey:@"news_description"];
    cell.News_date_lbl.text = convertedString;
    cell.News_title_lbl.text = [[news_ary objectAtIndex:indexPath.row] valueForKey:@"news_title"];
        
        
        NSString*news_image_string=[NSString stringWithFormat:@"%@",[[news_ary objectAtIndex:indexPath.row] valueForKey:@"news_image_1_1"]];
        NSURL*url=[NSURL URLWithString:news_image_string];
        [cell.News_image_view sd_setImageWithURL:url
                           placeholderImage:[UIImage imageNamed:@"News_Placeholder"]
                                    options:SDWebImageRefreshCached];

        
        
//    [self photoForCell:cell user:[[news_ary objectAtIndex:indexPath.row] valueForKey:@"news_image_1_1"] withIndexPath:indexPath];
    return cell;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_side_Menu_Table) {
        return 45;
    }
    
    return 114;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_side_Menu_Table) {
//        if (indexPath.row==4) {
            [cell setSelected:NO animated:NO];
//        }
//        else{
//            [cell setSelected:NO animated:NO];
//        }
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView==_side_Menu_Table) {
        if (indexPath.row==0)
        {
            HomeDashboardView*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"HomeDashboardView"];
            [self.navigationController pushViewController:controller animated:YES];
        }
        
        if (indexPath.row == 1)
        {
            DashboardViewController*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"DashboardViewController"];
            [self.navigationController pushViewController:controller animated:YES];
        }
        else if (indexPath.row == 2)
        {
            PrivacyPolicy*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"PrivacyPolicy"];
            [self.navigationController pushViewController:controller animated:YES];
        }
        else if (indexPath.row == 3)
        {
            TermsAndConditions *cntrl = [self.storyboard instantiateViewControllerWithIdentifier:@"TermsAndConditions"];
            [self.navigationController pushViewController:cntrl animated:YES];
        }
        else  if (indexPath.row == 4) {
            
            if (appDelegate.ridedashboard_home) {
                [self logoutRideGoingAlert:@"You cannot logout while the ride is going on."];
            }
            
            else
            {
                [self showpopup:Terminate];
            }

            [self.side_Menu_Table reloadData];
        }
        
          [self menuleft];
    }
    else{
    NewsDetailViewController *cntrl = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsDetailViewController"];
       cntrl.news_id = [NSString stringWithFormat:@"%@", [[news_ary objectAtIndex:indexPath.row] valueForKey:@"news_id"]];
    [self.navigationController pushViewController:cntrl animated:YES];
    }
    
    
}

#pragma mark Download cell image
//- (void)displayImage:(UIImageView*)imageView withImage:(UIImage*)image  {
//    [imageView setImage:image];
//    imageView.contentMode = UIViewContentModeScaleAspectFit;
//    [imageView setupImageViewer];
//    imageView.clipsToBounds = YES;
//}
//download pic
- (void)photoForCell:(NewsTableViewCell *)cell user:(NSString *)user withIndexPath:(NSIndexPath *)indexPath
{
    NSURL *url = [NSURL URLWithString:user];
    [self downloadImageWithURL:url completionBlock:^(BOOL succeeded, UIImage *image) {
        if (succeeded) {
            
            if (image==nil) {
                //cell.userImageView.image=[UIImage imageNamed:@"userLogo.png"];
            }
            else{
                cell.News_image_view.image=image;
                
            }
            
        }
    }];
    
}
- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES,image);
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
}



-(void)addclicked
{
    
}
-(void)back{
    
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)leftBtn:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)get_news_list{
    
    
    
    if (![SHARED_HELPER checkIfisInternetAvailable])
    {
        [SHARED_HELPER showAlert:Nonetwork];
        return;
    }

    
    activeIndicatore = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activeIndicatore.center = self.view.center;
    activeIndicatore.color = APP_YELLOW_COLOR;
    activeIndicatore.hidesWhenStopped = TRUE;
    [activeIndicatore startAnimating];
    [self.view addSubview:activeIndicatore];
    
    
    [SHARED_API newsRequestsWithParams:nil withSuccess:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           
                           NSLog(@"New data is %@",response);
                           if ([[response valueForKey:STATUS] isEqualToString:FAIL]) {
                               [SHARED_HELPER showAlert:ServiceFail];
                               
                               [self service_fail];
                           }
                           else
                           {
                               NSString *respons_str=[NSString stringWithFormat:@"%@",[response valueForKey:@"news"]];
                               if (respons_str ==nil|| [respons_str isEqualToString:@""]) {
                                    [SHARED_HELPER showAlert:NotificatiosEmpty];
                                    [self service_fail];
                               }
                               else{
                               news_ary = [response valueForKey:@"news"];
                                   if (news_ary.count>0) {
                                       
                                       [_News_tableview reloadData];
                                       _News_tableview.hidden = NO;
                                   }
                                   else{
                                       _no_Data_Label.hidden=NO;
                                   }
                           }
                        }
                    [activeIndicatore stopAnimating];
                       });
        
    } onfailure:^(NSError *theError) {
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           [SHARED_HELPER showAlert:ServiceFail];
                           
                            [activeIndicatore stopAnimating];
                           _News_tableview.hidden=YES;
                            [self service_fail];
                       });
    }];
}

-(void)service_fail{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:YES];
    });
}
-(void)menuleft
{
    _side_Menu_view.hidden=NO;
    menu_Status=@"LEFT";
    [UIView animateWithDuration:0.5 delay:0.3 options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.menu_Leading_Constraint.constant=-300;
                         [self.view layoutIfNeeded];
                     } completion:^(BOOL finished) {
                         self.News_tableview.alpha=1;
                         
                         self.News_tableview.userInteractionEnabled=YES;
                     }];
}
-(void)menuright
{
    
    menu_Status=@"RIGHT";
    _side_Menu_view.hidden=NO;
    [UIView animateWithDuration:0.5 delay:0.3 options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.menu_Leading_Constraint.constant=0;
                         [self.view layoutIfNeeded];
                     } completion:^(BOOL finished)
     {
         
         self.News_tableview.alpha=0.8;
         self.News_tableview.userInteractionEnabled=NO;
     }];
}


- (IBAction)Menu_Action:(id)sender {
    _side_Menu_view.hidden=NO;
    if ([menu_Status isEqualToString:@"LEFT"]) {
        [self menuright];
        
    }
    else{
        [self menuleft];
    }
}
- (IBAction)Profile_Action:(id)sender {
    
    ProfileView*controller=[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileView"];
    [self.navigationController pushViewController:controller animated:YES];
    [self menuleft];
}
-(void)logoutRideGoingAlert:(NSString *)alert_msg
{
    UIAlertController *alertview = [UIAlertController alertControllerWithTitle:@"Can't Logout" message:alert_msg preferredStyle:UIAlertControllerStyleAlert];
    
    [alertview addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        
    }]];
    [self presentViewController:alertview animated:YES completion:nil];
}

-(void)showpopup:(NSString *)message
{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"LOGOUT"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                             NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                             NSData *uData = [NSKeyedArchiver archivedDataWithRootObject:@""];
                             [defaults setObject:uData forKey:User_ID];
                             NSString *user_data;
                             user_data = [defaults objectForKey:User_ID];
                             [defaults removeObjectForKey:User_ID];
                             [defaults removeObjectForKey:@"UserName"];
                             [[FBSDKLoginManager new] logOut];
                             [[GIDSignIn sharedInstance] signOut];
                             [defaults synchronize];
                             
                             LoginViewController *login = [self.storyboard  instantiateViewControllerWithIdentifier:@"LoginViewController"];
                             [self.navigationController pushViewController:login animated:YES];
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"CANCEL"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)onClick_news_rideGoingBtn:(id)sender {
    
    self.news_scrollLabel.hidden=YES;
    self.news_rideGoingBtn.hidden=YES;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for(UIViewController *tempVC in navigationArray)
    {
        if([tempVC isKindOfClass:[RideDashboard class]])
        {
            [self.navigationController popToViewController:tempVC animated:NO];
        }
    }
}
@end
