//
//  RideInvitesPublicFrndsCell.h
//  openroadrides
//
//  Created by apple on 03/07/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RideInvitesPublicFrndsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *rideinvites_publicFrndsCell_bgView;
@property (weak, nonatomic) IBOutlet UIImageView *rideDetails_publicFrndsCell_profileImage;
@property (weak, nonatomic) IBOutlet UIButton *rideInvites_publicFrnds_checkBox_btn;
@property (weak, nonatomic) IBOutlet UILabel *rideDetails_publicFrndsCell_userName;
@property (weak, nonatomic) IBOutlet UIImageView *rideDetails_publicFrndsCell_location_imageView;
@property (weak, nonatomic) IBOutlet UILabel *rideInvites_publicFrndsCell_addressLabel;
@property (weak, nonatomic) IBOutlet UIView *rideInvites_publicFrnds_statusView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewTop_username_constraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rideInvites_viewTop_address_constraint;
@property (weak, nonatomic) IBOutlet UILabel *rideInvites_PublicFrnds_statusLabel;

@end
