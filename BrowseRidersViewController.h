//
//  BrowseRidersViewController.h
//  openroadrides
//
//  Created by Jyothsna on 7/24/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CBAutoScrollLabel.h"
@interface BrowseRidersViewController : UIViewController
{
    UIActivityIndicatorView  *activeIndicatore;
    NSUserDefaults *defaults;
    NSString *user_token_id,*screenstrng;
    BOOL searchstatus;
    UILabel *NO_DATA;
    UIView *no_Data_View;
    NSArray *riders_ary;
    NSString *search_Place_lat,*search_place_Long, *isFromFindRiders;
}
@property (strong, nonatomic) IBOutlet UIView *listmainview;
@property (strong, nonatomic) IBOutlet UITableView *list_tableview;
@property (strong, nonatomic) IBOutlet UIButton *buttom_btn;
@property (strong, nonatomic) IBOutlet UIButton *map_list_action;
- (IBAction)map_list_action:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *map_list_image_view;
@property (strong, nonatomic) IBOutlet UILabel *no_Riders_Label;
@property (strong, nonatomic) IBOutlet UIButton *re_center_btn;
- (IBAction)Re_center_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *browseRiders_rideGoingBtn;
- (IBAction)onClick_browseRiders_rideGoingBtn:(id)sender;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *browseRiders_scrollLabel;

@end
