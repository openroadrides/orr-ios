//
//  BrowsRiderCell.h
//  openroadrides
//
//  Created by Ebiz  on 04/05/1939 Saka.
//  Copyright © 1939 Saka apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrowsRiderCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *profileimage;
@property (strong, nonatomic) IBOutlet UILabel *name_label;
@property (strong, nonatomic) IBOutlet UILabel *address_label;
@property (strong, nonatomic) IBOutlet UIImageView *loactionimageview;

@property (strong, nonatomic) IBOutlet UIView *content_view;

@end
