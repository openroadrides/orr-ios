//
//  MapViewController.h
//  openroadrides
//
//  Created by apple on 14/07/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <GPXParser.h>
#import "AppDelegate.h"
#import "RideDashboard.h"
#import "CBAutoScrollLabel.h"
@interface MapViewController : UIViewController<GMSMapViewDelegate>
{
    NSMutableArray *places_images,*comments_images,*add_places,*tracks_array,*way_array;
    GMSMapView *mapView;
    GMSMutablePath *line_path;
    NSString *start_lat_long,*end_lat_long;
    GMSPolyline *polyline;
    double x,z;
    int mapZoom;
    UIActivityIndicatorView *activeIndicatore;
}
@property (weak, nonatomic) IBOutlet UIView *view_map;
@property(strong,nonatomic)NSArray *places_data_array,*way_points_array;
@property (nonatomic, strong) NSString *gpx_file,*encoded_str;
@property (weak, nonatomic) IBOutlet UIButton *routeMap_rideGoingBtn;
- (IBAction)onClick_routeMap_rideGoingBtn:(id)sender;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *routeMap_scrollLabel;
@end
